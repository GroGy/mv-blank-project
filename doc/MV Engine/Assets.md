Currently, all the assets in the engine are implemented using `MV::Resource` and `MV::AssetType`.

When asset needs to be loaded, it has to be done through something called `Resource`. `Resource` is a class from which Asset loader must inherit and implement `Process` and `Finish` functions.

## Process
* Process function is used to do the heavy loading part, like reading from disc, preprocessing data for GPU etc.
* This is done on Resource Loader thread pool and should be used carefully to not touch any non-thread safe environment.
## Finish
* Finish function is used to store the prepared data in memory, transfer it to proper storage and load to GPU. This should be as quick as possible
* This is called from main thread at the end of each frame.

