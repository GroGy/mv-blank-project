MV Engine includes custom step in build process called precompile.
During this step, precompiler executable is started to process all files in project (Engine and Editor source can be excluded) to process all the custom attributes to generate custom metadata.

This custom metadata contains info about fields, serialization, registers custom properties and more.

# Attributes

## MV::Class()
MV::Class can be specified on any class inheriting from `MV::EngineObject`.
This then generates Meta info about this class, allowing Meta system to be used to instantiate it, getting the class info passed to different system to instruct them which class to instantiate and more.

Meta classes are instantiated using either default constructor (with no arguments) or using the `ObjectInitializationContext` constructor if it's present. 

More extra attribute parameters can then be passed to this allowing for more functionality.
### Abstract
Abstract keyword specifies, that class is abstract type, this is only way to create Meta for class without having to inherit from `MV::EngineObject`.
Abstract meta classes will not get their meta constructors generated, which means that they can't be instantiated directly.
### Property
Property keyword parameter specifies, this class is Property and should be registered in property management system.
### Internal
Internal keyword is mainly used for internal engine purposes. It is used to hide certain classes in assembly inspector, when creating config objects.

### EditorWindow
**Editor Only**
Keyword parameter used to register classes inheriting from `Editor::Window` to editor windowing system, to allow `Editor::GetWindow<Type>` to access the internally created instance.

### PropertyRenderer
**Editor Only**
Keyword parameter used to register property renderer object. (Renderer for property in object inspector)
This object must inherit from `PropertyRenderer<PropertyType>` and must implement public function `Draw` with following description: 
`static bool(const MV::Meta::Field&, const MV::rc<MV::WorldObject>&,MV::Property*)`
No instances of this class are created, all the drawing is done in static context.
### PropertyGizmo
**Editor Only**
Keyword parameter used to register property gizmo object, which is used to draw Gizmo elements in viewport for selected property.
This object must inherit from `PropertyViewportRenderer<PropertyType>` and must implement public function RenderProperty with following description: 
`static void(const glm::vec4&,MV::Property*)`, where first parameter is viewport Bounding Box.

## MV::Trivial()
Trivial is special attribute, used for classes which we only want to serialize and allow very limited meta info about.
Classes with `Trivial` attribute support serialization, but their meta only contains following info:
* Class name
* That they are trivial
* If they are abstract

## MV::Field()
  
Property                    = 1 << 1,  
DisplayName                 = 1 << 2,  
AnimationCompatible         = 1 << 3,  
Serializable                = 1 << 4,  
Replicated                  = 1 << 5,  
HideInEditor                = 1 << 6