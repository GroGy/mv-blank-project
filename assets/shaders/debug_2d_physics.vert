#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 inPos;
layout(location = 1) in vec4 inColor;

layout( push_constant ) uniform constant
{
    mat4 MVP;
} PushConstants;

layout(location = 0) out vec4 fragColor;

void main() {
    gl_Position = PushConstants.MVP * vec4(inPos,0.0, 1.0);
    fragColor = inColor;
}