#version 450
#extension GL_ARB_separate_shader_objects : enable

#define ENABLE_PARTICLE_TEXTURE
#define MV_EDITOR

layout(location = 0) in vec4 particleColor;
layout(location = 1) in vec2 uv;
layout(location = 2) in flat int isAlive;

layout(location = 0) out vec4 outColor;

#if defined(MV_EDITOR)
layout(location = 1) out vec4 outId;
#endif

#if defined(ENABLE_PARTICLE_TEXTURE)
layout(binding = 0) uniform sampler2D uTexture;
#endif

#if defined(MV_EDITOR)
layout(binding = 1) uniform uEditorBuffer {
    uint objectId;
};
#endif

void main() {
    if(isAlive == 0) discard;

    #if defined(ENABLE_PARTICLE_TEXTURE)
    outColor = texture(uTexture, uv) * particleColor;
    #else
    outColor = particleColor;
    #endif


    #if defined(MV_EDITOR)
    outId = objectId;
    #endif
}