#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 inPos;
layout(location = 1) in vec2 inUV;

layout(location = 2) in vec2 inParticlePos;
layout(location = 3) in vec2 inParticleScale;
layout(location = 4) in float inParticleRotation;
layout(location = 5) in int inParticleAlive;
layout(location = 6) in vec4 inParticleColor;
layout(location = 7) in vec4 inParticleUV;

layout( push_constant ) uniform constant
{
    mat4 MVP;
} PushConstants;

layout(location = 0) out vec4 particleColor;
layout(location = 1) out vec2 uv;
layout(location = 2) out flat int isAlive;

void main() {
    gl_Position = PushConstants.MVP * vec4((inPos*inParticleScale) + inParticlePos,0.0, 1.0);
    particleColor = inParticleColor;
    uv = vec2(inParticleUV.x + (inUV.x * inParticleUV.z), inParticleUV.y + (inUV.y * inParticleUV.w));
    isAlive = inParticleAlive;
}