#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec2 inUV;

layout( push_constant ) uniform constant
{
    mat4 MVP;
} PushConstants;

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec2 uv;

void main() {
    vec4 pos = PushConstants.MVP * vec4(inPos, 1.0);
    gl_Position = pos;
    fragColor = vec4(inUV.xy,pos.z,1.0);
    uv = inUV;
}