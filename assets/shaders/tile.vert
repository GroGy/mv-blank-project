#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 inPos;
layout(location = 1) in vec2 inUV;

layout( push_constant ) uniform constant
{
    mat4 MVP;
} PushConstants;

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec2 uv;

void main() {
    //gl_Position = ubo.proj*ubo.view*ubo.model*vec4(inPos, 1.0);
    gl_Position = PushConstants.MVP * vec4(inPos,0.0, 1.0);
    fragColor = vec4(inUV.xy,0.0,1.0);
    uv = inUV;
}