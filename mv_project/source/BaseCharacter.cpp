//
// Created by Matty on 06.06.2024.
//

#include "BaseCharacter.h"
#include "common/World.h"
#include "input/Input.h"
#include "meta/Helpers.h"
#include "EnemyBase.h"

namespace Project {

    BaseCharacter::BaseCharacter(const MV::ObjectInitializationContext& worldInitContext) : WorldObject(worldInitContext)
    {}

    void BaseCharacter::FixedUpdate(float delta) {
        WorldObject::FixedUpdate(delta);

        processInputs(delta);

        if(MV::IsKeyPressed(MV::Key::F)) {
            MV::WorldObjectSpawnParams params;

            params.m_SpawnLocation = GetPosition() + glm::vec2{10.f};

            params.m_Class = MV::GetClassMeta<EnemyBase>();

            GetOwningWorld()->SpawnWorldObject(params);
        }

    }

    void BaseCharacter::Update(float delta)
    {
        Camera->SetPosition(GetPosition());

        if(GetVelocity().x < -0.1f) {
            Facing = PlayerFacing::Left;
        } else if (GetVelocity().x > 0.1f) {
            Facing = PlayerFacing::Right;
        }

        Sprite->m_Scale.x = (Facing == PlayerFacing::Left ? 1 : -1) * InitialSpriteScale;
    }

    void BaseCharacter::OnStart()
    {
        Camera->Activate();

        InitialSpriteScale = Sprite->m_Scale.x;
    }

    void BaseCharacter::processInputs(float delta)
    {
        if (MV::IsKeyDown(MV::Key::W))
        {
            Collider->ApplyForceToCenter({0, 1.0f*delta*Speed});
        }

        if (MV::IsKeyDown(MV::Key::S))
        {
            Collider->ApplyForceToCenter({0, -1.0f*delta*Speed});
        }

        if (MV::IsKeyDown(MV::Key::A)) {
            Collider->ApplyForceToCenter({-1.0f*delta*Speed,0});
        }

        if (MV::IsKeyDown(MV::Key::D)) {
            Collider->ApplyForceToCenter({1.0f*delta*Speed,0});
        }

        if (MV::IsKeyPressed(MV::Key::Space) && )
        {
            Collider->ApplyForceToCenter({1.0f*delta*Speed,0});
        }
    }
}