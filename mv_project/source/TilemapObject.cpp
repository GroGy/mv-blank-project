//
// Created by Matty on 03.07.2024.
//

#include "TilemapObject.h"

namespace Project
{
    TilemapObject::TilemapObject(const MV::ObjectInitializationContext& worldInitContext)
            : WorldObject(worldInitContext)
    {}
} // Project