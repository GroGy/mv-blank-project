//
// Created by matty on 19.06.2024.
//

#ifndef MV_EDITOR_BOXCOLLIDEROBJECT_H
#define MV_EDITOR_BOXCOLLIDEROBJECT_H


#include <EngineMinimal.h>
#include "2d/BoxColliderProperty.h"
#include "common/WorldObject.h"
#include "2d/SpriteProperty.h"
#include "particles/cpu/ParticleSystemProperty.h"

namespace Project
{
    class [[MV::Class()]] BoxColliderObject : public MV::WorldObject
    {
    public:

        explicit BoxColliderObject(const MV::ObjectInitializationContext& worldInitContext);

    public:

        [[MV::Field(Property,Serialize)]]
        MV::BoxColliderProperty* Collider = nullptr;

        [[MV::Field(Property,Serialize)]]
        MV::SpriteProperty* Sprite = nullptr;
    };
}



#endif //MV_EDITOR_BOXCOLLIDEROBJECT_H
