//
// Created by Matty on 06.06.2024.
//

#ifndef MV_PROJECT_BASECHARACTER_H
#define MV_PROJECT_BASECHARACTER_H

#include <EngineMinimal.h>
#include "2d/BoxColliderProperty.h"
#include "common/WorldObject.h"
#include "2d/SpriteProperty.h"
#include "particles/cpu/ParticleSystemProperty.h"
#include "2d/CameraProperty.h"

namespace Project
{
    enum class PlayerFacing {
        Left,
        Right
    };

    class [[MV::Class()]] BaseCharacter : public MV::WorldObject
    {
    public:

        explicit BaseCharacter(const MV::ObjectInitializationContext& worldInitContext);

    public:

        void FixedUpdate(float delta) override;

        void Update(float delta) override;

        void OnStart() override;

    private: // Internal helper functions

        void processInputs(float delta);

    public:
        [[MV::Field(Property,Serialize)]]
        MV::SpriteProperty* Sprite = nullptr;

        [[MV::Field(Property,Serialize)]]
        MV::BoxColliderProperty* Collider = nullptr;

        [[MV::Field(Property,Serialize)]]
        MV::ParticleSystemProperty* IdleParticles = nullptr;

        [[MV::Field(Property,Serialize)]]
        MV::ParticleSystemProperty* BoostParticles = nullptr;

        [[MV::Field(Property,Serialize)]]
        MV::CameraProperty* Camera = nullptr;

    public:

        [[MV::Field(Serialize)]]
        float Speed = 0.0;

        [[MV::Field(Serialize)]]
        float JumpCD = 0.0;

        [[MV::Field(Serialize)]]
        uint32_t BoostParticleCount = 10;

        [[MV::Field(Serialize)]]
        float RunSpeed = 0.0;

        [[MV::Field()]]
        double NextJumpTime = 0.0;

    private:

        PlayerFacing Facing = PlayerFacing::Right;

        float InitialSpriteScale = 1.0f;

    };
}


#endif //MV_PROJECT_BASECHARACTER_H
