//
// Created by Matty on 08.07.2024.
//

#include "EnemyBase.h"

namespace Project {

    EnemyBase::EnemyBase(const MV::ObjectInitializationContext& worldInitContext) : WorldObject(worldInitContext)
    {

    }

    void EnemyBase::FixedUpdate(float delta)
    {
        WorldObject::FixedUpdate(delta);
    }

    void EnemyBase::Update(float delta)
    {
        WorldObject::Update(delta);
    }

    void EnemyBase::OnStart()
    {
        WorldObject::OnStart();
    }
}