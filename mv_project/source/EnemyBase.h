//
// Created by Matty on 08.07.2024.
//

#ifndef MV_EDITOR_ENEMYBASE_H
#define MV_EDITOR_ENEMYBASE_H

#include "2d/SpriteProperty.h"
#include "common/Types.h"

namespace Project
{
    class BaseCharacter;

    class [[MV::Class()]] EnemyBase : public MV::WorldObject
    {
    public:

        explicit EnemyBase(const MV::ObjectInitializationContext& worldInitContext);

    public:

        void FixedUpdate(float delta) override;

        void Update(float delta) override;

        void OnStart() override;

    public:

        [[MV::Field(Property,Serialize)]]
        MV::SpriteProperty* Sprite = nullptr;

    public:

        MV::rc<BaseCharacter> m_Target;

    };
}


#endif //MV_EDITOR_ENEMYBASE_H
