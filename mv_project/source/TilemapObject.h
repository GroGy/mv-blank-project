//
// Created by Matty on 03.07.2024.
//

#ifndef MV_EDITOR_TILEMAPOBJECT_H
#define MV_EDITOR_TILEMAPOBJECT_H

#include "common/WorldObject.h"
#include "2d/TilemapProperty.h"
#include <EngineMinimal.h>

namespace Project
{
    class [[MV::Class()]] TilemapObject : public MV::WorldObject
    {
    public:

        TilemapObject(const MV::ObjectInitializationContext& worldInitContext);

    public:

        [[MV::Field(Property, Serialize)]]
        MV::TilemapProperty* Tilemap;

    };

}

#endif //MV_EDITOR_TILEMAPOBJECT_H
