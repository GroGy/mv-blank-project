# MV Project
MV project is example project with editor that uses **[MV Engine](https://gitlab.com/GroGy/mv_engine)**.

## Installation
Because of IDE issues, MV Engine is not pulled as submodule, you have to clone it yourself.  
All libraries are included. **(except Vulkan)**

## Setup
```bash
cmake -G Ninja -Bbuild
cd build
ninja install
```



## Features
A lot.  

## Libraries

- [ImGUI](https://github.com/ocornut/imgui)
- [Vulkan](https://www.lunarg.com/vulkan-sdk/)
- [stb](https://github.com/nothings/stb)
- [nlohmann json](https://github.com/nlohmann/json)
- [glfw](https://www.glfw.org/)
- [glslang](https://github.com/KhronosGroup/glslang)
- [shaderc](https://github.com/google/shaderc)
- [spdlog](https://github.com/gabime/spdlog)
- [SPRIV-Tools](https://github.com/KhronosGroup/SPIRV-Tools)
- [vk_mem_alloc](https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator)
- [GLM](https://github.com/g-truc/glm)