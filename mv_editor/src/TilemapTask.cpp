//
// Created by Matty on 08.12.2022.
//

#include "async/TilemapTask.h"
#include "resources/TilemapProducer.h"
#include "resources/ProducerUtils.h"
#include "common/EngineCommon.h"
#include "resources/ProjectProducer.h"
#include "EditorCommon.h"
#include "resources/common/TilemapAssetType.h"

namespace Editor {
    TilemapTask::TilemapTask(TilemapTaskInput input) : m_pInput(MV::move(input)) {

    }

    void TilemapTask::Process() {
        m_pProgress = 0.0f;

        try {
            m_Result.m_Tileset = m_pInput.m_Tileset;
            m_Result.m_Width = m_pInput.m_Width;
            m_Result.m_Height = m_pInput.m_Height;
            m_Result.m_HasCollisionData = false;

            m_Result.m_Data.resize(m_Result.m_Height * m_Result.m_Width);

            for (auto &t: m_Result.m_Data) {
                t.m_AtlasIndex = 0;
            }

            m_pProgress = 0.2f;

            m_pFinalPath = TilemapProducer::CreateTilemapResource(m_Result, m_pInput.m_Path);
            m_pFinalPath = Producer::ProducerUtils::PathAbsoluteToObjectRelative(m_pFinalPath);

            m_pProgress = 1.0f;
            m_pSuccess = true;

        } catch (std::exception &e) {
            (void) e;

            m_pProgress = 0.0f;
            m_pSuccess = false;
            m_pErrorText = e.what();
        }
    }

    EditorTaskFinishResult TilemapTask::Finish() {
        if (!m_pSuccess) {
            m_pDone = true;
            return EditorTaskFinishResult::FAILED;
        }

        auto &proj = MV::GetEngine()->GetProject();

        auto assetId = Producer::ProducerUtils::GenerateUUID();

        auto& asset = proj.m_Assets[assetId];
        asset.m_Path = m_pFinalPath.data();
        asset.m_Type = MV::AssetTypeRegistry::Get().GetRuntimeAssetType<MV::TilemapAssetType>();


        ProjectProducer::SaveProject(proj, GetEditor()->GetLoadedProjectPath());

        m_pDone = true;
        return EditorTaskFinishResult::SUCCESS;
    }
}