//
// Created by Matty on 2021-10-28.
//

#include "../editor/async/EditorTask.h"
#include "../editor/EditorCommon.h"

namespace Editor {
    void EditorTask::Start(MV::rc<EditorTask> self) {
        GetEditor()->queueTask(std::move(self));
    }

    void EditorTask::WaitForResource(uint64_t resource) {
        MV::Assert(false, "UNIMPLEMENTED DUE TO RT REWRITE!");
        /*
        MV::rc<MV::RenderingResource> res{};
        while(!MV::GetRenderer()->GetRenderingResource(resource, res)) {
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }*/
    }

    void EditorTask::WaitForResource(const MV::rc<MV::Resource> & res) {
        while(!res->IsFinished()) {
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
    }
}