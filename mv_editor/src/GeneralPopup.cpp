//
// Created by Matty on 26.06.2024.
//

#include "interface/common/GeneralPopup.h"
#include "imgui/imgui.h"

namespace Editor
{
    bool GeneralPopup::open(MV::string_view title, Editor::PopupCloseCallback&& callback, MV::string_view body,
                            Editor::PopupType type)
    {
        if(m_pOpen) {
            return false;
        }

        m_pCallback = MV::move(callback);
        m_pTitle = title;
        m_pBody = body;
        m_pType = type;
        m_pOpen = true;
        m_pNewlyOpenned = true;

        return true;
    }

    bool GeneralPopup::OpenOk(MV::string_view title, PopupCloseCallback&& callback, MV::string_view body)
    {
        return open(title, MV::move(callback), body, PopupType::Ok);
    }

    bool GeneralPopup::OpenConfirmOrCancel(MV::string_view title, PopupCloseCallback&& callback, MV::string_view body)
    {
        return open(title, MV::move(callback), body, PopupType::ConfirmOrCancel);
    }

    bool GeneralPopup::OpenYesNo(MV::string_view title, PopupCloseCallback&& callback, MV::string_view body)
    {
        return open(title, MV::move(callback), body, PopupType::YesNo);
    }

    void GeneralPopup::Draw()
    {
        if (!m_pOpen)
        {
            return;
        }

        if (m_pNewlyOpenned)
        {
            m_pNewlyOpenned = false;
            ImGui::OpenPopup("##global_popup");
        }

        const ImGuiViewport* viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->GetCenter(), ImGuiCond_Always, ImVec2(0.5f, 0.5f));

        if (ImGui::BeginPopupModal("##global_popup", nullptr, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoSavedSettings))
        {
            ImGui::TextUnformatted(m_pTitle.c_str());

            if (!m_pBody.empty())
            {
                ImGui::TextUnformatted(m_pBody.c_str());
            }

            switch (m_pType)
            {
                case PopupType::Ok:
                    if(ImGui::Button("Ok",{-1,0})) {
                        m_pCallback(true);
                        m_pOpen = false;
                    }
                    break;
                case PopupType::ConfirmOrCancel:
                    if(ImGui::Button("Confirm",{-1,0})) {
                        m_pCallback(true);
                        m_pOpen = false;
                    }
                    if(ImGui::Button("Cancel",{-1,0})) {
                        m_pCallback(false);
                        m_pOpen = false;
                    }
                    break;
                case PopupType::YesNo:
                    if(ImGui::Button("Yes",{-1,0})) {
                        m_pCallback(true);
                        m_pOpen = false;
                    }
                    if(ImGui::Button("No",{-1,0})) {
                        m_pCallback(false);
                        m_pOpen = false;
                    }
                    break;
                case PopupType::CustomModal: {
                    const auto close = m_pCustomModalCallback();
                    if(close) m_pOpen = false;
                    break;
                }
            }

            ImGui::EndPopup();
        }
    }

    bool GeneralPopup::OpenCustomModal(MV::string_view title, CustomModalDrawCallback&& callback)
    {
        if(m_pOpen) {
            return false;
        }

        m_pCustomModalCallback = MV::move(callback);
        m_pTitle = title;
        m_pType = PopupType::CustomModal;
        m_pOpen = true;
        m_pNewlyOpenned = true;

        return true;
    }
}