//
// Created by Matty on 2022-02-18.
//

#include "../editor/resources/AssetWatcher.h"
#include "common/EngineCommon.h"

namespace Editor {
    void AssetWatcher::Start(MV::string_view project_path) {
        return;
        /*
        m_pWatch = MV::make_rc<filewatch::FileWatch<std::filesystem::path>>(project_path,
                                                                            [](const std::filesystem::path& path, const filewatch::Event change_type) {
                                                                                switch (change_type)
                                                                                {
                                                                                    case filewatch::Event::added:
                                                                                        MV::GetLogger()->LogDebug("File was created: ", path.string());
                                                                                        break;
                                                                                    case filewatch::Event::removed:
                                                                                        MV::GetLogger()->LogDebug("File was removed: ", path.string());
                                                                                        break;
                                                                                    case filewatch::Event::modified:
                                                                                        MV::GetLogger()->LogDebug("File was modified: ", path.string());
                                                                                        break;
                                                                                    case filewatch::Event::renamed_old:
                                                                                        MV::GetLogger()->LogDebug("File was renamed from: ", path.string());
                                                                                        break;
                                                                                    case filewatch::Event::renamed_new:
                                                                                        MV::GetLogger()->LogDebug("File was renamed to: ", path.string());
                                                                                        break;
                                                                                };
        });*/
    }

    void AssetWatcher::Stop() {
        m_pWatch.reset();
    }

    void AssetWatcher::Init() {

    }

    void AssetWatcher::Cleanup() {
        m_pTasks.wake_cv();
        m_pWatch.reset();
        m_pTasks.wake_cv();
    }

    void AssetWatcher::Update(float delta) {

    }
}