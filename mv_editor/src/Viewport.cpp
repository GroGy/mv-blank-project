//
// Created by Matty on 2022-02-20.
//

#include "../editor/interface/Viewport.h"
#include "../editor/imgui/imgui_internal.h"
#include "common/EngineCommon.h"
#include "../editor/EditorCommon.h"
#include "../editor/imgui/ImGuizmo.h"
#include "imgui/imgui.h"
#include "interface/common/viewport_gizmo_providers/WorldObjectGizmoProvider.h"
#include "render/vulkan/renderpasses/EditorRenderPass.h"
#include "../editor/imgui/imgui_impl_vulkan.h"
#include "ext/matrix_transform.hpp"
#include "gtc/type_ptr.hpp"
#include "../editor/properties/PropertyGizmoManager.h"
#include "2d/CameraProperty.h"

namespace Editor
{
    static constexpr MV::string_view sc_ViewportWidgetName = "##world_viewport_widget";

    void Viewport::Render()
    {
        MV::AllocatorTagScope allocTag{MV::AllocationType::Editor};
        auto renderer = MV::GetRenderer();
        static bool viewportCollapsed = false;

        if (GetEditor()->m_SelectedWO != m_pSelectedWorldObject)
        {
            m_pSelectedWorldObject = GetEditor()->m_SelectedWO;
            if (m_pSelectedWorldObject == 0)
            {
                m_ViewportWidget.SetGizmoProvider(nullptr);
            } else
            {
                m_ViewportWidget.SetGizmoProvider(MV::make_rc<ActiveWorldWorldObjectGizmoProvider>(
                        MV::GetEngine()->GetWorld().m_WorldObjects[m_pSelectedWorldObject],
                        MV::GetEngine()->GetWorld()));
            }
        }

        // Disable padding, because we dont want space on sides of our viewport texture
        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0.0f, 0.0f});

        const auto viewportFlags =
                ImGuiWindowFlags_NoDecoration |
                ImGuiWindowFlags_NoScrollWithMouse;

        auto& world = MV::GetEngine()->GetWorld();
        if (ImGui::Begin("Viewport", &viewportCollapsed, viewportFlags))
        {
            ImGui::PopStyleVar();
            const bool isFocused = ImGui::IsWindowFocused();
            if (!m_pWasFocused && isFocused)
            {
                onFocused();
            }

            m_pWasFocused = isFocused;

            auto dockId = ImGui::DockSpace(ImGui::GetID("##viewport_window_dockspace"));

            auto dock = ImGui::DockContextFindNodeByID(ImGui::GetCurrentContext(), dockId);

            if (dock->IsLeafNode())
            {
                dock->LocalFlags |=
                        static_cast<ImGuiDockNodeFlags_>(ImGuiDockNodeFlags_NoDocking) | ImGuiDockNodeFlags_NoResize |
                        ImGuiDockNodeFlags_NoTabBar | ImGuiDockNodeFlags_NoSplit;

                ImGui::DockBuilderDockWindow(sc_ViewportWidgetName.data(), dockId);

                ImGui::DockBuilderFinish(dockId);
            }

            if (world.IsValid())
            {
                auto& camera = world.m_CameraOverride ? world.m_CameraOverride : world.m_Camera;

                m_ViewportWidget.Draw(sc_ViewportWidgetName.data(), camera);
            } else
            {
                ImGui::TextUnformatted("No world loaded.");
            }
        } else
        {
            ImGui::PopStyleVar();
        }
        ImGui::End();
    }

    void Viewport::OnResize()
    {
        m_ViewportWidget.OnResize();

        auto* engine = MV::GetEngine();
        if (!engine->GetWorld().IsValid())
        {
            return;
        }

        auto& world = engine->GetWorld();

        auto& cameras = world.m_PropertyManager.GetStorage<MV::CameraProperty>();
        cameras.for_each([](MV::Property* prop)
                         {
                             auto* cam = (MV::CameraProperty*) prop;
                             for (auto&& tex: cam->m_EditorPreviewTextures)
                             {
                                 ImGui_ImplVulkan_RemoveTexture((VkDescriptorSet) tex);
                             }
                             cam->m_EditorPreviewTextures.clear();
                         });
    }

    void Viewport::Init()
    {
    }

    void Viewport::onFocused()
    {
        auto changedAssets = GetEditor()->FetchDirtyAssetsForWorld();

        if (!changedAssets.empty())
        {
            MV::GetEngine()->GetWorld().Reload();
        }
    }
}