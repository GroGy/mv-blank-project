//
// Created by matty on 01.02.2023.
//

#include "interface/common/GradientEditor.h"
#include "imgui/imgui.h"

#include "imgui/imgui_internal.h"

namespace Editor {
    static bool drawColorArrow(uint32_t id, const ImVec4 &color, float xTip, float yTip) {
        const float arrowWidth = 12.5f;
        const float arrowHeight = 15.0f;
        const float tipHeight = 4.0f;
        auto *drawList = ImGui::GetWindowDrawList();

        MV::array<ImVec2, 5> points{
                ImVec2{xTip - arrowWidth / 2.0f, yTip + tipHeight},
                ImVec2{xTip - arrowWidth / 2.0f, yTip + tipHeight + arrowHeight},
                ImVec2{xTip + arrowWidth / 2.0f, yTip + tipHeight + arrowHeight},
                ImVec2{xTip + arrowWidth / 2.0f, yTip + tipHeight},
                ImVec2{xTip, yTip}
        };

        ImRect bb{ImVec2{xTip - arrowWidth / 2.0f, yTip},
                  ImVec2{xTip + arrowWidth / 2.0f, yTip + tipHeight + arrowHeight}};


        const bool hovered = ImGui::ItemHoverable(
                bb,
                ImGui::GetCurrentWindow()->GetID(id));

        drawList->AddConvexPolyFilled(points.data(), points.size(), ImGui::ColorConvertFloat4ToU32(
                hovered ? color * ImVec4{0.8f, 0.8f, 0.8f, 1.0f} : color));

        if (hovered)
            drawList->AddPolyline(points.data(), points.size(), IM_COL32(0, 255, 0, 255), ImDrawFlags_Closed, 3.0f);

        return hovered;
    }

    bool GradientEditor::Draw(MV::string_view windowName) {
        if (!m_pOpen) return false;
        bool dirty = drawColorPicker(windowName);

        bool open = true;
        ImGuiWindowFlags_ flags = ImGuiWindowFlags_NoResize;

        preDrawSetup();
        if (ImGui::Begin(windowName.data(), &open, flags)) {
            dirty |= drawContent();
        }
        ImGui::End();

        if (!open) Close();

        return dirty;
    }

    void GradientEditor::preDrawSetup() {
        ImGui::SetNextWindowSize({Config::sc_GradientEditorWindowSize[0], Config::sc_GradientEditorWindowSize[1]});
    }

    void GradientEditor::Open(MV::Gradient *gradient) {
        m_pGradient = gradient;
        m_pOpen = true;
    }

    void GradientEditor::Close() {
        m_pOpen = false;
        m_pGradient = nullptr;
    }

    bool GradientEditor::drawContent() {
        auto *drawList = ImGui::GetWindowDrawList();
        auto cursor = ImGui::GetCurrentWindow()->DC.CursorPos;
        auto contentSize = ImGui::GetContentRegionAvail();

        const float height = ImGui::GetFrameHeight() * 1.5f;

        contentSize.y = height;

        auto &colors = m_pGradient->GetValues();

        ImGui::ItemAdd(ImRect{cursor, cursor + contentSize}, ImGui::GetCurrentWindow()->GetID("gradient_bar"));

        if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left)) {
            const auto mousePos = ImGui::GetMousePos();
            const float value = (mousePos.x - cursor.x) / contentSize.x;
            m_pGradient->AddColor(value, {0.5, 0.5, 0.5, 1.0f});
        }

        for (uint32_t i = 0; i < colors.size() - 1; i++) {
            const auto &v = colors[i].first;
            const auto &v2 = colors[i + 1].first;
            const auto beginX = cursor.x + (contentSize.x * v);
            const auto endX = cursor.x + (contentSize.x * v2);

            const auto &c = colors[i].second;
            const auto &c2 = colors[i + 1].second;

            const auto cVal = ImGui::ColorConvertFloat4ToU32({c.x, c.y, c.z, c.w});
            const auto c2Val = ImGui::ColorConvertFloat4ToU32({c2.x, c2.y, c2.z, c2.w});

            drawList->AddRectFilledMultiColor({beginX, cursor.y}, {endX, cursor.y + contentSize.y}, cVal, c2Val, c2Val,
                                              cVal);
        }

        ImGui::ItemSize(contentSize);

        bool dirty = false;

        for (int32_t i = 0; i < colors.size(); i++) {
            auto &v = colors[i].first;
            auto &c = colors[i].second;
            const auto tipX = cursor.x + (contentSize.x * v);

            if (drawColorArrow(i, {c.x, c.y, c.z, c.w}, tipX,
                               cursor.y + contentSize.y + ImGui::GetStyle().ItemSpacing.y)) {
                if (ImGui::IsMouseDragging(ImGuiMouseButton_Left, 0.0f) && i != 0 && i != colors.size() - 1) {
                    if (m_pDraggingIndex == -1) {
                        m_pDraggingIndex = i;
                    }
                } else {
                    if (m_pDraggingIndex == i) {
                        m_pDraggingIndex = -1;
                    }
                }

                if (ImGui::IsMouseClicked(ImGuiMouseButton_Right) && i != 0 && i != colors.size() - 1) {
                    colors.erase(colors.begin() + i);
                    m_pGradient->Sort();
                    dirty = true;
                    break;
                }

                if (ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left)) {
                    openColorPicker(&c);
                }
            };

            if (ImGui::IsMouseDragging(ImGuiMouseButton_Left) && m_pDraggingIndex == i) {
                dirty = true;
                const auto mousePos = ImGui::GetMousePos();
                const float value = ImClamp((mousePos.x - cursor.x) / contentSize.x, 0.0f, 1.0f);

                int32_t oldValue = i;
                int32_t newValueCandidate = oldValue;

                //for (int32_t j = 0; j < colors.size() -1 ; j++) {
                //    if (value > colors[j].first && value < colors[j+1].first) {
                //        newValueCandidate = j;
                //        break;
                //    }
                //}

                if (oldValue != newValueCandidate) {
                    m_pDraggingIndex = newValueCandidate;
                }

                colors[i].first = value;

                m_pGradient->Sort();
            }
        }

        drawList->AddRect(cursor, cursor + contentSize, IM_COL32(0, 0, 0, 255));
        return dirty;
    }

    bool GradientEditor::drawColorPicker(MV::string_view windowName) {
        if (!m_pColorPickerOpen) return false;

        bool dirty = false;

        bool open = true;

        ImGui::SetNextWindowSize({400, 400}, ImGuiCond_Once);
        if (ImGui::Begin("Color picker", &open, ImGuiWindowFlags_NoResize)) {
            dirty = ImGui::ColorPicker4(fmt::format("##color_picker_editor{}", windowName.data()).c_str(), &m_pColorPickerColor->x);
            if (ImGui::Button("Confirm", {-1, 0})) {
                closeColorPicker();
            }
        }
        ImGui::End();

        if (!open)
            closeColorPicker();

        return dirty;
    }

    void GradientEditor::openColorPicker(glm::vec4 *color) {
        if (m_pColorPickerOpen) return;

        m_pColorPickerOpen = true;
        m_pColorPickerColor = color;
    }

    void GradientEditor::closeColorPicker() {
        m_pColorPickerOpen = false;
        m_pColorPickerColor = nullptr;
    }
}

bool ImGui::GradientPreviewButton(const char *id, MV::Gradient *gradient, const ImVec2 &size) {
    IM_ASSERT(gradient != nullptr);

    ImVec2 realSize = size;
    if (size.x < 0.0f) realSize.x = ImGui::GetContentRegionAvail().x;
    if (size.y < 0.0f) realSize.y = ImGui::GetContentRegionAvail().y;

    if (realSize.y == 0.0f) realSize.y = ImGui::GetFrameHeight();
    if (realSize.x == 0.0f) realSize.x = ImGui::GetFrameHeight() * 4;

    auto cursor = ImGui::GetCurrentWindow()->DC.CursorPos;
    auto *drawList = ImGui::GetWindowDrawList();

    if (!ImGui::ItemAdd(ImRect(cursor, cursor + realSize), ImGui::GetCurrentWindow()->GetID(id))) {
        return false;
    }

    ImGui::ItemSize(realSize);

    const auto &colors = gradient->GetValues();

    for (uint32_t i = 0; i < colors.size() - 1; i++) {
        const auto &v = colors[i].first;
        const auto &v2 = colors[i + 1].first;
        const auto beginX = cursor.x + (realSize.x * v);
        const auto endX = cursor.x + (realSize.x * v2);

        const auto &c = colors[i].second;
        const auto &c2 = colors[i + 1].second;

        const auto cVal = ImGui::ColorConvertFloat4ToU32({c.x, c.y, c.z, c.w});
        const auto c2Val = ImGui::ColorConvertFloat4ToU32({c2.x, c2.y, c2.z, c2.w});

        drawList->AddRectFilledMultiColor({beginX, cursor.y}, {endX, cursor.y + realSize.y}, cVal, c2Val, c2Val, cVal);
    }

    drawList->AddRect(cursor, cursor + realSize, ImGui::ColorConvertFloat4ToU32({0, 0, 0, 1.0f}));


    return ImGui::IsItemClicked(ImGuiMouseButton_Left);
}