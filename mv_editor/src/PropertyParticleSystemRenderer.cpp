//
// Created by Matty on 21.07.2023.
//

#include "properties/viewport_renderers/PropertyParticleSystemRenderer.h"
#include "common/EngineCommon.h"
#include "ext/matrix_transform.hpp"
#include "imgui/imgui.h"
#include "Engine.h"
#include <xutility>

#define M_PI           3.14159265358979323846

namespace Editor
{
    static constexpr auto sc_BaseVertices = {
            glm::vec2{0.5f, -0.5f},
            glm::vec2{-0.5f, -0.5f},
            glm::vec2{-0.5f, 0.5f},
            glm::vec2{0.5f, 0.5f}
    };

    static constexpr uint32_t sc_CircleResolution = 32;

    void PropertyParticleSystemRenderer::RenderProperty(const glm::vec4& viewport, MV::Property* propertyIn)
    {
        auto* property = (MV::ParticleSystemProperty*)propertyIn;
        const auto engine = MV::GetEngine();

        if(!property->m_WorldObject->GetOwningWorld()) {
            return;
        }

        const auto & world = *property->m_WorldObject->GetOwningWorld();

        if(!world.IsValid()) return;

        const auto drawList = ImGui::GetWindowDrawList();

        const auto wo = property->m_WorldObject;

        const auto woPos = wo->GetPosition();
        const auto woScale = wo->GetScale();
        const auto woRot = wo->GetRotation();

        // Remapping from -1 - 1 to viewport min - max
        const glm::vec2 viewScale = {
                (viewport.z - viewport.x) / 2.0f,
                (viewport.w - viewport.y) / 2.0f
        };

        MV::vector<glm::vec2> positions = sc_BaseVertices;

        glm::mat4 mat = glm::mat4(1.0f);
        mat = glm::translate(mat, {woPos.x + property->m_SpawnerSettings.m_SpawnerOffset.x, woPos.y + property->m_SpawnerSettings.m_SpawnerOffset.y, 0});

        auto viewportCamera = world.m_CameraOverride ? world.m_CameraOverride : world.m_Camera;

        ENSURE_VALID(viewportCamera);

        const auto & cameraData = viewportCamera->GetCameraData();

        switch(property->m_SpawnerSettings.m_SpawnerType) {
            case MV::ParticleSpawnerType::Point: {
                mat = cameraData.m_ProjMatrix * cameraData.m_ViewMatrix * mat;

                glm::vec4 posBase = {0,0, 0.0f, 1.0f};

                posBase = mat * posBase;

                posBase.x = ((posBase.x + 1.0f) * viewScale.x) + viewport.x;
                posBase.y = ((posBase.y + 1.0f) * viewScale.y) + viewport.y;

                drawList->AddCircleFilled({posBase.x, posBase.y}, 1.0f / viewportCamera->GetZoom(), IM_COL32(255,255,0,255));

                break;
            }
            case MV::ParticleSpawnerType::Box: {
//mat = glm::rotate(mat, glm::radians(property->m_Rotation + woRot), {0, 0, 1});
                mat = glm::scale(mat, {property->m_SpawnerSettings.m_BoxSpawnerSize.x * woScale.x,property->m_SpawnerSettings.m_BoxSpawnerSize.y * woScale.y, 1});
                mat = cameraData.m_ProjMatrix * cameraData.m_ViewMatrix * mat;

                for (auto &pos: positions) {
                    glm::vec4 posBase = {pos.x, pos.y, 0.0f, 1.0f};

                    //// Transform based on world object
                    posBase = mat * posBase;

                    // To viewport space
                    posBase.x = ((posBase.x + 1.0f) * viewScale.x) + viewport.x;
                    posBase.y = ((posBase.y + 1.0f) * viewScale.y) + viewport.y;

                    pos = glm::vec2{posBase.x, posBase.y};
                }

                drawList->AddQuad(
                        ImVec2{positions[0].x, positions[0].y},
                        ImVec2{positions[1].x, positions[1].y},
                        ImVec2{positions[2].x, positions[2].y},
                        ImVec2{positions[3].x, positions[3].y},
                        IM_COL32(255,255,0,255),
                        3.0f / viewportCamera->GetZoom()
                );

                break;
            }
            case MV::ParticleSpawnerType::Circle: {
                MV::vector<glm::vec2> circleDrawList;
                struct vector_view {
                    vector_view(MV::vector<glm::vec2> & parentIn, int32_t startIndex, int32_t countIn) {
                        parent = &parentIn;
                        start = startIndex;
                        count = countIn;
                    }
                    MV::vector<glm::vec2> * parent = nullptr;
                    int32_t start = -1;
                    int32_t count = 0;

                    void ApplyMatrix(const glm::mat4 & matrix, const glm::vec4& viewport,const glm::vec2 & viewScale) {
                        for (int32_t i = 0; i < count; i++) {
                            auto & pos = (*parent)[start + i];
                            glm::vec4 posBase = {pos.x, pos.y, 0.0f, 1.0f};

                            //// Transform based on world object
                            posBase = matrix * posBase;

                            // To viewport space
                            posBase.x = ((posBase.x + 1.0f) * viewScale.x) + viewport.x;
                            posBase.y = ((posBase.y + 1.0f) * viewScale.y) + viewport.y;

                            pos = glm::vec2{posBase.x, posBase.y};
                        }
                    }

                };

                vector_view baseCircleView{circleDrawList, 0, sc_CircleResolution + 1};
                vector_view innerCircleView{circleDrawList, sc_CircleResolution + 1, sc_CircleResolution + 1};

                bool showInnerCircle = false;

                if(property->m_SpawnerSettings.m_CircleSpawnerUseRange) {
                    if(property->m_SpawnerSettings.m_SpawnAngle >= 360.0f) {
                        showInnerCircle = true;

                        circleDrawList.resize((sc_CircleResolution + 1) * 2);
                        for(uint32_t i = 0; i < sc_CircleResolution; i++) {
                            circleDrawList[i] = { sin( (2.0 * M_PI / sc_CircleResolution) * i), cos( (2.0 * M_PI / sc_CircleResolution) * i)};
                        }

                        circleDrawList[sc_CircleResolution] = circleDrawList[0]; // Closes the loop


                        uint32_t offset = sc_CircleResolution + 1;

                        for(uint32_t i = 0; i < sc_CircleResolution; i++) {
                            circleDrawList[offset + i] = { sin( (2.0 * M_PI / sc_CircleResolution) * i), cos( (2.0 * M_PI / sc_CircleResolution) * i)};
                        }

                        circleDrawList[offset + sc_CircleResolution] = circleDrawList[offset]; // Closes the loop

                        auto innerCircleMat = mat;
                        innerCircleMat = glm::scale(innerCircleMat, {property->m_SpawnerSettings.m_CircleSpawnerRange.x * woScale.x,property->m_SpawnerSettings.m_CircleSpawnerRange.x * woScale.y, 1});
                        innerCircleMat = cameraData.m_ProjMatrix * cameraData.m_ViewMatrix * innerCircleMat;

                        innerCircleView.ApplyMatrix(innerCircleMat, viewport, viewScale);

                        mat = glm::scale(mat, {property->m_SpawnerSettings.m_CircleSpawnerRange.y * woScale.x,property->m_SpawnerSettings.m_CircleSpawnerRange.y * woScale.y, 1});
                        mat = cameraData.m_ProjMatrix * cameraData.m_ViewMatrix * mat;

                        baseCircleView.ApplyMatrix(mat, viewport, viewScale);
                    } else {
                        float angleOffset = property->m_SpawnerSettings.m_AngleOffset;
                        float angleSize = property->m_SpawnerSettings.m_SpawnAngle;

                        const int32_t totalVertCount = (sc_CircleResolution) * 2 + 1;

                        circleDrawList.resize(totalVertCount);

                        for(uint32_t i = 0; i < sc_CircleResolution; i++) {
                            float angleRad = glm::radians(angleOffset) + ((glm::radians(glm::clamp(angleSize, 0.f , 360.f))) / (sc_CircleResolution - 1)) * i;
                            circleDrawList[i] = {
                                    sin(angleRad ),
                                    cos( angleRad)
                            };
                        }

                        uint32_t offset = sc_CircleResolution;

                        for(uint32_t i = 0; i < sc_CircleResolution; i++) {
                            float angleRad = glm::radians(angleOffset) + ((glm::radians(glm::clamp(angleSize, 0.f , 360.f))) / (sc_CircleResolution - 1)) * (sc_CircleResolution - 1 - i);
                            circleDrawList[offset + i] = {
                                    sin( angleRad),
                                    cos( angleRad)
                            };
                        }

                        circleDrawList.back() = circleDrawList[0];

                        auto innerCircleMat = mat;
                        innerCircleMat = glm::scale(innerCircleMat, {property->m_SpawnerSettings.m_CircleSpawnerRange.x * woScale.x,property->m_SpawnerSettings.m_CircleSpawnerRange.x * woScale.y, 1});
                        innerCircleMat = cameraData.m_ProjMatrix * cameraData.m_ViewMatrix * innerCircleMat;

                        mat = glm::scale(mat, {property->m_SpawnerSettings.m_CircleSpawnerRange.y * woScale.x,property->m_SpawnerSettings.m_CircleSpawnerRange.y * woScale.y, 1});
                        mat = cameraData.m_ProjMatrix * cameraData.m_ViewMatrix * mat;

                        baseCircleView.count = 1;
                        baseCircleView.start = circleDrawList.size() - 1;
                        baseCircleView.ApplyMatrix(mat, viewport, viewScale);

                        baseCircleView.count = sc_CircleResolution;
                        baseCircleView.start = 0;
                        baseCircleView.ApplyMatrix(mat, viewport, viewScale);

                        baseCircleView.count = totalVertCount;

                        innerCircleView.start = sc_CircleResolution;
                        innerCircleView.count = sc_CircleResolution;
                        innerCircleView.ApplyMatrix(innerCircleMat, viewport, viewScale);
                    }
                } else {
                    float angleSize = property->m_SpawnerSettings.m_SpawnAngle;
                    int32_t totalVertCount = (sc_CircleResolution) + 1;

                    if(property->m_SpawnerSettings.m_SpawnAngle < 360.0f)
                    {
                        totalVertCount++;
                    }

                    circleDrawList.resize(totalVertCount);

                    for(uint32_t i = 0; i < sc_CircleResolution; i++) {
                        float angleRad = (glm::radians(glm::clamp(property->m_SpawnerSettings.m_AngleOffset, 0.f , 360.f))) + ((glm::radians(glm::clamp(angleSize, 0.f , 360.f))) / (sc_CircleResolution - 1)) * i;
                        circleDrawList[i] = {
                                sin(angleRad ),
                                cos( angleRad)
                        };
                    }

                    circleDrawList.back() = circleDrawList[0];


                    if(property->m_SpawnerSettings.m_SpawnAngle >= 360.0f)
                    {
                        circleDrawList.resize((sc_CircleResolution + 1));
                        for (uint32_t i = 0; i < sc_CircleResolution; i++)
                        {
                            circleDrawList[i] = {sin((2.0 * M_PI / sc_CircleResolution) * i),
                                                 cos((2.0 * M_PI / sc_CircleResolution) * i)};
                        }

                        circleDrawList[sc_CircleResolution] = circleDrawList[0]; // Closes the loop
                    }

                    mat = glm::scale(mat, {property->m_SpawnerSettings.m_CircleSpawnerRange.y * woScale.x,property->m_SpawnerSettings.m_CircleSpawnerRange.y * woScale.y, 1});
                    mat = cameraData.m_ProjMatrix * cameraData.m_ViewMatrix * mat;

                    baseCircleView.count = totalVertCount;

                    baseCircleView.ApplyMatrix(mat, viewport, viewScale);
                }


                drawList->AddPolyline((ImVec2*)(baseCircleView.parent->data() + baseCircleView.start),baseCircleView.count, IM_COL32(255,255,255,255), ImDrawFlags_None, 3.0f / viewportCamera->GetZoom());

                if(showInnerCircle)
                    drawList->AddPolyline((ImVec2*)(innerCircleView.parent->data() + innerCircleView.start),innerCircleView.count, IM_COL32(255,255,255,255), ImDrawFlags_None, 3.0f / viewportCamera->GetZoom());

                break;
            }
        }

        mat = cameraData.m_ProjMatrix * cameraData.m_ViewMatrix * mat;





    }
}
