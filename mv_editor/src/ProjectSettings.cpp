//
// Created by Matty on 07.12.2022.
//

#include "interface/ProjectSettings.h"
#include "imgui/imgui.h"
#include "util/UtilGui.h"
#include "imgui/imgui_stdlib.h"
#include "EditorCommon.h"

namespace Editor {
    ProjectSettings::ProjectSettings() : Window(Config::sc_ProjectSettingsWindowName.data(), false) {
        m_Flags = ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoDocking;
        m_pOpen = false;
        m_pTabs = {
                Tab{"General", [&]() {
                    drawGeneralSettings();
                }},
                Tab{"Input", [&]() {
                    drawInputSettings();
                }},
                Tab{"Rendering", [&]() {
                    drawRenderingSettings();
                }},
                Tab{"Current world", [&]() {
                    drawCurrentWorldSettings();
                }}
        };
    }

    void ProjectSettings::WindowSetup() {
        ImGui::SetNextWindowSize({512, 512});
    }

    void ProjectSettings::Render() {
        if(!m_pHasLoadedProject) {
            m_pHasLoadedProject = true;
            m_pEditedProject = MV::GetEngine()->GetProject();
        }

        m_pClassSelector.Render();

        ScopedVertical ver{"##project_settings_vertical", ImGui::GetContentRegionAvail()};
        {

            if (ImGui::BeginChild("##project_settings_MainWrapper",{ImGui::GetContentRegionAvail().x,
                        ImGui::GetContentRegionAvail().y - (ImGui::GetTextLineHeightWithSpacing() +
                        ImGui::GetStyle().ItemSpacing.y)})) {

                ScopedHorizontal hor{"##project_settings_horizontal",ImGui::GetContentRegionAvail()};
                // Draw left panel
                if (ImGui::BeginChild("##project_settings",
                                      ImVec2{
                                              ImGui::GetContentRegionAvail().x / 6.0f -
                                              ImGui::GetStyle().ItemSpacing.x,
                                              -1})
                        ) {
                    drawTabSelect();
                }
                ImGui::EndChild();
                ImGui::Spring();

                if (ImGui::BeginChild("##project_settings_right_panel", ImVec2{0, 0}, true)) {
                    if (ImGui::BeginTable("##settings", 2, ImGuiTableFlags_NoSavedSettings)) {
                        ImGui::TableNextRow();
                        m_pTabs[m_pSelectedTab].m_Callback();
                        ImGui::EndTable();
                    }
                }
                ImGui::EndChild();
            }
            ImGui::EndChild();
        }
        ScopedHorizontal buttonHor{"##bottom_button_horizontals"};
        if (ImGui::Button("Cancel")) {
            resetChanges();
            m_pOpen = false;
        }
        ImGui::Spring();
        if (ImGui::Button("Save")) {
            applyChanges();
            GetEditor()->SaveData();
            m_pOpen = false;
        }
        if (ImGui::Button("Apply")) {
            applyChanges();
        }
    }

    void ProjectSettings::PostRender() {

    }

    void ProjectSettings::OnCleanup() {

    }

    void ProjectSettings::OnInit() {

    }

    static bool BeginField(MV::string_view name) {
        if (!ImGui::TableSetColumnIndex(0))
            return false;
        ImGui::TextUnformatted(name.data());
        if (!ImGui::TableSetColumnIndex(1))
            return false;

        return true;
    }

    static void EndField() {
        ImGui::TableNextRow();
    }

    void ProjectSettings::drawGeneralSettings() {
        if (BeginField("Project name")) {
            ScopedDisabled dis{true};
            ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
            ImGui::InputText("##project_name",&m_pEditedProject.m_Name);
            EndField();
        }

        if (BeginField("Game class")) {
            //ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);

            if (ImGui::Button(m_pEditedProject.m_GameClass.IsValid() ? fmt::format("{}", m_pEditedProject.m_GameClass.m_Name).c_str() : "<Unselected>", {-1, 0}))
            {

            }

            //ImGui::InputText("##project_name",&m_pEditedProject.m_Name);
            EndField();
        }
    }

    void ProjectSettings::drawInputSettings() {

    }

    void ProjectSettings::drawRenderingSettings() {

    }

    void ProjectSettings::drawTabSelect() {
        if (ImGui::BeginListBox("##tab_select", {-1, -1})) {

            for (uint32_t i = 0; i < m_pTabs.size(); i++) {
                if (ImGui::Selectable(m_pTabs[i].m_Name.c_str(), i == m_pSelectedTab)) {
                    m_pSelectedTab = i;
                }
            }

            ImGui::EndListBox();
        }
    }

    void ProjectSettings::applyChanges() {
        MV::GetEngine()->GetProject() = m_pEditedProject;
    }

    void ProjectSettings::resetChanges() {
        m_pEditedProject = MV::GetEngine()->GetProject();
    }

    void ProjectSettings::drawCurrentWorldSettings() {
        bool dirty = false;
        if (BeginField("Default camera")) {
            auto & world = MV::GetEngine()->GetWorld();
            ScopedDisabled dis{!world.IsValid()};
            ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
            dirty |= ImGui::InputScalar("##world_default_camera_obj",ImGuiDataType_U32,&world.m_DefaultCameraWorldObject);
            EndField();
        }

        if (BeginField("Gravity")) {
            auto & world = MV::GetEngine()->GetWorld();
            ScopedDisabled dis{!world.IsValid()};
            ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
            dirty |= ImGui::InputScalar("##world_gravity",ImGuiDataType_Float,&world.m_WorldData.m_Gravity,nullptr, nullptr, "%.2f");
            EndField();
        }

        if (BeginField("Default Linear dampening")) {
            auto & world = MV::GetEngine()->GetWorld();
            ScopedDisabled dis{!world.IsValid()};
            ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
            dirty |= ImGui::InputScalar("##world_linear_dampening",ImGuiDataType_Float,&world.m_WorldData.m_DefaultLinearDamping,nullptr, nullptr, "%.2f");
            EndField();
        }

        if (BeginField("Default Angular dampening")) {
            auto & world = MV::GetEngine()->GetWorld();
            ScopedDisabled dis{!world.IsValid()};
            ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
            dirty |= ImGui::InputScalar("##world_angular_dampening",ImGuiDataType_Float,&world.m_WorldData.m_DefaultAngularDamping,nullptr, nullptr, "%.2f");
            EndField();
        }

        if(dirty) {
            ENSURE(MV::GetEngine()->GetWorld().IsValid());
            GetEditor()->m_DirtyAssets.emplace(MV::GetEngine()->GetWorld().m_ID);
        }
    }
}