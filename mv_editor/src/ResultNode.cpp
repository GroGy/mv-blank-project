//
// Created by Matty on 2022-06-19.
//

#include "tools/shader_editor/ResultNode.h"

namespace Editor {
    uint32_t ResultNode::GetId() {
        return ShaderNodeIds::ResultNodeId;
    }

    ResultNode::ResultNode(uint32_t &pinIdCounter) : ShaderNode(pinIdCounter) {
        m_pNodeName = "Shader";

        {
            ShaderNodePin colorIn{};
            colorIn.m_Type = ShaderNodePinType::VEC4;
            colorIn.m_Name = "Color";
            colorIn.m_ConnectedPin = 0;
            colorIn.m_Id = pinIdCounter++;
            m_pInputPins.push_back(eastl::move(colorIn));
        }

        {
            auto pinId = pinIdCounter++;

            ShaderNodePin vertexOffsetPin{};
            vertexOffsetPin.m_Type = ShaderNodePinType::VEC2;
            vertexOffsetPin.m_Name = "Vertex offset";
            vertexOffsetPin.m_Id = pinId;
            vertexOffsetPin.m_Optional = true;

            m_VertexOffsetInputPinIndex = m_pInputPins.size();

            m_pInputPins.push_back(eastl::move(vertexOffsetPin));

            m_VertexOffsetInputPinId = pinId;
        }
    }

    void ResultNode::loadNodeData(const nlohmann::json &data) {
    }

    void ResultNode::saveNodeData(nlohmann::json &data) {

    }
}