//
// Created by Matty on 05.02.2023.
//

#include "util/UtilVerify.h"

#if WIN32
#include <ShlObj_core.h>
#endif

namespace Editor {
    bool UtilVerify::VerifyScriptName(MV::string_view scriptName) {
        // Illegal characters
        static constexpr MV::array<char, 1> illegalCharacters = {
                ' '
        };

        static constexpr MV::array<char, 52> legalInitialCharacters = {
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
                'V', 'W', 'X', 'Y', 'Z',
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
                'v', 'w', 'x', 'y', 'z',
        };

        if (scriptName.empty()) return false;

        bool validInitial = false;

        for (auto&& c: legalInitialCharacters) {
            if (scriptName[0] == c) {
                validInitial = true;
                break;
            }
        }

        if (!validInitial) return false;

        for(auto && sc : scriptName) {
            for (auto&& c: illegalCharacters) {
                if(sc == c) return false;
            }
        }

        return true;
    }

    bool UtilVerify::VerifyFileName(MV::wstring_view name) {
        if(name.empty()) return false;

#if WIN32
        MV::wstring data = name.data();
        auto res = PathCleanupSpec(nullptr, data.data());
        return res != PCS_REMOVEDCHAR;
#endif


        return true;
    }
}