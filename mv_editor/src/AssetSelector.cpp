//
// Created by Matty on 2021-10-31.
//

#include <common/EngineCommon.h>
#include <filesystem>
#include "../editor/interface/common/AssetSelector.h"
#include "../editor/imgui/imgui.h"
#include "Engine.h"

namespace Editor {
    void AssetSelector::SetFilters(const std::initializer_list<MV::AssetType> &types) {
        m_pTypeFilter.clear();

        for (auto &&filter: types) {
            m_pTypeFilter.emplace(filter);
        }
    }

    void AssetSelector::ClearFilters() {
        m_pTypeFilter.clear();
    }

    void AssetSelector::Render() {
        if (!m_pWasOpened) {
            ImGui::OpenPopup(ASSET_SELECTOR_POPUP_ID);
            m_pWasOpened = true;
        }

        if (ImGui::BeginPopup(ASSET_SELECTOR_POPUP_ID, ImGuiWindowFlags_NoMove)) {
            auto &proj = MV::GetEngine()->GetProject();
            auto &assets = proj.m_Assets;
            uint32_t counter = 0;
            ImGui::BeginVertical("##wrapper");
            if (ImGui::BeginListBox("##asset_selector_asset_list")) {
                for (auto &&asset: assets) {
                    const auto & found = m_pTypeFilter.find(asset.second.m_Type);

                    if (!m_pTypeFilter.empty() && found == m_pTypeFilter.end()) {
                        continue;
                    }

                    bool isSelected = asset.first == m_pSelected;

                    ImGui::PushStyleColor(ImGuiCol_Header, IM_COL32(150, 150, 150, 150));
                    if (ImGui::Selectable(fmt::format("{}##_counter_{}",std::filesystem::path(asset.second.m_Path.c_str()).stem().string(), counter++).c_str(),
                                          isSelected)) {
                        m_pSelected = isSelected ? MV::IAssetReference::InvalidAsset().operator MV::UUID() : asset.first;
                    }
                    ImGui::PopStyleColor();

                    if (ImGui::IsItemHovered()) {
                        ImGui::BeginTooltip();
                            ImGui::TextUnformatted(asset.second.m_Path.c_str());
                        ImGui::EndTooltip();

                        if (ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left)) {
                            m_pFinished = true;
                            ImGui::CloseCurrentPopup();
                        }
                    }
                }
                ImGui::EndListBox();
            }

            ImGui::BeginHorizontal("##asset_selector_button_wrapper");
            ImGui::Spring();
            if (ImGui::Button("Confirm")) {
                m_pFinished = true;
                ImGui::CloseCurrentPopup();
            }
            ImGui::EndHorizontal();
            ImGui::EndVertical();
            ImGui::EndPopup();
        }
    }

    void AssetSelector::Clear() {
        m_pWasOpened = true;
        m_pSelected = 0;
        m_pFinished = false;
    }

    void AssetSelector::Open() {
        m_pWasOpened = false;
    }

    void AssetSelector::SetMultiselect(bool multiselect) {
        m_pMultiselect = multiselect;
    }

    bool AssetSelector::DrawAssetSelector(const char *name, bool open, const MV::unordered_set<MV::AssetType> &types,
                                          MV::UUID &res) {

        if (open) {
            ImGui::OpenPopup(name);
        }

        bool finished = false;
        uint32_t counter = 0;
        if (ImGui::BeginPopup(name, ImGuiWindowFlags_NoMove)) {
            auto &proj = MV::GetEngine()->GetProject();
            auto &assets = proj.m_Assets;
            ImGui::BeginVertical("##wrapper");
            if (ImGui::BeginListBox("##asset_selector_asset_list")) {
                for (auto &&asset: assets) {
                    const auto & found = types.find(asset.second.m_Type);

                    if (!types.empty() && found == types.end()) {
                        continue;
                    }


                    bool isSelected = res == asset.first;

                    if (ImGui::Selectable(fmt::format("{}##_counter_{}",std::filesystem::path(asset.second.m_Path.c_str()).stem().string(), counter++).c_str(),
                                          isSelected)) {
                        if (isSelected) {
                            res = 0;
                        } else {
                            res = asset.first;
                            finished = true;
                        }
                    }

                    if (ImGui::IsItemHovered()) {
                        ImGui::BeginTooltip();
                        ImGui::TextUnformatted(asset.second.m_Path.c_str());
                        ImGui::EndTooltip();

                        if (ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left)) {
                            finished = true;
                            ImGui::CloseCurrentPopup();
                        }
                    }
                }
                ImGui::EndListBox();
            }

            ImGui::BeginHorizontal("##asset_selector_button_wrapper");
            ImGui::Spring();
            if (ImGui::Button("Confirm")) {
                finished = true;
                ImGui::CloseCurrentPopup();
            }
            ImGui::EndHorizontal();
            ImGui::EndVertical();
            ImGui::EndPopup();
        }

        return finished;
    }

    bool AssetSelector::DrawAssetSelector(const char* name, bool open, const MV::unordered_set<MV::AssetType>& types,
                                          MV::IAssetReference& res) {
        return DrawAssetSelector(name, open, types, res.operator MV::UUID &());
    }
}