//
// Created by Matty on 2022-02-17.
//

#include "../editor/interface/Toolbar.h"
#include "../editor/imgui/imgui.h"
#include "../editor/imgui/icons.h"

#define IMGUI_DEFINE_MATH_OPERATORS

#include "../editor/imgui/imgui_internal.h"
#include "common/EngineCommon.h"
#include "../editor/util/UtilGui.h"
#include "../editor/EditorCommon.h"

namespace Editor {
    Toolbar::Toolbar() : Window(Config::sc_ToolbarWindowName.data(), false) {
        m_Flags = ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove |
                  ImGuiWindowFlags_NoDecoration;
    }

    void Toolbar::WindowSetup() {
        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0.0f, 0.0f});
    }

    static bool ToolbarBetterButton(MV::string_view icon, const ImVec2 &size) {
        auto *window = ImGui::GetCurrentWindow();
        auto cursor = window->DC.CursorPos;
        ImGui::Dummy(size);

        const bool hovered = ImGui::IsItemHovered(ImGuiHoveredFlags_RectOnly);

        window->DC.CursorPos = cursor;

        if (hovered) {
            ImGui::PushStyleColor(ImGuiCol_ChildBg, ImGui::GetStyleColorVec4(ImGuiCol_ButtonHovered));
        }

        ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[1]);
        auto iconSize = ImGui::CalcTextSize(icon.data());
        ImGui::PopFont();

        float scale = 1.0f;

        if (iconSize.x > size.x || iconSize.y > size.x) {
            float xScale = size.x / iconSize.x;
            float yScale = size.x / iconSize.y;

            scale = xScale > yScale ? yScale : xScale;
        }

        iconSize.x *= scale;
        iconSize.y *= scale;

        const float xOffset = (size.x - iconSize.x) / 2.0f;

        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0, 0});

        if (ImGui::BeginChild(icon.data(), size, false)) {
            window = ImGui::GetCurrentWindow();
            ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[1]);
            float fontSizePrev = ImGui::GetCurrentContext()->FontSize;
            ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, {0, 0});

            ImGui::GetCurrentContext()->FontSize = fontSizePrev * scale;

            window->DC.CursorPos.x += xOffset;

            ImGui::TextUnformatted(icon.data());

            ImGui::GetCurrentContext()->FontSize = fontSizePrev;
            ImGui::PopFont();
            ImGui::PopStyleVar();
        }
        ImGui::EndChild();

        ImGui::PopStyleVar();

        if (hovered) {
            ImGui::PopStyleColor();
        }

        return ImGui::IsItemClicked(ImGuiMouseButton_Left);
    }

    static bool ToolbarButton(MV::string_view icon, float buttonSize) {
        float scale = ImGui2::CalculateIconScaleToFitBox(buttonSize, icon, 1);

        auto &io = ImGui::GetIO();

        const auto prevScale = io.FontGlobalScale;

        io.FontGlobalScale = prevScale * scale * 0.85f;

        ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[1]);
        const auto res = ImGui::Button(icon.data(), {buttonSize, buttonSize});
        ImGui::PopFont();

        io.FontGlobalScale = prevScale;

        return res;
    }

    void Toolbar::Render() {

        MV::AllocatorTagScope allocTag{MV::AllocationType::Editor};
        ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, {0.0f, 0.0f});
        const auto area = ImGui::GetContentRegionAvail();
        ImGui::BeginVertical("top_toolbar_wrap_vert");
        ImGui::Spring();
        ImGui::BeginHorizontal("top_toolbar_wrap", area);
        ImGui::Spring();
        const float buttonSize = area.y * Config::sc_ToolbarButtonSizePercentageOfHeight;
/*

        if(ToolbarButton(MV::GetEngine()->IsGameRunning() ? ICON_FA_STOP : ICON_FA_PLAY, buttonSize)) {

        }

        if(ToolbarButton(ICON_FA_MICROCHIP, buttonSize)) {

        }

*/

        auto editor = GetEditor();
        ScopedDisabled dis{editor->IsGameStarting()};

        if (MV::GetEngine()->IsGameRunning()) {
            if (ToolbarBetterButton(ICON_FA_STOP, {buttonSize, buttonSize})) {
                editor->StopGame();
            }
        } else {
            if (ToolbarBetterButton(ICON_FA_PLAY, {buttonSize, buttonSize})) {
                editor->StartGame();
            }
        }

        ImGui::Spring(0.02f);
        {
            ScopedDisabled dis(!MV::GetEngine()->IsGameRunning());
            bool popColor = false;

            if(MV::GetEngine()->IsGameRunning() && GetEditor()->GetInputMode() == InputMode::Game) {
                popColor = true;
                ImGui::PushStyleColor(ImGuiCol_Text, {0.0,0.5,0.0,1.0});
            }

            if(ToolbarBetterButton(ICON_FA_RULER, {buttonSize, buttonSize})) {
                GetEditor()->SetInputMode(GetEditor()->GetInputMode() == InputMode::Game ? InputMode::Editor : InputMode::Game);
            }

            if(popColor) {
                ImGui::PopStyleColor();
            }
        }

        ImGui::Spring();
        ImGui::EndHorizontal();
        ImGui::Spring();
        ImGui::EndVertical();
        ImGui::PopStyleVar();
        ImGui::PopStyleVar();
    }

    void Toolbar::PostRender() {

    }

    void Toolbar::OnCleanup() {

    }

    void Toolbar::OnInit() {

    }

    void Toolbar::NoRender() {

        ImGui::PopStyleVar();

    }
}