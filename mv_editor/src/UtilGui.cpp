//
// Created by Matty on 2022-01-21.
//

#include <common/EngineCommon.h>
#include <filesystem>
#include "../editor/util/UtilGui.h"
#include "../editor/imgui/imgui_stdlib.h"
#include "../editor/imgui/imgui_internal.h"
#include "Engine.h"
#include "interface/common/PropertyDragAndDropPayload.h"

namespace Editor {
    ScopedHorizontal::ScopedHorizontal(MV::string_view name, ImVec2 size) {
        ImGui::BeginHorizontal(name.data(), size);
    }

    ScopedHorizontal::~ScopedHorizontal() {
        ImGui::EndHorizontal();
    }

    ScopedVertical::ScopedVertical(MV::string_view name, ImVec2 size) {
        ImGui::BeginVertical(name.data(), size);
    }

    ScopedVertical::~ScopedVertical() {
        ImGui::EndVertical();
    }

    ScopedCenter::ScopedCenter(MV::string_view name) : m_pHor(MV::string(name) + "##_hor", ImGui::GetContentRegionAvail()),
                                                          m_pVert(MV::string(name) + "##_vert", ImGui::GetContentRegionAvail()) {
    }

    ScopedDisabled::ScopedDisabled(bool enabled) : m_pEnabled(enabled) {
        if (m_pEnabled) {
            ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
        }
    }

    ScopedDisabled::~ScopedDisabled() {
        if (m_pEnabled) {
            ImGui::PopItemFlag();
            ImGui::PopStyleVar();
        }
    }

    float Convert_SRGB_ToLinear(float thesRGBValue)
    {
        return thesRGBValue <= 0.04045f
               ? thesRGBValue / 12.92f
               : powf((thesRGBValue + 0.055f) / 1.055f, 2.2f);
    }

    ImVec4 UtilGui::FixImGuiColorSpace(const ImVec4 &color) {
        ImVec4 res;

        res.x = Convert_SRGB_ToLinear(color.x);
        res.y = Convert_SRGB_ToLinear(color.y);
        res.z = Convert_SRGB_ToLinear(color.z);
        res.w = color.w;

        return res;
    }
}

namespace ImGui2 {
    void WideInputText(const char *label, MV::string *str, ImGuiInputTextFlags flags, ImGuiInputTextCallback callback,
                       void *user_data) {
        ImGui::PushItemWidth(ImGui::GetContentRegionAvail().x);
        ImGui::InputText(label, str, flags, callback);
    }

    bool AssetSelectButton(const uint64_t &asset, const ImVec2 &size /*= {0,0}*/) {
        auto &assets = MV::GetEngine()->GetProject().m_Assets;

        const auto foundAsset = assets.find(asset);

        MV::string filename = foundAsset == assets.end() ? "<missing asset>" : std::filesystem::path(foundAsset->second.m_Path.data()).stem().string().c_str();
        MV::string pathStr = foundAsset == assets.end() ? "<missing path>" : (foundAsset->second.m_Path);

        const auto res = ImGui::Button(
                asset == 0 ?
                "<not selected>" :
                filename.c_str()
                , size);
        if(ImGui::IsItemHovered() && asset != 0) {
            ImGui::BeginTooltip();
                ImGui::TextUnformatted(pathStr.c_str());
            ImGui::EndTooltip();
        }

        return res;
    }

    bool
    AssetBrowserButton(MV::string_view label, const ImVec2 &size, MV::string_view icon, MV::string_view item_name, bool invalidAsset, bool selected, bool is_directory) {
        auto * window = ImGui::GetCurrentWindow();
        auto cursor = window->DC.CursorPos;
        ImGui::Dummy(size);

        const bool hovered = ImGui::IsItemHovered(ImGuiHoveredFlags_RectOnly);

        window->DC.CursorPos = cursor;

        if(hovered || selected) {
            ImGui::PushStyleColor(ImGuiCol_ChildBg, ImGui::GetStyleColorVec4(ImGuiCol_ButtonHovered));
        }

        ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[1]);
        auto iconSize = ImGui::CalcTextSize(icon.data());
        ImGui::PopFont();

        float scale = 1.0f;

        if(iconSize.x > size.x || iconSize.y > size.x) {
            float xScale = size.x / iconSize.x;
            float yScale = size.x / iconSize.y;

            scale = xScale > yScale ? yScale : xScale;
        }

        iconSize.x *= scale;
        iconSize.y *= scale;

        const float xOffset = (size.x - iconSize.x) / 2.0f;

        const auto itemNameSize = ImGui::CalcTextSize(item_name.data());

        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0,0});

        if(ImGui::BeginChild(label.data(),size,false)) {
            window = ImGui::GetCurrentWindow();

            ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[1]);
            float fontSizePrev = ImGui::GetCurrentContext()->FontSize;
            ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, {0, 0});

            ImGui::GetCurrentContext()->FontSize = fontSizePrev * scale;

            window->DC.CursorPos.x += xOffset;

            if(is_directory) ImGui::PushStyleColor(ImGuiCol_Text, {1.f,0.933f,0.643f,1});
            ImGui::TextUnformatted(icon.data());
            if(is_directory) ImGui::PopStyleColor();

            ImGui::GetCurrentContext()->FontSize = fontSizePrev;
            ImGui::PopFont();

            window->DC.CursorPos.y += (size.y - (itemNameSize.y + iconSize.y));
            if(invalidAsset) ImGui::PushStyleColor(ImGuiCol_Text, {1.0f,0,0,1});
            ImGui::TextUnformatted(item_name.data());
            if(invalidAsset) ImGui::PopStyleColor();
            ImGui::PopStyleVar();
        }
        ImGui::EndChild();

        ImGui::PopStyleVar();

        if(hovered || selected) {
            ImGui::PopStyleColor();
        }

        return ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left);
    }

    float CalculateIconScaleToFitBox(float boxSideSize, MV::string_view icon, uint32_t customFontIndex) {
        if(customFontIndex > 0) ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[customFontIndex]);
        auto iconSize = ImGui::CalcTextSize(icon.data());
        if(customFontIndex > 0) ImGui::PopFont();

        float scale = 1.0f;
        if(iconSize.x > boxSideSize || iconSize.y > boxSideSize) {
            float xScale = boxSideSize / iconSize.x;
            float yScale = boxSideSize / iconSize.y;

            scale = xScale > yScale ? yScale : xScale;
        }
        return scale;
    }

    void LabeledSeparator(MV::string_view label)
    {
        const auto* window = ImGui::GetCurrentWindow();
        const auto cursor = window->DC.CursorPos;
        auto* drawList = ImGui::GetWindowDrawList();

        const float textOffset = 10.0f;

        const float fontSize = ImGui::GetFontSize() / 1.2f;

        const ImVec2 textSize = ImGui::GetFont()->CalcTextSizeA(fontSize, FLT_MAX, -1.0f, label.data());

        const float textWidth = textSize.x;
        const float textWidthWithSpacing = textWidth + ImGui::GetStyle().ItemSpacing.x * 2.0f;

        const float x1 = cursor.x - ImGui::GetStyle().WindowPadding.x;
        const float x2 = x1 + ImGui::GetContentRegionAvail().x + 2 * ImGui::GetStyle().WindowPadding.x;

        const ImRect bb(ImVec2(x1, window->DC.CursorPos.y), ImVec2(x2, window->DC.CursorPos.y + 1.0f));
        ImGui::ItemSize(ImVec2(0.0f, 1.0f + ImGui::GetStyle().ItemSpacing.y));
        const bool item_visible = ImGui::ItemAdd(bb, 0);
        if (item_visible)
        {
            drawList->AddLine(bb.Min, ImVec2(bb.Min.x + textOffset, bb.Min.y), ImGui::GetColorU32(ImGuiCol_Separator));

            drawList->AddText(nullptr, fontSize,ImVec2(bb.Min.x + textOffset + ImGui::GetStyle().ItemSpacing.x, bb.Min.y - textSize.y / 2.0f), ImGui::GetColorU32(ImGuiCol_Text), label.data());

            drawList->AddLine(ImVec2(bb.Min.x + textOffset + textWidthWithSpacing, bb.Min.y), ImVec2(bb.Max.x, bb.Min.y), ImGui::GetColorU32(ImGuiCol_Separator));
        }
    }

    bool PropertyRefButton(MV::string_view label,const ImVec2 &size, MV::IPropertyReference& propRef)
    {
        auto * window = ImGui::GetCurrentWindow();
        auto cursor = window->DC.CursorPos;
        ImGui::Dummy(size);

        window->DrawList->AddRectFilled(cursor, cursor + size, ImGui::ColorConvertFloat4ToU32(ImGui::GetStyle().Colors[ImGuiCol_FrameBg]));

        if(propRef.m_Name.empty()) {
            window->DrawList->AddText(cursor, ImGui::ColorConvertFloat4ToU32(ImGui::GetStyle().Colors[ImGuiCol_Text]),label.data());
        } else {
            window->DrawList->AddText(cursor, ImGui::ColorConvertFloat4ToU32(ImGui::GetStyle().Colors[ImGuiCol_Text]),propRef.m_Name.c_str());
        }

        const bool hovered = ImGui::IsItemHovered(ImGuiHoveredFlags_RectOnly);

        if(ImGui::BeginDragDropTarget()) {
            auto* payload = ImGui::GetDragDropPayload();
            if(payload && payload->IsDataType("MV_PROPERTY_REF")) {
                auto* pld = (Editor::PropertyDragAndDropPayload*)payload->Data;

                if(pld->m_PropertyId == propRef.ID()) {
                    auto cursorBef = window->DC.CursorPos;

                    window->DC.CursorPos = cursor;

                    window->DrawList->AddRect(cursor, cursor + size, ImGui::ColorConvertFloat4ToU32(ImGui::GetStyle().Colors[ImGuiCol_FrameBgHovered]), 0, 0, 3.f);

                    window->DC.CursorPos = cursorBef;

                    ImGuiDragDropFlags target_flags = 0;
                    target_flags |= ImGuiDragDropFlags_AcceptNoDrawDefaultRect; // Don't display the yellow rectangle
                    if(auto realPayload = ImGui::AcceptDragDropPayload("MV_PROPERTY_REF",target_flags)) {
                        propRef.m_Name = pld->m_Property->m_Field.m_Name;
                        return true;
                    }
                }
            }

            ImGui::EndDragDropTarget();
        }



        return false;
    }
}
