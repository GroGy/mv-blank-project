//
// Created by Matty on 07.12.2022.
//

#include "interface/AssemblyInspector.h"
#include "imgui/imgui.h"
#include "util/UtilGui.h"
#include "common/EngineCommon.h"

#define FTS_FUZZY_MATCH_IMPLEMENTATION

#include "Engine.h"
#include "util/FuzzyMatch.h"
#include "imgui/imgui_stdlib.h"

namespace Editor {
    AssemblyInspector::AssemblyInspector() : Window(Config::sc_AssemblyInspectorWindowName.data()) {
        m_Flags = ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoDocking;
        m_pOpen = false;
    }

    void AssemblyInspector::WindowSetup() {
        ImGui::SetNextWindowSize({1024,302});
    }

    void AssemblyInspector::Render() {
        MV::AllocatorTagScope allocTag{MV::AllocationType::Editor};
        ScopedHorizontal hor{"##assembly_horizontal", ImGui::GetContentRegionAvail()};

        if (m_pDataVersion == 0) {
            drawMissingData();
            return;
        }

        // Draw left panel
        if (ImGui::BeginChild("##assembly_left_panel",
                              ImVec2{
                                      ImGui::GetContentRegionAvail().x / 3.0f - ImGui::GetStyle().ItemSpacing.x,
                                      -1})
                ) {
            drawSelectPanel();

        }
        ImGui::EndChild();
        ImGui::Spring();

        ImGui::PushStyleColor(ImGuiCol_ChildBg, {0, 0, 0, 0.5f});
        ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 3.0f);
        if (ImGui::BeginChild("##assembly_right_panel")) {

            drawInfoPanel();

        }
        ImGui::EndChild();
        ImGui::PopStyleColor();
        ImGui::PopStyleVar();
    }

    void AssemblyInspector::PostRender() {

    }

    void AssemblyInspector::OnCleanup() {

    }

    void AssemblyInspector::OnInit() {

    }

    void AssemblyInspector::drawSelectPanel() {
        ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
        if(ImGui::InputTextWithHint("##assembly_search","Search",&m_pSearchInput/*,ImGuiInputTextFlags_EnterReturnsTrue*/)) {
            updateSearch();
        }

        if (ImGui::BeginListBox("##class_selection", {-1, -1})) {
            for (int64_t i = 0; i < m_pFilteredEntries.size(); i++) {
                auto &entry = m_pFilteredEntries[i];
                ImGui::PushStyleColor(ImGuiCol_Header, IM_COL32(150, 150, 150, 150));
                if (entry.m_Type == SearchPassedEntry::Enum) {
                    ImGui::PushStyleColor(ImGuiCol_Text, IM_COL32(248, 48, 20, 255));
                }
                if (ImGui::Selectable(fmt::format("{}##_{}",entry.m_Name, entry.m_Index).c_str(), i == m_pSelectedIndex)) {
                    m_pSelectedIndex = i == m_pSelectedIndex ? -1 : i;
                }
                if (entry.m_Type == SearchPassedEntry::Enum) {
                    ImGui::PopStyleColor();
                }
                ImGui::PopStyleColor();
            }

            ImGui::EndListBox();
        }
    }

    void AssemblyInspector::drawInfoPanel() {
        if (m_pSelectedIndex == -1)
            return;

        const auto &selectedItem = m_pFilteredEntries[m_pSelectedIndex];

        if (ImGui::BeginTable("##assembly_entry_info", 3, ImGuiTableFlags_NoSavedSettings | ImGuiTableFlags_Resizable, {-1, -1})) {
            ImGui::TableNextRow();
            switch (selectedItem.m_Type) {
                case SearchPassedEntry::Enum:
                    drawEnumInfo(selectedItem.m_Index);
                    break;
                case SearchPassedEntry::Class:
                    drawClassInfo(selectedItem.m_Index);
                    break;
            }
            ImGui::EndTable();
        }
    }

    void AssemblyInspector::drawMissingData() {
        ImGui::TextUnformatted("Assembly data not loaded.");
        if (ImGui::Button("Load assembly data")) {
            updateData();
        }
    }

    void AssemblyInspector::updateData() {
        /*
        auto &scriptManager = MV::GetEngine()->m_ScriptManager;

        m_pData = scriptManager.GetAssemblyInfo();
        m_pDataVersion = scriptManager.GetDomainVersion();
*/
        updateSearch();
    }

    void AssemblyInspector::updateSearch() {
        m_pSelectedIndex = -1;
        m_pFilteredEntries.clear();
/*
        for (uint32_t i = 0; i < m_pData.m_Classes.size(); ++i) {
            auto &klass = m_pData.m_Classes[i];

            if (!fts::fuzzy_match_simple(m_pSearchInput.c_str(), klass.m_Name.c_str())) {
                continue;
            }

            SearchPassedEntry v;
            v.m_Name = klass.m_Name;
            v.m_Index = i;
            v.m_Type = SearchPassedEntry::Class;
            v.m_ClassToken = klass.m_Token;
            m_pFilteredEntries.push_back(MV::move(v));
        }

        for (uint32_t i = 0; i < m_pData.m_Enums.size(); ++i) {
            auto &en = m_pData.m_Enums[i];
            if (!fts::fuzzy_match_simple(m_pSearchInput.c_str(), en.m_Name.c_str())) {
                continue;
            }
            SearchPassedEntry v;
            v.m_Name = en.m_Name;
            v.m_Index = i;
            v.m_Type = SearchPassedEntry::Enum;
            m_pFilteredEntries.push_back(MV::move(v));
        }
*/
        if(!m_pFilteredEntries.empty())
            m_pSelectedIndex = 0;
    }

    bool AssemblyInspector::hasAnyData() const {
        return false;
        /*
        return m_pData.m_Classes.empty() && m_pData.m_Enums.empty();*/
    }

    static void drawPrimitiveField(MV::string_view name, MV::string_view data) {
        if (!ImGui::TableSetColumnIndex(0))
            return;
        ImGui::TextUnformatted(name.data());
        if (!ImGui::TableSetColumnIndex(2))
            return;
        ImGui::TextUnformatted(data.data());
        ImGui::TableNextRow();
    }

    static bool drawRefField(MV::string_view name, MV::string_view classRefName) {
        bool res = false;
        if (!ImGui::TableSetColumnIndex(0))
            return false;
        ImGui::TextUnformatted(name.data());
        if (!ImGui::TableSetColumnIndex(2))
            return false;
        ImGui::PushStyleColor(ImGuiCol_Text, IM_COL32(210, 58, 253, 255));
        ImGui::TextUnformatted(classRefName.data());
        ImGui::PopStyleColor();
        if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left))
            res = true;
        ImGui::TableNextRow();
        return res;
    }
/*
    static bool drawFieldInfo(const MV::AssemblyFieldEntry &data) {
        bool res = false;
        if (!ImGui::TableSetColumnIndex(0))
            return res;
        ImGui::TextUnformatted("Field");
        if (!ImGui::TableSetColumnIndex(1))
            return res;

        if (data.m_Type.m_Valid)
            ImGui::PushStyleColor(ImGuiCol_Text, IM_COL32(210, 58, 253, 255));

        ImGui::TextUnformatted(data.m_Type.m_Name.c_str());

        if (data.m_Type.m_Valid) {
            ImGui::PopStyleColor();
            res = ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left);
        }

        if (!ImGui::TableSetColumnIndex(2))
            return res;

        ImGui::TextUnformatted(data.m_Name.c_str());

        ImGui::TableNextRow();

        return res;
    }*/
/*
    static uint32_t drawFunctionDescription(MV::string_view className, const MV::AssemblyFunctionEntry &data) {
        uint32_t res = 0;

        MV::vector<MV::AssemblyClassRef> vals;

        const bool isCtor = data.m_Name == ".ctor";

        vals.reserve(data.m_Params.size()+ (isCtor ? 0 : 1));

        if(!isCtor)
            vals.emplace_back(data.m_ReturnType);

        for(auto && param : data.m_Params)
            vals.emplace_back(param.m_Type);

        ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2{0,0});
        ImGui::BeginGroup();

        if(isCtor) {
            ImGui::TextUnformatted(className.data());
            ImGui::SameLine();
            ImGui::TextUnformatted("(");
            ImGui::SameLine();
        }

        for (uint32_t i = 0; i < vals.size(); i++) {
            if(!isCtor && i == 1) {
                ImGui::TextUnformatted("(");
                ImGui::SameLine();
            }

            const auto & v = vals[i];

            if (v.m_Valid)
                ImGui::PushStyleColor(ImGuiCol_Text, IM_COL32(210, 58, 253, 255));

            if(i != vals.size() - 1 && (i != 0 || isCtor))
                ImGui::Text("%s,",v.m_Name.c_str());
            else
                ImGui::TextUnformatted(v.m_Name.c_str());
            ImGui::SameLine();

            if (v.m_Valid) {
                ImGui::PopStyleColor();
                if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left)) {
                    res = v.m_Token;
                }
            }
        }

        if(vals.size() == 1 && !isCtor)
            ImGui::TextUnformatted("()");
        else
            ImGui::TextUnformatted(")");

        ImGui::EndGroup();

        if(ImGui::IsItemHovered())
        {
            ImGui::BeginTooltip();

            if(isCtor) {
                ImGui::TextUnformatted(className.data());
                ImGui::SameLine();
                ImGui::TextUnformatted("(");
                ImGui::SameLine();
            }

            for (uint32_t i = 0; i < vals.size(); i++) {
                if(!isCtor && i == 1) {
                    ImGui::TextUnformatted("(");
                    ImGui::SameLine();
                }

                const auto & v = vals[i];

                if (v.m_Valid)
                    ImGui::PushStyleColor(ImGuiCol_Text, IM_COL32(210, 58, 253, 255));

                if(i != vals.size() - 1 && (i != 0 || isCtor))
                    ImGui::Text("%s %s,",v.m_Name.c_str(), data.m_Params[isCtor ? i : i-1].m_Name.c_str());
                else if(i == vals.size() - 1 && (i != 0 || isCtor)) {
                    ImGui::Text("%s %s",v.m_Name.c_str(), data.m_Params[isCtor ? i : i-1].m_Name.c_str());
                } else {
                    ImGui::TextUnformatted(v.m_Name.c_str());
                }
                ImGui::SameLine();

                if (v.m_Valid) {
                    ImGui::PopStyleColor();
                }
            }

            if(vals.size() == 1 && !isCtor)
                ImGui::TextUnformatted("()");
            else
                ImGui::TextUnformatted(")");

            ImGui::EndTooltip();
        }

        ImGui::PopStyleVar();



        return res;
    }

    static uint32_t drawFunctionInfo(MV::string_view className, const MV::AssemblyFunctionEntry &data) {
        uint32_t res = 0;
        bool isCtor = data.m_Name == ".ctor";

        if (!ImGui::TableSetColumnIndex(0))
            return res;

        ImGui::TextUnformatted(isCtor ? "Constructor" : "Function");


        if (!ImGui::TableSetColumnIndex(1))
            return res;

        res = drawFunctionDescription(className, data);

        if (!ImGui::TableSetColumnIndex(2))
            return res;

        ImGui::TextUnformatted(isCtor ? className.data() : data.m_Name.c_str());

        ImGui::TableNextRow();

        return res;
    }
*/
    void AssemblyInspector::drawEnumInfo(uint64_t index) {
       // const auto &en = m_pData.m_Enums[index];
       // drawPrimitiveField("Name", en.m_Name);
    }

    void AssemblyInspector::drawClassInfo(uint64_t index) {
        //const auto &klass = m_pData.m_Classes[index];
        //drawPrimitiveField("Namespace", klass.m_Namespace);
        //drawPrimitiveField("Name", klass.m_Name);
        //if (klass.m_Parent.m_Valid && drawRefField("Parent", klass.m_Parent.m_Name))
        //    gotoClass(klass.m_Parent.m_Token);
//
        //for (auto &&f: klass.m_Fields) {
        //    if (drawFieldInfo(f))
        //        gotoClass(f.m_Type.m_Token);
        //}
//
        //for (auto &&f: klass.m_Functions) {
        //    uint32_t clickedToken = drawFunctionInfo(klass.m_Name, f);
        //    if (clickedToken != 0)
        //        gotoClass(clickedToken);
        //}
    }

    void AssemblyInspector::drawMemberInfoPanel() {

    }

    void AssemblyInspector::gotoClass(uint32_t token) {
        for (uint32_t i = 0; i < m_pFilteredEntries.size(); ++i) {
            if (m_pFilteredEntries[i].m_ClassToken == token) {
                m_pSelectedIndex = i;
                return;
            }
        }

        m_pSearchInput.clear();
        updateSearch();

        for (uint32_t i = 0; i < m_pFilteredEntries.size(); ++i) {
            if (m_pFilteredEntries[i].m_ClassToken == token) {
                m_pSelectedIndex = i;
                return;
            }
        }

        MV::GetLogger()->LogError("Class with token not found in assembly inspector. Token: {}", token);
        m_pSelectedIndex = -1;
    }
}
