//
// Created by Matty on 09.04.2023.
//

#include "interface/common/viewport_gizmo_providers/WorldObjectGizmoProvider.h"
#include "imgui/ImGuizmo.h"
#include "gtc/type_ptr.hpp"
#include "EditorCommon.h"

namespace Editor
{
    void WorldObjectGizmoProvider::SetPosition(const glm::vec2& position)
    {
        m_pWorldObject->SetPosition(position);
    }

    void WorldObjectGizmoProvider::SetRotation(float rotation)
    {
        m_pWorldObject->SetRotation(rotation);
    }

    void WorldObjectGizmoProvider::SetScale(const glm::vec2& scale)
    {
        m_pWorldObject->SetScale(scale);
    }

    void WorldObjectGizmoProvider::SetTransform(const glm::mat4& transform)
    {
        glm::vec3 pos{0, 0, 0};
        glm::vec3 rot{0, 0, 0};
        glm::vec3 scale{0, 0, 0};

        ImGuizmo::DecomposeMatrixToComponents(glm::value_ptr(transform), glm::value_ptr(pos),
                                              glm::value_ptr(rot), glm::value_ptr(scale));

        SetScale({scale.x, scale.y});
        SetRotation(rot.z);
        SetPosition({pos.x, pos.y});

        if (!MV::GetEngine()->IsGameRunning() || m_pModifyDataWhenRunning)
        {
            auto& world = m_pWorld;
            auto& worldData = world.m_WorldData;
            auto& worldObjects = worldData.m_WorldObjects;
            //auto& woData = worldObjects[m_pWorldObject->m_WorldID];

            //NOTE Removed code that modified woData

            OnDataChanged();
        }
    }

    glm::vec2 WorldObjectGizmoProvider::GetPosition() const
    {
        return m_pWorldObject->GetPosition();
    }

    float WorldObjectGizmoProvider::GetRotation() const
    {
        return m_pWorldObject->GetRotation();
    }

    glm::vec2 WorldObjectGizmoProvider::GetScale() const
    {
        return m_pWorldObject->GetScale();
    }

    WorldObjectGizmoProvider::WorldObjectGizmoProvider(const MV::rc<MV::WorldObject>& wo, MV::World& world,
                                                       bool modifyDataWhenRunning) : m_pWorldObject(wo),
                                                                                     m_pWorld(world),
                                                                                     m_pModifyDataWhenRunning(
                                                                                             modifyDataWhenRunning)
    {

    }

    ActiveWorldWorldObjectGizmoProvider::ActiveWorldWorldObjectGizmoProvider(const MV::rc<MV::WorldObject>& wo,
                                                                             MV::World& world, bool saveOnChange)
            : WorldObjectGizmoProvider(wo, world, false), m_pSaveOnChange(saveOnChange)
    {}

    void ActiveWorldWorldObjectGizmoProvider::OnDataChanged()
    {
        if(m_pSaveOnChange)
            GetEditor()->m_DirtyAssets.emplace(m_pWorld.m_ID);
    }
}