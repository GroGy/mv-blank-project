//
// Created by Matty on 2021-10-23.
//

#include <common/EngineCommon.h>
#include <render/vulkan/renderpasses/EditorRenderPass.h>
#include <render/vulkan/VulkanRenderer.h>
#include "EditorUI.h"
#include "allocator/Alloc.h"
#include "allocator/AllocatorProvider.h"
#include "common/OSDragAndDropEvent.h"
#include "common/OSDragAndDropEvent.h"
#include "cvar/CVars.h"
#include "imgui/imgui_impl_vulkan.h"
#include "imgui/imgui_impl_glfw.h"
#include "interface/Performance.h"
#include "resources/ProjectProducer.h"
#include "imgui/imgui_stdlib.h"
#include "interface/LogWindow.h"
#include "EditorCommon.h"
#include "interface/asset_browser/AssetBrowser.h"
#include "interface/ProjectSettings.h"
#include "imgui/imgui_neo_sequencer.h"
#include "util/UtilGui.h"
#include "imgui/icons.h"
#include "util/UtilDocking.h"
#include "profiler/Profiler.h"
#include "precompiler/PropertyFieldRendererRegistrator.h"
#include "tools/scriptable_config_editor/ScriptableConfigEditor.h"
#include "precompiler/PropertyGizmoRegistrator.h"
#include "resources/common/AssetDataProvider.h"
#include "resources/common/asset_providers/UnpackagedAssetDataProvider.h"
#include <fstream>

#include "test/TestSerializationObject.h"
#include "serialization/binary/BinaryArchiveProcessor.h"
#include "source/BaseCharacter.h"
#include "common/OSDragAndDropEvent.h"
#include "cvar/CVarManager.h"

namespace Editor
{
    namespace CVar
    {
        static bool s_TrackImGuiAllocations = false;
    }

    static MV::CVarRef CVar_Editor_TrackImGuiAllocations{
            "mv.editor.track_imgui_allocations",
            "Enables tracking of imgui allocations for memory tracking",
            CVar::s_TrackImGuiAllocations
    };

    void EditorUI::OnRender()
    {
        if (!m_pFontUploadCommandDispatched)
        {
            uploadFonts();
            return;
        }

        if (!m_pFontUploaded) return;

        ImGui_ImplVulkan_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        auto editor = GetEditor();

        m_pGlobalPopup.Draw();

        if (m_StartDialog)
        {
            renderStartDialog();
        } else
        {
            processRecentProjects();

            if (m_pTextInputOpen)
                drawTextInput();

            bool collapsed;
            ImGuiWindowFlags flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
            ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;
            ImGuiViewport* viewport = ImGui::GetMainViewport();
            ImGui::SetNextWindowPos(viewport->Pos);
            ImGui::SetNextWindowSize(viewport->Size);
            ImGui::SetNextWindowViewport(viewport->ID);
            ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
            ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0.0f, 0.0f});
            flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize |
                     ImGuiWindowFlags_NoMove;
            flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

            if (dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode)
                flags |= ImGuiWindowFlags_NoBackground;

            ImGui::Begin("##mainviewport", &collapsed, flags);
            UtilDocking::SetupDockspace();
            ImGui::PopStyleVar(3);

            drawMenuBar();

            ImGui::End();

            m_pViewport.Render();

            for (auto& window: m_pWindows)
            {
                MV_PROFILE_FUNCTION("EditorUI::OnRender::window");
                MV_PROFILE_TAG("Window:", window->GetNameV().data())
                window->OnRender();
            }

            ImGui::ShowDemoWindow();
            ImGui::ShowMetricsWindow();

            static bool focusViewport = true;
            static int framesTillFocus = 2;

            if (focusViewport)
            {
                if (framesTillFocus > 0)
                {
                    framesTillFocus--;
                } else
                {

                    focusViewport = false;
                    ImGuiWindow* window = ImGui::FindWindowByName("Viewport");
                    if (window == NULL || window->DockNode == NULL || window->DockNode->TabBar == NULL)
                        return;
                    window->DockNode->TabBar->NextSelectedTabId = window->TabId;
                }
            }
        }

        ImGui::Render();

        DISPATCH_RT_COMMAND_LAMBDA(ImGuiDraw,
                                   [drawData(duplicateDrawData(ImGui::GetDrawData()))](
                                           MV::RenderCommandBuilder& CmdBuilder)
                                   {
                                       ImGui_ImplVulkan_RenderDrawData((ImDrawData*) &drawData,
                                                                       (VkCommandBuffer) CmdBuilder.m_CommandBuffer);

                                       for (uint32_t i = 0; i < drawData.CmdListsCount; i++)
                                       {
                                           IM_DELETE(drawData.CmdLists[i]);
                                       }
                                   }
        );

    }

    void EditorUI::drawMenuBar()
    {
        if (ImGui::BeginMenuBar())
        {
            if (ImGui::BeginMenu("File"))
            {
                if (ImGui::MenuItem("New..."))
                {

                }
                if (ImGui::MenuItem("Save"))
                {
                    GetEditor()->SaveData();
                }

                if (ImGui::MenuItem("VSync", nullptr, MV::GetRenderer()->GetVSyncEnabled()))
                {
                    MV::GetRenderer()->SetVSyncEnabled(!MV::GetRenderer()->GetVSyncEnabled());
                }

                ImGui::EndMenu();
            }
            if (ImGui::BeginMenu("Edit"))
            {
                if (ImGui::MenuItem("Project Settings"))
                {
                    GetWindow<ProjectSettings>()->SetOpen(true);
                }
                ImGui::Separator();

                ImGui::EndMenu();
            }
            if (ImGui::BeginMenu("Run"))
            {
                auto* engine = MV::GetEngine();

                {
                    ScopedDisabled dis{engine->IsGameRunning()};
                    if (ImGui::MenuItem("Run"))
                    {
                        GetEditor()->StartGame();
                    }
                }
                {
                    ScopedDisabled dis{!engine->IsGameRunning()};
                    if (ImGui::MenuItem("Stop"))
                    {
                        GetEditor()->StopGame();
                    }
                }

                ImGui::EndMenu();
            }
            if (ImGui::BeginMenu("Editor"))
            {
                auto* engine = MV::GetEngine();

                {
                    if (ImGui::MenuItem("Save CVars", nullptr))
                    {
                        MV::CVarManager::Get().Save();
                    }

                    if (ImGui::MenuItem("Load CVars", nullptr))
                    {
                        MV::CVarManager::Get().Load();
                    }
                }

                {
                    if (ImGui::MenuItem("Reload shaders", nullptr))
                    {
                        engine->ReloadShaders();
                        OnResize();
                    }
                }

                {
                    bool v = MV::GetRenderer()->GetDrawsWireframe();
                    if (ImGui::MenuItem("Draw wireframe", nullptr, v))
                    {
                        MV::GetRenderer()->SetDrawWireframe(!v);
                    }
                }

                {
                    ImGui::MenuItem("Show Collisions (All)", nullptr, &GetEditor()->m_ShowAllCollisions);
                }
                {
                    auto& world = engine->GetWorld();

                    ScopedDisabled dis{!world.IsValid()};

                    /*
                    if (ImGui::MenuItem("Show Collision (physics engine)", nullptr, world.m_ShowColliders))
                    {
                        world.m_ShowColliders = !world.m_ShowColliders;
                    }*/
                }


                ImGui::EndMenu();
            }
            if (ImGui::BeginMenu("UI"))
            {
                if (ImGui::BeginMenu("Scale"))
                {
                    if (ImGui::MenuItem("100%"))
                    {
                        GetEditor()->m_Settings.m_UIScale = 1.0f;
                        ImGui::GetIO().FontGlobalScale = GetEditor()->m_Settings.m_UIScale;
                    }

                    if (ImGui::MenuItem("150%"))
                    {
                        GetEditor()->m_Settings.m_UIScale = 1.5f;
                        ImGui::GetIO().FontGlobalScale = GetEditor()->m_Settings.m_UIScale;
                    }
                    ImGui::EndMenu();
                };
                ImGui::EndMenu();
            }
            if (ImGui::BeginMenu("Windows"))
            {
                for (auto&& window: m_pWindows)
                {
                    if (!window->ShowInMenu())
                        continue;
                    if (ImGui::MenuItem(window->GetNameV().data(), nullptr, window->IsOpen(), true))
                    {
                        window->SwitchBetweenOpenAndClose();
                    }
                }
                ImGui::EndMenu();
            }
#ifndef NDEBUG
            if (ImGui::BeginMenu("Debug"))
            {
                if (ImGui::MenuItem("Assembly inspector"))
                {

                }

                ImGui::MenuItem("Log Allocations (SLOW)", nullptr, &MV::DefaultAllocator::s_LogAllocations);

                if (ImGui::MenuItem("Dump imgui allocations"))
                {
                    DumpImGuiAllocations();
                }

                ImGui::EndMenu();
            }
#endif
            ImGui::EndMenuBar();
        }
    }

    struct ImGuiMVAllocationInfo
    {
        MV::map<void*, size_t> m_Data;
    };

    static ImGuiMVAllocationInfo* imguiAllocInfo = nullptr;

    void EditorUI::initImgui()
    {
        IMGUI_CHECKVERSION();
        ImGui::CreateContext();

        MV::PushAllocatorFlag(MV::AllocationType::Editor);
        imguiAllocInfo = new ImGuiMVAllocationInfo();
        MV::PopAllocatorFlag();

        if (CVar::s_TrackImGuiAllocations)
        {
            ImGui::SetAllocatorFunctions([](size_t sz, void* user_data)
                                         {
                                             MV::PushAllocatorFlag(MV::AllocationType::Editor_ImGui);
                                             MV::AllocatorProvider provider{};

                                             if (sz > 100000000) // 100mb
                                             {
                                                 MV::GetLogger()->LogWarning("ImGui allocated {} bytes", sz);
                                             }

                                             auto res = provider.allocate(sz);

                                             auto data = (ImGuiMVAllocationInfo*) user_data;
                                             data->m_Data[res] = sz;

                                             MV::PopAllocatorFlag();
                                             return res;
                                         }, [](void* ptr, void* user_data)
                                         {
                                             MV::PushAllocatorFlag(MV::AllocationType::Editor_ImGui);
                                             MV::AllocatorProvider provider{};

                                             auto data = (ImGuiMVAllocationInfo*) user_data;
                                             const auto fnd = data->m_Data.find(ptr);

                                             size_t size = 0;

                                             if (fnd != data->m_Data.end())
                                             {
                                                 size = fnd->second;
                                             }

                                             provider.deallocate(ptr, size);
                                             MV::PopAllocatorFlag();
                                         }, imguiAllocInfo);

        }


        ImGuiIO& io = ImGui::GetIO();
        (void) io;
        //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;       // Enable Keyboard Controls
        //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
        io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking
        //io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;         // Enable Multi-Viewport / Platform Windows (not working for GLFW)
        // Sets styles
        ImGuiStyle& style = ImGui::GetStyle();
        if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
        {
            style.WindowRounding = 0.0f;
            style.Colors[ImGuiCol_WindowBg].w = 1.0f;
        }

        auto vulkanRenderer = MV::static_pointer_cast<MV::VulkanRenderer>(MV::GetEngine()->m_Renderer);
        auto& window = MV::GetEngine()->m_Window;

        auto glfwWindow = static_cast<GLFWwindow*>(window->GetContext());

        ImGui_ImplGlfw_InitForVulkan(glfwWindow, true);
        auto imguiCreateInfoMV = vulkanRenderer->CreateImguiInfo();

        ImGui_ImplVulkan_InitInfo imguiCreateInfo{};

        imguiCreateInfo.Instance = imguiCreateInfoMV.Instance;
        imguiCreateInfo.PhysicalDevice = imguiCreateInfoMV.PhysicalDevice;
        imguiCreateInfo.Device = imguiCreateInfoMV.Device;
        imguiCreateInfo.QueueFamily = imguiCreateInfoMV.QueueFamily;
        imguiCreateInfo.Queue = imguiCreateInfoMV.Queue;
        imguiCreateInfo.PipelineCache = imguiCreateInfoMV.PipelineCache;
        imguiCreateInfo.DescriptorPool = imguiCreateInfoMV.DescriptorPool;
        imguiCreateInfo.Subpass = imguiCreateInfoMV.Subpass;
        imguiCreateInfo.MinImageCount = imguiCreateInfoMV.MinImageCount;
        imguiCreateInfo.ImageCount = imguiCreateInfoMV.ImageCount;
        imguiCreateInfo.MSAASamples = imguiCreateInfoMV.MSAASamples;
        imguiCreateInfo.Allocator = imguiCreateInfoMV.Allocator;


        auto baseRenderPassHandle = vulkanRenderer->MT_GetMainRenderPass();

        MV::rc<MV::RenderPass> baseRenderPass;

        ENSURE_TRUE(vulkanRenderer->MT_GetResource(baseRenderPassHandle, baseRenderPass));

        auto* vulkanRenderPass = static_cast<MV::VulkanRenderPass*>(baseRenderPass.get());

        ImGui_ImplVulkan_Init(&imguiCreateInfo, vulkanRenderPass->GetVulkanRenderPass());

        ImGui::GetIO().FontGlobalScale = 1.0f;

        io.Fonts->AddFontFromFileTTF("fonts/Roboto-Regular.ttf", 16.0f);

        //io.Fonts->AddFontDefault();

        static const ImWchar icons_ranges[] = {ICON_MIN_FA, ICON_MAX_FA, 0};
        ImFontConfig icons_config;
        icons_config.MergeMode = true;
        icons_config.PixelSnapH = true;
        io.Fonts->AddFontFromFileTTF(FONT_ICON_FILE_NAME_FAS, 16.0f, &icons_config, icons_ranges);
        icons_config.MergeMode = false;
        io.Fonts->AddFontFromFileTTF(FONT_ICON_FILE_NAME_FAS, 56.0f, &icons_config, icons_ranges);

        for (auto&& uiWindow: m_pWindows)
            uiWindow->OnInit();
    }

    void EditorUI::cleanupImgui()
    {
        for (auto&& window: m_pWindows) window->OnCleanup();
        ImGui::DestroyContext();
    }

    void EditorUI::uploadFonts()
    {
        volatile bool* fontDone = &m_pFontUploaded;

        DISPATCH_RT_COMMAND_DELAYED(ImGuiUploadFont,
                                    {
                                        CmdBuilder.EndRenderPass();
                                        MV::GetLogger()->RenderRT_LogDebug("Creating imgui font texture!");
                                        ImGui_ImplVulkan_CreateFontsTexture(
                                                (VkCommandBuffer) CmdBuilder.m_CommandBuffer);
                                        *fontDone = true;
                                        CmdBuilder.BindMainRenderPass();
                                    }
        );

        m_pFontUploadCommandDispatched = true;
    }

    void EditorUI::initWindows()
    {
        LogWindow::s_Instance = GetWindow<LogWindow>();

        for (auto&& w: RegisteredWindows::GetInstance().m_WindowLookup)
        {
            m_pWindows.emplace_back(w.second.m_Window);
        }

        m_pViewport.Init();
    }

    bool EditorUI::OnCleanup()
    {
        cleanupImgui();
        m_pOpenProjectFileDialog->Cleanup();
        m_pCreateProjectFileDialog->Cleanup();
        return true;
    }

    void EditorUI::OnResize()
    {
        m_pViewport.OnResize();
        GetWindow<ScriptableConfigEditor>()->OnResize();
    }

    void EditorUI::OnInit()
    {
        static MV::unordered_map<MV::string, MV::string> fileNameCache;
        initWindows();

        initImgui();
        setupStyle(true, 0.5f);
        ImGui::GetIO().FontGlobalScale = GetEditor()->m_Settings.m_UIScale;

        m_pOpenProjectFileDialog = FileBrowser::Get(FileBrowserMode::OPEN_SINGLE);
        m_pOpenProjectFileDialog->SetTypeFilters({"*.proj"});
        m_pOpenProjectFileDialog->Init();

        m_pCreateProjectFileDialog = FileBrowser::Get(FileBrowserMode::OPEN_DIRECTORY);
        m_pCreateProjectFileDialog->Init();
    }

    bool EditorUI::OnRendererCleanup()
    {
        ImGui_ImplVulkan_DestroyFontUploadObjects();
        ImGui_ImplVulkan_Shutdown();
        return true;
    }

    static const char* const CREATE_PROJECT_POPUP_ID = "##create_project";
    static const char* const START_POPUP_ID = "Welcome";
    static const char* const WAIT_POPUP_ID = "##wait_for_load";

    void EditorUI::renderStartDialog()
    {
        static bool openCreatePopup = false;
        static bool openWaitPopup = false;
        static bool openMainPopup = true;

        static bool showBadNameError = false;
        static bool showBadPathError = false;
        static bool showCreateProjectError = false;

        if (MV::GetEngine()->GetProject().m_Valid)
        {
            m_StartDialog = false;
            GetEditor()->OnAssetChange();
        }

        if (!ImGui::IsPopupOpen(START_POPUP_ID) && !ImGui::IsPopupOpen(CREATE_PROJECT_POPUP_ID) &&
            !ImGui::IsPopupOpen(WAIT_POPUP_ID))
        {
            openMainPopup = true;
        }

        auto height = MV::GetEngine()->m_Window->GetWindowHeight();
        auto width = MV::GetEngine()->m_Window->GetWindowWidth();

        const char* projectTypeOptions[] = {"2D Tilemap", "3D World"};
        static const char* current_item = projectTypeOptions[0];

        if (openMainPopup)
        {
            ImGui::OpenPopup(START_POPUP_ID);
            openMainPopup = false;
        }

        ImGui::SetNextWindowPos({(width / 2.0f - 200), height / 2.0f - 125});
        ImGui::SetNextWindowSize({400, 250});

        if (ImGui::BeginPopup(START_POPUP_ID, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove))
        {
            const auto area = ImGui::GetContentRegionAvail();
            ImGui::BeginVertical("##top_text", area);
            ImGui::BeginHorizontal("TitleWrapper");
            ImGui::Spring();
            ImGui::Text("Recent projects:");
            ImGui::Spring();
            ImGui::EndHorizontal();
            float sub = ImGui::GetItemRectSize().y
                        + ImGui::CalcTextSize("APPLY").y
                        + ImGui::GetStyle().FramePadding.y * 2.0f
                        + ImGui::GetStyle().ItemSpacing.y * 2.0f
                        + ImGui::GetStyle().ItemInnerSpacing.y * 2.0f;

            if (ImGui::BeginChild("RecentProjects", {-1.0f, area.y - sub}))
            {
                auto& recents = GetEditor()->GetRecentProjects();

                if (recents.empty())
                {
                    ScopedDisabled dis{};
                    ImGui::BeginHorizontal("NoRecentsWrapper");
                    ImGui::Spring();
                    ImGui::TextUnformatted("No recent projects found.");
                    ImGui::Spring();
                    ImGui::EndHorizontal();
                }

                uint32_t counter = 0;

                ImGui::PushStyleColor(ImGuiCol_ChildBg, ImGui::GetStyleColorVec4(ImGuiCol_Button));

                bool anyHovered = false;

                for (size_t i = 0; i < recents.size(); i++)
                {
                    const auto& recent = recents[i];

                    if (m_pHoveredProjectIndex == i)
                        ImGui::PushStyleColor(ImGuiCol_ChildBg, ImGui::GetStyleColorVec4(ImGuiCol_ButtonHovered));

                    const auto singleProjectHeight =
                            ImGui::CalcTextSize("NAME").y * 2.0f
                            + ImGui::GetStyle().FramePadding.y * 2.0f
                            + ImGui::GetStyle().ItemSpacing.y * 4.0f;

                    if (ImGui::BeginChild((MV::string("##_recent_") + recent.m_Name).c_str(),
                                          {-1, singleProjectHeight},
                                          true, ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse))
                    {
                        ImGui::BeginVertical("Recent_Wrapper", {-1.0f, 0});

                        ImGui::BeginHorizontal("Recent_top_wrapper");

                        ImGui::TextUnformatted(recent.m_Name.c_str());
                        ImGui::Spring();

                        char buf[80];
                        struct tm* ts;
                        ts = localtime(&recent.m_LastOpened);
                        strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M", ts);

                        ImGui::TextUnformatted(buf);

                        ImGui::EndHorizontal();
                        {
                            ScopedDisabled dis{};
                            ImGui::TextWrapped(recent.m_Path.c_str());
                        }
                        ImGui::EndVertical();
                    }
                    ImGui::EndChild();
                    if (m_pHoveredProjectIndex == i)
                        ImGui::PopStyleColor();

                    if (ImGui::IsItemHovered())
                    {
                        m_pHoveredProjectIndex = i;
                        anyHovered = true;
                    }

                    if (ImGui::IsItemClicked(ImGuiMouseButton_Left))
                    {
                        MV::GetEngine()->LoadProject(recent.m_Path);
                        GetEditor()->m_pLoadedProjectPath = recent.m_Path;
                        openWaitPopup = true;
                        m_pJustOpenedProject = true;
                    }
                }

                if (!anyHovered)
                    m_pHoveredProjectIndex = -1;

                ImGui::PopStyleColor();
            }
            ImGui::EndChild();
            ImGui::Spring();

            ImGui::BeginHorizontal("##buttons_hor");
            if (ImGui::Button("Exit", {60, 24}))
            {
                MV::GetEngine()->Stop();
            }

            ImGui::Spring();

            if (ImGui::Button("Test Serialization", {60, 24}))
            {
                TestSerializationObject obj;

                obj.WOs.resize(10);

                MV::Archive ar;
                MV::SerializationWhitelist wl;
                auto wres = MV::ClassSerializer<TestSerializationObject>::Write(obj, ar, wl);
                ENSURE_TRUE(wres);

                MV::BinaryArchiveProcessor proc;
                proc.WriteToFile("test_binary", ar);


                auto res = proc.ReadFromFile("test_binary");
                if (res.valid())
                {
                    TestSerializationObject obj2;
                    MV::Archive ar2 = MV::move(res.extract_data());

                    auto success = MV::ClassSerializer<TestSerializationObject>::Read(obj2, ar2, wl);
                    ENSURE_TRUE(success == MV::FieldReadResult::Ok);
                }

            }

            if (ImGui::Button("Open", {60, 24}))
            {
                m_pOpenProjectFileDialog->Open();

                const auto& selectedFile = m_pOpenProjectFileDialog->GetSelectedFile();

                if (!selectedFile.empty())
                {
                    MV::GetEngine()->LoadProject(selectedFile.c_str());
                    GetEditor()->m_pLoadedProjectPath = selectedFile.c_str();
                    openWaitPopup = true;
                    m_pJustOpenedProject = true;
                }
            }

            if (ImGui::Button("Create", {60, 24}))
            {
                openCreatePopup = true;
            }
            ImGui::EndHorizontal();

            ImGui::EndVertical();

            ImGui::EndPopup();
        }

        if (openCreatePopup)
        {
            ImGui::OpenPopup(CREATE_PROJECT_POPUP_ID);
            openCreatePopup = false;
            ImGui::SetNextWindowPos({(width / 2.0f - 200), height / 2.0f - 125});
            ImGui::SetNextWindowSize({400, 250});
        }

        if (ImGui::BeginPopup(CREATE_PROJECT_POPUP_ID, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove))
        {
            ImGui::BeginVertical("##whole_popup", ImGui::GetContentRegionAvail());

            ImGui::BeginHorizontal("##title_wrapper");
            ImGui::Spring();
            ImGui::Text("Create new project");
            ImGui::Spring();
            ImGui::EndHorizontal();

            ImGui::BeginVertical("##input_child");

            ImGui::InputText("Project name", &m_pProjectName);

            if (showBadNameError)
            {
                ImGui::TextColored({0.9f, 0.1f, 0.1f, 1.0f}, "Invalid project name!");
            }

            ImGui::BeginHorizontal("##path_wrapper");
            ImGui::InputText("Path", &m_pProjectPath);
            if (ImGui::Button("Browse"))
            {
                m_pCreateProjectFileDialog->Open();
                m_pProjectPath = m_pCreateProjectFileDialog->GetSelectedFile();
            }
            ImGui::EndHorizontal();

            if (showBadPathError)
            {
                ImGui::TextColored({0.9f, 0.1f, 0.1f, 1.0f}, "Invalid project path!");
            }

            if (ImGui::BeginCombo("Project type", current_item))
            {
                for (auto& projectTypeOption: projectTypeOptions)
                {
                    bool is_selected = (current_item == projectTypeOption);
                    if (ImGui::Selectable(projectTypeOption, is_selected))
                        current_item = projectTypeOption;
                    if (is_selected)
                        ImGui::SetItemDefaultFocus();
                }
                ImGui::EndCombo();
            }

            ImGui::Spring();

            ImGui::EndVertical();

            ImGui::Spring();

            if (showCreateProjectError)
            {
                ImGui::TextColored({0.9f, 0.1f, 0.1f, 1.0f}, "Failed to create project save!");
            }

            ImGui::BeginHorizontal("##buttons_create_project");
            if (ImGui::Button("Cancel", {60, 24}))
            {
                openMainPopup = true;
                ImGui::CloseCurrentPopup();
            }

            ImGui::Spring();

            if (ImGui::Button("Confirm", {60, 24}))
            {
                bool valid = true;
                auto nameInvalid = m_pProjectName.length() < 2;
                auto pathInvalid = m_pProjectPath.empty();
                showBadNameError = nameInvalid;
                showBadPathError = pathInvalid;

                if (nameInvalid)
                    valid = false;
                if (pathInvalid)
                    valid = false;

                auto project = ProjectProducer::CreateProject(m_pProjectName);

                if (valid)
                {
                    try
                    {
                        m_pProjectPath = ProjectProducer::CreateProjectInDirectory(project, m_pProjectPath);
                        MV::GetEngine()->LoadProject(m_pProjectPath);
                        openWaitPopup = true;
                        ImGui::CloseCurrentPopup();
                        GetEditor()->m_pLoadedProjectPath = m_pProjectPath;
                    } catch (std::exception& e)
                    {
                        showCreateProjectError = true;
                    }
                }
            }

            ImGui::EndHorizontal();

            ImGui::EndVertical();

            ImGui::EndPopup();
        }

        if (openWaitPopup)
        {
            ImGui::OpenPopup(WAIT_POPUP_ID);
            openWaitPopup = false;
            ImGui::SetNextWindowPos({(width / 2.0f - 200), height / 2.0f - 125});
            ImGui::SetNextWindowSize({400, 250});
        }

        if (ImGui::BeginPopup(WAIT_POPUP_ID, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove))
        {
            ImGui::BeginVertical("##whole_popup_", ImGui::GetContentRegionAvail());

            ImGui::Spring();

            ImGui::ProgressBar(0.99f, {-1, 0}, "Loading...");

            ImGui::Spring();

            ImGui::EndVertical();
            ImGui::EndPopup();
        }

    }

    void EditorUI::setupStyle(bool isDarkTheme, float alpha)
    {
        ImGuiStyle& style = ImGui::GetStyle();

        style.AntiAliasedFill = true;
        style.AntiAliasedLines = true;
        style.ChildRounding = 1.0f;
        style.WindowRounding = 1.0f;
        style.PopupRounding = 4.0f;

        style.Alpha = 1.0f;
        style.FrameRounding = 3.0f;

        auto& colors = ImGui::GetStyle().Colors;
        auto& sequencerColors = ImGui::GetNeoSequencerStyle().Colors;
        colors[ImGuiCol_WindowBg] = ImVec4{0.075f, 0.075f, 0.075f, 1.0f};

        // Headers
        colors[ImGuiCol_Header] = ImVec4{0.2f, 0.205f, 0.21f, 1.0f};
        colors[ImGuiCol_HeaderHovered] = ImVec4{0.3f, 0.305f, 0.31f, 1.0f};
        colors[ImGuiCol_HeaderActive] = ImVec4{0.15f, 0.1505f, 0.151f, 1.0f};

        // Buttons
        colors[ImGuiCol_Button] = ImVec4{0.2f, 0.205f, 0.21f, 1.0f};
        colors[ImGuiCol_ButtonHovered] = ImVec4{0.3f, 0.305f, 0.31f, 1.0f};
        colors[ImGuiCol_ButtonActive] = ImVec4{0.15f, 0.1505f, 0.151f, 1.0f};

        // Frame BG
        colors[ImGuiCol_FrameBg] = ImVec4{0.2f, 0.205f, 0.21f, 1.0f};
        colors[ImGuiCol_FrameBgHovered] = ImVec4{0.3f, 0.305f, 0.31f, 1.0f};
        colors[ImGuiCol_FrameBgActive] = ImVec4{0.15f, 0.1505f, 0.151f, 1.0f};

        // Tabs
        colors[ImGuiCol_Tab] = ImVec4{0.15f, 0.1505f, 0.151f, 1.0f};
        colors[ImGuiCol_TabHovered] = ImVec4{0.38f, 0.3805f, 0.381f, 1.0f};
        colors[ImGuiCol_TabActive] = ImVec4{0.28f, 0.2805f, 0.281f, 1.0f};
        colors[ImGuiCol_TabUnfocused] = ImVec4{0.15f, 0.1505f, 0.151f, 1.0f};
        colors[ImGuiCol_TabUnfocusedActive] = ImVec4{0.2f, 0.205f, 0.21f, 1.0f};

        // Title
        colors[ImGuiCol_TitleBg] = ImVec4{0.15f, 0.1505f, 0.151f, 1.0f};
        colors[ImGuiCol_TitleBgActive] = ImVec4{0.15f, 0.1505f, 0.151f, 1.0f};
        colors[ImGuiCol_TitleBgCollapsed] = ImVec4{0.15f, 0.1505f, 0.151f, 1.0f};


        sequencerColors[ImGuiNeoSequencerCol_Bg] = colors[ImGuiCol_FrameBg];

        sequencerColors[ImGuiNeoSequencerCol_SelectedTimeline] = ImVec4{0.98f * 0.5f, 0.706f * 0.5f, 0.322f * 0.5f,
                                                                        0.88f};


        sequencerColors[ImGuiNeoSequencerCol_ZoomBarBg] = colors[ImGuiCol_Tab];
        sequencerColors[ImGuiNeoSequencerCol_ZoomBarSlider] = colors[ImGuiCol_TabHovered];
        sequencerColors[ImGuiNeoSequencerCol_ZoomBarSliderHovered] = colors[ImGuiCol_TabActive];

        sequencerColors[ImGuiNeoSequencerCol_FramePointer] = ImVec4{1.f, 0.396f, 0.09f, 1.0f};
        sequencerColors[ImGuiNeoSequencerCol_FramePointerHovered] = ImVec4{1.f, 0.51f, 0.18f, 1.0f};
        sequencerColors[ImGuiNeoSequencerCol_FramePointerPressed] = ImVec4{0.98f, 0.357f, 0.059f, 1.0f};

        for (auto& color: colors)
        {
            color = UtilGui::FixImGuiColorSpace(color);
        }

        for (auto& color: sequencerColors)
        {
            color = UtilGui::FixImGuiColorSpace(color);
        }
    }

    void EditorUI::processRecentProjects()
    {
        if (!m_pJustOpenedProject)
            return;

        m_pJustOpenedProject = false;

        onProjectLoad();
        auto editor = GetEditor();

        auto& projects = editor->GetRecentProjects();

        auto foundVal = std::find_if(projects.begin(), projects.end(), [&](const RecentProject& val) -> bool
        {
            return std::filesystem::path(val.m_Path.c_str()) ==
                   std::filesystem::path(editor->m_pLoadedProjectPath.c_str());
        });

        time_t now;
        time(&now);

        if (foundVal != projects.end())
        {
            foundVal->m_LastOpened = now;
        } else
        {
            auto& recent = projects.emplace_back();
            recent.m_Name = MV::GetEngine()->GetProject().m_Name.c_str();
            recent.m_Path = editor->m_pLoadedProjectPath;
            recent.m_LastOpened = now;
        }

        editor->SaveRecentProjects();
    }

    void EditorUI::onProjectLoad()
    {
        auto editor = GetEditor();
        editor->m_AssetWatcher.Stop();
        editor->m_AssetWatcher.Start(
                std::filesystem::path(editor->m_pLoadedProjectPath.c_str()).parent_path().string().c_str());

        if (MV::GetEngine()->GetProject().m_DefaultWorld == MV::InvalidUUID) return;

        MV::GetEngine()->m_ResourceManager.LoadResource(MV::GetEngine()->GetProject().m_DefaultWorld);
    }

    bool EditorUI::TextInput(MV::string_view name, MV::string* data, const GlobalTextInputCallback& onFinish)
    {
        if (m_pTextInputOpen)
            return false;

        m_pTextInputName = name;
        m_pTextInputOpen = true;
        m_pTextInputOpenPopup = true;
        m_pTextInputData = data;
        m_pTextInputCloseCallback = onFinish;

        return true;
    }

    void EditorUI::drawTextInput()
    {
        bool focus = false;

        if (m_pTextInputOpenPopup)
        {
            m_pTextInputOpenPopup = false;
            ImGui::OpenPopup("##global_text_input");
            focus = true;
        }

        ImGui::SetNextWindowSize({480, 360});
        if (ImGui::BeginPopupModal("##global_text_input", nullptr,
                                   ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize))
        {
            ImGui::Text("Set value for %s", m_pTextInputName.c_str());
            const float width = ImGui::GetContentRegionAvail().x;
            ImGui::PushItemWidth(width);

            if (focus)
                ImGui::SetKeyboardFocusHere(0);

            bool inputConfirmed = ImGui::InputText("##input", m_pTextInputData, ImGuiInputTextFlags_EnterReturnsTrue);

            bool inputEscaped = (ImGui::IsItemDeactivated() &&
                                 ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_Escape)));

            bool done = false;
            bool confirmed;

            if (ImGui::Button("Cancel", {width / 2.0f - ImGui::GetStyle().ItemSpacing.x / 2.0f, 0}) || inputEscaped)
            {
                ImGui::CloseCurrentPopup();
                done = true;
                confirmed = false;
            }
            ImGui::SameLine();
            if (ImGui::Button("Confirm", {width / 2.0f - ImGui::GetStyle().ItemSpacing.x / 2.0f, 0}) || inputConfirmed)
            {
                ImGui::CloseCurrentPopup();
                done = true;
                confirmed = true;
            }

            if (done)
            {
                m_pTextInputOpen = false;
                m_pTextInputData = nullptr;
                m_pTextInputName = "null";
                m_pTextInputCloseCallback(confirmed);
            }

            ImGui::EndPopup();
        }
    }

    MV::rc<Window> EditorUI::GetInterfaceWindow(uint32_t windowID)
    {
        auto& inst = RegisteredWindows::GetInstance();
        const auto res = inst.m_WindowLookup.find(windowID);

        return res == inst.m_WindowLookup.end() ? nullptr : res->second.m_Window;
    }

    void EditorUI::OnWorldRender()
    {
        if (!m_StartDialog)
        {
            for (auto&& window: m_pWindows)
                window->OnWorldRender();
        }
    }

    void EditorUI::OnUpdate(float deltaTime)
    {
        if (!m_StartDialog)
        {
            for (auto&& window: m_pWindows)
                window->Update(deltaTime);
        }
    }

    ImDrawData EditorUI::duplicateDrawData(ImDrawData* data)
    {
        ImDrawData res{};
        res.CmdLists = new ImDrawList* [data->CmdListsCount];
        for (int i = 0; i < data->CmdListsCount; i++)
        {
            res.CmdLists[i] = data->CmdLists[i]->CloneOutput();
        }

        res.Valid = data->Valid;
        res.CmdListsCount = data->CmdListsCount;
        res.TotalIdxCount = data->TotalIdxCount;
        res.TotalVtxCount = data->TotalVtxCount;
        res.DisplayPos = data->DisplayPos;
        res.DisplaySize = data->DisplaySize;
        res.FramebufferScale = data->FramebufferScale;

        return res;
    }

    void EditorUI::drawDragAndDrop()
    {
        if (m_DragAndDropContext.m_Active)
        {
            ImGuiDragDropFlags src_flags = 0;
            src_flags |= ImGuiDragDropFlags_SourceNoDisableHover;
            src_flags |= ImGuiDragDropFlags_SourceNoHoldToOpenOthers;
            src_flags |= ImGuiDragDropFlags_SourceExtern;

            if (ImGui::BeginDragDropSource(src_flags))
            {
                /*
                 *  Drawing code here
                 * */

                ImGui::TextUnformatted(m_DragAndDropContext.m_Name.c_str());

                if (m_DragAndDropContext.m_NonPossible)
                {
                    ImGui::SameLine();
                    ImGui::TextUnformatted("#!#");
                }


                if (m_DragAndDropContext.m_JustOpenned)
                {
                    ImGui::SetDragDropPayload(m_DragAndDropContext.m_ID.c_str(), m_DragAndDropContext.m_Payload,
                                              m_DragAndDropContext.m_PayloadSize);
                    m_DragAndDropContext.m_JustOpenned = false;
                }
                ImGui::EndDragDropSource();
            }
        }
    }

    bool EditorUI::GlobalPopupOk(MV::string_view title, PopupCloseCallback&& callback, MV::string_view body)
    {
        return m_pGlobalPopup.OpenOk(title, MV::move(callback), body);
    }

    bool
    EditorUI::GlobalPopupConfirmOrCancel(MV::string_view title, PopupCloseCallback&& callback, MV::string_view body)
    {
        return m_pGlobalPopup.OpenConfirmOrCancel(title, MV::move(callback), body);
    }

    bool EditorUI::GlobalPopupYesNo(MV::string_view title, PopupCloseCallback&& callback, MV::string_view body)
    {
        return m_pGlobalPopup.OpenYesNo(title, MV::move(callback), body);
    }

    void EditorUI::ProcessOsDragAndDrop(const MV::vector<MV::string>& data)
    {
        OSDragAndDropEvent e;

        e.m_pData = data;

        for (auto&& window: m_pWindows)
        {
            window->OnOSDragAndDropEvent(e);

            if (e.IsConsumed())
            {
                break;
            }
        }
    }

    void EditorUI::DumpImGuiAllocations()
    {
        std::ofstream ofs{"memorydump.mv", std::ios::out};

        if (!ofs.is_open())
        {
            MV::GetLogger()->LogError("Failed to open dump file");
            return;
        }

        for (auto& v: imguiAllocInfo->m_Data)
        {
            std::string line;

            line += fmt::format("{}", v.first).c_str();
            line += "|";
            line += fmt::format("{}", v.second).c_str();
            line += "\n";

            ofs << line;
        }

        ofs.close();
    }

    bool
    EditorUI::GlobalPopupCustomModal(MV::string_view title, CustomModalDrawCallback&& callback)
    {
        return m_pGlobalPopup.OpenCustomModal(title, MV::move(callback));
    }
}
