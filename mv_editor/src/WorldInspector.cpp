//
// Created by Matty on 2021-11-02.
//

#include <common/EngineCommon.h>
#include "../editor/interface/WorldInspector.h"
#include "../editor/imgui/imgui.h"
#include "../editor/util/UtilGui.h"
#include "../editor/EditorCommon.h"
#include "common/subsystem/Subsystem.h"
#include "interface/ObjectEditor.h"
#include "interface/WorldSettings.h"


namespace Editor
{

    WorldInspector::WorldInspector() : Window(Config::sc_WorldInspectorWindowName.data())
    {

    }

    void WorldInspector::WindowSetup()
    {

    }

    void WorldInspector::Render()
    {
        MV::AllocatorTagScope allocTag{MV::AllocationType::Editor};
        auto* engine = MV::GetEngine();
        auto& world = engine->GetWorld();

        const auto hasWorld = world.IsValid();
        bool requestOpenContextMenu = false;

        if (ImGui::IsMouseReleased(ImGuiMouseButton_Right) &&
            ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByPopup))
            if (!ImGui::IsAnyItemHovered())
                requestOpenContextMenu = false;

        ScopedDisabled dis{!hasWorld};

        if (ImGui::TreeNodeEx("World Root"))
        {
            if (hasWorld)
            {
                auto& selectedWO = MV::GetEngine()->IsGameRunning() ? GetEditor()->m_SelectedWO_Runtime
                                                                    : GetEditor()->m_SelectedWO;

                uint64_t toErase = 0;

                for (auto&& wo: world.m_WorldObjects)
                {
                    auto flags =
                            ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_Bullet | ImGuiTreeNodeFlags_NoTreePushOnOpen |
                            ImGuiTreeNodeFlags_SpanFullWidth | ImGuiTreeNodeFlags_AllowItemOverlap;

                    if (selectedWO == wo.first)
                        flags |= ImGuiTreeNodeFlags_Selected;

                    if (ImGui::TreeNodeEx("##wo", flags, "%s", wo.second->m_Name.c_str()))
                    {
                    }

                    if (ImGui::IsItemHovered(ImGuiHoveredFlags_RectOnly))
                    {
                        if (ImGui::IsMouseClicked(ImGuiMouseButton_Right))
                        {
                            m_pRightClickedWorldObject = wo.first;
                            ImGui::OpenPopup(fmt::format("{}", m_pRightClickedWorldObject).c_str());
                        }
                    }

                    if (m_pRightClickedWorldObject == wo.first &&
                        ImGui::BeginPopup(fmt::format("{}", m_pRightClickedWorldObject).c_str()))
                    {
                        requestOpenContextMenu = false;
                        {
                            ScopedDisabled disa{MV::GetEngine()->IsGameRunning()};
                            if (ImGui::MenuItem(fmt::format("Rename##{}", m_pRightClickedWorldObject).c_str()))
                            {
                                m_pRenameWorldObjectNewName = wo.second->m_Name;
                                ENSURE(GetEditor()->TextInputGlobal("Object name", &m_pRenameWorldObjectNewName,
                                                                    [&, beginName(wo.second->m_Name), w(wo.second)](
                                                                            bool confirmed)
                                                                    {
                                                                        if (confirmed &&
                                                                            beginName != m_pRenameWorldObjectNewName &&
                                                                            !m_pRenameWorldObjectNewName.empty())
                                                                        {
                                                                            w->m_Name = m_pRenameWorldObjectNewName;
                                                                            GetEditor()->m_DirtyAssets.emplace(
                                                                                    world.m_ID);
                                                                        }
                                                                        m_pRenameWorldObjectNewName.clear();
                                                                    }));
                            }
                            if (ImGui::MenuItem(fmt::format("Remove##{}", m_pRightClickedWorldObject).c_str()))
                            {
                                toErase = m_pRightClickedWorldObject;
                                GetEditor()->m_DirtyAssets.emplace(world.m_ID);
                            }
                        }

                        ImGui::EndPopup();
                    }

                    if (ImGui::IsItemClicked(ImGuiMouseButton_Left))
                    {
                        selectedWO = wo.first;
                        auto window = GetEditor()->GetWindow<ObjectEditor>();
                        window->SetOpen(true);
                        window->FocusInDock();
                    }
                }

                if (toErase != 0)
                {
                    if (GetEditor()->m_SelectedWO == toErase)
                        GetEditor()->m_SelectedWO = 0;

                    removeWorldObject(world, toErase);
                }
            }
            ImGui::TreePop();
        }

        drawSubsystems();

        if (requestOpenContextMenu)
            ImGui::OpenPopup("##world_inspector_popup_menu");

        if (ImGui::BeginPopup("##world_inspector_popup_menu",
                              ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoTitleBar |
                              ImGuiWindowFlags_NoSavedSettings))
        {
            if (ImGui::MenuItem("New"))
            {

            }


            ImGui::EndPopup();
        }
    }

    void WorldInspector::removeWorldObject(MV::World& world, uint64_t toErase) const
    {
        const auto& worldObject = world.m_WorldObjects[toErase];

        world.DestroyWorldObject(worldObject);

        GetEditor()->m_DirtyAssets.emplace(world.m_ID);
    }

    void WorldInspector::PostRender()
    {
    }

    void WorldInspector::OnCleanup()
    {

    }

    void WorldInspector::OnInit()
    {

    }

    void WorldInspector::drawSubsystems()
    {
        ImGui::Separator();

        auto* engine = MV::GetEngine();
        auto& world = engine->GetWorld();

        const auto hasWorld = world.IsValid();

        if (ImGui::TreeNodeEx("Subsystems"))
        {
            if (ImGui::IsItemClicked(ImGuiMouseButton_Right))
            {
                ImGui::OpenPopup("Subsystems");
            }
            if (ImGui::BeginPopup("Subsystems"))
            {
                if(ImGui::BeginMenu("Add")) {
                    const auto subsystems = MV::Meta::MetaCache::GetInstance().GetClassesWithBaseClass<MV::WorldSubsystem>();

                    MV::ObjectInitializationContext ctx;

                    ctx.m_World = &world;

                    for(auto && v : subsystems) {
                        if(world.m_Subsystems.find(v.GetHash()) != world.m_Subsystems.end()) {
                            continue;
                        }

                        if (ImGui::MenuItem(v.m_Name.c_str())) {
                            world.m_Subsystems[v.GetHash()] = MV::static_pointer_cast<MV::WorldSubsystem>(v.Construct(ctx));
                            GetEditor()->m_DirtyAssets.emplace(world.m_ID);
                        }
                    }

                    ImGui::EndMenu();
                }
                ImGui::EndPopup();
            }

            if (hasWorld)
            {
                for (auto&& subsystem: world.m_Subsystems)
                {
                    auto subsystemMeta = MV::Meta::MetaCache::GetInstance().GetClassMetaFromHash(subsystem.first);

                    if (!subsystemMeta.IsValid())
                    {
                        continue;
                    }

                    auto flags =
                            ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_Bullet | ImGuiTreeNodeFlags_NoTreePushOnOpen |
                            ImGuiTreeNodeFlags_SpanFullWidth | ImGuiTreeNodeFlags_AllowItemOverlap;

                    //if (selectedWO == wo.first)
                    //    flags |= ImGuiTreeNodeFlags_Selected;

                    if (ImGui::TreeNodeEx("##world_subsystem", flags, "%s", subsystemMeta.m_Name.c_str()))
                    {
                    }

                    if (ImGui::IsItemHovered(ImGuiHoveredFlags_RectOnly))
                    {
                        if (ImGui::IsItemClicked(ImGuiMouseButton_Left))
                        {
                            auto settings = GetEditor()->GetWindow<WorldSettings>();
                            settings->SetWorld(&world);
                            settings->SetSubsystemToDraw(subsystemMeta);
                            settings->SetOpen(true);
                            settings->FocusInDock();
                        }
                    }
                }
            }
            ImGui::TreePop();
        }


    }
}