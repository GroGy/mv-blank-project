//
// Created by Matty on 11.12.2022.
//

#include "EASTL/algorithm.h"
#include "allocator/Alloc.h"
#include "async/GenerateTilemapCollisionTask.h"
#include "clipper2/clipper.h"
#include "qtree/Quadtree.h"
#include "common/EngineCommon.h"
#include "clipper2/clipper.core.h"

namespace Editor
{
    using namespace TilemapCollisionGeneration;

    GenerateTilemapCollisionTask::GenerateTilemapCollisionTask(GenerateTilemapCollisionTaskInput&& input) : m_pInput(
            MV::move(input))
    {

    }

    static const MV::WorldTile& GetTile(const MV::TilemapData& data, uint32_t x, uint32_t y)
    {
        return data.m_Data[y * data.m_Width + x];
    }

    void GenerateTilemapCollisionTask::Process()
    {
        MV::AllocatorTagScope allocTag{MV::AllocationType::Editor};

        GenerationContext ctx;

        m_pProgress = 0.0f;
        ctx.m_Progress = &m_pProgress;

        generateCollision(ctx);

        convertBodiesToStrips(ctx);

        m_pSuccess = true;
    }

    EditorTaskFinishResult GenerateTilemapCollisionTask::Finish()
    {
        m_pDone = true;
        return EditorTaskFinishResult::SUCCESS;
    }

    void GenerateTilemapCollisionTask::generateCollision(GenerationContext& ctx)
    {
        const auto& data = m_pInput.m_Data;
        MV::bitvector& processed = ctx.m_ProcessedTiles;
        processed.resize(data.m_Data.size(), false);
        MV::queue<int32_t> processQueue;

        float step = 0.5f / data.m_Data.size();

        for (int32_t dontUse = 0; dontUse < data.m_Data.size(); ++dontUse)
        {
            *ctx.m_Progress += step;
            if (processed[dontUse]) continue;

            processQueue.emplace(dontUse);

            MV::vector<CollisionData> colliders;

            while (!processQueue.empty())
            {
                int32_t i = processQueue.front();
                processQueue.pop();

                if (processed[i]) continue;

                processed[i] = true;

                uint32_t x = i % data.m_Height;
                uint32_t y = i / data.m_Height;

                auto tile = GetTile(data, x, y);
                if (tile.m_AtlasIndex == 0)
                {
                    continue;
                }

                const auto tileData = m_pInput.m_Tileset->GetTile(tile.m_AtlasIndex);
                const auto collisionData = tileData.GetCollision();

                if (!collisionData.HasCollision())
                {
                    continue;
                }

                auto& collider = colliders.emplace_back();

                collider.m_TilePosition = {x, y};

                int32_t indexOffset = 0; //collider.m_Vertices.size();

                for (auto& v: collisionData.m_Vertices)
                {
                    auto& vert = collider.m_Vertices.emplace_back(v);
                    vert.x += x;
                    vert.y += y;
                }

                for (auto& v: collisionData.m_Edges)
                {
                    auto& edge = collider.m_Edges.emplace_back();
                    edge.index1 = v.m_Index1 + indexOffset;
                    edge.index2 = v.m_Index2 + indexOffset;
                }

                enqueueAdjacentColliders({x, y}, [&](const glm::uvec2& tilePos) -> int32_t
                {
                    if (tilePos.x >= m_pInput.m_Data.m_Height) return -1;
                    if (tilePos.y >= m_pInput.m_Data.m_Width) return -1;

                    int32_t index = tilePos.y * data.m_Width + tilePos.x;

                    if (processed[index])
                    {
                        return -1;
                    }

                    return index;
                }, processQueue);
            }

            if (colliders.empty())
            {
                continue;
            }

            colliders = mergeAdjacentTiles(colliders);

            //colliders = optimizeColliders(colliders);

            for (auto& c: colliders)
            {
                auto& body = ctx.m_Bodies.emplace_back();
                body.m_Data = c;
            }

        }
    }

    void GenerateTilemapCollisionTask::convertBodiesToStrips(GenerationContext& ctx)
    {
        float step = 0.5f / ctx.m_Bodies.size();

        for (auto& body: ctx.m_Bodies)
        {
            *ctx.m_Progress += step;

            if (body.m_Data.m_Edges.empty() || body.m_Data.m_Vertices.empty())
            {
                ENSURE(false);
                continue;
            }

            auto& strip = m_Result.m_Vertices.emplace_back();
            const auto& data = body.m_Data;

            MV::unordered_set<uint32_t> visited;

            CollisionEdge startEdge = findStartingEdge(body.m_Data);
            uint32_t currentIndex = startEdge.index1;
            uint32_t nextIndex = startEdge.index2;

            strip.push_back(data.m_Vertices[currentIndex]);
            strip.push_back(data.m_Vertices[nextIndex]);
            visited.insert(currentIndex);
            visited.insert(nextIndex);

            while (true)
            {
                bool found = false;
                for (const auto& edge: data.m_Edges)
                {
                    if (visited.find(edge.index1) != visited.end() && visited.find(edge.index2) == visited.end())
                    {
                        strip.push_back(data.m_Vertices[edge.index2]);
                        visited.insert(edge.index2);
                        found = true;
                        break;
                    } else if (visited.find(edge.index2) != visited.end() && visited.find(edge.index1) == visited.end())
                    {
                        strip.push_back(data.m_Vertices[edge.index1]);
                        visited.insert(edge.index1);
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    break;
                }
            }
        }
    }

    CollisionEdge GenerateTilemapCollisionTask::findStartingEdge(const CollisionData& data) const
    {
        MV::unordered_map<uint32_t, int> degree;
        for (const auto& edge: data.m_Edges)
        {
            degree[edge.index1]++;
            degree[edge.index2]++;
        }

        for (const auto& edge: data.m_Edges)
        {
            if (degree[edge.index1] == 1 || degree[edge.index2] == 1)
            {
                return edge;
            }
        }

        // Fallback in case there's no end vertex, just return the first edge
        return data.m_Edges[0];
    }

    MV::vector<CollisionData>
    GenerateTilemapCollisionTask::mergeAdjacentTiles(const MV::vector<CollisionData>& colliders) const
    {
        MV::vector<CollisionData> result;

        Clipper2Lib::PathsD paths;

        for (auto& c: colliders)
        {
            auto& pth = paths.emplace_back();
            for (int32_t i = 0; i < c.m_Edges.size(); i++)
            {
                const auto& a = c.m_Vertices[c.m_Edges[i].index1];
                //const auto& b = c.m_Vertices[c.m_Edges[i].index2];
                pth.emplace_back(Clipper2Lib::PointD{a.x, a.y});
            }
        }

        auto joined = Clipper2Lib::Union(paths, Clipper2Lib::FillRule::NonZero);

        for (auto&& v: joined)
        {
            auto& res = result.emplace_back();

            for (auto& p: v)
            {
                res.m_Vertices.emplace_back(p.x, p.y);
            }

            for (int32_t i = 1; i < v.size(); i++)
            {
                auto& edge = res.m_Edges.emplace_back();
                edge.index1 = i - 1;
                edge.index2 = i;
            }

            if (!v.empty())
            {
                auto& edge = res.m_Edges.emplace_back();
                edge.index1 = v.size() - 1;
                edge.index2 = 0;
            }
        }

        /*

        // Restructure for faster lookup
        MV::unordered_map<glm::uvec2, int32_t> positionLookup;
        MV::bitvector processed{colliders.size(), false};

        for (int32_t i = 0; i < colliders.size(); ++i)
        {
            const auto& c = colliders[i];
            positionLookup[c.m_TilePosition] = i;
        }

        MV::queue<int32_t> processQueue;
        MV::unordered_map<glm::uvec2, CollisionData> toMerge;

        for (int32_t i = 0; i < colliders.size(); ++i)
        {
            if (processed[i]) continue;

            processed[i] = true;

            toMerge[colliders[i].m_TilePosition] = colliders[i];

            auto& c = toMerge[colliders[i].m_TilePosition];

            enqueueAdjacentColliders(c.m_TilePosition,
                                     [&](const glm::uvec2& tilePos) -> int32_t {
                                         auto foundRes = positionLookup.find(tilePos);

                                         if(foundRes == positionLookup.end()) {
                                             return -1;
                                         }

                                         return foundRes->second;
                                     },processQueue);

            while (!processQueue.empty())
            {
                int32_t indexToMerge = processQueue.front();
                processQueue.pop();

                if (processed[indexToMerge]) continue;

                const auto& colToMerge = colliders[indexToMerge];

                // look for one of colliders next to this one, where

                CollisionData* toTry = nullptr;

                {
                    auto pos = colToMerge.m_TilePosition;

                    static const MV::array<glm::ivec2, 4> offsets{
                        glm::ivec2{0,1},
                        glm::ivec2{0,-1},
                        glm::ivec2{1,0},
                        glm::ivec2{-1,0}
                    };

                    for(int32_t g = 0; g < offsets.size(); g++) {
                        if(pos.x == 0 && offsets[g].x == -1) continue;
                        if(pos.y == 0 && offsets[g].y == -1) continue;
                        if(pos.x == m_pInput.m_Data.m_Width - 1 && offsets[g].x == 1) continue;
                        if(pos.x == m_pInput.m_Data.m_Height - 1 && offsets[g].y == 1) continue;

                        const auto fndRes = toMerge.find(pos + glm::uvec2{offsets[g].x, offsets[g].y});

                        if(fndRes != toMerge.end()) {
                            toTry = &fndRes->second;
                            break;
                        }
                    }
                }

                ENSURE(toTry != nullptr);

                bool merge = shouldMergeColliders(*toTry, colToMerge);

                if (!merge)
                {
                    continue;
                }

                processed[indexToMerge] = true;

                enqueueAdjacentColliders(colToMerge.m_TilePosition,
                                         [&](const glm::uvec2& tilePos) -> int32_t {
                                             auto foundRes = positionLookup.find(tilePos);

                                             if(foundRes == positionLookup.end()) {
                                                 return -1;
                                             }

                                             return foundRes->second;
                                         }, processQueue);


                toMerge[colToMerge.m_TilePosition] = colToMerge;
            }

            if(toMerge.empty()) {
                continue;
            }

            auto& collider = result.emplace_back();

            //TODO Accually merge all toMergeColliders

        }
         */

        return result;
    }

    bool areVerticesEqual(const glm::vec2& v1, const glm::vec2& v2, float epsilon = 1e-5f)
    {
        return std::fabs(v1.x - v2.x) < epsilon && std::fabs(v1.y - v2.y) < epsilon;
    }

    bool GenerateTilemapCollisionTask::shouldMergeColliders(const CollisionData& a, const CollisionData& b) const
    {
        MV::unordered_set<glm::vec2> commonVertices;

        auto vertexExists = [](const glm::vec2& vertex, const MV::vector<glm::vec2>& vertices, float epsilon = 1e-5f)
        {
            return std::any_of(vertices.begin(), vertices.end(), [&vertex, epsilon](const glm::vec2& v)
            {
                return areVerticesEqual(vertex, v, epsilon);
            });
        };

        for (const auto& edge: a.m_Edges)
        {
            const glm::vec2& vertex1 = a.m_Vertices[edge.index1];
            const glm::vec2& vertex2 = a.m_Vertices[edge.index2];

            if (vertexExists(vertex1, b.m_Vertices))
            {
                commonVertices.insert(vertex1);
            }
            if (vertexExists(vertex2, b.m_Vertices))
            {
                commonVertices.insert(vertex2);
            }

            if (commonVertices.size() >= 2)
            {
                return true;
            }
        }

        return false;
    }

    void GenerateTilemapCollisionTask::enqueueAdjacentColliders(const glm::uvec2& tilePosition,
                                                                const eastl::function<int32_t(
                                                                        const glm::uvec2&)>& lookup,
                                                                MV::queue<int32_t>& processQueue) const
    {
        {
            auto foundRes = lookup(tilePosition + glm::uvec2{0, 1});
            if (foundRes != -1)
            {
                processQueue.push(foundRes);
            }
        }
        {
            if (tilePosition.y > 0)
            {
                auto foundRes = lookup(tilePosition + glm::uvec2{0, -1});
                if (foundRes != -1)
                {
                    processQueue.push(foundRes);
                }
            }
        }
        {
            auto foundRes = lookup(tilePosition + glm::uvec2{1, 0});
            if (foundRes != -1)
            {
                processQueue.push(foundRes);
            }
        }
        {
            if (tilePosition.x > 0)
            {
                auto foundRes = lookup(tilePosition + glm::uvec2{-1, 0});
                if (foundRes != -1)
                {
                    processQueue.push(foundRes);
                }
            }
        }

    }

    MV::vector<CollisionData>
    GenerateTilemapCollisionTask::optimizeColliders(const MV::vector<CollisionData>& colliders)
    {
        MV::vector<CollisionData> result;

        for (auto& c: colliders)
        {
            auto& newCollider = result.emplace_back();

            MV::vector<CollisionEdge> originalEdges;
            originalEdges.reserve(c.m_Edges.size());

            CollisionEdge* currentEdge = nullptr;

            for (int32_t i = 0; i < c.m_Edges.size(); i++)
            {
                if (!currentEdge)
                {
                    currentEdge = &originalEdges.emplace_back();
                    currentEdge->index1 = c.m_Edges[i].index1;
                    currentEdge->index2 = c.m_Edges[i].index2;
                }

                auto v1 = c.m_Vertices[currentEdge->index1];
                auto v2a = c.m_Vertices[currentEdge->index2] - v1;
                auto v2b = c.m_Vertices[c.m_Edges[i].index2] - v1;
                const auto dot = glm::dot(v2a, v2b);

                if (dot < 0.99f)
                {
                    currentEdge = nullptr;
                    //i--;
                    continue;
                }

                currentEdge->index2 = c.m_Edges[i].index2;
            }

            MV::unordered_map<int32_t, int32_t> indexRemap;

            newCollider.m_Vertices.reserve(c.m_Vertices.size());

            for (auto& e: originalEdges)
            {
                if (indexRemap.find(e.index1) == indexRemap.end())
                {
                    indexRemap[e.index1] = newCollider.m_Vertices.size();
                    newCollider.m_Vertices.emplace_back(c.m_Vertices[e.index1]);
                }

                if (indexRemap.find(e.index2) == indexRemap.end())
                {
                    indexRemap[e.index2] = newCollider.m_Vertices.size();
                    newCollider.m_Vertices.emplace_back(c.m_Vertices[e.index2]);
                }
            }

            newCollider.m_Edges.resize(originalEdges.size());

            for (int32_t i = 0; i < newCollider.m_Edges.size(); ++i)
            {
                newCollider.m_Edges[i].index1 = indexRemap[originalEdges[i].index1];
                newCollider.m_Edges[i].index2 = indexRemap[originalEdges[i].index2];
            }
        }

        return result;
    }

    GenerateTilemapCollisionTaskInput::GenerateTilemapCollisionTaskInput(const MV::TilemapData& data) : m_Data(data)
    {}
}