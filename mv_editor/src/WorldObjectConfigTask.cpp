//
// Created by Matty on 05.06.2024.
//

#include "async/WorldObjectConfigTask.h"
#include "common/EngineCommon.h"
#include "resources/ProducerUtils.h"
#include "resources/ProjectProducer.h"
#include "EditorCommon.h"
#include "resources/ScriptableConfigProducer.h"
#include "meta/Helpers.h"
#include "resources/common/ScriptConfigAssetType.h"

namespace Editor {
    WorldObjectConfigTask::WorldObjectConfigTask(WorldObjectConfigTaskInput&& input) : m_pInput(MV::move(input))
    {

    }

    void WorldObjectConfigTask::Process()
    {
        if(!m_pInput.m_Class.IsChildOf(MV::GetClassMeta<MV::WorldObject>())) {
            m_pErrorText = "Class is not based of WorldObject";
            m_pProgress = 0.0f;
            m_pSuccess = false;
            return;
        }

        MV::ScriptableConfigData data;

        auto world = MV::World{};

        world.Init();

        world.m_PropertyManager.Init();


        MV::Archive ar;
        MV::SerializationWhitelist wl;

        MV::ObjectInitializationContext ctx;

        ctx.m_World = &world;

        auto obj = MV::static_pointer_cast<MV::WorldObject>(m_pInput.m_Class.Construct(ctx));
        obj->m_pOwningWorld = &world;


        if(!MV::IClassSerializer::WriteTypeFromHash(obj.get(), m_pInput.m_Class, ar, wl)) {
            m_pErrorText = "Failed to serialize class";
            m_pProgress = 0.0f;
            m_pSuccess = false;
            return;
        }

        world.Cleanup();

        m_pProgress = 0.5f;
        try {
            data.m_ClassHash = m_pInput.m_Class.GetHash();
            data.m_Data = MV::move(ar);

            m_pFinalPath = ScriptableConfigProducer::CreateConfigResource(data, m_pInput.m_Path);
            if(m_pFinalPath.empty()) {
                throw std::exception();
            }
            m_pFinalPath = Producer::ProducerUtils::PathAbsoluteToObjectRelative(m_pFinalPath);
            m_pProgress = 1.0f;
            m_pSuccess = true;
        } catch(std::exception &e) {
            (void) e;
            m_pProgress = 0.0f;
            m_pSuccess = false;
        }
    }

    EditorTaskFinishResult WorldObjectConfigTask::Finish()
    {
        if (!m_pSuccess) {
            m_pDone = true;
            return EditorTaskFinishResult::FAILED;
        }

        auto &proj = MV::GetEngine()->GetProject();

        auto assetId = Producer::ProducerUtils::GenerateUUID();

        auto& asset = proj.m_Assets[assetId];
        asset.m_Path = m_pFinalPath.data();
        asset.m_Type = MV::AssetTypeRegistry::Get().GetRuntimeAssetType<MV::ScriptConfigAssetType>();

        ProjectProducer::SaveProject(proj, GetEditor()->GetLoadedProjectPath());

        m_pDone = true;
        return EditorTaskFinishResult::SUCCESS;
    }
}