//
// Created by Matty on 10.12.2022.
//

#ifndef IMGUI_DEFINE_MATH_OPERATORS
#define IMGUI_DEFINE_MATH_OPERATORS
#endif

#include "imgui/imgui_internal.h"
#include "tools/tilemap_editor/Util.h"

namespace Editor {
    ImVec2 WorldSpaceToViewSpace(const glm::vec2 &worldPosition, const glm::vec2& cameraPos, const ImVec2 &windowMin, const ImVec2 &windowMax, const float tileSizePx) {
        const ImVec2 windowSize = windowMax - windowMin;

        const ImVec2 windowCenter = {
                (windowMin.x + windowSize.x / 2.0f),
                (windowMin.y + windowSize.y / 2.0f)
        };

        const auto realPosition =
                ImVec2{worldPosition.x, worldPosition.y} - ImVec2{cameraPos.x, cameraPos.y};

        const float windowTilesSize = tileSizePx;

        const ImVec2 result = {
                windowCenter.x + (((realPosition.x) / (2.0f)) * windowTilesSize),
                windowCenter.y - (((realPosition.y) / (2.0f)) * windowTilesSize),
        };

        return result;
    }

    glm::vec2 ViewSpaceToWorldSpace(const ImVec2& screenPos, const glm::vec2& cameraPos, const ImVec2 &windowMin, const ImVec2 &windowMax, const float tileSizePx) {
        const ImVec2 windowSize = windowMax - windowMin;
        auto minusMin = screenPos - windowMin;
        minusMin.y = windowSize.y - minusMin.y;
        auto normalizedPos = minusMin / (windowMax - windowMin);

        const ImVec2 windowTilesSize = windowSize / (tileSizePx);

        glm::vec2 center = cameraPos;

        const auto remappedNormalizedPos = ImVec2{
                (normalizedPos.x * 2.0f) - 1.0f,
                (normalizedPos.y * 2.0f) - 1.0f
        };

        const auto res = center + glm::vec2{
                remappedNormalizedPos.x * windowTilesSize.x,
                remappedNormalizedPos.y * windowTilesSize.y,
        };

        return res;
    }
}