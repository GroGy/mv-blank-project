//
// Created by Matty on 08.12.2022.
//

#include "tools/tilemap_editor/TilemapEditorTool.h"

namespace Editor {
    TilemapEditorToolContext::TilemapEditorToolContext(MV::TilemapData &mData, MV::vector<glm::uvec2> &mHighlight, bool & tilemapDirty)
            : m_Data(mData), m_Highlight(mHighlight), m_TilemapDirty(tilemapDirty) {}
}