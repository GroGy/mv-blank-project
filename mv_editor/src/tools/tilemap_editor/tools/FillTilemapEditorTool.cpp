//
// Created by Matty on 15.07.2024.
//

#include "tools/tilemap_editor/tools/FillTilemapEditorTool.h"
#include "imgui/icons.h"
#include "imgui/imgui.h"
#include "tools/tilemap_editor/Util.h"
#include "input/Key.h"
#include "imgui/imgui_internal.h"

namespace Editor {

    MV::string_view FillTilemapEditorTool::GetName()
    {
        return "Fill";
    }

    MV::string_view FillTilemapEditorTool::GetIconGlyph()
    {
        return ICON_FA_FILL;
    }

    void FillTilemapEditorTool::Update(TilemapEditorToolContext& context)
    {
        processInput(context);
    }

    void FillTilemapEditorTool::OnDeselect()
    {

    }

    void FillTilemapEditorTool::OnSelect()
    {

    }

    bool FillTilemapEditorTool::shouldPlaceTiles() const
    {
        return ImGui::IsMouseDown(ImGuiMouseButton_Left);
    }

    void floodFill(TilemapEditorToolContext& context, int32_t X, int32_t Y, uint16_t fillTile, uint16_t tileToFill) {
        if (X < 0 || X >= context.m_Data.m_Width || Y < 0 || Y >= context.m_Data.m_Height) return;

        int32_t width = context.m_Data.m_Width;
        int32_t height = context.m_Data.m_Height;

        MV::unordered_set<glm::ivec2> processed;
        MV::stack<glm::ivec2> stack;
        stack.push({X, Y});

        while (!stack.empty()) {
            auto c = stack.top();
            auto cx = c.x;
            auto cy = c.y;
            stack.pop();

            if(processed.find(c) != processed.end()) continue;

            processed.emplace(c);

            if (cx < 0 || cx >= width || cy < 0 || cy >= height) continue;
            if (context.m_Data.m_Data[cx + cy * width].m_AtlasIndex != fillTile) continue;

            context.m_Data.m_Data[cx + cy * width].m_AtlasIndex = tileToFill;

            stack.push({cx + 1, cy});
            stack.push({cx - 1, cy});
            stack.push({cx, cy + 1});
            stack.push({cx, cy - 1});
        }
    }

    void FillTilemapEditorTool::processInput(TilemapEditorToolContext& context)
    {
        auto viewBB = ImRect{context.m_WindowMin, context.m_WindowMax};

        auto ws = ::Editor::ViewSpaceToWorldSpace(ImGui::GetMousePos(),
                                                  context.m_CameraPosition,
                                                  context.m_WindowMin,
                                                  context.m_WindowMax,
                                                  context.m_TileSizePx);

        const int32_t X = (int32_t)ws.x;
        const int32_t Y = (int32_t)ws.y;

        if(X < 0 || X >= context.m_Data.m_Width) return;
        if(Y < 0 || Y >= context.m_Data.m_Height) return;

        uint16_t fillTile = context.m_Data.m_Data[X + Y*context.m_Data.m_Width].m_AtlasIndex;
        uint16_t tileToFill = context.m_SelectedTile;

        if(fillTile == tileToFill) { return; }

        if (shouldPlaceTiles() && viewBB.Contains(ImGui::GetMousePos())) {
            floodFill(context, X, Y, fillTile, tileToFill);
        }
    }
}