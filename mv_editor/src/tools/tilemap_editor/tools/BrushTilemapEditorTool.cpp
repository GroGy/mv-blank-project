//
// Created by Matty on 10.12.2022.
//

#include "tools/tilemap_editor/tools/BrushTilemapEditorTool.h"
#include "imgui/icons.h"
#include "imgui/imgui.h"
#include "tools/tilemap_editor/Util.h"
#include "input/Key.h"
#include "imgui/imgui_internal.h"

namespace Editor {
    MV::string_view IBrushTilemapEditorTool::GetName() {
        return m_pIsEraser ? "Eraser" : "Brush";
    }

    MV::string_view IBrushTilemapEditorTool::GetIconGlyph() {
        return m_pIsEraser ? ICON_FA_ERASER : ICON_FA_PAINT_BRUSH;
    }

    void IBrushTilemapEditorTool::Update(TilemapEditorToolContext &context) {
        updateRadius(context);
        generateHighlight(context);
        processInput(context);
    }

    void IBrushTilemapEditorTool::OnDeselect() {

    }

    void IBrushTilemapEditorTool::OnSelect() {

    }

    bool IBrushTilemapEditorTool::shouldPlaceTiles() const {
        return ImGui::IsMouseDown(ImGuiMouseButton_Left);
    }

    void IBrushTilemapEditorTool::updateRadius(TilemapEditorToolContext &context) {
        const float scroll = ImGui::GetIO().MouseWheel;
        if(ImGui::GetIO().KeysDown[static_cast<uint32_t>(MV::Key::LeftControl)] && (scroll > 0.01f || scroll < -0.01f)) {
            context.m_LockScrollInput = true;
            m_pRadius = std::max(int32_t(m_pRadius + scroll), 0);
        }
    }

    void IBrushTilemapEditorTool::generateHighlight(TilemapEditorToolContext &context) {
        auto ws = ::Editor::ViewSpaceToWorldSpace(ImGui::GetMousePos(),
                                                    context.m_CameraPosition,
                                                              context.m_WindowMin,
                                                              context.m_WindowMax,
                                                              context.m_TileSizePx);

        for(int32_t x = 0; x <  1+ m_pRadius * 2; x++) {
            for(int32_t y = 0; y < 1+ m_pRadius * 2; y++) {
                const int32_t rX = (int32_t)ws.x + (x - m_pRadius);
                const int32_t rY = (int32_t)ws.y + (y - m_pRadius);

                if(rX < 0 || rY < 0 || rX >= context.m_Data.m_Width || rY >= context.m_Data.m_Height)
                    continue;

                context.m_Highlight.emplace_back(rX,rY);
            }
        }
    }

    void IBrushTilemapEditorTool::processInput(TilemapEditorToolContext &context) {
        auto viewBB = ImRect{context.m_WindowMin, context.m_WindowMax};
        if(shouldPlaceTiles() && viewBB.Contains(ImGui::GetMousePos())) {
            for(auto && v : context.m_Highlight) {
                auto & tile = context.m_Data.m_Data[v.y * context.m_Data.m_Width + v.x];
                context.m_TilemapDirty = true;

                if(m_pIsEraser)
                {
                    tile.m_AtlasIndex = 0;
                    continue;
                }

                tile.m_AtlasIndex = context.m_SelectedTile;
            }
        }
    }
}