//
// Created by Matty on 2021-10-28.
//

#include "../editor/interface/importers/TextureImporter.h"
#include "EditorCommon.h"

namespace Editor
{
    TextureImporter::TextureImporter() : Window(Config::sc_TextureImportWindowName.data())
    {
        m_Flags = m_Flags | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse |
                  ImGuiWindowFlags_AlwaysAutoResize;
        m_pOpen = false;
        m_TextureFileDialog = FileBrowser::Get(FileBrowserMode::OPEN_SINGLE);
    }

    void TextureImporter::WindowSetup()
    {
        const ImGuiViewport* viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->GetCenter(), ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
    }

    void TextureImporter::Render()
    {
        ImGui::BeginVertical("##importer_wrapper");

        ImGui::BeginHorizontal("##input_wrapper");

        if (m_pReimportAsset != MV::InvalidUUID)
        {
            ImGui::TextUnformatted("REIMPORT");
            m_TextureFileDialog->SetMode(FileBrowserMode::OPEN_SINGLE);
        } else {
            m_TextureFileDialog->SetMode(FileBrowserMode::OPEN_SINGLE); // TODO: This can be changed to multiple when its supported
        }

        if (m_Paths.empty())
        {
            ImGui::Text("< No textures selected >");
        } else
        {
            ImGui::BeginVertical("##input_wrapper");
            for (const auto& input: m_Paths)
            {
                ImGui::Text(input.c_str());
            }
            ImGui::EndVertical();
        }
        ImGui::Spring();
        if (ImGui::Button("Browse"))
        {
            m_Paths.clear();
            m_TextureFileDialog->Open();

            auto path = m_TextureFileDialog->GetSelectedFile();
            m_Paths.reserve(1);

            if (!path.empty())
            {
                try
                {
                    std::filesystem::path fsPath{path.c_str()};
                    MV::string name = fsPath.stem().string().c_str();
                    m_Paths.emplace_back(path);
                } catch (const std::exception& e)
                {
                    m_pError = fmt::format("Path parsing failed with error:{}", e.what()).c_str();
                }
            }
        }
        ImGui::EndHorizontal();

        if (m_ShowFileError)
        {
            ImGui::TextColored({0.9f, 0.1f, 0.1f, 1.0f}, m_FileDialogError.c_str());
        }

        if (m_pImporting && m_pTask)
        {
            ImGui::ProgressBar(m_pTask->GetProgress(), {-1.0f, 0}, "Importing...");
            if (m_pTask->IsDone())
            {
                m_pImporting = false;
                m_pError = m_pTask->GetErrorText();
                if (m_pTask->Success())
                {
                    m_pOpen = false;
                    m_Paths.clear();
                    GetEditor()->OnAssetChange();
                }
                m_pTask.reset();
            }
        } else
        {
            ImGui::BeginHorizontal("##buttons_hor");
            ImGui::Spring();
            if (ImGui::Button(m_pReimportAsset != MV::InvalidUUID ? "Reimport" : "Import"))
            {
                m_pImporting = true;

                if(m_pReimportAsset != MV::InvalidUUID) {
                    m_pTask = MV::make_rc<TextureTask>(m_Paths[0], m_pReimportAsset);
                } else {
                    m_pTask = MV::make_rc<TextureTask>(m_Paths, m_CreateDirectory);
                }

                m_pTask->Start(m_pTask);
            }
            ImGui::EndHorizontal();
        }

        ImGui::EndVertical();
    }

    void TextureImporter::OnCleanup()
    {
        m_TextureFileDialog->Cleanup();
    }

    void TextureImporter::OnInit()
    {
        //m_TextureFileDialog.SetTypeFilters({".png"});
        m_TextureFileDialog->Init();
    }

    void TextureImporter::PostRender()
    {

    }

    void TextureImporter::OpenForReimport(MV::UUID asset)
    {
        SetOpen(true);
        m_pReimportAsset = asset;
    }

    void TextureImporter::OnOpenStateChanged(bool newValue)
    {
        if (!newValue)
        {
            m_pReimportAsset = MV::InvalidUUID;
        }
    }
}
