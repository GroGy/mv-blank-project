//
// Created by Matty on 2021-10-31.
//

#include <common/EngineCommon.h>
#include <render/vulkan/VulkanTexture.h>
#include "../editor/interface/common/ImagePreview.h"
#include "../editor/imgui/imgui.h"
#include "../editor/imgui/imgui_impl_vulkan.h"
#include <Engine.h>
#include "render/common/Renderer.h"


namespace Editor {
    void ImagePreview::Clear() {
        m_pImGuiImageCreated = false;
        m_pTextureHandle = {};
        m_pWasOpened = true;
    }

    void ImagePreview::Render() {
        if (!m_pWasOpened) {
            ImGui::OpenPopup(IMAGE_PREVIEW_POPUP_ID);
            m_pWasOpened = true;
        }

    if (ImGui::BeginPopup(IMAGE_PREVIEW_POPUP_ID, ImGuiWindowFlags_NoMove)) {
        auto engine = MV::GetEngine();
        auto &proj = engine->GetProject();
        auto &assets = proj.m_Assets;
        auto renderer = MV::GetRenderer();


        MV::rc<MV::Texture> res{};

        if(m_pTextureHandle.Valid() && !m_pImGuiImageCreated) {
            renderer->MT_GetResource(m_pTextureHandle,res);
        }

        if(!m_pImGuiImageCreated && (!res || !m_pTextureHandle.Created())) {
            ImGui::BeginVertical("##wrapper",{256,256 + ImGui::GetTextLineHeightWithSpacing()});
            ImGui::Text("Loading...");
            ImDrawList* draw_list = ImGui::GetWindowDrawList();
            ImGui::Spring();
            const ImVec2 p = ImGui::GetCursorScreenPos();
            static ImVec4 col = ImVec4(0.0f, 0.0f, 0.0f, 1.0f);
            const ImU32 col32 = ImColor(col);
            draw_list->AddRectFilled({p.x,p.y - 256}, ImVec2(p.x+256, p.y), col32, 0.0f,  ImDrawCornerFlags_All);
        } else {
            ImGui::BeginVertical("##wrapper");
            if(!m_pImGuiImageCreated) {
                if(auto vkImpl = eastl::dynamic_pointer_cast<MV::VulkanTexture>(res)) {
                    auto sampler = vkImpl->GetSampler();
                    auto view = vkImpl->m_TextureView;
                    m_pTextureRes.x = vkImpl->m_TextureWidth;
                    m_pTextureRes.y = vkImpl->m_TextureHeight;

                    auto ratio = ((float)m_pTextureRes.x / (float)m_pTextureRes.y);
                    m_pPreviewSize.x = static_cast<uint32_t>(256.0f * ratio);
                    m_pPreviewSize.y = 256;

                    m_pImGuiTextureID = ImGui_ImplVulkan_AddTexture(sampler,view,VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
                }
                m_pImGuiImageCreated = true;
            }

                ImGui::Text("TODO: Add texture asset"/*assets[m_pTextureID].second.c_str()*/);
                ImGui::Spring();

                //ImGui::Image(m_pImGuiTextureID,{(float)m_pPreviewSize.x,(float)m_pPreviewSize.y});
            }

            ImGui::EndVertical();
            ImGui::EndPopup();
        }

    }

    void ImagePreview::OpenHandle(MV::RTHandle textureHandle)
    {
        Clear();
        m_pTextureHandle = textureHandle;
        m_pWasOpened = false;
    }

    void ImagePreview::OpenAsset(MV::UUID assetID)
    {

    }

}