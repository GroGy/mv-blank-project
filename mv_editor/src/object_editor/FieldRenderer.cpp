//
// Created by Matty on 21.01.2023.
//

#include <utility>

#include "interface/object_editor/NumericFieldRenderers.h"
#include "interface/object_editor/PropertyFieldRenderer.h"
#include "interface/object_editor/GeneralFieldRenderers.h"
#include "interface/object_editor/common/FieldRenderer.h"
#include "imgui/imgui.h"
#include "common/EngineCommon.h"
#include "Engine.h"

namespace Editor {
    bool FieldRendererHelper::Begin(MV::string_view name) {
        if (!ImGui::TableSetColumnIndex(0))
            return false;
        ImGui::TextUnformatted(name.data());
        if (!ImGui::TableSetColumnIndex(1))
            return false;

        return true;
    }

    void FieldRendererHelper::End() {
        ImGui::TableNextRow();
    }

    bool FieldRendererHelper::Draw(const MV::Meta::Field& field, uint32_t &fieldCounter,
                                   const MV::rc<MV::EngineObject> &obj,
                                   void* objPtr,
                                   const MV::Meta::Class& klass) {
        bool objectIsDirty = false;

        if (Begin(field.m_Name)) {
            objectIsDirty = m_pDrawingCallback(field, fieldCounter, obj, objPtr, klass);
            End();
        }

        return objectIsDirty;
    }

    FieldRendererHelper::FieldRendererHelper(const DrawingCallback &drawingCallback) : m_pDrawingCallback(
            MV::move(drawingCallback)) {

    }

    void FieldDrawingImpl::DrawText(MV::string_view text) {
        ImGui::TextUnformatted(text.data());
    }

    bool FieldDrawingImpl::DrawCombo(MV::string_view internalName, const char **fields, uint32_t fieldCount,
                                     uint32_t &selectedIndex) {
        bool dirty = false;

        ImGui::PushItemWidth(ImGui::GetContentRegionAvail().x);
        if (ImGui::BeginCombo(internalName.data(), fields[selectedIndex])) {
            for (uint32_t i = 0; i < fieldCount; i++) {
                if (ImGui::Selectable(fields[i], i == selectedIndex)) {
                    selectedIndex = i;
                    dirty = true;
                }
            }
            ImGui::EndCombo();
        }

        return dirty;
    }

    bool FieldDrawingImpl::DrawCombo(MV::string_view internalName, const MV::vector<MV::string>& fields,
                                     uint32_t& selectedIndex)
    {
        bool dirty = false;

        ImGui::PushItemWidth(ImGui::GetContentRegionAvail().x);
        if (ImGui::BeginCombo(internalName.data(), fields[selectedIndex].c_str())) {
            for (uint32_t i = 0; i < fields.size(); i++) {
                if (ImGui::Selectable(fields[i].c_str(), i == selectedIndex)) {
                    selectedIndex = i;
                    dirty = true;
                }
            }
            ImGui::EndCombo();
        }

        return dirty;
    }

    bool FieldRendererProxy::DrawImpl(const MV::Meta::Field& field, uint32_t &fieldCounter,
                                  const MV::rc<MV::EngineObject> &obj,
                                  void* objPtr,
                                  const MV::Meta::Class& klass) {
        if (field.m_Type == MV::Meta::FieldType::Property)
        {
            return FieldRenderer<PropertyType>::Draw(field, fieldCounter, obj, objPtr, klass);
        }

        if (field.m_Type == MV::Meta::FieldType::Reference)
        {
            return FieldRenderer<AssetReferenceType>::Draw(field, fieldCounter, obj, objPtr, klass);
        }

        switch (field.m_PrimitiveType) {
            case MV::Meta::PrimitiveType::F16:
                break;
            case MV::Meta::PrimitiveType::F32:
                return FieldRenderer<float>::Draw(field, fieldCounter, obj, objPtr, klass);
            case MV::Meta::PrimitiveType::F64:
                return FieldRenderer<double>::Draw(field, fieldCounter, obj, objPtr, klass);
            case MV::Meta::PrimitiveType::I8:
                return FieldRenderer<int8_t>::Draw(field, fieldCounter, obj, objPtr, klass);
            case MV::Meta::PrimitiveType::I16:
                return FieldRenderer<int16_t>::Draw(field, fieldCounter, obj, objPtr, klass);
            case MV::Meta::PrimitiveType::I32:
                return FieldRenderer<int32_t>::Draw(field, fieldCounter, obj, objPtr, klass);
            case MV::Meta::PrimitiveType::U8:
                return FieldRenderer<uint8_t>::Draw(field, fieldCounter, obj, objPtr, klass);
            case MV::Meta::PrimitiveType::U16:
                return FieldRenderer<uint16_t>::Draw(field, fieldCounter, obj, objPtr, klass);
            case MV::Meta::PrimitiveType::U32:
                return FieldRenderer<uint32_t>::Draw(field, fieldCounter, obj, objPtr, klass);
            case MV::Meta::PrimitiveType::STRING:
                return FieldRenderer<MV::string>::Draw(field, fieldCounter, obj, objPtr, klass);
            case MV::Meta::PrimitiveType::Bool:
                return FieldRenderer<bool>::Draw(field, fieldCounter, obj, objPtr, klass);
            /*
             *
                return FieldRenderer<int64_t>::Draw(worldData, field, fieldCounter, wo, world);


                return FieldRenderer<bool>::Draw(worldData, field, fieldCounter, wo, world);
                return FieldRenderer<char>::Draw(worldData, field, fieldCounter, wo, world);
                return FieldRenderer<glm::ivec2>::Draw(worldData, field, fieldCounter, wo, world);
                return FieldRenderer<glm::ivec3>::Draw(worldData, field, fieldCounter, wo, world);
                return FieldRenderer<glm::uvec2>::Draw(worldData, field, fieldCounter, wo, world);
                return FieldRenderer<glm::uvec3>::Draw(worldData, field, fieldCounter, wo, world);
                return FieldRenderer<glm::vec2>::Draw(worldData, field, fieldCounter, wo, world);
                return FieldRenderer<glm::vec3>::Draw(worldData, field, fieldCounter, wo, world);
                return FieldRenderer<uint64_t>::Draw(worldData, field, fieldCounter, wo, world);
             * */
        }

        ENSURE_NO_ENTRY(false);
        return false;
    }

    bool IFieldRenderer::IsRunningGame() {
        return MV::GetEngine()->IsGameRunning();
    }

    EngineInternalFieldRendererHelper::EngineInternalFieldRendererHelper(NoFieldDrawingCallback callback)
    : FieldRendererHelper([](const MV::Meta::Field&, uint32_t&,
                             const MV::rc<MV::EngineObject>&, void*, const MV::Meta::Class&){ return false; }), m_pNoFieldDrawingCallback(MV::move(callback)) {

    }

    bool EngineInternalFieldRendererHelper::Draw(MV::string_view fieldName,
                                                 const MV::rc<MV::EngineObject>& obj,
                                                 const MV::Meta::Class& klass) {
        bool objectIsDirty = false;

        if (Begin(fieldName)) {
            objectIsDirty = m_pNoFieldDrawingCallback(fieldName, obj, klass);
            End();
        }

        return objectIsDirty;
    }
}