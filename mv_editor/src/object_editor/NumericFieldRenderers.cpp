//
// Created by Matty on 21.01.2023.
//

#include "interface/object_editor/NumericFieldRenderers.h"
#include "imgui/imgui.h"
#include "util/UtilGui.h"

namespace Editor {
    bool FieldDrawingImpl::DrawFloat(MV::string_view internalName, float& value, MV::optional<float> min, MV::optional<float> max) {
        ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);

        auto minV = min.valid() ? &min.get() : nullptr;
        auto maxV = max.valid() ? &max.get() : nullptr;

        return ImGui::DragScalar(internalName.data(),ImGuiDataType_Float,&value,0.1f,minV, maxV, "%.2f");
    }

    bool FieldDrawingImpl::DrawVec2(MV::string_view internalName, glm::vec2 & value, MV::optional<float> min, MV::optional<float> max) {
        const float widthTotal = ImGui::GetContentRegionAvail().x;
        const float singleColumnWidth = (widthTotal / 2.0f) - ImGui::GetStyle().ItemSpacing.x / 2.0f;
        ImGui::SetNextItemWidth(singleColumnWidth);
        bool dirty = ImGui::DragScalar(fmt::format("{}_x",internalName.data()).c_str(),ImGuiDataType_Float,&value.x,1.0f,nullptr, nullptr, "%.2f");

        ImGui::SameLine();

        ImGui::SetNextItemWidth(singleColumnWidth);
        if(ImGui::DragScalar(fmt::format("{}_y",internalName.data()).c_str(),ImGuiDataType_Float,&value.y,1.0f,nullptr, nullptr, "%.2f")) {
            dirty = true;
        }

        return dirty;
    }

    bool FieldDrawingImpl::DrawUVec2(MV::string_view internalName, glm::uvec2& value, MV::optional<uint32_t> min, MV::optional<uint32_t> max) {
        const float widthTotal = ImGui::GetContentRegionAvail().x;
        const float singleColumnWidth = (widthTotal / 2.0f) - ImGui::GetStyle().ItemSpacing.x / 2.0f;
        ImGui::SetNextItemWidth(singleColumnWidth);
        bool dirty = ImGui::DragScalar(fmt::format("{}_x",internalName.data()).c_str(),ImGuiDataType_U32,&value.x,1.0f);

        ImGui::SameLine();

        ImGui::SetNextItemWidth(singleColumnWidth);
        if(ImGui::DragScalar(fmt::format("{}_y",internalName.data()).c_str(),ImGuiDataType_U32,&value.y,1.0f)) {
            dirty = true;
        }

        return dirty;
    }

    bool FieldDrawingImpl::DrawU32(MV::string_view internalName, uint32_t& value, MV::optional<uint32_t> min, MV::optional<uint32_t> max) {
        ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
        return ImGui::DragScalar(internalName.data(),ImGuiDataType_U32,&value,1.0f,nullptr, nullptr, "%d");
    }

    bool FieldDrawingImpl::DrawI32(MV::string_view internalName, int32_t& value, MV::optional<int32_t> min, MV::optional<int32_t> max) {
        ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
        return ImGui::DragScalar(internalName.data(),ImGuiDataType_S32,&value,1.0f,nullptr, nullptr, "%d");
    }

    bool FieldDrawingImpl::DrawFloatWithOverride(MV::string_view internalName, float& value, bool& override)
    {
        const float checkboxWidth = ImGui::GetFrameHeight();
        const float totalWidth = ImGui::GetContentRegionAvail().x;
        const float inputWidth = totalWidth - checkboxWidth - ImGui::GetStyle().ItemSpacing.x;

        bool dirty = false;
        {
            ScopedDisabled dis{!override};
            ImGui::SetNextItemWidth(inputWidth);
            dirty = ImGui::DragScalar(internalName.data(),ImGuiDataType_Float,&value,1.0f,nullptr, nullptr, "%.2f");
        }

        ImGui::SameLine(0, ImGui::GetStyle().ItemSpacing.x);

        dirty |= ImGui::Checkbox(fmt::format("{}_override_checkbox", internalName).c_str(), &override);

        return dirty;
    }
}