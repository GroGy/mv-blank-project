//
// Created by Matty on 22.01.2023.
//

#include "interface/object_editor/GeneralFieldRenderers.h"
#include "imgui/imgui.h"
#include "imgui/imgui_stdlib.h"
#include "util/UtilGui.h"
#include "interface/common/AssetSelector.h"

namespace Editor {
    bool FieldDrawingImpl::DrawBool(MV::string_view internalName, bool& value) {
        const auto checkboxSize = ImGui::GetFrameHeight();
        const auto sideOffset = ImGui::GetContentRegionAvail().x / 2.0f
                - checkboxSize / 2.0f
                - ImGui::GetStyle().ItemSpacing.x;
        ImGui::Dummy({sideOffset, 0.0f});
        ImGui::SameLine();
        return ImGui::Checkbox(internalName.data(), &value);
    }

    bool FieldDrawingImpl::DrawString(MV::string_view internalName, MV::string& value) {
        ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
        return ImGui::InputText(internalName.data(), &value);
    }

    bool FieldDrawingImpl::DrawAssetReference(MV::string_view internalName, const MV::AssetTypeRuntimeLookup& type, MV::IAssetReference& value)
    {
        bool open = ::ImGui2::AssetSelectButton(value, {-1.0, 0.0f});

        MV::unordered_set<MV::AssetType> types;
        types.emplace(type);

        bool confirm = AssetSelector::DrawAssetSelector(
                internalName.data(), open,
                types,
                value);

        return confirm;
    }
}
