//
// Created by Matty on 22.01.2023.
//

#include "interface/object_editor/PropertyFieldRenderer.h"
#include "common/EngineCommon.h"
#include "imgui/imgui.h"
#include "Engine.h"
#include "meta/MetaCache.h"
#include "precompiler/PropertyFieldRendererRegistrator.h"
#include "imgui/imgui_internal.h"
#include "interface/common/PropertyDragAndDropPayload.h"

namespace Editor {
    static float nextItemSizeOverride = -1.f;

    static void processDragAndDrop(MV::string_view nameIn, MV::Property* property, const MV::rc<MV::WorldObject>& wo, MV::PropertyIDType propertyId) {
        static MV::string name;

        ImGuiDragDropFlags src_flags = 0;
        src_flags |= ImGuiDragDropFlags_SourceNoDisableHover;
        src_flags |= ImGuiDragDropFlags_SourceNoHoldToOpenOthers;

        if (ImGui::BeginDragDropSource(src_flags))
        {
            name = nameIn;

            PropertyDragAndDropPayload pld{};

            pld.m_Property = property;
            pld.m_Owner = wo.get();
            pld.m_PropertyId = propertyId;

            ImGui::TextUnformatted(name.c_str());

            ImGui::SetDragDropPayload("MV_PROPERTY_REF", &pld, sizeof(pld));
            ImGui::EndDragDropSource();
        }
    }

    bool FieldDrawingImpl::DrawProperty(EditorPropertyDrawingData data,
                                        uint32_t& fieldCounter,
                                        const MV::rc<MV::WorldObject>& wo, const MV::Meta::Class& klass, bool forceOpen) {
        if(forceOpen)
            ImGui::SetNextItemOpen(true);

        float windowHeight = nextItemSizeOverride <= 0.f ? 256.0f : nextItemSizeOverride;


        nextItemSizeOverride = -1.f;

        bool isDirty = false;

        auto cursorBefore = ImGui::GetCurrentWindow()->DC.CursorPos.y;

        MV::string name = data.m_Type == EditorPropertyDrawingData::Type::Direct ? data.m_Name : data.m_Field->m_Name;

        if (ImGui::TreeNodeEx(name.c_str(),
                              ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_NoTreePushOnOpen |
                              ImGuiTreeNodeFlags_SpanFullWidth)) {
            //processDragAndDrop(field.m_Name, storage->get_property(index), wo, world.m_PropertyManager.GetIDFromToken(field.m_Class));
            float frameOffset = ImGui::GetCurrentWindow()->DC.CursorPos.y - cursorBefore;
            windowHeight -= frameOffset;

            const auto& cache = MV::Meta::MetaCache::GetInstance();

            if (ImGui::BeginChild(fmt::format("##property_child_{}", fieldCounter++).c_str(),
                                  {ImGui::GetContentRegionAvail().x, windowHeight}, true)) {
                {
                    const auto& registeredPropertyRenderers = RegisteredPropertyRenderers::GetInstance().m_RenderersLookup;

                    MV::Meta::ClassHashType fieldHash = MV::Meta::sc_InvalidClassHash;
                    MV::Property* propertyPtr = nullptr;
                    const MV::Meta::Field* field = nullptr;

                    if(data.m_Type == EditorPropertyDrawingData::Type::FromField) {
                        const auto fieldTypeMeta = cache.GetClassMetaFromHash(data.m_Field->m_ClassHash);

                        if(!fieldTypeMeta.IsValid()) {
                            ImGui::EndChild();
                            return isDirty;
                        }

                        field = data.m_Field;
                        fieldHash = data.m_Field->m_ClassHash;
                    } else {
                        fieldHash = data.m_Property->GetTypeHash();
                        propertyPtr = data.m_Property;
                    }

                    const auto foundRes = registeredPropertyRenderers.find(fieldHash);

                    if (foundRes == registeredPropertyRenderers.end()) {
                        ImGui::EndChild();
                        return false;
                    }

                    if(data.m_Type == EditorPropertyDrawingData::Type::FromField) {
                        uint8_t* property = nullptr;
                        const bool res = klass.GetFieldLowLevel(wo.get(),*data.m_Field, property);

                        CHECK(res);
                        CHECK(property != nullptr);

                        auto* casted = (MV::Property**)property;

                        propertyPtr = (MV::Property*)*casted;
                    }

                    try {
                        bool dirty = foundRes->second.m_Draw(field, wo, propertyPtr);

                        isDirty |= dirty;
                    } catch (std::exception &e) {
                        (void)e;
                    }


                }
            }
            ImGui::EndChild();
        } else {
            //processDragAndDrop(field.m_Name, storage->get_property(index), wo, world.m_PropertyManager.GetIDFromToken(field.m_Class));
        }

        return isDirty;
    }

    void FieldDrawingImpl::SetNextPropertyHeight(float size)
    {
        nextItemSizeOverride = size;
    }

    EditorPropertyDrawingData EditorPropertyDrawingData::FromField(const MV::Meta::Field& field)
    {
        EditorPropertyDrawingData res{};

        res.m_Type = Type::FromField;
        res.m_Field = &field;

        return res;
    }

    EditorPropertyDrawingData EditorPropertyDrawingData::FromProperty(const MV::string& name, MV::Property* property)
    {
        EditorPropertyDrawingData res{};

        res.m_Type = Type::Direct;
        res.m_Name = name;
        res.m_Property = property;

        return res;
    }
}