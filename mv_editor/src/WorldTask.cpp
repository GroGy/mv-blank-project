//
// Created by Matty on 2022-01-17.
//

#include <common/EngineCommon.h>
#include "../editor/async/WorldTask.h"
#include "../editor/resources/ProducerUtils.h"
#include "../editor/resources/ProjectProducer.h"
#include "../editor/EditorCommon.h"
#include "../editor/resources/WorldProducer.h"

namespace Editor {
    void WorldTask::Process() {
        m_pProgress = 0.0f;
        MV::WorldData worldData{};
        worldData.m_Name = m_pInput.m_Name;
        //auto &mainLayer = worldData.m_TileLayers.emplace_back();

        m_pProgress = 0.3f;
        //mainLayer.m_pData.resize(m_pInput.m_Size.x * m_pInput.m_Size.y, 0);
        //mainLayer.m_Tileset = m_pInput.m_DefaultTileset;
        m_pProgress = 0.5f;

        m_pProgress = 0.8f;

        m_Result = worldData;

        m_FinalPath = WorldProducer::CreateWorldResource(worldData, m_pInput.m_Path);

        if(m_FinalPath.empty()) {
            m_pProgress = 1.0f;
            m_pSuccess = false;
            return;
        }

        m_FinalPath = Producer::ProducerUtils::PathAbsoluteToObjectRelative(m_FinalPath);

        m_pProgress = 1.0f;
        m_pSuccess = true;
    }

    EditorTaskFinishResult WorldTask::Finish() {
        if (!m_pSuccess) {
            m_pDone = true;
            return EditorTaskFinishResult::FAILED;
        }

        auto &proj = MV::GetEngine()->GetProject();

        m_WorldUUID = Producer::ProducerUtils::GenerateUUID();

        auto& asset = proj.m_Assets[m_WorldUUID];
        asset.m_Path = m_FinalPath.data();
        asset.m_Type = MV::AssetTypeRegistry::Get().GetRuntimeAssetType<MV::WorldAssetType>();

        if (proj.m_DefaultWorld == 0) {
            proj.m_DefaultWorld = m_WorldUUID;
        }

        ProjectProducer::SaveProject(proj, GetEditor()->GetLoadedProjectPath());

        m_pDone = true;
        return EditorTaskFinishResult::SUCCESS;
    }

    WorldTask::WorldTask(WorldTaskInput input) : m_pInput(std::move(input)) {

    }
}
