//
// Created by Matty on 2022-01-28.
//

#include "../editor/interface/AnimationTimeline.h"
#include "../editor/imgui/imgui_neo_sequencer.h"
#include "../editor/util/UtilGui.h"
#include "common/EngineCommon.h"

namespace Editor {
    void AnimationTimeline::WindowSetup() {

    }

    void AnimationTimeline::Render() {
        MV::AllocatorTagScope allocTag{MV::AllocationType::Editor};
    }

    void AnimationTimeline::PostRender() {

    }

    AnimationTimeline::AnimationTimeline() : Window(Config::sc_AnimationTimelineWindowName.data()) {

    }

    void AnimationTimeline::OnCleanup() {

    }

    void AnimationTimeline::OnInit() {

    }
}