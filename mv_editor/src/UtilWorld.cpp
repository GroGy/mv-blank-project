//
// Created by Matty on 2022-02-20.
//

#include "../editor/util/UtilWorld.h"
#include "common/EngineCommon.h"
#include "../editor/EditorCommon.h"
#include "serialization/common/ClassSerializer.h"
#include "precompiler/ClassSerializerRegistrator.h"

namespace Editor
{
    MV::rc<MV::WorldObject> UtilWorld::AddScriptableToWorld(MV::World& world, MV::string_view scriptName)
    {
        const auto camPos = (world.m_CameraOverride ? world.m_CameraOverride : world.m_Camera)->GetPosition();
        //Data side
        const auto id = world.m_WoIDCounter++;

        auto& data = world.m_WorldData.m_WorldObjects.emplace_back();
        data.m_WorldID = id;

        MV::ObjectInitializationContext ctx;
        MV::SerializationWhitelist wl;

        /*
        auto scriptable = MV::make_rc<MV::Scriptable>(ctx);

        scriptable->SetScriptName(ctx, scriptName);
        scriptable->m_WorldID = id;
        scriptable->m_Name = scriptName;

        ENSURE_TRUE(MV::ClassSerializer<MV::WorldObject>::Write(*scriptable, data.m_DataAr, wl, MV::SerializationCallbackMode::Delay), nullptr);

        scriptable->Init();

        MV_LOG("TODO: I removed code that creates properties here, pls fix. {}:{}", __FILE__,__LINE__)

        scriptable->SetOwningWorld(&world);
        world.m_WorldObjects[id] = std::move(scriptable);

        MV::RegisteredSerializers::GetInstance().ExecuteDelayedSerializations();

        if (world.IsRealWorld())
        {
            GetEditor()->m_DirtyAssets.emplace(world.m_ID);
            GetEditor()->m_SelectedWO = id;
        }

        return world.m_WorldObjects[id];*/
        return nullptr;
    }
}
