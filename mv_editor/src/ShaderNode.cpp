//
// Created by Matty on 2022-06-19.
//

#include "tools/shader_editor/common/ShaderNode.h"
#include "EditorCommon.h"
#include "imgui/imgui.h"
#include "imgui/imgui_internal.h"
#include "imgui/node_editor/imgui_node_editor.h"
#include "tools/shader_editor/ShaderEditor.h"
#include "tools/shader_editor/common/NodePins.h"
#include "tools/shader_editor/common/ShaderNodePinType.h"
#include "util/UtilGui.h"

namespace Editor
{
    namespace ed = ax::NodeEditor;

    void ShaderNode::Draw()
    {
        if (!shouldDraw()) return;

        ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, {1,0});

        ed::BeginNode(m_pNodeId);

        {
            ScopedVertical nodeVert{fmt::format("##node_{}_wrapper", m_pNodeId).c_str()};

            drawHeader();

            {
                ScopedHorizontal pinSpringWrapper{fmt::format("##node_{}_pin_spring_wrapper", m_pNodeId).c_str()};
                drawInputPins();
                ImGui::Spring();
                drawBodyImpl();
                ImGui::Spring();
                drawOutputPins();
            }

            drawPreview();
        }
        ed::EndNode();

        ImGui::PopStyleVar();
    }

    bool ShaderNode::shouldDraw() const
    {
        return true;
        //TODO: Implement check
    }

    void ShaderNode::drawInputPins()
    {
        ScopedVertical inputPinsWrapper{fmt::format("##node_{}_input_pins_wrapper", m_pNodeId).c_str()};

        for (auto&& pin: m_pInputPins)
        {
            ed::BeginPin(pin.m_Id, ax::NodeEditor::PinKind::Input);

            ed::PinPivotAlignment(ImVec2(0.05f, 0.5f));

            ed::PinPivotSize(ImVec2(0, 0));

            const float size = ImGui::CalcTextSize("#").y;

            auto pinColor = ShaderNodePinType::GetColor(pin.m_Type);
            pinColor.Value.w = pin.m_Optional ? 0.5 : 1;

            auto errorIds = GetEditor()->GetWindow<ShaderEditor>()->GetErrorIds();
            if(errorIds.find(pin.m_Id) != errorIds.end()){
                pinColor = {255,0,0,255};
            }

            BasicPin(ImGui::GetWindowDrawList(), pin.m_ConnectedPin != 0, ImGui::GetCurrentWindow()->DC.CursorPos,pinColor);
            ImGui::Dummy({size, size});
            ed::EndPin();

            ImGui::SameLine();

            ImGui::TextUnformatted(pin.m_Name.c_str());

        }
    }

    void ShaderNode::drawOutputPins() {
        ScopedVertical inputPinsWrapper{fmt::format("##node_{}_output_pins_wrapper", m_pNodeId).c_str()};

        for(auto && pin : m_pOutputPins) {
            if(pin.m_ShowName) {
                ImGui::TextUnformatted(pin.m_Name.c_str());
                ImGui::SameLine();
            }

            ed::BeginPin(pin.m_Id, ax::NodeEditor::PinKind::Output);

            ed::PinPivotAlignment(ImVec2(0.95f, 0.5f));
            ed::PinPivotSize(ImVec2(0, 0));
            const float size = ImGui::CalcTextSize("#").y;
            BasicPin(ImGui::GetWindowDrawList(), !pin.m_ConnectedOutputPins.empty(), ImGui::GetCurrentWindow()->DC.CursorPos,ShaderNodePinType::GetColor(pin.m_Type));
            ImGui::Dummy({size, size});
            ed::EndPin();

        }
    }

    void ShaderNode::drawHeader() {
        if(!shouldDrawHeader()) return;

        ScopedHorizontal hor{fmt::format("##node_{}_header_wrapper", m_pNodeId).c_str()};
        ImGui::Spring();
        ImGui::TextUnformatted(m_pNodeName.empty() ? "Missing node name" : m_pNodeName.c_str());
        ImGui::Spring();
    }

    uint32_t ShaderNode::GetId() {
        return ShaderNodeIds::InvalidNodeId;
    }

    uint32_t ShaderNode::GetNodeID() const {
        return m_pNodeId;
    }

    void ShaderNode::LoadData(const nlohmann::json &data) {
        auto pos = data["position"].get<glm::vec2>();
        m_pPosition.x = pos.x;
        m_pPosition.y = pos.y;

        m_pNodeId = data["id"];

        loadNodeData(data["data"]);
    }

    void ShaderNode::SaveData(nlohmann::json &data) {
        glm::vec2 pos{m_pPosition.x, m_pPosition.y};

        data["position"] = pos;
        data["id"] = m_pNodeId;

        nlohmann::json nodeData{};

        saveNodeData(nodeData);

        data["data"] = nodeData;
    }

    void ShaderNode::drawPreview()
    {
        if(!shouldDrawPreview()) return;

        bool shownArrowThisFrame = false;

        bool drawPreview = forceDrawPreview() || m_pPreviewVisible;
        if(!forceDrawPreview() && !m_pPreviewVisible) {
            //Draw button with arrow

            shownArrowThisFrame = true;
            ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 0.0f);
            if(ImGui::ArrowButtonEx(fmt::format("##preview_arrow_button_{}", m_pNodeId).c_str(), ImGuiDir_Down, ImVec2(150,ImGui::GetFrameHeight() - 2), ImGuiButtonFlags_None)) {
                m_pPreviewVisible = true;
                drawPreview = m_pPreviewVisible;
            }
            ImGui::PopStyleVar();
        }

        if(drawPreview)
        {
            drawPreviewImpl();

            if(!shownArrowThisFrame) { // Removes pop when clicking on button
                ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 0.0f);
                if(ImGui::ArrowButtonEx(fmt::format("##preview_arrow_button_{}", m_pNodeId).c_str(), ImGuiDir_Up, ImVec2(150,ImGui::GetFrameHeight() - 2), ImGuiButtonFlags_None)) {
                    m_pPreviewVisible = false;
                }
                ImGui::PopStyleVar();
            }
        }

    }

    bool ShaderNode::AreOutputsUnAssigned() const
    {
        if(m_pOutputPins.empty()) return false;

        for(auto && pin : m_pOutputPins)
            if(!pin.m_ConnectedOutputPins.empty()) return false;

        return true;
    }
}