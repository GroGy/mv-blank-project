//
// Created by Matty on 2022-02-17.
//

#include "util/UtilDocking.h"
#include "imgui/imgui.h"
#include "imgui/imgui_internal.h"
#include "interface/Toolbar.h"
#include "tools/shader_editor/ShaderEditor.h"
#include "interface/WorldInspector.h"
#include "interface/ObjectEditor.h"
#include "interface/Performance.h"
#include "interface/asset_browser/AssetBrowser.h"
#include "interface/AnimationTimeline.h"
#include "interface/LogWindow.h"
#include "tools/tilemap_editor/TilemapEditor.h"
#include "tools/tileset_editor/TilesetEditor.h"
#include "tools/animation_editor/AnimationEditor.h"
#include "interface/WorldSettings.h"

namespace Editor {
    void UtilDocking::SetupDockspace() {
        ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None ;
        ImGuiID dockspace_id;
        dockspace_id = ImGui::GetID("##maindockspace");
        const auto baseID = ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);

        const auto baseNode = ImGui::DockBuilderGetNode(baseID);
        baseNode->LocalFlags = ImGuiDockNodeFlags_NoSplit;
        //ImGui::DockBuilderFinish(baseID);

        if (!baseNode->IsSplitNode()) {

            ImGuiID topID;
            ImGuiID bottomID;

            ImGui::DockBuilderSplitNode(baseID, ImGuiDir_Up, 0.07f, &topID, &bottomID);

            const auto topNode = ImGui::DockBuilderGetNode(topID);
            topNode->LocalFlags |= static_cast<ImGuiDockNodeFlags_>(ImGuiDockNodeFlags_NoDocking) | ImGuiDockNodeFlags_NoResize | ImGuiDockNodeFlags_NoTabBar | ImGuiDockNodeFlags_NoSplit;

            const auto botNode = ImGui::DockBuilderGetNode(bottomID);
            botNode->LocalFlags = ImGuiDockNodeFlags_NoResize;

            ImGui::DockBuilderFinish(topID);
            ImGui::DockBuilderDockWindow(Config::sc_ToolbarWindowName.data(), topID);

            //Build default UI
            ImGuiID rightPanelID;
            ImGuiID centerPanelWithRightID;

            ImGui::DockBuilderSplitNode(bottomID, ImGuiDir_Right, 0.15f, &rightPanelID, &centerPanelWithRightID);
            ImGui::DockBuilderDockWindow(Config::sc_ObjectEditorWindowName.data(), rightPanelID);
            ImGui::DockBuilderDockWindow(Config::sc_WorldSettingsWindowName.data(), rightPanelID);

            ImGuiID leftPanelID;
            ImGuiID centerWithBottomPanelID;

            ImGui::DockBuilderSplitNode(centerPanelWithRightID, ImGuiDir_Left, (1.0f - 0.15f) * 0.15f, &leftPanelID, &centerWithBottomPanelID);

            ImGui::DockBuilderDockWindow(Config::sc_WorldInspectorWindowName.data(), leftPanelID);

            ImGuiID centerID;
            ImGuiID underViewportID;

            ImGui::DockBuilderSplitNode(centerWithBottomPanelID, ImGuiDir_Up, 0.7f, &centerID, &underViewportID);
            ImGui::DockBuilderDockWindow("Viewport", centerID);
            ImGui::DockBuilderDockWindow(Config::sc_ShaderEditorWindowName.data(), centerID);
            ImGui::DockBuilderDockWindow(Config::sc_TilemapEditorWindowName.data(), centerID);
            ImGui::DockBuilderDockWindow(Config::sc_TilesetEditorWindowName.data(), centerID);
            ImGui::DockBuilderDockWindow(Config::sc_AnimationEditorWindowName.data(), centerID);
            ImGui::DockBuilderDockWindow("Dear ImGui Demo", centerID);
            ImGui::DockBuilderDockWindow("Dear ImGui Metrics/Debugger", centerID);


            ImGuiID bottomRightSideNextToRightPanelID;
            ImGuiID bottomLeftSideID;
            ImGui::DockBuilderSplitNode(underViewportID, ImGuiDir_Left, 0.75f, &bottomLeftSideID, &bottomRightSideNextToRightPanelID);

            ImGui::DockBuilderDockWindow(Config::sc_PerformanceWindowName.data(), bottomRightSideNextToRightPanelID);

            ImGui::DockBuilderDockWindow(Config::sc_AssetBrowserWindowName.data(), bottomLeftSideID);
            ImGui::DockBuilderDockWindow(Config::sc_LogWindowName.data(), bottomLeftSideID);
            ImGui::DockBuilderDockWindow(Config::sc_AnimationTimelineWindowName.data(), bottomLeftSideID);
        }
    }
}