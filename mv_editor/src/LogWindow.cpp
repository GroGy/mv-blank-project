//
// Created by Matty on 2021-10-24.
//

#include "../editor/interface/LogWindow.h"
#include "../editor/imgui/imgui.h"
#include "../editor/EditorCommon.h"

namespace Editor {
    MV::rc<LogWindow> LogWindow::s_Instance{};

    LogWindow::LogWindow() : Window(Config::sc_LogWindowName.data()) {

    }

    void LogWindow::WindowSetup() {

    }

    void LogWindow::Render() {
        if (ImGui::Button("Clear")) Clear();
        ImGui::SameLine();
        bool copy = ImGui::Button("Copy");
        ImGui::SameLine();
        m_pFilter.Draw("Filter", -100.0f);
        ImGui::Separator();
        auto avaContent = ImGui::GetContentRegionAvail();
        ImGui::SetNextItemWidth(avaContent.x);
        ImGui::BeginChild("scrolling##log");
        ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0, 1));
        if (copy) ImGui::LogToClipboard();

        if (m_pFilter.IsActive()) {
            const char* buf_begin = m_pBuffer.begin();
            const char* line = buf_begin;
            for (int line_no = 0; line!=NULL; line_no++) {
                const char* line_end = (line_no<m_pLineOffsets.Size) ? buf_begin+m_pLineOffsets[line_no] : NULL;
                if (m_pFilter.PassFilter(line, line_end))
                    ImGui::TextUnformatted(line, line_end);
                line = line_end && line_end[1] ? line_end+1 : NULL;
            }
        }
        else {
            if (m_pScrollToBottom)
                ImGui::SetNextWindowScroll({0.5f,1.0f});
            ImGui::InputTextMultiline("##_log_box", (char*)m_pBuffer.begin(), m_pBuffer.size(), { 0,avaContent.y }, ImGuiInputTextFlags_ReadOnly);
            //ImGui::TextUnformatted(Buf.begin());
        }

        m_pScrollToBottom = false;
        ImGui::PopStyleVar();
        ImGui::EndChild();
    }

    void LogWindow::PostRender() {

    }

    void LogWindow::OnCleanup() {

    }

    void LogWindow::OnInit() {

    }

    void LogWindow::AddLog(MV::string_view message) {
        int old_size = m_pBuffer.size();
        m_pBuffer.append(message.data());
        for (int new_size = m_pBuffer.size(); old_size < new_size; old_size++)
            if (m_pBuffer[old_size] == '\n')
                m_pLineOffsets.push_back(old_size);
        m_pScrollToBottom = true;
    }

    void LogWindow::Clear() {
        m_pBuffer.clear();
        m_pLineOffsets.clear();
        m_pFilter.Clear();
    }
}
