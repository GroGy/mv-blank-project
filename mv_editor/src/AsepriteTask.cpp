//
// Created by Matty on 2021-10-28.
//

#include <common/EngineCommon.h>
#include <json.hpp>
#include <filesystem>
#include "../editor/async/AsepriteTask.h"
#include "../editor/resources/AtlasProducer.h"
#include "../editor/resources/ProjectProducer.h"
#include "../editor/EditorCommon.h"
#include "../editor/resources/ProducerUtils.h"
#include "common/Assert.h"

namespace Editor {
    void AsepriteTask::Process() {
        try {
            m_pProgress = 0.2f;
            m_pProgress = 0.4f;
            nlohmann::json input = importJson();
            m_pProgress = 0.6f;
            processJson(input);
            m_pProgress = 0.8f;
        } catch(std::exception &e) {
            m_pProgress = 1.0f;
            m_pSuccess = false;
            return;
        }

        m_pProgress = 1.0f;
        m_pSuccess = true;
    }

    EditorTaskFinishResult AsepriteTask::Finish() {
        if (!m_pSuccess) {
            m_pDone = true;
            return EditorTaskFinishResult::FAILED;
        }


        m_pDone = true;
        return EditorTaskFinishResult::SUCCESS;
    }

    AsepriteTask::AsepriteTask(MV::string dataPath) : m_pDataPath(std::move(dataPath)) {

    }

    nlohmann::json AsepriteTask::importJson() {
        nlohmann::ordered_json res;

        std::ifstream i(m_pDataPath.c_str());
        if (i.is_open()) {
            i >> res;
            i.close();
            return res;
        }

        throw std::exception();
    }

    void AsepriteTask::processJson(nlohmann::json &json) {
        if (!json.contains(ASEPRITE_JSON_FRAMES_FIELD)) throw std::exception(); // Must have frames field
        if (!json.contains(ASEPRITE_JSON_METADATA_FIELD)) throw std::exception(); // Must have metadata

        auto &framesJson = json[ASEPRITE_JSON_FRAMES_FIELD];
        auto &metadataJson = json[ASEPRITE_JSON_METADATA_FIELD];

        if (!metadataJson.contains(ASEPRITE_JSON_FRAME_TOTAL_SIZE_FIELD))
            throw std::exception(); // We need total size from aseprite json

        glm::uvec2 totalSize{metadataJson[ASEPRITE_JSON_FRAME_TOTAL_SIZE_FIELD][ASEPRITE_JSON_WIDTH_FIELD].get<uint32_t>(),
                            metadataJson[ASEPRITE_JSON_FRAME_TOTAL_SIZE_FIELD][ASEPRITE_JSON_HEIGHT_FIELD].get<uint32_t>()};

        auto frameCount = framesJson.size();

        m_Result.m_Sprites.resize(frameCount);

        uint32_t index = 0;

        if(framesJson.is_array()) {
            for (auto &&val: framesJson) {
                processFrame(val, index, totalSize);
            }
        } else {
            for (auto &&val: framesJson.items()) {
                processFrame(val.value(), index, totalSize, val.key().c_str());
            }
        }
    }

    void AsepriteTask::processFrame(const nlohmann::json& val,uint32_t& index,const glm::uvec2& totalSize, const MV::string& frameName)
    {
        if (!val.contains(ASEPRITE_JSON_FRAME_UV_FIELD)) throw std::exception(); // We need uv data
        if (!val.contains(ASEPRITE_JSON_FRAME_DURATION_FIELD)) throw std::exception(); // We need duration
        if (!val.contains(ASEPRITE_JSON_FRAME_OFFSET_FIELD)) throw std::exception(); // We need frame offset

        auto &uvJson = val[ASEPRITE_JSON_FRAME_UV_FIELD];
        auto durationSrc = val[ASEPRITE_JSON_FRAME_DURATION_FIELD].get<uint32_t>();
        //m_Result.m_Timings[index] = static_cast<float>(durationSrc) / 1000.0f;
        auto &offsetJson = val[ASEPRITE_JSON_FRAME_OFFSET_FIELD];

        float uvX = uvJson[ASEPRITE_JSON_X_FIELD].get<float>() / totalSize.x;
        float uvY = uvJson[ASEPRITE_JSON_Y_FIELD].get<float>() / totalSize.y;
        float uvWidth = uvJson[ASEPRITE_JSON_WIDTH_FIELD].get<float>() / totalSize.x;
        float uvHeight = uvJson[ASEPRITE_JSON_HEIGHT_FIELD].get<float>() / totalSize.y;

        const auto & sizeJson = val[ASEPRITE_JSON_SIZE_FIELD];

        float sourceSizeX = sizeJson[ASEPRITE_JSON_WIDTH_FIELD].get<float>();
        float sourceSizeY = sizeJson[ASEPRITE_JSON_HEIGHT_FIELD].get<float>();

        float offsetXmin = offsetJson[ASEPRITE_JSON_X_FIELD].get<float>();
        float offsetYmin = offsetJson[ASEPRITE_JSON_Y_FIELD].get<float>();
        float sizeX = offsetJson[ASEPRITE_JSON_WIDTH_FIELD].get<float>();
        float sizeY = offsetJson[ASEPRITE_JSON_HEIGHT_FIELD].get<float>();

        auto& result = m_Result.m_Sprites[index];

        result.m_UV = {
                uvX,
                uvY,
                uvX + uvWidth,
                uvY + uvHeight
        };

        //m_Result.m_UVOffsets[index] = glm::vec4{
        //        offsetXmin / sourceSizeX,
        //        offsetYmin / sourceSizeY,
        //        (offsetXmin + sizeX) / sourceSizeX,
        //        (offsetYmin + sizeY) / sourceSizeY
        //};

        result.m_Name =  frameName.empty() ? val[ASEPRITE_JSON_FRAME_NAME_FIELD].get<MV::string>() : frameName;

        index++;
    }
}
