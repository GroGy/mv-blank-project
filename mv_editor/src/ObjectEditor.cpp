//
// Created by Matty on 2022-02-13.
//

#include "interface/ObjectEditor.h"
#include "common/EngineCommon.h"
#include "EditorCommon.h"
#include <util/Container.h>

#include "2d/TilemapProperty.h"
#include "interface/object_editor/common/FieldRenderer.h"
#include "interface/object_editor/PropertyFieldRenderer.h"
#include "interface/object_editor/NumericFieldRenderers.h"
#include "interface/object_editor/GeneralFieldRenderers.h"
#include "meta/MetaCache.h"
#include "precompiler/PropertyFieldRendererRegistrator.h"
#include "util/UtilGui.h"

namespace Editor
{
    ObjectEditor::ObjectEditor() : Window(Config::sc_ObjectEditorWindowName.data(), false)
    {
        m_Flags |= ImGuiWindowFlags_NoCollapse;
    }

    void ObjectEditor::OnCleanup()
    {

    }

    void ObjectEditor::OnInit()
    {
    }

    void ObjectEditor::WindowSetup()
    {

    }

    void ObjectEditor::Render()
    {
        MV::AllocatorTagScope allocTag{MV::AllocationType::Editor};
        auto& world = MV::GetEngine()->GetWorld();

        if (!world.IsValid())
            return;

        uint64_t id = MV::GetEngine()->IsGameRunning() ? GetEditor()->m_SelectedWO_Runtime : GetEditor()->m_SelectedWO;

        ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing,
                            ImVec2{ImGui::GetStyle().ItemSpacing.x / 2, ImGui::GetStyle().ItemSpacing.y});
        drawObjectSettings(id);
        ImGui::PopStyleVar();
    }

    void ObjectEditor::PostRender()
    {

    }

    bool ObjectEditor::ShouldRender() const
    {
        const auto runtimeId = GetEditor()->m_SelectedWO_Runtime;
        const auto editorId = GetEditor()->m_SelectedWO;

        return MV::GetEngine()->IsGameRunning() ?  runtimeId != 0 : editorId != 0;
    }

    void ObjectEditor::drawObjectSettings(uint64_t id)
    {
        drawObjectSettingsData(id);
    }

    void ObjectEditor::drawObjectSettingsData(uint64_t id)
    {
        auto& world = MV::GetEngine()->GetWorld();

        if(!MV::IsGameRunning()) {
            const auto foundRes = eastl::find_if(world.m_WorldData.m_WorldObjects.begin(), world.m_WorldData.m_WorldObjects.end(),[id](const MV::WorldObjectData& d){
                return d.m_WorldID == id;
            });

            if (foundRes == world.m_WorldData.m_WorldObjects.end())
            {
                ImGui::Text("Object not found. ID: %d", id);
                return;
            }
        } else {
            const auto foundRes = world.m_WorldObjects.find(id);

            if (foundRes == world.m_WorldObjects.end())
            {
                ImGui::Text("Object not found. ID: %d", id);
                return;
            }
        }


        const auto wo = world.m_WorldObjects[id];

        uint32_t counter = 0;

        ImGui2::LabeledSeparator("General");

        // First render engine defined fields
        drawGeneralWorldObjectFields(world, wo);

        ImGui2::LabeledSeparator("Physics");

        // Second render physics settings
        drawObjectPhysicsSettings(world, wo);

        ImGui2::LabeledSeparator("Fields");

        if(m_pCachedObjectId != id) {
            m_pCachedObjectId = id;
            m_pFetchedData = false;
        }

        if (!m_pFetchedData) {
            auto woTypeHash = wo->GetTypeHash();

            auto& inst = MV::Meta::MetaCache::GetInstance();

            m_pCachedMeta = inst.GetClassMetaFromHash(woTypeHash);
            m_pFetchedData = true;
        }

        MV::vector<MV::Meta::Field> fields;

        if(m_pCachedMeta.IsValid()) {
            fields = m_pCachedMeta.GetFields();
        }

        // Then user defined fields
        if (ImGui::BeginTable("##selected_object_fields", 2, ImGuiTableFlags_NoSavedSettings))
        {
            ImGui::TableNextRow();

            for(auto & f : fields)
            {
                if (f.m_Type != MV::Meta::FieldType::Primitive)
                {
                    continue;
                }

                if (!f.m_ShowInEditor)
                {
                    continue;
                }

                if (FieldRendererProxy::Draw(f, counter, wo, m_pCachedMeta))
                {
                    if (!MV::GetEngine()->IsGameRunning())
                        GetEditor()->m_DirtyAssets.emplace(world.m_ID);
                }

            }

            ImGui::EndTable();
        }

        // Properties are renderer after all other fields

        ImGui2::LabeledSeparator("Properties");


        bool first = true;
        for (auto& v: fields)
        {
            if(v.m_Type != MV::Meta::FieldType::Property) {
                continue;
            }

            if(first) {
                first = false;
            } else {
                ImGui::Separator();
            }
            if (FieldRenderer<PropertyType>::DrawProperty(EditorPropertyDrawingData::FromField(v), counter, wo, wo.get(), m_pCachedMeta))
            {
                GetEditor()->m_DirtyAssets.emplace(world.m_ID);
            }
        }
    }

    void
    ObjectEditor::drawGeneralWorldObjectFields(MV::World& world, const eastl::shared_ptr<MV::WorldObject>& wo) const
    {
        bool dirty = false;

        if (ImGui::BeginTable("##selected_object_engine_fields", 2, ImGuiTableFlags_NoSavedSettings))
        {
            ImGui::TableNextRow();

            static EngineInternalFieldRendererHelper position_frh{
                    [&](MV::string_view fieldname_, const MV::rc<MV::EngineObject>& obj_, const MV::Meta::Class& klass)
                    {
                        auto wo_ = MV::static_pointer_cast<MV::WorldObject>(obj_);
                        glm::vec2 pos = wo_->GetPosition();
                        const bool changed = FieldDrawingImpl::DrawVec2(fmt::format("##{}", fieldname_).c_str(), pos);

                        if (changed)
                        {
                            wo_->SetPosition(pos);
                            /*
                            if (!MV::GetEngine()->IsGameRunning())
                                w_.m_WorldObjects[wo_->m_WorldID].m_Position = pos;*/
                        }

                        return changed;
                    }};
            dirty |= position_frh.Draw("Position", wo, {});

            static EngineInternalFieldRendererHelper rotation_frh{
                    [&](MV::string_view fieldname_, const MV::rc<MV::EngineObject>& obj_, const MV::Meta::Class& klass)
                    {
                        auto wo_ = MV::static_pointer_cast<MV::WorldObject>(obj_);
                        float rot = wo_->GetRotation();
                        const bool changed = FieldDrawingImpl::DrawFloat(fmt::format("##{}", fieldname_).c_str(), rot);

                        if (changed)
                        {
                            wo_->SetRotation(rot);/*
                            if (!MV::GetEngine()->IsGameRunning())
                                w_.m_WorldObjects[wo_->m_WorldID].m_Rotation = rot;*/
                        }

                        return changed;
                    }};
            dirty |= rotation_frh.Draw("Rotation", wo, {});

            static EngineInternalFieldRendererHelper scale_frh{
                    [&](MV::string_view fieldname_, const MV::rc<MV::EngineObject>& obj_, const MV::Meta::Class& klass)
                    {
                        auto wo_ = MV::static_pointer_cast<MV::WorldObject>(obj_);
                        glm::vec2 scale = wo_->GetScale();
                        const bool changed = FieldDrawingImpl::DrawVec2(fmt::format("##{}", fieldname_).c_str(), scale);

                        if (changed)
                        {
                            wo_->SetScale(scale);
                        }

                        return changed;
                    }};
            dirty |= scale_frh.Draw("Scale", wo, {});

            static EngineInternalFieldRendererHelper tag_frh{
                    [&](MV::string_view fieldname_, const MV::rc<MV::EngineObject>& obj_, const MV::Meta::Class& klass)
                    {
                        auto wo_ = MV::static_pointer_cast<MV::WorldObject>(obj_);
                        const bool changed = FieldDrawingImpl::DrawString(fmt::format("##{}", fieldname_).c_str(),
                                                                          wo_->m_Tag);
/*
                        if (changed && !MV::GetEngine()->IsGameRunning())
                            w_.m_WorldObjects[wo_->m_WorldID].m_Tag = wo_->m_Tag;*/

                        return changed;
                    }};
            dirty |= tag_frh.Draw("Tag", wo, {});


            ImGui::EndTable();
        }

        if (dirty)
            GetEditor()->m_DirtyAssets.emplace(world.m_ID);
    }

    void ObjectEditor::drawObjectPhysicsSettings(MV::World& world, const MV::rc<MV::WorldObject>& wo)
    {
        bool dirty = false;
        if (ImGui::BeginTable("##selected_object_physics_fields", 2, ImGuiTableFlags_NoSavedSettings))
        {
            ImGui::TableNextRow();

            {
                static EngineInternalFieldRendererHelper static_frh{
                        [&](MV::string_view fieldname_, const MV::rc<MV::EngineObject>& obj_, const MV::Meta::Class& klass)
                        {
                            auto wo_ = MV::static_pointer_cast<MV::WorldObject>(obj_);
                            const bool changed = FieldDrawingImpl::DrawBool(fmt::format("##{}", fieldname_).c_str(),
                                                                            wo_->m_IsStatic);

                            if (MV::GetEngine()->IsGameRunning() && wo_->GetPhysicsBody())
                                wo_->GetPhysicsBody()->SetType(wo_->m_IsStatic ? b2_staticBody : b2_dynamicBody);

                            return changed;
                        }};
                dirty |= static_frh.Draw("Static body", wo, {});
            }

            {
                static EngineInternalFieldRendererHelper fixed_rotation_frh{
                        [&](MV::string_view fieldname_, const MV::rc<MV::EngineObject>& obj_, const MV::Meta::Class& klass)
                        {
                            auto wo_ = MV::static_pointer_cast<MV::WorldObject>(obj_);
                            const bool changed = FieldDrawingImpl::DrawBool(fmt::format("##{}", fieldname_).c_str(),
                                                                            wo_->m_FixedRotation);

                            if (MV::GetEngine()->IsGameRunning() && wo_->GetPhysicsBody())
                                wo_->GetPhysicsBody()->SetFixedRotation(wo_->m_FixedRotation);

                            return changed;
                        }};
                dirty |= fixed_rotation_frh.Draw("Fixed rotation", wo, {});
            }

            {
                static EngineInternalFieldRendererHelper linearDampening_frh{
                        [&](MV::string_view fieldname_, const MV::rc<MV::EngineObject>& obj_, const MV::Meta::Class& klass)
                        {
                            auto wo_ = MV::static_pointer_cast<MV::WorldObject>(obj_);
                            const bool changed = FieldDrawingImpl::DrawFloatWithOverride(fmt::format("##{}", fieldname_).c_str(),
                                                                             wo_->m_LinearDamping, wo_->m_OverrideLinearDamping);

                            if (MV::GetEngine()->IsGameRunning() && wo_->GetPhysicsBody())
                                wo_->GetPhysicsBody()->SetLinearDamping(wo_->m_LinearDamping);

                            return changed;
                        }};

                dirty |= linearDampening_frh.Draw("Linear Dampening", wo, {});
            }

            {
                static EngineInternalFieldRendererHelper angularDampening_frh{
                        [&](MV::string_view fieldname_, const MV::rc<MV::EngineObject>& obj_, const MV::Meta::Class& klass)
                        {
                            auto wo_ = MV::static_pointer_cast<MV::WorldObject>(obj_);
                            const bool changed = FieldDrawingImpl::DrawFloatWithOverride(fmt::format("##{}", fieldname_).c_str(),
                                                                             wo_->m_AngularDamping, wo_->m_OverrideAngularDamping);

                            if (MV::GetEngine()->IsGameRunning() && wo_->GetPhysicsBody())
                                wo_->GetPhysicsBody()->SetAngularDamping(wo_->m_AngularDamping);

                            return changed;
                        }};

                dirty |= angularDampening_frh.Draw("Angular Dampening", wo, {});
            }

            ImGui::EndTable();
        }

        if (dirty)
        {
            GetEditor()->m_DirtyAssets.emplace(world.m_ID);
        }
    }
}