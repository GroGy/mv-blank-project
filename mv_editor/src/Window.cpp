//
// Created by Matty on 2021-10-15.
//

#include "imgui/imgui_internal.h"
#define IMGUI_DEFINE_MATH_OPERATORS
#include "../editor/interface/common/Window.h"
#include "../editor/imgui/imgui.h"

namespace Editor {
    Window::Window(MV::string && name, bool showInMenu) : m_pName(std::move(name)) , m_pShowInMenu(showInMenu) {
    };

    void Window::OnRender() {
        if(!m_pOpen) return;
        if(!ShouldRender()) return;

        WindowSetup();

        auto openBefore = m_pOpen;

        if(ImGui::Begin(m_pName.c_str(), &m_pOpen, m_Flags | (m_pShowDirtyDot ? ImGuiWindowFlags_UnsavedDocument : 0))) {
            if(m_pNewlyCreated) {
                m_pSize = {ImGui::GetWindowSize().x, ImGui::GetWindowSize().y};
                OnResize();
                m_pNewlyCreated = false;
            }

            Render();

            if(!MV::IsGameRunning() && GetIsFocused()) {
                ProcessInput();
            }
        } else {
            NoRender();
        }

        auto openAfter = m_pOpen;

        if(openBefore != openAfter) {
            OnOpenStateChanged(openAfter);
            if(openAfter) {
                m_pNewlyCreated = true;
            }
        }

        ImGui::End();

        PostRender();
    }

    bool Window::GetIsFocused() const
    {
        return ImGui::IsWindowFocused(ImGuiFocusedFlags_ChildWindows);
    }

    void Window::FocusInDock()
    {
        ImGuiWindow* window = ImGui::FindWindowByName(m_pName.data());
        if (window == NULL || window->DockNode == NULL || window->DockNode->TabBar == NULL)
            return;
        window->DockNode->TabBar->NextSelectedTabId = window->TabId;
    };
}