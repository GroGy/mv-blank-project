//
// Created by Matty on 2022-07-12.
//

#include "../editor/properties/PropertyGizmoManager.h"
#include "common/EngineCommon.h"
#include "../editor/properties/viewport_renderers/PropertyBoxColliderRenderer.h"
#include "meta/MetaCache.h"
#include "precompiler/PropertyGizmoRegistrator.h"
#include "Engine.h"

namespace Editor
{
    void PropertyGizmoManager::Init()
    {
        const auto& registeredRenderers = RegisteredPropertyGizmos::GetInstance();
    }

    void PropertyGizmoManager::DrawProperties(const glm::vec4& viewport, const MV::rc<MV::WorldObject>& worldObject,
                                              const MV::unordered_map<MV::string, MV::Meta::Field>* fieldsOverride)
    {
        if (!worldObject) return;
        const auto& registeredRenderers = RegisteredPropertyGizmos::GetInstance();

        const auto& metaCache = MV::Meta::MetaCache::GetInstance();

        const auto typeHash = worldObject->GetTypeHash();

        const auto meta = metaCache.GetClassMetaFromHash(typeHash);

        if (!meta.IsValid())
        {
            return;
        }

        //const MV::unordered_map<MV::string, MV::Meta::Field>* fieldsPtr = fieldsOverride ? fieldsOverride : &meta.GetFields();

        for (auto&& field: meta.GetFields())
        {
            if (field.m_Type != MV::Meta::FieldType::Property)
            {
                continue;
            }
            const auto foundRes = registeredRenderers.m_GizmoLookup.find(field.m_ClassHash);

            if (foundRes != registeredRenderers.m_GizmoLookup.end())
            {
                MV::Property* property = nullptr;
                if(!meta.GetPropertyField(worldObject.get(), field, property)) {
                    continue;
                }

                if(!property) {
                    continue;
                }

                //Callback exists
                foundRes->second.m_DrawCallback(viewport,property);

            }
        }
    }
}