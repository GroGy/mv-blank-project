//
// Created by Matty on 2021-10-28.
//

#include <common/EngineCommon.h>
#include "../editor/async/TextureTask.h"
#include "../editor/resources/TextureProducer.h"
#include "../editor/resources/ProducerUtils.h"
#include "../editor/resources/ProjectProducer.h"
#include "../editor/EditorCommon.h"

namespace Editor {
    EditorTaskFinishResult TextureTask::Finish() {
        if(!m_pSuccess) {
            m_pDone = true;
            return EditorTaskFinishResult::FAILED;
        }

        if (m_pIsReimport)
        {
            MV::GetEngine()->m_ResourceManager.UnloadAsset(m_pReimportAsset);
            m_pDone = true;
            return EditorTaskFinishResult::SUCCESS;
        }

        auto &proj = MV::GetEngine()->GetProject();

        for (auto &m_pResultPath: m_pResultPaths) {
            auto assetId = Producer::ProducerUtils::GenerateUUID();

            MV::ProjectAsset v;

            auto& asset = proj.m_Assets[assetId];
            asset.m_Path = Producer::ProducerUtils::PathAbsoluteToObjectRelative(m_pResultPath.data());
            asset.m_Type = MV::AssetTypeRegistry::Get().GetRuntimeAssetType<MV::TextureAssetType>();
        }

        ProjectProducer::SaveProject(proj, GetEditor()->GetLoadedProjectPath());

        m_pDone = true;
        return EditorTaskFinishResult::SUCCESS;
    }

    void TextureTask::Process() {
        if(m_pIsReimport && m_pReimportTexturePath.empty()) { // Failed to start
            m_pProgress = 0.0f;
            m_pSuccess = false;
            return;
        }

        auto progressPerTexture = 1.0f / static_cast<float>(m_pTexturePaths.size());

        try {
            if(m_pIsReimport) {
                m_pResultPaths.resize(1);
                m_pResultPaths[0] = TextureProducer::CreateTextureResourceToFile(m_pTexturePaths[0], m_pReimportTexturePath, true);
            } else {
                m_pResultPaths.resize(m_pTexturePaths.size());

                for (uint32_t i = 0; i < m_pTexturePaths.size(); i++) {
                    m_pResultPaths[i] = TextureProducer::CreateTextureResource(m_pTexturePaths[i], m_pDirectory);
                    m_pProgress += progressPerTexture;
                }
            }

            m_pProgress = 1.0f;
            m_pSuccess = true;
        } catch(std::exception &e) {
            (void)e;

            m_pProgress = 0.0f;
            m_pSuccess = false;
            m_pErrorText = e.what();
        }
    }

    TextureTask::TextureTask(const MV::vector<MV::string>& textures, MV::string_view resultDirectory) : m_pTexturePaths(textures), m_pDirectory(resultDirectory), m_pIsReimport(false) {

    }

    TextureTask::TextureTask(const MV::string& textureReimportPath, MV::UUID textureToReimport) : m_pIsReimport(true), m_pReimportAsset(textureToReimport)
    {
        m_pTexturePaths.emplace_back(textureReimportPath);

        const auto& assets = MV::GetEngine()->GetProject().m_Assets;

        const auto foundRes = assets.find(textureToReimport);

        if(foundRes == assets.end()) {
            m_pReimportTexturePath.clear();
            m_pErrorText = "Couldn't find targeted reimport texture asset";
            m_pSuccess = false;
            return;
        }

        m_pReimportTexturePath = foundRes->second.m_Path;
    }
}