//
// Created by Matty on 08.12.2022.
//

#include "resources/TilemapProducer.h"
#include "../editor/resources/ProducerUtils.h"
#include "serialization/binary/BinaryArchiveProcessor.h"
#include "serialization/common/SerializationWhitelist.h"
#include "serialization/common/ClassSerializer.h"
#include <resources/common/AssetType.h>

namespace Editor {
    MV::string TilemapProducer::CreateTilemapResource(const MV::TilemapData &data, const MV::string &path, bool overwrite) {
        using namespace Producer;

        auto finalPath = ProducerUtils::ResolvePath(path, overwrite);

        MV::Archive ar;
        MV::SerializationWhitelist wl;

        const auto res = MV::ClassSerializer<MV::TilemapData>::Write(data, ar, wl);

        if(!res) return "";

        MV::BinaryArchiveProcessor proc;

        auto writeRes = proc.WriteToFile(finalPath, ar);

        if(writeRes != MV::ArchiveProcessor::Ok) {
            return "";
        }

        return finalPath;
    }
}
