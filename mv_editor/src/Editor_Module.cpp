//
// Created by Matty on 2021-10-01.
//

#include <common/EngineCommon.h>

#include <utility>
#include "Editor_Module.h"
#include "imgui/imgui_impl_glfw.h"
#include "interface/Performance.h"
#include "tools/shader_editor/ShaderEditor.h"
#include "../vendor/stb_image.h"

bool Editor::Editor_Module::OnInit() {
    m_pEditorCallbacks.onInit();
    return true;
}

void Editor::Editor_Module::OnUpdate(float deltaTime) {
    m_pEditorCallbacks.onUpdate(deltaTime);
}

void Editor::Editor_Module::OnRender() {
    m_pEditorCallbacks.onRender();
}

bool Editor::Editor_Module::OnCleanup() {
    return m_pEditorCallbacks.onCleanup();
}

MV::string Editor::Editor_Module::GetModuleName() const {
    return "Editor";
}

bool Editor::Editor_Module::OnPostInit() {
    m_pEditorCallbacks.onPostInit();
    return true;
}

bool Editor::Editor_Module::OnRenderCleanup() {
    return m_pEditorCallbacks.onRendererCleanup();
}

void Editor::Editor_Module::OnEditorRender() {
    m_pEditorCallbacks.onEditorRender();
}

void Editor::Editor_Module::OnWindowResize() {
    m_pEditorCallbacks.onWindowResize();
}

Editor::Editor_Module::Editor_Module(Editor::EditorCallbacks editor) : m_pEditorCallbacks(std::move(editor)) {

}
