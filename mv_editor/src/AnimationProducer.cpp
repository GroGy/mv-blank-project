//
// Created by Matty on 19.08.2023.
//

#include "resources/AnimationProducer.h"
#include "resources/ProducerUtils.h"
#include "precompiler/AnimatablePropertyRegistrator.h"
#include "serialization/binary/BinaryArchiveProcessor.h"

namespace Editor {
    MV::string AnimationProducer::CreateAnimationResource(MV::AnimationData& data, const MV::string& path, bool override)
    {
        using namespace Producer;

        auto finalPath = ProducerUtils::ResolvePath(path, override);

        MV::Archive ar;
        MV::SerializationWhitelist wl;

        const auto res = MV::ClassSerializer<MV::AnimationData>::Write(data, ar, wl);

        if(!res) return "";

        MV::BinaryArchiveProcessor proc;

        auto writeRes = proc.WriteToFile(finalPath, ar);

        if(writeRes != MV::ArchiveProcessor::Ok) {
            return "";
        }

        return finalPath;
    }
}