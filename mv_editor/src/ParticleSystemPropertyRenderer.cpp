//
// Created by Matty on 2022-09-20.
//

#include <interface/worldobject_renderers/ParticleSystemPropertyRenderer.h>
#include <EngineMinimal.h>
#include <util/UtilGui.h>
#include <particles/cpu/ParticleSystemProperty.h>
#include "EditorCommon.h"
#include "imgui/imgui.h"
#include "interface/common/AssetSelector.h"
#include "interface/object_editor/NumericFieldRenderers.h"
#include "interface/object_editor/GeneralFieldRenderers.h"
#include "interface/common/GradientEditor.h"
#include "tools/particle_editor/ParticleEditor.h"

namespace Editor
{
    static GradientEditor s_GradientEditor;

    void ParticleSystemPropertyRenderer::drawSpriteRenderingFields(bool& dirty, MV::Property* dataIn)
    {
        auto data = (MV::ParticleSystemProperty*) (dataIn);

        if (BeginField("Offset:"))
        {
            dirty |= FieldDrawingImpl::DrawVec2("##particle_system_sprite_offset",
                                                data->m_RenderingSettings.m_SpriteOffset);
            EndField();
        }

        if (BeginField("Size:"))
        {
            dirty |= FieldDrawingImpl::DrawVec2("##particle_system_sprite_size",
                                                data->m_RenderingSettings.m_SpriteSize);
            EndField();
        }
    }

    void ParticleSystemPropertyRenderer::drawSpriteSheetRenderingFields(bool& dirty, MV::Property* dataIn)
    {
        auto data = (MV::ParticleSystemProperty*) (dataIn);

        if (BeginField("Sprite duration:"))
        {
            dirty |= FieldDrawingImpl::DrawFloat("##particle_system_spritesheet_sprite_len",
                                                 data->m_RenderingSettings.m_SpriteDuration);
            EndField();
        }

        if (BeginField("Sprite count:"))
        {
            auto prev = data->m_RenderingSettings.m_SpriteCount;
            dirty |= FieldDrawingImpl::DrawUVec2("##particle_system_spritesheet_count",
                                                 data->m_RenderingSettings.m_SpriteCount);


            if(data->m_RenderingSettings.m_SpriteCount.x == 0 || data->m_RenderingSettings.m_SpriteCount.y == 0)
                data->m_RenderingSettings.m_SpriteCount = prev;

            EndField();
        }

        if (BeginField("Sprite order:"))
        {
            uint32_t selectedV = static_cast<uint32_t>(data->m_RenderingSettings.m_SpriteSheetOrder);

            static const char* orders[4] = {
                    "By rows",
                    "By columns",
                    "By rows (Reversed)",
                    "By columns (Reversed)",
            };

            const bool changed = FieldDrawingImpl::DrawCombo("##particle_system_spritesheet_order", orders, 4,
                                                             selectedV);

            if (changed)
            {
                dirty = true;
                data->m_RenderingSettings.m_SpriteSheetOrder = static_cast<MV::ParticleTextureSpriteSheetOrder>(selectedV);
            }
            EndField();
        }


        if (BeginField("Use reduced number of sprites:"))
        {
            dirty |= FieldDrawingImpl::DrawBool("##particle_system_spritesheet_reduced_count",
                                                data->m_RenderingSettings.m_UseReducedNumberOfSprites);
            EndField();
        }

        {
            ScopedDisabled dis{!data->m_RenderingSettings.m_UseReducedNumberOfSprites};

            if (BeginField("Total count:"))
            {
                auto prev = data->m_RenderingSettings.m_SpriteCountTotal;
                dirty |= FieldDrawingImpl::DrawU32("##particle_system_spritesheet_total_count",
                                                   data->m_RenderingSettings.m_SpriteCountTotal);

                const auto count = data->m_RenderingSettings.m_SpriteCount;

                if(data->m_RenderingSettings.m_SpriteCountTotal == 0)
                    data->m_RenderingSettings.m_SpriteCountTotal = prev;

                if (data->m_RenderingSettings.m_SpriteCountTotal > count.x * count.y)
                    data->m_RenderingSettings.m_SpriteCountTotal = count.x * count.y;

                EndField();
            }
        }
    }

    bool ParticleSystemPropertyRenderer::Draw(const MV::Meta::Field* field, const MV::rc<MV::WorldObject>& wo,
                                              MV::Property* property)
    {
        auto data = dynamic_cast<MV::ParticleSystemProperty*>(property);

        ENSURE_VALID(data, false);
        bool dirty = s_GradientEditor.Draw(fmt::format("Color over life editor##{}", ImGui::GetID("hidden")).c_str());


        ImGui::Text("Particle count: %d", data->GetActiveParticles());

        if (data->m_ShowOpenEditorButton)
        {
            ImGui::SameLine(ImGui::GetContentRegionAvail().x - ImGui::CalcTextSize("Open in editor").x -
                            ImGui::GetStyle().FrameBorderSize);

            ScopedDisabled dis{field == nullptr};
            if (ImGui::Button("Open in editor"))
            {
                auto particleEditor = GetEditor()->GetWindow<ParticleEditor>();
                particleEditor->SetParticleSystem(wo->GetOwningWorld(), wo, *field);
                particleEditor->SetOpen(true);
                particleEditor->SetOnSavedCallback([id(wo->GetOwningWorld()->m_ID)](){
                    GetEditor()->m_DirtyAssets.emplace(id);
                });
                ImGui::SetWindowFocus(Config::sc_ParticleEditorWindowName.data());
            }
        }

        if (ImGui::TreeNodeEx("General settings", ImGuiTreeNodeFlags_CollapsingHeader))
        {
            if (!Begin("##particle_system_general_settings"))
            {
                return false;
            }

            if (BeginField("Layer:"))
            {
                dirty |= FieldDrawingImpl::DrawI32("##particle_layer", data->m_Layer);
                EndField();
            }

            if (BeginField("Buffer size:"))
            {
                dirty |= FieldDrawingImpl::DrawU32("##max_particles", data->m_GeneralSettings.m_MaxParticles);
                EndField();
            }

            if (BeginField("Coordinate Space:"))
            {
                static const char* spaces[2] = {
                        "Local",
                        "World"
                };

                auto index = static_cast<uint32_t>(data->m_GeneralSettings.m_CoordinateSpace);

                if (FieldDrawingImpl::DrawCombo("##coordinate_space_combo", spaces, 2, index))
                {
                    data->m_GeneralSettings.m_CoordinateSpace = static_cast<MV::ParticleCoordinateSpace>(index);
                    dirty = true;
                }

                EndField();
            }

            End();
        }

        if (ImGui::TreeNodeEx("Particle settings", ImGuiTreeNodeFlags_CollapsingHeader))
        {
            if (!Begin("##particle_system_particle_settings"))
            {
                return dirty;
            }

            drawParticleSettings(dirty, data);

            End();
        }

        if (ImGui::TreeNodeEx("Spawner settings", ImGuiTreeNodeFlags_CollapsingHeader))
        {
            if (!Begin("##particle_system_spawner_settigs"))
            {
                return dirty;
            }

            if (BeginField("Spawn rate:"))
            {
                dirty |= FieldDrawingImpl::DrawU32("##particle_system_property_spawn_rate",
                                                   data->m_SpawnerSettings.m_SpawnRate);
                EndField();
            }

            if (BeginField("Spawner offset:"))
            {
                dirty |= FieldDrawingImpl::DrawVec2("##particle_system_spawner_offset",
                                                    data->m_SpawnerSettings.m_SpawnerOffset);
                EndField();
            }

            if (BeginField("Spawner:"))
            {
                static const char* spaces[3] = {
                        "Point",
                        "Box",
                        "Circle"
                };

                auto index = static_cast<uint32_t>(data->m_SpawnerSettings.m_SpawnerType);

                if (FieldDrawingImpl::DrawCombo("##particle_system_spawner_type", spaces, 3, index))
                {
                    data->m_SpawnerSettings.m_SpawnerType = static_cast<MV::ParticleSpawnerType>(index);
                    dirty = true;
                }

                EndField();
            }

            switch (data->m_SpawnerSettings.m_SpawnerType)
            {
                case MV::ParticleSpawnerType::Point:
                    break;
                case MV::ParticleSpawnerType::Box:
                {
                    if (BeginField("Box scale:"))
                    {
                        dirty |= FieldDrawingImpl::DrawVec2("##particle_system_spawner_box_scale",
                                                            data->m_SpawnerSettings.m_BoxSpawnerSize);
                        EndField();
                    }

                    break;
                }
                case MV::ParticleSpawnerType::Circle:
                {
                    if (BeginField("Enable spawner range:"))
                    {
                        dirty |= FieldDrawingImpl::DrawBool("##particle_system_spawner_circle_use_range",
                                                            data->m_SpawnerSettings.m_CircleSpawnerUseRange);
                        EndField();
                    }
                    if (data->m_SpawnerSettings.m_CircleSpawnerUseRange)
                    {
                        if (BeginField("Range:"))
                        {
                            dirty |= FieldDrawingImpl::DrawVec2("##particle_system_spawner_circle_range",
                                                                data->m_SpawnerSettings.m_CircleSpawnerRange);
                            EndField();
                        }
                    } else
                    {
                        if (BeginField("Radius:"))
                        {
                            dirty |= FieldDrawingImpl::DrawFloat("##particle_system_spawner_circle_radius",
                                                                 data->m_SpawnerSettings.m_CircleSpawnerRange.y);
                            EndField();
                        }
                    }

                    if (BeginField("Angle:"))
                    {
                        dirty |= FieldDrawingImpl::DrawFloat("##particle_system_spawner_circle_angle",
                                                             data->m_SpawnerSettings.m_SpawnAngle);
                        EndField();
                    }

                    if (BeginField("Angle offset:"))
                    {
                        dirty |= FieldDrawingImpl::DrawFloat("##particle_system_spawner_circle_angle_offset",
                                                             data->m_SpawnerSettings.m_AngleOffset);
                        EndField();
                    }

                    break;
                }
            }

            End();

            {
                ScopedHorizontal hor{"##particle_spawn_velocity_wrapper"};
                if (ImGui::ArrowButton("##drop_down_spawn_vel_type", ImGuiDir_Down))
                {
                    data->m_MovementSettings.m_pUseSpawnVelocityRanges = !data->m_MovementSettings.m_pUseSpawnVelocityRanges;
                }
                ImGui::TextUnformatted("Spawn velocity:");
                ImGui::Spring();

                if (data->m_MovementSettings.m_pUseSpawnVelocityRanges)
                {
                    ScopedVertical verWrapper{"##vertical_wrapper_spawn_velocity_range"};
                    {
                        ScopedHorizontal horXWrapper{"##horizontal_wrapper_spawn_velocity_range_x"};
                        ImGui::PushItemWidth(ImGui::GetFontSize() * 4.0f);
                        bool res = ImGui::DragScalar("##vec2_a_velocity_spawn_x", ImGuiDataType_Float,
                                                     (void*) &data->m_MovementSettings.m_SpawnVelocityXRange.x);

                        ImGui::PushItemWidth(ImGui::GetFontSize() * 4.0f);
                        res |= ImGui::DragScalar("##vec2_B_velocity_spawn_x", ImGuiDataType_Float,
                                                 (void*) &data->m_MovementSettings.m_SpawnVelocityXRange.y);

                        dirty = res || dirty;
                    }
                    {
                        ScopedHorizontal horXWrapper{"##horizontal_wrapper_spawn_velocity_range_y"};
                        ImGui::PushItemWidth(ImGui::GetFontSize() * 4.0f);
                        bool res = ImGui::DragScalar("##vec2_a_velocity_spawn_y", ImGuiDataType_Float,
                                                     (void*) &data->m_MovementSettings.m_SpawnVelocityYRange.x);

                        ImGui::PushItemWidth(ImGui::GetFontSize() * 4.0f);
                        res |= ImGui::DragScalar("##vec2_B_velocity_spawn_y", ImGuiDataType_Float,
                                                 (void*) &data->m_MovementSettings.m_SpawnVelocityYRange.y);

                        dirty = res || dirty;
                    }

                } else
                {
                    float velocityX = data->m_MovementSettings.m_SpawnScaleXRange.x;
                    float velocityY = data->m_MovementSettings.m_SpawnScaleYRange.x;

                    ImGui::PushItemWidth(ImGui::GetFontSize() * 4.0f);
                    bool res = ImGui::DragScalar("##vec2_a_velocity", ImGuiDataType_Float, (void*) &velocityX);

                    ImGui::PushItemWidth(ImGui::GetFontSize() * 4.0f);
                    res |= ImGui::DragScalar("##vec2_B_velocity", ImGuiDataType_Float, (void*) &velocityY);

                    if (res)
                    {
                        data->m_MovementSettings.m_SpawnScaleXRange.x = velocityX;
                        data->m_MovementSettings.m_SpawnScaleYRange.x = velocityY;
                    }

                    dirty = res || dirty;
                }
            }

        }

        if (ImGui::TreeNodeEx("Movement settings", ImGuiTreeNodeFlags_CollapsingHeader))
        {
            drawSettingField<glm::vec2>(dirty, "##particle_system_world_force_wrapper", "World force",
                                        &data->m_MovementSettings.m_WorldForce);
        }

        if (ImGui::TreeNodeEx("Rendering settings", ImGuiTreeNodeFlags_CollapsingHeader))
        {
            if (!Begin("##particle_system_rendering_settigs"))
            {
                return dirty;
            }

            if (BeginField("Color type:"))
            {
                static const char* color_types[2] = {
                        "Constant",
                        "Gradient over lifetime"
                };

                auto selectedMode = static_cast<uint32_t>(data->m_RenderingSettings.m_ColorType);
                const bool changed = FieldDrawingImpl::DrawCombo("##particle_system_color_type",
                                                                 color_types, 2, selectedMode);

                if (changed)
                {
                    data->m_RenderingSettings.m_ColorType = static_cast<MV::ParticleColorType>(selectedMode);
                    dirty = true;
                }


                EndField();
            }

            switch (data->m_RenderingSettings.m_ColorType)
            {
                case MV::ParticleColorType::Constant:
                {
                    if (BeginField("Color:"))
                    {
                        const auto c = data->m_RenderingSettings.m_Color;
                        ImGui::TextColored({c.x, c.y, c.z, c.w}, "Color");
                        EndField();
                    }
                    break;
                }
                case MV::ParticleColorType::Gradient:
                {
                    if (BeginField("Color:"))
                    {
                        if (ImGui::GradientPreviewButton("##gradient_preview",
                                                         &data->m_RenderingSettings.m_ColorOverLifetime, {-1, 0}))
                        {
                            s_GradientEditor.Open(&data->m_RenderingSettings.m_ColorOverLifetime);
                        }
                        EndField();
                    }
                    break;
                }
            }

            if (BeginField("Texture:"))
            {
                /*
                auto oldTex = data->m_RenderingSettings.m_Texture;
                bool open = ImGui2::AssetSelectButton(data->m_RenderingSettings.m_Texture, {-1, 0.0f});
                bool confirm = AssetSelector::DrawAssetSelector("##particle_texture_selector", open,
                                                                {MV::TextureAssetType},
                                                                data->m_RenderingSettings.m_Texture);
                auto newTex = data->m_RenderingSettings.m_Texture;
                if(oldTex != newTex) {
                    data->m_UpdateShaderData = true;
                }
                if (confirm)
                    dirty = true;*/
                EndField();
            }

            {
                ScopedDisabled dis{data->m_RenderingSettings.m_Texture == MV::InvalidUUID};

                static const char* sprite_rendering_types[3] = {
                        "Sprite",
                        "Sprite sheet",
                        "Random sprite from sheet"
                };

                if (BeginField("Rendering type:"))
                {
                    auto selectedMode = static_cast<uint32_t>(data->m_RenderingSettings.m_TextureRenderingType);
                    const bool changed = FieldDrawingImpl::DrawCombo("##particle_system_sprite_rendering_type",
                                                                     sprite_rendering_types, 3, selectedMode);

                    if (changed)
                    {
                        data->m_RenderingSettings.m_TextureRenderingType = static_cast<MV::ParticleTextureRenderingType>(selectedMode);
                        dirty = true;
                    }

                    EndField();
                }

                switch (data->m_RenderingSettings.m_TextureRenderingType)
                {
                    case MV::ParticleTextureRenderingType::Sprite:
                    {
                        drawSpriteRenderingFields(dirty, data);
                        break;
                    }
                    case MV::ParticleTextureRenderingType::SpriteSheet:
                    {
                        drawSpriteSheetRenderingFields(dirty, data);
                        break;
                    }
                    case MV::ParticleTextureRenderingType::RandomSpriteFromSheet:
                    {
                        drawSpriteSheetRenderingFields(dirty, data);
                        break;
                    }
                    case MV::ParticleTextureRenderingType::Count:
                        break;
                }
            }

            End();

            /*
            auto &curveData = data->m_RenderingSettings.m_SizeOverLifetime.GetData();
            uint32_t dataSize = data->m_RenderingSettings.m_SizeOverLifetime.GetDataSize();

            struct CurveEditorUserPtr {
                MV::vector<MV::BezierCurve::BezierPoint> &m_Data;

                CurveEditorUserPtr(MV::vector<MV::BezierCurve::BezierPoint> &inData) : m_Data(inData) {};
            };

            CurveEditorUserPtr curveEditorData{curveData};

            auto getPosition = [](uint32_t index, void *userData) -> ImVec2 * {
                auto *dataPtr = (CurveEditorUserPtr *) userData;
                return (ImVec2 *) &dataPtr->m_Data[index].m_Point;
            };

            auto getPrevTangent = [](uint32_t index, void *userData) -> ImVec2 * {
                auto *dataPtr = (CurveEditorUserPtr *) userData;
                return (ImVec2 *) &dataPtr->m_Data[index].m_Left;
            };

            auto getNextTangent = [](uint32_t index, void *userData) -> ImVec2 * {
                auto *dataPtr = (CurveEditorUserPtr *) userData;
                return (ImVec2 *) &dataPtr->m_Data[index].m_Right;
            };

            ImGui::PushNeoCurveStyleColor(ImGuiNeoCurveCol_Curve, {1.0,1.0,1.0,1.0});
            ImGui::GetNeoCurveStyle().CurveThickness = 3.0f;

            int32_t flags = ImGuiNeoCurveFlags_EnablePointMoving | ImGuiNeoCurveFlags_ShowGrid |
                            ImGuiNeoCurveFlags_ShowInfiniteLines;

            if (ImGui::BeginNeoCurve("##size_over_lifetime_editor", curveData.size(), getPrevTangent, getPosition,
                                     getNextTangent, &curveEditorData, {0, 0},
                                     flags)) {

                if (ImGui::AddNeoCurvePoint()) {
                    ImVec2 newPoint = ImGui::GetAddNeoCurvePointPosition();
                    MV::BezierCurve::BezierPoint point{};
                    point.m_Point = {newPoint.x, newPoint.y};
                    point.m_Right[0] = newPoint.x + 0.1f;
                    point.m_Right[1] = newPoint.y + 0.1f;
                    point.m_Left[0] = newPoint.x - 0.1f;
                    point.m_Left[1] = newPoint.y - 0.1f;
                    curveData.emplace_back(point);

                    eastl::sort(curveData.begin(), curveData.end(), [](const MV::BezierCurve::BezierPoint &a, const MV::BezierCurve::BezierPoint &b) {
                        return a.m_Point.x > b.m_Point.x;
                    });
                }

                ImGui::EndNeoCurve();
            }

            ImGui::GetNeoCurveStyle().CurveThickness = 1.0f;
            ImGui::PopNeoCurveStyleColor();

            curveData[0].m_Point.x = 0.0f;
            curveData[curveData.size() - 1].m_Point.x = 1.0f;
*/
        }

        return dirty;
    }

    void ParticleSystemPropertyRenderer::drawParticleSettings(bool& dirty, MV::Property* dataIn)
    {
        auto data = static_cast<MV::ParticleSystemProperty*>(dataIn);


        if (BeginField("Lifetime fixed:"))
        {
            dirty |= ImGui::Checkbox("##particle_system_particle_lifetime_fixed", &data->m_ParticleSettings.m_LifetimeFixed);
            EndField();
        }

        if (BeginField("Lifetime:"))
        {
            bool changed = false;

            auto minValue = MV::optional<float>{};
            minValue.set(0.0f);

            if(data->m_ParticleSettings.m_LifetimeFixed) {
                changed = FieldDrawingImpl::DrawFloat("##particle_system_particle_lifetime_fixed_value",data->m_ParticleSettings.m_Lifetime.x, minValue);
            } else {
                changed = FieldDrawingImpl::DrawVec2("##particle_system_particle_lifetime_range_value",data->m_ParticleSettings.m_Lifetime, minValue);
            }

            if(changed) {
                if(data->m_ParticleSettings.m_Lifetime.x <= 0.0f) {
                    data->m_ParticleSettings.m_Lifetime.x = 0.0f;
                }

                if(data->m_ParticleSettings.m_Lifetime.y <= 0.0f) {
                    data->m_ParticleSettings.m_Lifetime.y = 0.0f;
                }

                if(data->m_ParticleSettings.m_Lifetime.y < data->m_ParticleSettings.m_Lifetime.x) {
                    data->m_ParticleSettings.m_Lifetime.y = data->m_ParticleSettings.m_Lifetime.x;
                }
            }

            dirty |= changed;

            EndField();
        }

        if (BeginField("Size type:"))
        {
            static const char* size_types[3] = {
                    "Constant",
                    "Random from range",
                    "Size over lifetime"
            };

            auto selectedMode = static_cast<uint32_t>(data->m_ParticleSettings.m_SizeType);
            const bool changed = FieldDrawingImpl::DrawCombo("##particle_system_size_type",
                                                             size_types, 3, selectedMode);

            if (changed)
            {
                data->m_ParticleSettings.m_SizeType = static_cast<MV::ParticleSystemParticleSettings::SizeType>(selectedMode);
                dirty = true;
            }

            EndField();
        }

        if (BeginField("Size:")) {

            switch(data->m_ParticleSettings.m_SizeType) {
                case MV::ParticleSystemParticleSettings::Fixed:
                    dirty |= FieldDrawingImpl::DrawVec2("##particle_system_particle_size_fixed",data->m_ParticleSettings.m_Size);
                    break;
                case MV::ParticleSystemParticleSettings::Random:
                    dirty |= FieldDrawingImpl::DrawVec2("##particle_system_particle_size_random_range_x",data->m_ParticleSettings.m_Size);
                    dirty |= FieldDrawingImpl::DrawVec2("##particle_system_particle_size_random_range_y",data->m_ParticleSettings.m_Size2);
                    break;
                case MV::ParticleSystemParticleSettings::OverLifetime:
                    ImGui::TextUnformatted("Curves are not added to ImGui yet!");
                    break;
            }

            EndField();
        }
    }
}
