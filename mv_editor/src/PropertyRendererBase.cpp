//
// Created by Matty on 2022-09-27.
//

#include <util/UtilGui.h>
#include "interface/worldobject_renderers/PropertyRendererBase.h"
#include "imgui/imgui.h"

namespace Editor {

    IPropertyRendererHelper::OnAnimableKeyframeCreateCallback IPropertyRendererHelper::s_OnCreateKeyframeCallback;
    IPropertyRendererHelper::IsFieldSerializationEnabledCallback IPropertyRendererHelper::s_KeySerializationEnabledCallback;
    IPropertyRendererHelper::EnableFieldSerializationCallback IPropertyRendererHelper::s_EnableFieldSerializationCallback;
    bool IPropertyRendererHelper::s_ShowCreateKeyframeButton = false;

    void PropertyRendererBase::drawSettingFieldImpl(bool& dirtyOut, MV::string_view wrapperName, MV::string_view title,
                                                    void* data, ImGuiDataType dataType, MV::string_view format,
                                                    float speed, float* min, float* max) {
        ScopedHorizontal hor{wrapperName.data()};
        ImGui::TextUnformatted(fmt::format("{}:", title).c_str());
        ImGui::Spring();
        ImGui::PushItemWidth(ImGui::GetFontSize() * 4.0f);
        bool res = ImGui::DragScalar(fmt::format("##{}_field", title).c_str(), dataType, data, speed, min, max,
                                     format.empty() ? nullptr : format.data());
        dirtyOut = res || dirtyOut;
    }

    void
    PropertyRendererBase::drawSettingFieldVec2Impl(bool& dirtyOut, MV::string_view wrapperName, MV::string_view title,
                                                   glm::vec2* data) {
        ScopedHorizontal hor{wrapperName.data()};
        ImGui::TextUnformatted(fmt::format("{}:", title).c_str());
        ImGui::Spring();
        ImGui::PushItemWidth(ImGui::GetFontSize() * 4.0f);
        bool res = ImGui::DragScalar(fmt::format("##{}_data_a", title.data()).c_str(), ImGuiDataType_Float,
                                     (void*) &data->x);

        ImGui::PushItemWidth(ImGui::GetFontSize() * 4.0f);
        res |= ImGui::DragScalar(fmt::format("##{}_data_b", title.data()).c_str(), ImGuiDataType_Float,
                                 (void*) &data->y);

        dirtyOut = dirtyOut || res;
    }

    bool IPropertyRendererHelper::Begin(MV::string_view name) {
        if (ImGui::BeginTable(name.data(), 2, ImGuiTableFlags_NoSavedSettings)) {
            ImGui::TableNextRow();
            return true;
        }

        return false;
    }

    void IPropertyRendererHelper::End() {
        ImGui::EndTable();
    }

    static MV::vector<bool> s_DisabledFieldStack;

    bool IPropertyRendererHelper::BeginField(MV::string_view name, MV::Archive::ValueId valueId, MV::AnimationInputField* animationData) {
        if (!ImGui::TableSetColumnIndex(0))
            return false;

        bool enabled = true;

        if(s_KeySerializationEnabledCallback && s_EnableFieldSerializationCallback && valueId != 0 && animationData) {
            bool v = s_KeySerializationEnabledCallback(valueId);
            bool changed = ImGui::Checkbox(fmt::format("##checkbox_field_animation_{}",name).c_str(), &v);
            if(changed) {
                s_EnableFieldSerializationCallback(valueId, v);
            }
            ImGui::SameLine();
            if(ImGui::SmallButton(fmt::format("R##{}", name).c_str())) {
                s_OnCreateKeyframeCallback(animationData);
            }

            //enabled = v;

            ImGui::SameLine();
        }

        if(!enabled) {
            ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
        }

        ImGui::TextUnformatted(name.data());

        if (!ImGui::TableSetColumnIndex(1))
            return false;


        s_DisabledFieldStack.emplace_back(enabled);

        return true;
    }

    void IPropertyRendererHelper::EndField() {
        ImGui::TableNextRow();

        if(!s_DisabledFieldStack.back()) {
            ImGui::PopItemFlag();
            ImGui::PopStyleVar();
        }

        s_DisabledFieldStack.pop_back();
    }
}
