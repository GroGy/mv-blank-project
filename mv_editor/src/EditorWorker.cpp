//
// Created by Matty on 2021-10-28.
//

#include "../editor/async/EditorWorker.h"
#include "profiler/Profiler.h"
#include "util/Thread.h"

namespace Editor {
    void EditorWorker::AddTask(MV::rc<EditorTask> task) {
        m_pTasks->push(std::move(task));
    }

    EditorWorker::EditorWorker(MV::rc<MV::thread_safe_queue<MV::rc<EditorTask>>> &tasks,
                               MV::rc<MV::thread_safe_queue<MV::rc<EditorTask>>> &finished) :
                               m_pTasks(tasks), m_pFinishedTasks(finished) {

    }

    void EditorWorker::Init() {
        m_pThread = std::thread{
                [&]() {
                    MV_PROFILE_THREAD("Editor worker")
                    processLoop();
                }
        };

        MV::SetThreadName(&m_pThread, "Editor worker");
    }

    void EditorWorker::Cleanup() {
        if(!m_pRun) return;
        m_pRun = false;
        m_pTasks->wake_cv();

        while(!m_pJoinable) {
            m_pTasks->wake_cv();
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
        }

        m_pThread.join();
    }

    void EditorWorker::processLoop() {
        while (m_pRun) {
            try {
                auto resource = std::move(m_pTasks->pop());
                resource->Process();
                if (!resource->Success()) {
                    //todo: handle fail
                }
                m_pFinishedTasks->push(std::move(resource));
            } catch(std::exception &e) {
                (void) e;
            }
        }
        m_pJoinable = true;
    }
}