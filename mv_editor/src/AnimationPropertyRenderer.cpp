//
// Created by Matty on 31.08.2023.
//

#include "interface/worldobject_renderers/AnimationPropertyRenderer.h"
#include "animation/AnimationProperty.h"
#include "util/UtilGui.h"
#include "interface/common/AssetSelector.h"

namespace Editor
{
    bool AnimationPropertyRenderer::Draw(const MV::Meta::Field* field, const MV::rc<MV::WorldObject>& wo,
                                         MV::Property* property)
    {
        auto data = dynamic_cast<MV::AnimationProperty*>(property);
        bool dirty = false;

        if (!Begin("##animation_general_settings"))
        {
            return false;
        }

        if (BeginField("Animation:"))
        {
            /*
            bool open = ::ImGui2::AssetSelectButton(data->m_AnimationAsset, {ImGui::GetContentRegionAvail().x, 0.0f});
            bool confirm = AssetSelector::DrawAssetSelector(
                    fmt::format("##animation_property_animation_selector_{}", data->m_StorageIndex).c_str(), open,
                    {MV::AnimationAssetType},
                    data->m_AnimationAsset);

            if (confirm) {
                dirty = true;
                data->m_Animation.reset();
                data->m_Inputs.clear();
            }
*/
            EndField();
        }

        if (data->m_Animation)
        {
            for (uint32_t i = 0; i < data->m_Animation->m_InputFields.size(); i++)
            {
                auto & inputInfo = data->m_Animation->m_InputFields[i];
                auto & f = data->m_Inputs[i];
                if (BeginField(fmt::format("{}:", inputInfo.m_Name).c_str()))
                {
                    if(ImGui2::PropertyRefButton("Empty", ImVec2{ImGui::GetContentRegionAvail().x, ImGui::GetFrameHeight()}, *f)) {
                        dirty |= true;
                        data->UpdatePreviewAnimation();
                    }
                    EndField();
                }
            }
        }

        End();

        return dirty;
    }
}