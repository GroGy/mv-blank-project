//
// Created by Matty on 2022-01-23.
//

#include <fstream>
#include "../editor/interface/EditorSettings.h"
#include "../editor/util/UtilGui.h"
#include "../editor/EditorCommon.h"
#include "EditorSettings.h"


namespace Editor {

    EditorSettings::EditorSettings() : Window("Editor settings") {

    }

    void EditorSettings::WindowSetup() {

    }

    void EditorSettings::Render() {
        ScopedVertical vert{"EditorSettings##vert_wrapper", ImGui::GetContentRegionAvail()};

        {
            ScopedHorizontal hor{"EditorSettings##draw_grid"};
            ImGui::TextUnformatted("Draw grid:");
            ImGui::Spring();
            ImGui::Checkbox("##editor_settings_draw_grid", &GetEditor()->m_Settings.m_DrawViewportGrid);
        }

        {
            ScopedHorizontal hor{"EditorSettings##draw_colliders"};
            ImGui::TextUnformatted("Draw colliders:");
            ImGui::Spring();
            ImGui::Checkbox("##editor_settings_draw_colliders", &GetEditor()->m_Settings.m_DrawDebugColliders);
        }
    }

    void EditorSettings::PostRender() {

    }

    void EditorSettings::OnCleanup() {

    }

    void EditorSettings::OnInit() {

    }

    void EditorSettingsVals::Serialize(const MV::string &path) {
        nlohmann::json j;

        j["ui_scale"] = m_UIScale;

        std::filesystem::path pth = path.c_str();

        if(path.empty()) {
            pth /= Config::sc_DefaultEditorSettingsPath.data();
        }

        std::ofstream out{pth.string()};

        if (!out.is_open())
        {
           MV::GetLogger()->LogError("Failed to write editor settings file.");
           return;
        }

        out << j;
    }

    void EditorSettingsVals::Deserialize(const MV::string &path) {
        std::filesystem::path pth = path.c_str();

        if(path.empty()) {
            pth /= Config::sc_DefaultEditorSettingsPath.data();
        }

        std::ifstream in{pth.string()};

        if (!in.is_open())
        {
            MV::GetLogger()->LogError("Failed to open editor settings file {}", pth.string());
            return;
        }

        try {
            auto j = nlohmann::json::parse(in);

            m_UIScale = j.at("ui_scale").get<float>();
        } catch (std::exception& e) {
            MV::GetLogger()->LogError("Failed to parse settings file {}", pth.string());
        }

        in.close();
    }
}