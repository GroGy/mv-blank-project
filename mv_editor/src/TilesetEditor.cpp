//
// Created by Matty on 11.12.2022.
//

#ifndef IMGUI_DEFINE_MATH_OPERATORS
#define IMGUI_DEFINE_MATH_OPERATORS
#endif

#include "imgui/imgui_internal.h"

#include "tools/tileset_editor/TilesetEditor.h"

#include <utility>
#include "util/UtilGui.h"
#include "common/EngineCommon.h"
#include "render/vulkan/VulkanTexture.h"
#include "imgui/imgui_impl_vulkan.h"
#include "imgui/icons.h"
#include "resources/TilesetProducer.h"
#include "Engine.h"
#include "common/TextureAsset.h"
#include "cvar/CVars.h"
#include "tools/tileset_editor/CollisionLookup.h"

namespace Editor
{
    namespace CVar
    {
        static glm::vec4 s_CollisionPointColor = {0.2f, 0.2f, 0.2f, 1.0f};
        static glm::vec4 s_CollisionPointHoveredColor = {1.0f, 1.0f, 1.0f, 1.0f};
        static glm::vec4 s_CollisionPointActiveColor = {0.8f, 1.0f, 0.8f, 1.0f};
        static float s_CollisionPointRadius = 1.0f;
    }

    static MV::CVarRef CVar_TilesetEditor_CollisionPointColor{
            "mv.editor.TilesetEditor.collision_point_color",
            "Color for collision vertex",
            CVar::s_CollisionPointColor
    };

    static MV::CVarRef CVar_TilesetEditor_CollisionPointHoveredColor{
            "mv.editor.TilesetEditor.collision_point_color_hovered",
            "Color for hovered collision vertex",
            CVar::s_CollisionPointHoveredColor
    };

    static MV::CVarRef CVar_TilesetEditor_CollisionPointActiveColor{
            "mv.editor.TilesetEditor.collision_point_color_active",
            "Color for active collision vertex",
            CVar::s_CollisionPointActiveColor
    };

    static MV::CVarRef CVar_TilesetEditor_CollisionPointRadius{
            "mv.editor.TilesetEditor.collision_point_radius",
            "Radius of collision point",
            CVar::s_CollisionPointRadius
    };

    TilesetEditor::TilesetEditor() : Window(Config::sc_TilesetEditorWindowName.data(), true)
    {
        m_pOpen = false;
        m_Flags |= ImGuiWindowFlags_MenuBar;
    }

    void TilesetEditor::WindowSetup()
    {
    }

    void TilesetEditor::Render()
    {
        MV::AllocatorTagScope allocTag{MV::AllocationType::Editor};
        updateData();

        drawMenuBar();

        drawAddTilePopup();

        ScopedVertical verticalWrapper{"##TilesetEditor_vertical", ImGui::GetContentRegionAvail()};

        ScopedHorizontal hor{"##TilesetEditor_horizontal"};

        // Draw left panel
        if (ImGui::BeginChild("##TilesetEditor_left_panel",
                              ImVec2{
                                      ImGui::GetContentRegionAvail().x / 5.0f - ImGui::GetStyle().ItemSpacing.x,
                                      -1})
                )
        {
            drawTileSelectPanel();
        }
        ImGui::EndChild();

        if (ImGui::BeginChild("##TilesetEditor_right_panel_with_viewport"))
        {
            ScopedHorizontal horViewport{"##TilesetEditor_horizontal_viewport_part", {-1, -1}};
            const float rightPanelSize = ImGui::GetFontSize() + ImGui::GetStyle().ItemSpacing.x * 2;

            if (ImGui::BeginChild("##TilesetEditor_viewport_with_bottom_bar",
                                  {ImGui::GetContentRegionAvail().x - rightPanelSize - ImGui::GetStyle().ItemSpacing.x,
                                   0}))
            {
                ScopedVertical vertWrapper{"tileseteditor_viewport_top_bar_wrapper", ImGui::GetContentRegionAvail()};
                const float bottomBarHeight =
                        ImGui::GetFontSize() + ImGui::GetStyle().ItemSpacing.y + ImGui::GetStyle().FramePadding.y * 2;
                if (ImGui::BeginChild("##TilesetEditor_viewport",
                                      {-1,
                                       ImGui::GetContentRegionAvail().y - bottomBarHeight}, true))
                {

                    drawViewport(ImGui::GetWindowPos(), ImGui::GetWindowPos() + ImGui::GetWindowSize());

                }
                ImGui::EndChild();

                ImGui::Spring(0);

                if (ImGui::BeginChild("##TilesetEditor_bottombar",
                                      {-1, bottomBarHeight}))
                {

                    drawUnderViewportBar();
                }
                ImGui::EndChild();
            }
            ImGui::EndChild();

            ImGui::Spring(0);
            if (ImGui::BeginChild("##TilesetEditor_properties_panel", {rightPanelSize, 0}))
            {
                drawModePanel();
            }
            ImGui::EndChild();
        }
        ImGui::EndChild();
    }

    void TilesetEditor::drawMenuBar()
    {
        if (ImGui::BeginMenuBar())
        {
            if (ImGui::BeginMenu("File"))
            {
                if (ImGui::MenuItem("Open"))
                {

                }

                if (ImGui::MenuItem("Save"))
                {
                    auto path = MV::GetEngine()->GetProject().m_Assets[m_pTilesetID].m_Path;
                    auto resPath = TilesetProducer::CreateTilesetResource(m_pData, path, true);

                    if (MV::GetEngine()->m_ResourceManager.IsAssetLoaded(m_pTilesetID))
                        MV::GetEngine()->m_ResourceManager.UnloadAsset(m_pTilesetID);
                }

                if (ImGui::MenuItem("Reimport"))
                {

                }
                ImGui::EndMenu();
            }


            auto cursor = ImGui::GetCurrentWindow()->DC.CursorPos;
            m_pAddTileWindowPosition = {cursor.x, cursor.y + ImGui::GetTextLineHeight() +
                                                  ImGui::GetStyle().FramePadding.y * 2};
            if (ImGui::BeginMenu("Tileset"))
            {
                ImGui::MenuItem("Add tile...", nullptr, &m_pTileAddOpen);
                ImGui::EndMenu();
            }
            ImGui::EndMenuBar();
        }
    }

    void TilesetEditor::PostRender()
    {

    }

    void TilesetEditor::OnCleanup()
    {

    }

    void TilesetEditor::OnInit()
    {

    }

    void TilesetEditor::drawTileSelectPanel()
    {
        if (m_pDataState != HasData)
        {
            ScopedVertical vert{"TilemapEditor_TileSelection_VertWrapper", ImGui::GetContentRegionAvail()};
            ImGui::Spring();
            {
                ScopedHorizontal hor{"TilemapEditor_TileSelection_HorWrapper"};
                if (m_pDataState == NoData)
                {
                    ImGui::TextUnformatted("No tileset loaded.");
                } else
                {
                    ImGui::ProgressBar(0.5f, ImVec2(-FLT_MIN, 0), "Loading...");
                }
            }
            ImGui::Spring();
            return;
        }

        const float buttonSize = (ImGui::GetFontSize() * 2.0f) + ImGui::GetStyle().ItemSpacing.x * 2;

        const uint32_t buttonsPerRow = std::max(
                ImGui::GetContentRegionAvail().x / (buttonSize + ImGui::GetStyle().ItemSpacing.x * 2), 1.0f);

        for (uint32_t i = 0; i < m_pData.m_Tiles.size(); i++)
        {
            const auto& tile = m_pData.m_Tiles[i];

            bool pressed;

            const auto cursor = ImGui::GetCurrentWindow()->DC.CursorPos;

            if (m_pTexture.GetLoadState() != EditorTexture::Loaded || !m_pAtlas)
            {
                pressed = ImGui::Button(fmt::format("##tile_{}", i).c_str(),
                                        ImVec2{buttonSize, buttonSize} + (ImGui::GetStyle().FramePadding * 2));
            } else
            {
                const auto& uv = m_pAtlas->GetUVs(tile.m_SpriteIndex);

                ImGui::PushID(fmt::format("##tile_{}", i).c_str());
                pressed = ImGui::ImageButton(m_pTexture.GetHandle(), {buttonSize, buttonSize}, {uv.x, uv.y},
                                             {uv.z, uv.w});
                ImGui::PopID();
            }

            if (i == m_pSelectedTile)
            {
                auto* drawList = ImGui::GetWindowDrawList();

                drawList->AddRect(cursor,
                                  cursor + ImVec2{buttonSize, buttonSize} + ImGui::GetStyle().FramePadding * 2,
                                  ImGui::ColorConvertFloat4ToU32({0.1f, 0.88f, 0.1f, 1.0f}),
                                  ImGui::GetStyle().FrameRounding, 0,
                                  2.0f);
            }

            if ((i + 1) % buttonsPerRow != 0)
            {
                ImGui::SameLine();
            }

            if (pressed)
                m_pSelectedTile = i;
        }
    }

    void TilesetEditor::drawViewport(const ImVec2& windowMin, const ImVec2& windowMax)
    {
        auto drawList = ImGui::GetWindowDrawList();

        drawBackground(windowMin, windowMax);

        if (m_pSelectedTile == -1 || !m_pAtlas)
            return;

        const auto windowSize = windowMax - windowMin;

        const auto center = windowMin + windowSize / 2;

        const float shorterSide = windowSize.y < windowSize.x ? windowSize.y : windowSize.x;

        const auto quaterSize = ImVec2{shorterSide / 4, shorterSide / 4};

        if (m_pTexture.GetLoadState() != EditorTexture::Loaded)
        {
            drawList->AddRectFilled(center - quaterSize, center + quaterSize, IM_COL32(0, 0, 0, 255));
        } else
        {
            auto uv = m_pAtlas->GetUVs(m_pData.m_Tiles[m_pSelectedTile].m_SpriteIndex);
            drawList->AddImage(m_pTexture.GetHandle(), center - quaterSize, center + quaterSize, {uv.x, uv.y},
                               {uv.z, uv.w});
        }

        if (m_pToolState == ToolState::EditingCollision && m_pShowCollisionPreview)
        {
            drawCollisionOverlay(center - quaterSize, center + quaterSize);
        }

    }

    void TilesetEditor::drawModePanel()
    {
        struct ModePreset
        {
            ModePreset(MV::string mName, MV::string mIcon) : m_Name(MV::move(mName)), m_Icon(MV::move(mIcon))
            {}

            MV::string m_Name;
            MV::string m_Icon;
        };

        static MV::array<ModePreset, static_cast<uint32_t>(ToolState::Count) - 1> tools = {
                ModePreset{"Edit collision", ICON_FA_BUILDING},
        };

        for (uint32_t i = 0; i < tools.size(); i++)
        {
            auto* window = ImGui::GetCurrentWindow();
            const float btnSize = ImGui::GetContentRegionAvail().x;
            auto& tool = tools[i];
            const bool isActive = static_cast<uint32_t>(m_pToolState) == i + 1;

            const auto cursor = window->DC.CursorPos;

            if (ImGui::Button(fmt::format("{}##_state_{}", tool.m_Icon.c_str(), i).c_str(),
                              {btnSize, btnSize}))
            {
                setToolState(static_cast<ToolState>(i + 1));
            }

            if (isActive)
            {
                auto* drawList = ImGui::GetWindowDrawList();

                drawList->AddRect(cursor,
                                  cursor + ImVec2{btnSize, btnSize},
                                  ImGui::ColorConvertFloat4ToU32({0.1f, 0.88f, 0.1f, 1.0f}),
                                  ImGui::GetStyle().FrameRounding, 0,
                                  2.0f);
            }
        }
    }

    void TilesetEditor::LoadTileset(uint64_t tilesetID)
    {
        MV::AllocatorTagScope allocTag{MV::AllocationType::Editor};
        clearData();

        m_pTilesetID = tilesetID;

        m_pResource = MV::dynamic_pointer_cast<MV::TilesetResource>(
                MV::GetEngine()->m_ResourceManager.LoadResource(tilesetID, true));

        m_pDataState = Loading;
    }

    void TilesetEditor::updateData()
    {
        if (m_pDataState == NoData)
            return;


        if (m_pResource)
        {
            if (!m_pResource->IsFinished())
                return;
            if (!m_pResource->Success())
            {
                m_pResource.reset();
                m_pDataState = NoData;
                return;
            }

            m_pData = MV::move(m_pResource->m_Result);
            m_pResource.reset();
            m_pDataState = HasData;
        }

        if (!m_pAtlas)
        {
            if (MV::GetEngine()->m_ResourceManager.IsAssetLoaded(m_pData.m_Atlas))
            {
                m_pAtlas = MV::GetResources()->m_Atlases[m_pData.m_Atlas];
            } else if (!m_pAtlasResource)
            {
                m_pAtlasResource = MV::dynamic_pointer_cast<MV::AtlasResource>(
                        MV::GetEngine()->m_ResourceManager.LoadResource(m_pData.m_Atlas));
                return;
            } else
            {
                if (!m_pAtlasResource->IsFinished())
                {
                    return;
                }

                if (!m_pAtlasResource->Success())
                {
                    return;
                }

                if (MV::GetEngine()->m_ResourceManager.IsAssetLoaded(m_pData.m_Atlas))
                {
                    m_pAtlas = MV::GetResources()->m_Atlases[m_pData.m_Atlas];
                    m_pAtlasResource.reset();
                } else
                {
                    return;
                }
            }
        }

        if (!m_pAtlas->AssetLoaded())
        {
            return;
        }

        if (m_pTexture.GetLoadState() == EditorTexture::Unloaded)
        {
            if (!MV::GetEngine()->m_ResourceManager.IsAssetLoaded(m_pAtlas->GetTextureAssetID()))
            {
                if (!m_pTextureResource)
                {
                    m_pTextureResource = MV::dynamic_pointer_cast<MV::TextureResource>(
                            MV::GetEngine()->m_ResourceManager.LoadResource(m_pAtlas->GetTextureAssetID()));
                    return;
                }

                if (!m_pTextureResource->IsFinished())
                {
                    return;
                }

                if (!m_pTextureResource->Success())
                {
                    return;
                }
            }

            const auto& textures = MV::GetResources()->m_Textures;
            const auto foundTexture = textures.find(m_pAtlas->GetTextureAssetID());

            ENSURE_TRUE(foundTexture != textures.end());

            if (!foundTexture->second->AssetLoaded())
            {
                return;
            }

            m_pTexture.Load(foundTexture->second->GetTextureHandle());
        }
    }

    void TilesetEditor::clearData()
    {
        m_pDataState = NoData;
        m_pData = {};
        m_pResource.reset();
        m_pAtlas.reset();
        m_pAtlasResource.reset();
        m_pTextureResource.reset();

        if (m_pTexture.GetLoadState() != EditorTexture::Unloaded)
        {
            m_pTexture.Unload();
        }
    }

    static constexpr MV::array<glm::vec2, 8> FullVertices = {
            glm::vec2{0.0f, 0.0f},
            glm::vec2{0.5f, 0.0f},
            glm::vec2{1.0f, 0.0f},
            glm::vec2{1.0f, 0.5f},
            glm::vec2{1.0f, 1.0f},
            glm::vec2{0.5f, 1.0f},
            glm::vec2{0.0f, 1.0f},
            glm::vec2{0.0f, 0.5f},
    };

    static constexpr MV::array<MV::TileCollision::Edge, 8> FullEdges = {
            MV::TileCollision::Edge{0, 1, static_cast<uint8_t>(MV::TileSideMergeable::Mergeable), MV::TileSide::Bottom},
            MV::TileCollision::Edge{1, 2, static_cast<uint8_t>(MV::TileSideMergeable::Mergeable), MV::TileSide::Bottom},
            MV::TileCollision::Edge{2, 3, static_cast<uint8_t>(MV::TileSideMergeable::Mergeable), MV::TileSide::Right},
            MV::TileCollision::Edge{3, 4, static_cast<uint8_t>(MV::TileSideMergeable::Mergeable), MV::TileSide::Right},
            MV::TileCollision::Edge{4, 5, static_cast<uint8_t>(MV::TileSideMergeable::Mergeable), MV::TileSide::Top},
            MV::TileCollision::Edge{5, 6, static_cast<uint8_t>(MV::TileSideMergeable::Mergeable), MV::TileSide::Top},
            MV::TileCollision::Edge{6, 7, static_cast<uint8_t>(MV::TileSideMergeable::Mergeable), MV::TileSide::Left},
            MV::TileCollision::Edge{7, 0, static_cast<uint8_t>(MV::TileSideMergeable::Mergeable), MV::TileSide::Left},
    };

    void TilesetEditor::generateTileCollisionData(uint32_t tileIndex, bool setSolid)
    {
        ENSURE_TRUE(m_pDataState == HasData);
        ENSURE_TRUE(tileIndex < m_pData.m_Tiles.size())

        auto& tile = m_pData.m_Tiles[tileIndex];

        auto& collision = tile.m_Collision;

        collision.m_Vertices.clear();
        collision.m_Edges.clear();

        collision.m_Vertices.insert(collision.m_Vertices.begin(), FullVertices.begin(), FullVertices.end());
    }

    void TilesetEditor::drawBackground(const ImVec2& windowMin, const ImVec2& windowMax)
    {
        auto drawList = ImGui::GetWindowDrawList();
        drawList->AddRectFilled(windowMin, windowMax, IM_COL32(20, 20, 20, 100));
    }

    void TilesetEditor::setToolState(TilesetEditor::ToolState newState)
    {
        auto oldState = m_pToolState;
        if (oldState == newState)
            return;

        if (newState == ToolState::EditingCollision)
        {
            m_pShowCollisionPreview = true;
        }

        m_pToolState = newState;
    }

    void TilesetEditor::drawUnderViewportBar()
    {
        switch (m_pToolState)
        {
            case ToolState::EditingCollision:
            {
                {
                    ScopedDisabled dis{m_pSelectedTile == -1};
                    if (ImGui::Button("Fill collision"))
                    {
                        generateTileCollisionData(m_pSelectedTile);
                    }
                }

                {
                    ImGui::SameLine();
                    ImGui::TextUnformatted("Point preview:");
                    ImGui::SameLine();

                    static const char* typeNames[] = {
                            "None",
                            "Circle"
                    };

                    ImGui::PushItemWidth(ImGui::CalcTextSize("LONG_NAME_THING").x);
                    if (ImGui::BeginCombo("##point_preview_type",
                                          typeNames[static_cast<uint32_t>(m_pCollisionPointPreviewType)]))
                    {
                        for (uint32_t i = 0; i < static_cast<uint32_t>(CollisionPointPreview::Count); i++)
                        {
                            if (ImGui::Selectable(typeNames[i],
                                                  i == static_cast<uint32_t>(m_pCollisionPointPreviewType)))
                            {
                                m_pCollisionPointPreviewType = static_cast<CollisionPointPreview>(i);
                            }
                        }
                        ImGui::EndCombo();
                    }
                }

                {
                    ScopedDisabled dis{m_pSelectedTile == -1};

                    ImGui::SameLine();
                    if (ImGui::SmallButton("0"))
                    {
                        m_pData.m_Tiles[m_pSelectedTile].m_MergeableSides[0] =
                                m_pData.m_Tiles[m_pSelectedTile].m_MergeableSides[0] == MV::TileSideMergeable::Mergeable
                                ?
                                MV::TileSideMergeable::NonMergeable : MV::TileSideMergeable::Mergeable;
                    }

                    for (uint32_t i = 1; i < static_cast<uint32_t>(MV::TileSide::Count); i++)
                    {
                        ImGui::SameLine();
                        if (ImGui::SmallButton(fmt::format("{}", i).c_str()))
                        {
                            m_pData.m_Tiles[m_pSelectedTile].m_MergeableSides[i] =
                                    m_pData.m_Tiles[m_pSelectedTile].m_MergeableSides[i] ==
                                    MV::TileSideMergeable::Mergeable ?
                                    MV::TileSideMergeable::NonMergeable : MV::TileSideMergeable::Mergeable;
                        }
                    }
                }

                break;
            }
            default:
                return;
        }

    }

    static ImColor glmColToImColor(const glm::vec4& in)
    {
        return ImColor{in.x, in.y, in.z, in.w};
    }

    void TilesetEditor::drawCollisionOverlay(const ImVec2& tileMin, const ImVec2& tileMax)
    {
        if (m_pSelectedTile == -1)
            return;

        auto& tile = m_pData.m_Tiles[m_pSelectedTile];
        auto* drawList = ImGui::GetWindowDrawList();

        const auto tileSize = tileMax - tileMin;

        //Draw collision merge info

        static MV::array<ImVec2, 4> sideMinMultipliers = {
                ImVec2{0.0f, -Config::sc_MergeablePreviewSize},
                ImVec2{0.f, 1.0f},
                ImVec2{-Config::sc_MergeablePreviewSize, 0.f},
                ImVec2{1.f, 0.f}
        };

        static MV::array<ImVec2, 4> sideMaxMultipliers = {
                ImVec2{1.f, 0.f},
                ImVec2{1.f, 1.0f + Config::sc_MergeablePreviewSize},
                ImVec2{0.f, 1.f},
                ImVec2{1.f + Config::sc_MergeablePreviewSize, 1.f}
        };

        static MV::array<ImVec2, 4> sideHalfMinMultipliers = {
                ImVec2{1.f, 0.f},
                ImVec2{1.f, 0.f},
                ImVec2{0.f, 1.f},
                ImVec2{0.f, 1.f}
        };

        static MV::array<ImVec2, 4> sideHalfMaxMultipliers = {
                ImVec2{0.5f, 1.f},
                ImVec2{0.5f, 1.f},
                ImVec2{1.f, 0.5f},
                ImVec2{1.f, 0.5f}
        };

        /*
        for (uint32_t i = 0; i < static_cast<uint32_t>(MV::TileSide::Count); i++)
        {
            const bool showHalf = tile.m_MergeableSides[i] == MV::TileSideMergeable::LTHalfMergeable ||
                                  tile.m_MergeableSides[i] == MV::TileSideMergeable::RBHalfMergeable;

            ImVec2 min = {tileMin + (sideMinMultipliers[i] * tileSize)};
            ImVec2 max = {tileMin + (sideMaxMultipliers[i] * tileSize)};

            if (showHalf)
            {
                const ImColor lcolor = tile.m_MergeableSides[i] == MV::TileSideMergeable::LTHalfMergeable ?
                                       glmColToImColor(Config::sc_MergeablePreviewColor) :
                                       glmColToImColor(Config::sc_NonMergeablePreviewColor);

                const ImColor rcolor = tile.m_MergeableSides[i] == MV::TileSideMergeable::RBHalfMergeable ?
                                       glmColToImColor(Config::sc_MergeablePreviewColor) :
                                       glmColToImColor(Config::sc_NonMergeablePreviewColor);

                const auto halfMinSize = ((max - min) / 2) * sideHalfMinMultipliers[i];
                const auto halfMaxSize = ((max - min)) * sideHalfMaxMultipliers[i];

                drawList->AddRectFilled(min, min + halfMaxSize, ImGui::ColorConvertFloat4ToU32(lcolor));

                drawList->AddRectFilled(min + halfMinSize, max, ImGui::ColorConvertFloat4ToU32(rcolor));

                continue;
            }

            const ImColor color = tile.m_MergeableSides[i] == MV::TileSideMergeable::Mergeable ?
                                  glmColToImColor(Config::sc_MergeablePreviewColor) :
                                  glmColToImColor(Config::sc_NonMergeablePreviewColor);

            drawList->AddRectFilled(min, max, ImGui::ColorConvertFloat4ToU32(color));
        }
*/
        // Draw collision

        for (auto&& edge: tile.m_Collision.m_Edges)
        {
            const auto& p1v = tile.m_Collision.m_Vertices[edge.m_Index1];
            const auto p1 = tileMin + ImVec2{p1v.x * tileSize.x, tileSize.y - p1v.y * tileSize.y};
            const auto& p2v = tile.m_Collision.m_Vertices[edge.m_Index2];
            const auto p2 = tileMin + ImVec2{p2v.x * tileSize.x, tileSize.y - p2v.y * tileSize.y};

            drawEdge(p1, p2, edge);
        }


        int32_t counter = 0;
        for (auto&& p: tile.m_Collision.m_Vertices)
        {
            const auto pos = tileMin + ImVec2{p.x * tileSize.x, p.y * tileSize.y};

            switch (m_pCollisionPointPreviewType)
            {
                case CollisionPointPreview::None:
                    break;
                case CollisionPointPreview::Circle:
                {
                    ImRect bb{pos - ImVec2{CVar::s_CollisionPointRadius, CVar::s_CollisionPointRadius},
                              pos + ImVec2{CVar::s_CollisionPointRadius, CVar::s_CollisionPointRadius}};
                    ImGui::PushID(counter);
                    ImGui::ItemAdd(bb, ImGui::GetID("point"));
                    ImGui::PopID();

                    auto mask = 1 << (counter);

                    const bool hovered = ImGui::IsItemHovered();
                    const bool active = (tile.m_Collision.m_VertFlags & mask) == mask;

                    auto color = active ? CVar::s_CollisionPointActiveColor : CVar::s_CollisionPointColor;

                    if (hovered)
                    {
                        color = CVar::s_CollisionPointHoveredColor;
                    }


                    drawList->AddCircleFilled(pos, CVar::s_CollisionPointRadius,
                                              ImGui::ColorConvertFloat4ToU32GLM(color));

                    if (ImGui::IsItemClicked())
                    {
                        if ((tile.m_Collision.m_VertFlags & mask) == mask)
                        {
                            tile.m_Collision.m_VertFlags &= ~mask;
                        } else
                        {
                            tile.m_Collision.m_VertFlags |= mask;
                        }

                        MV::GetLogger()->LogInfo("{:08b} => {}", static_cast<uint16_t>(tile.m_Collision.m_VertFlags),
                                                 static_cast<uint16_t>(tile.m_Collision.m_VertFlags));

                        regenerateCollisionForTile(m_pSelectedTile);
                    }

                    counter++;

                    break;
                }
                case CollisionPointPreview::Count:
                    break;
            }

        }
    }

    void TilesetEditor::drawEdge(const ImVec2& start, const ImVec2& end, const MV::TileCollision::Edge& edge)
    {
        auto* drawList = ImGui::GetWindowDrawList();

        drawList->AddLine(start, end, IM_COL32(0, 0, 0, 255), 7.0f);
        drawList->AddLine(start, end, IM_COL32(0, 240, 20, 255), 5.0f);

        const float arrowOffset = 30.0f;
        ImVec2 edgeDir = end - start;
        edgeDir /= (abs(edgeDir.x) + abs(edgeDir.y));
        const ImVec2 invDir = {edgeDir.y, -edgeDir.x};


        const ImVec2 center = start + ((end - start) / 2);


        const float size = 10.0f;
        const ImVec2 position = center + (invDir * arrowOffset);
        const ImColor mergeableArrowColor = {0.0f, 1.0f, 0.0f, 1.0f};
        const ImColor nonmergeableArrowColor = {1.0f, 0.0f, 0.0f, 1.0f};
        const float thickness = 1.0f;
        const float arrowFlapsWidth = 5.0f;
        const float arrowFlapsRatio = 0.6f;

        const float arrowSubOffset = 15.0f;

        auto isEdgeMergeable = [](const MV::TileCollision::Edge& edgeIn, MV::TileSide side) -> bool
        {
            return edgeIn.m_Side == side && edgeIn.m_Mergeable != 0;
        };

        //drawArrow(drawList, {-1, 0}, size, position + (ImVec2{-1, 0} * arrowSubOffset),
        //          isEdgeMergeable(edge, MV::TileSide::Left) ? mergeableArrowColor : nonmergeableArrowColor, thickness,
        //          arrowFlapsWidth, arrowFlapsRatio);
        //drawArrow(drawList, {1, 0}, size, position + (ImVec2{1, 0} * arrowSubOffset),
        //          isEdgeMergeable(edge, MV::TileSide::Right) ? mergeableArrowColor : nonmergeableArrowColor, thickness,
        //          arrowFlapsWidth, arrowFlapsRatio);
        //drawArrow(drawList, {0, 1}, size, position + (ImVec2{0, 1} * arrowSubOffset),
        //          isEdgeMergeable(edge, MV::TileSide::Top) ? mergeableArrowColor : nonmergeableArrowColor, thickness,
        //          arrowFlapsWidth, arrowFlapsRatio);
        //drawArrow(drawList, {0, -1}, size, position + (ImVec2{0, -1} * arrowSubOffset),
        //          isEdgeMergeable(edge, MV::TileSide::Bottom) ? mergeableArrowColor : nonmergeableArrowColor, thickness,
        //          arrowFlapsWidth, arrowFlapsRatio);
    }

    void TilesetEditor::drawAddTilePopup()
    {
        if (!m_pTileAddOpen)
        {
            return;
        }

        const auto flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoMove;

        ImGui::SetNextWindowPos({m_pAddTileWindowPosition.x, m_pAddTileWindowPosition.y}, ImGuiCond_Always);
        ImGui::SetNextWindowSize({300, 250}, ImGuiCond_Always);


        if (ImGui::Begin("##add_tile", &m_pTileAddOpen, flags))
        {
            {
                ScopedDisabled dis{m_pSelectedSpriteIndex == -1};
                if (ImGui::Button("Add", {-1, 0}))
                {
                    auto& d = m_pData.m_Tiles.emplace_back();
                    d.m_SpriteIndex = m_pSelectedSpriteIndex;

                    generateTileCollisionData(m_pData.m_Tiles.size() - 1, m_pAddTileSolid);

                    m_pTileAddOpen = false;
                }
            }

            ImGui::Separator();

            ImGui::TextUnformatted("Solid:");
            ImGui::SameLine();
            ImGui::Checkbox("##solid", &m_pAddTileSolid);

            if (ImGui::BeginChild("##tiles", ImGui::GetContentRegionAvail()))
            {
                const auto& sprites = m_pAtlas->GetSprites();

                float width = ImGui::GetContentRegionAvail().x - ImGui::GetStyle().ScrollbarSize;

                float size = 32.f;

                float num = width / (size + ImGui::GetStyle().ItemSpacing.x);

                float rest = num - (float) ((int32_t) num);

                if (num < 1)
                    num = 1;

                int32_t xCount = num;

                size = ((rest / xCount) + 1) * size;

                int32_t xCounter = 0;

                auto drawList = ImGui::GetWindowDrawList();
                auto startCursor = ImGui::GetCurrentWindow()->DC.CursorPos;
                auto cursor = startCursor;

                for (int32_t i = 0; i < sprites.size(); i++)
                {
                    cursor = ImGui::GetCurrentWindow()->DC.CursorPos;
                    xCounter++;

                    const auto& s = sprites[i];

                    ImRect bb{cursor, cursor + ImVec2{size, size}};

                    ImGui::ItemSize(bb.GetSize());

                    auto hovered = false;
                    auto col = IM_COL32(0, 255, 0, 255);

                    if (!ImGui::ItemAdd(bb, ImGui::GetID(fmt::format("##{}", i).c_str())))
                    {
                        goto after;
                    }

                    hovered = ImGui::IsItemHovered();
                    col = hovered ? IM_COL32(0, 255, 0, 255) : IM_COL32_WHITE;

                    if (i == m_pSelectedSpriteIndex)
                    {
                        drawList->AddRect(bb.GetTL(), bb.GetBR(), IM_COL32(0, 255, 0, 255), 0, 0, 2);
                    }

                    drawList->AddImage(m_pTexture.GetHandle(), bb.GetTL(), bb.GetBR(), {s.m_UV.x, s.m_UV.y},
                                       {s.m_UV.z, s.m_UV.w}, col);


                    if (ImGui::IsItemClicked())
                    {
                        m_pSelectedSpriteIndex = i;
                    }

                    after:

                    if (xCounter == xCount)
                    {
                        xCounter = 0;
                    } else
                    {
                        ImGui::SameLine();
                    }
                }
            }
            ImGui::EndChild();

            ImGui::End();

        }
    }

    void TilesetEditor::regenerateCollisionForTile(uint32_t tileIndex)
    {
        ENSURE_TRUE(m_pDataState == HasData);
        ENSURE_TRUE(tileIndex < m_pData.m_Tiles.size())

        auto& tile = m_pData.m_Tiles[tileIndex];

        tile.m_Collision.m_Edges.clear();

        auto collisionData = Lookup::sc_EdgeLookup[tile.m_Collision.m_VertFlags];

        for (int32_t i = 1; i < collisionData.size(); ++i)
        {
            auto& edge = tile.m_Collision.m_Edges.emplace_back();
            edge.m_Index1 = collisionData[i - 1];
            edge.m_Index2 = collisionData[i - 0];
        }

        if (!collisionData.empty())
        {
            auto& edge = tile.m_Collision.m_Edges.emplace_back();
            edge.m_Index1 = collisionData[collisionData.size() - 1];
            edge.m_Index2 = collisionData[0];
        }

        tile.m_Collision.m_IsSolid = (collisionData == Lookup::sc_EdgeLookup[255]); // 255 Is all colliders == FULLY SOLID
    }
}