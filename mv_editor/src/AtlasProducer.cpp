//
// Created by Matty on 2021-10-28.
//

#include <filesystem>
#include "../editor/resources/AtlasProducer.h"
#include "../editor/resources/ProducerUtils.h"
#include "serialization/common/Archive.h"
#include "serialization/common/SerializationWhitelist.h"
#include "serialization/common/ClassSerializer.h"
#include "serialization/binary/BinaryArchiveProcessor.h"

namespace Editor {
    MV::string AtlasProducer::CreateAtlasResource(MV::AtlasData &data, const MV::string &path, bool overwrite) {
        using namespace Producer;

        auto finalPath = ProducerUtils::ResolvePath(path, overwrite);

        MV::Archive ar;
        MV::SerializationWhitelist wl;

        const auto res = MV::ClassSerializer<MV::AtlasData>::Write(data, ar, wl);

        if(!res) return "";

        MV::BinaryArchiveProcessor proc;

        auto writeRes = proc.WriteToFile(finalPath, ar);

        if(writeRes != MV::ArchiveProcessor::Ok) {
            return "";
        }

        return finalPath;
    }
}
