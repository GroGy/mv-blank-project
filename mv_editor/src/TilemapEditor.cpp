//
// Created by Matty on 2022-01-24.
//

#include "tools/tilemap_editor/TilemapEditor.h"
#include "common/TextureAsset.h"
#include "tools/tilemap_editor/Util.h"
#include "util/UtilGui.h"
#include "imgui/imgui.h"

#ifndef IMGUI_DEFINE_MATH_OPERATORS
#define IMGUI_DEFINE_MATH_OPERATORS
#endif

#include "imgui/imgui_internal.h"
#include "common/EngineCommon.h"
#include "tools/tilemap_editor/tools/TilemapEditorTools.h"
#include "render/vulkan/VulkanTexture.h"
#include "imgui/imgui_impl_vulkan.h"
#include "resources/TilemapProducer.h"
#include "EditorCommon.h"
#include "profiler/Profiler.h"
#include "render/common/Renderer.h"
#include "cvar/CVars.h"

namespace Editor
{
    namespace CVar
    {
        static float s_VertStopRenderingZoom = 1.5f;
        static float s_GridStopFadeStartRenderingZoom = 1.5f;
        static float s_GridStopRenderingZoom = 1.0f;
    }

    static MV::CVarRef CVar_TilemapEditor_VertStopRenderingZoom{
            "mv.editor.TilemapEditor.vert_stop_rendering_zoom",
            "Value on which collision verts are no longer rendered",
            CVar::s_VertStopRenderingZoom
    };

    static MV::CVarRef CVar_TilemapEditor_GridStopRenderingZoom{
            "mv.editor.TilemapEditor.grid_stop_rendering_zoom",
            "Value on which grid is no longer rendered",
            CVar::s_GridStopRenderingZoom
    };

    static MV::CVarRef CVar_TilemapEditor_GridStopFadeStartRenderingZoom{
            "mv.editor.TilemapEditor.grid_fade_start_rendering_zoom",
            "Value on which grid is starting to fade to no longer rendered",
            CVar::s_GridStopFadeStartRenderingZoom
    };

    void TilemapEditor::WindowSetup()
    {

    }

    void TilemapEditor::Render()
    {
        MV::AllocatorTagScope allocTag{MV::AllocationType::Editor};
        checkAllResources();
        checkCollisionGenerationTask();
        m_pHighlight.clear();

        ScopedVertical verticalWrapper{"##TilemapEditor_vertical", ImGui::GetContentRegionAvail()};

        {
            if (ImGui::BeginChild("##TilemapEditor_top_bar",
                                  {-1.f, ImGui::CalcTextSize("###").y + ImGui::GetStyle().ItemSpacing.x * 2}))
            {
                drawTopBar();
            }
            ImGui::EndChild();
        }

        ScopedHorizontal hor{"##TilemapEditor_horizontal"};

        // Draw left panel
        if (ImGui::BeginChild("##TilemapEditor_left_panel",
                              ImVec2{
                                      ImGui::GetContentRegionAvail().x / 6.0f - ImGui::GetStyle().ItemSpacing.x,
                                      -1})
                )
        {
            drawTileSelection();
        }
        ImGui::EndChild();

        if (ImGui::BeginChild("##TilemapEditor_right_panel_with_viewport"))
        {
            ScopedHorizontal horViewport{"##TilemapEditor_horizontal_viewport_part", {-1, -1}};
            const float rightPanelSize = ImGui::GetFontSize() + ImGui::GetStyle().ItemSpacing.x * 2;
            if (ImGui::BeginChild("##TilemapEditor_viewport",
                                  {ImGui::GetContentRegionAvail().x - rightPanelSize - ImGui::GetStyle().ItemSpacing.x,
                                   0}, true))
            {

                drawViewport(ImGui::GetWindowPos(), ImGui::GetWindowPos() + ImGui::GetWindowSize());

            }
            ImGui::EndChild();

            ImGui::Spring(0);
            if (ImGui::BeginChild("##TilemapEditor_right_panel_buttons", {rightPanelSize, 0}))
            {
                drawTools();
            }
            ImGui::EndChild();
        }
        ImGui::EndChild();
    }

    void TilemapEditor::updateActiveTool(const ImVec2& windowMin, const ImVec2& windowMax)
    {
        m_pLockZoomInput = false;
        bool tilemapDirty = false;

        TilemapEditorToolContext context{m_pData, m_pHighlight, tilemapDirty};

        context.m_CameraPosition = m_pCameraPosition;
        context.m_TileSizePx = m_pTileSizePx;
        context.m_WindowMin = windowMin;
        context.m_WindowMax = windowMax;
        context.m_SelectedTile = m_pSelectedTile;

        if (m_pActiveTool != -1)
            m_pTools[m_pActiveTool]->Update(context);

        m_pLockZoomInput = context.m_LockScrollInput;

        if (tilemapDirty)
        {
            GetEditor()->MarkAssetDirtyForWorld(m_pTilemapID);
        }
    }

    void TilemapEditor::PostRender()
    {

    }

    TilemapEditor::TilemapEditor() : Window(Config::sc_TilemapEditorWindowName.data(), true)
    {
        m_pOpen = false;
    }

    void TilemapEditor::OnCleanup()
    {

    }

    void TilemapEditor::OnInit()
    {
        m_pTools.emplace_back(MV::make_rc<BrushTilemapEditorTool>());
        m_pTools.emplace_back(MV::make_rc<EraserTilemapEditorTool>());
        m_pTools.emplace_back(MV::make_rc<FillTilemapEditorTool>());
    }

    void TilemapEditor::drawTileSelection()
    {
        if (!m_pTileset)
        {
            ScopedVertical vert{"TilemapEditor_TileSelection_VertWrapper", ImGui::GetContentRegionAvail()};
            ImGui::Spring();
            {
                ScopedHorizontal hor{"TilemapEditor_TileSelection_HorWrapper"};
                if (m_pDataState == TilemapEditorDataState::NoData)
                {
                    ImGui::TextUnformatted("No tileset loaded.");
                } else
                {
                    ImGui::ProgressBar(0.5f, ImVec2(-FLT_MIN, 0), "Loading...");
                }
            }
            ImGui::Spring();
            return;
        }

        const float buttonSize = (ImGui::GetFontSize() * 2) + ImGui::GetStyle().ItemSpacing.x * 2;

        const uint32_t buttonsPerRow =
                ImGui::GetContentRegionAvail().x / (buttonSize + ImGui::GetStyle().ItemSpacing.x * 2);

        for (uint32_t i = 0; i < m_pTileset->GetTiles().size(); i++)
        {
            const auto& tile = m_pTileset->GetTiles()[i];

            bool pressed = false;

            const auto& uv = tile.GetUV();

            const auto cursor = ImGui::GetCurrentWindow()->DC.CursorPos;

            if (m_pTexture.GetLoadState() != EditorTexture::Loaded)
            {
                pressed = ImGui::Button(fmt::format("##tile_{}", i + 1).c_str(), {buttonSize, buttonSize});
            } else
            {
                ImGui::PushID(fmt::format("##tile_{}", i + 1).c_str());
                pressed = ImGui::ImageButton(m_pTexture.GetHandle(), {buttonSize, buttonSize}, {uv.x, uv.y},
                                             {uv.z, uv.w});
                ImGui::PopID();
            }

            if (i + 1 == m_pSelectedTile)
            {
                auto* drawList = ImGui::GetWindowDrawList();

                drawList->AddRect(cursor,
                                  cursor + ImVec2{buttonSize, buttonSize} + ImGui::GetStyle().FramePadding * 2,
                                  ImGui::ColorConvertFloat4ToU32({0.1f, 0.88f, 0.1f, 1.0f}),
                                  ImGui::GetStyle().FrameRounding, 0,
                                  2.0f);
            }

            if ((i + 1) % buttonsPerRow != 0)
            {
                ImGui::SameLine();
            }

            if (pressed)
                m_pSelectedTile = i + 1;
        }
    }

    void TilemapEditor::drawTools()
    {
        auto* window = ImGui::GetCurrentWindow();
        const float btnSize = ImGui::GetContentRegionAvail().x;
        for (int32_t i = 0; i < m_pTools.size(); i++)
        {
            auto& tool = m_pTools[i];
            const bool isActive = m_pActiveTool == i;

            const auto cursor = window->DC.CursorPos;

            if (ImGui::Button(fmt::format("{}##_button_{}", tool->GetIconGlyph().data(), i).c_str(),
                              {btnSize, btnSize}))
            {
                m_pActiveTool = i;
            }

            if (isActive)
            {
                auto prev = ImGui::GetStyleColorVec4(ImGuiCol_Button);

                //ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{prev.x + 0.1f, prev.y + 0.1f, prev.z + 0.1f, 1.0f});
                auto* drawList = ImGui::GetWindowDrawList();

                drawList->AddRect(cursor,
                                  cursor + ImVec2{btnSize, btnSize},
                                  ImGui::ColorConvertFloat4ToU32({0.1f, 0.88f, 0.1f, 1.0f}),
                                  ImGui::GetStyle().FrameRounding, 0,
                                  2.0f);
            }
        }
    }

    void TilemapEditor::drawViewport(const ImVec2& windowMin, const ImVec2& windowMax)
    {
        MV_PROFILE_FUNCTION("TilemapEditor::drawViewport");
        if (m_pDataState != TilemapEditorDataState::HasData)
        {
            drawMissingData();
            return;
        }

        if (m_pState == TilemapEditorState::Idle)
        {
            updateActiveTool(windowMin, windowMax);
        }

        processInput(windowMin, windowMax);

        drawBackground(windowMin, windowMax);

        drawView(windowMin, windowMax);

        drawHighlight(windowMin, windowMax);

        if (m_pShowCollision)
            drawCollision(windowMin, windowMax);

        // Draw grid after everything else
        if (m_pShowGrid)
            drawGrid(windowMin, windowMax);
    }

    void TilemapEditor::drawMissingData() const
    {
        ScopedVertical vert{"TilemapEditorVertWrapper", ImGui::GetContentRegionAvail()};
        ImGui::Spring();
        {
            ScopedHorizontal hor{"TilemapEditorHorWrapper"};
            if (m_pDataState == TilemapEditorDataState::NoData)
            {
                ImGui::TextUnformatted("No tilemap loaded.");
            } else
            {
                ImGui::ProgressBar(0.5f, ImVec2(-FLT_MIN, 0), "Loading...");
            }
        }
        ImGui::Spring();
    }

    void TilemapEditor::LoadTilemap(MV::AssetReference<MV::TilemapAssetType> tilemapID)
    {
        MV::AllocatorTagScope allocTag{MV::AllocationType::Editor};
        m_pNewTilemapToLoad = tilemapID;
        m_pLoadNewData = true;
    }

    void TilemapEditor::checkAllResources()
    {
        if (m_pLoadNewData)
        {
            m_pLoadNewData = false;

            loadNewData();
        }

        if (m_pDataState == TilemapEditorDataState::NoData)
            return;

        if (m_pResource && m_pDataState == TilemapEditorDataState::Loading)
        {
            if (!m_pResource->IsFinished())
                return;

            m_pDataState = TilemapEditorDataState::HasData;
            m_pData = m_pResource->m_Result;
            m_pHasTilemap = true;
            m_pResource.reset();
            resetSettings();
        }

        if (!m_pTileset)
        {
            if (MV::GetEngine()->m_ResourceManager.IsAssetLoaded(m_pData.m_Tileset))
            {
                m_pTileset = MV::GetResources()->m_Tilesets[m_pData.m_Tileset];
                if (!m_pTileset)
                {
                    ENSURE_NO_ENTRY();
                    return;
                }
            } else if (!m_pTilesetResource)
            {
                m_pTilesetResource = eastl::dynamic_pointer_cast<MV::TilesetResource>(
                        MV::GetEngine()->m_ResourceManager.LoadResource(m_pData.m_Tileset));
                return;
            } else if (!(m_pTilesetResource->IsFinished() && m_pTilesetResource->Success()))
            {
                return;
            }

            if (m_pTilesetResource)
            {
                if (MV::GetEngine()->m_ResourceManager.IsAssetLoaded(m_pData.m_Tileset))
                {
                    m_pTileset = MV::GetResources()->m_Tilesets[m_pData.m_Tileset];
                }
                m_pTilesetResource.reset();
                return;
            }
        }

        if (m_pTexture.GetLoadState() == EditorTexture::Unloaded)
        {
            if (!m_pAtlas)
            {
                if (MV::GetEngine()->m_ResourceManager.IsAssetLoaded(m_pTileset->GetAtlasAsset()))
                {
                    m_pAtlas = MV::GetResources()->m_Atlases[m_pTileset->GetAtlasAsset()];
                } else if (!m_pAtlasResource)
                {
                    m_pAtlasResource = eastl::dynamic_pointer_cast<MV::AtlasResource>(
                            MV::GetEngine()->m_ResourceManager.LoadResource(m_pTileset->GetAtlasAsset()));
                    return;
                } else if (!(m_pAtlasResource->IsFinished() && m_pAtlasResource->Success()))
                {
                    return;
                }

                if (m_pAtlas && m_pAtlasResource)
                {
                    m_pAtlasResource.reset();
                }
            }

            auto foundTexture = MV::GetResources()->m_Textures.find(m_pAtlas->GetTextureAssetID());
            if (foundTexture == MV::GetResources()->m_Textures.end())
            {
                MV::GetEngine()->m_ResourceManager.LoadResource(m_pAtlas->GetTextureAssetID());
                return;
            }

            if (!foundTexture->second->AssetLoaded())
            {
                return;
            }

            m_pTexture.Load(foundTexture->second->GetTextureHandle());
        }
    }

    void TilemapEditor::loadNewData()
    {
        clearData();
        m_pTilemapID = m_pNewTilemapToLoad;

        m_pNewTilemapToLoad = MV::InvalidUUID;

        if (MV::GetEngine()->m_ResourceManager.IsAssetLoaded(m_pTilemapID))
        {
            m_pDataState = TilemapEditorDataState::HasData;
            m_pData = MV::GetResources()->m_Tilemaps[m_pTilemapID];
            m_pHasTilemap = true;
            resetSettings();
        } else
        {
            m_pResource = eastl::dynamic_pointer_cast<MV::TilemapResource>(
                    MV::GetEngine()->m_ResourceManager.LoadResource(m_pTilemapID));
            m_pDataState = TilemapEditorDataState::Loading;
        }
    }

    void TilemapEditor::drawBackground(const ImVec2& windowMin, const ImVec2& windowMax)
    {
        auto* drawList = ImGui::GetWindowDrawList();
        drawList->AddRectFilled(windowMin, windowMax, IM_COL32(20, 20, 20, 100));

        drawList->AddRectFilled(WorldSpaceToViewSpace({0, 0}, windowMin, windowMax),
                                WorldSpaceToViewSpace({m_pData.m_Width, m_pData.m_Height}, windowMin, windowMax),
                                IM_COL32(30, 30, 30, 100));
    }

    void TilemapEditor::drawGrid(const ImVec2& windowMin, const ImVec2& windowMax)
    {
        float zoom = m_pTileSizePx / m_pBaseTileSizePx;

        if(zoom < CVar::s_GridStopRenderingZoom) {
            return;
        }

        ImDrawList* drawList = ImGui::GetWindowDrawList();

        const ImVec2 windowSize = windowMax - windowMin;

        const ImRect worldCameraViewRect{
                ImVec2{
                        m_pCameraPosition.x - ((windowSize.x / m_pTileSizePx)),
                        m_pCameraPosition.y - ((windowSize.y / m_pTileSizePx))
                },
                ImVec2{
                        m_pCameraPosition.x + ((windowSize.x / m_pTileSizePx)),
                        m_pCameraPosition.y + ((windowSize.y / m_pTileSizePx))
                }
        };

        ImVec2 gridMin = WorldSpaceToViewSpace({0, 0}, windowMin, windowMax);
        ImVec2 gridMax = WorldSpaceToViewSpace({m_pData.m_Width, m_pData.m_Height}, windowMin, windowMax);

        gridMin.x = (std::max)(gridMin.x, windowMin.x);
        gridMin.y = (std::max)(gridMin.y, windowMin.y);
        gridMax.x = (std::min)(gridMax.x, windowMax.x);
        gridMax.y = (std::min)(gridMax.y, windowMax.y);

        auto color = m_pGridColor;

        for (int32_t x = 0; x <= m_pData.m_Width; x++)
        {
            if (x < worldCameraViewRect.Min.x || x > worldCameraViewRect.Max.x)
            {
                continue;
            }

            float lineX = WorldSpaceToViewSpace({x, 0}, windowMin,
                                                windowMax).x;//(windowMin.x + windowSize.x / 2.0f) + (((x - m_pCameraPosition.x) * m_pBaseTileSizePx) / GetZoom());

            drawList->AddLine(
                    ImVec2{
                            lineX, gridMin.y
                    },
                    ImVec2{
                            lineX, gridMax.y
                    }, ImGui::ColorConvertFloat4ToU32(color), GetGridLineWidth()
            );
        }

        for (int32_t y = 0; y <= m_pData.m_Height; y++)
        {
            if (y < worldCameraViewRect.Min.y || y > worldCameraViewRect.Max.y)
            {
                continue;
            }

            const float viewWidthBase = windowMax.y - windowMin.y;

            float lineY = WorldSpaceToViewSpace({0, y}, windowMin,
                                                windowMax).y;// (windowMin.y + windowSize.y / 2.0f) + (((y - m_pCameraPosition.y) * m_pBaseTileSizePx) / GetZoom());

            drawList->AddLine(
                    ImVec2{
                            gridMin.x, lineY
                    },
                    ImVec2{
                            gridMax.x, lineY
                    }, ImGui::ColorConvertFloat4ToU32(m_pGridColor), GetGridLineWidth()
            );
        }
    }

    void TilemapEditor::drawView(const ImVec2& windowMin, const ImVec2& windowMax)
    {
        //TODO: for each tile in view, draw it (kinda expansive, but this is editor)
        MV_PROFILE_FUNCTION("TilemapEditor::drawView");
        const auto windowSize = windowMax - windowMin;
        const auto windowSizeTiles = windowSize / (m_pTileSizePx / 2);

        auto drawList = ImGui::GetWindowDrawList();

        ENSURE_TRUE(windowSizeTiles.x > 0 && windowSizeTiles.y > 0)

        int32_t xScreenTiles = std::ceil(windowSizeTiles.x) + 2;
        int32_t yScreenTiles = std::ceil(windowSizeTiles.y) + 2;

        for (int32_t x = 0; x < xScreenTiles; x++)
        {
            for (int32_t y = 0; y < yScreenTiles; y++)
            {
                const int32_t rX = (int32_t) m_pCameraPosition.x - (xScreenTiles / 2) + x;
                const int32_t rY = (int32_t) m_pCameraPosition.y - (yScreenTiles / 2) + y;

                if (rX < 0 || rY < 0 || rX >= m_pData.m_Width || rY >= m_pData.m_Height)
                    continue;

                const auto& tile = m_pData.m_Data[rY * m_pData.m_Width + rX];

                if (tile.m_AtlasIndex == 0)
                    continue;

                if (m_pTexture.GetLoadState() != EditorTexture::Loaded)
                {
                    auto vs = WorldSpaceToViewSpace(glm::vec2{rX, rY + 1}, windowMin, windowMax);

                    drawList->AddRectFilled(vs, vs + (ImVec2{m_pTileSizePx / 2.0f, m_pTileSizePx / 2.0f}),
                                            ImGui::ColorConvertFloat4ToU32({1, 1, 1, 1}));
                    continue;
                }


                auto uv = m_pTileset->GetTile(tile.m_AtlasIndex).GetUV();

                auto vs = WorldSpaceToViewSpace(glm::vec2{rX, rY + 1}, windowMin, windowMax);
                drawList->AddImage(m_pTexture.GetHandle(),
                                   vs, vs + (ImVec2{m_pTileSizePx / 2.0f, m_pTileSizePx / 2.0f}),
                                   ImVec2{uv.x, uv.y},
                                   ImVec2{uv.z, uv.w}
                );
            }
        }
    }

    void TilemapEditor::resetSettings()
    {
        m_pTileSizePx = m_pBaseTileSizePx;
        if (m_pHasTilemap)
        {
            m_pCameraPosition.x = (float) m_pData.m_Width / 2.0f;
            m_pCameraPosition.y = (float) m_pData.m_Height / 2.0f;
        }
    }

    void TilemapEditor::processInput(const ImVec2& windowMin, const ImVec2& windowMax)
    {
        if (m_pIsDraggingCamera)
        {
            if (ImGui::IsMouseDragging(ImGuiMouseButton_Middle))
            {
                auto mousePos = ImGui::GetMousePos();

                auto mouseDelta = mousePos - ImVec2{m_pMouseDragStart.x, m_pMouseDragStart.y};

                const ImVec2 windowSize = windowMax - windowMin;

                mouseDelta /= windowSize;

                mouseDelta.x *= -1;

                const ImVec2 windowTileScale = windowSize / m_pTileSizePx * 2.0f;

                const auto screenDelta = (mouseDelta * windowTileScale);

                m_pCameraPosition = m_pCameraDragStart + glm::vec2{screenDelta.x, screenDelta.y};
            } else
            {
                m_pIsDraggingCamera = false;
            }
        }

        ImRect bb{
                windowMin,
                windowMax
        };

        if (bb.Contains(ImGui::GetMousePos()))
        {
            if (!m_pIsDraggingCamera && ImGui::IsMouseDragging(ImGuiMouseButton_Middle))
            {
                m_pIsDraggingCamera = true;
                m_pCameraDragStart = m_pCameraPosition;
                m_pMouseDragStart = {ImGui::GetMousePos().x, ImGui::GetMousePos().y};
            }

            if (!m_pLockZoomInput)
            {
                const float scroll = ImGui::GetIO().MouseWheel;
                m_pTileSizePx = std::clamp(m_pTileSizePx + scroll * 4, 8.f, 96.0f);
            }
            //m_pZoomValue = std::clamp(m_pZoomValue - scroll, 1U, 64U);
        }

    }

    ImVec2 TilemapEditor::WorldSpaceToViewSpace(const glm::vec2& worldPosition, const ImVec2& windowMin,
                                                const ImVec2& windowMax)
    {
        return ::Editor::WorldSpaceToViewSpace(worldPosition, m_pCameraPosition, windowMin, windowMax, m_pTileSizePx);
    }

    void TilemapEditor::drawHighlight(const ImVec2& windowMin, const ImVec2& windowMax)
    {
        ImDrawList* drawList = ImGui::GetWindowDrawList();

        const float tileRealScreenSize = m_pTileSizePx;
        const ImVec2 windowSize = windowMax - windowMin;

        for (auto&& hl: m_pHighlight)
        {
            if (hl.x >= m_pData.m_Width)
            {
                continue;
            }
            if (hl.y >= m_pData.m_Height)
            {
                continue;
            }

            auto vs = WorldSpaceToViewSpace(glm::vec2{hl.x, hl.y + 1}, windowMin, windowMax);

            drawList->AddRectFilled(vs, vs + (ImVec2{m_pTileSizePx / 2.0f, m_pTileSizePx / 2.0f}),
                                    ImGui::ColorConvertFloat4ToU32(m_pTileHighlight));
        }

    }

    glm::vec2
    TilemapEditor::ViewSpaceToWorldSpace(const ImVec2& screenPos, const ImVec2& windowMin, const ImVec2& windowMax)
    {
        return ::Editor::ViewSpaceToWorldSpace(screenPos, m_pCameraPosition, windowMin, windowMax, m_pTileSizePx);
    }

    float TilemapEditor::GetGridLineWidth() const
    {
        return m_pGridBaseWidth;/// (m_pBaseTileSizePx / 24.0f);
    }

    void TilemapEditor::drawTopBar()
    {
        float value = (m_pTileSizePx / m_pBaseTileSizePx) * 100.0f;
        float min = (8.f / m_pBaseTileSizePx) * 100.0f;
        float max = (96.0f / m_pBaseTileSizePx) * 100.0f;
        ImGui::PushItemWidth(ImGui::CalcTextSize("ZOOOOOOOOOOOOOOM").x);
        if (ImGui::SliderFloat("##zoom", &value, min, max, "%.0f%%"))
        {
            m_pTileSizePx = m_pBaseTileSizePx * (value / 100.f);
        }
        ImGui::SameLine();
        ImGui::SeparatorEx(ImGuiSeparatorFlags_Vertical);
        ImGui::SameLine();
        {
            ScopedDisabled dis{m_pState != TilemapEditorState::Idle || m_pDataState == TilemapEditorDataState::NoData};
            if (ImGui::Button("Save"))
            {
                m_pState = TilemapEditorState::Saving;
                //TODO change to async process
                auto path = MV::GetEngine()->GetProject().m_Assets[m_pTilemapID].m_Path;

                //TODO: When creating atlas, we should specify size of each tile, so that we can get size based from that, tileSize / atlas->GetPixelsPerUnit();
                m_pData.m_PixelScale = 1.0f;

                auto resPath = TilemapProducer::CreateTilemapResource(m_pData, path, true);

                MV::GetEngine()->m_ResourceManager.UnloadAsset(m_pTilemapID);   // Forces reimport in world
                MV::GetEngine()->m_ResourceManager.LoadResource(m_pTilemapID);  // Forces reimport in world

                m_pState = TilemapEditorState::Idle;
            }
        }
        ImGui::SameLine();
        ImGui::SeparatorEx(ImGuiSeparatorFlags_Vertical);
        ImGui::SameLine();
        ImGui::Checkbox("Grid", &m_pShowGrid);
        ImGui::SameLine();
        {
            ScopedDisabled dis{m_pState != TilemapEditorState::Idle ||
                               m_pDataState == TilemapEditorDataState::NoData ||
                               !m_pData.m_HasCollisionData};
            ImGui::Checkbox("Show collision", &m_pShowCollision);
            ImGui::SameLine();
        }
        ImGui::SeparatorEx(ImGuiSeparatorFlags_Vertical);
        ImGui::SameLine();
        {
            ScopedDisabled dis{m_pState != TilemapEditorState::Idle || m_pDataState == TilemapEditorDataState::NoData};
            if (ImGui::Button(m_pGenerateCollisionTask ? "Baking..." : "Bake collision"))
            {
                m_pState = TilemapEditorState::Baking;
                GenerateTilemapCollisionTaskInput input{m_pData};
                input.m_Tileset = m_pTileset;
                m_pGenerateCollisionTask = MV::make_rc<GenerateTilemapCollisionTask>(MV::move(input));
                m_pGenerateCollisionTask->Start(m_pGenerateCollisionTask);
            }
        }

        if (m_pGenerateCollisionTask)
        {
            ImGui::SameLine();
            ImGui::ProgressBar(m_pGenerateCollisionTask->GetProgress(), {ImGui::CalcTextSize("ZOOOOOOOOOOOOOOM").x, 0});
        }

        ImGui::SameLine();
        ImGui::SeparatorEx(ImGuiSeparatorFlags_Vertical);
    }

    void TilemapEditor::clearData()
    {
        m_pTexture.Unload();

        m_pHasTilemap = false;
        m_pData = {};

        m_pTileset.reset();
        m_pTilesetResource.reset();

        m_pAtlas.reset();
        m_pAtlasResource.reset();

        m_pTilemapID = 0;

        m_pDataState = TilemapEditorDataState::NoData;
    }

    void TilemapEditor::drawCollision(const ImVec2& windowMin, const ImVec2& windowMax)
    {
        if (m_pGenerateCollisionTask) return;

        ImDrawList* drawList = ImGui::GetWindowDrawList();

        float zoom = m_pTileSizePx / m_pBaseTileSizePx;

        // Edges
        for (uint32_t i = 0; i < m_pData.m_Collision.m_Vertices.size(); i++)
        {
            const auto& colBodyVerts = m_pData.m_Collision.m_Vertices[i];
            const auto bodyColor = i * 31;

            static MV::vector<ImVec2> col;
            col.resize(colBodyVerts.size());

            for (uint32_t j = 0; j < colBodyVerts.size(); j++)
            {
                const auto& p = colBodyVerts[j];

                auto vs = WorldSpaceToViewSpace(p, windowMin, windowMax);

                col[j] = vs;


                auto p1 = WorldSpaceToViewSpace(colBodyVerts[j], windowMin, windowMax);
                auto p2 = WorldSpaceToViewSpace(colBodyVerts[j == colBodyVerts.size() - 1 ? 0 : j + 1], windowMin,
                                                windowMax);

                bool p1Clipped = false;
                if(p1.x < windowMin.x || p1.x > windowMax.x || p1.y < windowMin.y || p1.y > windowMax.y) p1Clipped = true;
                if(p1Clipped && (p2.x < windowMin.x || p2.x > windowMax.x || p2.y < windowMin.y || p2.y > windowMax.y)) continue;

                drawList->AddLine(p1, p2,
                                  IM_COL32(100 + bodyColor % 150, 255 - (bodyColor % 200), ((bodyColor % 240) + 30),
                                           255), 5.0f);
            }

            //drawList->AddConvexPolyFilled(col.data(), colBodyVerts.size(), IM_COL32(100 + bodyColor % 150, 255 - (bodyColor % 200), ((bodyColor % 240) + 30), 100));
        }

        // Verts
        if (zoom > CVar::s_VertStopRenderingZoom)
        {
            for (uint32_t i = 0; i < m_pData.m_Collision.m_Vertices.size(); i++)
            {
                const auto& colBodyVerts = m_pData.m_Collision.m_Vertices[i];

                MV::vector<ImVec2> col;
                col.resize(colBodyVerts.size());

                for (uint32_t j = 0; j < colBodyVerts.size(); j++)
                {
                    const auto& p = colBodyVerts[j];

                    auto vs = WorldSpaceToViewSpace(p, windowMin, windowMax);

                    if(vs.x < windowMin.x || vs.x > windowMax.x || vs.y < windowMin.y || vs.y > windowMax.y) continue;

                    drawList->AddCircleFilled(vs, m_pTileSizePx / 16.0f, IM_COL32(0, 30, 240, 255), 4);;
                }

                //drawList->AddConvexPolyFilled(col.data(), col.size(), IM_COL32(0, 30, 240, 100));
            }
        }

    }

    void TilemapEditor::checkCollisionGenerationTask()
    {
        if (!m_pGenerateCollisionTask)
            return;

        if (!m_pGenerateCollisionTask->IsDone())
            return;

        if (!m_pGenerateCollisionTask->Success())
        {
            MV_LOG("Failed to generate collision for map with error {}", m_pGenerateCollisionTask->GetErrorText());
            m_pGenerateCollisionTask.reset();
            return;
        }

        m_pState = TilemapEditorState::Idle;

        auto& collision = m_pData.m_Collision;

        collision = MV::move(m_pGenerateCollisionTask->m_Result);

        m_pData.m_HasCollisionData = !collision.m_Vertices.empty();

        m_pGenerateCollisionTask.reset();
    }
}
