//
// Created by Matty on 2022-09-03.
//

#include "interface/common/FileBrowser.h"
#include "platform/windows/WindowsFileBrowser.h"

namespace Editor {
    MV::rc<Editor::FileBrowser> FileBrowser::Get(FileBrowserMode mode){
#if WIN32
        return MV::rc<WindowsFileBrowser>(new WindowsFileBrowser(mode));
#else
        return {};
#endif
    }

    FileBrowser::FileBrowser(FileBrowserMode mode) :
        m_pMode(mode)
    {

    }

    const MV::vector<MV::string> &FileBrowser::GetSelectedFiles() const {
        return m_pFiles;
    }

    const MV::string &FileBrowser::GetSelectedFile() const {
        return m_pFile;
    }

    const MV::string &FileBrowser::GetSavePath() const {
        return m_pSave;
    }

    void FileBrowser::SetTypeFilters(const MV::vector<MV::string> & fileFilters) {
        m_pFileFilters = fileFilters;
    }

    void FileBrowser::SetMode(FileBrowserMode mode)
    {
        m_pMode = mode;
    }
}
