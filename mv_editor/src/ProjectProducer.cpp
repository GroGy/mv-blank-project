//
// Created by Matty on 2021-10-23.
//

#include <common/EngineCommon.h>
#include <json.hpp>
#include <filesystem>
#include <fstream>
#include "../editor/resources/ProjectProducer.h"
#include "fmt/format.h"
#include "Engine.h"

namespace Editor {
    MV::Project ProjectProducer::CreateProject(const MV::string& name) {
        MV::Project res{};
        res.m_Name = name.c_str();
        res.m_Version = MV::GetEngine()->GetVersion();
        return res;
    }

    void ProjectProducer::SaveProject(MV::Project &proj, const MV::string &path) {
        nlohmann::json result;

        result[sc_JsonName.data()] = proj.m_Name.c_str();
        result[sc_JsonVersion.data()] = proj.m_Version;
        result[sc_JsonDefaultWorld.data()] = proj.m_DefaultWorld.operator MV::UUID();
        result[sc_JsonGameClass.data()] = proj.m_GameClass.GetHash();

        addAssets(proj, result);

        std::ofstream out{path.c_str()};

        if(out.fail()) {
            throw std::exception();
        }

        if(out.is_open()) {
            out << result.dump(4);
        }

        if(out.fail()) {
            throw std::exception();
        }

        out.close();
    }

    void ProjectProducer::addAssets(MV::Project &proj, nlohmann::json &json) {
        nlohmann::json assetsJson{};

        for(auto && asset : proj.m_Assets) {
            nlohmann::json singleAssetJson{};

            auto& registry = MV::AssetTypeRegistry::Get();

            singleAssetJson[sc_JsonAssetType.data()] = registry.m_RuntimeLookup[asset.second.m_Type.m_LookupID].m_RegistryName.c_str();
            singleAssetJson[sc_JsonAssetPath.data()] = asset.second.m_Path.c_str();

            assetsJson[std::to_string(asset.first)] = singleAssetJson;
        }


        json[sc_JsonAssets.data()] =assetsJson.empty() ? nlohmann::json::array() : assetsJson;
    }

    const MV::string_view FORBIDDEN_CHARACTERS = "></|\"\\:?*";

    MV::string ProjectProducer::CreateProjectInDirectory(MV::Project &proj, const MV::string &path) {
        auto pathOutFS = std::filesystem::path(path.c_str());

        auto nameFixed = proj.m_Name;

        std::replace_if(nameFixed.begin(), nameFixed.end(), [](char & c)-> bool{
            return std::any_of(FORBIDDEN_CHARACTERS.begin(), FORBIDDEN_CHARACTERS.end(), [&](const auto & b) -> bool {
                return c == b;
            });
        }, '_');

        pathOutFS /= fmt::format("{}.proj",nameFixed);

        std::filesystem::create_directories(pathOutFS.parent_path());

        SaveProject(proj,pathOutFS.string().c_str());

        return pathOutFS.string().c_str();
    }
};