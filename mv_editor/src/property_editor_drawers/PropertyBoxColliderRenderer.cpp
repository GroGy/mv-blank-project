//
// Created by Matty on 2022-07-12.
//
#include "properties/viewport_renderers/PropertyBoxColliderRenderer.h"
#include "2d/BoxColliderProperty.h"
#include "common/EngineCommon.h"

#include "imgui/imgui.h"
#include "ext/matrix_transform.hpp"
#include "Editor.h"
#include "EditorCommon.h"

namespace Editor {
    static constexpr auto sc_BaseVertices = {
            glm::vec2{0.5f, -0.5f},
            glm::vec2{-0.5f, -0.5f},
            glm::vec2{-0.5f, 0.5f},
            glm::vec2{0.5f, 0.5f}
    };

    void BoxColliderPropertyViewportRenderer::RenderProperty(const glm::vec4 &viewport, MV::Property *propertyIn) {
        auto* property = (MV::BoxColliderProperty*)propertyIn;
        const auto engine = MV::GetEngine();
        const auto & world = *property->m_WorldObject->GetOwningWorld();

        if(!world.IsValid()) return;

        const auto drawList = ImGui::GetWindowDrawList();

        const auto wo = property->m_WorldObject;

        const auto woPos = wo->GetPosition();
        const auto woScale = wo->GetScale();
        const auto woRot = wo->GetRotation();

        // Remapping from -1 - 1 to viewport min - max
        const glm::vec2 viewScale = {
                (viewport.z - viewport.x) / 2.0f,
                (viewport.w - viewport.y) / 2.0f
        };

        MV::vector<glm::vec2> positions = sc_BaseVertices;

        glm::mat4 mat = glm::mat4(1.0f);
        mat = glm::translate(mat, {woPos.x + property->m_Offset.x, woPos.y + property->m_Offset.y, 0});
        mat = glm::rotate(mat, glm::radians(property->m_Rotation + woRot), {0, 0, 1});
        mat = glm::scale(mat, {property->m_Scale.x * woScale.x, property->m_Scale.y * woScale.y, 1});

        auto viewportCamera = world.m_CameraOverride ? world.m_CameraOverride : world.m_Camera;

        ENSURE_VALID(viewportCamera);

        const auto & cameraData = viewportCamera->GetCameraData();

        mat = cameraData.m_ProjMatrix * cameraData.m_ViewMatrix * mat;

        for (auto &pos: positions) {
            glm::vec4 posBase = {pos.x, pos.y, 0.0f, 1.0f};

            //// Transform based on world object
            posBase = mat * posBase;

            // To viewport space
            posBase.x = ((posBase.x + 1.0f) * viewScale.x) + viewport.x;
            posBase.y = ((posBase.y + 1.0f) * viewScale.y) + viewport.y;

            pos = glm::vec2{posBase.x, posBase.y};
        }

        drawList->AddQuad(
                ImVec2{positions[0].x, positions[0].y},
                ImVec2{positions[1].x, positions[1].y},
                ImVec2{positions[2].x, positions[2].y},
                ImVec2{positions[3].x, positions[3].y},
                BoxColliderPropertyViewportRenderer::s_pColliderColor,
                3.0f / viewportCamera->GetZoom()
        );
    }

    ImU32 BoxColliderPropertyViewportRenderer::s_pColliderColor = IM_COL32_LIME;
}
