//
// Created by Matty on 2022-01-22.
//

#include "../editor/async/AtlasTask.h"
#include <common/EngineCommon.h>
#include <json.hpp>
#include <filesystem>
#include "../editor/resources/AtlasProducer.h"
#include "../editor/resources/ProjectProducer.h"
#include "../editor/EditorCommon.h"
#include "../editor/resources/ProducerUtils.h"
#include "fmt/format.h"
#include <render/common/Texture.h>
#include <common/TextureAsset.h>

namespace Editor {
    AtlasTask::AtlasTask(AtlasTaskInput input) : m_pInput(std::move(input)) {

    }

    void AtlasTask::Process() {
        m_pProgress = 0.0f;

        try {
            m_pProgress = 0.2f;
            auto res = MV::GetEngine()->m_ResourceManager.LoadResourceSync(m_pInput.m_Texture);
            m_pProgress = 0.4f;
            bool loaded = false;
            do {
                MV::GetEngine()->AddSyncCallback([&]() {
                    loaded = MV::GetEngine()->m_ResourceManager.IsAssetLoaded(m_pInput.m_Texture);
                });
                if(!loaded && res && !res->Success()) {
                    throw Producer::ProducerException("Failed to load input texture.");
                }
            } while (!loaded);
            m_pProgress = 0.6f;
            createAtlasData();
            m_pProgress = 0.8f;
            m_pFinalPath = AtlasProducer::CreateAtlasResource(m_Result, m_pInput.m_Path);
            m_pFinalPath = Producer::ProducerUtils::PathAbsoluteToObjectRelative(m_pFinalPath);
            m_pProgress = 1.0f;
            m_pSuccess = true;
        } catch(std::exception &e) {
            (void)e;

            m_pProgress = 0.0f;
            m_pSuccess = false;
            m_pErrorText = e.what();
        }
    }

    EditorTaskFinishResult AtlasTask::Finish() {
        if (!m_pSuccess) {
            m_pDone = true;
            return EditorTaskFinishResult::FAILED;
        }

        auto &proj = MV::GetEngine()->GetProject();

        auto assetId = Producer::ProducerUtils::GenerateUUID();

        auto& asset = proj.m_Assets[assetId];
        asset.m_Path = m_pFinalPath.data();
        asset.m_Type = MV::AssetTypeRegistry::Get().GetRuntimeAssetType<MV::AtlasAssetType>();

        ProjectProducer::SaveProject(proj, GetEditor()->GetLoadedProjectPath());

        MV::GetEngine()->m_ResourceManager.UnloadAsset(m_pInput.m_Texture);
        MV::GetEngine()->m_ResourceManager.UnloadAsset(assetId);

        m_pDone = true;
        return EditorTaskFinishResult::SUCCESS;
    }

    void AtlasTask::createAtlasData() {
        glm::uvec2 textureResolution{0,0};
        bool success = true;
        MV::GetEngine()->AddSyncCallback([&]() {
            const auto& textures = MV::GetResources()->m_Textures;
            auto found = textures.find(m_pInput.m_Texture);

            if(found == textures.end()) {
                success = false;
                return;
            }

            if(!found->second->AssetLoaded()) {
                success = false;
                return;
            }

            auto data = found->second->GetData();

            textureResolution = {data.m_Width, data.m_Height};
        });

        if(!success)
            throw std::exception();

        if (!m_pInput.m_GenerateSprites)
        {
            m_Result.m_Texture = m_pInput.m_Texture;
            return;
        }

        const auto xSpriteCount = textureResolution.x / m_pInput.m_SpriteSize.x;
        const auto ySpriteCount = textureResolution.y / m_pInput.m_SpriteSize.y;

        const auto spriteCountTotal = xSpriteCount * ySpriteCount;

        if(spriteCountTotal == 0)
            throw Producer::ProducerException(fmt::format("Can't fit single sprite of that size to passed texture ({}x{})", textureResolution.x, textureResolution.y).c_str());

        const auto maxX = (xSpriteCount) * m_pInput.m_SpriteSize.x;
        const auto maxY = (ySpriteCount) * m_pInput.m_SpriteSize.y;

        const auto maxUV_x = (float)textureResolution.x / (float)maxX;
        const auto maxUV_y = (float)textureResolution.y / (float)maxY;

        m_Result.m_Sprites.resize(spriteCountTotal);

        uint32_t i = 0;

        const auto spriteUVSize_x = maxUV_x / (float)xSpriteCount;
        const auto spriteUVSize_y = maxUV_y / (float)ySpriteCount;

        for(uint32_t x = 0; x < xSpriteCount; x++) {
            for(uint32_t y = 0; y < ySpriteCount; y++) {
                const float min_UV_X = spriteUVSize_x * (float)x;
                const float max_UV_X = spriteUVSize_x*(float)(x + 1);
                const float min_UV_Y = spriteUVSize_y*(float)(y + 1);
                const float max_UV_Y = spriteUVSize_y * (float)y;

                m_Result.m_Sprites[i].m_UV = {min_UV_X, min_UV_Y, max_UV_X, max_UV_Y};

                i++;
            }
        }

        m_Result.m_Texture = m_pInput.m_Texture;
    }
}
