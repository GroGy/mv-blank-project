//
// Created by Matty on 19.12.2022.
//

#include "interface/worldobject_renderers/TilemapPropertyRenderer.h"
#include "2d/TilemapProperty.h"
#include "util/UtilGui.h"
#include "interface/common/AssetSelector.h"
#include "common/EngineCommon.h"
#include "interface/object_editor/GeneralFieldRenderers.h"
#include "interface/object_editor/NumericFieldRenderers.h"

namespace Editor {
    bool TilemapPropertyRenderer::Draw(const MV::Meta::Field *field, const MV::rc<MV::WorldObject> &wo,
                                               MV::Property *property) {
        auto data = dynamic_cast<MV::TilemapProperty *>(property);
        bool dirty = false;

        if(!Begin("##tilemap_general_settings")) {
            return false;
        }

        if(BeginField("Tilemap:"))
        {
            /*
            bool open = ImGui2::AssetSelectButton(data->m_TilemapAsset, {ImGui::GetContentRegionAvail().x, 0.0f});
            bool confirm = AssetSelector::DrawAssetSelector("##tilemap_asset_selector", open,
                                                            {MV::TilemapAssetType},
                                                            data->m_TilemapAsset);
            if (confirm) {
                dirty = true;
            }
*/
            EndField();
        }

        if(BeginField("Visible:"))
        {
            if (FieldDrawingImpl::DrawBool("##tilemap_visible_box",data->m_Visible)) {
                dirty = true;
            }
            EndField();
        }

        if(BeginField("Layer:"))
        {
            dirty |= FieldDrawingImpl::DrawI32("##tilemap_layer", data->m_Layer);
            EndField();
        }

        End();

        return dirty;
    }
}