//
// Created by Matty on 2022-09-20.
//

#include <interface/worldobject_renderers/BoxColliderPropertyRenderer.h>
#include <2d/BoxColliderProperty.h>
#include <EngineMinimal.h>
#include <util/UtilGui.h>
#include "interface/object_editor/NumericFieldRenderers.h"

namespace Editor {
    bool BoxColliderPropertyRenderer::Draw(const MV::Meta::Field* field, const MV::rc<MV::WorldObject> & wo, MV::Property *property) {
        auto data = dynamic_cast<MV::BoxColliderProperty *>(property);

        ENSURE_VALID(data, false);
        bool dirty = false;

        if(!Begin("##box_collider_general_settings")) {
            return false;
        }

        if(BeginField("Position:"))
        {
            dirty |= FieldDrawingImpl::DrawVec2("##box_collider_position", data->m_Offset);
            EndField();
        }

        if(BeginField("Scale:"))
        {
            dirty |= FieldDrawingImpl::DrawVec2("##box_collider_scale", data->m_Scale);
            EndField();
        }

        if(BeginField("Rotation:"))
        {
            dirty |= FieldDrawingImpl::DrawFloat("##box_collider_rotation", data->m_Rotation);
            EndField();
        }

        if(BeginField("Density:"))
        {
            dirty |= FieldDrawingImpl::DrawFloat("##box_collider_density", data->m_Density);
            EndField();
        }

        End();

        return dirty;
    }
}
