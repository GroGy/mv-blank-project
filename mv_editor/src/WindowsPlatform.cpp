//
// Created by Matty on 2022-02-10.
//


#ifdef WIN32

#include <windows.h>
#include "../editor/platform/windows/WindowsPlatform.h"
#include <fmt/format.h>
#include <comdef.h>
#include <sstream>
#include <locale>
#include <codecvt>
#include <string>
#include <regex>

namespace Editor {
    void WindowsPlatform::OpenInSystemDefault(MV::string_view path) {
        //SHELLEXECUTEINFO ShExecInfo = {0};
        //ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
        //ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
        //ShExecInfo.hwnd = NULL;
        //ShExecInfo.lpVerb = NULL;
        //ShExecInfo.lpFile = path.data();
        //ShExecInfo.lpParameters = "";
        //ShExecInfo.lpDirectory = NULL;
        //ShExecInfo.nShow = SW_SHOW;
        //ShExecInfo.hInstApp = NULL;
        //ShellExecuteEx(&ShExecInfo);
        ////WaitForSingleObject(ShExecInfo.hProcess, INFINITE);
        //CloseHandle(ShExecInfo.hProcess);

        ShellExecute(0, 0, path.data(), 0, 0, SW_SHOW);
    }

    void WindowsPlatform::RunCommandUnwaited(MV::string_view command, MV::string_view args,
                                             MV::string_view directory) {
        const auto res = ShellExecute(0, "open", command.data(), args.data(), directory.data(),
                                      SW_SHOWNORMAL);

        WaitForInputIdle(res, INFINITE);
    }

    int32_t WindowsPlatform::RunCommandAwait(MV::string_view command, MV::string_view args,
                                             MV::string_view directory, const std::function<void(bool isError,MV::string_view)> & messageClb) {
        std::vector<MV::string> stdOutLines;
        BOOL bSuccess = FALSE;
        DWORD exitCode = 0;
        stdOutLines.clear();

        // Create a pipe for the redirection of the STDOUT
        // of a child process.
        HANDLE g_hChildStd_OUT_Rd = NULL;
        HANDLE g_hChildStd_OUT_Wr = NULL;
        SECURITY_ATTRIBUTES saAttr;
        saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
        saAttr.bInheritHandle = TRUE;
        saAttr.lpSecurityDescriptor = NULL;
        bSuccess = CreatePipe(&g_hChildStd_OUT_Rd,
                              &g_hChildStd_OUT_Wr, &saAttr, 0);
        if (!bSuccess) return bSuccess;
        bSuccess = SetHandleInformation(g_hChildStd_OUT_Rd,
                                        HANDLE_FLAG_INHERIT, 0);
        if (!bSuccess) return bSuccess;

        // Setup the child process to use the STDOUT redirection
        PROCESS_INFORMATION piProcInfo;
        STARTUPINFO siStartInfo;
        ZeroMemory(&piProcInfo, sizeof(PROCESS_INFORMATION));
        ZeroMemory(&siStartInfo, sizeof(STARTUPINFO));
        siStartInfo.cb = sizeof(STARTUPINFO);
        siStartInfo.hStdError = g_hChildStd_OUT_Wr;
        siStartInfo.hStdOutput = g_hChildStd_OUT_Wr;
        siStartInfo.dwFlags |= STARTF_USESTDHANDLES;

        MV::string bCommand = fmt::format("{} {}", command, args).c_str();

        // Execute a synchronous child process & get exit code
        bSuccess = CreateProcess(NULL,
                                 bCommand.data(),  // command line
                                 NULL,                 // process security attributes
                                 NULL,                 // primary thread security attributes
                                 TRUE,                 // handles are inherited
                                 0,                    // creation flags
                                 NULL,                 // use parent's environment
                                 NULL,                 // use parent's current directory
                                 &siStartInfo,         // STARTUPINFO pointer
                                 &piProcInfo);        // receives PROCESS_INFORMATION
        if (!bSuccess) return bSuccess;

        bool isDone = false;

        while(!isDone) {
            auto waitRes = WaitForSingleObject(piProcInfo.hProcess, (DWORD) (50));

            if(waitRes == WAIT_OBJECT_0)
                isDone = true;

            if(waitRes == WAIT_FAILED) {
                isDone = true;
                exitCode = 1;
            }

            // Read the data written to the pipe
            DWORD bytesInPipe = 0;
            while (bytesInPipe == 0) {
                bSuccess = PeekNamedPipe(g_hChildStd_OUT_Rd, NULL, 0, NULL,
                                         &bytesInPipe, NULL);
                if (!bSuccess) return bSuccess;

                if(bytesInPipe == 0) {
                    WaitForSingleObject(piProcInfo.hProcess, (DWORD) (10));
                    if(waitRes == WAIT_OBJECT_0) {
                        goto afterLoop;
                    }
                }
            }
            DWORD dwRead;
            CHAR *pipeContents = new CHAR[bytesInPipe];
            bSuccess = ReadFile(g_hChildStd_OUT_Rd, pipeContents,
                                bytesInPipe, &dwRead, NULL);
            if (!bSuccess || dwRead == 0) return 1;

            /*
            WCHAR * realPipeContents = nullptr;
            auto requiredBytes = MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, pipeContents, bytesInPipe,realPipeContents,0);
            realPipeContents = new WCHAR[requiredBytes];
            MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, pipeContents, bytesInPipe,realPipeContents,requiredBytes);
            */
            // Split the data into lines and add them to the return vector
            std::stringstream stream(pipeContents);
            std::string str;

            while (getline(stream, str)) {
                if(str.length() > 0) {
                    auto res = str.find('\r');

                    str = str.substr(0, res);


                    //str = str.substr(0, str.length() - 1); //Cut new line

                    //std::wstring_convert<std::codecvt_utf8<WCHAR>,WCHAR> cv;
                    //MV::string bts = cv.to_bytes((WCHAR*)str.data()).c_str();

                    //MV::string realString{str.c_str(), str.length()};

                    messageClb(false, str.c_str());
                    stdOutLines.push_back(str.c_str());
                }
            }
        }

        afterLoop:

        if(exitCode == 0)
            GetExitCodeProcess(piProcInfo.hProcess, &exitCode);

        CloseHandle(piProcInfo.hProcess);
        CloseHandle(piProcInfo.hThread);

        // Return if the caller is not requesting the stdout results
        //if (!stdOutLines) return TRUE;

        return exitCode;
    }

    MV::string WindowsPlatform::GetWindowsError(HRESULT hr) {
        _com_error err(hr);

        return {err.ErrorMessage()};
    }

    void WindowsPlatform::OpenFolderInExplorer(MV::string_view path)
    {
        ShellExecute(0, "open", path.data(), 0, 0, SW_SHOW);
    }
}
#endif
