//
// Created by Matty on 2022-06-19.
//

#include "tools/shader_editor/common/ShaderNodePinType.h"

namespace Editor {

    MV::string ShaderNodePinType::ToString(ShaderNodePinType::Flags flag) {
        switch (flag) {
            case FLOAT:
                return "float";
            case VEC2:
                return "vec2";
            case VEC3:
                return "vec3";
            case VEC4:
                return "vec4";
            case BOOL:
                return "bool";
        }
        return {};
    }

    ImColor ShaderNodePinType::GetColor(ShaderNodePinType::Flags flag) {
        switch (flag) {
            case FLOAT:
                return FloatPinColor;
            case VEC2:
                return BasePinColor;
            case VEC3:
                return BasePinColor;
            case VEC4:
                return BasePinColor;
            case BOOL:
                return BasePinColor;
        }
        return BasePinColor;
    }
}