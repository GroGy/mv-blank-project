//
// Created by Matty on 06.04.2023.
//

#include "interface/common/ViewportWidget.h"

#include <utility>
#include "common/Types.h"
#include "imgui/imgui_impl_vulkan.h"
#include "imgui/imgui_internal.h"
#include "interface/common/viewport_gizmo_providers/WorldObjectGizmoProvider.h"
#include "properties/PropertyGizmoManager.h"
#include "render/common/CameraData.h"
#include "render/vulkan/VulkanTextureImpl.h"
#include "imgui/ImGuizmo.h"
#include "ext/matrix_transform.hpp"
#include "gtc/type_ptr.hpp"
#include "EditorCommon.h"
#include "Engine.h"
#include "render/common/Renderer.h"

namespace Editor
{
    static ImVec2 viewBBMin;
    static ImVec2 viewBBSize;

    void IViewportWidget::DrawImpl(MV::string_view label, ImTextureID texture, const ImVec2& size_arg)
    {
        viewBBMin = ImGui::GetCurrentWindow()->DC.CursorPos;
        viewBBSize = ImGui::GetContentRegionAvail();

        ImGui::Image(texture, viewBBSize);

        ImGuizmo::SetDrawlist(ImGui::GetWindowDrawList());
        ImGuizmo::AllowAxisFlip(true);
        ImGuizmo::SetOrthographic(true);
        ImGuizmo::SetRect(viewBBMin.x, viewBBMin.y, viewBBSize.x, viewBBSize.y);
    }

    bool IViewportWidget::recreateCameraTexturesIfNeeded(const MV::rc<MV::Camera>& camera)
    {
        if (camera->GetCameraData().m_Handle == m_pTexturesHandle)
            return false;

        if(m_CameraTexturesState == Pending) return false;

        ENSURE_TRUE(camera->GetCameraData().m_Handle != MV::sc_InvalidCameraHandle, false);

        if(!m_pCameraTextures.empty())
        {
            auto lambda = [textures(m_pCameraTextures)](MV::RenderCommandBuilder & CmdBuilder){
                for (auto&& tex: textures)
                    ImGui_ImplVulkan_RemoveTexture((VkDescriptorSet) tex);
            };

            DISPATCH_RT_COMMAND_LAMBDA_DELAYED(DestroyViewportCameraImGuiDesciptorSets, lambda);
        }

        m_pCameraTextures.clear();

        auto rp = camera->GetCameraData().m_RenderPassFrameBuffers;

        if(!rp.Valid()) return false;

        //TODO: for now we force only first attachment to be here, as its colo

        auto textureCount = MV::GetRenderer()->GetRendererInfo().m_SwapChainImageCount;

        m_pCameraTextures.resize(textureCount);

        m_CameraTexturesState = Pending;

        auto lambda = [rpFbsHandle = rp, texturesState = &m_CameraTexturesState, textures = &m_pCameraTextures, textureCount = textureCount](MV::RenderCommandBuilder & CmdBuilder) {
            MV::rc<MV::RenderPassFrameBuffers> rpFbs;
            ENSURE_TRUE(MV::GetRenderer()->RT_GetResource(rpFbsHandle,rpFbs));

            for (uint32_t i = 0; i < textureCount; i++)
            {
                const auto& colorAttachment = (*rpFbs->RT_GetTexture(CmdBuilder.GetContext(),i))[0];
                auto& texture = colorAttachment;
                auto textureImpl = MV::dynamic_pointer_cast<MV::VulkanTextureImpl>(texture);
                (*textures)[i] =
                        ImGui_ImplVulkan_AddTexture(textureImpl->GetSampler(), textureImpl->m_TextureView,
                                                    VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
            }

            *texturesState = Loaded;
        };

        DISPATCH_RT_COMMAND_LAMBDA(CreateViewportCameraImGuiDesciptorSets, lambda);

        m_pTexturesHandle = camera->GetCameraData().m_Handle;

        return false;
    }

    void IViewportWidget::drawGrid(const MV::rc<MV::Camera>& camera)
    {
        glm::mat4 identity{1.0f};
        const auto camPos = camera->GetPosition();
        identity = glm::rotate(identity, glm::radians(90.0f), {1, 0, 0});
        identity = glm::translate(identity, glm::floor(glm::vec3{camPos.x, 0, -camPos.y}));

        if (camera->GetZoom() < 5.0f)
        {
            ImGuizmo::DrawGrid(glm::value_ptr(camera->GetView()),
                               glm::value_ptr(camera->CalcYInverseProj()), glm::value_ptr(identity),
                               100.0f, glm::floor(camPos.x), glm::floor(-camPos.y));
        }
    }

    void IViewportWidget::drawGizmo(const MV::rc<MV::Camera>& camera)
    {
        if (GetEditor()->GetInputMode() != InputMode::Editor)
        {
            return;
        }

        if (!m_pGizmoProvider)
            return;

        ImGuizmo::OPERATION op;
        try
        {
            switch (m_pViewportOperation)
            {
                case ViewportOperation::Moving:
                    op = ImGuizmo::TRANSLATE_X | ImGuizmo::TRANSLATE_Y;
                    break;
                case ViewportOperation::Rotating:
                    op = ImGuizmo::ROTATE_Z;
                    break;
                case ViewportOperation::Scaling:
                    op = ImGuizmo::SCALE_X | ImGuizmo::SCALE_Y;
                    break;
            }

            auto matrix = m_pGizmoProvider->GetTransform();

            if (ImGuizmo::Manipulate(glm::value_ptr(camera->GetView()),
                                     glm::value_ptr(camera->CalcYInverseProj()),
                                     op,
                                     ImGuizmo::WORLD,
                                     glm::value_ptr(matrix), NULL, NULL) &&
                m_pViewportAction == ViewportAction::None)
            {

                m_pGizmoProvider->SetTransform(matrix);
            }
        } catch (std::exception& e)
        {

        }
    }

    void IViewportWidget::processInput(const ImVec2& viewportSize, const MV::rc<MV::Camera>& camera)
    {
        /*
        if (ImGui::IsWindowFocused()) {
            if (ImGui::IsKeyPressed(EDITOR_KEY_G)) {
                m_pViewportOperation = ViewportOperation::MOVING;
            } else if (ImGui::IsKeyPressed(EDITOR_KEY_R)) {
                m_pViewportOperation = ViewportOperation::ROTATING;
            } else if (ImGui::IsKeyPressed(EDITOR_KEY_W)) {
                m_pViewportOperation = ViewportOperation::SCALING;
            }
        }*/

        if (ImGui::IsItemHovered())
        {
            //Is viewport hovered?
            auto pressed = false;

            if (ImGui::IsMouseDown(ImGuiMouseButton_Middle))
            {
                pressed = true;
                const auto viewportMin = ImGui::GetItemRectMin();
                const auto viewportMax = ImGui::GetItemRectMax();

                if (m_pViewportAction == ViewportAction::None)
                {
                    m_pViewportAction = ViewportAction::Moving;

                    const auto pos = ImGui::GetMousePos();

                    m_pCameraStartPos = camera->GetPosition();
                    m_pStartPos = {pos.x - viewportMin.x, pos.y - viewportMin.y};
                }

                if (m_pViewportAction == ViewportAction::Moving)
                {
                    ImGui::SetMouseCursor(ImGuiMouseCursor_Hand);
                    auto engine = MV::GetEngine();
                    if (engine->GetWorld().IsValid())
                    {
                        const auto pos = ImGui::GetMousePos();
                        const glm::vec2 current = {pos.x - viewportMin.x, pos.y - viewportMin.y};

                        const auto delta = glm::vec2(current.x - m_pStartPos.x, current.y - m_pStartPos.y);

                        auto normDelta = glm::vec2{delta.x / viewportSize.x, delta.y / viewportSize.y};


                        camera->SetPosition(
                                m_pCameraStartPos +
                                glm::vec2{normDelta.x * -(camera->GetViewWidth()) * camera->GetZoom(),
                                          normDelta.y * (camera->GetViewWidth() *
                                                         camera->GetRatio()) * camera->GetZoom()});
                    }
                }
            }

            if (!pressed)
            {
                const auto scroll = ImGui::GetIO().MouseWheel;
                auto engine = MV::GetEngine();
                if (engine->GetWorld().IsValid())
                {
                    camera->SetZoom((glm::max)(camera->GetZoom() + scroll * -0.2f, 1.0f));
                }
            }

            if (!pressed && m_pViewportAction != ViewportAction::None)
            {
                m_pViewportAction = ViewportAction::None;
                ImGui::SetMouseCursor(ImGuiMouseCursor_Arrow);
            }

            if (pressed)
                ImGui::SetWindowFocus();
        } else if (m_pViewportAction != ViewportAction::None)
        {
            m_pViewportAction = ViewportAction::None;
            ImGui::SetMouseCursor(ImGuiMouseCursor_Arrow);
        }
    }

    void IViewportWidget::SetGizmoProvider(MV::rc<IViewportGizmoProvider> provider)
    {
            m_pGizmoProvider = MV::move(provider);
    }

    void IViewportWidget::drawPropertyGizmos(const ImVec2& size)
    {
        if(MV::IsGameRunning() && GetEditor()->GetInputMode() != InputMode::Editor) {
            return;
        }

        static PropertyGizmoManager gizmoManager;

        if (!m_pGizmoProvider) return;

        glm::vec4 vp;


        if (auto v = MV::dynamic_pointer_cast<WorldObjectGizmoProvider>(m_pGizmoProvider)) {
            vp.x = viewBBMin.x;
            vp.y = viewBBMin.y;
            vp.z = viewBBMin.x + viewBBSize.x;
            vp.w = viewBBMin.y + viewBBSize.y;
            gizmoManager.DrawProperties(vp,v->GetWorldObject(), v->GetWorldObjectFieldsOverride());
        }

    }

    glm::mat4 IViewportGizmoProvider::GetTransform() const
    {
        const auto vec2Pos = GetPosition();
        const auto vec2Scale = GetScale();
        const auto vec1Rot = GetRotation();

        glm::vec3 pos{vec2Pos.x, vec2Pos.y, 0};
        glm::vec3 rot{0, 0, vec1Rot};
        glm::vec3 scale{vec2Scale.x, vec2Scale.y, 0};

        glm::mat4 matrix{1.0f};

        ImGuizmo::RecomposeMatrixFromComponents(glm::value_ptr(pos), glm::value_ptr(rot),
                                                glm::value_ptr(scale), glm::value_ptr(matrix));

        return matrix;
    }
}