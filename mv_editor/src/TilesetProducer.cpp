//
// Created by Matty on 2021-11-03.
//

#include "../editor/resources/TilesetProducer.h"
#include "../editor/resources/ProducerUtils.h"
#include "serialization/common/ClassSerializer.h"
#include "serialization/common/SerializationWhitelist.h"
#include "serialization/binary/BinaryArchiveProcessor.h"

namespace Editor {
    MV::string TilesetProducer::CreateTilesetResource(const MV::TilesetData &data, const MV::string &pathOut, bool overwrite) {
        using namespace Producer;

        auto finalPath = ProducerUtils::ResolvePath(pathOut, overwrite);

        MV::Archive ar;
        MV::SerializationWhitelist wl;

        const auto res = MV::ClassSerializer<MV::TilesetData>::Write(data, ar, wl);

        if(!res) return "";

        MV::BinaryArchiveProcessor proc;

        auto writeRes = proc.WriteToFile(finalPath, ar);

        if(writeRes != MV::ArchiveProcessor::Ok) {
            return "";
        }

        return finalPath;
    }
}
