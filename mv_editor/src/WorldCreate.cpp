//
// Created by Matty on 2021-11-03.
//

#include "../editor/interface/WorldCreate.h"
#include "../editor/imgui/imgui.h"
#include "../editor/imgui/imgui_stdlib.h"
#include "../editor/util/UtilGui.h"
#include "../editor/EditorCommon.h"
#include <common/EngineCommon.h>

namespace Editor {
    WorldCreate::WorldCreate() : Window(Config::sc_WorldCreateWindowName.data()) {
        m_pOpen = false;
        m_Flags |= ImGuiWindowFlags_NoDocking;
    }

    void WorldCreate::WindowSetup() {
        const ImGuiViewport* viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->GetCenter(), ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
    }

    void WorldCreate::Render() {
        if (m_pCreating && m_pTask) {
            ImGui::BeginVertical("##main_wrapper", ImGui::GetContentRegionAvail());
            ImGui::ProgressBar(m_pTask->GetProgress(), {-1.0f, 0}, "Creating...");
            if(m_pTask->IsDone()) {
                m_pCreating = false;
                auto engine = MV::GetEngine();
                engine->GetWorld() = MV::World{std::move(m_pTask->m_Result)};
                engine->GetWorld().Init();
                engine->GetWorld().m_ID = m_pTask->m_WorldUUID;
                m_pError = m_pTask->GetErrorText();
                if(m_pTask->Success()) {
                    m_pOpen = false;
                    GetEditor()->OnAssetChange();
                }
                m_pTask.reset();
            }
            ImGui::EndVertical();
            return;
        }

        if(!Begin("Default")) return;

        if(BeginField("World name:"))
        {
            ImGui2::WideInputText("##world_name:", &m_pName);
            EndField();
        }

        End();

        if(ImGui::Button("Create##world_create", {-1,0})) {
            m_pCreating = true;
            WorldTaskInput input{};

            input.m_Name = m_pName;
            input.m_Path = fmt::format("{}/{}.mvasset", m_pPathBase, m_pName).c_str();

            m_pTask = MV::make_rc<WorldTask>(input);
            m_pTask->Start(m_pTask);
        }
    }

    void WorldCreate::PostRender() {

    }

    void WorldCreate::OnCleanup() {

    }

    void WorldCreate::OnInit() {

    }

    void WorldCreate::SetPath(MV::string_view path) {
        m_pPathBase = path;
    }
}
