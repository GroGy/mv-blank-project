//
// Created by Matty on 2021-10-15.
//

#include "tools/shader_editor/ShaderEditor.h"
#include "allocator/Alloc.h"
#include "asset/AssetUUID.h"
#include "common/EngineCommon.h"
#include "common/Types.h"
#include "imgui/imgui.h"
#include "imgui/node_editor/imgui_node_editor.h"
#include "interface/object_editor/GeneralFieldRenderers.h"
#include "precompiler/ShaderEditorRegistrator.h"
#include "render/common/RenderPass.h"
#include "resources/common/ShaderAssetType.h"
#include "tools/shader_editor/ShaderGraphCompiler.h"
#include "tools/shader_editor/common/ShaderNode.h"
#include "util/UtilGui.h"
#include "tools/shader_editor/ResultNode.h"
#include "imgui/imgui_internal.h"
#include "resources/ShaderProducer.h"
#include "tools/shader_editor/nodes/ParameterNodes.h"
#include "common/ShaderAsset.h"
#include "Engine.h"

namespace Editor
{
    void ShaderEditor::WindowSetup()
    {

    }

    static constexpr MV::string_view sc_NodeEditorWindowName = "##shader_editor_node_editor";
    static constexpr MV::string_view sc_SettingsPanelWindowName = "##shader_editor_settings_panel";

    void ShaderEditor::Render()
    {
        if (m_pAsset == MV::InvalidUUID)
        {
            drawNoAssetSelected();
            return;
        }

        drawMenuBar();

        if (m_pShaderResource)
        {
            drawLoadingShader();
            return;
        }

        setupDockspace();

        drawSettings();

        drawNodeEditor();

    }

    void ShaderEditor::PostRender()
    {

    }

    Editor::ShaderEditor::ShaderEditor() : Window(Config::sc_ShaderEditorWindowName.data())
    {
        m_Flags |= ImGuiWindowFlags_MenuBar;
        m_pOpen = false;
    }

    void ShaderEditor::OnCleanup()
    {
    }

    void ShaderEditor::OnInit()
    {
        m_pRenderPassBp = MV::RenderPassBlueprint::DepthTexturePass();
        auto cp = m_pRenderPassBp;

        MV::GetLogger()->LogError("TODO: Implement render pass creation in shader editor.");
        //m_pPreviewRenderPass = MV::GetRenderer()->CreateRenderPass(MV::move(cp));
    }

    void ShaderEditor::Compile()
    {
        MV::AllocatorTagScope allocatorTag{MV::AllocationType::Editor};

        MV::string res{};
        MV::string resV{};

        ShaderGraphCompiler graphCompiler{};

        m_pErrorIds.clear();
        auto result = graphCompiler.CompileShaderCode(m_pNodes, m_pLinks, m_pInputs, m_pData);

        for (auto&& v: result.m_ErrorIds)
            m_pErrorIds.emplace(v);
    }

    bool ShaderEditor::Validate() const
    {
        ShaderGraphCompiler graphCompiler{};
        auto result = graphCompiler.CompileShaderCode(m_pNodes, m_pLinks, m_pInputs, m_pData);
        return result.m_ErrorIds.empty();
    }

    void ShaderEditor::drawNoAssetSelected()
    {
        MV::AllocatorTagScope allocatorTag{MV::AllocationType::Editor};

        ScopedVertical vert{"ShaderEditorVertWrapper", ImGui::GetContentRegionAvail()};
        ImGui::Spring();
        {
            ScopedHorizontal hor{"ShaderEditorHorWrapper"};
            ImGui::Text("No shader loaded.");
        }
        ImGui::Spring();
    }

    bool ShaderEditor::LoadShader(MV::UUID asset)
    {
        MV::AllocatorTagScope allocatorTag{MV::AllocationType::Editor};
        auto* engine = MV::GetEngine();

        const auto foundRes = engine->GetProject().m_Assets.find(asset);

        if (foundRes == engine->GetProject().m_Assets.end())
        {
            MV::GetLogger()->LogWarning("Tried to load asset to shader editor that does not exist!");
            return false;
        }

        if (!MV::Is<MV::ShaderAssetType>(foundRes->second.m_Type))
        {
            MV::GetLogger()->LogWarning("Tried to load asset to shader editor that is not shader!");
            return false;
        }

        auto foundShader = MV::GetResources()->m_Shaders.find(asset);
        if(foundShader == MV::GetResources()->m_Shaders.end()) {
            auto res = engine->m_ResourceManager.LoadResource(asset);
            m_pShaderResource = MV::dynamic_pointer_cast<MV::ShaderResource>(res);

            MV::Assert(m_pShaderResource.operator bool(),"Failed to cast MV::Resource to MV::ShaderResource!");
        } else {
            m_pData = foundShader->second->GetData();
        }

        m_pLinks.clear();
        m_pNodes.clear();
        m_pIdCounter = 0;
        m_pInputs.clear();
        m_pErrorIds.clear();
        m_pPushConstantsCollapsed = true;
        m_pParametersCollapsed = true;

        if (g_Context)
            ed::DestroyEditor(g_Context);

        m_pAsset = asset;

        if(foundShader != MV::GetResources()->m_Shaders.end()) {
            g_Context = ed::CreateEditor();
            auto resultNode = MV::make_rc<ResultNode>(m_pIdCounter);
            m_pNodes[resultNode->GetNodeID()] = eastl::move(resultNode);
        }

        return true;
    }

    void ShaderEditor::drawLoadingShader()
    {
        MV::AllocatorTagScope allocatorTag{MV::AllocationType::Editor};
        if (!m_pShaderResource)
        {
            return;
        }

        if (!m_pShaderResource->IsFinished())
        {
            ScopedVertical vert{"ShaderEditorVertLoadingWrapper", ImGui::GetContentRegionAvail()};
            ImGui::Spring();
            {
                ScopedHorizontal hor{"ShaderEditorHorLoadingWrapper"};
                ImGui::ProgressBar(0.5f, ImVec2(-FLT_MIN, 0), "Loading...");
            }
            ImGui::Spring();
            return;
        }

        if (!m_pShaderResource->Success())
        {
            m_pAsset = MV::InvalidUUID;
            m_pShaderResource.reset();
        }

        g_Context = ed::CreateEditor();
        auto resultNode = MV::make_rc<ResultNode>(m_pIdCounter);
        m_pNodes[resultNode->GetNodeID()] = eastl::move(resultNode);

        m_pData = m_pShaderResource->m_Result;

        m_pShaderResource.reset();
    }

    void ShaderEditor::setupDockspace()
    {
        MV::AllocatorTagScope allocatorTag{MV::AllocationType::Editor};
        auto centerNode = ImGui::GetID("##shader_editor_dockspace");
        ImGui::DockSpace(centerNode);

        //TODO: Maximum size of settings

        if (ImGui::DockBuilderGetNode(centerNode)->IsLeafNode())
        {
            ImGuiID node_editor_nodeId;
            ImGuiID settings_nodeId;
            ImGui::DockBuilderSplitNode(centerNode, ImGuiDir_Left, 0.2f, &settings_nodeId, &node_editor_nodeId);

            const auto viewport_node = ImGui::DockBuilderGetNode(node_editor_nodeId);
            viewport_node->LocalFlags |=
                    static_cast<ImGuiDockNodeFlags_>(ImGuiDockNodeFlags_NoDocking | ImGuiDockNodeFlags_NoTabBar) |
                    ImGuiDockNodeFlags_NoSplit;

            const auto settings_node = ImGui::DockBuilderGetNode(settings_nodeId);
            settings_node->LocalFlags |=
                    static_cast<ImGuiDockNodeFlags_>(ImGuiDockNodeFlags_NoDocking | ImGuiDockNodeFlags_NoTabBar) |
                    ImGuiDockNodeFlags_NoSplit;

            ImGui::DockBuilderDockWindow(sc_NodeEditorWindowName.data(), node_editor_nodeId);
            ImGui::DockBuilderDockWindow(sc_SettingsPanelWindowName.data(), settings_nodeId);

            ImGui::DockBuilderFinish(node_editor_nodeId);
            ImGui::DockBuilderFinish(settings_nodeId);
        }
    }

    void ShaderEditor::drawSettings()
    {
        MV::AllocatorTagScope allocatorTag{MV::AllocationType::Editor};
        if (!ImGui::Begin(sc_SettingsPanelWindowName.data()))
        {
            ImGui::End();
            return;
        }

        bool dirty = false;

        drawShaderSettings(dirty);

        if (dirty)
            m_AssetChanged = true;

        if (ImGui::Button("COMPILE", {-1, 0}))
        {
            Compile();
        }

        drawPushConstants();

        drawParams();

        ImGui::End();
    }

    void ShaderEditor::drawNodeEditor()
    {
        MV::AllocatorTagScope allocatorTag{MV::AllocationType::Editor};
        if (!ImGui::Begin(sc_NodeEditorWindowName.data()))
        {
            ImGui::End();
            return;
        }

        ed::SetCurrentEditor(g_Context);

        setupStyle();
        ed::PushStyleVar(ax::NodeEditor::StyleVar_AntiAliasedLines, 0.f);

        ed::Begin("ShaderEditor");

        for (auto&& node: m_pNodes)
        {
            node.second->Draw();
        }

        for (auto&& link: m_pLinks)
        {
            ed::Link(link.m_Id, link.m_StartPin, link.m_EndPin);
        }

        ed::Suspend();

        static ImVec2 openPopupPosition;

        if (ed::ShowBackgroundContextMenu())
        {
            ImGui::OpenPopup("create_node_popup");
            openPopupPosition = ImGui::GetMousePos();
        }

        drawCreateNodePopup(openPopupPosition);

        ed::Resume();

        processCreate();
        processAltLinkDelete();
        processDelete();

        ed::End();
        ed::PopStyleVar();

        if (ImGui::BeginDragDropTarget())
        {
            ImGuiDragDropFlags target_flags = 0;
            target_flags |= ImGuiDragDropFlags_AcceptNoDrawDefaultRect; // Don't display the yellow rectangle
            if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("##shader_editor_dnd_param", target_flags))
            {
                auto bigType = m_pData.m_Uniforms[m_pDraggingParamIndex].m_Type;
                auto type = m_pData.m_Uniforms[m_pDraggingParamIndex].m_BufferDataType;

                if(bigType == 0 && type == 7) {
                    for(auto & c : RegisteredShaderNodes::GetInstance().m_NodeDataLookup) {
                        if(c.m_NodeName == "ParameterV4") {
                            auto node = c.m_CreateCallback(m_pIdCounter);
                            auto castedNode = MV::dynamic_pointer_cast<IParameterNode>(node);

                            castedNode->m_ParameterId = m_pDraggingParamIndex;
                            castedNode->SetNodeName(m_pData.m_Uniforms[m_pDraggingParamIndex].m_Name);

                            auto id = node->GetNodeID();

                            if (node->ShouldCompilePreviewShader())
                            {
                                MV::Assert(false, "UNIMPLEMENTED DUE TO RT REWRITE!");
                                /*
                                node->m_pPreviewFrameBuffers = MV::make_rc<MV::RenderPassFrameBuffers>();
                                node->m_pPreviewFrameBuffers->InitForRenderPass(m_pPreviewRenderPass);*/
                            }

                            m_pNodes[node->GetNodeID()] = eastl::move(node);

                            ed::SetNodePosition(id, ed::ScreenToCanvas(ImGui::GetMousePos()));
                            break;
                        }
                    }
                }
            }
            ImGui::EndDragDropTarget();
        }

        ImGui::End();
    }

    void ShaderEditor::processCreate()
    {
        if (ed::BeginCreate(ImColor(255, 255, 255), 2.0f))
        {
            ed::PinId startPinId;
            ed::PinId endPinId;

            if (ed::QueryNewLink(&startPinId, &endPinId))
            {
                processNewLink(startPinId, endPinId);
            } else {

            }
        }
        ed::EndCreate();
    }

    void ShaderEditor::processAltLinkDelete()
    {
        MV::vector<ed::LinkId> possibleIds;
        possibleIds.resize(64);

        static int32_t prevSelectedLinkCount = 0;
        int32_t selectedLinkCount = ed::GetSelectedLinks(possibleIds.data(), possibleIds.size());

        if (selectedLinkCount > 0 && prevSelectedLinkCount == 0 && ImGui::IsKeyDown(ImGuiKey_LeftAlt))
        {
            for (uint32_t i = 0; i < selectedLinkCount; i++)
            {
                ed::DeleteLink(possibleIds[i]);
            }
        }

        prevSelectedLinkCount = selectedLinkCount;
    }

    bool ShaderEditor::processNewLink(const ed::PinId& startPinId, const ed::PinId& endPinId)
    {
        enum FoundPinsState
        {
            None,
            Input,
            Output,
            Both
        };

        if (!startPinId || !endPinId)
        {
            ed::RejectNewItem({1, 0, 0, 1});
            return false;
        }

        uint32_t inputPinId = 0;
        uint32_t outputPinId = 0;

        MV::rc<ShaderNode> inputNode;
        MV::rc<ShaderNode> outputNode;

        FoundPinsState foundPins = None;

        for (auto&& v: m_pNodes)
        {
            bool foundPin = false;
            const auto& inputs = v.second->GetInputPins();

            for (const auto& p: inputs)
            {
                if (p.m_Id == startPinId.Get() || p.m_Id == endPinId.Get())
                {
                    if (foundPins == Both || foundPins == Input)
                    {
                        ed::RejectNewItem({1, 0, 0, 1});
                        return false;
                    }

                    if (foundPins == None)
                    {
                        foundPins = Input;
                    }

                    if (foundPins == Output)
                    {
                        foundPins = Both;
                    }

                    outputNode = v.second;
                    outputPinId = p.m_Id;
                    foundPin = true;
                    break;
                }
            }

            if (foundPin) continue;
            if (foundPins == Both) break;

            const auto& outputs = v.second->GetOutputPins();
            for (const auto& p: outputs)
            {
                if (p.m_Id == startPinId.Get() || p.m_Id == endPinId.Get())
                {
                    if (foundPins == Both || foundPins == Output)
                    {
                        ed::RejectNewItem({1, 0, 0, 1});
                        return false;
                    }

                    if (foundPins == None)
                    {
                        foundPins = Output;
                    }

                    if (foundPins == Input)
                    {
                        foundPins = Both;
                    }

                    inputNode = v.second;
                    inputPinId = p.m_Id;
                    foundPin = true;
                    break;
                }
            }

            if (foundPins == Both) break;
        }

        const bool onDifferentNodes = inputNode && outputNode && inputNode->GetNodeID() != outputNode->GetNodeID();

        if (!onDifferentNodes)
        {
            ed::RejectNewItem({1, 0, 0, 1});
            return false;
        }

        const bool onInputAndOutput = foundPins == Both;

        if (!onInputAndOutput)
        {
            ed::RejectNewItem({1, 0, 0, 1});
            return false;
        }

        if (ed::AcceptNewItem({0, 1, 0, 1}, 4.0f))
        {
            NodeEditorLink newLink{};

            newLink.m_Id = ++m_pIdCounter;
            newLink.m_InputNode = inputNode->GetNodeID();
            newLink.m_OutputNode = outputNode->GetNodeID();
            newLink.m_StartPin = inputPinId;
            newLink.m_EndPin = outputPinId;


            for (auto& input: outputNode->GetInputPins())
            {
                if (input.m_Id == outputPinId)
                {
                    if (input.m_ConnectedPin != 0)
                    {
                        // Disconnect previous connection
                        for (uint32_t i = 0; i < m_pLinks.size(); i++)
                        {
                            if (m_pLinks[i].m_StartPin == input.m_ConnectedPin &&
                                m_pLinks[i].m_EndPin == outputPinId)
                            {
                                auto node = m_pNodes[m_pLinks[i].m_InputNode];

                                for (auto&& pin: node->m_pOutputPins)
                                {
                                    if (pin.m_Id == m_pLinks[i].m_StartPin)
                                    {
                                        for (uint32_t j = 0; j < pin.m_ConnectedOutputPins.size(); j++)
                                        {
                                            auto& output = pin.m_ConnectedOutputPins[j];
                                            if (output == input.m_Id)
                                            {
                                                pin.m_ConnectedOutputPins.erase(pin.m_ConnectedOutputPins.begin() + j);
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                }

                                m_pLinks.erase(m_pLinks.begin() + i);
                                break;
                            }
                        }
                    }

                    input.m_ConnectedPin = inputPinId;
                    break;
                }
            }

            for (auto& output: inputNode->GetOutputPins())
            {
                if (output.m_Id == inputPinId)
                {
                    output.m_ConnectedOutputPins.emplace_back(outputPinId);
                    break;
                }
            }

            m_pLinks.push_back(MV::move(newLink));

            return true;
        }

        // Update data in node

        return false;
    }

    void ShaderEditor::drawShaderSettings(bool& dirty)
    {
        if (ImGui::BeginTable("##shader_editor_settings", 2, ImGuiTableFlags_NoSavedSettings, {-1, 0}))
        {
            ImGui::TableNextRow();

            if (BeginField("Alpha blending"))
            {
                dirty |= FieldDrawingImpl::DrawBool("##alpha_blending", m_pData.m_EnableAlphaBlending);

                EndField();
            }

            ImGui::EndTable();
        }
    }

    void ShaderEditor::drawMenuBar()
    {
        if (ImGui::BeginMenuBar())
        {
            if (ImGui::BeginMenu("File"))
            {
                {
                    ScopedDisabled dis{!m_AssetChanged};
                    if (ImGui::MenuItem("Save"))
                    {
                        auto path = MV::GetEngine()->GetProject().m_Assets[m_pAsset].m_Path;
                        auto resPath = ShaderProducer::CreateShaderResource(m_pData, path, true);

                        MV::GetEngine()->m_ResourceManager.UnloadAsset(m_pAsset);

                        m_AssetChanged = false;
                    }
                }

                ImGui::EndMenu();
            }

            ImGui::EndMenuBar();
        }
    }

    void ShaderEditor::setupStyle()
    {
        auto& neStyle = ed::GetStyle();

        neStyle.NodeRounding = 0.0f;
        neStyle.NodePadding = {2, 2, 2, 2};
        neStyle.Colors[ax::NodeEditor::StyleColor_Bg] = UtilGui::FixImGuiColorSpace({0.1f, 0.1f, 0.13f, 1.0f});
    }

    void ShaderEditor::drawCreateNodePopup(const ImVec2& clickPos)
    {
        if (ImGui::BeginPopup("create_node_popup"))
        {
            for (auto&& v: RegisteredShaderNodes::GetInstance().m_NodeDataLookup)
            {
                if (!v.m_ShowInCreatePopup) continue;
                if (ImGui::MenuItem(v.m_NodeName.c_str()))
                {
                    auto node = v.m_CreateCallback(m_pIdCounter);
                    auto id = node->GetNodeID();

                    if (node->ShouldCompilePreviewShader())
                    {
                        MV::Assert(false, "UNIMPLEMENTED DUE TO RT REWRITE!");
                        /*
                        node->m_pPreviewFrameBuffers = MV::make_rc<MV::RenderPassFrameBuffers>();
                        node->m_pPreviewFrameBuffers->InitForRenderPass(m_pPreviewRenderPass);*/
                    }

                    m_pNodes[node->GetNodeID()] = eastl::move(node);

                    ed::SetNodePosition(id, ed::ScreenToCanvas(clickPos));
                }
            }
            ImGui::EndPopup();
        }
    }

    void ShaderEditor::OnWorldRender()
    {
        MV::CameraData cData;

        //cData.m_RenderPass = m_pPreviewRenderPass;
        cData.m_ViewMatrix = m_PreviewViewMatrix;
        cData.m_ProjMatrix = m_PreviewProjMatrix;

        for (auto&& node: m_pNodes)
        {
            if (!node.second->ShouldCompilePreviewShader()) continue;
            auto nd = node.second;

            if (!nd->m_pPreviewPipeline) continue;

            //cData.m_RenderPassFrameBuffers = nd->m_pPreviewFrameBuffers;

            /*
            MV::PipelineBlueprint bp;

            MV::GetRenderer()->CreateShader();

            nd->m_pPreviewPipeline = MV::GetRenderer()->CreateShaderPipeline()
*/
            //TODO: Draw
        }
    }

    void ShaderEditor::processDelete()
    {
        if (ed::BeginDelete())
        {
            ed::LinkId linkId;

            while (ed::QueryDeletedLink(&linkId))
            {
                deleteLink(linkId.Get());
            }

            ed::NodeId nodeId;
            while (ed::QueryDeletedNode(&nodeId))
            {
                deleteNode(nodeId.Get());
            }
        }
        ed::EndDelete();
    }

    void ShaderEditor::deleteLink(uint32_t id)
    {
        NodeEditorLink link{};
        size_t linkIndex = 0;

        for (uint32_t i = 0; i < m_pLinks.size(); i++)
        {
            auto& v = m_pLinks[i];
            if (v.m_Id == id)
            {
                link = v;
                linkIndex = i;
                break;
            }
        }


        {
            auto node = m_pNodes[link.m_InputNode];
            for (auto&& pin: node->m_pOutputPins)
            {
                if (pin.m_Id == link.m_StartPin)
                {
                    for (uint32_t j = 0; j < pin.m_ConnectedOutputPins.size(); j++)
                    {
                        auto& output = pin.m_ConnectedOutputPins[j];
                        if (output == link.m_EndPin)
                        {
                            pin.m_ConnectedOutputPins.erase(pin.m_ConnectedOutputPins.begin() + j);
                            break;
                        }
                    }
                    break;
                }
            }
        }

        {
            auto node = m_pNodes[link.m_OutputNode];
            for (auto&& pin: node->m_pInputPins)
            {
                if (pin.m_ConnectedPin == link.m_StartPin && pin.m_Id == link.m_EndPin)
                {
                    pin.m_ConnectedPin = 0;
                    break;
                }
            }
        }
        m_pLinks.erase(m_pLinks.begin() + linkIndex);

    }

    void ShaderEditor::deleteNode(uint32_t id)
    {
        if(id == 1) return;

        auto foundNode = m_pNodes.find(id);

        ENSURE_TRUE(foundNode != m_pNodes.end());

        auto node = foundNode->second;

        for(auto && p : node->GetInputPins()) {
            if(p.m_ConnectedPin == 0) continue;

            for(auto && l : m_pLinks) {
                if(l.m_EndPin == p.m_Id) {
                    deleteLink(l.m_Id);
                    break;
                }
            }
        }

        for(auto && p : node->GetOutputPins()) {
            if(p.m_ConnectedOutputPins.empty()) continue;

            MV::vector<uint32_t> toRemove;

            for(auto && l : m_pLinks) {
                if(l.m_StartPin == p.m_Id) {
                    toRemove.emplace_back(l.m_Id);
                    break;
                }
            }

            for(auto & i : toRemove)
            {
                deleteLink(i);
            }
        }

        m_pNodes.erase(id);
    }

    void ShaderEditor::drawPushConstants()
    {
        ScopedDisabled dis{};
        if(ImGui::TreeNodeEx("Push constants",
                          ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_NoTreePushOnOpen |
                          ImGuiTreeNodeFlags_SpanFullWidth)) {
            if(ImGui::Button("Add", {0,-1})) {

            }
        }
    }

    void ShaderEditor::drawParams()
    {
        if(ImGui::TreeNodeEx("Parameters",
                             ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_NoTreePushOnOpen |
                             ImGuiTreeNodeFlags_SpanFullWidth)) {
            if(ImGui::Button("Add", {-1,0})) {
                auto & newV = m_pData.m_Uniforms.emplace_back();
                newV.m_Name = "New";
                newV.m_Nullable = true;
            }

            static const char* paramTypes[] = {
                    "texture",
                    "int32",
                    "uint32",
                    "int8",
                    "uint8",
                    "float32",
                    "vec2",
                    "vec3",
                    "vec4",
                    "mat4"
            };

            if (ImGui::BeginTable("##shader_editor_settings_params", 2, ImGuiTableFlags_NoSavedSettings, {-1, 0}))
            {
                ImGui::TableNextRow();

                for(uint32_t j = 0; j < m_pData.m_Uniforms.size(); j++) {
                    auto & param = m_pData.m_Uniforms[j];

                    if (BeginField(param.m_Name))
                    {
                        //ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
                        if(ImGui::BeginCombo(fmt::format("##param_{}", j).c_str(), (param.m_Type == 1) ? paramTypes[0] : paramTypes[param.m_BufferDataType + 1])) {
                            for(uint32_t i = 0; i < IM_ARRAYSIZE(paramTypes); i++) {
                                if(ImGui::Selectable(paramTypes[i], (i == 0 && param.m_Type == 1) || param.m_BufferDataType == i)) {
                                    if(i == 0) {
                                        param.m_Type = 1;
                                    } else {
                                        param.m_Type = 0;
                                    }
                                    param.m_BufferDataType = i - 1;
                                }
                                //TODO: Check if any nodes with reference to this are in graph, change them
                            }

                            ImGui::EndCombo();
                        }
                        ImGui::SmallButton(fmt::format("X##_{}", j).c_str());

                        ImGuiDragDropFlags src_flags = 0;
                        src_flags |= ImGuiDragDropFlags_SourceNoDisableHover;
                        src_flags |= ImGuiDragDropFlags_SourceNoHoldToOpenOthers;

                        if (ImGui::BeginDragDropSource(src_flags))
                        {
                            ImGui::Text("Moving \"%s\"", m_pData.m_Uniforms[j].m_Name.c_str());
                            m_pDraggingParamIndex = j;
                            ImGui::SetDragDropPayload("##shader_editor_dnd_param", &m_pDraggingParamIndex, sizeof(int));
                            ImGui::EndDragDropSource();
                        }

                        EndField();
                    }
                }

                ImGui::EndTable();
            }
        }
    }

    void ShaderEditor::drawDragAndDrop()
    {

    }
}
