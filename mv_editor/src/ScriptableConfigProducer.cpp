//
// Created by Matty on 11.02.2023.
//

#include "resources/ScriptableConfigProducer.h"
#include "resources/ProducerUtils.h"
#include "common/EngineCommon.h"
#include "render/common/PropertyManager.h"
#include "Engine.h"
#include "serialization/binary/BinaryArchiveProcessor.h"

namespace Editor
{
    using namespace Producer;

    MV::string
    ScriptableConfigProducer::CreateConfigResource(const MV::ScriptableConfigData& data, const MV::string& pathOut,
                                                   bool overwrite)
    {
        auto finalPath = ProducerUtils::ResolvePath(pathOut, overwrite);

        MV::Archive ar;
        MV::SerializationWhitelist wl;

        const auto res = MV::ClassSerializer<MV::ScriptableConfigData>::Write(data, ar, wl);

        if(!res) return "";

        MV::BinaryArchiveProcessor proc;

        auto writeRes = proc.WriteToFile(finalPath, ar);

        if(writeRes != MV::ArchiveProcessor::Ok) {
            return "";
        }

        return finalPath;
    }

    ScriptableConfigProducer& ScriptableConfigProducer::GetInstance()
    {
        static ScriptableConfigProducer instance;
        return instance;
    }

    MV::string ScriptableConfigProducer::GetPath(MV::string_view filename)
    {
        return GetAssetPath(filename);
    }
}