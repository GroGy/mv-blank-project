//
// Created by Matty on 2021-10-28.
//

#include <random>
#include <filesystem>
#include <common/EngineCommon.h>
#include "../editor/resources/ProducerUtils.h"
#include "../vendor/stb_image.h"
#include "Engine.h"

namespace Editor {
    namespace Producer {
        void ProducerUtils::WriteHeader(std::ofstream
                                        &out,
                                        MV::AssetType type, uint32_t
                                        version) {
            out.write(reinterpret_cast<const char *>(&type), sizeof(type));
            out.write(reinterpret_cast <const char *>(&version), sizeof(version));
        }

        MV::TextureData ProducerUtils::ReadImage(const MV::string &path) {
            int32_t width, height, textChannels;

            MV::TextureData res{};

            std::string s = path.c_str();

            stbi_set_flip_vertically_on_load(false);
            auto *data = stbi_load(s.c_str(), &width, &height, &textChannels, 4);

            if (!data) throw ProducerException("Failed to read image data");

            auto size = width * height * textChannels;

            res.m_TextureChannels = textChannels;
            res.m_Width = width;
            res.m_Height = height;
            res.m_Data.resize(size);
            memcpy(res.m_Data.data(), data, size);

            stbi_image_free(data);

            return res;
        }

        MV::UUID ProducerUtils::GenerateUUID() {
            static thread_local std::random_device rd;
            static thread_local std::mt19937 gen(rd());

            std::uniform_int_distribution<MV::UUID> dis(
                    std::numeric_limits<MV::UUID>::min(),
                    std::numeric_limits<MV::UUID>::max()
            );

            return dis(gen);
        }

        MV::string ProducerUtils::ResolvePath(const MV::string &inputPath, bool override, MV::string_view extension) {

            auto pathOutFS = std::filesystem::path(MV::GetEngine()->GetProject().m_Path.c_str());

            pathOutFS /= inputPath.c_str();

            std::filesystem::create_directories(pathOutFS.parent_path());

            auto finalPath = pathOutFS.string();

            if(override)
                return finalPath.c_str();

            if (std::filesystem::exists(pathOutFS)) {
                auto parent = pathOutFS.parent_path().string();
                auto name = pathOutFS.stem().string();

                uint32_t counter = 1;

                auto newPath = std::filesystem::path(
                        parent + "/" + name + "_" + std::to_string(counter++) + extension.data());

                while (std::filesystem::exists(newPath)) {
                    newPath = std::filesystem::path(parent + "/" + name + "_" + std::to_string(counter++) + extension.data());
                }

                finalPath = newPath.string();
            }

            return finalPath.c_str();
        }

        MV::string ProducerUtils::PathAbsoluteToObjectRelative(MV::string_view absPath) {
            return std::filesystem::relative(absPath.data(),MV::GetEngine()->GetProject().m_Path.c_str() ).string().c_str();
        }

        MV::string ProducerUtils::PathObjectRelativeToAbsolute(MV::string_view relativePath)
        {
            std::filesystem::path proj{MV::GetEngine()->GetProject().m_Path.c_str()};
            std::filesystem::path rel{relativePath.data()};

            proj /= rel;

            return proj.string().c_str();
        }

        ProducerException::~ProducerException() noexcept = default;

        const char *ProducerException::what() const noexcept {
            return m_pWhat.data();
        }

        ProducerException::ProducerException(MV::string what) : m_pWhat(std::move(what)){

        }
    }
}