//
// Created by Matty on 07.12.2022.
//

#include "interface/common/ClassSelector.h"
#include "imgui/imgui.h"
#include "common/EngineCommon.h"
#include "Engine.h"

namespace Editor {
    void ClassSelector::SetFilter(const MV::Meta::Class &base) {
        m_pBaseClass = base;
        m_pHasFilter = true;
    }

    void ClassSelector::ClearFilters() {
        m_pHasFilter = false;
    }

    void ClassSelector::Render() {
        if (m_pWasOpened) {
            ImGui::OpenPopup(Config::sc_ClassSelectorWindowName.data());
            auto engine = MV::GetEngine();

            MV_LOG("TODO: Cant do this yet, as no MetaCache is not yet present. {}:{}", __FILE__, __LINE__);
            /*
            m_pData = engine->m_ScriptManager.GetClassesThatAreChildOfClass(m_pBaseClass);*/
            m_pWasOpened = false;
        }

        if (ImGui::BeginPopup(Config::sc_ClassSelectorWindowName.data(), ImGuiWindowFlags_NoMove)) {
            uint32_t counter = 0;
            ImGui::BeginVertical("##class_selector_wrapper");
            if (ImGui::BeginListBox("##class_selector_class_list")) {
                for (auto &&c: m_pData)
                    /*
                    if (ImGui::Selectable(fmt::format("{}##_counter_{}", c.GetClassName().data(), counter++).c_str(),
                                          m_pSelected.GetClassName() == c.GetClassName())) {
                        m_pSelected = c;
                    }*/

                if (ImGui::IsItemHovered()) {
                    if (ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left)) {
                        m_pFinished = true;
                        ImGui::CloseCurrentPopup();
                    }
                }
            }
            ImGui::EndListBox();

            ImGui::BeginHorizontal("##asset_selector_button_wrapper");
            ImGui::Spring();
            if (ImGui::Button("Confirm")) {
                m_pFinished = true;
                ImGui::CloseCurrentPopup();
            }
            ImGui::EndHorizontal();
            ImGui::EndVertical();
            ImGui::EndPopup();
        }
    }

    void ClassSelector::Open() {
        m_pWasOpened = true;
    }

    void ClassSelector::Clear() {
        m_pFinished = false;
    }
}