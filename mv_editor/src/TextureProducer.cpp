//
// Created by Matty on 2021-10-28.
//

#include <fstream>
#include <filesystem>
#include "../editor/resources/TextureProducer.h"
#include "../editor/resources/ProducerUtils.h"
#include "serialization/binary/BinaryArchiveProcessor.h"
#include "serialization/common/ClassSerializer.h"
#include "serialization/common/SerializationWhitelist.h"

namespace Editor
{
    MV::string TextureProducer::CreateTextureResource(const MV::string& pathIn, const MV::string& directory)
    {
        using namespace Producer;

        auto textureData = ProducerUtils::ReadImage(pathIn);

        std::filesystem::path p{pathIn.c_str()};
        std::filesystem::path dirPath{directory.c_str()};

        dirPath /= p.stem();

        auto pathOut = MV::string(dirPath.string().c_str()) + ".mvasset";

        auto finalPath = ProducerUtils::ResolvePath(pathOut, false);

        MV::Archive ar;
        MV::SerializationWhitelist wl;

        const auto res = MV::ClassSerializer<MV::TextureData>::Write(textureData, ar, wl);

        if (!res) return "";

        MV::BinaryArchiveProcessor proc;

        auto writeRes = proc.WriteToFile(finalPath, ar);

        if (writeRes != MV::ArchiveProcessor::Ok)
        {
            return "";
        }

        return finalPath;
    }

    MV::string
    TextureProducer::CreateTextureResourceToFile(const MV::string& pathIn, const MV::string& pathOut, bool overwrite)
    {
        using namespace Producer;

        auto textureData = ProducerUtils::ReadImage(pathIn);

        auto finalPath = ProducerUtils::ResolvePath(pathOut, overwrite);

        MV::Archive ar;
        MV::SerializationWhitelist wl;

        const auto res = MV::ClassSerializer<MV::TextureData>::Write(textureData, ar, wl);

        if (!res) return "";

        MV::BinaryArchiveProcessor proc;

        auto writeRes = proc.WriteToFile(finalPath, ar);

        if (writeRes != MV::ArchiveProcessor::Ok)
        {
            return "";
        }

        return finalPath;
    }
}