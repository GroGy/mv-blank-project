//
// Created by Matty on 2022-02-10.
//

#ifdef WIN32
#include "../editor/platform/windows/WindowsPlatform.h"
#endif

#include "../editor/platform/Platform.h"

namespace Editor {
    void Platform::OpenInSystemDefault(const MV::string_view path) {
#ifdef WIN32
        WindowsPlatform::OpenInSystemDefault(path);
#endif

    }

    void Platform::RunCommandUnwaited(MV::string_view command, MV::string_view args, MV::string_view directory) {
#ifdef WIN32
        WindowsPlatform::RunCommandUnwaited(command, args, directory);
#endif
    }

    bool
    Platform::RunCommandAwait(MV::string_view command, MV::string_view args, MV::string_view directory, const std::function<void(bool isError,MV::string_view)> & messageClb) {
#ifdef WIN32
        return WindowsPlatform::RunCommandAwait(command, args, directory,messageClb) == 0;
#endif
        return false;
    }

    void Platform::OpenFolderInExplorer(MV::string_view path)
    {
#ifdef WIN32
        WindowsPlatform::OpenFolderInExplorer(path);
#endif

    }
}