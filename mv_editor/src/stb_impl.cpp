//
// Created by Matty on 2021-10-18.
//

#define STB_RECT_PACK_IMPLEMENTATION
#include "../vendor/stb_rect_pack.h"


#include "allocator/AllocatorProvider.h"

#define STBI_NO_SIMD

#define STBI_MALLOC(sz)           MV::AllocatorProvider().allocate(sz)
#define STBI_REALLOC(p,newsz)     MV::AllocatorProvider().reallocate(p, newsz, 0)
#define STBI_FREE(p)              MV::AllocatorProvider().deallocate(p, 0)

#define STB_IMAGE_IMPLEMENTATION
#include "../vendor/stb_image.h"