//
// Created by Matty on 08.12.2022.
//

#include "interface/TilemapCreate.h"
#include "util/UtilGui.h"
#include "EditorCommon.h"

namespace Editor {
    TilemapCreate::TilemapCreate() : Window(Config::sc_TilemapCreateWindowName.data()) {
        m_Flags = m_Flags | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse;
        m_pOpen = false;
    }

    void TilemapCreate::WindowSetup() {
        const ImGuiViewport* viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->GetCenter(), ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
        ImGui::SetNextWindowSize({450.0f, 250.0f}, ImGuiCond_Appearing);
    }

    void TilemapCreate::Render() {
        m_AssetSelector.Render();
        auto *engine = MV::GetEngine();

        if (m_pCreating && m_pTask) {
            ScopedCenter center{"TilesetCreate##wrapper_center"};
            ImGui::ProgressBar(m_pTask->GetProgress(), {-1.0f, 0}, "Creating...");
            if (m_pTask->IsDone()) {
                m_pCreating = false;
                m_pError = m_pTask->GetErrorText();
                if(m_pTask->Success()) {
                    m_pOpen = false;
                    GetEditor()->OnAssetChange();
                }
                m_pTask.reset();
            }
            return;
        }

        ScopedVertical vert{"TilesetCreate##wrapper_vert"};

        {
            ScopedHorizontal hor{"TilesetCreate##wrapper_name"};
            ImGui::TextUnformatted("Name:");
            ImGui2::WideInputText("##tilemap_create_name", &m_pName);
        }

        {
            ScopedHorizontal hor{"TilesetCreate##wrapper_size"};
            ImGui::TextUnformatted("Size:");
            if(ImGui::BeginChild("##tilemap_create_size_input_wrapper", {-1,ImGui::GetFrameHeight()})) {
                const auto width = ImGui::GetContentRegionAvail().x / 2 - ImGui::GetStyle().ItemSpacing.x / 2;
                ImGui::PushItemWidth(width);
                ImGui::DragScalar("##TilemapCreate_width",ImGuiDataType_U32,&m_pWidth);
                ImGui::SameLine();
                ImGui::PushItemWidth(width);
                ImGui::DragScalar("##TilemapCreate_height",ImGuiDataType_U32,&m_pHeight);
            }
            ImGui::EndChild();
        }

        {
            ScopedHorizontal hor{"TilemapCreate##wrapper_tilemap_select"};
            ImGui::TextUnformatted("Tilemap:");
            ImGui::Spring();
            if (ImGui2::AssetSelectButton(m_pSelectedTileset)) {
                m_AssetSelector.Open();
            }
        }

        {
            ScopedDisabled dis{m_pSelectedTileset == 0 || !m_pSelectedTilesetResource || !m_pSelectedTilesetResource->IsFinished()};

            if (ImGui::Button("Create", {-1.0, 0})) {
                TilemapTaskInput input;

                input.m_Name = m_pName;
                input.m_Path = GetAssetPath(m_pName);
                input.m_Tileset = m_pSelectedTileset;
                input.m_Width = m_pWidth;
                input.m_Height = m_pHeight;

                m_pTask = MV::make_rc<TilemapTask>(input);
                m_pTask->Start(m_pTask);
                m_pCreating = true;
            }
        }

        if (m_AssetSelector.HasSelected()) {
            auto selected = m_AssetSelector.GetSelected();
            if (m_pSelectedTilesetResource) {
                MV::GetEngine()->m_ResourceManager.UnloadAsset(m_pSelectedTileset);
            }
            m_pSelectedTileset = selected;
            m_pSelectedTilesetResource = eastl::dynamic_pointer_cast<MV::TilesetResource>(
                    engine->m_ResourceManager.LoadResource(m_pSelectedTileset));

            m_AssetSelector.Clear();
        }
    }

    void TilemapCreate::PostRender() {

    }

    void TilemapCreate::OnCleanup() {

    }

    void TilemapCreate::OnInit() {
        //m_AssetSelector.SetFilters({MV::AssetType::TILESET});

    }
}