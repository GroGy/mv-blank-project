//
// Created by Matty on 20.07.2024.
//

#include "interface/asset_browser/AnimationAssetTypeEditorHandler.h"
#include "interface/asset_browser/AssetTypeEditorRegistry.h"
#include "interface/importers/TextureImporter.h"
#include "EditorCommon.h"
#include "tools/animation_editor/AnimationEditor.h"

namespace Editor {

    void AnimationAssetTypeEditorHandler::OnAssetBrowserClicked(MV::UUID id)
    {
        auto animationEditor = GetEditor()->GetWindow<AnimationEditor>();

        animationEditor->LoadData(id);
        animationEditor->SetOpen(true);
        ImGui::SetWindowFocus(Config::sc_AnimationEditorWindowName.data());
    }

    void AnimationAssetTypeEditorHandler::OnAssetBrowserRightClickPopup(MV::UUID id)
    {

    }

    REGISTER_EDITOR_ASSET_TYPE_HANDLER(AnimationAssetTypeEditorHandler);
}