//
// Created by Matty on 20.07.2024.
//

#include "interface/asset_browser/ComputeShaderAssetTypeEditorHandle.h"
#include "interface/asset_browser/AssetTypeEditorRegistry.h"
#include "imgui/imgui.h"
#include "EditorCommon.h"
#include "util/UtilVerify.h"
#include "util/String.h"
#include "resources/ComputeShaderProducer.h"
#include "resources/ProducerUtils.h"
#include "resources/ProjectProducer.h"
#include "../editor/platform/Platform.h"
#include "resources/common/asset_providers/UnpackagedAssetDataProvider.h"

#include <fstream>

namespace Editor
{

    void ComputeShaderAssetTypeEditorHandle::OnAssetBrowserClicked(MV::UUID id)
    {
        auto& proj = MV::GetEngine()->GetProject();

        const auto foundRes = proj.m_Assets.find(id);

        ENSURE_TRUE(foundRes != proj.m_Assets.end());

        Platform::OpenInSystemDefault(Producer::ProducerUtils::PathObjectRelativeToAbsolute(foundRes->second.m_Path));
    }

    void ComputeShaderAssetTypeEditorHandle::OnAssetBrowserRightClickPopup(MV::UUID id)
    {
        if (ImGui::MenuItem("Compile"))
        {
            auto& proj = MV::GetEngine()->GetProject();
            auto& unpkg = MV::UnpackagedAssetDataProvider::Get();



            const auto foundRes = unpkg.m_Files.find(id);

            ENSURE_TRUE(foundRes !=  unpkg.m_Files.end());

            const auto path = foundRes->second.m_Path;

            std::ifstream ifs{path.c_str(), std::ios::in};

            if(!ifs.is_open()) {
                ENSURE_NO_ENTRY();
            }

            std::stringstream ss;
            ss << ifs.rdbuf();

            ifs.close();

            MV::string source = ss.str().c_str();

            const auto res = MV::GetEngine()->m_ShaderCompiler.CompileComputeShader(id,source);

            if(!res.m_Error.empty()) {
                MV::GetLogger()->LogError("Failed to compile compute shader, error: {}", res.m_Error);
            }
        }
    }

    void ComputeShaderAssetTypeEditorHandle::OnAssetBrowserNewPopup(MV::string_view currentDirectory)
    {
        if (ImGui::MenuItem("Compute Shader"))
        {
            static MV::string Filename;
            Filename.clear();
            MV::string dir = currentDirectory.data();
            GetEditor()->TextInputGlobal("Compute shader name", &Filename,
                                         [dirv(MV::move(dir)), res(&Filename)](bool confirm)
                                         {
                                             if (confirm && UtilVerify::VerifyFileName(MV::StringToWString(*res)))
                                             {
                                                 MV::string finalPath = fmt::format("{}/{}.comp", dirv, *res).c_str();
                                                 finalPath = ComputeShaderProducer::CreateComputeShader(finalPath);

                                                 auto& proj = MV::GetEngine()->GetProject();

                                                 auto assetId = Producer::ProducerUtils::GenerateUUID();

                                                 auto& asset = proj.m_Assets[assetId];
                                                 asset.m_Path = Producer::ProducerUtils::PathAbsoluteToObjectRelative(
                                                         finalPath);
                                                 asset.m_Type = MV::AssetTypeRegistry::Get().GetRuntimeAssetType<MV::ComputeShaderAssetType>();

                                                 ProjectProducer::SaveProject(proj,
                                                                              GetEditor()->GetLoadedProjectPath());
                                             }
                                         });
        }
    }

    REGISTER_EDITOR_ASSET_TYPE_HANDLER(ComputeShaderAssetTypeEditorHandle);
}