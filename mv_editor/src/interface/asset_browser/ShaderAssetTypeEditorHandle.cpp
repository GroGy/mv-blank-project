//
// Created by Matty on 20.07.2024.
//

#include "interface/asset_browser/ShaderAssetTypeEditorHandle.h"
#include "interface/asset_browser/AssetTypeEditorRegistry.h"
#include "imgui/imgui.h"
#include "EditorCommon.h"
#include "tools/shader_editor/ShaderEditor.h"

namespace Editor {

    void ShaderAssetTypeEditorHandle::OnAssetBrowserClicked(MV::UUID id)
    {
        auto shaderEditor = GetEditor()->GetWindow<ShaderEditor>();
        ENSURE(shaderEditor.operator bool());
        shaderEditor->LoadShader(id);
        shaderEditor->SetOpen(true);
        ImGui::SetWindowFocus(Config::sc_ShaderEditorWindowName.data());
    }

    void ShaderAssetTypeEditorHandle::OnAssetBrowserRightClickPopup(MV::UUID id)
    {

    }

    REGISTER_EDITOR_ASSET_TYPE_HANDLER(ShaderAssetTypeEditorHandle);
}