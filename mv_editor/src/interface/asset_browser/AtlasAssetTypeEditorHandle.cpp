//
// Created by Matty on 20.07.2024.
//

#include "interface/asset_browser/AtlasAssetTypeEditorHandle.h"
#include "interface/asset_browser/AssetTypeEditorRegistry.h"
#include "imgui/imgui.h"
#include "EditorCommon.h"
#include "tools/atlas_editor/AtlasEditor.h"

namespace Editor {

    void AtlasAssetTypeEditorHandle::OnAssetBrowserClicked(MV::UUID id)
    {
        auto atlasEditor = GetEditor()->GetWindow<AtlasEditor>();

        atlasEditor->LoadAtlas(id);
        atlasEditor->SetOpen(true);
        ImGui::SetWindowFocus(Config::sc_AtlasEditorWindowName.data());
    }

    void AtlasAssetTypeEditorHandle::OnAssetBrowserRightClickPopup(MV::UUID id)
    {

    }

    REGISTER_EDITOR_ASSET_TYPE_HANDLER(AtlasAssetTypeEditorHandle);
}