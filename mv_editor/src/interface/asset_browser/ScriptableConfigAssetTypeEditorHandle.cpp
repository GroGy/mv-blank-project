//
// Created by Matty on 20.07.2024.
//

#include "interface/asset_browser/ScriptableConfigAssetTypeEditorHandle.h"
#include "interface/asset_browser/AssetTypeEditorRegistry.h"
#include "imgui/imgui.h"
#include "EditorCommon.h"
#include "tools/scriptable_config_editor/ScriptableConfigEditor.h"

namespace Editor {

    void ScriptableConfigAssetTypeEditorHandle::OnAssetBrowserClicked(MV::UUID id)
    {
        auto window = GetEditor()->GetWindow<ScriptableConfigEditor>();
        ENSURE(window.operator bool());
        window->LoadScriptableConfig(id);
    }

    void ScriptableConfigAssetTypeEditorHandle::OnAssetBrowserRightClickPopup(MV::UUID id)
    {
        if (ImGui::MenuItem("Spawn"))
        {
            MV::WorldObjectSpawnParams params;

            params.m_LoadType = MV::WorldObjectSpawnParams::FromAsset;
            params.m_BaseDataAssetID = id;

            auto spawnedWorldObject = MV::GetEngine()->GetWorld().SpawnWorldObject(params);

            GetEditor()->m_SelectedWO = spawnedWorldObject->m_WorldID;
        }
    }

    REGISTER_EDITOR_ASSET_TYPE_HANDLER(ScriptableConfigAssetTypeEditorHandle);
}