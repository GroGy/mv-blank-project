//
// Created by Matty on 20.07.2024.
//

#include "interface/asset_browser/TilesetAssetTypeEditorHandle.h"
#include "interface/asset_browser/AssetTypeEditorRegistry.h"
#include "imgui/imgui.h"
#include "tools/tileset_editor/TilesetEditor.h"
#include "EditorCommon.h"

namespace Editor {

    void TilesetAssetTypeEditorHandle::OnAssetBrowserClicked(MV::UUID id)
    {
        auto tilesetEditor = GetEditor()->GetWindow<TilesetEditor>();
        ENSURE(tilesetEditor.operator bool());
        tilesetEditor->LoadTileset(id);
        tilesetEditor->SetOpen(true);
        ImGui::SetWindowFocus(Config::sc_TilesetEditorWindowName.data());
    }

    void TilesetAssetTypeEditorHandle::OnAssetBrowserRightClickPopup(MV::UUID id)
    {

    }

    REGISTER_EDITOR_ASSET_TYPE_HANDLER(TilesetAssetTypeEditorHandle);
}