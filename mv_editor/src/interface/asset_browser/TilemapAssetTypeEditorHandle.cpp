//
// Created by Matty on 20.07.2024.
//

#include "interface/asset_browser/TilemapAssetTypeEditorHandle.h"
#include "interface/asset_browser/AssetTypeEditorRegistry.h"
#include "imgui/imgui.h"
#include "tools/tilemap_editor/TilemapEditor.h"
#include "EditorCommon.h"

namespace Editor {

    void TilemapAssetTypeEditorHandle::OnAssetBrowserClicked(MV::UUID id)
    {
        auto tilemapEditor = GetEditor()->GetWindow<TilemapEditor>();
        ENSURE(tilemapEditor.operator bool());
        tilemapEditor->LoadTilemap(id);
        tilemapEditor->SetOpen(true);

        ImGui::SetWindowFocus(Config::sc_TilemapEditorWindowName.data());
    }

    void TilemapAssetTypeEditorHandle::OnAssetBrowserRightClickPopup(MV::UUID id)
    {

    }

    REGISTER_EDITOR_ASSET_TYPE_HANDLER(TilemapAssetTypeEditorHandle);
}