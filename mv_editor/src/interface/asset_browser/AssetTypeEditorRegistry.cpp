//
// Created by Matty on 19.07.2024.
//

#include "interface/asset_browser/AssetTypeEditorRegistry.h"

namespace Editor {
    AssetTypeEditorRegistry* Singleton = nullptr;

    AssetTypeEditorRegistry& AssetTypeEditorRegistry::Get()
    {
        if(!Singleton) {
            Singleton = new AssetTypeEditorRegistry();
        }

        return *Singleton;
    }

}