//
// Created by Matty on 20.07.2024.
//

#include "interface/asset_browser/WorldAssetTypeEditorHandle.h"
#include "interface/asset_browser/AssetTypeEditorRegistry.h"
#include "imgui/imgui.h"
#include "Engine.h"

namespace Editor {

    void WorldAssetTypeEditorHandle::OnAssetBrowserClicked(MV::UUID id)
    {
        MV::GetEngine()->m_ResourceManager.LoadResource(id);
        ImGui::SetWindowFocus("Viewport");
    }

    void WorldAssetTypeEditorHandle::OnAssetBrowserRightClickPopup(MV::UUID id)
    {

    }

    REGISTER_EDITOR_ASSET_TYPE_HANDLER(WorldAssetTypeEditorHandle);
}