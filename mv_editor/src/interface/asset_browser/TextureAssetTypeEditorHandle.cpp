//
// Created by Matty on 19.07.2024.
//

#include "interface/asset_browser/TextureAssetTypeEditorHandle.h"
#include "interface/asset_browser/AssetTypeEditorRegistry.h"
#include "interface/importers/TextureImporter.h"
#include "EditorCommon.h"

namespace Editor {

    void TextureAssetTypeEditorHandle::OnAssetBrowserClicked(MV::UUID id)
    {

    }

    void TextureAssetTypeEditorHandle::OnAssetBrowserRightClickPopup(MV::UUID id)
    {
        if(ImGui::MenuItem("Reimport...")) {
            auto textureImporter = GetEditor()->GetWindow<TextureImporter>();
            textureImporter->OpenForReimport(id);
            ImGui::SetWindowFocus(Config::sc_TextureImportWindowName.data());
        }
    }

    REGISTER_EDITOR_ASSET_TYPE_HANDLER(TextureAssetTypeEditorHandle);
}