//
// Created by Matty on 05.07.2024.
//

#include "interface/common/TextureViewport.h"
#include "imgui/imgui_internal.h"
#include "common.hpp"
#include "tools/tilemap_editor/Util.h"

namespace Editor
{
    void TextureViewport::Draw(MV::string_view label, const glm::vec2& size)
    {
        updateAnimation();
        auto* window = ImGui::GetCurrentWindow();
        auto cursor = window->DC.CursorPos;
        ImRect bb{cursor, cursor + ImVec2{size.x, size.y}};

        ImGui::ItemSize({size.x, size.y});

        ImGui::PushID(label.data());

        if (!ImGui::ItemAdd(bb, ImGui::GetID("##wrapper")))
        {
            return;
        }

        auto drawList = window->DrawList;

        if (!m_pTexture || !m_pTexture->IsValid())
        {
            drawList->AddText(cursor, IM_COL32_WHITE, "Texture is not assigned in texture viewport");
            ImGui::PopID();
            return;
        }

        processInput({bb.GetTL().x, bb.GetTL().y, bb.GetBR().x, bb.GetBR().y});

        const auto drawnBounds = drawView({bb.GetTL().x, bb.GetTL().y, bb.GetBR().x, bb.GetBR().y});

        if (m_DrawCustomOverlayCallback)
        {
            m_DrawCustomOverlayCallback(this, drawnBounds);
        }

        ImGui::PopID();
    }

    void TextureViewport::Init(EditorTexture* texture)
    {
        m_pTexture = texture;
    }

    void TextureViewport::processInput(const glm::vec4& bounds)
    {
        bool hovered = ImGui::IsItemHovered();
        bool consumed = false;
        ImRect bb{bounds.x, bounds.y, bounds.z, bounds.w};

        if (hovered)
        {
            const float scroll = ImGui::GetIO().MouseWheel;
            m_pPixelScale = glm::clamp(m_pPixelScale + scroll * 0.2f, 0.05f, 10.0f);
        }

        const bool draggingButtonDown = ImGui::IsMouseDown(ImGuiMouseButton_Middle);

        if (draggingButtonDown && !m_pIsDraggingCamera)
        {
            m_pIsDraggingCamera = true;
            m_pCameraDragStart = m_pCameraOffset;
            m_pMouseDragStart = {ImGui::GetMousePos().x, ImGui::GetMousePos().y};
        }

        if (m_pIsDraggingCamera)
        {
            auto mousePos = ImGui::GetMousePos();

            auto mouseDelta = mousePos - ImVec2{m_pMouseDragStart.x, m_pMouseDragStart.y};

            const ImVec2 windowSize = bb.GetSize();

            mouseDelta /= windowSize;

            const ImVec2 windowTileScale = windowSize / m_pPixelScale;

            const auto screenDelta = (mouseDelta * windowTileScale);

            m_pCameraOffset = m_pCameraDragStart + glm::vec2{screenDelta.x, screenDelta.y};

            if (!draggingButtonDown)
            {
                m_pIsDraggingCamera = false;
                m_pCameraDragStart = {0, 0};
                m_pMouseDragStart = {0, 0};
            }
        }
    }

    glm::vec4 TextureViewport::drawView(const glm::vec4& bounds)
    {
        auto drawList = ImGui::GetWindowDrawList();
        const auto textureSize = m_pTexture->GetSize();

        ImRect bb{bounds.x, bounds.y, bounds.z, bounds.w};

        ImVec2 uvTL = {0, 0};
        ImVec2 uvBR = {1, 1};

        float scaledSizeX = textureSize.x * m_pPixelScale;
        float scaledSizeY = textureSize.y * m_pPixelScale;

        const auto scaledCamera = m_pCameraOffset * m_pPixelScale;

        ImVec2 TL = {bb.GetCenter() - ImVec2{scaledSizeX / 2, scaledSizeY / 2}};
        ImVec2 BR = {bb.GetCenter() + ImVec2{scaledSizeX / 2, scaledSizeY / 2}};

        TL += ImVec2{scaledCamera.x, scaledCamera.y};
        BR += ImVec2{scaledCamera.x, scaledCamera.y};

        drawList->AddRectFilled(TL, BR, IM_COL32(20,20,20,255));
        drawList->AddImage(m_pTexture->GetHandle(), TL, BR, uvTL, uvBR);

        if (m_DrawScale)
        {
            drawList->AddText(bb.GetBR() - ImGui::CalcTextSize("###########"), IM_COL32_WHITE,
                              fmt::format("Scale: {}", m_pPixelScale).c_str());
        }

        if (m_DrawMousePosition)
        {
            auto mouse = ImGui::GetMousePos();
            mouse -= TL;

            mouse /= m_pPixelScale;

            mouse = ImClamp(mouse, {0,0}, {m_pTexture->GetSize().x,m_pTexture->GetSize().y});

            drawList->AddText(bb.GetTL() + ImVec2{0,ImGui::GetTextLineHeight()}, IM_COL32_WHITE,
                              fmt::format("X: {:.0f}, Y: {:.0f}", mouse.x, mouse.y).c_str());
        }

        return {TL.x, TL.y, BR.x, BR.y};
    }

    void TextureViewport::ResetState()
    {
        m_pPixelScale = m_pPixelScaleBase;
        m_pCameraOffset = {0, 0};
        m_pInAnimation = false;
        m_pFocusPosition = {0, 0};
    }

    glm::vec2 TextureViewport::TransformToViewport(const glm::vec2& PixelPosition) const
    {
        const auto scaledCamera = m_pCameraOffset * m_pPixelScale;

        ImVec2 point{PixelPosition.x * m_pPixelScale, PixelPosition.y * m_pPixelScale};

        point += ImVec2{scaledCamera.x, scaledCamera.y};

        return {point.x, point.y};
    }

    void TextureViewport::MoveToPosition(const glm::vec2& position)
    {
        if (!m_pTexture || !m_pTexture->IsValid())
        {
            return;
        }

        m_pInAnimation = true;

        auto size = m_pTexture->GetSize();

        m_pFocusPosition = position + (size / 2.f);
    }

    void TextureViewport::updateAnimation()
    {
        if (!m_pInAnimation) return;

        if (glm::distance(m_pFocusPosition, m_pCameraOffset) < 0.5f)
        {
            m_pInAnimation = false;
            m_pFocusPosition = {0, 0};
            return;
        }

        m_pCameraOffset = glm::mix(m_pCameraOffset, m_pFocusPosition, 0.5f);
    }

    void TextureViewport::MoveToUV(const glm::vec4& uv)
    {
        if (!m_pTexture || !m_pTexture->IsValid())
        {
            return;
        }

        m_pInAnimation = true;

        auto size = m_pTexture->GetSize();

        ImRect bb{uv.x, uv.y, uv.z, uv.w};

        glm::vec2 cameraOffset = (size / 2.f);

        auto center = glm::vec2(bb.GetCenter().x, bb.GetCenter().y);

        center.x = 1.0f - center.x;
        center.y = 1.0f - center.y;

        m_pFocusPosition = (size * center) - cameraOffset;
    }
}
