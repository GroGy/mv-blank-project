//
// Created by Matty on 2021-10-29.
//

#include <common/EngineCommon.h>
#include <platform/Platform.h>
#include "EASTL/sort.h"
#include "allocator/Alloc.h"
#include "asset/AssetUUID.h"
#include "common/Types.h"
#include "interface/AnimationCreate.h"
#include "interface/asset_browser/AssetBrowser.h"
#include "imgui/imgui.h"
#include "imgui/imgui_stdlib.h"
#include "tools/atlas_editor/AtlasEditor.h"
#include "interface/importers/TextureImporter.h"
#include "interface/importers/AsepriteImporter.h"
#include "interface/WorldCreate.h"
#include "interface/TilesetCreate.h"
#include "interface/TilemapCreate.h"
#include "interface/AtlasCreate.h"
#include "interface/ShaderCreate.h"
#include "interface/WorldObjectConfigCreate.h"
#include <util/String.h>

#define IMGUI_DEFINE_MATH_OPERATORS

#include "imgui/imgui_internal.h"
#include "../editor/platform/Platform.h"
#include "util/UtilGui.h"
#include "resources/AssetIcons.h"
#include "EditorCommon.h"
#include "util/Container.h"
#include "resources/ProjectProducer.h"
#include "resources/ProducerUtils.h"
#include "interface/asset_browser/AssetTypeEditorRegistry.h"

namespace Editor
{
    AssetBrowser::AssetBrowser() : Window(Config::sc_AssetBrowserWindowName.data())
    {

    }

    void AssetBrowser::WindowSetup()
    {

    }

    void AssetBrowser::treeAddDirectory(const std::filesystem::path& path)
    {
        for (auto const& dir_entry: std::filesystem::directory_iterator{path})
        {
            if (dir_entry.is_directory())
            {
                const auto dirName = dir_entry.path().filename().string();

                if (MV::StartsWith(dirName, '.'))
                    continue;

                //if (ImGui::TreeNode(MV::string(MV::string(Config::sc_FolderIcon) + dirName).c_str())) {
                if (ImGui::TreeNode(dirName.c_str()))
                {
                    treeAddDirectory(dir_entry.path());
                    ImGui::TreePop();
                }

                if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left))
                {
                    m_pCurrentFolder = dir_entry.path();
                }
            }
        }
    }

    void AssetBrowser::Render()
    {
        MV::AllocatorTagScope allocTag{MV::AllocationType::Editor};
        m_ImagePreview.Render();

        const auto area = ImGui::GetContentRegionAvail();

        const auto hierWidth = area.x / 5.0f;

        auto& proj = MV::GetEngine()->GetProject();

        if (!m_pLoaded)
        {
            m_pLoaded = true;
            m_pRootFolder = proj.m_Path.data();
            m_pCurrentFolder = m_pRootFolder;
        }

        if (m_pCacheDirty)
            rebuildCache();

        ImGui::BeginHorizontal("top_bar_wrapper", {area.x, 0.0f});

        ImGui::PushItemWidth(hierWidth);
        ImGui::InputTextWithHint("##search_bar", "Search", &m_pSearchVal);
        ImGui::TextUnformatted(m_pCurrentFolder.string().c_str());
        ImGui::Spring();

        if(ImGui::Button("Refresh")) {
            Refresh();
        }

        ImGui::EndHorizontal();

        ImGui::BeginHorizontal("browser horizontal wrapper");

        if (ImGui::BeginChild("Hierarchy##file_hierarchy", {hierWidth, -1.0f}, true))
        {
            treeAddDirectory(m_pRootFolder);
        }
        ImGui::EndChild();

        if (ImGui::BeginChild("Browser##file_hierarchy", {-1.0, -1.0f}, true))
        {
            const auto browserSize = ImGui::GetContentRegionAvail();
            uint32_t lineCounter = 0;
            uint32_t lineItemCount = ImFloor(
                    browserSize.x / (Config::sc_AssetMaxSize + (ImGui::GetStyle().ItemSpacing.x * 2.0f)));

            if (ImGui::BeginPopupContextWindow("##popup_no_item_clicked_assetbrowser"))
            {
                drawPopupNoItemClicked();
            }

            //bool openContextPopup = ImGui::IsItemClicked(ImGuiMouseButton_Right);

            if (m_pCurrentFolder != m_pRootFolder)
            {
                lineCounter++;
                if (ImGui2::AssetBrowserButton("##main_doubledot_",
                                               {Config::sc_AssetMaxSize, Config::sc_AssetMaxSize + 15.0f},
                                               Config::sc_FolderIcon, "..",false, false, true))
                {
                    m_pCurrentFolder = m_pCurrentFolder.parent_path();
                    m_pCurrentLocation = m_pCurrentLocation->m_Parent;
                }
            }

            for (auto&& entry: m_pCurrentLocation->m_Entries)
            {
                if (lineCounter <= lineItemCount)
                {
                    ImGui::SameLine(lineCounter * Config::sc_AssetMaxSize +
                                    (lineCounter + 1) * ImGui::GetStyle().ItemSpacing.x +
                                    ImGui::GetStyle().FramePadding.x);
                    lineCounter++;
                } else
                {
                    lineCounter = 1;
                }



                MV::string_view icon = entry.m_Type == FileSystemEntry::EntryType::Directory ? Config::sc_FolderIcon
                                                                                             : entry.m_AssetIcon.data();

                if (ImGui2::AssetBrowserButton(entry.m_Name,
                                               {Config::sc_AssetMaxSize, Config::sc_AssetMaxSize + 15.0f}, icon,
                                               entry.m_Name,
                                               entry.m_Type == FileSystemEntry::EntryType::Directory ? false :
                                               entry.m_ID == MV::InvalidUUID,
                                               m_pSelected == entry.m_Path, entry.m_Type == FileSystemEntry::EntryType::Directory))
                {

                    if (entry.m_Type == FileSystemEntry::EntryType::Directory)
                    {
                        m_pCurrentFolder /= entry.m_Name.c_str();
                        m_pCurrentLocation = &entry;
                        continue;
                    }

                    if (entry.m_ID == MV::InvalidUUID)
                    {
                        ImGui::SetMouseCursor(ImGuiMouseCursor_NotAllowed);
                        Platform::OpenInSystemDefault(entry.m_Path.string().c_str());
                        continue;
                    }

                    const auto& registry = AssetTypeEditorRegistry::Get();

                    auto data = registry.GetEditorDataFromAssetType(entry.m_AssetType);

                    if(data.m_OnFileOpenFn) {
                        data.m_OnFileOpenFn(entry.m_ID);
                    }
                }
                if (ImGui::IsItemClicked())
                {
                    m_pSelected = entry.m_Path;
                }

                if(ImGui::IsItemHovered()) {
                    ImGui::BeginTooltip();
                    ImGui::TextUnformatted(entry.m_Path.string().c_str());
                    ImGui::EndTooltip();
                }

                ImGuiDragDropFlags src_flags = 0;
                src_flags |= ImGuiDragDropFlags_SourceNoDisableHover;
                src_flags |= ImGuiDragDropFlags_SourceNoHoldToOpenOthers;
                src_flags |= ImGuiDragDropFlags_SourceAllowNullID;

                if (ImGui::BeginPopupContextItem(
                        (entry.m_Name + Config::sc_AssetContextMenuPopup.data()).c_str()))
                {
                    if(entry.m_Type == FileSystemEntry::EntryType::Directory) {

                    } else {
                        const auto& registry = AssetTypeEditorRegistry::Get();

                        auto data = registry.GetEditorDataFromAssetType(entry.m_AssetType);

                        if(data.m_OnRightClickPopupFn) {
                            data.m_OnRightClickPopupFn(entry.m_ID);
                        }
                    }
                    ImGui::EndPopup();
                }
            }
        }

        ImGui::EndChild();
        ImGui::EndHorizontal();

        renderCreateAssetPopup();

        renderEditAssetPopup();
    }

    void AssetBrowser::PostRender()
    {

    }

    void AssetBrowser::OnCleanup()
    {
    }

    void AssetBrowser::OnInit()
    {
    }

    void AssetBrowser::renderCreateAssetPopup()
    {
        if (ImGui::BeginPopup(Config::sc_CreateAssetPopup.data()))
        {
            if (ImGui::MenuItem("New Texture"))
            {
                auto textureImporter = GetEditor()->GetWindow<TextureImporter>();

                textureImporter->SetOpen(true);
                textureImporter->m_CreateDirectory = m_pCurrentFolder.string().c_str();
                ImGui::SetWindowFocus(Config::sc_TextureImportWindowName.data());
            }

            if (ImGui::MenuItem("New Atlas"))
            {
                auto asepriteImporter = GetEditor()->GetWindow<AsepriteImporter>();

                asepriteImporter->SetOpen(true);
                ImGui::SetWindowFocus(Config::sc_TextureImportWindowName.data());
            }

            if (ImGui::MenuItem("New World"))
            {
                //TODO: Create empty world
            }


            if (ImGui::MenuItem("New Script"))
            {
                //TODO: Create script
            }

            if (ImGui::MenuItem("New tileset"))
            {
                auto tilesetCreate = GetEditor()->GetWindow<TilesetCreate>();

                tilesetCreate->m_CreateDirectory = m_pCurrentFolder.string().c_str();
                tilesetCreate->SetOpen(true);
                ImGui::SetWindowFocus(Config::sc_TilesetCreateWindowName.data());
            }

            if (ImGui::MenuItem("New tilemap"))
            {
                auto tilemapCreate = GetEditor()->GetWindow<TilemapCreate>();

                tilemapCreate->m_CreateDirectory = m_pCurrentFolder.string().c_str();
                tilemapCreate->SetOpen(true);
                ImGui::SetWindowFocus(Config::sc_TilemapCreateWindowName.data());
            }

            if (ImGui::MenuItem("New animation"))
            {

            }

            ImGui::EndPopup();
        }
    }

    void AssetBrowser::renderEditAssetPopup()
    {

    }
/*
    void AssetBrowser::drawPopupMenu(bool isAsset, MV::AssetType type, uint64_t assetID, bool isAssetInProject,
                                     const std::filesystem::path& entryPath)
    {
        if (isAsset)
        {
            ScopedDisabled dis{isAssetInProject};
            if (ImGui::MenuItem("Add to project"))
            {

            }
        }

        if (ImGui::MenuItem("Copy"))
        {

        }
        if (ImGui::MenuItem("Delete"))
        {
            std::filesystem::remove(entryPath);
            MV::GetEngine()->GetProject().m_Assets.erase(assetID);
            GetEditor()->OnAssetChange();
            GetEditor()->SaveData();
            Refresh();
        }
        ImGui::Separator();


        if (!isAssetInProject)  {
            return;
        }

        switch (type)
        {
            case MV::ShaderAssetType:
            {
                if (ImGui::MenuItem("Open in shader editor"))
                {
                    auto shaderEditor = GetEditor()->GetWindow<ShaderEditor>();

                    shaderEditor->LoadShader(assetID);
                    ImGui::SetWindowFocus(Config::sc_ShaderEditorWindowName.data());
                }
                break;
            }
            case MV::TextureAssetType: {
                if (ImGui::MenuItem("Reimport..."))
                {
                    auto textureImporter = GetEditor()->GetWindow<TextureImporter>();
                    textureImporter->OpenForReimport(assetID);
                    ImGui::SetWindowFocus(Config::sc_TextureImportWindowName.data());
                }
                break;
            }
            case MV::AssetType::MODEL:
                break;
            case MV::AssetType::PROJECT:
                break;
            case MV::AssetType::ATLAS:
                break;
            case MV::AssetType::WORLD:
                if (ImGui::MenuItem("Set as Default"))
                {
                    MV::GetEngine()->GetProject().m_DefaultWorld = assetID;
                    GetEditor()->SaveData();
                }
                break;
            case MV::AssetType::TILESET:
            {
                if (ImGui::MenuItem("Edit"))
                {
                    auto tilesetEditor = GetEditor()->GetWindow<TilesetEditor>();

                    tilesetEditor->LoadTileset(assetID);
                    tilesetEditor->SetOpen(true);
                    ImGui::SetWindowFocus(Config::sc_TilesetEditorWindowName.data());
                }
                break;
            }
            case MV::ScriptConfigAssetType: {
                if (ImGui::MenuItem("Spawn"))
                {
                    MV::WorldObjectSpawnParams params;

                    params.m_LoadType = MV::WorldObjectSpawnParams::FromAsset;
                    params.m_BaseDataAssetID = assetID;

                    auto spawnedWorldObject = MV::GetEngine()->GetWorld().SpawnWorldObject(params);

                    GetEditor()->m_SelectedWO = spawnedWorldObject->m_WorldID;
                }
                break;
            }
            case MV::ScriptConfigAssetType_ADDITIONAL_DATA:
                break;
            case MV::TilemapAssetType:
            {
                if (ImGui::MenuItem("Open in tilemap editor"))
                {
                    auto tilemapEditor = GetEditor()->GetWindow<TilemapEditor>();

                    tilemapEditor->LoadTilemap(assetID);
                    tilemapEditor->SetOpen(true);
                    ImGui::SetWindowFocus(Config::sc_TilemapEditorWindowName.data());
                }
                break;
            }
            case MV::AnimationAssetType: {
                if (ImGui::MenuItem("Open in animation editor"))
                {
                    auto animationEditor = GetEditor()->GetWindow<AnimationEditor>();

                    animationEditor->LoadData(assetID);
                    animationEditor->SetOpen(true);
                    ImGui::SetWindowFocus(Config::sc_AnimationEditorWindowName.data());
                }
                break;
            }
        }
        //TODO Asset specific stuff
    }
*/
    void AssetBrowser::drawPopupNoItemClicked()
    {
        if (ImGui::BeginMenu("New"))
        {
            for(const auto & v : AssetTypeEditorRegistry::Get().m_RuntimeLookup) {
                if(v.m_OnNewItemPopupFn) {
                    v.m_OnNewItemPopupFn(m_pCurrentFolder.string().c_str());
                }
            }

            if (ImGui::MenuItem("Texture"))
            {
                auto textureImporter = GetEditor()->GetWindow<TextureImporter>();

                textureImporter->SetOpen(true);
                textureImporter->m_CreateDirectory = m_pCurrentFolder.string().c_str();
                ImGui::SetWindowFocus(Config::sc_TextureImportWindowName.data());
            }
            if (ImGui::MenuItem("Shader"))
            {
                auto shaderCreate = GetEditor()->GetWindow<ShaderCreate>();

                shaderCreate->SetOpen(true);
                shaderCreate->SetPath(m_pCurrentFolder.string().c_str());
                ImGui::SetWindowFocus(Config::sc_ShaderCreateWindowName.data());
            }
            if (ImGui::MenuItem("Atlas"))
            {
                auto atlasCreate = GetEditor()->GetWindow<AtlasCreate>();

                atlasCreate->m_CreateDirectory = m_pCurrentFolder.string().c_str();
                atlasCreate->SetOpen(true);
                ImGui::SetWindowFocus(Config::sc_AtlasCreateWindowName.data());
            }
            if (ImGui::MenuItem("World"))
            {
                auto worldCreate = GetEditor()->GetWindow<WorldCreate>();

                worldCreate->SetOpen(true);
                worldCreate->SetPath(m_pCurrentFolder.string().c_str());
                ImGui::SetWindowFocus(Config::sc_WorldCreateWindowName.data());
            }

            if (ImGui::MenuItem("Tileset"))
            {
                auto tilesetCreate = GetEditor()->GetWindow<TilesetCreate>();

                tilesetCreate->m_CreateDirectory = m_pCurrentFolder.string().c_str();
                tilesetCreate->SetOpen(true);
                ImGui::SetWindowFocus(Config::sc_TilesetCreateWindowName.data());
            }

            if (ImGui::MenuItem("Tilemap"))
            {
                auto tilemapCreate = GetEditor()->GetWindow<TilemapCreate>();

                tilemapCreate->m_CreateDirectory = m_pCurrentFolder.string().c_str();
                tilemapCreate->SetOpen(true);
                ImGui::SetWindowFocus(Config::sc_TilemapCreateWindowName.data());
            }

            if (ImGui::MenuItem("Animation"))
            {
                auto animationCreate = GetEditor()->GetWindow<AnimationCreate>();

                animationCreate->m_CreateDirectory = m_pCurrentFolder.string().c_str();
                animationCreate->SetOpen(true);
                ImGui::SetWindowFocus(Config::sc_AnimationCreateWindowName.data());
            }

            if (ImGui::MenuItem("World object config")) {
                auto wocCreate = GetEditor()->GetWindow<WorldObjectConfigCreate>();

                wocCreate->m_CreateDirectory = m_pCurrentFolder.string().c_str();
                wocCreate->SetOpen(true);
                ImGui::SetWindowFocus(Config::sc_WorldObjectConfigCreateWindowName.data());
            }

            ImGui::EndMenu();
        }
        if (ImGui::MenuItem("New directory"))
        {
            auto currDir = m_pCurrentFolder;
            GetEditor()->TextInputGlobal("Directory name", &m_pCreateFolderName,
                                         [&, startDir(MV::move(currDir))](bool confirmed)
                                         {
                                             if (confirmed)
                                             {
                                                 try
                                                 {
                                                     std::filesystem::path path = fmt::format("{}/{}",
                                                                                              startDir.string(),
                                                                                              m_pCreateFolderName).c_str();
                                                     if (!std::filesystem::create_directory(path))
                                                     {
                                                         MV::Platform::Get()->ShowErrorPopup(
                                                                 "Failed to create directory.", "");
                                                     }
                                                 } catch (std::exception& e)
                                                 {
                                                     MV::Platform::Get()->ShowErrorPopup("Failed to create directory.",
                                                                                         e.what());
                                                 }
                                             }
                                             m_pCreateFolderName.clear();
                                         });
        }
        if (ImGui::MenuItem("Open in explorer")) {
            Platform::OpenFolderInExplorer(m_pCurrentFolder.string().c_str());
        }
        ImGui::EndPopup();
    }

    void AssetBrowser::rebuildCache()
    {
        m_pCurrentLocation = nullptr;
        MV::AllocatorTagScope allocTag{MV::AllocationType::Editor};
        m_pCache = decltype(m_pCache){};
        m_pCacheDirty = false;
        m_pCache.m_Type = FileSystemEntry::EntryType::Directory;
        m_pCache.m_Entries.clear();
        m_pCache.m_Name = m_pRootFolder.filename().string().c_str();
        m_pCache.m_Path = m_pRootFolder;

        populateFSEntry(m_pCache);

        if (!m_pCurrentLocation)
        {
            m_pCurrentLocation = &m_pCache;
            m_pCurrentFolder = m_pRootFolder;
        }
    }

    void AssetBrowser::populateFSEntry(FileSystemEntry& root)
    {
        for (auto const& entry: std::filesystem::directory_iterator{root.m_Path})
        {
            const auto entryPath = entry.path();
            const auto name = entryPath.filename().string();
            const auto extension = entryPath.extension().string();
            const auto stem = entryPath.stem().string();
            const auto projectRelativePath = relative(entryPath, m_pRootFolder);

            if (MV::StartsWith(name, '.'))
                continue;

            bool isAsset = false;
            bool validAsset = false;
            bool showEntry = false;
            uint64_t assetID = 0;
            MV::AssetType type;

            MV::string_view icon = Config::sc_SystemFileIcon;

            if (entry.is_directory())
            {
                showEntry = true;
                icon = Config::sc_FolderIcon;

                FileSystemEntry fsEntry;
                fsEntry.m_Name = name.c_str();
                fsEntry.m_Type = FileSystemEntry::EntryType::Directory;
                fsEntry.m_Path = entry;
                fsEntry.m_Parent = &root;

                root.m_Entries.push_back(MV::move(fsEntry));
            } else
            {
                if (MV::Contains(Config::sc_AllowedExtensions, MV::string(extension.c_str())))
                    showEntry = true;

                if (extension == MV::Config::sc_AssetExtension.data() || extension == ".comp")
                {
                    isAsset = true;
                    icon = Config::sc_UnknownAsset;
                    auto findRes = GetEditor()->m_ProjectAssetTypes.find(projectRelativePath.string().c_str());
                    if (findRes != GetEditor()->m_ProjectAssetTypes.end())
                    {
                        validAsset = true;
                        type = findRes->second.second;
                        assetID = findRes->second.first;
                        icon = MV::AssetTypeRegistry::Get().m_RuntimeLookup[type.m_LookupID].m_IconCharacter;
                    }
                }

                if (showEntry)
                {
                    FileSystemEntry fsEntry;
                    fsEntry.m_Name = name.c_str();
                    fsEntry.m_Type = FileSystemEntry::EntryType::File;
                    fsEntry.m_Path = entry;
                    fsEntry.m_AssetType = type;
                    fsEntry.m_AssetIcon = icon;
                    fsEntry.m_ID = validAsset ? assetID : MV::InvalidUUID;
                    fsEntry.m_Parent = &root;

                    root.m_Entries.push_back(MV::move(fsEntry));
                }
            }
        }

        eastl::sort(root.m_Entries.begin(),root.m_Entries.end(),[](const FileSystemEntry& a, const FileSystemEntry& b){
            if(a.m_Type == FileSystemEntry::EntryType::Directory) {
                if(b.m_Type != FileSystemEntry::EntryType::Directory) return true;

                return a.m_Name < b.m_Name;
            }

            if(b.m_Type == FileSystemEntry::EntryType::Directory) return false;


            return a.m_Name < b.m_Name;
        });

        for (auto& v: root.m_Entries)
        {
            if (v.m_Type == FileSystemEntry::EntryType::Directory)
            {
                populateFSEntry(v);

                if (!m_pCurrentLocation && v.m_Path == m_pCurrentFolder)
                    m_pCurrentLocation = &v;
            }
        }
    }

    void AssetBrowser::Refresh()
    {
        m_pCacheDirty = true;
    }

    void AssetBrowser::OnOSDragAndDropEvent(OSDragAndDropEvent& event)
    {
        auto& data = event.GetData();
        event.Consume();

        MV::vector<MV::string> textures;

        for(auto & v : data) {
            if(MV::EndsWith(MV::ToLower(v), MV::string_view(".png"))) {
                textures.emplace_back(v);
            }
        }

        auto textureImporter = GetEditor()->GetWindow<TextureImporter>();

        textureImporter->SetOpen(true);
        textureImporter->m_CreateDirectory = m_pCurrentFolder.string().c_str();
        textureImporter->m_Paths = textures;
        ImGui::SetWindowFocus(Config::sc_TextureImportWindowName.data());
    }
}

