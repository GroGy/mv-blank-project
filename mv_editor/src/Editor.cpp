//
// Created by Matty on 2021-10-15.
//

#include "../editor/Editor.h"
#include "../editor/EditorCommon.h"
#include "../editor/resources/WorldProducer.h"
#include "../editor/resources/ProjectProducer.h"
#include "allocator/Alloc.h"
#include "common/subsystem/Subsystem.h"
#include "meta/MetaCache.h"
#include "serialization/common/ClassSerializer.h"
#include "tools/scriptable_config_editor/ScriptableConfigEditor.h"
#include "tools/particle_editor/ParticleEditor.h"
#include "precompiler/PropertyFieldRendererRegistrator.h"
#include "precompiler/PropertyGizmoRegistrator.h"
#include <util/Container.h>
#include <fstream>

namespace Editor
{
    void Editor::Init(MV::Engine& engine)
    {
        MV::AllocatorTagScope allocTag{MV::AllocationType::Editor};
        EditorCommon::SetEditorGlobal(this);
        EditorCallbacks callbacks{};

        callbacks.onCleanup = [this]() -> bool
        { return onCleanup(); };
        callbacks.onEditorRender = [this]()
        { onEditorRender(); };
        callbacks.onInit = [this]()
        { onInit(); };
        callbacks.onPostInit = [this]()
        { onPostInit(); };
        callbacks.onRender = [this]()
        { onRender(); };
        callbacks.onWindowResize = [this]()
        { onWindowResize(); };
        callbacks.onUpdate = [this](float delta)
        { onUpdate(delta); };
        callbacks.onRendererCleanup = [this]() -> bool
        { return onRendererCleanup(); };

        m_pModule = MV::make_rc<Editor_Module>(callbacks);
        engine.RegisterModule(m_pModule);

        m_pEditorUI = MV::make_rc<EditorUI>();

        m_EditorViewportCamera = MV::make_rc<ViewportCamera>();
    }

    bool Editor::onCleanup()
    {
        m_pEditorUI->OnCleanup();
        m_AssetWatcher.Cleanup();
        m_Settings.Serialize();
        cleanupWorkers();
        return true;
    }

    bool Editor::onRendererCleanup()
    {
        m_pEditorUI->OnRendererCleanup();
        m_EditorViewportCamera->Cleanup();
        return true;
    }

    void Editor::onWindowResize()
    {
        m_pEditorUI->OnResize();
    }

    void Editor::onInit()
    {
        RegisteredPropertyRenderers::GetInstance().InitializeRenderers();
        RegisteredPropertyGizmos::GetInstance().Initialize();
        m_Settings.Deserialize();
        initWorkers();
        initLastOpenedProjects();
    }

    void Editor::onPostInit()
    {
        m_pEditorUI->OnInit();

        m_EditorViewportCamera->Init();

        MV::GetEngine()->m_Window->SetDragAndDropCallback([&](const MV::vector<MV::string>& paths)
                                                          {
                                                              m_pEditorUI->ProcessOsDragAndDrop(paths);
                                                          });
    }

    void Editor::onUpdate(float deltaTime)
    {

        const auto engine = MV::GetEngine();

        if (engine->GetWorld().IsValid() &&
            ((!engine->GetWorld().m_CameraOverride && !m_pAssignedCameraToWorld) || !engine->GetWorld().m_Camera))
        {

            engine->GetWorld().m_CameraOverride = m_EditorViewportCamera;
            m_pAssignedCameraToWorld = true;
        }

        finishTasks();

        if (engine->IsForceStopped())
        {
            StopGame(true);
        }

        m_pEditorUI->OnUpdate(deltaTime);

        updatePendingWorldStart();
    }

    void Editor::onRender()
    {
        m_pEditorUI->OnWorldRender();
    }

    void Editor::onEditorRender()
    {
        m_pEditorUI->OnRender();
    }

    void Editor::initWorkers()
    {
        m_pWorkers.reserve(WORKER_THREAD_COUNT);

        m_pTasks = MV::make_rc<MV::thread_safe_queue<MV::rc<EditorTask>>>();
        m_pFinishedTasks = MV::make_rc<MV::thread_safe_queue<MV::rc<EditorTask>>>();

        for (uint32_t i = 0; i < WORKER_THREAD_COUNT; i++)
        {
            auto& worker = m_pWorkers.emplace_back(MV::make_rc<EditorWorker>(m_pTasks, m_pFinishedTasks));
            worker->Init();
        }
    }

    void Editor::cleanupWorkers()
    {
        for (uint32_t i = 0; i < WORKER_THREAD_COUNT; i++)
        {
            m_pWorkers[i]->Cleanup();
        }
    }

    void Editor::finishTasks()
    {
        try
        {
            auto toFinish = std::move(m_pFinishedTasks->pop(false));
            const auto res = toFinish->Finish();

            if (res == EditorTaskFinishResult::FAILED)
            {
                MV::GetLogger()->LogError("Failed to process task. {0}", toFinish->GetErrorText());
            }

            if (res == EditorTaskFinishResult::REQUEUE)
            {
                m_pFinishedTasks->push(std::move(toFinish));
            }
        } catch (MV::thread_safe_queue_no_pop_exception& e)
        {

        } catch (std::exception& e)
        {
            MV::GetLogger()->LogError("Failed to process task. err: {0}", e.what());
        }
    }

    void Editor::queueTask(MV::rc<EditorTask> task)
    {
        m_pTasks->push(std::move(task));
    }

    Editor::~Editor()
    {
        for (auto& worker: m_pWorkers)
        {
            if (worker)
                worker->Cleanup();
            worker.reset();
        }
        if (m_pFinishedTasks) m_pFinishedTasks->wake_cv();
        if (m_pTasks) m_pTasks->wake_cv();
    }

    void Editor::initLastOpenedProjects()
    {
        std::ifstream ifs("recent_projects.json");

        if (!ifs.is_open())
            return;

        try
        {
            auto json = nlohmann::json::parse(ifs);

            const auto size = json.size();

            m_pRecentProjects.resize(size);

            for (size_t i = 0; i < size; i++)
            {
                m_pRecentProjects[i].FromJson(json[i]);
            }

            for (size_t i = 0; i < size; i++)
            {
                auto index = size - i - 1;

                std::ifstream tmpIfs(m_pRecentProjects[index].m_Path.c_str());

                if (!tmpIfs.is_open())
                {
                    m_pRecentProjects.erase(m_pRecentProjects.begin() + index);
                    continue;
                }

                tmpIfs.close();
            }


        } catch (std::exception& e)
        {
            m_pRecentProjects.clear();
        }
        ifs.close();
    }

    std::vector<RecentProject>& Editor::GetRecentProjects()
    {
        return m_pRecentProjects;
    }

    void Editor::SaveRecentProjects()
    {
        std::ofstream ofs("recent_projects.json");

        if (!ofs.is_open())
            return;

        try
        {
            auto json = nlohmann::json::array();

            for (size_t i = 0; i < m_pRecentProjects.size(); i++)
            {
                auto rcJson = nlohmann::json{};
                m_pRecentProjects[i].ToJson(rcJson);
                json.emplace_back(rcJson);
            }

            if (json.empty())
            {
                ofs << "[]";
            } else
            {
                ofs << json;
            }
        } catch (std::exception& e)
        {
            m_pRecentProjects.clear();
        }
        ofs.close();
    }

    void Editor::StartGame()
    {
        if (m_pGameStartPending) return;

        m_SelectedWO_Runtime = m_SelectedWO;

        auto& world = MV::GetEngine()->GetWorld();

        world.m_CameraOverride.reset();


        MV::SerializationWhitelist wl;
        for (auto& v: world.m_WorldData.m_WorldObjects)
        {
            auto& wo = world.m_WorldObjects[v.m_WorldID];
            v.m_DataAr = MV::Archive{};

            const auto type = wo->GetTypeHash();

            if (type == MV::Meta::sc_InvalidClassHash)
            {
                continue;
            }

            const auto meta = MV::Meta::MetaCache::GetInstance().GetClassMetaFromHash(type);

            if (!meta.IsValid())
            {
                continue;
            }

            ENSURE_TRUE(MV::IClassSerializer::WriteTypeFromHash(wo.get(), meta, v.m_DataAr, wl));

            v.m_DataAr.SetToRead();
        }

        world.Reload();
        if (world.AreDependenciesLoaded())
        {
            MV::GetEngine()->StartGame();
            m_pInputMode = InputMode::Game;
            return;
        }

        m_pGameStartPending = true;

        GlobalPopupCustomModal("World loading...", []()
        {
            if (!GetEditor()->IsGameStarting())
            {
                return true;
            }

            const auto progress = GetEditor()->GetStartProgress();

            ImGui::ProgressBar((float) progress.m_Current / (float) progress.m_Total, {-1, 0},
                               fmt::format("{} / {}", progress.m_Current, progress.m_Total).c_str());

            return false;
        });
    }

    void Editor::StopGame(bool skipEngineStopGame /*= false*/)
    {
        if (m_pGameStartPending) return;

        m_SelectedWO_Runtime = 0;
        m_SelectedWO = 0;
        m_pInputMode = InputMode::Editor;
        MV::GetEngine()->StopGame();

        const auto camera = MV::GetEngine()->GetWorld().m_Camera;

        auto& cameraSettings = camera->GetCameraData();
        auto cameraZoom = camera->GetZoom();

        MV::GetEngine()->GetWorld().Reload();

        auto newCamera = MV::GetEngine()->GetWorld().m_Camera;

        //newCamera->GetCameraData() = cameraSettings;
        newCamera->SetZoom(cameraZoom);
        m_pAssignedCameraToWorld = false;
    }

    void Editor::CloseCurrentProject()
    {
        auto* engine = MV::GetEngine();

        engine->StopGame();
        m_SelectedWO_Runtime = 0;
        m_SelectedWO = 0;


        m_ProjectAssetTypes.clear();
        m_pLoadedProjectPath.clear();
        m_pEditorUI->m_StartDialog = true;
    }

    void Editor::SaveData()
    {
        auto engine = MV::GetEngine();
        ProjectProducer::SaveProject(engine->GetProject(),
                                     GetEditor()->GetLoadedProjectPath());

        auto& world = engine->GetWorld();

        if (world.IsValid() && MV::Contains(GetEditor()->m_DirtyAssets, world.m_ID))
        {
            const auto path = MV::Resource::PathProjectRelativeToAbsolute(
                    engine->GetProject().m_Assets[world.m_ID].m_Path.c_str());

            MV::SerializationWhitelist wl;
            for (auto& v: world.m_WorldData.m_WorldObjects)
            {
                auto& wo = world.m_WorldObjects[v.m_WorldID];
                v.m_DataAr = MV::Archive{};

                const auto type = wo->GetTypeHash();

                if (type == MV::Meta::sc_InvalidClassHash)
                {
                    continue;
                }

                const auto meta = MV::Meta::MetaCache::GetInstance().GetClassMetaFromHash(type);

                if (!meta.IsValid())
                {
                    continue;
                }

                bool result = (MV::IClassSerializer::WriteTypeFromHash(wo.get(), meta, v.m_DataAr, wl));

                ENSURE_TRUE(result);

                v.m_DataAr.SetToRead();
            }

            engine->GetWorld().m_WorldData.m_Dependencies = engine->GetWorld().m_DependencyGraph;

            world.m_WorldData.m_Subsystems.clear();
            for (auto&& v: world.m_Subsystems)
            {
                const auto type = v.second->GetTypeHash();

                if (type == MV::Meta::sc_InvalidClassHash)
                {
                    continue;
                }

                const auto meta = MV::Meta::MetaCache::GetInstance().GetClassMetaFromHash(type);
                if (!meta.IsValid())
                {
                    continue;
                }

                auto& result = world.m_WorldData.m_Subsystems.emplace_back();

                result.m_ClassHash = v.first;
                result.m_Enabled = true;

                bool writeRes = (MV::IClassSerializer::WriteTypeFromHash(v.second.get(), meta, result.m_Data, wl));

                ENSURE_TRUE(writeRes);

                result.m_Data.SetToRead();
            }

            WorldProducer::CreateWorldResource(engine->GetWorld().m_WorldData,
                                               path, true);
            GetEditor()->m_DirtyAssets.erase(engine->GetWorld().m_ID);
        }
    }

    void Editor::OnAssetChange()
    {
        m_ProjectAssetTypes.clear();
        for (auto& asset: MV::GetEngine()->GetProject().m_Assets)
        {
            m_ProjectAssetTypes[asset.second.m_Path] = std::make_pair(asset.first, asset.second.m_Type);
        }
    }

    bool Editor::TextInputGlobal(MV::string_view name, MV::string* data, const GlobalTextInputCallback& onFinish)
    {
        return m_pEditorUI->TextInput(name, data, onFinish);
    }

    void Editor::MarkAssetDirtyForWorld(MV::IAssetReference Asset)
    {
        m_pDirtyAssetsForWorld.emplace(Asset);
    }

    MV::vector<MV::IAssetReference> Editor::FetchDirtyAssetsForWorld()
    {
        auto res = MV::ToVector(m_pDirtyAssetsForWorld);

        m_pDirtyAssetsForWorld.clear();

        return res;
    }

    void Editor::SetInputMode(InputMode inputMode)
    {
        m_pInputMode = inputMode;

        auto& world = MV::GetEngine()->GetWorld();

        if (!world.IsValid()) return;

        if (m_pInputMode == InputMode::Editor)
        {
            world.m_CameraOverride = m_EditorViewportCamera;
        } else
        {
            world.m_CameraOverride.reset();
        }
    }

    bool Editor::GlobalPopupOk(MV::string_view title, PopupCloseCallback&& callback, MV::string_view body)
    {
        return m_pEditorUI->GlobalPopupOk(title, MV::move(callback), body);
    }

    bool Editor::GlobalPopupConfirmOrCancel(MV::string_view title, PopupCloseCallback&& callback, MV::string_view body)
    {
        return m_pEditorUI->GlobalPopupConfirmOrCancel(title, MV::move(callback), body);
    }

    bool Editor::GlobalPopupYesNo(MV::string_view title, PopupCloseCallback&& callback, MV::string_view body)
    {
        return m_pEditorUI->GlobalPopupYesNo(title, MV::move(callback), body);
    }

    void Editor::updatePendingWorldStart()
    {
        if (!m_pGameStartPending) return;

        auto& world = MV::GetEngine()->GetWorld();
        if (!world.AreDependenciesLoaded())
        {
            m_pGameStartProgress = world.GetLoadingProgress();
            return;
        }

        MV::GetEngine()->StartGame();
        m_pInputMode = InputMode::Game;

        m_pGameStartPending = false;
    }

    bool Editor::GlobalPopupCustomModal(MV::string_view title, CustomModalDrawCallback&& callback)
    {
        return m_pEditorUI->GlobalPopupCustomModal(title, MV::move(callback));
    }
}
