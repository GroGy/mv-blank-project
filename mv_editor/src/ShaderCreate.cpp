//
// Created by Matty on 2022-03-13.
//

#include "../editor/interface/ShaderCreate.h"
#include "../editor/imgui/imgui.h"
#include "../editor/util/UtilGui.h"
#include "../editor/EditorCommon.h"

#include <filesystem>

namespace Editor {
    ShaderCreate::ShaderCreate() : Window(Config::sc_ShaderCreateWindowName.data()) {
        m_Flags = m_Flags | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse;
        m_pOpen = false;
    }

    void ShaderCreate::WindowSetup() {
        const ImGuiViewport* viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->GetCenter(), ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
        ImGui::SetNextWindowSize({450.0f, 250.0f}, ImGuiCond_Appearing);
    }

    void ShaderCreate::Render() {
        ScopedVertical vert{"ShaderCreate##wrapper_vert"};

        if (m_pCreating && m_pTask) {
            ScopedCenter center{"ShaderCreate##wrapper_center"};
            ImGui::ProgressBar(m_pTask->GetProgress(), {-1.0f, 0}, "Creating...");
            if (m_pTask->IsDone()) {
                m_pCreating = false;
                m_pError = m_pTask->GetErrorText();
                if(m_pTask->Success()) {
                    m_pOpen = false;
                    GetEditor()->OnAssetChange();
                }
                m_pTask.reset();
            }
            return;
        }
        
        {
            ScopedHorizontal hor{"ShaderCreate##wrapper_name"};
            ImGui::TextUnformatted("Name:");
            ImGui2::WideInputText("##shader_create_name", &m_pName);
        }

        if (ImGui::Button("Create", {-1.0, 0})) {
            ShaderTaskInput input;

            input.m_Name = m_pName;
            input.m_Path = fmt::format("{}/{}.mvasset", m_pPathBase, m_pName).c_str();

            m_pTask = MV::make_rc<ShaderTask>(MV::move(input));
            m_pTask->Start(m_pTask);
            m_pCreating = true;
        }
    }

    void ShaderCreate::PostRender() {

    }

    void ShaderCreate::OnCleanup() {

    }

    void ShaderCreate::OnInit() {

    }

    void ShaderCreate::SetPath(MV::string_view path) {
        m_pPathBase = path;
    }
}