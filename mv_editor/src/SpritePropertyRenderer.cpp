//
// Created by Matty on 2022-03-11.
//

#include "../editor/interface/worldobject_renderers/SpritePropertyRenderer.h"
#include "../editor/util/UtilGui.h"
#include <2d/SpriteProperty.h>
#include "../editor/interface/common/AssetSelector.h"
#include "common/EngineCommon.h"
#include "interface/object_editor/NumericFieldRenderers.h"
#include "interface/object_editor/GeneralFieldRenderers.h"
#include "Engine.h"
#include "precompiler/AnimatablePropertyRegistrator.h"
#include "tools/animation_editor/AnimationEditorImgui.h"
#include "serialization/common/FieldSerializer.h"

namespace Editor {
    template <typename ClassType,auto F>
    static bool IsFieldAnimated() {
        auto field = MV::GetFieldFromOffset(MV::GetClassMeta<ClassType>(),MV::GetFieldOffset<F>());

        return field &&
               ((field->m_Flags & MV::Meta::Field::AnimationCompatible) == MV::Meta::Field::AnimationCompatible);
    }


    template <typename ClassType,auto F>
    static MV::AnimationInputField* GetAnimationData() {
        if(!IsFieldAnimated<ClassType, F>()) return nullptr;

        auto res = MV::GetAnimatableFieldFromOffset<ClassType>(MV::GetFieldOffset<F>());

        if (!res) {
            return nullptr;
        }

        res->m_Owner = MV::GetClassMeta<ClassType>();

        return res;
    }

    bool SpritePropertyRenderer::Draw(const MV::Meta::Field* field, const MV::rc<MV::WorldObject>& wo,
                                      MV::Property* property) {
        auto data = (MV::SpriteProperty*)(property);
        bool dirty = false;

        if (!Begin("##sprite_general_settings")) {
            return false;
        }

        if (BeginField("Shader:", MV::GetFieldIdChecked<&MV::SpriteProperty::m_Shader>())) {
            //dirty |= FieldDrawingImpl::DrawAssetReference("##sprite_shader");


            bool open = ::ImGui2::AssetSelectButton(data->m_Shader, {ImGui::GetContentRegionAvail().x, 0.0f});

            MV::unordered_set<MV::AssetType> types;
            types.emplace(MV::AssetTypeRegistry::Get().GetRuntimeAssetType<MV::ShaderAssetType>());

            bool confirm = AssetSelector::DrawAssetSelector(
                    fmt::format("##sprite_pipeline_selector_{}", data->m_StorageIndex).c_str(), open,
                    types,
                    data->m_Shader);

            if (confirm)
                dirty = true;

            EndField();
        }

        if (BeginField("Texture:",
                       MV::GetFieldIdChecked<&MV::SpriteProperty::m_Texture>(),
                       GetAnimationData<MV::SpriteProperty, &MV::SpriteProperty::m_Texture>())) {

            MV::unordered_set<MV::AssetType> types;
            types.emplace(MV::AssetTypeRegistry::Get().GetRuntimeAssetType<MV::TextureAssetType>());
            types.emplace(MV::AssetTypeRegistry::Get().GetRuntimeAssetType<MV::AtlasAssetType>());

            auto oldTex = data->m_Texture;
            bool open = ::ImGui2::AssetSelectButton(data->m_Texture, {ImGui::GetContentRegionAvail().x, 0.0f});
            bool confirm = AssetSelector::DrawAssetSelector(fmt::format("##sprite_texture_selector_{}", data->m_StorageIndex).c_str() , open,
                                                            types,
                                                            data->m_Texture);
            auto newTex = data->m_Texture;

            if(oldTex != newTex) {
                data->m_UpdateShaderData = true;
            }

            if (confirm) {
                dirty = true;
                if (data->m_Texture != 0)
                {
                    const auto& proj = MV::GetEngine()->GetProject();
                    const auto foundAsset = proj.m_Assets.find(data->m_Texture);

                    if (foundAsset != proj.m_Assets.end())
                    {
                        data->m_IsAtlasSprite = foundAsset->second.m_Type == MV::AssetTypeRegistry::Get().GetRuntimeAssetType<MV::AtlasAssetType>();
                        data->m_UV = {0.0f, 0.0f, 1.0f, 1.0f};
                    }
                }
            }

            EndField();
        }

        if (data->m_IsAtlasSprite) {
            if (!MV::GetEngine()->m_ResourceManager.IsAssetLoaded(data->m_Texture)) {
                MV::GetEngine()->m_ResourceManager.LoadResource(data->m_Texture);
            } else {
                auto atlas = MV::GetResources()->m_Atlases[data->m_Texture];

                if (BeginField("Sprite:", MV::GetFieldIdChecked<&MV::SpriteProperty::m_SpriteUUID>(),
                               GetAnimationData<MV::SpriteProperty, &MV::SpriteProperty::m_SpriteUUID>())) {
                    ImGui::PushItemWidth(ImGui::GetContentRegionAvail().x);

                    const auto& sprites = atlas->GetSprites();
                    int32_t spriteIndex = -1;
                    MV::Atlas::Sprite selected;
                    static MV::string name;

                    for (int32_t i = 0; i < sprites.size(); ++i)
                    {
                        const auto& v = sprites[i];

                        if(v.m_UUID != data->m_SpriteUUID) continue;

                        selected = v;
                        spriteIndex = i;
                        break;
                    }

                    if(spriteIndex == -1 ) {
                        name = "Unknown";
                    } else {
                        name = selected.m_Name;
                    }

                    if (ImGui::BeginCombo("##sprite_selector_combo", name.c_str())) {
                        for (uint32_t i = 0; i < sprites.size(); i++) {
                            if (ImGui::Selectable(fmt::format("{}##{}",sprites[i].m_Name,i).c_str(), i == spriteIndex)) {
                                const auto& sprite = sprites[i];
                                data->m_SpriteUUID = sprite.m_UUID;
                                data->m_UV = sprite.m_UV;
                                data->m_Pivot = sprite.m_Pivot;
                                data->m_SpriteSize = sprite.m_Size;
                                dirty = true;
                            }
                        }

                        ImGui::EndCombo();
                    }
                }

                EndField();
            }
        }

        if(BeginField("Position:", MV::GetFieldIdChecked<&MV::SpriteProperty::m_Offset>(),
                      GetAnimationData<MV::SpriteProperty, &MV::SpriteProperty::m_Offset>()))
        {
            dirty |= FieldDrawingImpl::DrawVec2("##sprite_position", data->m_Offset);
            EndField();
        }

        if(BeginField("Scale:", MV::GetFieldIdChecked<&MV::SpriteProperty::m_Scale>(),
                      GetAnimationData<MV::SpriteProperty, &MV::SpriteProperty::m_Scale>()))
        {
            dirty |= FieldDrawingImpl::DrawVec2("##sprite_scale", data->m_Scale);
            EndField();
        }

        if(BeginField("Rotation:", MV::GetFieldIdChecked<&MV::SpriteProperty::m_Rotation>()))
        {
            dirty |= FieldDrawingImpl::DrawFloat("##sprite_rotation", data->m_Rotation);
            EndField();
        }

        if(BeginField("Visible:", MV::GetFieldIdChecked<&MV::SpriteProperty::m_Visible>()))
        {
            dirty |= FieldDrawingImpl::DrawBool("##sprite_visible", data->m_Visible);
            EndField();
        }

        if(BeginField("Layer:"))
        {
            dirty |= FieldDrawingImpl::DrawI32("##sprite_layer", data->m_Layer);
            EndField();
        }

        End();

        return dirty;
    }
}