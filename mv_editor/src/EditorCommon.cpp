//
// Created by Matty on 2021-10-24.
//

#include "../editor/EditorCommon.h"

static Editor::Editor * s_Editor;

Editor::Editor *Editor::GetEditor() {
    return s_Editor;
}

void Editor::EditorCommon::SetEditorGlobal(Editor * editor) {
    s_Editor = editor;
}
