//
// Created by Matty on 2022-01-22.
//

#include "../editor/interface/AtlasCreate.h"
#include "../editor/util/UtilGui.h"
#include <common/EngineCommon.h>
#include <resources/AtlasResource.h>
#include "../editor/imgui/imgui_stdlib.h"
#include "../editor/EditorCommon.h"
#include "imgui/imgui_impl_vulkan.h"
#include "render/vulkan/VulkanTexture.h"
#include "common/TextureAsset.h"
#include "render/common/Renderer.h"

namespace Editor {
    AtlasCreate::AtlasCreate() : Window(Config::sc_AtlasCreateWindowName.data()) {
        m_Flags = m_Flags | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse;
        m_pOpen = false;
    }

    void AtlasCreate::WindowSetup() {
        const ImGuiViewport* viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->GetCenter(), ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
        ImGui::SetNextWindowSize({450.0f, 250.0f}, ImGuiCond_Appearing);
    }

    void AtlasCreate::Render() {
        m_pAssetSelector.Render();

        processTexturePreview();

        if (m_pCreating && m_pTask) {
            ScopedCenter center{"TilesetCreate##wrapper_center"};
            ImGui::ProgressBar(m_pTask->GetProgress(), {-1.0f, 0}, "Creating...");
            if (m_pTask->IsDone()) {
                m_pCreating = false;
                m_pError = m_pTask->GetErrorText();
                if (m_pTask->Success()) {
                    m_pOpen = false;
                    GetEditor()->OnAssetChange();
                }
                m_pTask.reset();
            }
            return;
        }

        ScopedVertical vert{"AtlasCreate##wrapper_vert"};

        {
            ScopedHorizontal hor{"AtlasCreate##wrapper_name"};
            ImGui::TextUnformatted("Name:");
            ImGui2::WideInputText("##atlas_create_name", &m_pName);
        }

        {
            ScopedHorizontal hor{"AtlasCreate##wrapper_texture_select"};
            ImGui::TextUnformatted("Texture:");
            ImGui::Spring();
            if (ImGui2::AssetSelectButton(m_pTexture)) {
                m_pAssetSelector.Open();
            }
        }

        {
            float size = 120.0f;
            if(m_pTextureState == Loaded) {
                float sizeX = size * (m_pTextureSize.x / m_pTextureSize.y);

                if(sizeX > ImGui::GetContentRegionAvail().x) {
                    float downscale = ImGui::GetContentRegionAvail().x / sizeX;
                    size *= downscale;
                    sizeX *= downscale;
                }

                ImGui::Image(m_pImGuiTexture, {sizeX, size});
            } else {
                const auto cursor = ImGui::GetCurrentWindow()->DC.CursorPos;
                ImGui::ItemSize({size, size});
                ImGui::GetWindowDrawList()->AddRectFilled(cursor, cursor + ImVec2{size, size}, IM_COL32(0,0,0,255));
            }
        }

        {
            ScopedHorizontal hor{"AtlasCreate##wrapper_generate_sprites"};
            ImGui::Text("Generate sprites:");
            ImGui::Spring();
            ImGui::Checkbox("##generate_sprites", &m_pGenerateSprites);
        }

        {
            ScopedDisabled dis{!m_pGenerateSprites};
            ScopedHorizontal hor{"AtlasCreate##wrapper_size"};
            ImGui::Text("Sprite size:");
            ImGui::Spring();
            ImGui::SetNextItemWidth(64.0f);
            ImGui::DragScalar("##sprite_size_x", ImGuiDataType_U32, &m_pSpriteSize.x);
            ImGui::SetNextItemWidth(64.0f);
            ImGui::DragScalar("##sprite_size_y", ImGuiDataType_U32, &m_pSpriteSize.y);
        }

        if (!m_pError.empty())
            ImGui::TextColored({1.0f, 0.f, 0.f, 1.0f}, "%s", m_pError.c_str());

        {
            const bool checkSpriteSize = m_pGenerateSprites && (m_pSpriteSize.x == 0 || m_pSpriteSize.y == 0);

            ScopedDisabled dis{m_pTexture == 0 || m_pName.empty() || checkSpriteSize};

            if (ImGui::Button("Create", {-1.0, 0})) {
                AtlasTaskInput input;

                input.m_Name = m_pName;
                input.m_Path = GetAssetPath(m_pName);
                input.m_SpriteSize = m_pSpriteSize;
                input.m_Texture = m_pTexture;
                input.m_GenerateSprites = m_pGenerateSprites;

                m_pTask = MV::make_rc<AtlasTask>(input);
                m_pTask->Start(m_pTask);
                m_pCreating = true;
            }
        }

        if (m_pAssetSelector.HasSelected()) {
            auto selected = m_pAssetSelector.GetSelected();

            if (m_pTexture != 0 && m_pTextureState != Unloaded) {
                auto lambda = [texture(m_pImGuiTexture)](MV::RenderCommandBuilder & CmdBuilder){
                    ImGui_ImplVulkan_RemoveTexture((VkDescriptorSet) texture);
                };

                DISPATCH_RT_COMMAND_LAMBDA_DELAYED(DestroyViewportCameraImGuiDesciptorSets, lambda);

                m_pImGuiTexture = nullptr;
                m_pTextureState = Unloaded;
            }

            m_pTexture = selected;
            m_pAssetSelector.Clear();

            if (m_pTextureLoadResource) {
                m_pTextureLoadResource.reset();
            }

            if (MV::GetEngine()->m_ResourceManager.IsAssetLoaded(m_pTexture)) {
                assignImGuiTexture();
            } else {
                if(!m_pTextureLoadResource)
                    m_pTextureLoadResource = MV::dynamic_pointer_cast<MV::TextureResource>(MV::GetEngine()->m_ResourceManager.LoadResource(m_pTexture));
            }
        }
    }

    void AtlasCreate::assignImGuiTexture() {
        if(m_pTextureState != Unloaded) return;

        auto& textures = MV::GetResources()->m_Textures;

        auto foundRes = textures.find(m_pTexture);

        if(foundRes == textures.end()) {
            return;
        }

        if(!foundRes->second->AssetLoaded()) {
            return;
        }

        auto lambda = [texturesState = &m_pTextureState, texture = &m_pImGuiTexture, textureHandle(foundRes->second->GetTextureHandle()), textureSize(&m_pTextureSize)](MV::RenderCommandBuilder & CmdBuilder) {
            MV::rc<MV::Texture> rtTexture;

            ENSURE_TRUE(MV::GetRenderer()->RT_GetResource(textureHandle, rtTexture));

            auto textureImpl = MV::dynamic_pointer_cast<MV::VulkanTextureImpl>(rtTexture);
            *texture = ImGui_ImplVulkan_AddTexture(textureImpl->GetSampler(), textureImpl->m_TextureView, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

            *textureSize = ImVec2{(float)rtTexture->GetSize().x,(float)rtTexture->GetSize().y};

            *texturesState = Loaded;
        };

        m_pTextureState = Pending;

        DISPATCH_RT_COMMAND_LAMBDA(CreateViewportCameraImGuiDesciptorSets, lambda);
    }

    void AtlasCreate::processTexturePreview() {
        if(m_pTextureLoadResource) {
            if(!m_pTextureLoadResource->IsFinished())
                return;

            if(!m_pTextureLoadResource->Success()) {
                m_pTextureLoadResource.reset();
                return;
            }

            assignImGuiTexture();
            m_pTextureLoadResource.reset();
        }
    }

    void AtlasCreate::PostRender() {

    }

    void AtlasCreate::OnCleanup() {

    }

    void AtlasCreate::OnInit() {
        //m_pAssetSelector.SetFilters({MV::TextureAssetType});
    }
}
