//
// Created by Matty on 05.06.2024.
//

#include "interface/WorldObjectConfigCreate.h"
#include "common/EngineCommon.h"
#include "meta/MetaCache.h"
#include "util/UtilGui.h"
#include "EditorCommon.h"
#include "util/UtilVerify.h"
#include "util/String.h"
#include "interface/object_editor/GeneralFieldRenderers.h"
#include "interface/asset_browser/AssetBrowser.h"

namespace Editor
{
    WorldObjectConfigCreate::WorldObjectConfigCreate() : Window(Config::sc_WorldObjectConfigCreateWindowName.data(),
                                                                false)
    {
        m_Flags = m_Flags | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse;
        m_pOpen = false;
    }

    void WorldObjectConfigCreate::WindowSetup()
    {
        const ImGuiViewport* viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->GetCenter(), ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
        ImGui::SetNextWindowSize({450.0f, 250.0f}, ImGuiCond_Appearing);
    }

    void WorldObjectConfigCreate::Render()
    {
        processTask();
        processData();

        ScopedDisabled dis{m_pTask.operator bool()};

        if (!m_pError.empty())
        {
            ImGui::TextColored(ImColor{255, 0, 0}, m_pError.c_str());
        }

        if (ImGui::BeginTable("##wo_config_create_fields", 2, ImGuiTableFlags_NoSavedSettings))
        {
            ImGui::TableNextRow();

            drawFields();

            ImGui::EndTable();
        }

        if (ImGui::Button("Refresh", {-1, 0}))
        {
            m_pHasData = false;
        }

        if (ImGui::BeginChild("##class_selector", {-1, ImGui::GetContentRegionAvail().y - ImGui::GetTextLineHeight() -
                                                       ImGui::GetStyle().ItemSpacing.y * 2 -
                                                       ImGui::GetStyle().WindowPadding.y}, true))
        {
            for (int32_t i = 0; i < m_pAllWorldObjectClasses.size(); i++)
            {
                const auto& info = m_pAllWorldObjectClasses[i];

                if (ImGui::Selectable(fmt::format("{}##{}", info.m_Name.c_str(), i).c_str(), i == m_pSelectedIndex))
                {
                    m_pSelectedIndex = i;
                }
            }

            ImGui::EndChild();
        }

        if (ImGui::Button("Confirm", {-1, 0}))
        {
            startTask();
        }
    }

    void WorldObjectConfigCreate::PostRender()
    {

    }

    void WorldObjectConfigCreate::OnCleanup()
    {

    }

    void WorldObjectConfigCreate::OnInit()
    {

    }

    void WorldObjectConfigCreate::startTask()
    {
        MV::AllocatorTagScope tag{MV::AllocationType::Editor};
        ENSURE_TRUE(!m_pTask.operator bool())

        WorldObjectConfigTaskInput input;

        if (!UtilVerify::VerifyFileName(MV::StringToWString(m_pName)))
        {
            m_pError = "Invalid asset name!";
            return;
        }

        if (m_pSelectedIndex < 0 || m_pSelectedIndex > m_pAllWorldObjectClasses.size())
        {
            m_pError = "Invalid world object class selected!";
            return;
        }

        CHECK(m_pAllWorldObjectClasses[m_pSelectedIndex].IsValid());

        input.m_Name = m_pName;
        input.m_Path = GetAssetPath(m_pName);
        input.m_Class = m_pAllWorldObjectClasses[m_pSelectedIndex];

        m_pTask = MV::make_rc<WorldObjectConfigTask>(MV::move(input));
        m_pTask->Start(m_pTask);
        m_pCreating = true;
    }

    void WorldObjectConfigCreate::processTask()
    {
        if (m_pCreating && m_pTask)
        {
            if (m_pTask->IsDone())
            {
                m_pCreating = false;
                m_pError = m_pTask->GetErrorText();
                if (m_pTask->Success())
                {
                    m_pOpen = false;
                    GetEditor()->OnAssetChange();
                    GetEditor()->GetWindow<AssetBrowser>()->Refresh();
                }
                m_pTask.reset();
            }
        }
    }

    static bool Begin(MV::string_view name)
    {
        if (!ImGui::TableSetColumnIndex(0))
            return false;
        ImGui::TextUnformatted(name.data());
        if (!ImGui::TableSetColumnIndex(1))
            return false;

        return true;
    }

    static void End()
    {
        ImGui::TableNextRow();
    }

    void WorldObjectConfigCreate::drawFields()
    {
        if (Begin("Name"))
        {
            FieldDrawingImpl::DrawString("##wo_config_name", m_pName);
            End();
        }
    }

    void WorldObjectConfigCreate::processData()
    {
        if (m_pHasData) return;

        auto& inst = MV::Meta::MetaCache::GetInstance();

        m_pAllWorldObjectClasses.clear();
        m_pAllWorldObjectClasses = inst.GetClassesWithBaseClass<MV::WorldObject>();
        m_pSelectedIndex = -1;

        m_pHasData = true;
    }
}