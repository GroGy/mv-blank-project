//
// Created by Matty on 2021-11-03.
//

#include "../editor/resources/WorldProducer.h"
#include "../editor/resources/ProducerUtils.h"
#include "precompiler/ClassSerializerRegistrator.h"
#include "../generated/generated.WorldData.Serialization.h"
#include "serialization/binary/BinaryArchiveProcessor.h"

namespace Editor {
    MV::string WorldProducer::CreateWorldResource(MV::WorldData &data, const MV::string &path, bool override) {
        using namespace Producer;

        auto finalPath = override ? path : ProducerUtils::ResolvePath(path);

        MV::Archive ar;
        MV::SerializationWhitelist wl;

        if(!MV::ClassSerializer<MV::WorldData>::WriteClass(data, ar, wl)) {
            return "";
        }

        MV::BinaryArchiveProcessor proc;

        proc.WriteToFile(finalPath, ar);

        return finalPath;
    }
}
