//
// Created by Matty on 03.04.2023.
//

#include <vulkan/vulkan_core.h>
#include "world_picking/ViewportCamera.h"

namespace Editor {

    MV::RenderPassBlueprint ViewportCamera::CreateRenderPassBlueprint()
    {
        auto colorFormat = MV::TextureFormat::R8G8B8A8_SRGB;
        auto depthFormat = MV::TextureFormat::D32_SFLOAT;
        auto idFormat = MV::TextureFormat::R32_UINT;
        auto size = GetResolution();

        auto colorAttachment = MV::RenderPassAttachmentBlueprint(colorFormat);
        colorAttachment.m_SampleCount = 1;
        colorAttachment.m_Layout = MV::TextureLayout::COLOR_ATTACHMENT_OPTIMAL;
        colorAttachment.m_Format = colorFormat;
        colorAttachment.m_FinalLayout = MV::TextureLayout::SHADER_READ_ONLY_OPTIMAL;

        MV::TextureBlueprint colorAttachmentTextureBp{};
        colorAttachmentTextureBp.m_Height = size.y;
        colorAttachmentTextureBp.m_Width = size.x;
        colorAttachmentTextureBp.m_TextureChannels = 4;
        colorAttachmentTextureBp.m_Format = colorFormat;
        colorAttachmentTextureBp.m_Filter = MV::TextureFilter::NEAREST;
        colorAttachmentTextureBp.m_Usage = MV::TextureUsage::ATTACHMENT;
        colorAttachmentTextureBp.m_DebugName = "Editor Viewport Color texture";

        auto depthAttachment = MV::RenderPassAttachmentBlueprint(depthFormat);
        depthAttachment.m_SampleCount = 1;
        depthAttachment.m_Layout = MV::TextureLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
        depthAttachment.m_StoreOP = MV::AttachmentStoreOperation::DONT_CARE;
        depthAttachment.m_FinalLayout = MV::TextureLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        MV::TextureBlueprint depthAttachmentTextureBp{};
        depthAttachmentTextureBp.m_Height = size.y;
        depthAttachmentTextureBp.m_Width = size.x;
        depthAttachmentTextureBp.m_TextureChannels = 1;
        depthAttachmentTextureBp.m_Format = depthFormat;
        depthAttachmentTextureBp.m_Filter = MV::TextureFilter::NEAREST;
        depthAttachmentTextureBp.m_Usage = MV::TextureUsage::DEPTH;
        depthAttachmentTextureBp.m_Mipmaps = 1;
        depthAttachmentTextureBp.m_DebugName = "Editor Viewport Depth texture";


        auto idAttachment = MV::RenderPassAttachmentBlueprint(idFormat);
        idAttachment.m_SampleCount = 1;
        idAttachment.m_Layout = MV::TextureLayout::COLOR_ATTACHMENT_OPTIMAL;
        idAttachment.m_StoreOP = MV::AttachmentStoreOperation::DONT_CARE;
        idAttachment.m_FinalLayout = MV::TextureLayout::COLOR_ATTACHMENT_OPTIMAL;

        MV::TextureBlueprint idAttachmentTextureBp{};
        idAttachmentTextureBp.m_Height = size.y;
        idAttachmentTextureBp.m_Width = size.x;
        idAttachmentTextureBp.m_TextureChannels = 1;
        idAttachmentTextureBp.m_Format = idFormat;
        idAttachmentTextureBp.m_Filter = MV::TextureFilter::NEAREST;
        idAttachmentTextureBp.m_Usage = MV::TextureUsage::ATTACHMENT;
        idAttachmentTextureBp.m_Mipmaps = 1;
        idAttachmentTextureBp.m_DebugName = "Editor Viewport ID picking texture";

        auto blueprint = MV::RenderPassBlueprint();

        blueprint.SetSize(size);

        blueprint.SetClearColor({0.13,0.13,0.15,1.0f});

        auto colorAttachmentIndex = blueprint.AddTextureAttachment(MV::move(colorAttachmentTextureBp),MV::move(colorAttachment));
        auto idAttachmentIndex = blueprint.AddTextureAttachment(MV::move(idAttachmentTextureBp),MV::move(idAttachment));
        auto depthAttachmentIndex = blueprint.AddDepthAttachment(MV::move(depthAttachmentTextureBp),MV::move(depthAttachment));

        {
            MV::SubpassDependency dep{};
            dep.m_SourcePass = MV::sc_ExternalSubpass;
            dep.m_DestinationPass = 0;
            dep.m_SourceStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
            dep.m_DestinationStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
            dep.m_SourceAccessMask = VK_ACCESS_SHADER_READ_BIT;
            dep.m_DestinationAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
            dep.m_DependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

            blueprint.AddSubpassDependency(dep);
        }

        {
            MV::SubpassDependency dep{};
            dep.m_SourcePass = 0;
            dep.m_DestinationPass = MV::sc_ExternalSubpass;
            dep.m_SourceStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
            dep.m_DestinationStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
            dep.m_SourceAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
            dep.m_DestinationAccessMask = VK_ACCESS_SHADER_READ_BIT;
            dep.m_DependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

            blueprint.AddSubpassDependency(dep);
        }

        MV::vector<MV::RenderPassFrameBufferAttachment> attachments;
        attachments.resize(3);

        attachments[0].m_AttachmentIndex = colorAttachmentIndex; // Color
        attachments[1].m_AttachmentIndex = idAttachmentIndex; // id
        attachments[2].m_AttachmentIndex = depthAttachmentIndex; // Depth

        blueprint.SetFrameBufferAttachments(MV::move(attachments));

        //MV::RenderPassBlueprint::DepthTexturePass();

        return blueprint;
    }
}