//
// Created by Matty on 2021-10-15.
//

#include "../editor/interface/Performance.h"
#include "../editor/imgui/imgui.h"
#include "../editor/util/UtilGui.h"
#include "allocator/Alloc.h"
#include "allocator/DefaultAllocator.h"
#include "imgui/imgui_internal.h"
#include "util/Math.h"
#include "common/EngineCommon.h"

#include "render/common/Renderer.h"

namespace Editor {
    void Performance::WindowSetup() {

    }

    void Performance::Render() {
        MV::AllocatorTagScope allocTag{MV::AllocationType::Editor};
        ScopedVertical vert{"perf_vertical_wrapper"};

        const auto renderingStatistics = MV::GetRenderer()->GetRenderingStatistics();

        {
            ScopedHorizontal hor{"gpu_perf_field_memory"};
            ImGui::Text("[GPU] Allocated:");
            ImGui::Spring();
            const auto totalAllocated = renderingStatistics.m_Memory.m_Usage;
            const auto totalAllocatedD = (double)totalAllocated;
            const auto budget = renderingStatistics.m_Memory.m_Budget;
            const auto budgetD = (double)budget;

            ImGui::Text("%.3f / %.3f MB", totalAllocatedD / 1000000.0, budgetD / 1000000.0);
        }


        {
            ScopedHorizontal hor{"perf_field_memory"};
            ImGui::Text("Total allocated:");
            ImGui::Spring();
            const auto totalAllocated = MV::DefaultAllocator::GetInstance().get_total_allocated();
            const auto totalAllocatedD = (double)totalAllocated;

            ImGui::Text("%.3f MB", totalAllocatedD / 1000000.0);
        }

        {
            ScopedHorizontal hor{"perf_field_memory_per_frame"};
            ImGui::Text("Last frame allocated:");
            ImGui::Spring();
            const auto totalAllocated = MV::DefaultAllocator::GetInstance().get_allocated_this_frame();
            const auto totalAllocatedD = (double)totalAllocated;

            ImGui::Text("%.3f kB", totalAllocatedD / 1000.0);
        }

        {
            auto widthTotal = ImGui::GetContentRegionAvail().x;
            auto height = ImGui::GetFrameHeight();

            auto max = 10.0;

            auto clamped = MV::Math::Clamp(MV::GetRenderer()->GetThreadSyncTime() * 1000, -max, max);

            static MV::vector<double> interpolationValues;
            static uint32_t counter = 0;
            static uint32_t interpolationValueCount = 10;

            if(interpolationValues.size() < counter + 1) {
                interpolationValues.emplace_back(clamped);
                counter++;
            } else {
                interpolationValues[counter++] = clamped;
            }


            if(counter == interpolationValueCount) counter = 0;

            auto* window = ImGui::GetCurrentWindow();

            auto cursorStart = window->DC.CursorPos;

            float halfWidth = widthTotal / 2;

            //Middle line
            window->DrawList->AddLine(cursorStart + ImVec2{halfWidth, 0}, cursorStart + ImVec2{halfWidth, height}, IM_COL32(255,255,255,255));

            window->DrawList->AddRect(cursorStart, cursorStart + ImVec2{widthTotal, height}, IM_COL32(255,255,255,255));

            if(interpolationValues.size() == interpolationValueCount) {
                double shownValue = 0;
                for(auto && v : interpolationValues)
                    shownValue += v;

                shownValue /= interpolationValueCount;

                ImColor color = shownValue > 0 ? ImColor{1.0f, 0.33f, 0.0f, 1.0f} : ImColor{0.13f, 0.65f, 1.0f, 1.0f};

                double mapped = MV::Math::Map(shownValue, -max, max, 1, -1);

                auto center = cursorStart + ImVec2{halfWidth, 0};

                float finalPosition = center.x + halfWidth * static_cast<float>(mapped);

                window->DrawList->AddRectFilled(center, ImVec2{finalPosition, center.y + height}, ImGui::ColorConvertFloat4ToU32(color));

                auto text = fmt::format("{:.1f}{}", shownValue, (shownValue == 20.0 ? "+" : (shownValue == -20.0 ? "-" : "")));

                auto totalTextSize = ImGui::CalcTextSize(text.c_str());

                height += totalTextSize.y + ImGui::GetStyle().ItemSpacing.y;

                window->DrawList->AddText(ImVec2{finalPosition - totalTextSize.x / 2, center.y + totalTextSize.y + ImGui::GetStyle().ItemSpacing.y}, IM_COL32(255,255,255,255), text.c_str());
            }


            ImGui::ItemSize(ImVec2{widthTotal, height});
        }

        if(ImGui::BeginTable("##allocations", 2)) {
            ImGui::TableSetupColumn("Category", 0);
            ImGui::TableSetupColumn("Allocated", 0);
            ImGui::TableHeadersRow();

            ImGui::TableNextRow();

            const auto & allocInfo = MV::DefaultAllocator::GetInstance().get_allocation_counters();

            for(uint32_t i = 0; i < MV::AllocationType::Count; i++) {
                auto name = MV::AllocationType::ToString((MV::AllocationType::Flags)i);
                ImGui::TableSetColumnIndex(0);
                ImGui::TextUnformatted(name.data());
                ImGui::TableSetColumnIndex(1);
                const auto totalAllocated = allocInfo[i].load();
                const auto totalAllocatedD = (double)totalAllocated;
                if(totalAllocatedD > 1000000) {  // Show in Mb when more than 1 MB
                    ImGui::TextUnformatted(fmt::format("{:.3f} mB", totalAllocatedD / 1000000.0).c_str());
                } else { // Show in Kb when less
                    ImGui::TextUnformatted(fmt::format("{:.3f} kB", totalAllocatedD / 1000.0).c_str());
                }
                ImGui::TableNextRow();
            }

            ImGui::EndTable();
        }
    }

    void Performance::PostRender() {

    }

    Performance::Performance() : Window(Config::sc_PerformanceWindowName.data()) {

    }

    void Performance::OnCleanup() {

    }

    void Performance::OnInit() {

    }
}