//
// Created by Matty on 2022-03-13.
//

#include "../editor/resources/ShaderProducer.h"
#include "../editor/resources/ProducerUtils.h"

namespace Editor {
    MV::string ShaderProducer::CreateShaderResource(MV::ShaderData &data, const MV::string &path, bool override) {
        using namespace Producer;

        auto finalPath = ProducerUtils::ResolvePath(path, override);
        std::ofstream out{finalPath.c_str()};
        if(!out.is_open()) throw std::exception{};

        data.m_RawData["vertex"] = data.m_Vertex;
        data.m_RawData["fragment"] = data.m_Fragment;

        auto buffersJson = nlohmann::json::array({});
        for(auto && v : data.m_DataBuffers) {
                nlohmann::json bufData{};

                bufData[MV::Config::sc_ShaderJsonStrideField.data()] = v.m_Stride;
                bufData[MV::Config::sc_ShaderJsonBindingsField.data()] = v.m_Binding;
                bufData[MV::Config::sc_ShaderJsonRateField.data()] = static_cast<uint32_t>(v.m_Rate);

                auto attData = nlohmann::json::array({});

                for(auto && a : v.m_Attributes) {
                    nlohmann::json aD{};
                    aD[MV::Config::sc_ShaderJsonLayoutNameField.data()] = a.m_Name;
                    aD[MV::Config::sc_ShaderJsonLayoutTypeField.data()] = a.m_Type;
                    aD[MV::Config::sc_ShaderJsonLayoutLocationField.data()] = a.m_Location;
                    aD[MV::Config::sc_ShaderJsonLayoutOffsetField.data()] = a.m_Offset;
                    attData.push_back(eastl::move(aD));
                }

                bufData[MV::Config::sc_ShaderJsonVertexLayoutField.data()] = eastl::move(attData);

                buffersJson.push_back(eastl::move(bufData));
        }
        data.m_RawData[MV::Config::sc_ShaderJsonInputBuffersField.data()] = eastl::move(buffersJson);

        auto uniformsJson = nlohmann::json::array({});
        for(auto && v : data.m_Uniforms) {
            nlohmann::json uJson{};

            uJson[MV::Config::sc_ShaderJsonUniformShaderStageField.data()] = v.m_ShaderStage;
            uJson[MV::Config::sc_ShaderJsonUniformBindingField.data()] = v.m_Binding;
            uJson[MV::Config::sc_ShaderJsonUniformArraySizeField.data()] = v.m_DescriptorCount;
            uJson[MV::Config::sc_ShaderJsonUniformOffsetField.data()] = v.m_Offset;
            uJson[MV::Config::sc_ShaderJsonUniformSizeField.data()] = v.m_Size;
            uJson[MV::Config::sc_ShaderJsonUniformTypeField.data()] = v.m_Type;
            uJson[MV::Config::sc_ShaderJsonUniformNameField.data()] = v.m_Name;

            uniformsJson.push_back(eastl::move(uJson));
        }
        data.m_RawData[MV::Config::sc_ShaderJsonUniformsField.data()] = eastl::move(uniformsJson);

        auto pushConstantsJson = nlohmann::json::array({});
        for(auto && v : data.m_PushConstants) {
            nlohmann::json pcJson{};

            pcJson[MV::Config::sc_ShaderJsonPushConstantShaderStageField.data()] = v.m_ShaderStage;
            pcJson[MV::Config::sc_ShaderJsonPushConstantOffsetField.data()] = v.m_Offset;
            pcJson[MV::Config::sc_ShaderJsonPushConstantSizeField.data()] = v.m_Size;
            pcJson[MV::Config::sc_ShaderJsonPushConstantNameField.data()] = v.m_Name;
            pcJson[MV::Config::sc_ShaderJsonPushConstantTypeField.data()] = v.m_Type;
            pcJson[MV::Config::sc_ShaderJsonPushConstantTagField.data()] = v.m_Tag;

            pushConstantsJson.push_back(eastl::move(pcJson));
        }
        data.m_RawData[MV::Config::sc_ShaderJsonPushConstantsField.data()] = eastl::move(pushConstantsJson);

        data.m_RawData[MV::Config::sc_ShaderJsonAlphaBlendingField.data()] = data.m_EnableAlphaBlending;

        out << data.m_RawData.dump();

        out.close();

        return finalPath;
    }
}