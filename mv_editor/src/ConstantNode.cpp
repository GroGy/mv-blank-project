//
// Created by Matty on 29.04.2023.
//

#include "tools/shader_editor/nodes/ConstantNode.h"
#include "imgui/imgui.h"
#include "imgui/imgui_internal.h"
#include "tools/shader_editor/common/ShaderGenerationContext.h"


namespace Editor
{

    void IConstantNode::loadNodeData(const nlohmann::json& data)
    {
        ShaderNode::loadNodeData(data);
    }

    void IConstantNode::saveNodeData(nlohmann::json& data)
    {
        ShaderNode::saveNodeData(data);
    }

    bool IConstantNode::shouldDrawPreview() const
    {
        return true;
    }

    bool IConstantNode::shouldDrawHeader() const
    {
        return false;
    }

    IConstantNode::IConstantNode(uint32_t& idCounter) : ShaderNode(idCounter)
    {

    }

    void IConstantNode::drawPreviewImpl()
    {
        auto* window = ImGui::GetCurrentWindow();
        auto cursor = window->DC.CursorPos;
        ImVec2 size{150, 150};

        auto* drawlist = window->DrawList;

        drawlist->AddRectFilled(cursor, cursor + size, ImGui::ColorConvertFloat4ToU32(
                {m_pPreviewColor.x, m_pPreviewColor.y, m_pPreviewColor.z, m_pPreviewColor.w}));

        ImGui::ItemSize(size);

        ShaderNode::drawPreviewImpl();
    }

    bool IConstantNode::drawBodyImpl()
    {
        return ShaderNode::drawBodyImpl();
    }

    bool IConstantNode::InsertShaderCode(ShaderGenerationContext& context)
    {
        return ShaderNode::InsertShaderCode(context);
    }

    MV::string IConstantNode::getDeclareVariableShaderCode(MV::string_view variableType, MV::string_view variableName)
    {
        return fmt::format(Config::sc_DeclareShaderVariablePrefixFmt.data(), variableType, variableName).c_str();
    }

    Vec4ConstantNode::Vec4ConstantNode(uint32_t& idCounter) : ScalarConstantNode(idCounter)
    {
        m_pNodeName = "Vec4 constant";
        updatePreviewColor();
    }

    void Vec4ConstantNode::updatePreviewColor()
    {
        m_pPreviewColor = m_Value;
    }

    bool Vec4ConstantNode::InsertShaderCode(ShaderGenerationContext& context)
    {
        auto name = context.NextVariableName();
        context.m_OutputNames[m_pOutputPins.front().m_Id] = name;
        auto prefix = getDeclareVariableShaderCode("vec4", name);

        MV::string finalLine = fmt::format("{}vec4({:3}f,{:3}f,{:3}f,{:3}f);", prefix, m_Value.x, m_Value.y, m_Value.z,
                                           m_Value.w).c_str();

        context.AddShaderLine(MV::move(finalLine));
        return true;
    }

    FloatConstantNode::FloatConstantNode(uint32_t& idCounter) : ScalarConstantNode(idCounter)
    {
        m_pNodeName = "Float constant";
        updatePreviewColor();
    }

    void FloatConstantNode::updatePreviewColor()
    {
        m_pPreviewColor.x = m_Value.x;
        m_pPreviewColor.y = m_Value.x;
        m_pPreviewColor.z = m_Value.x;
        m_pPreviewColor.w = 1.0f;
    }

    bool FloatConstantNode::InsertShaderCode(ShaderGenerationContext& context)
    {
        auto name = context.NextVariableName();
        context.m_OutputNames[m_pOutputPins.front().m_Id] = name;
        auto prefix = getDeclareVariableShaderCode("float", name);

        MV::string finalLine = fmt::format("{}{:3}f;", prefix, m_Value.x).c_str();

        context.AddShaderLine(MV::move(finalLine));
        return true;
    }

    MakeVec4Node::MakeVec4Node(uint32_t& idCounter) : MakeScalarNode(idCounter)
    {
        updatePreviewColor();
    }

    bool MakeVec4Node::InsertShaderCode(ShaderGenerationContext& context)
    {
        for (auto&& i: m_pInputPins)
        {
            if (context.m_OutputNames.find(i.m_ConnectedPin) == context.m_OutputNames.end())
                return false;
        }

        auto resultVariableName = context.NextVariableName();

        context.m_OutputNames[m_pOutputPins.front().m_Id] = resultVariableName;

        MV::string_view type = "vec4";

        MV::string xVar = context.m_OutputNames[m_pInputPins[0].m_ConnectedPin];
        MV::string yVar = context.m_OutputNames[m_pInputPins[1].m_ConnectedPin];
        MV::string zVar = context.m_OutputNames[m_pInputPins[2].m_ConnectedPin];
        MV::string wVar = context.m_OutputNames[m_pInputPins[3].m_ConnectedPin];

        MV::string line = fmt::format("{0} {1} = {0}({2}, {3}, {4}, {5});", type, resultVariableName, xVar, yVar, zVar,
                                      wVar).c_str();

        context.AddShaderLine(MV::move(line));

        return true;
    }

    void MakeVec4Node::updatePreviewColor()
    {
        m_pPreviewColor.x = m_Value.r;
        m_pPreviewColor.y = m_Value.g;
        m_pPreviewColor.z = m_Value.b;
        m_pPreviewColor.w = m_Value.a;
    }

    MakeVec3Node::MakeVec3Node(uint32_t& idCounter) : MakeScalarNode(idCounter)
    {
        updatePreviewColor();
    }

    bool MakeVec3Node::InsertShaderCode(ShaderGenerationContext& context)
    {
        for (auto&& i: m_pInputPins)
        {
            if (context.m_OutputNames.find(i.m_ConnectedPin) == context.m_OutputNames.end())
                return false;
        }

        auto resultVariableName = context.NextVariableName();

        context.m_OutputNames[m_pOutputPins.front().m_Id] = resultVariableName;

        MV::string_view type = "vec3";

        MV::string xVar = context.m_OutputNames[m_pInputPins[0].m_ConnectedPin];
        MV::string yVar = context.m_OutputNames[m_pInputPins[1].m_ConnectedPin];
        MV::string zVar = context.m_OutputNames[m_pInputPins[2].m_ConnectedPin];

        MV::string line = fmt::format("{0} {1} = {0}({2}, {3}, {4});", type, resultVariableName, xVar, yVar,
                                      zVar).c_str();

        context.AddShaderLine(MV::move(line));

        return true;
    }

    void MakeVec3Node::updatePreviewColor()
    {
        m_pPreviewColor.x = m_Value.r;
        m_pPreviewColor.y = m_Value.g;
        m_pPreviewColor.z = m_Value.b;
        m_pPreviewColor.w = 1.0f;
    }

    Vec3ConstantNode::Vec3ConstantNode(uint32_t& idCounter) : ScalarConstantNode(idCounter)
    {
        m_pNodeName = "Vec3 constant";
        updatePreviewColor();
    }

    bool Vec3ConstantNode::InsertShaderCode(ShaderGenerationContext& context)
    {
        auto name = context.NextVariableName();
        context.m_OutputNames[m_pOutputPins.front().m_Id] = name;
        auto prefix = getDeclareVariableShaderCode("vec3", name);

        MV::string finalLine = fmt::format("{}vec3({:3}f,{:3}f,{:3}f);", prefix, m_Value.x, m_Value.y,
                                           m_Value.z).c_str();

        context.AddShaderLine(MV::move(finalLine));
        return true;
    }

    void Vec3ConstantNode::updatePreviewColor()
    {
        m_pPreviewColor.x = m_Value.r;
        m_pPreviewColor.y = m_Value.g;
        m_pPreviewColor.z = m_Value.b;
        m_pPreviewColor.w = 1.0f;
    }

    BreakVec4Node::BreakVec4Node(uint32_t& idCounter) : BreakScalarNode(idCounter)
    {

    }

    static bool
    breakVecShaderCode(ShaderGenerationContext& context, int32_t len, const MV::vector<ShaderNodePin>& inputPins,
                       const MV::vector<ShaderNodePin>& outputPins)
    {
        for (auto&& i: inputPins)
        {
            if (context.m_OutputNames.find(i.m_ConnectedPin) == context.m_OutputNames.end())
                return false;
        }

        MV::string inputName = context.m_OutputNames.find(inputPins.front().m_ConnectedPin)->second;

        static const char* pinNames[] = {
                "x",
                "y",
                "z",
                "w"
        };

        for (int32_t i = 0; i < len; i++)
        {
            MV::string name = fmt::format("{}.{}", inputName, pinNames[i]).c_str();
            context.m_OutputNames[outputPins[i].m_Id] = name;
        }

        return true;
    }

    bool BreakVec4Node::InsertShaderCode(ShaderGenerationContext& context)
    {
        return breakVecShaderCode(context, m_Len, m_pInputPins, m_pOutputPins);
    }

    BreakVec3Node::BreakVec3Node(uint32_t& idCounter) : BreakScalarNode(idCounter)
    {

    }

    bool BreakVec3Node::InsertShaderCode(ShaderGenerationContext& context)
    {
        return breakVecShaderCode(context, m_Len, m_pInputPins, m_pOutputPins);
    }

    TimeNode::TimeNode(uint32_t& idCounter) : ShaderNode(idCounter)
    {
        m_pNodeName = "Time";

        ShaderNodePin pin;
        pin.m_Id = ++idCounter;
        pin.m_Optional = false;
        pin.m_ShowName = false;
        pin.m_Name = "Output";
        pin.m_Type = ShaderNodePinType::FLOAT;
        this->m_pOutputPins.push_back(MV::move(pin));

    }

    bool TimeNode::InsertShaderCode(ShaderGenerationContext& context)
    {
        context.m_OutputNames[m_pOutputPins.front().m_Id] = "PushConstants.scTime";
        return true;
    }

    bool TimeNode::drawBodyImpl()
    {
        ImGui::TextUnformatted("Time");

        return false;
    }

    bool TimeNode::shouldDrawHeader() const
    {
        return false;
    }

    void TimeNode::AssignShaderFlags(ShaderGenerationContext& context)
    {
        context.m_Flags |= ShaderFlags_Time;
    }
}