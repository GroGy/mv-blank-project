//
// Created by Matty on 19.08.2023.
//

#include "interface/AnimationCreate.h"
#include "common/EngineCommon.h"
#include "util/UtilGui.h"
#include "EditorCommon.h"
#include "util/UtilVerify.h"
#include "util/String.h"
#include "interface/object_editor/GeneralFieldRenderers.h"

namespace Editor {
    AnimationCreate::AnimationCreate() : Window(Config::sc_AnimationCreateWindowName.data(), false)
    {
        m_Flags = m_Flags | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse;
        m_pOpen = false;
    }

    void AnimationCreate::WindowSetup()
    {
        ImGui::SetNextWindowSize({450.0f, 250.0f}, ImGuiCond_Appearing);
    }

    void AnimationCreate::Render()
    {
        processTask();

        ScopedDisabled dis{m_pTask.operator bool()};

        if (ImGui::BeginTable("##animation_create_fields", 2, ImGuiTableFlags_NoSavedSettings)) {
            ImGui::TableNextRow();

            drawFields();

            ImGui::EndTable();
        }

        if(ImGui::Button("Confirm", {-1,0})) {
            startTask();
        }
    }

    void AnimationCreate::PostRender()
    {

    }

    void AnimationCreate::OnCleanup()
    {

    }

    void AnimationCreate::OnInit()
    {

    }

    void AnimationCreate::startTask()
    {
        MV::AllocatorTagScope tag{MV::AllocationType::Editor};
        ENSURE_TRUE(!m_pTask.operator bool())

        AnimationTaskInput input;

        if(!UtilVerify::VerifyFileName(MV::StringToWString(m_pName))) {
            m_pError = "Invalid asset name!";
            return;
        }

        input.m_Name = m_pName;
        input.m_Path = GetAssetPath(m_pName);

        m_pTask = MV::make_rc<AnimationTask>(MV::move(input));
        m_pTask->Start(m_pTask);
        m_pCreating = true;
    }

    void AnimationCreate::processTask()
    {
        if (m_pCreating && m_pTask) {
            if (m_pTask->IsDone()) {
                m_pCreating = false;
                m_pError = m_pTask->GetErrorText();
                if(m_pTask->Success()) {
                    m_pOpen = false;
                    GetEditor()->OnAssetChange();
                }
                m_pTask.reset();
            }
        }
    }

    static bool Begin(MV::string_view name) {
        if (!ImGui::TableSetColumnIndex(0))
            return false;
        ImGui::TextUnformatted(name.data());
        if (!ImGui::TableSetColumnIndex(1))
            return false;

        return true;
    }

    static void End() {
        ImGui::TableNextRow();
    }

    void AnimationCreate::drawFields()
    {
        if(Begin("Name")) {
            FieldDrawingImpl::DrawString("##animation_name", m_pName);
            End();
        }
    }
}