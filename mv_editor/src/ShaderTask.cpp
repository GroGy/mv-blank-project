//
// Created by Matty on 2022-03-13.
//

#include "../editor/async/ShaderTask.h"
#include "../editor/resources/ShaderProducer.h"
#include "common/EngineCommon.h"
#include "../editor/resources/ProducerUtils.h"
#include "../editor/resources/ProjectProducer.h"
#include "../editor/EditorCommon.h"
#include "resources/common/ShaderAssetType.h"

namespace Editor {
    ShaderTask::ShaderTask(ShaderTaskInput &&input) : m_pInput(eastl::move(input)) {

    }

    void ShaderTask::Process() {
        m_pProgress = 0.0f;
        MV::ShaderData data;

        data.m_Vertex = Config::sc_VertexDefault;
        data.m_Fragment = Config::sc_FragmentDefault;
        m_pProgress = 0.3f;

        MV::ShaderDataBuffer inputDataBufferConfig{};

        inputDataBufferConfig.m_Binding = 0;
        inputDataBufferConfig.m_Stride = 20;
        inputDataBufferConfig.m_Rate = static_cast<uint8_t>(MV::VertexInputRate::PER_VERTEX);

        inputDataBufferConfig.m_Attributes.resize(2);

        inputDataBufferConfig.m_Attributes[0].m_Location = 0;
        inputDataBufferConfig.m_Attributes[0].m_Type = static_cast<uint32_t>(MV::AttributeType::R32G32B32_SFLOAT);
        inputDataBufferConfig.m_Attributes[0].m_Offset = 0;
        inputDataBufferConfig.m_Attributes[0].m_Name = "Position";

        inputDataBufferConfig.m_Attributes[1].m_Location = 1;
        inputDataBufferConfig.m_Attributes[1].m_Type = static_cast<uint32_t>(MV::AttributeType::R32G32_SFLOAT);
        inputDataBufferConfig.m_Attributes[1].m_Offset = 12;
        inputDataBufferConfig.m_Attributes[1].m_Name = "UV";

        data.m_DataBuffers.push_back(eastl::move(inputDataBufferConfig));

        MV::ShaderDataPushConstant mvpConstant{};
        mvpConstant.m_Offset = 0;
        mvpConstant.m_Name = "MVP";
        mvpConstant.m_Tag = static_cast<uint32_t>(MV::PushConstantInfo::Field::MVP);
        mvpConstant.m_Size = 4*4*sizeof(float); //Matrix4x4
        mvpConstant.m_Type = static_cast<uint32_t>(MV::AttributeType::MAT4_SFLOAT);
        mvpConstant.m_ShaderStage = MV::ShaderStage::VERTEX;

        data.m_PushConstants.push_back(eastl::move(mvpConstant));

        auto & uniforms = data.m_Uniforms;

        uniforms.resize(2);

        uniforms[0].m_Size = 0; //Sampler size is 0 because its texture sampler
        uniforms[0].m_ShaderStage = MV::ShaderStage::FRAGMENT;
        uniforms[0].m_Name = "uTexture";
        uniforms[0].m_Binding = 0;
        uniforms[0].m_Offset = 0; //Offset is 0 because its texture sampler
        uniforms[0].m_Type = 1; //0 is buffer, 1 is texture
        uniforms[0].m_DescriptorCount = 1; // Single texture, not array

        uniforms[1].m_Size = 4*sizeof(float); //Vec4
        uniforms[1].m_ShaderStage = MV::ShaderStage::FRAGMENT;
        uniforms[1].m_Name = "ubo";
        uniforms[1].m_Binding = 1;
        uniforms[1].m_Offset = 0;
        uniforms[1].m_Type = 0; //0 is buffer, 1 is texture
        uniforms[1].m_DescriptorCount = 1;

        data.m_EnableAlphaBlending = false;

        m_pProgress = 0.5f;
        try {
            m_pInput.m_Path = ShaderProducer::CreateShaderResource(data, m_pInput.m_Path);
            m_pInput.m_Path = Producer::ProducerUtils::PathAbsoluteToObjectRelative(m_pInput.m_Path);
            m_pProgress = 1.0f;
            m_pSuccess = true;
        } catch(std::exception &e) {
            (void) e;
            m_pProgress = 0.0f;
            m_pSuccess = false;
        }
    }

    EditorTaskFinishResult ShaderTask::Finish() {
        if(!m_pSuccess) {
            m_pDone = true;
            return EditorTaskFinishResult::FAILED;
        }

        auto &proj = MV::GetEngine()->GetProject();

        auto assetId = Producer::ProducerUtils::GenerateUUID();

        auto& asset = proj.m_Assets[assetId];
        asset.m_Path = m_pInput.m_Path.data();
        asset.m_Type = MV::AssetTypeRegistry::Get().GetRuntimeAssetType<MV::ShaderAssetType>();

        ProjectProducer::SaveProject(proj, GetEditor()->GetLoadedProjectPath());

        m_pDone = true;
        return EditorTaskFinishResult::SUCCESS;
    }
}