//
// Created by Matty on 2022-09-03.
//
#include <shobjidl.h>     // for IFileDialogEvents and IFileDialogControlEvents
#include <propidl.h>      // for the Property System APIs
#include <strsafe.h>      // for StringCchPrintfW
#include "../editor/platform/windows/WindowsFileBrowser.h"
#include "common/EngineCommon.h"
#include "../editor/platform/windows/WindowsPlatform.h"
#include "util/String.h"

namespace Editor {
    WindowsFileBrowser::WindowsFileBrowser(FileBrowserMode mode) : FileBrowser(mode) {

    }

    void WindowsFileBrowser::Open() {
        Open(std::filesystem::current_path());
    }

    void WindowsFileBrowser::Open(std::filesystem::path dir) {
        if (ENSURE(m_pComInitialized)) {
            MV::GetLogger()->LogWarning("Tried to open windows file dialog without initialized COM");
            return;
        }

        FILEOPENDIALOGOPTIONS allowMultiselectFlag =
                m_pMode == FileBrowserMode::OPEN_MULTIPLE ? FOS_ALLOWMULTISELECT : 0L;

        FILEOPENDIALOGOPTIONS selectFoldersFlag =
                m_pMode == FileBrowserMode::OPEN_DIRECTORY ? FOS_PICKFOLDERS : 0L;

        m_pWinFileDialog->SetOptions(
                allowMultiselectFlag |
                FOS_PATHMUSTEXIST |
                FOS_FILEMUSTEXIST |
                selectFoldersFlag);

        MV::vector<_COMDLG_FILTERSPEC> filters;

        MV::vector<MV::wstring> wFilters;
        wFilters.resize(m_pFileFilters.size());

        filters.resize(m_pFileFilters.size());

        for(uint32_t i = 0; i < m_pFileFilters.size(); i++) {
            auto & filter = filters[i];
            auto & filterDef = m_pFileFilters[i];
            auto & filterWDef = wFilters[i];

            filterWDef = MV::StringToWString(filterDef);

            filter.pszName = filterWDef.c_str();
            filter.pszSpec = filterWDef.c_str();
        }

        m_pWinFileDialog->SetFileTypes(filters.size(), filters.data());

        if (m_pMode == FileBrowserMode::OPEN_DIRECTORY || m_pMode == FileBrowserMode::OPEN_MULTIPLE) {
            m_pFiles.clear();
            m_pFiles.reserve(8);
        }

        if (m_pMode == FileBrowserMode::OPEN_SINGLE) {
            m_pFile.clear();
        }

        HRESULT hr = m_pWinFileDialog->Show(NULL);

        if (SUCCEEDED(hr)) {
            IShellItem *pItem;
            HRESULT validItem = m_pWinFileDialog->GetResult(&pItem);

            if (SUCCEEDED(validItem)) {
                PWSTR pszFilePath;
                HRESULT internalHr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);

                // Display the file name to the user.
                if (SUCCEEDED(internalHr)) {
                    if (m_pMode == FileBrowserMode::OPEN_SINGLE) {
                        assert(m_pFile.empty());
                        m_pFile = MV::WStringToString(pszFilePath);
                    }

                    if (m_pMode == FileBrowserMode::OPEN_DIRECTORY) {
                        assert(m_pFile.empty());
                        m_pFile = MV::WStringToString(pszFilePath);
                    }

                    if (m_pMode == FileBrowserMode::SAVE) {
                        assert(!m_pSave.empty());
                        m_pSave = MV::WStringToString(pszFilePath);
                    }

                    if (m_pMode == FileBrowserMode::OPEN_MULTIPLE) {
                        m_pFiles.push_back(eastl::move(MV::WStringToString(pszFilePath)));
                    }

                    CoTaskMemFree(pszFilePath);
                }
                pItem->Release();
            }
        }
    }

    void WindowsFileBrowser::Init() {
        HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED |
                                          COINIT_DISABLE_OLE1DDE);
        if (!SUCCEEDED(hr)) {
            MV::GetLogger()->LogError("Failed to open CoInitializeEx windows file browser.\n\tError: {}",
                                      WindowsPlatform::GetWindowsError(hr).c_str());
            return;
        }

        hr = CoCreateInstance(m_pMode == FileBrowserMode::SAVE ? CLSID_FileSaveDialog : CLSID_FileOpenDialog,
                              NULL,
                              CLSCTX_INPROC_SERVER,
                              IID_PPV_ARGS(&m_pWinFileDialog));
        if (!SUCCEEDED(hr)) {
            MV::GetLogger()->LogError("Failed to create IFileDialog.\n\tError: {}",
                                      WindowsPlatform::GetWindowsError(hr).c_str());
            CoUninitialize();
            return;
        }

        m_pComInitialized = true;
    }

    void WindowsFileBrowser::Cleanup() {
        if (m_pComInitialized) {
            m_pWinFileDialog->Release();
            m_pWinFileDialog = nullptr;
            CoUninitialize();
            m_pComInitialized = false;
        }
    }
}