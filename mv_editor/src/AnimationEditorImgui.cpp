//
// Created by Matty on 21.08.2023.
//

#include "tools/animation_editor/AnimationEditorImgui.h"
#include "imgui/imgui_internal.h"
#include "imgui/icons.h"

namespace Editor::ImGui2
{
    using namespace ImGui;


    SequenceButtonRes SequenceButton(MV::string_view label, const ImVec2& size_args /* = ImVec2{0,0}*/)
    {
        SequenceButtonRes result = SequenceButtonRes_None;

        const ImGuiStyle& style = GetStyle();
        const ImGuiID id = GetID(label.data());
        const ImVec2 label_size = CalcTextSize(label.data(), NULL, true);
        auto window = GetCurrentWindow();

        const ImVec2 real_size{label_size.x, GetFrameHeight() * 2.0f};

        ImVec2 size = CalcItemSize(size_args, real_size.x + style.FramePadding.x * 2.0f,
                                   real_size.y + style.FramePadding.y * 2.0f);

        auto cursor = window->DC.CursorPos;

        auto* drawList = window->DrawList;


        auto bgColor = style.Colors[ImGuiCol_Tab];


        auto heightWithoutPadding = real_size.y - style.FramePadding.y;

        auto centerAnchor = heightWithoutPadding / 2.0f - label_size.y / 2.0f;

        const auto widgetPadding = style.FramePadding.x * 3.0f;

        bool deletePressed = false;

        const auto buttonSize = real_size.y / 2;

        const ImVec2 deleteButtonCursor =
                (cursor + ImVec2{size.x, 0}) - ImVec2{widgetPadding, 0} - ImVec2{buttonSize, 0} +
                ImVec2{0, style.FramePadding.y + heightWithoutPadding / 2.0f - buttonSize / 2.0f};

        bool hoveredDelete;
        bool held;

        deletePressed = ButtonBehavior(ImRect{deleteButtonCursor, deleteButtonCursor + ImVec2{buttonSize, buttonSize}},
                                       GetID(label.data()), &hoveredDelete, &held, ImGuiButtonFlags_PressedOnClick);
        const bool hovered = ItemHoverable(ImRect{cursor, cursor + size}, window->GetID(label.data()));

        if (hovered && !hoveredDelete)
        {
            bgColor = style.Colors[ImGuiCol_TabHovered];
            result |= SequenceButtonRes_Hovered;
        }

        //deletePressed = deletePressed || (hoveredDelete && held);

        drawList->AddRectFilled(cursor, cursor + size, ColorConvertFloat4ToU32(bgColor));
        drawList->AddRect(cursor, cursor + size, ColorConvertFloat4ToU32(style.Colors[ImGuiCol_TabHovered]));
        drawList->AddText(cursor + ImVec2{widgetPadding, style.FramePadding.y} + ImVec2{0, centerAnchor},
                          ColorConvertFloat4ToU32(style.Colors[ImGuiCol_Text]), label.data());

        {
            auto color = style.Colors[ImGuiCol_Button];

            if (hovered) color = style.Colors[ImGuiCol_ButtonHovered];

            if (deletePressed) color = style.Colors[ImGuiCol_ButtonActive];

            drawList->AddRectFilled(deleteButtonCursor, deleteButtonCursor + ImVec2{buttonSize, buttonSize},
                                    ColorConvertFloat4ToU32(color), style.FrameRounding);
            drawList->AddText(deleteButtonCursor + style.FramePadding,
                              ColorConvertFloat4ToU32(style.Colors[ImGuiCol_Text]), ICON_FA_TRASH);

            if (deletePressed)
            {
                result |= SequenceButtonRes_DeleteButton;
            }
        }

        ItemSize(size);

        if (!deletePressed && hovered && GetIO().MouseDoubleClicked[ImGuiMouseButton_Left])
        {
            result |= SequenceButtonRes_SelectButton;
        }

        if (hovered && GetIO().MouseClicked[ImGuiMouseButton_Right])
        {
            result |= SequenceButtonRes_RightClicked;
        }

        return result;
    }

    bool RecordButton(const char* label, const ImVec2& size_arg, ImGuiButtonFlags flags)
    {
        using namespace ImGui;
        ImGuiWindow* window = GetCurrentWindow();
        if (window->SkipItems)
            return false;

        ImGuiContext& g = *GImGui;
        const ImGuiStyle& style = g.Style;
        const ImGuiID id = window->GetID(label);
        const ImVec2 label_size = CalcTextSize(label, NULL, true);

        ImVec2 pos = window->DC.CursorPos;
        if ((flags & ImGuiButtonFlags_AlignTextBaseLine) && style.FramePadding.y <
                                                            window->DC.CurrLineTextBaseOffset) // Try to vertically align buttons that are smaller/have no padding so that text baseline matches (bit hacky, since it shouldn't be a flag)
            pos.y += window->DC.CurrLineTextBaseOffset - style.FramePadding.y;
        ImVec2 size = ImVec2{GetFrameHeight() + style.FramePadding.x / 2, GetFrameHeight() - style.FramePadding.y / 2};

        const ImRect bb(pos, pos + size);
        ItemSize(size, style.FramePadding.y);
        if (!ItemAdd(bb, id))
            return false;

        if (g.LastItemData.InFlags & ImGuiItemFlags_ButtonRepeat)
            flags |= ImGuiButtonFlags_Repeat;

        bool hovered, held;
        bool pressed = ButtonBehavior(bb, id, &hovered, &held, flags);

        // Render
        const ImU32 col = GetColorU32(
                (held && hovered) ? ImGuiCol_ButtonActive : hovered ? ImGuiCol_ButtonHovered : ImGuiCol_Button);
        RenderNavHighlight(bb, id);
        RenderFrame(bb.Min, bb.Max, col, true, style.FrameRounding);

        const float radius = label_size.y / 2;

        window->DrawList->AddCircleFilled(bb.Min + style.FramePadding + ImVec2{radius, radius}, radius,
                                          ColorConvertFloat4ToU32(style.Colors[ImGuiCol_Text]), 4);

        // Automatically close popups
        //if (pressed && !(flags & ImGuiButtonFlags_DontClosePopups) && (window->Flags & ImGuiWindowFlags_Popup))
        //    CloseCurrentPopup();

        IMGUI_TEST_ENGINE_ITEM_INFO(id, label, g.LastItemData.StatusFlags);
        return pressed;
    }
}