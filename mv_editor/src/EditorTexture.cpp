//
// Created by Matty on 24.06.2024.
//

#include "interface/common/EditorTexture.h"
#include "common/EngineCommon.h"
#include "imgui/imgui_impl_vulkan.h"
#include "render/common/RenderCommandBuilder.h"
#include "render/common/Renderer.h"
#include "render/common/Texture.h"
#include "render/vulkan/VulkanTextureImpl.h"

namespace Editor {
    void EditorTexture::Unload()
    {
        if(m_pLoadState == Unloaded) {
            return;
        }

        auto lambda = [ texture = m_pHandle](MV::RenderCommandBuilder & CmdBuilder) {
            ImGui_ImplVulkan_RemoveTexture((VkDescriptorSet)texture);
        };

        DISPATCH_RT_COMMAND_LAMBDA_DELAYED(DestroyImGuiTexture, lambda);

        m_pLoadState = Unloaded;
    }

    void EditorTexture::Load(MV::RTHandle textureHandleIn)
    {
        ENSURE_TRUE(m_pLoadState == Unloaded); // Dont load texture without unloading previous one

        auto lambda = [texturesState = &m_pLoadState, texture = &m_pHandle, textureHandle(textureHandleIn), textureSize(&m_pSize)](MV::RenderCommandBuilder & CmdBuilder) {
            MV::rc<MV::Texture> rtTexture;

            ENSURE_TRUE(MV::GetRenderer()->RT_GetResource(textureHandle, rtTexture));

            auto textureImpl = MV::dynamic_pointer_cast<MV::VulkanTextureImpl>(rtTexture);
            *texture = ImGui_ImplVulkan_AddTexture(textureImpl->GetSampler(), textureImpl->m_TextureView, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

            *textureSize = {(float)rtTexture->GetSize().x,(float)rtTexture->GetSize().y};

            *texturesState = Loaded;
        };

        m_pLoadState = Loading;

        DISPATCH_RT_COMMAND_LAMBDA(CreateImGuiTexture, lambda);
    }
}