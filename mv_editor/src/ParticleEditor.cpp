//
// Created by Matty on 09.04.2023.
//

#include "tools/particle_editor/ParticleEditor.h"
#include "imgui/imgui.h"
#include "interface/object_editor/NumericFieldRenderers.h"
#include "interface/object_editor/common/FieldRenderer.h"
#include "particles/cpu/ParticleSystemProperty.h"
#include "interface/object_editor/PropertyFieldRenderer.h"
#include "interface/object_editor/GeneralFieldRenderers.h"
#include "interface/common/viewport_gizmo_providers/WorldObjectGizmoProvider.h"
#include "tools/particle_editor/ParticleEditorDummy.h"
#include "meta/Helpers.h"
#include "profiler/Profiler.h"

namespace Editor
{

    void ParticleEditor::OnCleanup()
    {
        if (m_pWorld.IsValid())
        {
            m_pWorld.Cleanup();
        }
    }

    void ParticleEditor::OnInit()
    {

    }

    void ParticleEditor::WindowSetup()
    {
        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0.0f, 0.0f});
    }

    static constexpr MV::string_view sc_ViewportWindowName = "##particle_editor_viewport";
    static constexpr MV::string_view sc_SettingsWindowName = "##particle_editor_settings";

    void ParticleEditor::Render()
    {
        ImGui::PopStyleVar();

        if (m_pDataState == DataState::NoData)
        {
            ImGui::TextUnformatted("No particle system loaded");
            return;
        }

        setupDockspace();

        drawMenubar();

        if (m_pWorld.m_CameraOverride)
            m_pViewport.Draw(sc_ViewportWindowName.data(), m_pWorld.m_CameraOverride);


        drawSettings();
    }

    void ParticleEditor::PostRender()
    {
    }

    bool ParticleEditor::ShouldRender() const
    {
        return Window::ShouldRender();
    }

    void ParticleEditor::NoRender()
    {
        ImGui::PopStyleVar();
    }

    ParticleEditor::ParticleEditor() : Window(Config::sc_ParticleEditorWindowName.data())
    {
        m_pOpen = false;
        m_Flags |= ImGuiWindowFlags_MenuBar;
    }

    void
    ParticleEditor::SetParticleSystem(MV::World* world, MV::rc<MV::WorldObject> wo, const MV::Meta::Field& field)
    {
        m_pOnSavedCallback = nullptr;
        m_pDataState = DataState::NoData;
        m_pSourceWorld = world;
        m_pSourceWorldObject = MV::move(wo);
        m_pWorldData = MV::WorldData{};

        if (!m_pDummyMeta.IsValid())
        {
            auto meta = MV::GetClassMeta<ParticleEditorDummy>();
            CHECK(meta.IsValid());
            m_pDummyMeta = MV::move(meta);

            for(const auto & f : m_pDummyMeta.GetFields()) {
                if (f.m_Type == MV::Meta::FieldType::Property) { // Particle system is only property on dummy
                    m_pParticleSystemField = f;
                    break;
                }
            }
        }

        if (m_pWorld.m_CameraOverride)
        {
            m_pWorld.m_CameraOverride->Cleanup();
        }

        if (m_pWorld.IsValid())
        {
            m_pDummy->Particles->Destroy();
            m_pWorld.Cleanup();
        }

        m_pWorld = ParticleEditorWorld{MV::WorldData{m_pWorldData}};
        m_pWorld.Init();

        m_pWorld.m_CameraOverride = MV::make_rc<MV::Camera>();
        m_pWorld.m_CameraOverride->Init();

        // Copy object from data here

        m_pSourceField = field;

        MV::WorldObjectSpawnParams params;

        params.m_Class = m_pDummyMeta;
        params.m_Name = "Edited object";
        params.m_SkipSerialization = true;

        m_pDummy = MV::static_pointer_cast<ParticleEditorDummy>(m_pWorld.SpawnWorldObject(params));


        MV::Archive ar;
        MV::SerializationWhitelist wl;
        auto srcMeta = MV::Meta::MetaCache::GetInstance().GetClassMetaFromHash(m_pSourceWorldObject->GetTypeHash());

        CHECK(srcMeta.IsValid());

        MV::Property* prop = nullptr;
        const bool success = srcMeta.GetPropertyField(m_pSourceWorldObject.get(), field,prop);
        CHECK(success);

        const auto res = MV::ClassSerializer<MV::ParticleSystemProperty>::Write(*(MV::ParticleSystemProperty*)prop, ar, wl);

        ENSURE_TRUE(res);

        ar.ResetCursor();
        ar.SetToRead();

        const auto readRes = MV::ClassSerializer<MV::ParticleSystemProperty>::Read(*m_pDummy->Particles,ar, wl);

        ENSURE_TRUE(readRes == MV::FieldReadResult::Ok);

        m_pViewport.SetGizmoProvider(
                MV::make_rc<ActiveWorldWorldObjectGizmoProvider>(m_pDummy, m_pWorld));

        m_pDummy->Particles->m_ShowOpenEditorButton = false;

        m_pDataState = DataState::HasData;
    }

    void ParticleEditor::setupDockspace()
    {
        auto centerNode = ImGui::GetID("##particle_editor_dockspace");
        ImGui::DockSpace(centerNode);

        if (ImGui::DockBuilderGetNode(centerNode)->IsLeafNode())
        {
            ImGuiID viewport_nodeId;
            ImGuiID settings_nodeId;
            ImGui::DockBuilderSplitNode(centerNode, ImGuiDir_Right, 0.3f, &settings_nodeId, &viewport_nodeId);

            const auto viewport_node = ImGui::DockBuilderGetNode(viewport_nodeId);
            viewport_node->LocalFlags |=
                    static_cast<ImGuiDockNodeFlags_>(ImGuiDockNodeFlags_NoDocking | ImGuiDockNodeFlags_NoTabBar) |
                    ImGuiDockNodeFlags_NoSplit;

            const auto settings_node = ImGui::DockBuilderGetNode(settings_nodeId);
            settings_node->LocalFlags |=
                    static_cast<ImGuiDockNodeFlags_>(ImGuiDockNodeFlags_NoDocking | ImGuiDockNodeFlags_NoTabBar) |
                    ImGuiDockNodeFlags_NoSplit;

            ImGui::DockBuilderDockWindow(sc_ViewportWindowName.data(), viewport_nodeId);
            ImGui::DockBuilderDockWindow(sc_SettingsWindowName.data(), settings_nodeId);

            ImGui::DockBuilderFinish(viewport_nodeId);
            ImGui::DockBuilderFinish(settings_nodeId);
        }
    }

    static bool Begin(MV::string_view name)
    {
        if (ImGui::BeginTable(name.data(), 2, ImGuiTableFlags_NoSavedSettings))
        {
            ImGui::TableNextRow();
            return true;
        }

        return false;
    }

    static void End()
    {
        ImGui::EndTable();
    }

    static bool BeginField(MV::string_view name)
    {
        if (!ImGui::TableSetColumnIndex(0))
            return false;
        ImGui::TextUnformatted(name.data());
        if (!ImGui::TableSetColumnIndex(1))
            return false;

        return true;
    }

    static void EndField()
    {
        ImGui::TableNextRow();
    }

    void ParticleEditor::drawSettings()
    {
        if (!ImGui::Begin(sc_SettingsWindowName.data()))
        {
            ImGui::End();
            return;
        }

        if (Begin("##particle_editor_general_settings"))
        {
            if (BeginField("Update:"))
            {
                FieldDrawingImpl::DrawBool("##update_system", m_pUpdateSystem);
                EndField();
            }

            if (BeginField("Burst size:"))
            {
                FieldDrawingImpl::DrawU32("##burst_size", m_pBurstSize);
                EndField();
            }

            End();
            if (ImGui::Button("Burst", {-1, 0}))
            {
                if (m_pBurstSize > 0)
                {
                    m_pDummy->Particles->Burst(m_pBurstSize);
                }
            }
        }

        ImGui::Separator();

        uint32_t counter = 0;

        FieldDrawingImpl::SetNextPropertyHeight(ImGui::GetContentRegionAvail().y);

        bool changed = FieldRenderer<PropertyType>::DrawProperty(EditorPropertyDrawingData::FromField(m_pParticleSystemField), counter,
                                                                 m_pDummy, m_pDummy.get(), m_pDummyMeta);

        if (changed)
        {
            m_pDirty = true;
            m_pShowDirtyDot = true;
        }

        ImGui::End();
    }

    void ParticleEditor::Update(float deltaTime)
    {
        if (m_pDataState == DataState::NoData) return;
        if (!m_pOpen) return;

        if (m_pUpdateSystem)
        {
            MV_PROFILE_FUNCTION("ParticleEditor::Update");
            m_pDummy->Particles->Update(deltaTime);
        }
    }

    void ParticleEditor::OnWorldRender()
    {
        if (m_pDataState == DataState::NoData) return;
        if (!m_pOpen) return;

        m_pWorld.Render();
    }

    void ParticleEditor::drawMenubar()
    {
        if (ImGui::BeginMenuBar())
        {
            if (ImGui::BeginMenu("File"))
            {
                if (ImGui::MenuItem("Save"))
                {
                    if (m_pDirty)
                    {
                        saveToSource();
                        if (m_pOnSavedCallback)
                            m_pOnSavedCallback();
                    }
                    m_pDirty = false;
                    m_pShowDirtyDot = false;
                }

                ImGui::EndMenu();
            }

            ImGui::EndMenuBar();
        }
    }

    void ParticleEditor::saveToSource()
    {
        if (!m_pSourceWorld) return;

        MV::Archive ar;
        MV::SerializationWhitelist wl;
        auto srcMeta = MV::Meta::MetaCache::GetInstance().GetClassMetaFromHash(m_pSourceWorldObject->GetTypeHash());

        CHECK(srcMeta.IsValid());

        const auto serializeRes = MV::ClassSerializer<MV::ParticleSystemProperty>::Write(*m_pDummy->Particles,ar, wl);
        ENSURE_TRUE(serializeRes);

        ar.ResetCursor();
        ar.SetToRead();

        MV::Property* prop = nullptr;
        const bool readRes = srcMeta.GetPropertyField(m_pSourceWorldObject.get(), m_pSourceField,prop);
        CHECK(readRes);

        const auto res = MV::ClassSerializer<MV::ParticleSystemProperty>::Read(*(MV::ParticleSystemProperty*)prop, ar, wl);

        ENSURE_TRUE(res == MV::FieldReadResult::Ok);
    }

    void ParticleEditor::SetOnSavedCallback(const OnSavedCallback& callback)
    {
        m_pOnSavedCallback = callback;
    }

    void ParticleEditor::OnOpenStateChanged(bool newValue)
    {
        /*
        if (!newValue && m_pWorld.IsValid())
        {
            m_pWorld.Cleanup();
        }
         */
    }
}