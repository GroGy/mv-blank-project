//
// Created by Matty on 2022-09-20.
//

#include <interface/common/AssetGenerator.h>
#include <EngineMinimal.h>
#include <filesystem>

namespace Editor {
    MV::string IAssetGenerator::GetAssetPath(MV::string_view assetName, const bool withMvExtension /*= true*/) const {
        std::string pathStr = fmt::format("{}/{}{}", m_CreateDirectory, assetName.data(), withMvExtension ? ".mvasset" : "");
        try {
            std::filesystem::path pathFs{pathStr};
        } catch (const std::exception & e) {
            ENSURE_NO_ENTRY("");
        }
        return pathStr.c_str();
    }
}