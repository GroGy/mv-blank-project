//
// Created by Matty on 2022-01-20.
//

#include <common/EngineCommon.h>
#include <render/vulkan/VulkanTexture.h>
#include <resources/AtlasResource.h>
#include "../editor/interface/TilesetCreate.h"
#include "../editor/imgui/imgui.h"
#include "../editor/imgui/imgui_stdlib.h"
#include "../editor/util/UtilGui.h"
#include "../editor/imgui/imgui_impl_vulkan.h"
#include "../editor/imgui/imgui_internal.h"
#include "../editor/EditorCommon.h"
#include "common/TextureAsset.h"
#include "render/common/Renderer.h"
#include "render/common/Renderer.h"

namespace Editor {

    TilesetCreate::TilesetCreate() : Window(Config::sc_TilesetCreateWindowName.data()) {
        m_Flags = m_Flags | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse;
        m_pOpen = false;
    }

    void TilesetCreate::WindowSetup() {
        const ImGuiViewport* viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->GetCenter(), ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
        ImGui::SetNextWindowSize({450.0f, 250.0f}, ImGuiCond_Appearing);
    }

    void TilesetCreate::Render() {
        m_AssetSelector.Render();
        auto *engine = MV::GetEngine();

        if (m_pCreating && m_pTask) {
            ScopedCenter center{"TilesetCreate##wrapper_center"};
            ImGui::ProgressBar(m_pTask->GetProgress(), {-1.0f, 0}, "Creating...");
            if (m_pTask->IsDone()) {
                m_pCreating = false;
                m_pError = m_pTask->GetErrorText();
                if(m_pTask->Success()) {
                    m_pOpen = false;
                    GetEditor()->OnAssetChange();
                }
                m_pTask.reset();
            }
            return;
        }

        ScopedVertical vert{"TilesetCreate##wrapper_vert"};

        {
            ScopedHorizontal hor{"TilesetCreate##wrapper_name"};
            ImGui::TextUnformatted("Name:");
            ImGui2::WideInputText("##tileset_create_name", &m_pName);
        }

        {
            ScopedHorizontal hor{"TilesetCreate##wrapper_atlas_select"};
            ImGui::TextUnformatted("Atlas:");
            ImGui::Spring();
            if (ImGui2::AssetSelectButton(m_pSelectedAtlas)) {
                m_AssetSelector.Open();
            }
        }

        {
            ScopedDisabled dis{m_pSelectedAtlas == 0 || !m_pSelectedAtlasResource || !m_pSelectedAtlasResource->IsFinished()};
            ImGui::TextUnformatted("Mapping:");
            if (ImGui::Button("Auto-Generate", {-1.0, 0})) {
                generateMappings();
            }

            drawMappings();

            if (ImGui::Button("Create", {-1.0, 0})) {
                TilesetTaskInput input;

                input.m_Name = m_pName;
                input.m_Path = GetAssetPath(m_pName);
                input.m_Atlas = m_pSelectedAtlas;
                input.m_IDToAtlasID = m_pIDToAtlasID;

                m_pTask = MV::make_rc<TilesetTask>(input);
                m_pTask->Start(m_pTask);
                m_pCreating = true;
            }
        }

        if (m_AssetSelector.HasSelected()) {
            auto selected = m_AssetSelector.GetSelected();
            if (m_pSelectedAtlasResource) {
                MV::GetEngine()->m_ResourceManager.UnloadAsset(m_pSelectedAtlas);
            }
            m_pSelectedAtlas = selected;
            m_pSelectedAtlasResource = eastl::dynamic_pointer_cast<MV::AtlasResource>(
                    engine->m_ResourceManager.LoadResource(m_pSelectedAtlas));

            m_AssetSelector.Clear();
        }
    }

    void TilesetCreate::PostRender() {

    }

    void TilesetCreate::OnCleanup() {

    }

    void TilesetCreate::OnInit() {
        //m_AssetSelector.SetFilters({MV::AssetType::ATLAS});
    }

    enum MappingsTableColumnIDs {
        MappingsTableColumnID_Control,
        MappingsTableColumnID_ID,
        MappingsTableColumnID_AtlasID,
        MappingsTableColumnID_Preview
    };

    void TilesetCreate::drawMappings() {
        const auto textureID =
                m_pSelectedAtlas == 0 ? 0 : MV::GetResources()->m_Atlases[m_pSelectedAtlas]->GetTextureAssetID();

        if (textureID == MV::InvalidUUID) {
            return;
        }

        const bool loaded = MV::GetEngine()->m_ResourceManager.IsAssetLoaded(textureID);

        if(!loaded) {
            MV::GetEngine()->m_ResourceManager.LoadResource(textureID);
            return;
        }

        const auto foundRes = MV::GetResources()->m_Textures.find(textureID);

        ENSURE_TRUE(foundRes != MV::GetResources()->m_Textures.end());

        if (!foundRes->second->AssetLoaded())
        {
            return;
        }

        if(m_pTextureState == None) {
            auto lambda = [texturesState = &m_pTextureState, texture = &m_pImGuiTextureID, textureHandle(foundRes->second->GetTextureHandle()), textureSize(&m_pTextureSize)](MV::RenderCommandBuilder & CmdBuilder) {
                MV::rc<MV::Texture> rtTexture;

                ENSURE_TRUE(MV::GetRenderer()->RT_GetResource(textureHandle, rtTexture));

                auto textureImpl = MV::dynamic_pointer_cast<MV::VulkanTextureImpl>(rtTexture);
                *texture = ImGui_ImplVulkan_AddTexture(textureImpl->GetSampler(), textureImpl->m_TextureView, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

                *textureSize = {(float)rtTexture->GetSize().x,(float)rtTexture->GetSize().y};

                *texturesState = Loaded;
            };

            m_pTextureState = Pending;

            DISPATCH_RT_COMMAND_LAMBDA(CreateImGuiTextureForTilesetCreate, lambda);
        }

        if(m_pTextureState != Loaded) {
            return;
        }

        static ImGuiTableFlags tableFlags =
                ImGuiTableFlags_Resizable
                | ImGuiTableFlags_RowBg | ImGuiTableFlags_Borders | ImGuiTableFlags_NoBordersInBody
                | ImGuiTableFlags_ScrollY
                | ImGuiTableFlags_SizingFixedFit;

        int32_t toRemove = -1;

        if (ImGui::BeginTable("Assets table", 4, tableFlags, {-1.0f, -1.0f})) {
            ImGui::TableSetupColumn("Control", ImGuiTableColumnFlags_NoSort | ImGuiTableColumnFlags_WidthFixed,
                                    0.0f, MappingsTableColumnID_Control);
            ImGui::TableSetupColumn("ID", ImGuiTableColumnFlags_DefaultSort | ImGuiTableColumnFlags_WidthFixed,
                                    0.0f, MappingsTableColumnID_ID);
            ImGui::TableSetupColumn("AtlasID", ImGuiTableColumnFlags_WidthFixed, 0.0f,
                                    MappingsTableColumnID_AtlasID);
            ImGui::TableSetupColumn("Preview", ImGuiTableColumnFlags_NoSort | ImGuiTableColumnFlags_WidthStretch,
                                    0.0f, MappingsTableColumnID_Preview);

            ImGui::PushStyleColor(ImGuiCol_TableHeaderBg, {0.8, 0.470, 0.196, 1.0f});
            ImGui::TableHeadersRow();
            ImGui::PopStyleColor();
            ImGui::TableNextRow();


            for (uint32_t i = 0; i < m_pIDToAtlasID.size(); i++) {
                const auto atlas = MV::GetResources()->m_Atlases[m_pSelectedAtlas];
                const auto uv = atlas->GetUVs(m_pIDToAtlasID[i]);

                ImGui::TableSetColumnIndex(MappingsTableColumnID_Control);
                if (ImGui::Button(fmt::format("X##{}", i).c_str(),{64.0f,64.0f})) {
                    toRemove = i;
                }
                ImGui::TableSetColumnIndex(MappingsTableColumnID_ID);
                ImGui::TextUnformatted(std::to_string(i).c_str());
                ImGui::TableSetColumnIndex(MappingsTableColumnID_AtlasID);
                ImGui::TextUnformatted(std::to_string(m_pIDToAtlasID[i]).c_str());
                ImGui::TableSetColumnIndex(MappingsTableColumnID_Preview);

                auto textureSize = m_pTextureSize;

                const auto minXp = (float)textureSize.x * uv.x;
                const auto minYp = (float)textureSize.y * uv.y;
                const auto maxXp = (float)textureSize.x * uv.z;
                const auto maxYp = (float)textureSize.y * uv.w;

                float ratio = fabs(minXp - maxXp) / fabs(minYp - maxYp);

                ImGui::Image(m_pImGuiTextureID, {ratio*64.0f, 64.0f}, {uv.x,uv.y}, {uv.z, uv.w});

                ImGui::SameLine();

                ImGuiWindow* window = ImGui::GetCurrentWindow();
                auto cursor = window->DC.CursorPos;
                ImVec2 imgMax = {64.0f, 64.0f};
                auto uvRect = ImRect{
                    cursor.x + (imgMax.x * uv.x), cursor.y + (imgMax.y * uv.y),
                    cursor.x + (imgMax.x * uv.z), cursor.y + (imgMax.y * uv.w)
                    };
                ImGui::Image(m_pImGuiTextureID, {64.0f, 64.0f});
                auto * drw = ImGui::GetWindowDrawList();
                drw->AddRect(uvRect.Min, uvRect.Max, IM_COL32_WHITE,0);

                ImGui::TableNextRow();
            }
            ImGui::EndTable();
        }

        if(toRemove != -1) {
            m_pIDToAtlasID.erase(m_pIDToAtlasID.begin() + toRemove);
        }

    }

    void TilesetCreate::generateMappings() {
        const auto atlas = MV::GetResources()->m_Atlases[m_pSelectedAtlas];
        m_pIDToAtlasID.clear();

    }

}