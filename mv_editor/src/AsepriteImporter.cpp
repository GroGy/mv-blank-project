//
// Created by Matty on 2021-10-28.
//

#include "../editor/interface/importers/AsepriteImporter.h"
#include "../editor/EditorCommon.h"
#include "../editor/imgui/imgui_stdlib.h"

namespace Editor {
    void AsepriteImporter::Render() {
        m_TextureSelector.Render();

        static bool showTextureError = false;
        static bool showJsonError = false;

        auto & assets = MV::GetEngine()->GetProject().m_Assets;

        ImGui::BeginVertical("##importer_wrapper");

        ImGui::BeginHorizontal("##atlas_texture_wrapper");
        ImGui::Text("Atlas texture");
        ImGui::Spring();
        if(ImGui::Button(m_Texture == 0 ? "<not selected>" : assets[m_Texture].m_Path.c_str())) {
            m_TextureSelector.Open();
        }
        ImGui::EndHorizontal();

        if(showTextureError) {
            ImGui::TextColored({0.9f, 0.1f, 0.1f, 1.0f}, "No valid texture selected!");
        }

        if(m_TextureSelector.HasSelected()) {
            auto selected = m_TextureSelector.GetSelected();
            m_Texture = selected;
            m_TextureSelector.Clear();
        }

        static bool showError = false;
        static bool showSuccess = false;

        ImGui::BeginHorizontal("##json_path_wrapper");
        ImGui::Text("Aseprite json");
        ImGui::Spring();
        ImGui::InputText("##json_path", &m_JsonPath);
        if (ImGui::SmallButton("Browse")) {
            m_JsonFileDialog->Open();
            m_JsonPath = m_JsonFileDialog->GetSelectedFile();
        }
        ImGui::EndHorizontal();
        if(showJsonError) {
            ImGui::TextColored({0.9f, 0.1f, 0.1f, 1.0f}, "No valid json selected!");
        }


        ImGui::Spring();

        if(showSuccess) {
            ImGui::TextColored({0.2f,1.0f,0.2f,1.0f},"Successfully imported atlas.");
        }

        if(showError) {
            ImGui::TextColored({0.9f, 0.1f, 0.1f, 1.0f},"Failed to import atlas.");
        }

        if (m_pImporting && m_pTask) {
            ImGui::ProgressBar(m_pTask->GetProgress(), {-1.0f, 0}, "Importing...");
            if(m_pTask->IsDone()) {
                showError = !m_pTask->Success();
                showSuccess = !showError;
                m_pImporting = false;
                m_pTask.reset();
            }
        } else {
            ImGui::BeginHorizontal("##buttons_hor");
            ImGui::Spring();
            if (ImGui::Button("Import")) {
                bool valid = true;

                showTextureError = m_Texture == 0;
                showJsonError = m_JsonPath.empty();

                if(showTextureError) {
                    valid = false;
                }

                if(showJsonError) {
                    valid = false;
                }

                if(valid) {
                    m_pImporting = true;
                    m_pTask = MV::make_rc<AsepriteTask>(m_JsonPath);
                    m_pTask->Start(m_pTask);
                }
            }
            ImGui::EndHorizontal();
        }

        ImGui::EndVertical();
    }

    AsepriteImporter::AsepriteImporter() : Window(Config::sc_AsepriteImportWindowName.data()) {
        m_Flags = m_Flags | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_AlwaysAutoResize;
        m_pOpen = false;
        m_JsonFileDialog = FileBrowser::Get(FileBrowserMode::OPEN_SINGLE);
    }

    void AsepriteImporter::WindowSetup() {
        m_JsonFileDialog->SetTypeFilters({"*.json"});
    }

    void AsepriteImporter::OnCleanup() {
        m_JsonFileDialog->Cleanup();
    }

    void AsepriteImporter::OnInit() {
        //m_TextureSelector.SetFilters({MV::TextureAssetType});
        m_JsonFileDialog->Init();
    }

    void AsepriteImporter::PostRender() {

    }
}
