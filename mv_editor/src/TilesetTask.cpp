//
// Created by Matty on 2022-01-20.
//

#include <common/EngineCommon.h>
#include "../editor/async/TilesetTask.h"
#include "../editor/resources/TilesetProducer.h"
#include "../editor/resources/ProjectProducer.h"
#include "../editor/EditorCommon.h"
#include "../editor/resources/ProducerUtils.h"
#include "resources/common/TilesetAssetType.h"

namespace Editor {
    TilesetTask::TilesetTask(TilesetTaskInput input) : m_pInput(std::move(input)) {

    }

    void TilesetTask::Process() {
        m_pProgress = 0.0f;

        try {
            MV::TilesetData data{};
            data.m_Atlas = m_pInput.m_Atlas;
            data.m_Tiles.resize(m_pInput.m_IDToAtlasID.size());

            for(uint32_t i = 0; i < m_pInput.m_IDToAtlasID.size(); i++) {
                data.m_Tiles[i].m_SpriteIndex = m_pInput.m_IDToAtlasID[i];
            }

            m_pProgress = 0.2f;

            m_pFinalPath = TilesetProducer::CreateTilesetResource(data, m_pInput.m_Path);
            m_pFinalPath = Producer::ProducerUtils::PathAbsoluteToObjectRelative(m_pFinalPath);

            m_pProgress = 1.0f;
            m_pSuccess = true;
        } catch(std::exception &e) {
            (void)e;

            m_pProgress = 0.0f;
            m_pSuccess = false;
        }
    }

    EditorTaskFinishResult TilesetTask::Finish() {
        if (!m_pSuccess) {
            m_pDone = true;
            return EditorTaskFinishResult::FAILED;
        }

        auto &proj = MV::GetEngine()->GetProject();

        auto assetId = Producer::ProducerUtils::GenerateUUID();

        auto& asset = proj.m_Assets[assetId];
        asset.m_Path = Producer::ProducerUtils::PathAbsoluteToObjectRelative(m_pFinalPath.data());
        asset.m_Type = MV::AssetTypeRegistry::Get().GetRuntimeAssetType<MV::TilesetAssetType>();

        ProjectProducer::SaveProject(proj, GetEditor()->GetLoadedProjectPath());

        m_pDone = true;
        return EditorTaskFinishResult::SUCCESS;
    }
}