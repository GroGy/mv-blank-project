//
// Created by Matty on 02.03.2023.
//
#include "interface/worldobject_renderers/CameraPropertyRenderer.h"
#include "2d/CameraProperty.h"
#include "render/vulkan/VulkanTextureImpl.h"
#include "common/EngineCommon.h"
#include "imgui/imgui_impl_vulkan.h"
#include "interface/object_editor/NumericFieldRenderers.h"
#include "interface/object_editor/GeneralFieldRenderers.h"
#include "util/Math.h"


namespace Editor
{
    bool CameraPropertyRenderer::Draw(const MV::Meta::Field* field, const MV::rc<MV::WorldObject>& wo,
                                      MV::Property* property)
    {
        auto data = dynamic_cast<MV::CameraProperty*>(property);

        ENSURE_VALID(data, false);
        bool dirty = false;
/*
        auto* drawList = ImGui::GetWindowDrawList();

        const auto cursor = ImGui::GetCurrentWindow()->DC.CursorPos;

        auto width = ImGui::GetContentRegionAvail().x;

        width = ImMin(256.0f, width);

        ImRect bb{cursor, cursor + ImVec2{width, width}};

        ImGui::Button("Lock to viewport");


        if (!(data->GetRealCamera() && data->GetRealCamera()->GetCameraData().m_RenderPass.Valid()))
        {
            drawList->AddRectFilled(bb.Min, bb.Max, IM_COL32(255, 255, 255, 255));
        } else
        {
            drawList->AddRectFilled(bb.Min, bb.Max, IM_COL32(0, 255, 255, 255));
        }

        ImGui::ItemSize(bb.GetSize());
        ImGui::ItemAdd(bb, ImGui::GetID("##camera_preview"));
*/
        if (!Begin("Settings"))
        {
            return false;
        }

        if (BeginField("Enabled:"))
        {
            dirty |= FieldDrawingImpl::DrawBool("##camera_enabled", data->m_Enabled);
            EndField();
        }

        if (BeginField("Zoom:"))
        {
            float zoom = data->m_Zoom;
            float initialValue = zoom;

            MV::optional<float> min;
            min.set(0.1f);

            dirty |= FieldDrawingImpl::DrawFloat("##camera_zoom", zoom, min);

            if (!MV::Math::IsNearlyEqual(zoom,initialValue))
            {
                data->SetZoom(zoom);
            }

            EndField();
        }

        if (BeginField("View Width:"))
        {
            float vw = data->m_ViewWidth;
            float initialValue = vw;

            MV::optional<float> min;
            min.set(0.1f);

            dirty |= FieldDrawingImpl::DrawFloat("##camera_view_width", vw, min);

            if (!MV::Math::IsNearlyEqual(vw,initialValue))
            {
                data->SetViewWidth(vw);
            }

            EndField();
        }

        End();

        return dirty;
    }
}