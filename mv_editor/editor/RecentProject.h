//
// Created by Matty on 2022-02-10.
//

#ifndef MV_EDITOR_RECENTPROJECT_H
#define MV_EDITOR_RECENTPROJECT_H

#include <EngineMinimal.h>

namespace Editor {
    static const MV::string_view RECENT_PROJECT_JSON_PATH = "path";
    static const MV::string_view RECENT_PROJECT_JSON_NAME = "name";
    static const MV::string_view RECENT_PROJECT_JSON_LAST_OPENED = "last_opened";

    struct RecentProject {
        MV::string m_Path;
        MV::string m_Name;
        time_t m_LastOpened;
        bool m_Valid;

        void ToJson(nlohmann::json & json) {
            json[RECENT_PROJECT_JSON_PATH.data()] = m_Path;
            json[RECENT_PROJECT_JSON_NAME.data()] = m_Name;
            json[RECENT_PROJECT_JSON_LAST_OPENED.data()] = m_LastOpened;
        };

        void FromJson(nlohmann::json & json) {
            m_Path = json[RECENT_PROJECT_JSON_PATH.data()].get<MV::string>();
            m_Name = json[RECENT_PROJECT_JSON_NAME.data()].get<MV::string>();
            m_LastOpened = json[RECENT_PROJECT_JSON_LAST_OPENED.data()].get<time_t>();
        };
    };
}

#endif //MV_EDITOR_RECENTPROJECT_H
