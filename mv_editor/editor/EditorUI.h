//
// Created by Matty on 2021-10-23.
//

#ifndef MV_EDITOR_EDITORUI_H
#define MV_EDITOR_EDITORUI_H

#include "interface/common/Window.h"
#include "imgui/imgui.h"
#include "imgui/imfilebrowser.h"
#include "imgui/imgui_internal.h"
#include "interface/Viewport.h"
#include "interface/common/FileBrowser.h"
#include "precompiler/InterfaceWindowRegistrator.h"
#include "common/DragAndDropContext.h"
#include "interface/common/GeneralPopup.h"
#include <string>
#include <render/common/Texture.h>

namespace Editor {
    class ProjectSettings;

    namespace Config {
        static constexpr MV::string_view sc_DropScriptableID = "drag_and_drop_scriptable";
        static constexpr MV::string_view sc_DropScriptableConfigID = "drag_and_drop_script_config";
    }

    using GlobalTextInputCallback = eastl::function<void(bool)>;

    class EditorUI {

    public: // Public fields
        bool m_StartDialog = true;

        bool m_pJustOpenedProject = false;

    public: // Public functions

        void OnRender();

        bool OnCleanup();

        bool OnRendererCleanup();

        void OnResize();

        void OnInit();

        void DumpImGuiAllocations();

        bool TextInput(MV::string_view name, MV::string *data, const GlobalTextInputCallback & onFinish = [](bool){});

        void OnWorldRender();

        void OnUpdate(float deltaTime);

        void ProcessOsDragAndDrop(const MV::vector<MV::string>& data);

    public: // Popup

        bool GlobalPopupOk(MV::string_view title, PopupCloseCallback&& callback, MV::string_view body = "");

        bool GlobalPopupConfirmOrCancel(MV::string_view title, PopupCloseCallback&& callback, MV::string_view body = "");

        bool GlobalPopupYesNo(MV::string_view title, PopupCloseCallback&& callback, MV::string_view body = "");

        bool GlobalPopupCustomModal(MV::string_view title, CustomModalDrawCallback&& callback);

    public: // Window getting functions

        static MV::rc<Window> GetInterfaceWindow(uint32_t windowID) ;

        template <typename WindowType>
        inline MV::rc<WindowType> GetWindow() const;

    public:

        DragAndDropContext m_DragAndDropContext;

    private:

        void drawDragAndDrop();

    private:

        Viewport m_pViewport{};

        std::vector<MV::rc<Editor::Window>> m_pWindows{};

        bool m_pFontUploadCommandDispatched = false;
        volatile bool m_pFontUploaded = false;
        MV::string m_pProjectName{};
        MV::string m_pProjectPath{};
        MV::rc<Editor::FileBrowser> m_pCreateProjectFileDialog{};
        MV::rc<Editor::FileBrowser> m_pOpenProjectFileDialog{};

        int32_t m_pHoveredProjectIndex = -1;

        void initImgui();

        void cleanupImgui();

        void uploadFonts();

        void initWindows();

        void renderStartDialog();

        inline void setupStyle(bool isDarkTheme, float alpha);

        void onProjectLoad();

        void processRecentProjects();

        void drawMenuBar();

        ImDrawData duplicateDrawData(ImDrawData * data);

    private: // Text Input fields

        bool m_pTextInputOpen = false;

        bool m_pTextInputOpenPopup = false;

        MV::string *m_pTextInputData = nullptr;

        MV::string m_pTextInputName = "null";

        GlobalTextInputCallback m_pTextInputCloseCallback;

    private: // Global popup

        GeneralPopup m_pGlobalPopup;

    private: // Text input functions

        void drawTextInput();

    };

    template<typename WindowType>
    MV::rc<WindowType> EditorUI::GetWindow() const
    {
        static_assert(std::is_base_of_v<Window, WindowType>, "WindowType must be child of Window");

        return MV::dynamic_pointer_cast<WindowType>(GetInterfaceWindow(WindowID<WindowType>::ID()));
    }
}


#endif //MV_EDITOR_EDITORUI_H
