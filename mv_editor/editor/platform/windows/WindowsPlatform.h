//
// Created by Matty on 2022-02-10.
//

#ifndef MV_EDITOR_WINDOWSPLATFORM_H
#define MV_EDITOR_WINDOWSPLATFORM_H

#ifdef WIN32
#include <filesystem>
#include <windows.h>
#include <EngineMinimal.h>

namespace Editor {
    class WindowsPlatform {
    public:
        static MV::string GetWindowsError(HRESULT result);

        static void OpenInSystemDefault(MV::string_view path);
        static void RunCommandUnwaited(MV::string_view command, MV::string_view args, MV::string_view directory);
        static int32_t RunCommandAwait(MV::string_view command, MV::string_view args, MV::string_view directory, const std::function<void(bool isError,MV::string_view)> & messageClb);

        static void OpenFolderInExplorer(MV::string_view path);
    };
}
#endif


#endif //MV_EDITOR_WINDOWSPLATFORM_H
