//
// Created by Matty on 2022-09-03.
//

#ifndef MV_EDITOR_WINDOWSFILEBROWSER_H
#define MV_EDITOR_WINDOWSFILEBROWSER_H

#if WIN32

#include <windows.h>
#include <shobjidl.h>
#include "interface/common/FileBrowser.h"

namespace Editor {
    class WindowsFileBrowser : public FileBrowser {
    public:
        friend class FileBrowser;

    private:
        explicit WindowsFileBrowser(FileBrowserMode mode);

    public:

        void Open() override;
        void Open(std::filesystem::path dir) override;
        void Init() override;
        void Cleanup() override;
    private:
        IFileDialog * m_pWinFileDialog = nullptr;
        bool m_pComInitialized = false;
    };
}

#endif
#endif //MV_EDITOR_WINDOWSFILEBROWSER_H
