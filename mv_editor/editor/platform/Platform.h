//
// Created by Matty on 2022-02-10.
//

#ifndef MV_EDITOR_PLATFORM_H
#define MV_EDITOR_PLATFORM_H

#include <filesystem>
#include <EngineMinimal.h>

namespace Editor {
    class Platform {
    public:
        static void OpenInSystemDefault(MV::string_view path);

        static void RunCommandUnwaited(MV::string_view command, MV::string_view args, MV::string_view directory);
        static bool RunCommandAwait(MV::string_view command, MV::string_view args, MV::string_view directory = MV::string(std::filesystem::current_path().string().c_str()), const std::function<void(bool isError,MV::string_view)> & messageClb = [](bool, MV::string_view){});

        static void OpenFolderInExplorer(MV::string_view path);
    };
}


#endif //MV_EDITOR_PLATFORM_H
