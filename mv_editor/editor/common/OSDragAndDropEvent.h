//
// Created by Matty on 02.07.2024.
//

#ifndef MV_EDITOR_OSDRAGANDDROPEVENT_H
#define MV_EDITOR_OSDRAGANDDROPEVENT_H

#include <common/Types.h>
#include <EngineMinimal.h>

namespace Editor
{
    class OSDragAndDropEvent
    {
        friend class EditorUI;
    public:

        FORCEINLINE bool IsConsumed() const { return m_pConsumed; }

        void Consume() { ENSURE_TRUE(!m_pConsumed); m_pConsumed = true; };

        FORCEINLINE const MV::vector<MV::string>& GetData() const { return m_pData; }

    private:

        bool m_pConsumed = false;

        MV::vector<MV::string> m_pData;

    };
}


#endif //MV_EDITOR_OSDRAGANDDROPEVENT_H
