//
// Created by Matty on 01.09.2023.
//

#ifndef MV_EDITOR_DRAGANDDROPCONTEXT_H
#define MV_EDITOR_DRAGANDDROPCONTEXT_H

#include <EngineMinimal.h>

namespace Editor {
    class DragAndDropContext {
    public:
        bool m_Active = false;

        bool m_JustOpenned = true;

        // Indicates that current drag and drop is not possible, changes rendering of tooltip
        bool m_NonPossible = false;

        MV::string m_Name;

        MV::string m_ID;

        void* m_Payload;
        size_t m_PayloadSize = 0;
    };
}

#endif //MV_EDITOR_DRAGANDDROPCONTEXT_H
