//
// Created by Matty on 10.04.2023.
//

#ifndef MV_EDITOR_PROPERTYGIZMOREGISTRATOR_H
#define MV_EDITOR_PROPERTYGIZMOREGISTRATOR_H

#include <EngineMinimal.h>
#include "log/Logger.h"
#include "render/common/FrameContext.h"
#include "common/Property.h"
#include "meta/Helpers.h"

namespace Editor {
    class RegisteredPropertyGizmos {
    public: // Types

        using PropertyGizmoDrawCallback = eastl::function<void(const glm::vec4 &,MV::Property *)>;

        using GetClassHashCallback = eastl::function<uint64_t()>;

        struct PropertyGizmoData {
            PropertyGizmoDrawCallback m_DrawCallback;
        };


    public: // Singleton

        static RegisteredPropertyGizmos& GetInstance();

    public: // Public API

        MV::unordered_map<uint64_t, PropertyGizmoData> m_GizmoLookup;

    private: // Registration API

        friend class IPropertyGizmoRegistrator;

        void RegisterProperty(PropertyGizmoDrawCallback&& gizmoDrawingCallback,GetClassHashCallback && getHashCallback);

    private: // Token initialization functions, used for loading tokens from scripting backend after its ready

        friend class Editor;

        void Initialize();

    private: // Token initialization fields

        bool m_pInitialized = false;


    private: // Internal fields

        MV::vector<PropertyGizmoData> m_pUnregisteredRenderers;

        MV::vector<GetClassHashCallback> m_pRegistrationCallbacks;

    };

    class IPropertyGizmoRegistrator {
        using GetClassHashCallback = RegisteredPropertyGizmos::GetClassHashCallback;
    protected:
        IPropertyGizmoRegistrator() = default;

        static void RegisterPropertyGizmo(RegisteredPropertyGizmos::PropertyGizmoDrawCallback && callback,GetClassHashCallback && getHashCallback);
    };

    template<typename T>
    class PropertyGizmoRegistrator : public IPropertyGizmoRegistrator {
    public:
        explicit PropertyGizmoRegistrator() {
            RegisterPropertyGizmo([](const glm::vec4 & viewport,MV::Property * property){
                T::RenderProperty(viewport, property);
            }, []() -> uint64_t {
                return MV::GetClassHash<T::PropertyType>();
            });
        };
    };
}


#endif //MV_EDITOR_PROPERTYGIZMOREGISTRATOR_H
