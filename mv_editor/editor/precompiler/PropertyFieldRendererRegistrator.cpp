//
// Created by Matty on 22.01.2023.
//

#include "PropertyFieldRendererRegistrator.h"
#include "common/EngineCommon.h"
#include "Engine.h"
#include "meta/MetaCache.h"

namespace Editor {
    void IPropertyRegistrator::RegisterProperty(RegisteredPropertyRenderers::DrawingCallback&& callback, GetClassHashCallback && hashCallback) {
        RegisteredPropertyRenderers::GetInstance().RegisterProperty(MV::move(callback), MV::move(hashCallback));
    }

    RegisteredPropertyRenderers& RegisteredPropertyRenderers::GetInstance() {
        static RegisteredPropertyRenderers instance;
        return instance;
    }

    void RegisteredPropertyRenderers::RegisterProperty(RegisteredPropertyRenderers::DrawingCallback&& drawingCallback, GetClassHashCallback&& hashCallback) {
        ENSURE_TRUE(!m_pTokensInitialized); // Dont allow registration after initialized

        RendererData d;

        d.m_Draw = MV::move(drawingCallback);
        d.m_InitGetHashCallback = MV::move(hashCallback);

        m_pUnregisteredRenderers.push_back(MV::move(d));
    }

    void RegisteredPropertyRenderers::InitializeRenderers() {
        for(auto && v : m_pUnregisteredRenderers) {
            const auto& metaCache = MV::Meta::MetaCache::GetInstance();

            CHECK(v.m_InitGetHashCallback.operator bool());

            const auto hash = v.m_InitGetHashCallback();

            auto meta = metaCache.GetClassMetaFromHash(hash);

            if(!meta.IsValid()) {
                v.m_InitGetHashCallback = {};
                MV::GetLogger()->LogWarning("Registered property renderer for property without valid meta info. (Hash: {})", hash);
            }

            m_RenderersLookup[hash] = v;

            v.m_InitGetHashCallback = {};
        }

        m_pTokensInitialized = true;

    }
}
