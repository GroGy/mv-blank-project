//
// Created by Matty on 12.02.2023.
//

#include "InterfaceWindowRegistrator.h"

namespace Editor {
    static WindowIDType s_GlobalWindowIDCounter = 0;


    WindowIDType WindowIDBase::getNextID()
    {
        return ++s_GlobalWindowIDCounter;
    }

    RegisteredWindows& RegisteredWindows::GetInstance()
    {
        static RegisteredWindows instance;
        return instance;
    }

    void RegisteredWindows::RegisterWindow(WindowIDType id, MV::rc<Window>&& windowObject)
    {
        m_WindowLookup[id] = {MV::move(windowObject)};
    }

    void IWindowRegistrator::RegisterWindow(WindowIDType id, MV::rc<Window>&& windowObject)
    {
        RegisteredWindows::GetInstance().RegisterWindow(id, MV::move(windowObject));
    }
}