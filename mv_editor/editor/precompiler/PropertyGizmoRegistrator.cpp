//
// Created by Matty on 10.04.2023.
//

#include "PropertyGizmoRegistrator.h"
#include "common/EngineCommon.h"
#include "Engine.h"

namespace Editor {

    void IPropertyGizmoRegistrator::RegisterPropertyGizmo(RegisteredPropertyGizmos::PropertyGizmoDrawCallback&& callback,GetClassHashCallback && getHashCallback)
    {
        auto & inst = RegisteredPropertyGizmos::GetInstance();
        inst.RegisterProperty(MV::move(callback), MV::move(getHashCallback));
    }

    RegisteredPropertyGizmos& RegisteredPropertyGizmos::GetInstance()
    {
        static RegisteredPropertyGizmos instance;
        return instance;
    }

    void RegisteredPropertyGizmos::RegisterProperty(RegisteredPropertyGizmos::PropertyGizmoDrawCallback&& gizmoDrawingCallback
                                                    ,GetClassHashCallback && getHashCallback)
    {
        auto data = RegisteredPropertyGizmos::PropertyGizmoData{};
        data.m_DrawCallback = MV::move(gizmoDrawingCallback);

        m_pUnregisteredRenderers.push_back(MV::move(data));
        m_pRegistrationCallbacks.push_back(MV::move(getHashCallback));
    }

    void RegisteredPropertyGizmos::Initialize()
    {
        if(m_pInitialized) {
            ENSURE_NO_ENTRY();
            return;
        }

        for(int32_t i = 0; i < m_pUnregisteredRenderers.size(); i++) {
            const auto& gh = m_pRegistrationCallbacks[i];
            const auto& v = m_pUnregisteredRenderers[i];

            const auto hash = gh();

            m_GizmoLookup[hash] = MV::move(v);
        }

        m_pRegistrationCallbacks = {};
        m_pInitialized = true;
    }
}