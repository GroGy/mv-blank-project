//
// Created by Matty on 12.02.2023.
//

#ifndef MV_EDITOR_INTERFACEWINDOWREGISTRATOR_H
#define MV_EDITOR_INTERFACEWINDOWREGISTRATOR_H

#include <EngineMinimal.h>
#include "allocator/Alloc.h"
#include "interface/common/Window.h"
#include "log/Logger.h"

namespace Editor
{
    using WindowIDType = uint32_t;

    class WindowIDBase
    {
    protected:
        static WindowIDType getNextID();
    };

    template <typename T>
    class WindowID : public WindowIDBase
    {
    public:
        static WindowIDType ID();
        static bool     Registered();
    private:
        static WindowIDType s_ID;
        friend class RegisteredWindows;
    };

    template <typename T>
    WindowIDType WindowID<T>::s_ID = (std::numeric_limits<WindowIDType>::max)();

    template<typename T>
    inline WindowIDType WindowID<T>::ID()
    {
        if(!Registered()) {
            s_ID = getNextID();
        }

        MV::Assert(s_ID != (std::numeric_limits<WindowIDType>::max)(), "Component type was not registered");
        return s_ID;
    }

    template<typename T>
    inline bool WindowID<T>::Registered()
    {
        return s_ID != (std::numeric_limits<WindowIDType>::max)();
    }

    class RegisteredWindows {
    public: // Types

        struct WindowData {
            MV::rc<Window> m_Window;
        };

    public: // Singleton

        static RegisteredWindows& GetInstance();

    public: // Public API

        MV::unordered_map<uint32_t, WindowData> m_WindowLookup;

    private: // Registration API

        friend class IWindowRegistrator;

        void RegisterWindow(WindowIDType id, MV::rc<Window>&& windowObject);

    private: // Token initialization functions, used for loading tokens from scripting backend after its ready

        friend class EditorUI;

    };

    class IWindowRegistrator {
    protected:
        IWindowRegistrator() = default;

        static void RegisterWindow(WindowIDType id, MV::rc<Window>&& windowObject);
    };

    template<typename T>
    class WindowRegistrator : public IWindowRegistrator {
    public:
        WindowRegistrator() {
            MV::AllocatorTagScope editorAllocTag{MV::AllocationType::Editor};
            RegisterWindow(WindowID<T>::ID(),MV::make_rc<T>());
        };
    };
}


#endif //MV_EDITOR_INTERFACEWINDOWREGISTRATOR_H
