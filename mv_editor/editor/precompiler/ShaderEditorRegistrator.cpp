//
// Created by Matty on 02.05.2023.
//

#include "ShaderEditorRegistrator.h"
#include "allocator/Alloc.h"

void Editor::IShaderNodeRegistrator::RegisterShaderNode(MV::string_view nodeName, bool showInPopup,
                                                  Editor::RegisteredShaderNodes::CreateNodeCallback&& callback)
{
    auto & inst = Editor::RegisteredShaderNodes::GetInstance();
    Editor::RegisteredShaderNodes::NodeData d;

    d.m_CreateCallback = MV::move(callback);
    d.m_NodeName = nodeName;
    d.m_ShowInCreatePopup = showInPopup;

    inst.m_NodeDataLookup.push_back(MV::move(d));
}

Editor::RegisteredShaderNodes& Editor::RegisteredShaderNodes::GetInstance()
{
    MV::AllocatorTagScope tag{MV::AllocationType::Editor};
    static Editor::RegisteredShaderNodes instance;
    return instance;
}
