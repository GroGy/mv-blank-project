//
// Created by Matty on 02.05.2023.
//

#ifndef MV_EDITOR_SHADEREDITORREGISTRATOR_H
#define MV_EDITOR_SHADEREDITORREGISTRATOR_H

#include <EngineMinimal.h>
#include "allocator/Alloc.h"
#include "tools/shader_editor/common/ShaderNode.h"

namespace Editor {
    class RegisteredShaderNodes {
    public: // Types

        using CreateNodeCallback = eastl::function<MV::rc<ShaderNode>(uint32_t&)>;

        struct NodeData {
            MV::string m_NodeName;
            bool m_ShowInCreatePopup;
            CreateNodeCallback m_CreateCallback;
        };

    public: // Singleton

        static RegisteredShaderNodes& GetInstance();

    public: // Public API

        MV::vector<NodeData> m_NodeDataLookup;

    };

    class IShaderNodeRegistrator {
    protected:
        IShaderNodeRegistrator() = default;

        void RegisterShaderNode(MV::string_view nodeName, bool showInPopup, RegisteredShaderNodes::CreateNodeCallback&& callback);
    };

    template<typename T,bool ShowInPopup = true>
    class ShaderNodeRegistrator : public IShaderNodeRegistrator {
    public:
        explicit ShaderNodeRegistrator(MV::string_view nodeName) {
            RegisterShaderNode(nodeName,ShowInPopup,
                             [](uint32_t & idCounter) {
                                    MV::AllocatorTagScope tag{MV::AllocationType::Editor};
                                    return MV::make_rc<T>(idCounter);
                             });
        };
    };
}


#endif //MV_EDITOR_SHADEREDITORREGISTRATOR_H
