//
// Created by Matty on 22.01.2023.
//

#ifndef MV_EDITOR_PROPERTYFIELDRENDERERREGISTRATOR_H
#define MV_EDITOR_PROPERTYFIELDRENDERERREGISTRATOR_H

#include <EngineMinimal.h>
#include "common/WorldObject.h"
#include "common/Property.h"
#include "meta/Class.h"
#include "meta/Helpers.h"

namespace Editor {
    class RegisteredPropertyRenderers {
    public: // Types

        using DrawingCallback = eastl::function<bool(const MV::Meta::Field*, const MV::rc<MV::WorldObject>&,
                                                     MV::Property*)>;

        using GetClassHashCallback = eastl::function<uint64_t()>;

        struct RendererData {
            DrawingCallback m_Draw;
            GetClassHashCallback m_InitGetHashCallback;
        };

    public: // Singleton

        static RegisteredPropertyRenderers& GetInstance();

    public: // Public API

        MV::unordered_map<uint64_t, RendererData> m_RenderersLookup;

    private: // Registration API

        friend class IPropertyRegistrator;

        void RegisterProperty(DrawingCallback&& drawingCallback, GetClassHashCallback && hashCallback);

    private: // Token initialization functions, used for loading tokens from scripting backend after its ready

        friend class Editor;

        void InitializeRenderers();

    private: // Token initialization fields

        bool m_pTokensInitialized = false;


    private: // Internal fields

        MV::vector<RendererData> m_pUnregisteredRenderers;

    };

    class IPropertyRegistrator {
    public:

        using GetClassHashCallback = RegisteredPropertyRenderers::GetClassHashCallback;

    protected:
        IPropertyRegistrator() = default;

        void RegisterProperty(RegisteredPropertyRenderers::DrawingCallback&& callback, GetClassHashCallback && hashCallback);
    };

    template<typename T>
    class PropertyFieldRendererRegistrator : public IPropertyRegistrator {
    public:
        PropertyFieldRendererRegistrator() {
            RegisterProperty([](const MV::Meta::Field* _f, const MV::rc<MV::WorldObject>& _wo, MV::Property* _p) {
                                 return T::Draw(_f,_wo,_p);
                             },[]() { return MV::GetClassHash<T::PropertyType>(); });
        };
    };
}


#endif //MV_EDITOR_PROPERTYFIELDRENDERERREGISTRATOR_H
