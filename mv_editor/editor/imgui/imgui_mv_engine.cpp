//
// Created by Matty on 07.07.2024.
//

#include "imgui_mv_engine.h"
#include "imgui.h"
#include "imgui_internal.h"


namespace ImGui {
    ImU32 ImGui::ColorConvertFloat4ToU32GLM(const glm::vec4& in)
    {
        ImU32 out;
        out  = ((ImU32)IM_F32_TO_INT8_SAT(in.x)) << IM_COL32_R_SHIFT;
        out |= ((ImU32)IM_F32_TO_INT8_SAT(in.y)) << IM_COL32_G_SHIFT;
        out |= ((ImU32)IM_F32_TO_INT8_SAT(in.z)) << IM_COL32_B_SHIFT;
        out |= ((ImU32)IM_F32_TO_INT8_SAT(in.w)) << IM_COL32_A_SHIFT;
        return out;
    }
}