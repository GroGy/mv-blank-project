//
// Created by Matty on 2022-10-02.
//

#include "imgui_neo_curve.h"

#ifndef IMGUI_DEFINE_MATH_OPERATORS
#define IMGUI_DEFINE_MATH_OPERATORS
#endif

#include "imgui_internal.h"

namespace ImGui {
    // Curve context

    struct ImGuiNeoCurveContext {
        // Base info
        ImGuiID m_ID = 0;

        // Selection fields
        ImVector<uint32_t> m_Selection; // Future support for multiselect
        bool m_SelectionOnlyHovered = false; // Selection is only hovered, not selected
        bool m_Selecting = false;               // Is now selecting?

        // Dragging fields
        bool m_Dragging = false;
        ImVector<ImVec2> m_DraggingStartPositions; // Future support for multiselect
        ImVec2 m_DraggingMouseStartPosition;

        // Moving canvas
        bool m_HoldingCanvas = false;
        ImVec2 m_HoldingCanvasMouseStartPosition;
        ImVec2 m_HoldingCanvasViewStartPosition;

        // Callback requests
        bool m_RequestSort = false;
        bool m_RequestAdd = false;

        ImVec2 m_PointToAdd;

        ImRect m_View = {0.0f, 0.0f, 1.0f, 1.0f};
    };

    // Global state

    static bool drawingCurve = false;
    static ImGuiNeoCurveStyle curveStyle;
    static ImVector<ImGuiNeoCurveContext> contexts;
    static ImGuiID activeContextId = 0;
    static ImVector<ImGuiColorMod> colorStack;

    // Helpers

    static void checkMinMax(ImVec2 *v, ImVec2 *min, ImVec2 *max) {
        if (v->x < min->x) {
            min->x = v->x;
        }

        if (v->y < min->y) {
            min->y = v->y;
        }

        if (v->x > max->x) {
            max->x = v->x;
        }

        if (v->y > max->y) {
            max->y = v->y;
        }
    }

    static ImVec2 transformToCanvas(const ImVec2 &point, const ImVec2 &min, const ImVec2 &max, const ImVec2 &canvasMin,
                                    const ImVec2 &canvasMax) {
        float xOnCanvas = (point.x - min.x) / (max.x - min.x);
        float yOnCanvas = (point.y - min.y) / (max.y - min.y);

        float canvasHeight = (canvasMax.y - canvasMin.y);

        // TODO(transform point according to zoom)

        float xRes = canvasMin.x + (canvasMax.x - canvasMin.x) * xOnCanvas;
        float yRes = canvasMin.y + canvasHeight - (canvasHeight * yOnCanvas);

        return {xRes, yRes};
    }

    static ImRect getPointBB(const ImVec2 *point, float radius) {
        float halfR = radius;
        ImVec2 min = {point->x - halfR, point->y - halfR};
        ImVec2 max = {point->x + halfR, point->y + halfR};

        return {min, max};
    }

    static ImGuiNeoCurveContext &getContext(ImGuiID id) {
        for (ImGuiNeoCurveContext &ctx: contexts) {
            if (ctx.m_ID == id) {
                return ctx;
            }
        }

        ImGuiNeoCurveContext newCtx;
        newCtx.m_ID = id;

        contexts.push_back(newCtx);

        return contexts.back();
    }

    // Index is accual index, subindex is point index in this iteration (p1 = 0, p2 = 1, tangent1 = 2, tangent2 = 3)
    static void processPointSelection(const ImVec2 *point, uint32_t pointIndex, uint32_t pointSubIndex,
                                      ImGuiNeoCurveContext &context, bool isTangentPoint) {
        static char internalNameBuffer[64];

        if (context.m_Dragging) return;

        snprintf(internalNameBuffer, 64, "%d_point_%d", pointIndex, pointSubIndex);

        const uint32_t pointI = pointIndex * 4 + pointSubIndex;
        if (context.m_Selecting) {
            //TODO: Check if point is in selection rect, if it is, add it to m_Selection
            IM_ASSERT(!context.m_Selection.contains(pointI));
            context.m_Selection.push_back(pointI);
        } else {
            if (ItemHoverable(getPointBB(point, isTangentPoint ? curveStyle.CurveTangentPointRadius
                                                               : curveStyle.CurvePointRadius),
                              GetID(internalNameBuffer))) {
                IM_ASSERT(!context.m_Selection.contains(pointI));
                context.m_Selection.push_back(pointI);
            }
        }
    }

    static ImVec4
    getPointColor(uint32_t pointIndex, uint32_t pointSubIndex, ImGuiNeoCurveContext &context, bool isTangentPoint) {
        if (context.m_Selection.contains(pointIndex * 4 + pointSubIndex)) {
            //Point is selected or hovered
            if (context.m_Dragging) {
                return isTangentPoint ? curveStyle.Colors[ImGuiNeoCurveCol_CurveTangentPointSelected]
                                      : curveStyle.Colors[ImGuiNeoCurveCol_CurvePointSelected];
            }

            return isTangentPoint ? curveStyle.Colors[ImGuiNeoCurveCol_CurveTangentPointHovered]
                                  : curveStyle.Colors[ImGuiNeoCurveCol_CurvePointHovered];
        }
        return isTangentPoint ? curveStyle.Colors[ImGuiNeoCurveCol_CurveTangentPoint]
                              : curveStyle.Colors[ImGuiNeoCurveCol_CurvePoint];
    }

    static void clearContextSelection(ImGuiNeoCurveContext &context) {
        if (context.m_Dragging) {
            context.m_SelectionOnlyHovered = false;
            return;
        }
        context.m_Selection.clear();
        context.m_SelectionOnlyHovered = true;
    }

    static ImVec2 getPointFromIndex(uint32_t index, PrevTangetGetter prevTangentGetter,
                                    PositionGetter positionGetter, NextTangentGetter nextTangentGetter,
                                    void *userData) {
        uint32_t point = index / 4;
        uint32_t subIndex = index % 4;

        switch (subIndex) {
            case 0:
                return *positionGetter(point, userData);
            case 1:
                return *positionGetter(point + 1, userData);
            case 2:
                return *nextTangentGetter(point, userData);
            case 3:
                return *prevTangentGetter(point + 1, userData);
            default:
                break;
        }

        IM_ASSERT(false); // Should never happen

        return {};
    }

    static void setPointPositionFromIndex(uint32_t index, const ImVec2 &newPosition, PrevTangetGetter prevTangentGetter,
                                          PositionGetter positionGetter, NextTangentGetter nextTangentGetter,
                                          void *userData) {
        uint32_t point = index / 4;
        uint32_t subIndex = index % 4;

        switch (subIndex) {
            case 0:
                *positionGetter(point, userData) = newPosition;
                break;
            case 1:
                *positionGetter(point + 1, userData) = newPosition;
                break;
            case 2:
                *nextTangentGetter(point, userData) = newPosition;
                break;
            case 3:
                *prevTangentGetter(point + 1, userData) = newPosition;
                break;
            default:
                break;
        }
    }

    static void applyDeltaToPointFromIndex(uint32_t index, const ImVec2 &delta, PrevTangetGetter prevTangentGetter,
                                           PositionGetter positionGetter, NextTangentGetter nextTangentGetter,
                                           void *userData) {
        uint32_t point = index / 4;
        uint32_t subIndex = index % 4;

        switch (subIndex) {
            case 0:
                *positionGetter(point, userData) += delta;
                break;
            case 1:
                *positionGetter(point + 1, userData) += delta;
                break;
            case 2:
                *nextTangentGetter(point, userData) += delta;
                break;
            case 3:
                *prevTangentGetter(point + 1, userData) += delta;
                break;
            default:
                break;
        }
    }

    static void updateSelectionPosition(ImGuiNeoCurveContext &context, PrevTangetGetter prevTangentGetter,
                                        PositionGetter positionGetter, NextTangentGetter nextTangentGetter,
                                        void *userData, const ImVec2 *min, const ImVec2 *max,
                                        ImVec2 *const canvasSize) {
        const ImVec2 currentMousePos = GetMousePos();
        const ImVec2 startMousePos = context.m_DraggingMouseStartPosition;

        const ImVec2 mouseDelta = currentMousePos - startMousePos;

        const ImVec2 curveRatio = (*max - *min);
        const ImVec2 normalizedDelta = mouseDelta / *canvasSize;

        ImVec2 selectionDelta = normalizedDelta * curveRatio;

        selectionDelta.y = -selectionDelta.y;

        //TODO: Apply zoom?

        for (int32_t i = 0; i < context.m_Selection.size(); i++) {
            const ImVec2 startPos = context.m_DraggingStartPositions[i];
            setPointPositionFromIndex(context.m_Selection[i], startPos + selectionDelta, prevTangentGetter,
                                      positionGetter, nextTangentGetter, userData);
        }
    }

    static void checkForSort(ImGuiNeoCurveContext &context, uint32_t pointCount, PrevTangetGetter prevTangentGetter,
                             PositionGetter positionGetter, NextTangentGetter nextTangentGetter, void *userData) {
        bool request = false;

        for (uint32_t i = 1; i < pointCount; i++) {
            //TODO implement checking of right order
        }

        context.m_RequestSort = request;
    }

    static bool isZeroAxis(float val) {
        return val > -FLT_EPSILON && val < FLT_EPSILON;
    }

    static void
    drawGridLines(ImDrawList *drawList, const ImVec2 &min, const ImVec2 &max, const ImVec2 &minBB, const ImVec2 &maxBB,
                  const ImVec2 &cursor) {
        const ImVec2 viewportSize = max - min;

        {
            uint32_t gridLineCount = ceil(viewportSize.x) + 1;
            const float start = min.x < 0.0f ? ceil(min.x) : floor(min.x);
            for (uint32_t i = 0; i < gridLineCount; i++) {
                float normalizedX = (start + i);

                float const canvasPosition = transformToCanvas({normalizedX, 0.0f}, min, max, minBB, maxBB).x;

                const bool isMajor = isZeroAxis(normalizedX);

                drawList->AddLine(
                        {canvasPosition, minBB.y},
                        {canvasPosition, maxBB.y},
                        isMajor ? IM_COL32(255, 255, 255, 240) : IM_COL32(255, 255, 255, 100),
                        isMajor ? 0.5f : 0.1f);
            }
        }

        {
            uint32_t gridLineCount = ceil(viewportSize.y) + 1;
            const float start = min.y < 0.0f ? ceil(min.y) : floor(min.y);
            for (uint32_t i = 0; i < gridLineCount; i++) {
                float normalizedY = (start + i);

                float const canvasPosition = transformToCanvas({0.0f, normalizedY}, min, max, minBB, maxBB).y;

                const bool isMajor = isZeroAxis(normalizedY);

                drawList->AddLine(
                        {minBB.x, canvasPosition},
                        {maxBB.x, canvasPosition},
                        isMajor ? IM_COL32(255, 255, 255, 240) : IM_COL32(255, 255, 255, 100),
                        isMajor ? 0.5f : 0.1f);
            }
        }
    }

    static void processCanvasControl(ImGuiNeoCurveContext &context, const ImVec2 &size, const ImVec2 & bbMin, const ImVec2 & bbMax) {
        ImRect bb{bbMin, bbMax};
        // Dont process mouse input when mouse is outside of bounding box
        if(!bb.Contains(GetMousePos())) {
            return;
        }

        // Manages zoom
        //SetKeyOwner(ImGuiKey_MouseWheelY, GetItemID());
        const float currentScroll = GetIO().MouseWheel;

        ImVec2 canvasCenter = context.m_View.GetCenter();

        ImVec2 prevSize = context.m_View.GetSize();
        const float zoom = (-currentScroll) * 0.3f;
        ImVec2 newSize = ImMax(prevSize + ImVec2{zoom, zoom}, ImVec2{1.0f, 1.0f});

        // Dont zoom while holding canvas
        if (!context.m_HoldingCanvas) {
            context.m_View = ImRect{
                    canvasCenter - newSize / 2.0f,
                    canvasCenter + newSize / 2.0f,
            };
        }

        // Canvas movement
        if (IsMouseDragging(ImGuiMouseButton_Middle, 0.0f)) {
            if (!context.m_HoldingCanvas) {
                context.m_HoldingCanvas = true;
                context.m_HoldingCanvasMouseStartPosition = GetMousePos();
                context.m_HoldingCanvasViewStartPosition = context.m_View.GetCenter();
            }
        } else {
            context.m_HoldingCanvas = false;
            context.m_HoldingCanvasMouseStartPosition = {0, 0};
        }

        if (context.m_HoldingCanvas) {
            const ImVec2 mouseDelta = GetMousePos() - context.m_HoldingCanvasMouseStartPosition;
            const ImVec2 centerDelta = ImVec2{-mouseDelta.x, mouseDelta.y} / size; //TODO: Apply scale from view size
            const ImVec2 newCenter = context.m_HoldingCanvasViewStartPosition + centerDelta;
            context.m_View = ImRect{
                    {newCenter - context.m_View.GetSize() / 2.0f},
                    {newCenter + context.m_View.GetSize() / 2.0f}
            };
        }
    }

    void drawInfiniteLines(ImDrawList *drawList, bool drawInfiniteMin, bool drawInfiniteMax, const ImVec2 &minPoint,
                           const ImVec2 &maxPoint, const ImVec2 &min,
                           const ImVec2 &max, const ImVec2 &minBB, const ImVec2 &maxBB) {
        if (drawInfiniteMin) {
            const ImVec2 point = transformToCanvas(minPoint, min, max, minBB, maxBB);

            if (minPoint.x < min.x || minPoint.y < min.y) {
                goto skip_draw;
            }

            drawList->AddLine(point,
                              {minBB.x, point.y},
                              IM_COL32(255, 0, 0, 128)
            );
        }

        skip_draw:
        if (drawInfiniteMax) {
            const ImVec2 point = transformToCanvas(maxPoint, min, max, minBB, maxBB);

            if (maxPoint.x > max.x || maxPoint.y > max.y) {
                return;
            }

            drawList->AddLine(point,
                              {maxBB.x, point.y},
                              IM_COL32(0, 255, 0, 128)
            );
        }
    }

    void
    processPointAdding(ImGuiNeoCurveContext &context, const ImVec2 &min,
                       const ImVec2 &max, const ImVec2 &minBB, const ImVec2 &maxBB) {
        const auto bb = ImRect{minBB, maxBB};

        if(!bb.Contains(GetMousePos()))
            return;

        if(IsMouseDoubleClicked(ImGuiMouseButton_Left)) {
            IM_ASSERT(!context.m_RequestAdd && "New point was already requested");
            context.m_RequestAdd = true;

            ImVec2 mousePos = GetMousePos();

            ImVec2 normalizedPosition = (mousePos - minBB) / (maxBB - minBB);

            ImVec2 pointPosition = min + ImVec2{normalizedPosition.x, 1.0f-normalizedPosition.y} * (max - min);

            context.m_PointToAdd = pointPosition;
        }
    }

    // API functions

    void EndNeoCurve() {
        IM_ASSERT(drawingCurve && "Called while not drawing any curve, that won't work!");
        drawingCurve = false;
        activeContextId = 0;
        const auto drawList = GetWindowDrawList();

        drawList->PopClipRect();

        PopID();
    }

    bool ImGui::BeginNeoCurve(const char *id, uint32_t pointCount, PrevTangetGetter prevTangentGetter,
                              PositionGetter positionGetter, NextTangentGetter nextTangentGetter, void *userData,
                              const ImVec2 &size,
                              ImGuiNeoCurveFlags flags) {
        IM_ASSERT(!drawingCurve && "Called while drawing other curve, that won't work, call End!");
        IM_ASSERT(pointCount >= 2 && "There must be at least 2 points");

        ImGuiNeoCurveContext &context = getContext(GetID(id));
        const bool canMovePoints = (flags & ImGuiNeoCurveFlags_EnablePointMoving) != 0;
        const bool canAddPoints = (flags & ImGuiNeoCurveFlags_EnablePointAdding) != 0;
        const bool drawGrid = (flags & ImGuiNeoCurveFlags_ShowGrid) != 0;


        ImGuiWindow *window = GetCurrentWindow();
        const auto &imStyle = GetStyle();

        if (drawingCurve)
            return false;

        if (window->SkipItems)
            return false;

        const auto drawList = GetWindowDrawList();

        const auto cursor = GetCursorScreenPos();
        const auto area = ImGui::GetContentRegionAvail();

        drawingCurve = true;
        activeContextId = GetID(id);

        PushID(id);

        auto realSize = ImFloor(size);
        if (realSize.x <= 0.0f)
            realSize.x = ImMax(4.0f, area.x);
        if (realSize.y <= 0.0f)
            realSize.y = ImMax(4.0f, area.y);

        const ImVec2 minBB = cursor;
        const ImVec2 maxBB = cursor + ImVec2{realSize};

        processCanvasControl(context, realSize, minBB, maxBB);

        ItemSize(realSize);

        // Draw background
        ImVec4 backgroundColor = curveStyle.Colors[ImGuiNeoCurveCol_Bg];
        drawList->AddRectFilled(cursor, cursor + ImVec2{realSize}, ColorConvertFloat4ToU32(backgroundColor));

        ImVec4 borderColor = curveStyle.Colors[ImGuiNeoCurveCol_Border];
        drawList->AddRect(cursor, cursor + ImVec2{realSize}, ColorConvertFloat4ToU32(borderColor));

        ImVec2 minVal = context.m_View.Min;
        ImVec2 maxVal = context.m_View.Max;

        // Draw curves

        drawList->PushClipRect(minBB, maxBB);

        if (drawGrid) {
            //Draws grid
            drawGridLines(drawList, minVal, maxVal, minBB, maxBB, cursor);
        }

        ImVec4 curveColor = curveStyle.Colors[ImGuiNeoCurveCol_Curve];

        //TODO: Check if multiselection flag is NOT set
        clearContextSelection(context);

        if (context.m_Dragging && canMovePoints) {
            updateSelectionPosition(context, prevTangentGetter, positionGetter, nextTangentGetter, userData, &minVal,
                                    &maxVal, &realSize);

            checkForSort(context, pointCount, prevTangentGetter, positionGetter, nextTangentGetter, userData);
        }


        drawInfiniteLines(drawList, flags & ImGuiNeoCurveFlags_ShowInfiniteMin,
                          flags & ImGuiNeoCurveFlags_ShowInfiniteMax, *positionGetter(0, userData),
                          *positionGetter(pointCount - 1, userData), minVal, maxVal, minBB, maxBB);


        for (int32_t i = 0; i < pointCount - 1; ++i) {
            ImVec2 p1 = *positionGetter(i, userData);
            ImVec2 p1NextTangent = *nextTangentGetter(i, userData);
            ImVec2 p2PrevTangent = *prevTangentGetter(i + 1, userData);
            ImVec2 p2 = *positionGetter(i + 1, userData);

            //Transform point to viewport
            p1 = transformToCanvas(p1, minVal, maxVal, minBB, maxBB);
            p1NextTangent = transformToCanvas(p1NextTangent, minVal, maxVal, minBB, maxBB);
            p2PrevTangent = transformToCanvas(p2PrevTangent, minVal, maxVal, minBB, maxBB);
            p2 = transformToCanvas(p2, minVal, maxVal, minBB, maxBB);

            // Adds items, so that we can move points

            if (i == 0)  // Process dragging for this point only when first iteration
                processPointSelection(&p1, i, 0, context, false);
            processPointSelection(&p2, i, 1, context, false);
            processPointSelection(&p1NextTangent, i, 2, context, true);
            processPointSelection(&p2PrevTangent, i, 3, context, true);

            // Adds bezier part of curve
            drawList->AddBezierCubic(p1, p1NextTangent, p2PrevTangent, p2, ColorConvertFloat4ToU32(curveColor),
                                     curveStyle.CurveThickness, curveStyle.CurveSmoothness);

            ImVec4 pointColor;

            // Draw lines to tangent points
            drawList->AddLine(p1, p1NextTangent,
                              ColorConvertFloat4ToU32(curveStyle.Colors[ImGuiNeoCurveCol_LineToTangent]),
                              curveStyle.LineToTangentThickness);
            drawList->AddLine(p2, p2PrevTangent,
                              ColorConvertFloat4ToU32(curveStyle.Colors[ImGuiNeoCurveCol_LineToTangent]),
                              curveStyle.LineToTangentThickness);


            // Add point to points
            if (i == 0) { // We dont draw previous P1, only when this is first iteration
                pointColor = getPointColor(i, 0, context, false);
                drawList->AddCircleFilled(p1, curveStyle.CurvePointRadius, ColorConvertFloat4ToU32(pointColor),
                                          curveStyle.CurvePointSegments);
            }


            pointColor = getPointColor(i, 1, context, false);
            drawList->AddCircleFilled(p2, curveStyle.CurvePointRadius, ColorConvertFloat4ToU32(pointColor),
                                      curveStyle.CurvePointSegments);


            // Add point to all tangent points
            pointColor = getPointColor(i, 2, context, true);
            drawList->AddCircleFilled(p1NextTangent, curveStyle.CurveTangentPointRadius,
                                      ColorConvertFloat4ToU32(pointColor), curveStyle.CurveTangentPointSegments);


            pointColor = getPointColor(i, 3, context, true);
            drawList->AddCircleFilled(p2PrevTangent, curveStyle.CurveTangentPointRadius,
                                      ColorConvertFloat4ToU32(pointColor), curveStyle.CurveTangentPointSegments);
        }

        if (canMovePoints && !context.m_Selection.empty()) {
            if (IsMouseDragging(ImGuiMouseButton_Left, 0.0f)) {
                if (!context.m_Dragging) {
                    context.m_Dragging = true;
                    context.m_DraggingMouseStartPosition = GetMousePos();
                    for (int32_t i = 0; i < context.m_Selection.size(); i++) {
                        context.m_DraggingStartPositions.push_back(
                                getPointFromIndex(context.m_Selection[i], prevTangentGetter,
                                                  positionGetter, nextTangentGetter, userData));
                    }
                    IM_ASSERT(context.m_Selection.size() == context.m_DraggingStartPositions.size());
                }
            } else {
                context.m_Dragging = false;
                context.m_DraggingMouseStartPosition = {0, 0};
                context.m_DraggingStartPositions.clear();
            }
        }

        if (canAddPoints) {
            processPointAdding(context,minVal, maxVal, minBB, maxBB);
        }

        return true;
    }

    // Styling

    void PushNeoCurveStyleColor(ImGuiNeoCurveCol idx, ImU32 col) {
        ImGuiColorMod backup;
        backup.Col = idx;
        backup.BackupValue = curveStyle.Colors[idx];
        colorStack.push_back(backup);
        curveStyle.Colors[idx] = ColorConvertU32ToFloat4(col);
    }

    void ImGui::PushNeoCurveStyleColor(ImGuiNeoCurveCol idx, const ImVec4 &col) {
        ImGuiColorMod backup;
        backup.Col = idx;
        backup.BackupValue = curveStyle.Colors[idx];
        colorStack.push_back(backup);
        curveStyle.Colors[idx] = col;
    }

    void PopNeoCurveStyleColor(int count) {
        while (count > 0) {
            ImGuiColorMod &backup = colorStack.back();
            curveStyle.Colors[backup.Col] = backup.BackupValue;
            colorStack.pop_back();
            count--;
        }
    }

    ImGuiNeoCurveStyle &GetNeoCurveStyle() {
        return curveStyle;
    }

    bool AddNeoCurvePoint() {
        IM_ASSERT(drawingCurve && "Called while not drawing any curve, that won't work!");
        ImGuiNeoCurveContext &context = getContext(activeContextId);

        return context.m_RequestAdd;
    }

    ImVec2 GetAddNeoCurvePointPosition() {
        IM_ASSERT(drawingCurve && "Called while not drawing any curve, that won't work!");
        ImGuiNeoCurveContext &context = getContext(activeContextId);

        context.m_RequestAdd = false;

        return context.m_PointToAdd;
    }
}

ImGuiNeoCurveStyle::ImGuiNeoCurveStyle() {
    Colors[ImGuiNeoCurveCol_Bg] = ImVec4{0.11f, 0.11f, 0.11f, 1.00f};
    Colors[ImGuiNeoCurveCol_Border] = ImVec4{0.71f, 0.71f, 0.71f, 1.00f};
    Colors[ImGuiNeoCurveCol_Curve] = ImVec4{0.81f, 0.21f, 0.21f, 1.00f};
    Colors[ImGuiNeoCurveCol_CurvePoint] = ImVec4{0.81f, 0.46f, 0.21f, 1.00f};
    Colors[ImGuiNeoCurveCol_CurvePointHovered] = ImVec4{1.0f, 1.0f, 1.0f, 1.00f};
    Colors[ImGuiNeoCurveCol_CurvePointSelected] = ImVec4{0.95f, 0.56f, 0.21f, 1.00f};
    Colors[ImGuiNeoCurveCol_CurveTangentPoint] = ImVec4{0.81f, 0.46f, 0.46f, 1.00f};
    Colors[ImGuiNeoCurveCol_CurveTangentPointHovered] = ImVec4{1.0f, 1.0f, 1.0f, 1.00f};
    Colors[ImGuiNeoCurveCol_CurveTangentPointSelected] = ImVec4{0.95f, 0.56f, 0.21f, 1.00f};
    Colors[ImGuiNeoCurveCol_LineToTangent] = ImVec4{0.71f, 0.36f, 0.36f, 1.00f};

}
