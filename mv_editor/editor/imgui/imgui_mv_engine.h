//
// Created by Matty on 07.07.2024.
//

#ifndef MV_EDITOR_IMGUI_MV_ENGINE_H
#define MV_EDITOR_IMGUI_MV_ENGINE_H

#include "vec4.hpp"
#include "imgui.h"

namespace ImGui {

    IMGUI_API ImU32 ColorConvertFloat4ToU32GLM(const glm::vec4& in);

}

#endif //MV_EDITOR_IMGUI_MV_ENGINE_H
