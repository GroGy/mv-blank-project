//
// Created by Matty on 2022-10-02.
//

#ifndef IMGUI_NEO_CURVE_H
#define IMGUI_NEO_CURVE_H

#include "imgui.h"
#include <cstdint>

typedef int ImGuiNeoCurveFlags; // Used for passing ImGuiNeoCurveFlags_
typedef int ImGuiNeoCurveCol; // Used for passing ImGuiNeoCurveCol_

// Flags for ImGui::BeginNeoCurve()
enum ImGuiNeoCurveFlags_
{
    ImGuiNeoCurveFlags_None                     = 0     ,
    ImGuiNeoCurveFlags_EnablePointAdding        = 1 << 0, // Enables adding point to data, requires calling AddNeoCurvePoint in scope curve scope and sorting
    ImGuiNeoCurveFlags_EnablePointMoving        = 1 << 1, // Enables moving of points, requires sort callback to work properly
    ImGuiNeoCurveFlags_ShowGrid                 = 1 << 2, // Shows grid in background
    ImGuiNeoCurveFlags_ShowInfiniteMin          = 1 << 3, // Shows infinite line to -infinity from lowest point
    ImGuiNeoCurveFlags_ShowInfiniteMax          = 1 << 4, // Shows infinite line to infinity from highest point

    ImGuiNeoCurveFlags_ShowInfiniteLines          = ImGuiNeoCurveFlags_ShowInfiniteMin | ImGuiNeoCurveFlags_ShowInfiniteMax,
};

enum ImGuiNeoCurveCol_
{
    ImGuiNeoCurveCol_Bg,
    ImGuiNeoCurveCol_Border,

    ImGuiNeoCurveCol_Curve,

    ImGuiNeoCurveCol_CurvePoint,
    ImGuiNeoCurveCol_CurvePointHovered,
    ImGuiNeoCurveCol_CurvePointSelected,

    ImGuiNeoCurveCol_CurveTangentPoint,
    ImGuiNeoCurveCol_CurveTangentPointHovered,
    ImGuiNeoCurveCol_CurveTangentPointSelected,

    ImGuiNeoCurveCol_LineToTangent,

    ImGuiNeoCurveCol_COUNT
};

struct ImGuiNeoCurveStyle {

    ImVec4          Colors[ImGuiNeoCurveCol_COUNT];

    float           CurveThickness = 1.0f;
    int32_t         CurveSmoothness = 24;

    float           CurvePointRadius = 4.0f;
    int32_t         CurvePointSegments = 8;

    float           CurveTangentPointRadius = 3.5f;
    int32_t         CurveTangentPointSegments = 4;

    float           LineToTangentThickness = 1.0f;

    explicit ImGuiNeoCurveStyle();
};

namespace ImGui {
    typedef ImVec2 * (*PrevTangetGetter)(uint32_t,void*);
    typedef ImVec2 * (*PositionGetter)(uint32_t,void*);
    typedef ImVec2 * (*NextTangentGetter)(uint32_t,void*);

    // Draws neo curve editor
    // Pass
    IMGUI_API bool BeginNeoCurve(const char* id, uint32_t pointCount, PrevTangetGetter prevTangentGetter, PositionGetter positionGetter, NextTangentGetter nextTangentGetter, void * userData, const ImVec2& size = ImVec2(0, 0), ImGuiNeoCurveFlags flags = ImGuiNeoCurveFlags_None);

    // Call inside Begin / End NeoCurve scope, returns true if new point on curve was requested, call GetAddNeoCurve after, this completes request
    IMGUI_API bool AddNeoCurvePoint();
    IMGUI_API ImVec2 GetAddNeoCurvePointPosition();

    // Call only when BeginNeoCurve returns true
    IMGUI_API void EndNeoCurve();

    // Neo curve style
    IMGUI_API ImGuiNeoCurveStyle& GetNeoCurveStyle();

    IMGUI_API void PushNeoCurveStyleColor(ImGuiNeoCurveCol idx, ImU32 col);
    IMGUI_API void PushNeoCurveStyleColor(ImGuiNeoCurveCol idx, const ImVec4& col);
    IMGUI_API void PopNeoCurveStyleColor(int count = 1);
}

#endif //IMGUI_NEO_CURVE_H
