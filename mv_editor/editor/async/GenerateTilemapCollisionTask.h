//
// Created by Matty on 11.12.2022.
//

#ifndef MV_EDITOR_GENERATETILEMAPCOLLISIONTASK_H
#define MV_EDITOR_GENERATETILEMAPCOLLISIONTASK_H

#include "EditorTask.h"
#include "resources/TilemapData.h"
#include "2d/TileSet.h"

namespace Editor {
    struct GenerateTilemapCollisionTaskInput {
        explicit GenerateTilemapCollisionTaskInput(const MV::TilemapData &data);

        const MV::TilemapData & m_Data;
        MV::rc<MV::Tileset> m_Tileset;
    };

    struct CollisionEdge {
        uint32_t index1;
        uint32_t index2;
    };

    struct CollisionData {
        MV::vector<CollisionEdge> m_Edges;
        MV::vector<glm::vec2> m_Vertices;

        // Hold tile position of initial collider, invalid after mergeAdjacentTiles gets called.
        glm::uvec2 m_TilePosition;
    };

    namespace TilemapCollisionGeneration {
        struct CollisionBody {
            CollisionData m_Data;
            /// Indicates that this shape is convex, if its not, it has to split into convex polygons
            bool m_Convex = false;
            /// Indicates that this body is processed and has less or equal polygons to our physics limitations
            bool m_Processed = false;
        };

        struct GenerationContext {
            MV::bitvector m_ProcessedTiles;
            MV::vector<CollisionBody> m_Bodies;
            float* m_Progress = nullptr;
        };
    }

    class GenerateTilemapCollisionTask : public EditorTask {
    public:

        GenerateTilemapCollisionTask(GenerateTilemapCollisionTaskInput&& input);

    public: // Overrides

        void Process() override;

        EditorTaskFinishResult Finish() override;

    private: // Helper functions

        void generateCollision(TilemapCollisionGeneration::GenerationContext& ctx);

        void convertBodiesToStrips(TilemapCollisionGeneration::GenerationContext& ctx);

        /// Merges adjacent tiles into bigger colliders, result might not be convex colliders, but those get processed later
        MV::vector<CollisionData> mergeAdjacentTiles(const MV::vector<CollisionData>& colliders) const;

        /// Determine, if two colliders directly next to each other should merge
        bool shouldMergeColliders(const CollisionData& a, const CollisionData& b) const;

        /// Enqueues colliders to process queue if they are in position lookup and next to each other
        void enqueueAdjacentColliders(  const glm::uvec2& tilePosition,
                                        const eastl::function<int32_t(const glm::uvec2&)>& lookupCallback,
                                        MV::queue<int32_t>& processQueue) const;

    private: // Converting helper functions

        CollisionEdge findStartingEdge(const CollisionData& data) const;

        MV::vector<CollisionData> optimizeColliders(const MV::vector<CollisionData>& colliders);

    public: // Public fields

        MV::TilesetCollision m_Result;

    private: // Input

        GenerateTilemapCollisionTaskInput m_pInput;

    private: // Internal fields

    };
}


#endif //MV_EDITOR_GENERATETILEMAPCOLLISIONTASK_H
