//
// Created by Matty on 05.06.2024.
//

#ifndef MV_EDITOR_WORLDOBJECTCONFIGTASK_H
#define MV_EDITOR_WORLDOBJECTCONFIGTASK_H

#include "EditorTask.h"
#include "meta/Class.h"

namespace Editor
{
    struct WorldObjectConfigTaskInput {
        MV::string m_Name;
        MV::string m_Path;
        MV::Meta::Class m_Class;
    };

    class WorldObjectConfigTask : public EditorTask
    {
    public:
        WorldObjectConfigTask(WorldObjectConfigTaskInput&& input);

        void Process() override;

        EditorTaskFinishResult Finish() override;

    private:
        WorldObjectConfigTaskInput m_pInput;

        MV::string m_pFinalPath;
    };
}


#endif //MV_EDITOR_WORLDOBJECTCONFIGTASK_H
