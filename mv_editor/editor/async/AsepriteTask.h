//
// Created by Matty on 2021-10-28.
//

#ifndef MV_EDITOR_ASEPRITETASK_H
#define MV_EDITOR_ASEPRITETASK_H

#include <2d/TextureAtlas.h>
#include "EditorTask.h"
#include <json.hpp>
#include <resources/AtlasData.h>

namespace Editor {
    static const char * ASEPRITE_JSON_FRAMES_FIELD = "frames";
    static const char * ASEPRITE_JSON_METADATA_FIELD = "meta";
    static const char * ASEPRITE_JSON_FRAME_DURATION_FIELD = "duration";
    static const char * ASEPRITE_JSON_FRAME_UV_FIELD = "frame";
    static const char * ASEPRITE_JSON_FRAME_OFFSET_FIELD = "spriteSourceSize";
    static const char * ASEPRITE_JSON_SIZE_FIELD = "sourceSize";
    static const char * ASEPRITE_JSON_FRAME_TOTAL_SIZE_FIELD = "size";
    static const char * ASEPRITE_JSON_WIDTH_FIELD = "w";
    static const char * ASEPRITE_JSON_HEIGHT_FIELD = "h";
    static const char * ASEPRITE_JSON_FRAME_NAME_FIELD = "filename";
    static const char * ASEPRITE_JSON_X_FIELD = "x";
    static const char * ASEPRITE_JSON_Y_FIELD = "y";

    /// Task which parses aseprite json files and returns atlas, which stores sprites generated off this data
    class AsepriteTask : public EditorTask {
    public: //CTor

        AsepriteTask(MV::string dataPath);

    public: // Overrides

        void Process() override;

        EditorTaskFinishResult Finish() override;

    private: // Internal helpers

        nlohmann::json importJson();

        void processJson(nlohmann::json & json);

        void processFrame(const nlohmann::json& json,uint32_t& index,const glm::uvec2& totalSize, const MV::string& frameName = {});


    public: // Public fields

        MV::AtlasData m_Result;

        bool m_LoadAfterFinishing = false;

    private: // Internal fields

        MV::string m_pDataPath;

    };
}


#endif //MV_EDITOR_ASEPRITETASK_H
