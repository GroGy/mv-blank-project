//
// Created by Matty on 2022-01-22.
//

#ifndef MV_EDITOR_ATLASTASK_H
#define MV_EDITOR_ATLASTASK_H

#include "EditorTask.h"
#include <glm.hpp>
#include <resources/AtlasData.h>

namespace Editor {
    struct AtlasTaskInput {
        MV::string m_Name;
        MV::string m_Path;
        glm::uvec2 m_SpriteSize{};
        uint64_t m_Texture;
        bool m_GenerateSprites = false;
    };

    class [[with_debug_printer(verbose)]] AtlasTask : public EditorTask {
    private:
        AtlasTaskInput m_pInput;
        MV::string m_pFinalPath;
    public:
        explicit AtlasTask(AtlasTaskInput input);

        bool m_LoadAfterFinishing = false;
        MV::AtlasData m_Result;

        void Process() override;

        EditorTaskFinishResult Finish() override;

        void createAtlasData();
    };

}


#endif //MV_EDITOR_ATLASTASK_H
