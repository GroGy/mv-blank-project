//
// Created by Matty on 2021-10-28.
//

#ifndef MV_EDITOR_TEXTURETASK_H
#define MV_EDITOR_TEXTURETASK_H

#include "EditorTask.h"

namespace Editor {
    /// Task which takes path and load texture from it, if it loads it successfully, its then either reimported to asset which was selected or created as new asset.
    /// Also supports multi-import, where set of textures can be imported.
    class TextureTask : public EditorTask {
    public:

        TextureTask(const MV::vector<MV::string>& textures, MV::string_view resultDirectory);

        TextureTask(const MV::string& textureReimportPath, MV::UUID textureToReimport);

    public: // Overrides

        void Process() override;

        EditorTaskFinishResult Finish() override;

    private:

        MV::vector<MV::string> m_pTexturePaths;

        MV::vector<MV::string> m_pResultPaths;

        MV::string m_pDirectory;

        // Path where reimported texture should be stored
        MV::string m_pReimportTexturePath;

        // Specifies that this task is reimport
        bool m_pIsReimport = false;

        MV::UUID m_pReimportAsset = MV::InvalidUUID;
    };
}



#endif //MV_EDITOR_TEXTURETASK_H
