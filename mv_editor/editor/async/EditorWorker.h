//
// Created by Matty on 2021-10-28.
//

#ifndef MV_EDITOR_EDITORWORKER_H
#define MV_EDITOR_EDITORWORKER_H

#include "EditorTask.h"
#include "util/thread_safe_queue.h"
#include <common/EngineCommon.h>

namespace Editor {
    class EditorWorker {
    private:
        MV::rc<MV::thread_safe_queue<MV::rc<EditorTask>>> & m_pTasks;
        MV::rc<MV::thread_safe_queue<MV::rc<EditorTask>>> & m_pFinishedTasks;
        std::thread m_pThread;
        volatile bool m_pRun = true;
        bool m_pValid = false;
        volatile bool m_pJoinable = false;

        void processLoop();
    public:
        EditorWorker(MV::rc<MV::thread_safe_queue<MV::rc<EditorTask>>> & tasks, MV::rc<MV::thread_safe_queue<MV::rc<EditorTask>>> & finished);

        void AddTask(MV::rc<EditorTask> task);

        void Init();

        void Cleanup();
    };
}


#endif //MV_EDITOR_EDITORWORKER_H
