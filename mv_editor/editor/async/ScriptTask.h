//
// Created by Matty on 2022-02-08.
//

#ifndef MV_EDITOR_SCRIPTTASK_H
#define MV_EDITOR_SCRIPTTASK_H


#include "EditorTask.h"

namespace Editor {
    namespace Config {
        static constexpr MV::string_view sc_WorldObjectScriptHeaderFmt = "using System;\n"
                                                                         "\n"
                                                                         "namespace {}\n";

        static constexpr MV::string_view sc_WorldObjectScriptDefinitionFmt = "    public class {} : {}\n";

        static constexpr MV::string_view sc_WorldObjectScriptFmt = "    {\n"
                                                                   "        public override void OnStart()\n"
                                                                   "        {\n"
                                                                   "\n"
                                                                   "        }\n"
                                                                   "\n"
                                                                   "        public override void Update(float delta)\n"
                                                                   "        {\n"
                                                                   "\n"
                                                                   "        }\n"
                                                                   "    }\n"
                                                                   "}";
    }

    struct ScriptTaskInput {
        MV::string m_Name;
        MV::string m_Path;
        MV::string m_BaseClass;
    };

    class ScriptTask : public EditorTask {
    private:
        ScriptTaskInput m_pInput;
        MV::string m_pFinalPath;
    public:
        explicit ScriptTask(ScriptTaskInput input);

        bool m_LoadAfterFinishing = false;
        //MV::ScriptData m_Result;

        void Process() override;

        EditorTaskFinishResult Finish() override;
    };
}


#endif //MV_EDITOR_SCRIPTTASK_H
