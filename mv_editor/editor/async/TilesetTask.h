//
// Created by Matty on 2022-01-20.
//

#ifndef MV_EDITOR_TILESETTASK_H
#define MV_EDITOR_TILESETTASK_H

#include <resources/TilesetData.h>
#include "EditorTask.h"

namespace Editor {
    struct TilesetTaskInput {
        MV::string m_Name;
        MV::string m_Path;
        std::vector<uint32_t> m_IDToAtlasID;
        uint64_t m_Atlas = 0;
    };

class TilesetTask : public EditorTask {
private:
    TilesetTaskInput m_pInput;
    MV::string m_pFinalPath;
public:
    explicit TilesetTask(TilesetTaskInput input);
    bool m_LoadAfterFinishing = false;
    MV::TilesetData m_Result;

    void Process() override;

    EditorTaskFinishResult Finish() override;
};
}

#endif //MV_EDITOR_TILESETTASK_H
