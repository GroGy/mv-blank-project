//
// Created by Matty on 08.12.2022.
//

#ifndef MV_EDITOR_TILEMAPTASK_H
#define MV_EDITOR_TILEMAPTASK_H

#include <resources/TilemapData.h>
#include "EditorTask.h"

namespace Editor {
    struct TilemapTaskInput {
        MV::string m_Name;
        MV::string m_Path;
        uint64_t m_Tileset = 0;
        uint32_t m_Width;
        uint32_t m_Height;
    };

    class TilemapTask : public EditorTask {
    private:
        TilemapTaskInput m_pInput;
        MV::string m_pFinalPath;
    public:
        explicit TilemapTask(TilemapTaskInput input);
        bool m_LoadAfterFinishing = false;
        MV::TilemapData m_Result;

        void Process() override;

        EditorTaskFinishResult Finish() override;
    };
}

#endif //MV_EDITOR_TILEMAPTASK_H
