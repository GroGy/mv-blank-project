//
// Created by Matty on 19.08.2023.
//

#ifndef MV_EDITOR_ANIMATIONTASK_H
#define MV_EDITOR_ANIMATIONTASK_H

#include "EditorTask.h"

namespace Editor
{
    struct AnimationTaskInput {
        MV::string m_Name;
        MV::string m_Path;
    };

    class AnimationTask : public EditorTask
    {
    public:
        AnimationTask(AnimationTaskInput&& input);

        bool m_LoadAfterFinishing = false;

        void Process() override;

        EditorTaskFinishResult Finish() override;

    private:
        AnimationTaskInput m_pInput;

        MV::string m_pFinalPath;
    };
}


#endif //MV_EDITOR_ANIMATIONTASK_H
