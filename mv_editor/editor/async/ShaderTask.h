//
// Created by Matty on 2022-03-13.
//

#ifndef MV_EDITOR_SHADERTASK_H
#define MV_EDITOR_SHADERTASK_H

#include "EditorTask.h"

namespace Editor {
    namespace Config {
        static constexpr MV::string_view sc_VertexDefault = "#version 450\n"
                                                            "#extension GL_ARB_separate_shader_objects : enable\n"
                                                            "\n"
                                                            "layout(location = 0) in vec3 inPos;\n"
                                                            "layout(location = 1) in vec2 inUV;\n"
                                                            "\n"
                                                            "layout( push_constant ) uniform constant\n"
                                                            "{\n"
                                                            "    mat4 MVP;\n"
                                                            "} PushConstants;\n"
                                                            "\n"
                                                            "layout(location = 0) out vec4 fragColor;\n"
                                                            "layout(location = 1) out vec2 uv;\n"
                                                            "\n"
                                                            "void main() {\n"
                                                            "    gl_Position = PushConstants.MVP * vec4(inPos, 1.0);\n"
                                                            "    fragColor = vec4(inUV.xy,0.0,1.0);\n"
                                                            "    uv = inUV;\n"
                                                            "}";
        static constexpr MV::string_view sc_FragmentDefault = "#version 450\n"
                                                              "#extension GL_ARB_separate_shader_objects : enable\n"
                                                              "\n"
                                                              "layout(location = 0) in vec4 fragColor;\n"
                                                              "layout(location = 1) in vec2 uv;\n"
                                                              "\n"
                                                              "layout(binding = 0) uniform sampler2D uTexture;\n"
                                                              "\n"
                                                              "layout(set = 1, binding = 1) uniform UniformBufferObject {\n"
                                                              "    vec4 color;\n"
                                                              "} ubo;\n"
                                                              "\n"
                                                              "layout(location = 0) out vec4 outColor;\n"
                                                              "\n"
                                                              "void main() {\n"
                                                              "    outColor = texture(uTexture, uv);\n"
                                                              "}\n";
    }

    enum class ShaderCreateType {
        VISUAL,//Fragment + Vertex
        COMPUTE
    };

    struct ShaderTaskInput {
        MV::string m_Name;
        MV::string m_Path;

        ShaderCreateType m_Type = ShaderCreateType::VISUAL;
    };

    class ShaderTask : public EditorTask {
    private:
        ShaderTaskInput m_pInput;
    public:
        ShaderTask(ShaderTaskInput&& input);

        bool m_LoadAfterFinishing = false;

        void Process() override;

        EditorTaskFinishResult Finish() override;
    };
}


#endif //MV_EDITOR_SHADERTASK_H
