//
// Created by Matty on 2021-10-28.
//

#ifndef MV_EDITOR_EDITORTASK_H
#define MV_EDITOR_EDITORTASK_H

#include <common/Types.h>
#include <resources/common/Resource.h>

namespace Editor {
    enum class EditorTaskFinishResult {
        FAILED,
        SUCCESS,
        REQUEUE
    };

    class EditorTask {
    public:
        static void WaitForResource(uint64_t resource);
        static void WaitForResource(const MV::rc<MV::Resource> & res);

        virtual void Process() = 0;
        virtual EditorTaskFinishResult Finish() = 0;

        [[nodiscard]]
        bool Success() const { return m_pSuccess; }

        [[nodiscard]]
        float GetProgress() const { return m_pProgress;}

        void Start(MV::rc<EditorTask> self);

        [[nodiscard]]
        bool IsDone() const { return m_pDone;}

        [[nodiscard]]
        MV::string_view GetErrorText() const {return m_pErrorText;};
    protected:
        bool m_pSuccess = false;
        bool m_pDone = false;
        float m_pProgress = 0.0f;
        MV::string m_pErrorText;
    };
}


#endif //MV_EDITOR_EDITORTASK_H
