//
// Created by Matty on 2022-01-17.
//

#ifndef MV_EDITOR_WORLDTASK_H
#define MV_EDITOR_WORLDTASK_H

#include "EditorTask.h"
#include <glm.hpp>
#include <common/World.h>

namespace Editor {
    struct WorldTaskInput {
        MV::string m_Name;
        MV::string m_Path;
    };

    class WorldTask : public EditorTask {
    private:
        WorldTaskInput m_pInput;
    public:
        MV::string m_FinalPath;
        explicit WorldTask(WorldTaskInput input);
        bool m_LoadAfterFinishing = false;
        MV::WorldData m_Result;
        uint64_t m_WorldUUID;

        void Process() override;

        EditorTaskFinishResult Finish() override;
    };
}



#endif //MV_EDITOR_WORLDTASK_H
