//
// Created by Matty on 2021-10-15.
//

#ifndef MV_EDITOR_EDITOR_H
#define MV_EDITOR_EDITOR_H

#include <common/EngineCommon.h>
#include <Engine.h>

#include "Editor_Module.h"
#include "EditorUI.h"
#include "async/EditorTask.h"
#include "async/EditorWorker.h"
#include "EditorSettings.h"
#include "RecentProject.h"
#include "resources/AssetWatcher.h"
#include "world_picking/ViewportCamera.h"

namespace Editor {
    enum class InputMode {
        Editor,
        Game
    };

    static const uint32_t WORKER_THREAD_COUNT = 32;

    class Editor {
    public:

        friend class Editor_Module;
        friend class EditorTask;
        friend class EditorUI;

    public:

        Editor() = default;
        ~Editor();

        void Init(MV::Engine & engine);

        [[nodiscard]]
        MV::string GetLoadedProjectPath() const { return m_pLoadedProjectPath.empty() ? MV::GetEngine()->GetAutoLoadedProjectPath() : m_pLoadedProjectPath; }


        std::vector<RecentProject> & GetRecentProjects();

        void SaveRecentProjects();

        void StartGame();
        void StopGame(bool skipEngineStopGame = false);

        void SaveData();

        void CloseCurrentProject();

        void OnAssetChange();

        bool TextInputGlobal(MV::string_view name,MV::string * data, const GlobalTextInputCallback & onFinish = [](bool){});

        bool GlobalPopupOk(MV::string_view title, PopupCloseCallback&& callback, MV::string_view body = "");

        bool GlobalPopupConfirmOrCancel(MV::string_view title, PopupCloseCallback&& callback, MV::string_view body = "");

        bool GlobalPopupYesNo(MV::string_view title, PopupCloseCallback&& callback, MV::string_view body = "");

        bool GlobalPopupCustomModal(MV::string_view title, CustomModalDrawCallback&& callback);

        void MarkAssetDirtyForWorld(MV::IAssetReference Asset);

        MV::vector<MV::IAssetReference> FetchDirtyAssetsForWorld();

        MV::rc<ViewportCamera> m_EditorViewportCamera;

    public: // Getters and setters

        [[nodiscard]]
        InputMode GetInputMode() const { return m_pInputMode; };

        void SetInputMode(InputMode inputMode);

        template<typename WindowType>
        inline MV::rc<WindowType> GetWindow();

        FORCEINLINE bool IsGameStarting() const { return m_pGameStartPending; }

        FORCEINLINE MV::WorldLoadProgress GetStartProgress() const { return m_pGameStartProgress; }

    private:

        void updatePendingWorldStart();

    private: // Recent projects functions

        void initLastOpenedProjects();

    private: // Task system functions

        void initWorkers();

        void finishTasks();

        void cleanupWorkers();

        void queueTask(MV::rc<EditorTask> task);

    private: // Base callbacks invoked by engine

        bool onCleanup();

        bool onRendererCleanup();

        void onWindowResize();

        void onInit();

        void onPostInit();

        void onUpdate(float deltaTime);

        void onRender();

        void onEditorRender();

    public:

        MV::unordered_map<MV::string, std::pair<uint64_t,MV::AssetType>> m_ProjectAssetTypes;

        MV::unordered_set<uint64_t> m_DirtyAssets{};

        // Contains assets that were changed in editor while world wasn't focused,
        // if world contains reference to any of this, its reloaded on focus
        MV::unordered_set<uint64_t> m_WorldReloadAssets{};

        bool m_ShowAllCollisions = false;

        uint64_t m_SelectedWO = 0;

        uint64_t m_SelectedWO_Runtime = 0;

        EditorSettingsVals m_Settings;

        AssetWatcher m_AssetWatcher{};

        bool m_pGameStartPending = false;

        MV::WorldLoadProgress m_pGameStartProgress;

    private: // Base fields for module and UI

        MV::rc<Editor_Module> m_pModule{};

        MV::rc<EditorUI> m_pEditorUI{};

    private: // Task system fields

        MV::vector<MV::rc<EditorWorker>> m_pWorkers{};

        MV::rc<MV::thread_safe_queue<MV::rc<EditorTask>>> m_pTasks{};

        MV::rc<MV::thread_safe_queue<MV::rc<EditorTask>>> m_pFinishedTasks{};

    private: // Recent projects fields

        std::vector<RecentProject> m_pRecentProjects{};

    private: // Current project data

        MV::string m_pLoadedProjectPath;

    private: // Input

        InputMode m_pInputMode = InputMode::Editor;

        bool m_pAssignedCameraToWorld = false;

    private: // Dirty data fields

        MV::unordered_set<MV::IAssetReference> m_pDirtyAssetsForWorld;
    };

    template <typename WindowType>
    MV::rc<WindowType> Editor::GetWindow()
    {
        const auto res = m_pEditorUI->GetWindow<WindowType>();
        ENSURE_TRUE(res.operator bool(), nullptr);
        return res;
    }
}

#endif //MV_EDITOR_EDITOR_H
