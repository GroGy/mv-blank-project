//
// Created by Matty on 21.08.2023.
//

#ifndef MV_EDITOR_ANIMATIONEDITORIMGUI_H
#define MV_EDITOR_ANIMATIONEDITORIMGUI_H

#include "imgui/imgui.h"

namespace Editor::ImGui2
{
    typedef uint8_t SequenceButtonRes;

    enum SequenceButtonRes_
    {
        SequenceButtonRes_None                  = 0     ,
        SequenceButtonRes_SelectButton          = 1 << 0,
        SequenceButtonRes_DeleteButton          = 1 << 1,
        SequenceButtonRes_RightClicked          = 1 << 2,
        SequenceButtonRes_Hovered               = 1 << 3
    };

    IMGUI_API SequenceButtonRes SequenceButton(MV::string_view label, const ImVec2& size_args = ImVec2{0,0});

    IMGUI_API bool RecordButton(const char* id, const ImVec2& size_arg = {0,0}, ImGuiButtonFlags flags = ImGuiButtonFlags_None);
}


#endif //MV_EDITOR_ANIMATIONEDITORIMGUI_H
