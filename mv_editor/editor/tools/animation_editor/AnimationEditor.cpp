//
// Created by Matty on 19.08.2023.
//

#include "AnimationEditor.h"
#include "animation/AnimationProperty.h"
#include "util/UtilGui.h"
#include "common/EngineCommon.h"
#include "Engine.h"
#include "EditorCommon.h"
#include "imgui/imgui_neo_sequencer.h"
#include "AnimationEditorImgui.h"
#include "interface/common/viewport_gizmo_providers/WorldObjectGizmoProvider.h"
#include "AnimationEditorGizmoProvider.h"
#include "render/common/Renderer.h"
#include "precompiler/AnimatablePropertyRegistrator.h"
#include "imgui/icons.h"
#include "interface/object_editor/PropertyFieldRenderer.h"
#include "resources/ProducerUtils.h"
#include "resources/AnimationProducer.h"
#include "resources/AnimationResource.h"
#include "common/AnimationAsset.h"
#include "2d/SpriteProperty.h"
#include "serialization/common/FieldSerializer.h"
#include "common/Assert.h"
#include "AnimationDummyObject.h"

namespace Editor
{

    AnimationEditor::AnimationEditor() : Window(Config::sc_AnimationEditorWindowName.data(), false)
    {
        m_pOpen = false;
        m_Flags |= ImGuiWindowFlags_MenuBar;
    }

    static constexpr MV::string_view sc_SettingsPanelWindowName = "##animation_editor_settings_panel";
    static constexpr MV::string_view sc_EditorsWindowName = "##animation_editor_editors";
    static constexpr MV::string_view sc_ViewportWindowName = "##animation_editor_sequencer_viewport";

    void AnimationEditor::WindowSetup()
    {
        if (isDirty())
        {
            m_Flags |= ImGuiWindowFlags_UnsavedDocument;   // Add the flag
        } else
        {
            m_Flags &= ~ImGuiWindowFlags_UnsavedDocument;  // Remove the flag
        }
    }

    void AnimationEditor::Render()
    {
        MV::AllocatorTagScope tag{MV::AllocationType::Editor};

        if (m_pDataState == DataState::NoData)
        {
            ImGui::TextUnformatted("No data loaded in animation editor.");
            return;
        }

        updateResources();

        ScopedDisabled dis{m_pDataState == DataState::Loading};

        drawDragAndDropState();

        drawInputPopup();

        updateActiveSequence();

        drawMenuBar();

        setupDockspace();

        drawSettings();

        drawEditors();
    }

    void AnimationEditor::PostRender()
    {

    }

    void AnimationEditor::OnWorldRender()
    {
        if (m_pDataState == DataState::NoData) return;
        if (!m_pOpen) return;
        if (m_pSelectedSequenceIndex == -1 || !m_pWorld.IsValid()) return;

        m_pWorld.Render();
    }

    void AnimationEditor::OnCleanup()
    {
        if (m_pWorld.IsValid())
        {
            m_pWorld.Cleanup();
        }

        //TODO: if(m_pStateMachineEditor) m_pStateMachineEditor->Cleanup();
    }

    void AnimationEditor::OnInit()
    {
        m_pStateMachineEditor = MV::make_rc<AESMEditor>();
        m_pStateMachineEditor->Init();
        m_pStateMachineEditor->SetEditor(this);
    }

    void AnimationEditor::drawSettings()
    {
        MV::AllocatorTagScope allocatorTag{MV::AllocationType::Editor};
        if (!ImGui::Begin(sc_SettingsPanelWindowName.data()))
        {
            ImGui::End();
            return;
        }

        bool dirty = false;

        drawSettingsSequences(dirty);

        if (dirty)
            m_AssetChanged = true;

        ImGui::End();
    }

    void AnimationEditor::setupDockspace()
    {
        MV::AllocatorTagScope allocatorTag{MV::AllocationType::Editor};
        auto centerNode = ImGui::GetID("##animation_editor_dockspace");
        ImGui::DockSpace(centerNode);

        //TODO: Maximum size of settings

        if (ImGui::DockBuilderGetNode(centerNode)->IsLeafNode())
        {
            ImGuiID node_editor_nodeId;
            ImGuiID settings_nodeId;
            ImGui::DockBuilderSplitNode(centerNode, ImGuiDir_Left, 0.2f, &settings_nodeId, &node_editor_nodeId);

            const auto viewport_node = ImGui::DockBuilderGetNode(node_editor_nodeId);
            viewport_node->LocalFlags |=
                    static_cast<ImGuiDockNodeFlags_>(ImGuiDockNodeFlags_NoDocking | ImGuiDockNodeFlags_NoTabBar) |
                    ImGuiDockNodeFlags_NoSplit;

            const auto settings_node = ImGui::DockBuilderGetNode(settings_nodeId);
            settings_node->LocalFlags |=
                    static_cast<ImGuiDockNodeFlags_>(ImGuiDockNodeFlags_NoDocking | ImGuiDockNodeFlags_NoTabBar) |
                    ImGuiDockNodeFlags_NoSplit;

            ImGui::DockBuilderDockWindow(sc_EditorsWindowName.data(), node_editor_nodeId);
            ImGui::DockBuilderDockWindow(sc_SettingsPanelWindowName.data(), settings_nodeId);

            ImGui::DockBuilderFinish(node_editor_nodeId);
            ImGui::DockBuilderFinish(settings_nodeId);
        }
    }

    void AnimationEditor::drawEditors()
    {
        MV::AllocatorTagScope allocatorTag{MV::AllocationType::Editor};


        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0.0f, 0.0f});
        if (!ImGui::Begin(sc_EditorsWindowName.data()))
        {
            ImGui::PopStyleVar();
            ImGui::End();
            return;
        }
        ImGui::PopStyleVar();

        if (ImGui::BeginTabBar("##animation_editor_tab_bar", ImGuiTabBarFlags_FittingPolicyResizeDown))
        {
            ImGuiTabBar* tab_bar = ImGui::GetCurrentContext()->CurrentTabBar;

            if (m_pFocusSequencer)
            {
                tab_bar->VisibleTabId = ImGui::GetCurrentWindow()->GetID("Sequence");
                m_pFocusSequencer = false;
            }

            if (m_pSelectedSequenceIndex != -1 && ImGui::BeginTabItem("Sequence"))
            {
                drawSequenceEditor();
                ImGui::EndTabItem();
            }
            if (ImGui::BeginTabItem("State machine"))
            {
                drawStateMachineEditor();
                ImGui::EndTabItem();
            }
            ImGui::EndTabBar();
        }

        ImGui::End();
    }

    void AnimationEditor::drawMenuBar()
    {
        if (ImGui::BeginMenuBar())
        {
            if (ImGui::BeginMenu("File"))
            {
                {
                    ScopedDisabled dis{!isDirty()};
                    if (ImGui::MenuItem("Save"))
                    {
                        saveData();
                    }
                }

                ImGui::EndMenu();
            }

            if (ImGui::BeginMenu("Import"))
            {
                {
                    ScopedDisabled dis{m_pSelectedSequenceIndex == -1};
                    if (ImGui::MenuItem("Atlas animation"))
                    {
                        //TODO: Open popup that will allow importing animation directly from atlas, as atlas can have some extra metadata
                    }
                }

                ImGui::EndMenu();
            }

            ImGui::EndMenuBar();
        }
    }

    void AnimationEditor::LoadData(MV::AssetReference<MV::AnimationAssetType> animationAsset)
    {
        if (isDirty())
        {
            //TODO: Show modal popup which asks if user wants to override current data, which are changed
            MV::GetLogger()->LogWarning("TODO: Add modal popup for checking of override unsaved changes! {}:{}",
                                        __FILE__, __LINE__);
            loadDataImpl(animationAsset);
        } else
        {
            loadDataImpl(animationAsset);
        }
    }

    void AnimationEditor::drawSettingsSequences(bool& dirty)
    {
        ::ImGui2::LabeledSeparator("Inputs");

        drawAnimationInputs();

        ImGui::Dummy({0, ImGui::GetStyle().ItemSpacing.y / 2});

        ::ImGui2::LabeledSeparator("Sequences");

        drawAllSequences();
    }

    void AnimationEditor::drawStateMachineEditor()
    {
        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0.0f, 0.0f});
        if (!ImGui::BeginChild("##animation_editor_state_machine_editor", {-1, -1}, true))
        {
            ImGui::PopStyleVar();
            ImGui::EndChild();
            return;
        }
        ImGui::PopStyleVar();

        m_pStateMachineEditor->Render("State machine editor");

        ImGui::EndChild();
    }

    void AnimationEditor::addSequence(MV::string_view name, bool open)
    {
        if (name.empty()) return;

        MV::SingleAnimationData lf;
        lf.m_Name = name;

        auto foundRes = eastl::find(m_pData.m_Animations.begin(), m_pData.m_Animations.end(), lf,
                                    [](const MV::SingleAnimationData& el, const MV::SingleAnimationData& input)
                                    {
                                        return el.m_Name == input.m_Name;
                                    });

        if (foundRes != m_pData.m_Animations.end())
        {
            return;
        }

        uint32_t highestId = 0;

        for (const auto& anim: m_pData.m_Animations)
        {
            if (anim.m_ID > highestId)
            {
                highestId = anim.m_ID;
            }
        }

        auto& newSequence = m_pData.m_Animations.emplace_back();

        newSequence.m_Name = name;

        newSequence.m_ID = ++highestId;

        m_pSelectedSequenceIndex = m_pData.m_Animations.size() - 1;

        m_AssetChanged = true;

        m_pFocusSequencer = true;

        m_pSequencerData = {};

        createPreviewWorld();
    }

    void AnimationEditor::drawSequenceEditor()
    {
        bool openPopup = false;

        if (ImGui::BeginPopup("##animation_editor_seq_add_input_to_seq_popup",
                              ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoMove))
        {
            for (uint32_t inputIndex = 0; inputIndex < m_Inputs.size(); inputIndex++)
            {
                const auto& input = m_Inputs[inputIndex];

                if (ImGui::BeginMenu(input.m_Name.c_str()))
                {
                    for (uint32_t fieldIndex = 0; fieldIndex < input.m_Inputs.size(); fieldIndex++)
                    {
                        const auto& field = input.m_Inputs[fieldIndex];
                        if (ImGui::MenuItem(field.m_Name.c_str()))
                        {
                            addFieldToSequence(inputIndex, input, fieldIndex, field);
                            m_pSequencerData.m_Dirty = true;
                        }
                    }
                    ImGui::EndMenu();
                }
            }

            ImGui::EndPopup();
        }


        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0.0f, 0.0f});
        if (!ImGui::BeginChild(sc_EditorsWindowName.data(), {-1, -1}, true))
        {
            ImGui::PopStyleVar();
            ImGui::EndChild();
            return;
        }
        ImGui::PopStyleVar();

        auto nodeId = ImGui::DockSpace(ImGui::GetID("##sequencer_dockspace"));

        if (ImGui::DockBuilderGetNode(nodeId)->IsLeafNode())
        {
            ImGuiID viewport_nodeID;
            ImGuiID sequencer_nodeID;
            ImGuiID selected_timeline_nodeID;

            ImGui::DockBuilderSplitNode(nodeId, ImGuiDir_Right, 0.2f, &selected_timeline_nodeID, &nodeId);

            const auto selected_timeline_props_node = ImGui::DockBuilderGetNode(selected_timeline_nodeID);
            selected_timeline_props_node->LocalFlags |=
                    static_cast<ImGuiDockNodeFlags_>(ImGuiDockNodeFlags_NoDocking | ImGuiDockNodeFlags_NoTabBar) |
                    ImGuiDockNodeFlags_NoSplit;

            ImGui::DockBuilderSplitNode(nodeId, ImGuiDir_Down, 0.5f, &sequencer_nodeID, &viewport_nodeID);

            const auto viewport_node = ImGui::DockBuilderGetNode(viewport_nodeID);
            viewport_node->LocalFlags |=
                    static_cast<ImGuiDockNodeFlags_>(ImGuiDockNodeFlags_NoDocking | ImGuiDockNodeFlags_NoTabBar) |
                    ImGuiDockNodeFlags_NoSplit;

            const auto sequencer_node = ImGui::DockBuilderGetNode(sequencer_nodeID);
            sequencer_node->LocalFlags |=
                    static_cast<ImGuiDockNodeFlags_>(ImGuiDockNodeFlags_NoDocking | ImGuiDockNodeFlags_NoTabBar) |
                    ImGuiDockNodeFlags_NoSplit;

            ImGui::DockBuilderDockWindow(sc_ViewportWindowName.data(), viewport_nodeID);
            ImGui::DockBuilderDockWindow("##animation_editor_seq_editor_sequencer", sequencer_nodeID);
            ImGui::DockBuilderDockWindow("##animation_editor_seq_prop_editor", selected_timeline_nodeID);

            ImGui::DockBuilderFinish(viewport_nodeID);
            ImGui::DockBuilderFinish(sequencer_nodeID);
            ImGui::DockBuilderFinish(selected_timeline_nodeID);
        }

        {
            if (m_pWorld.IsValid() && m_pWorld.m_CameraOverride)
            {
                m_pViewport.Draw(sc_ViewportWindowName.data(), m_pWorld.m_CameraOverride);
            } else
            {
                ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0.0f, 0.0f});
                if (ImGui::Begin(sc_ViewportWindowName.data()))
                {
                    ImGui::PopStyleVar();

                    // Viewport
                    auto window = ImGui::GetCurrentWindow();
                    auto cursor = window->DC.CursorPos;
                    auto size = ImGui::GetContentRegionAvail();

                    window->DrawList->AddRectFilled(cursor, cursor + size, IM_COL32(0, 0, 0, 255));
                    ImGui::Dummy(size);
                } else
                {
                    ImGui::PopStyleVar();
                }
                ImGui::End();
            }
        }

        if (m_pSequencerData.m_SelectedTimelineGroup != -1)
        {
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0.0f, 0.0f});
            if (ImGui::Begin("##animation_editor_seq_prop_editor"))
            {
                PropertyRendererBase::s_ShowCreateKeyframeButton = true;
                PropertyRendererBase::s_OnCreateKeyframeCallback = [this](MV::AnimationInputField* res)
                {
                    createKeyframe(res);
                };

                PropertyRendererBase::s_KeySerializationEnabledCallback = [this](MV::Archive::ValueId valueId) -> bool
                {
                    const auto& fields = m_pSequencerData.m_Groups[m_pSequencerData.m_SelectedTimelineGroup].m_EnabledFields;
                    return fields.find(valueId) != fields.end();
                };

                PropertyRendererBase::s_EnableFieldSerializationCallback = [this](MV::Archive::ValueId valueId,
                                                                                  bool value)
                {
                    auto& fields = m_pSequencerData.m_Groups[m_pSequencerData.m_SelectedTimelineGroup].m_EnabledFields;
                    m_pSequencerData.m_Dirty = true;
                    if (value)
                    {
                        fields.emplace(valueId);
                    } else
                    {
                        fields.erase(valueId);
                    }
                };

                ImGui::PopStyleVar();

                const auto& group = m_pSequencerData.m_Groups[m_pSequencerData.m_SelectedTimelineGroup];
                const auto& input = m_Inputs[group.m_InputIndex];

                auto& wo = m_pWorld.m_WorldObjects[1];

                uint32_t counter = 0;

                FieldRenderer<PropertyType>::SetNextPropertyWindowHeight(ImGui::GetContentRegionAvail().y);

                MV::Property* inputProperty;

                auto& pManager = m_pWorld.m_PropertyManager;
                auto* storage = pManager.GetIStorageFromClass(input.m_PropertyClass);

                CHECK(storage);

                inputProperty = storage->get_property(input.m_Index);

                m_pSequencerData.m_Dirty |= FieldRenderer<PropertyType>::DrawProperty(EditorPropertyDrawingData::FromProperty(fmt::format("{}##animation_editor_property",input.m_Name).c_str(), inputProperty), counter, wo, wo.get(), {}, true);

                PropertyRendererBase::s_ShowCreateKeyframeButton = false;
                PropertyRendererBase::s_OnCreateKeyframeCallback = nullptr;
            } else
            {
                ImGui::PopStyleVar();
            }

            ImGui::End();
        }

        {
            bool anyTimelineGroupSelected = false;
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0.0f, 0.0f});
            if (ImGui::Begin("##animation_editor_seq_editor_sequencer"))
            {
                ImGui::PopStyleVar();

                ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, {1.0f, 0.0f});
                ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0.0f, 0.0f});

                if (ImGui::BeginChild("##up_bar", {-1.0, ImGui::GetFrameHeight()}))
                {
                    if (ImGui::Button(ICON_FA_PLUS, {0, -1}))
                    {
                        openPopup = !m_Inputs.empty();
                    }

                    ImGui::PopStyleVar(2);

                    if (ImGui::IsItemHovered() && m_Inputs.empty())
                    {
                        ImGui::BeginTooltip();
                        ImGui::TextUnformatted("No inputs present in animation, add them in inputs window!");
                        ImGui::EndTooltip();
                    }

                    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, {1.0f, 0.0f});
                    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0.0f, 0.0f});

                    ImGui::SameLine();

                    if (ImGui::Button(m_pSequencerData.m_Playing ? ICON_FA_PAUSE : ICON_FA_PLAY, {0, -1}))
                    {
                        if (m_pSequencerData.m_Playing)
                        {
                            m_pSequencerData.m_Playing = false;
                        } else
                        {
                            m_pSequencerData.m_Playing = true;
                            m_pSequencerData.m_LastAddedTime = MV::GetEngine()->GetTime();
                        }
                    }

                    ImGui::SameLine();
                    {
                        ScopedDisabled dis{m_pSequencerData.m_SelectedTimelineGroup == -1};
                        if (ImGui2::RecordButton("##add_frame_button", {0, -1}))
                        {

                        }
                    }

                    {
                        ImGui::SameLine();
                        ImGui::TextUnformatted("Time:");
                        ImGui::SameLine();
                        ImGui::PushItemWidth(ImGui::GetFrameHeight() * 4.0f);
                        m_pSequencerData.m_Dirty |= ImGui::InputDouble("##time", &m_pSequencerData.m_Time);
                        ImGui::SameLine();
                        ImGui::TextUnformatted("Frame:");
                        ImGui::SameLine();
                        m_pSequencerData.m_Dirty |= ImGui::InputInt("##frame", &m_pSequencerData.m_SelectedFrame, 1,
                                                                    100,
                                                                    ImGuiInputTextFlags_ReadOnly);

                        uint32_t frameMS = (uint32_t) (m_pSequencerData.m_FrameDuration * 1000);
                        uint32_t step = 1;
                        uint32_t fastStep = 100;

                        ImGui::SameLine();
                        ImGui::TextUnformatted("Frame duration:");
                        ImGui::SameLine();
                        ImGui::PushItemWidth(ImGui::GetFrameHeight() * 8.0f);
                        if (ImGui::InputScalar("##frame_duration", ImGuiDataType_U32, &frameMS, &step, &fastStep,
                                               "%ums"))
                        {
                            m_pSequencerData.m_Dirty = true;
                            m_pSequencerData.m_FrameDuration = (float) frameMS / 1000;
                        }
                        ImGui::SameLine();
                        if (ImGui::Button("12 fps"))
                        {
                            m_pSequencerData.m_FrameDuration = 1.0f / 12.0f;
                            m_pSequencerData.m_Dirty = true;
                        }

                        ImGui::SameLine();
                        ImGui::TextUnformatted("Range:");
                        ImGui::SameLine();
                        ImGui::PushItemWidth(ImGui::GetFrameHeight());
                        m_pSequencerData.m_Dirty |= ImGui::InputScalar("##animation_range_y", ImGuiDataType_U32,
                                                                       &m_pSequencerData.m_AnimationRange.x);
                        ImGui::SameLine();
                        ImGui::PushItemWidth(ImGui::GetFrameHeight());
                        m_pSequencerData.m_Dirty |= ImGui::InputScalar("##animation_range_x", ImGuiDataType_U32,
                                                                       &m_pSequencerData.m_AnimationRange.y);

                        if (m_pSequencerData.m_AnimationRange.x > m_pSequencerData.m_AnimationRange.y)
                            m_pSequencerData.m_AnimationRange.x = m_pSequencerData.m_AnimationRange.y;
                    }
                }
                ImGui::EndChild();

                ImGui::PopStyleVar();
                ImGui::PopStyleVar();

                if (ImGui::BeginNeoSequencer("##animation_editor_sequencer_widget", &m_pSequencerData.m_SelectedFrame,
                                             &m_pSequencerData.m_View.x, &m_pSequencerData.m_View.y,
                                             ImGui::GetContentRegionAvail(), ImGuiNeoSequencerFlags_EnableSelection |
                                                                             ImGuiNeoSequencerFlags_Selection_EnableDragging |
                                                                             ImGuiNeoSequencerFlags_Selection_EnableDeletion))
                {

                    if (ImGui::IsHoldingNeoFrameSelector())
                    {
                        m_pSequencerData.m_Time = (double) ((float) m_pSequencerData.m_SelectedFrame *
                                                            m_pSequencerData.m_FrameDuration);
                    }

                    for (auto&& tml: m_pSequencerData.m_Groups)
                    {
                        if (ImGui::BeginNeoGroup(tml.m_Name.c_str(), &tml.m_Open))
                        {
                            if (ImGui::IsNeoTimelineSelected())
                            {
                                m_pSequencerData.m_SelectedTimelineGroup = (int32_t) tml.m_InputIndex;
                                anyTimelineGroupSelected = true;
                            }
                            for (auto& fieldTml: tml.m_Timelines)
                            {
                                if (ImGui::BeginNeoTimelineEx(fieldTml.m_Name.c_str()))
                                {
                                    if (ImGui::IsNeoTimelineSelected())
                                    {
                                        m_pSequencerData.m_SelectedTimelineGroup = (int32_t) tml.m_InputIndex;
                                        anyTimelineGroupSelected = true;
                                    }

                                    for (auto& kf: fieldTml.m_Keyframes)
                                    {
                                        ImGui::NeoKeyframe(&kf.m_Keyframe);

                                        if (ImGui::IsNeoKeyframeRightClicked())
                                        {

                                        }
                                    }

                                    if (MV::GetEngine()->m_Input.IsKeyPressed(MV::Key::Delete))
                                    {
                                        if (ImGui::NeoCanDeleteSelection())
                                        {
                                            size_t size = ImGui::GetNeoKeyframeSelectionSize();

                                            if (size > 0)
                                            {
                                                MV::vector<int32_t> frames;
                                                frames.resize(size);

                                                ImGui::GetNeoKeyframeSelection(frames.data());

                                                for (auto& k: fieldTml.m_Keyframes)
                                                {
                                                    for (auto& dk: frames)
                                                    {
                                                        if (k.m_Keyframe == dk)
                                                        {
                                                            //TODO: delete
                                                        }
                                                    }
                                                }

                                                ImGui::NeoClearSelection();
                                            }
                                        }
                                    }

                                    ImGui::EndNeoTimeLine();
                                }
                            }

                            ImGui::EndNeoGroup();
                        }

                        if (!anyTimelineGroupSelected)
                        {
                            if (ImGui::IsLastNeoTimelineSelected())
                            {
                                m_pSequencerData.m_SelectedTimelineGroup = (int32_t) tml.m_InputIndex;
                                anyTimelineGroupSelected = true;
                            }
                        }
                    }

                    ImGui::EndNeoSequencer();
                }
            } else
            {
                ImGui::PopStyleVar();
            }
            ImGui::End();


            if (!anyTimelineGroupSelected)
            {
                m_pSequencerData.m_SelectedTimelineGroup = -1;
            }
        }

        ImGui::EndChild();

        if (openPopup)
        {
            ImGui::OpenPopup("##animation_editor_seq_add_input_to_seq_popup");
        }
    }

    void AnimationEditor::addFieldToSequence(uint32_t inputIndex, const AnimationInputData& input, uint32_t fieldIndex,
                                             const MV::AnimationInputField& field)
    {
        SequencerData::TimelineDataGroup* group = nullptr;

        for (auto& tmlGroup: this->m_pSequencerData.m_Groups)
        {
            if (tmlGroup.m_InputIndex == inputIndex)
            {
                group = &tmlGroup;
                break;
            }
        }

        if (!group)
        {
            group = &this->m_pSequencerData.m_Groups.emplace_back();
            group->m_InputIndex = inputIndex;
            group->m_Name = input.m_Name;
        }

        for (auto& seq: group->m_Timelines)
        {
            if (seq.m_InputFieldIndex == fieldIndex)
            {
                ENSURE_NO_ENTRY();
                return;
            }
        }

        auto& seq = group->m_Timelines.emplace_back();

        seq.m_Name = field.m_Name;
        seq.m_InputId = inputIndex;
        seq.m_InputFieldIndex = fieldIndex;
        seq.m_InputFieldUUID = m_Inputs[seq.m_InputId].m_Inputs[seq.m_InputFieldIndex].m_UUID;
    }

    AnimationEditor::TrySelectSequenceResult
    AnimationEditor::tryToSelectSequence(const MV::SingleAnimationData& sequenceData)
    {
        if (m_pSelectedSequenceIndex != -1 && m_pSequencerData.m_Dirty)
        {
            saveEditorDataToAssetData();
            loadEditorDataFromLoadedAsset();
            //TODO: Show global modal popup, to inform user that there are changes that might get lost if not saved
        }

        auto res = eastl::find(m_pData.m_Animations.begin(), m_pData.m_Animations.end(), sequenceData);

        if (res == m_pData.m_Animations.end())
        {
            return TrySelectSequenceResult::Failed;
        }

        m_pSequencerData = {};

        m_pSelectedSequenceIndex = eastl::distance(m_pData.m_Animations.begin(), res);

        m_pSequencerData.m_AnimationRange = res->m_AnimationRange;
        m_pSequencerData.m_FrameDuration = res->m_FrameDuration;

        for (const auto& seq: res->m_Sequences)
        {
            SequencerData::TimelineDataGroup* group = nullptr;

            auto index = -1;

            for (uint32_t i = 0; i < m_Inputs.size(); i++)
            {
                auto& input = m_Inputs[i];
                if (input.m_ID == seq.m_InputID)
                {
                    index = (int32_t) i;
                    break;
                }
            }

            if (index == -1)
            {
                MV::Assert(false);
                continue;
            }

            for (auto& tmlGroup: this->m_pSequencerData.m_Groups)
            {
                if (m_Inputs[tmlGroup.m_InputIndex].m_ID == seq.m_InputID)
                {
                    group = &tmlGroup;
                    break;
                }
            }

            if (!group)
            {
                group = &m_pSequencerData.m_Groups.emplace_back();
                group->m_InputIndex = index;
                group->m_Name = m_Inputs[index].m_Name;
                for (const auto& v: m_pData.m_Animations)
                {
                    for (const auto& p: v.m_Data)
                    {
                        if(p.m_ID != m_Inputs[index].m_ID) continue;

                        for (auto&& sf: p.m_SerializedFields)
                            group->m_EnabledFields.emplace(sf);
                        break;
                    }
                }
            }

            for (auto& gSeq: group->m_Timelines)
            {
                if (gSeq.m_InputFieldUUID == seq.m_FieldUUID)
                {
                    ENSURE_NO_ENTRY(TrySelectSequenceResult::Failed);
                    return TrySelectSequenceResult::Failed;
                }
            }

            const MV::AnimationInputField* fieldInfo = nullptr;
            size_t fieldIndex = 0;

            for (uint32_t i = 0; i < m_Inputs[index].m_Inputs.size(); i++)
            {
                const auto& input = m_Inputs[index].m_Inputs[i];
                if (input.m_UUID == seq.m_FieldUUID)
                {
                    fieldInfo = &input;
                    fieldIndex = i;
                    break;
                }
            }

            if (!fieldInfo)
            {
                ENSURE_NO_ENTRY(TrySelectSequenceResult::Failed)
                return TrySelectSequenceResult::Failed;
            }

            auto& gSeq = group->m_Timelines.emplace_back();

            gSeq.m_Name = fieldInfo->m_Name;
            gSeq.m_InputFieldIndex = fieldIndex;
            gSeq.m_InputFieldUUID = seq.m_FieldUUID;
            gSeq.m_InputId = index;
            gSeq.m_Keyframes = seq.m_Keyframes;
        }

        m_pFocusSequencer = true;

        createPreviewWorld();

        return
                TrySelectSequenceResult::Selected;
    }

    void AnimationEditor::createPreviewWorld()
    {
        m_pWorldData = MV::WorldData{};

        if (m_pWorld.m_CameraOverride)
        {
            m_pWorld.m_CameraOverride->Cleanup();
        }


        if (m_pWorld.IsValid())
        {
            m_pWorld.Cleanup();
        }

        m_pWorld = MV::move(AnimationEditorWorld{MV::WorldData{m_pWorldData}});

        m_pWorld.Init();

        m_pWorld.m_CameraOverride = MV::make_rc<MV::Camera>();
        m_pWorld.m_CameraOverride->Init();

        MV::WorldObjectSpawnParams params;

        params.m_Name = "AnimationObject";
        params.m_SkipSerialization = true;
        params.m_Class = MV::GetClassMeta<AnimationDummyObject>();

        m_pWorld.m_PropertyManager.Init();

        auto wo = m_pWorld.SpawnWorldObject(params);

        m_pGizmoProvider = MV::make_rc<AnimationEditorGizmoProvider>(wo, m_pWorld);

        m_pViewport.m_AllowGizmo = true;
        m_pViewport.m_AllowPropertyGizmos = true;
        m_pViewport.SetGizmoProvider(m_pGizmoProvider);

        for (auto&& i: m_Inputs)
        {
            addInputPropertyToWorld(i, i.m_PropertyClass);
        }

        m_pAnimationProperty = &m_pWorld.m_PropertyManager.GetStorage<MV::AnimationProperty>().emplace_back();
        m_pAnimationProperty->m_WorldObject = wo;
    }

    void AnimationEditor::drawAnimationInputs()
    {
        bool openAddPopup = false;

        const auto totalHeight = ImGui::GetContentRegionAvail().y;

        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0.0f, 0.0f});
        bool drawInputs = ImGui::BeginChild("##animation_editor_inputs", {-1, totalHeight / 2}, true);
        ImGui::PopStyleVar();

        if (!drawInputs)
        {
            ImGui::EndChild();
        } else
        {
            if (ImGui::Button("Add", {-1, 0}))
            {
                openAddPopup = true;
            }

            for (uint32_t i = 0; i < m_Inputs.size(); i++)
            {
                auto& v = m_Inputs[i];
                const auto res = ImGui2::SequenceButton(v.m_Name, {-1, 0});

                if (res & ImGui2::SequenceButtonRes_RightClicked)
                {
                    m_pOpenEditInputPopup = true;
                }
            }

            ImGui::EndChild();
        }


        if (openAddPopup)
        {
            ImGui::OpenPopup("##animation_editor_add_input_popup");
        }

        if (ImGui::BeginPopup("##animation_editor_add_input_popup",
                              ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoMove))
        {
            auto& registeredAnims = MV::RegisteredAnimatableProperties::GetInstance();

            for (auto& v: registeredAnims.m_Data)
            {
                if (ImGui::MenuItem(v.m_PropertyName.c_str()))
                {
                    addAnimationInput(v);
                }
            }

            ImGui::EndPopup();
        }
    }

    void AnimationEditor::drawAllSequences()
    {
        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0.0f, 0.0f});
        if (!ImGui::BeginChild("##animation_editor_sequences", {-1, -1}, true))
        {
            ImGui::PopStyleVar();
            ImGui::EndChild();
            return;
        }
        ImGui::PopStyleVar();

        static MV::string newSequenceName;

        if (ImGui::Button("Add", {-1, 0}))
        {
            newSequenceName.clear();
            GetEditor()->TextInputGlobal("Sequence", &newSequenceName, [this, name = &newSequenceName](bool success)
            {
                if (success)
                    addSequence(*name);
            });
        }

        for (auto&& seq: m_pData.m_Animations)
        {
            auto res = ImGui2::SequenceButton(seq.m_Name, {-1, 0});

            if (res & ImGui2::SequenceButtonRes_DeleteButton)
            {
                MV::GetLogger()->LogWarning("Deleted {}", seq.m_Name);
            }

            if (res & ImGui2::SequenceButtonRes_SelectButton)
            {
                tryToSelectSequence(seq);
            }

            if (m_pDraggingAnimationId == 0 && ImGui::IsMouseDragging(ImGuiMouseButton_Left) &&
                res & ImGui2::SequenceButtonRes_Hovered)
            {
                m_pDraggingAnimationId = seq.m_ID;
                m_pJustStartedDragging = true;
            }
        }

        ImGui::EndChild();
    }

    void AnimationEditor::addAnimationInput(const MV::RegisteredAnimatableProperties::PropertyData& data)
    {
        static MV::string name;
        name.clear();

        static uint32_t inputIdCounter = 1000000;

        GetEditor()->TextInputGlobal(data.m_PropertyName, &name, [&, d(data)](bool success)
        {
            if (!success) return;
            if (name.empty()) return;

            m_AssetChanged = true;

            auto& v = m_Inputs.emplace_back();
            v.m_Name = name;
            v.m_InternalName = name;
            v.m_Inputs = d.m_Fields;
            v.m_ID = ++inputIdCounter;
            v.m_PropertyClass = d.m_PropertyClass;

            if (m_pSelectedSequenceIndex != -1)
                addInputPropertyToWorld(v, d.m_PropertyClass);
        });

    }

    void AnimationEditor::updateActiveSequence()
    {
        if (m_pSelectedSequenceIndex == -1) return;

        if (!m_pSequencerData.m_Playing && m_pSequencerData.m_PreviewFrame == m_pSequencerData.m_SelectedFrame) return;

        if (m_pSequencerData.m_Playing)
        {
            auto now = MV::GetEngine()->GetTime();

            auto diff = now - m_pSequencerData.m_LastAddedTime;

            m_pSequencerData.m_LastAddedTime = now;

            m_pSequencerData.m_Time += diff;

            auto animationLen = m_pSequencerData.m_AnimationRange.y - m_pSequencerData.m_AnimationRange.x;

            if (animationLen > 0)
            {
                m_pSequencerData.m_SelectedFrame = m_pSequencerData.m_AnimationRange.x +
                                                   (int32_t) (m_pSequencerData.m_Time /
                                                              m_pSequencerData.m_FrameDuration) %
                                                   animationLen;
            }
        }

        m_pSequencerData.m_PreviewFrame = m_pSequencerData.m_SelectedFrame;

        for (auto& g: m_pSequencerData.m_Groups)
        {
            auto& input = m_Inputs[g.m_InputIndex];
        }
    }

    void AnimationEditor::addInputPropertyToWorld(AnimationInputData& data, MV::Meta::Class propertyClass)
    {
        auto& pManager = m_pWorld.m_PropertyManager;
        auto* storage = pManager.GetIStorageFromClass(propertyClass);
        size_t index = storage->create();

        auto& wo = m_pWorld.m_WorldObjects[1];

        storage->get_property(index)->m_WorldObject = wo;

        data.m_Index = index;

    }

    void AnimationEditor::drawInputPopup()
    {
        if (m_pOpenEditInputPopup)
        {
            ImGui::OpenPopup("##edit_input_property");
            m_pOpenEditInputPopup = false;
        }

        if (ImGui::BeginPopup("##edit_input_property", ImGuiWindowFlags_NoDecoration))
        {
            {
                ScopedDisabled dis{m_pSelectedSequenceIndex == -1};
                if (ImGui::MenuItem("Rename"))
                {
                    static MV::string newName;
                    newName.clear();
                    GetEditor()->TextInputGlobal("Input", &newName, [this, name = &newName](bool success)
                    {
                        if (success)
                        {

                        }
                    });
                    ImGui::CloseCurrentPopup();
                }

                if (ImGui::MenuItem("Delete"))
                {
                    ImGui::CloseCurrentPopup();
                }
            }
            ImGui::EndPopup();
        }
    }

    void AnimationEditor::saveData()
    {
        saveEditorDataToAssetData();

        auto path = MV::GetEngine()->GetProject().m_Assets[m_pAsset].m_Path;

        auto resPath = AnimationProducer::CreateAnimationResource(m_pData, path, true);

        MV::GetEngine()->m_ResourceManager.UnloadAsset(m_pAsset);

        GetEditor()->MarkAssetDirtyForWorld(m_pAsset);

        m_AssetChanged = false;
        m_pSequencerData.m_Dirty = false;
        m_pStateMachineEditor->ClearDirty();
    }

    void AnimationEditor::loadDataImpl(MV::AssetReference<MV::AnimationAssetType> asset)
    {
        m_pAsset = asset;

        const auto& animations = MV::GetResources()->m_Animations;

        const auto foundAnimation = animations.find(asset);

        if (foundAnimation == animations.end())
        {
            auto resource = MV::GetEngine()->m_ResourceManager.LoadResource(asset);
            m_pResource = MV::static_pointer_cast<MV::AnimationResource>(resource);
            m_pDataState = DataState::Loading;
            return;
        }

        if (!foundAnimation->second->AssetLoaded())
        {
            m_pDataState = DataState::Loading;
            return;
        }

        loadEditorDataFromLoadedAsset();
    }

    void AnimationEditor::updateResources()
    {
        if (m_pDataState == DataState::NoData) return;

        if (!m_pResource && m_pDataState == DataState::HasData)
        {
            return;
        }

        if (m_pResource)
        {
            if (!m_pResource->IsFinished()) return;

            if (!m_pResource->Success())
            {
                m_pDataState = DataState::NoData;
                m_pResource.reset();
                return;
            }

            loadEditorDataFromLoadedAsset();
            m_pResource.reset();

        }

        const auto& animations = MV::GetResources()->m_Animations;
        const auto foundAnimation = animations.find(m_pAsset);

        ENSURE_TRUE(foundAnimation != animations.end())

        if (foundAnimation->second->AssetLoaded())
        {
            loadEditorDataFromLoadedAsset();
        }
    }

    void AnimationEditor::loadEditorDataFromLoadedAsset()
    {
        const auto& animations = MV::GetResources()->m_Animations;
        const auto foundAnimation = animations.find(m_pAsset);

        ENSURE_TRUE(foundAnimation != animations.end())

        m_pData = foundAnimation->second->GetData();

        m_pSequencerData = {};
        m_pSelectedSequenceIndex = -1;

        m_Inputs.clear();

        auto& registeredProps = MV::RegisteredAnimatableProperties::GetInstance();

        m_Inputs.reserve(m_pData.m_Inputs.size());

        m_pStateMachineEditor->LoadData(m_pData.m_StateMachine);

        m_pDataState = DataState::HasData;
    }

    void AnimationEditor::saveEditorDataToAssetData()
    {
        auto oldData = m_pData;

        if (m_pSelectedSequenceIndex != -1)
        {
            saveSelectedSequence();
        }

        m_pData.m_Inputs.clear();

        m_pData.m_Inputs.resize(m_Inputs.size());

        m_pData.m_StateMachine = m_pStateMachineEditor->SaveData();
    }

    void AnimationEditor::saveSelectedSequence()
    {
        ENSURE_TRUE(m_pSelectedSequenceIndex != -1);
        auto& saveTarget = m_pData.m_Animations[m_pSelectedSequenceIndex];
        const auto& src = m_pSequencerData;

        saveTarget.m_AnimationRange = src.m_AnimationRange;
        saveTarget.m_FrameDuration = src.m_FrameDuration;

        MV::unordered_map<uint64_t, uint32_t> fieldIds;

        for (auto&& v: saveTarget.m_Sequences)
        {
            fieldIds[v.m_FieldUUID] = v.m_ID;
        }

        uint32_t idCounter = 0;

        for (const auto& s: m_pData.m_Animations)
        {
            for (const auto& t: s.m_Sequences)
            {
                if (t.m_ID > idCounter) idCounter = t.m_ID;
            }
        }

        saveTarget.m_Sequences.clear();
        saveTarget.m_Data.clear();

        for (const auto& g: src.m_Groups)
        {
            for (const auto& t: g.m_Timelines)
            {
                auto& seq = saveTarget.m_Sequences.emplace_back();

                auto foundId = fieldIds.find(t.m_InputFieldUUID);

                if (foundId == fieldIds.end())
                {
                    seq.m_ID = ++idCounter;
                } else
                {
                    seq.m_ID = foundId->second;
                }

                seq.m_InputID = m_Inputs[t.m_InputId].m_ID;
                seq.m_FieldUUID = t.m_InputFieldUUID;

                seq.m_Keyframes = t.m_Keyframes;
            }

            const auto& input = m_Inputs[g.m_InputIndex];
        }
    }

    void AnimationEditor::drawDragAndDropState()
    {
        if (m_pDraggingAnimationId == 0) return;

        ImGuiDragDropFlags src_flags = 0;
        src_flags |= ImGuiDragDropFlags_SourceNoDisableHover;
        src_flags |= ImGuiDragDropFlags_SourceNoHoldToOpenOthers;
        src_flags |= ImGuiDragDropFlags_SourceExtern;

        if (ImGui::BeginDragDropSource(src_flags))
        {
            for (auto&& a: m_pData.m_Animations)
            {
                if (a.m_ID == m_pDraggingAnimationId)
                {
                    ImGui::TextUnformatted(a.m_Name.c_str());
                    break;
                }
            }

            if (m_pJustStartedDragging)
            {
                AnimationDragAndDropPayload pld;
                pld.m_DragAnimId = m_pDraggingAnimationId;
                pld.m_Editor = this;

                ImGui::SetDragDropPayload("MV_ANIM_ED_State", &pld, sizeof(pld));
                m_pJustStartedDragging = false;
            }
            ImGui::EndDragDropSource();
        }

        if (!ImGui::IsMouseDown(ImGuiMouseButton_Left))
        {
            ClearDragAndDrop();
        }
    }

    void AnimationEditor::ClearDragAndDrop()
    {
        m_pDraggingAnimationId = 0;
        m_pJustStartedDragging = false;
    }

    const MV::SingleAnimationData* AnimationEditor::GetAnimationFromID(uint32_t id) const
    {
        ENSURE_TRUE(id != 0, nullptr);

        for (const auto& a: m_pData.m_Animations)
            if (a.m_ID == id) return &a;

        return nullptr;
    }

    void AnimationEditor::OnOpenStateChanged(bool newValue)
    {
        if(!newValue) {
            m_pAsset = MV::InvalidUUID;
            m_pSequencerData = {};
            m_pSelectedSequenceIndex = -1;
            m_Inputs.clear();
            m_pDataState = DataState::NoData;

            if(m_pWorld.IsValid()) {
                m_pWorld.Cleanup();
            }
        }
    }

    void AnimationEditor::createKeyframe(MV::AnimationInputField* res)
    {
        ENSURE_VALID(res);

        if (!res) return;

        auto foundGroup = eastl::find(m_pSequencerData.m_Groups.begin(), m_pSequencerData.m_Groups.end(),
                                      m_pSequencerData.m_SelectedTimelineGroup,
                                      [](const SequencerData::TimelineDataGroup& a,
                                         uint32_t selectedTimelineIndex)
                                      {
                                          return a.m_InputIndex == selectedTimelineIndex;
                                      });

        if (foundGroup == m_pSequencerData.m_Groups.end()) return;

        SequencerData::TimelineDataGroup::TimelineData* fieldTimeline = nullptr;

        for (auto& f: foundGroup->m_Timelines)
        {
            if (f.m_InputFieldUUID == res->m_UUID)
            {
                fieldTimeline = &f;
                break;
            }
        }

        if (!fieldTimeline)
        {
            auto& timeline = foundGroup->m_Timelines.emplace_back();
            timeline.m_InputId = m_pSequencerData.m_SelectedTimelineGroup;
            timeline.m_Name = res->m_Name;

            int32_t index = -1;
            for (uint32_t i = 0; i < m_Inputs[timeline.m_InputId].m_Inputs.size(); i++)
            {
                auto& value = m_Inputs[timeline.m_InputId].m_Inputs[i];
                if (value.m_UUID == res->m_UUID)
                {
                    index = (int32_t) i;
                    break;
                }
            }

            if (index == -1)
            {
                ENSURE_NO_ENTRY();
                return;
            }

            timeline.m_InputFieldIndex = (uint32_t) index;
            timeline.m_InputFieldUUID = res->m_UUID;

            fieldTimeline = &timeline;
        }

        {
            SequencerData::TimelineDataGroup::TimelineData::Keyframe* keyframe = nullptr;
            for (auto& kf: fieldTimeline->m_Keyframes)
            {
                if (kf.m_Keyframe == m_pSequencerData.m_SelectedFrame)
                {
                    keyframe = &kf;
                    break;
                }
            }

            if (!keyframe)
            {
                auto& newKf = fieldTimeline->m_Keyframes.emplace_back();
                newKf.m_Keyframe = m_pSequencerData.m_SelectedFrame;
                keyframe = &newKf;
            }

            auto& registeredProps = MV::RegisteredAnimatableProperties::GetInstance();

            MV::RegisteredAnimatableProperties::PropertyData* animableData = nullptr;

            auto id = m_pWorld.m_PropertyManager.GetIDFromClass(res->m_Owner);

            for (auto& d: registeredProps.m_Data)
            {
                if (d.m_ID == id)
                {
                    animableData = &d;
                    break;
                }
            }

            if (!animableData)
            {
                ENSURE_NO_ENTRY();
                return;
            }

            auto& input = m_Inputs[foundGroup->m_InputIndex];

            auto istorage = m_pWorld.m_PropertyManager.GetIStorageFromClass(input.m_PropertyClass);

            auto* prop = istorage->get_property(input.m_Index);

            animableData->m_ReadCallback(prop, *res, &keyframe->m_Value);

        }
    }

}