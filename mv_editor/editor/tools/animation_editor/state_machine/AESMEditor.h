//
// Created by Matty on 29.08.2023.
//

#ifndef MV_EDITOR_AESMEDITOR_H
#define MV_EDITOR_AESMEDITOR_H

#include <EngineMinimal.h>
#include "StateMachineNodeEditor.h"
#include "AESMData.h"
#include "resources/AnimationData.h"

namespace Editor
{
    class StateNode;
    class AnimationEditor;

    // Stands for Animation Editor State Machine Editor
    class AESMEditor
    {
        friend class StateMachineNodeEditor;
    public:

        void Init();

        void Render(MV::string_view label);

        void LoadData(const MV::AnimationStateMachineData& data);

        MV::AnimationStateMachineData SaveData();

    public: // Setters

        void SetEditor(AnimationEditor* editor);

        void ClearDirty();

    public: // Getters

        [[nodiscard]]
        FORCEINLINE bool IsDirty() const { return m_pDirty; }

    private:

        void drawPendingLinkPopup();

        uint32_t findFirstValidId() const;

    private:

        void setDefaultNode(const MV::rc<StateNode>& node);

        void drawTransitionSetting(const NodeEditorLink& link);

        void addLink();

    private:

        MV::rc<StateMachineNodeEditor> m_pNodeEditor;

        AESMData m_pData;

        AnimationEditor* m_pAnimationEditor = nullptr;

        bool m_pDirty = false;

        // Maps from State ID to its only Pin on node
        MV::unordered_map<uint32_t, NodeEditorId> m_pStateIdPins;

        MV::unordered_map<NodeEditorId, uint32_t> m_pLinkToTransition;
    };
}


#endif //MV_EDITOR_AESMEDITOR_H
