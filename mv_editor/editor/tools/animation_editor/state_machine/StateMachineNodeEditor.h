//
// Created by Matty on 29.08.2023.
//

#ifndef MV_EDITOR_STATEMACHINENODEEDITOR_H
#define MV_EDITOR_STATEMACHINENODEEDITOR_H

#include "node_editor/NodeEditorBase.h"

namespace Editor
{
    class AESMEditor;

    struct PendingLink {
        bool m_Pending = false;
        NodeEditorId m_StartPin = sc_InvalidNodeEditorId;
        NodeEditorId m_EndPin = sc_InvalidNodeEditorId;

        uint32_t m_FromAnimation;
        uint32_t m_ToAnimation;
    };

    class StateMachineNodeEditor : public NodeEditorBase
    {
    protected:

        void RenderImpl() override;

        void InitNodeTypes() override;

        void InitImpl() override;

        void PreRender() override;

        void PostRender() override;

        bool ConfirmLink(NodeEditorId beginPin, NodeEditorId endPin) override;

        void OnNodeAdded(const MV::rc<NodeEditorNodeBase>& node) override;

        void OnNodeRemoved(const MV::rc<NodeEditorNodeBase>& node) override;

        void ClearImpl() override;

        void DrawNodeContext(const MV::rc<NodeEditorNodeBase>& node) override;

        void DrawLinkContext(const NodeEditorLink& link) override;

        bool EnableNodeContextMenu() override;

        bool EnableLinkContextMenu() override;

    public:


    public:

        NodeEditorId  ConfirmPendingLink();

    public:

        MV::unordered_set<uint32_t> m_PresentStates;

        MV::unordered_map<NodeEditorId, uint32_t> m_PinAnimationIds;

        PendingLink m_PendingLink;

        bool m_OpenPendingLinkPopup = false;

        AESMEditor* m_Editor = nullptr;
    };
}


#endif //MV_EDITOR_STATEMACHINENODEEDITOR_H
