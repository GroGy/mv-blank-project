//
// Created by Matty on 30.08.2023.
//

#ifndef MV_EDITOR_AESMDATA_H
#define MV_EDITOR_AESMDATA_H

#include <cstdint>
#include "common/Types.h"
#include "vec2.hpp"
#include "resources/AnimationData.h"

namespace Editor
{
    namespace AESM {
        struct StateTransition {
            using Type = MV::AnimationStateMachineTransitionType;

            uint32_t m_ID;

            Type m_Type;

            uint32_t m_FromState;
            uint32_t m_ToState;
        };


        struct State {
            uint32_t m_ID;
            uint32_t m_AnimationId;

            MV::vector<uint32_t> m_OutTransitions;

            glm::vec2 m_Position;

            bool m_Looping;

            bool m_Cancelable;
        };
    }

    struct AESMData
    {
        MV::vector<AESM::State> m_States;

        MV::vector<AESM::StateTransition> m_Transitions;

        int32_t m_DefaultStateID = -1;
    };
}

#endif //MV_EDITOR_AESMDATA_H
