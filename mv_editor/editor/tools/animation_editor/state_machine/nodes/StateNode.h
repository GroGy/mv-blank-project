//
// Created by Matty on 29.08.2023.
//

#ifndef MV_EDITOR_STATENODE_H
#define MV_EDITOR_STATENODE_H

#include "StateMachineNodeBase.h"

namespace Editor {
    class StateNode : public StateMachineNodeBase
    {
    protected:

        void RenderImpl(const NodeEditorContext& ctx) override;

    public:

        MV::string m_StateName;

        uint32_t m_AnimationID;

        uint32_t m_StateID;

        bool m_Looping = false;

        bool m_Cancelable = false;
    };
}


#endif //MV_EDITOR_STATENODE_H
