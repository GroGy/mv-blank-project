//
// Created by Matty on 29.08.2023.
//

#include "StateMachineNodeBase.h"
#include "imgui/imgui.h"
#include "util/UtilGui.h"

namespace Editor
{
    void StateMachineNodeBase::RenderImpl(const NodeEditorContext& ctx)
    {
    }

    void StateMachineNodeBase::InitImpl(NodeEditorId& idCounter)
    {
        AddOutputPin(idCounter);
    }

    void StateMachineNodeBase::AfterRenderImpl()
    {
        auto dl = ed::GetNodeBackgroundDrawList(GetID());

        if(!dl) {
            return;
        }

        float sideOffset = m_pRect.GetWidth() / 10.0f;



        auto color = UtilGui::FixImGuiColorSpace(m_IsDefaultState ? Config::sc_InitialNodeExtraColor : m_pColor);

        ImRect extraRect{m_pRect.GetTL() + ImVec2{sideOffset,-ed::GetStyle().NodePadding.y + ed::GetStyle().NodeBorderWidth}, ImVec2{m_pRect.GetBR().x - sideOffset, m_pRect.GetCenter().y}};

        dl->AddRectFilled(extraRect.GetTL(), extraRect.GetBR(), ImGui::ColorConvertFloat4ToU32(color));
        dl->AddRect(extraRect.GetTL() + ImVec2(0, -ed::GetStyle().NodeBorderWidth), extraRect.GetBR(), ImGui::ColorConvertFloat4ToU32(ed::GetStyle().Colors[ed::StyleColor_NodeBorder]));


    }

    void StateMachineNodeBase::RenderPin(const NodeEditorPin& pin)
    {
        auto padding = ed::GetStyle().NodePadding;
        ImRect rectWithPadding = m_pRect;
        rectWithPadding.Min.x -= padding.x;
        rectWithPadding.Min.y -= padding.y;
        rectWithPadding.Max.x += padding.z;
        rectWithPadding.Max.y += padding.w;


        ed::PinRect(rectWithPadding.GetTL(), rectWithPadding.GetBR());

        rectWithPadding.Expand(10.0f);

        ed::PinPivotRect(rectWithPadding.GetTL(), rectWithPadding.GetBR());
    }

    void StateMachineNodeBase::PreRender()
    {
        if(m_IsDefaultState) {
            ed::PushStyleVar(ed::StyleVar_NodeBorderWidth, 3.0f);
        }
    }

    void StateMachineNodeBase::PostRender()
    {
        if(m_IsDefaultState) {
            ed::PopStyleVar();
        }
    }
}