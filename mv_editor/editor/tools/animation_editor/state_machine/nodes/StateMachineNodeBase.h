//
// Created by Matty on 29.08.2023.
//

#ifndef MV_EDITOR_STATEMACHINENODEBASE_H
#define MV_EDITOR_STATEMACHINENODEBASE_H

#include "node_editor/NodeEditorNode.h"

namespace Editor {
    namespace Config {
        static constexpr glm::vec2 sc_NodeSize = {256,36};
        static constexpr ImColor sc_NodeExtraColor = {0.f,0.361f,0.325f, 0.3f};
        static constexpr ImColor sc_InitialNodeExtraColor = {0.016f,0.161f,0.251f, 1.0f};
    }

    class StateMachineNodeBase : public NodeEditorNodeBase
    {

    protected:

        void PreRender() override;

        void PostRender() override;

        void RenderImpl(const NodeEditorContext& ctx) override;

        void InitImpl(NodeEditorId& idCounter) override;

        void AfterRenderImpl() override;

        void RenderPin(const NodeEditorPin& pin) override;


    public:

        bool m_IsDefaultState = false;

    private:

        ImColor m_pColor = Config::sc_NodeExtraColor;

    };
}


#endif //MV_EDITOR_STATEMACHINENODEBASE_H
