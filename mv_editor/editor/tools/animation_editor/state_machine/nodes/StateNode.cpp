//
// Created by Matty on 29.08.2023.
//

#include "StateNode.h"

namespace Editor
{
    void StateNode::RenderImpl(const NodeEditorContext& ctx)
    {
        auto cursor = ImGui::GetCurrentWindow()->DC.CursorPos;

        if(m_StateName.empty()) {
            ImGui::Dummy({Config::sc_NodeSize.x,Config::sc_NodeSize.y});
            StateMachineNodeBase::RenderImpl(ctx);
            return;
        }

        auto fontPrev = ImGui::GetFont()->FontSize;

        ImGui::GetFont()->FontSize = fontPrev + 1;

        auto textSize = ImGui::CalcTextSize(m_StateName.c_str());

        auto width = textSize.x > Config::sc_NodeSize.x ? textSize.x : Config::sc_NodeSize.x;

        if(width > textSize.x) {
            const auto offset = (Config::sc_NodeSize.x - textSize.x) / 2;
            ImGui::GetCurrentWindow()->DC.CursorPos.x += offset;
        }

        ImGui::TextUnformatted(m_StateName.c_str());

        ImGui::GetFont()->FontSize = fontPrev;

        ImGui::GetCurrentWindow()->DC.CursorPos = cursor;

        ImGui::Dummy({width,Config::sc_NodeSize.y});

        StateMachineNodeBase::RenderImpl(ctx);
    }
}

