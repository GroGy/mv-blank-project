//
// Created by Matty on 29.08.2023.
//

#include "AESMEditor.h"
#include "tools/animation_editor/AnimationEditor.h"
#include "tools/animation_editor/state_machine/nodes/StateNode.h"
#include "util/Container.h"

namespace Editor
{
    void AESMEditor::Render(MV::string_view label)
    {
        m_pNodeEditor->Render(label);

        m_pDirty |= m_pNodeEditor->IsDirty();

        if (ImGui::BeginDragDropTarget())
        {
            ImGuiDragDropFlags target_flags = 0;
            target_flags |= ImGuiDragDropFlags_AcceptNoDrawDefaultRect; // Don't display the yellow rectangle
            if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("MV_ANIM_ED_State", target_flags))
            {
                AnimationDragAndDropPayload pld = *(AnimationDragAndDropPayload*)payload->Data;

                pld.m_Editor->ClearDragAndDrop();

                auto anim = pld.m_Editor->GetAnimationFromID(pld.m_DragAnimId);

                if(MV::Contains(m_pNodeEditor->m_PresentStates, pld.m_DragAnimId)) {
                    ImGui::EndDragDropTarget();
                    return;
                }

                if(anim) {
                    auto node = m_pNodeEditor->AddNode<StateNode>();
                    node->m_AnimationID = pld.m_DragAnimId;
                    node->m_StateName = anim->m_Name;
                    node->m_StateID = findFirstValidId();

                    m_pNodeEditor->m_PresentStates.emplace(pld.m_DragAnimId);

                    auto& s = m_pData.m_States.emplace_back();
                    s.m_AnimationId = pld.m_DragAnimId;
                    s.m_Position = node->GetPosition();
                    s.m_ID = node->m_StateID;

                    ed::SetNodePosition(node->GetID(), ed::ScreenToCanvas(ImGui::GetMousePos()));
                }


            }
            ImGui::EndDragDropTarget();
        }

        if(m_pNodeEditor->m_OpenPendingLinkPopup) {
            ImGui::OpenPopup("##pending_link");
            m_pNodeEditor->m_OpenPendingLinkPopup = false;
        }

        drawPendingLinkPopup();
    }

    void AESMEditor::Init()
    {
        m_pNodeEditor = MV::make_rc<StateMachineNodeEditor>();
        m_pNodeEditor->Init();
        m_pNodeEditor->m_Editor = this;
    }

    void AESMEditor::drawPendingLinkPopup()
    {
        static bool wasOpen = false;
        if(ImGui::BeginPopup("##pending_link")) {
            wasOpen = true;
            ImGui::TextUnformatted("Set transition settings!");
            if(ImGui::Button("Confirm")) {
                auto id = m_pNodeEditor->ConfirmPendingLink();
                auto & tr = m_pData.m_Transitions.emplace_back();

                uint32_t maxId = 1;
                for(auto & t : m_pData.m_Transitions) {
                    if(t.m_ID >= maxId) maxId = t.m_ID+1;
                }

                tr.m_ID = maxId;

                tr.m_Type = MV::AnimationStateMachineTransitionType::OnExternal;

                m_pLinkToTransition[id] = tr.m_ID;
                ImGui::CloseCurrentPopup();
            }
            if(ImGui::Button("Cancel")) {
                m_pNodeEditor->m_PendingLink.m_Pending = false;
                ImGui::CloseCurrentPopup();
            }
            ImGui::EndPopup();
        } else {
            if(wasOpen) {
                m_pNodeEditor->m_PendingLink.m_Pending = false;
                wasOpen = false;
            }
        }
    }

    void AESMEditor::LoadData(const MV::AnimationStateMachineData& data)
    {
        m_pData = {};
        m_pNodeEditor->Clear();
        m_pStateIdPins.clear();
        m_pLinkToTransition.clear();
        m_pNodeEditor->m_PresentStates.clear();

        m_pData.m_DefaultStateID = data.m_DefaultState;

        for(auto & stateData : data.m_States) {
            auto& state = m_pData.m_States.emplace_back();
            state.m_ID = stateData.m_ID;
            state.m_AnimationId = stateData.m_AnimationID;
            state.m_Position = stateData.m_Position;
            state.m_Cancelable = stateData.m_Cancelable;
            state.m_Looping = stateData.m_Looping;

            for(auto & transitionId : stateData.m_OutTransitions) {
                state.m_OutTransitions.emplace_back(transitionId);
            }
        }

        for(auto & transitionData : data.m_Transitions) {
            auto& transition = m_pData.m_Transitions.emplace_back();
            transition.m_ID = transitionData.m_ID;
            transition.m_FromState = transitionData.m_From;
            transition.m_ToState = transitionData.m_To;

            transition.m_Type = static_cast<AESM::StateTransition::Type>(transitionData.m_Type);
        }


        {// Add nodes to editor
            for(const auto & s : m_pData.m_States) {
                auto node = m_pNodeEditor->AddNode<StateNode>();
                node->m_AnimationID = s.m_AnimationId;
                node->m_StateID = s.m_ID;
                node->m_StateName = m_pAnimationEditor->GetAnimationFromID(s.m_AnimationId)->m_Name;
                node->SetPosition(s.m_Position);
                node->m_Looping = s.m_Looping;
                node->m_Cancelable = s.m_Cancelable;
                node->m_IsDefaultState = node->m_StateID == m_pData.m_DefaultStateID;
                m_pStateIdPins[s.m_ID] = node->GetPins().front().m_Id;
            }
        }

        {// Add all links to editor
            for(const auto& t : m_pData.m_Transitions) {
                auto fromPin = m_pStateIdPins[t.m_FromState];
                auto toPin = m_pStateIdPins[t.m_ToState];

                auto& link = m_pNodeEditor->AddLink(fromPin, toPin);

                m_pLinkToTransition[link.m_Id] = t.m_ID;
            }
        }
    }

    MV::AnimationStateMachineData AESMEditor::SaveData()
    {
        MV::AnimationStateMachineData res{};

        uint32_t idCounter = 0;

        for(const auto& nodeA : m_pNodeEditor->GetNodes()) {
            auto node = MV::static_pointer_cast<StateNode>(nodeA);
            auto& state = res.m_States.emplace_back();

            state.m_ID = node->m_StateID;
            state.m_AnimationID = node->m_AnimationID;
            state.m_Position = node->GetPosition();
            state.m_Looping = node->m_Looping;
            state.m_Cancelable = node->m_Cancelable;

            for(const auto & l : m_pNodeEditor->GetLinks()) {
                if(l.m_StartPin == node->GetPins().front().m_Id) {
                    state.m_OutTransitions.emplace_back(l.m_Id);
                }
            }
        }

        for(const auto & l : m_pNodeEditor->GetLinks()) {
            auto& t = res.m_Transitions.emplace_back();
            t.m_ID = l.m_Id;

            for(auto && v : m_pData.m_Transitions) {
                if(v.m_ID == m_pLinkToTransition[l.m_Id]) {
                    t.m_Type = v.m_Type;
                    break;
                }
            }

            bool foundEnd = false;
            bool foundStart = false;
            for(const auto& nodeA : m_pNodeEditor->GetNodes()) {
                auto node = MV::static_pointer_cast<StateNode>(nodeA);
                if(!foundStart && node->GetPins().front().m_Id == l.m_StartPin) {
                    t.m_From = node->m_StateID;
                    foundStart = true;
                }

                if(!foundEnd && node->GetPins().front().m_Id == l.m_EndPin) {
                    t.m_To = node->m_StateID;
                    foundEnd = true;
                }

                if(foundEnd && foundStart) break;
            }
        }

        res.m_DefaultState = m_pData.m_DefaultStateID;

        return res;
    }

    void AESMEditor::SetEditor(AnimationEditor* editor)
    {
        m_pAnimationEditor = editor;
    }

    void AESMEditor::ClearDirty()
    {
        m_pDirty = false;
        m_pNodeEditor->ClearDirty();
    }

    uint32_t AESMEditor::findFirstValidId() const
    {
        uint32_t res = 0;

        for(const auto& nodeA : m_pNodeEditor->GetNodes()) {
            auto node = MV::static_pointer_cast<StateNode>(nodeA);
            if(node->m_StateID >= res) {
                res = node->m_StateID + 1;
            }
        }

        return res;
    }

    void AESMEditor::setDefaultNode(const MV::rc<StateNode>& node)
    {
        for(auto && n : m_pData.m_States) {
            if(n.m_ID == node->m_StateID) { // Verify that state exists
                m_pData.m_DefaultStateID = n.m_ID;
                break;
            }
        }

        for(auto && n : m_pNodeEditor->GetNodes()) {
            auto state = MV::dynamic_pointer_cast<StateNode>(n);

            ENSURE_VALID(state);

            state->m_IsDefaultState = state->m_StateID == m_pData.m_DefaultStateID;
        }

        m_pDirty = true;
    }

    void AESMEditor::drawTransitionSetting(const NodeEditorLink& link)
    {
        ImGui::TextUnformatted(fmt::format("ID: {}", link.m_Id).c_str());

        auto foundRes = m_pLinkToTransition.find(link.m_Id);

        ENSURE_TRUE(foundRes != m_pLinkToTransition.end());

        AESM::StateTransition* transition = nullptr;

        for (auto& v: m_pData.m_Transitions) {
            if(v.m_ID == foundRes->second) {
                transition = &v;
                break;
            }
        }

        ENSURE_VALID(transition);

        if(ImGui::BeginMenu("Type")) {
            if(ImGui::MenuItem("On End", nullptr, transition->m_Type == MV::AnimationStateMachineTransitionType::OnEnd)) {
                transition->m_Type = MV::AnimationStateMachineTransitionType::OnEnd;
            }
            if(ImGui::MenuItem("On Cancel", nullptr, transition->m_Type == MV::AnimationStateMachineTransitionType::OnCancel)) {
                transition->m_Type = MV::AnimationStateMachineTransitionType::OnCancel;
            }
            if(ImGui::MenuItem("Possible", nullptr, transition->m_Type == MV::AnimationStateMachineTransitionType::OnExternal)) {
                transition->m_Type = MV::AnimationStateMachineTransitionType::OnExternal;
            }

            ImGui::EndMenu();
        }

        if(ImGui::MenuItem("Delete")) {

        }

    }
}
