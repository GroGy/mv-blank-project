//
// Created by Matty on 29.08.2023.
//

#include "StateMachineNodeEditor.h"
#include "tools/animation_editor/state_machine/nodes/StateNode.h"
#include "util/UtilGui.h"
#include "AESMEditor.h"

namespace Editor
{
    namespace ed = ax::NodeEditor;

    void StateMachineNodeEditor::RenderImpl()
    {
        m_pDrawContext.m_EnablePinSelect = ImGui::IsKeyDown(ImGuiKey_ModShift);
    }

    void StateMachineNodeEditor::InitNodeTypes()
    {

    }

    void StateMachineNodeEditor::ClearImpl()
    {

    }

    void StateMachineNodeEditor::InitImpl()
    {
        NodeEditorBase::InitImpl();
    }

    static bool disabledStarted = false;

    void StateMachineNodeEditor::PreRender()
    {
        ed::PushStyleVar(ed::StyleVar_LinkStrength, 0.f); // Makes lines straight
        ed::PushStyleVar(ed::StyleVar_DragLinkEndArrowSize, 10.0f); // Makes arrow on dragging link visible
        ed::PushStyleVar(ed::StyleVar_DragLinkEndArrowWidth, 10.0f); // Makes arrow on dragging link visible
        ed::PushStyleVar(ed::StyleVar_LinkEndArrowSizeOverride, 10.0f); // Makes arrow on present links visible
        ed::PushStyleVar(ed::StyleVar_LinkEndArrowWidthOverride, 10.0f); // Makes arrow on present links visible
        ed::PushStyleVar(ed::StyleVar_EnableMultiDirLinks, true);

        disabledStarted = false;
        if(m_PendingLink.m_Pending) {
            disabledStarted = true;
            ImGui::BeginDisabled();
        }
    }

    void StateMachineNodeEditor::PostRender()
    {
        ed::PopStyleVar(6);
        if(disabledStarted) {
            ImGui::EndDisabled();
        }
    }

    bool StateMachineNodeEditor::ConfirmLink(NodeEditorId beginPin, NodeEditorId endPin)
    {
        if(m_PendingLink.m_Pending) return false;

        m_PendingLink.m_StartPin = beginPin;
        m_PendingLink.m_EndPin = endPin;
        m_PendingLink.m_Pending = true;

        m_OpenPendingLinkPopup = true;

        return false;
    }

    void StateMachineNodeEditor::OnNodeAdded(const MV::rc<NodeEditorNodeBase>& node)
    {
        const auto& pins = node->GetPins();

        // State machine only contains state nodes, these nodes have only single pin

        m_PinAnimationIds[pins.front().m_Id] = MV::static_pointer_cast<StateNode>(node)->m_AnimationID;
    }

    void StateMachineNodeEditor::OnNodeRemoved(const MV::rc<NodeEditorNodeBase>& node)
    {
        const auto& pins = node->GetPins();

        m_PinAnimationIds.erase(pins.front().m_Id);
    }

    NodeEditorId StateMachineNodeEditor::ConfirmPendingLink()
    {
        m_PendingLink.m_Pending = false;
        return AddLink(m_PendingLink.m_StartPin, m_PendingLink.m_EndPin).m_Id;
    }

    void StateMachineNodeEditor::DrawNodeContext(const MV::rc<NodeEditorNodeBase>& node)
    {
        auto stateNode = MV::dynamic_pointer_cast<StateNode>(node);

        if(!stateNode) return;

        if(ImGui::MenuItem("Set as default")) {
            m_Editor->setDefaultNode(stateNode);
        }

        ImGui::MenuItem("Loops", nullptr, &stateNode->m_Looping);
        ImGui::MenuItem("Cancelable", nullptr, &stateNode->m_Cancelable);
    }

    bool StateMachineNodeEditor::EnableNodeContextMenu()
    {
        return true;
    }

    bool StateMachineNodeEditor::EnableLinkContextMenu()
    {
        return true;
    }

    void StateMachineNodeEditor::DrawLinkContext(const NodeEditorLink& link)
    {
        if(m_Editor) m_Editor->drawTransitionSetting(link);
    }
}