//
// Created by Matty on 21.08.2023.
//

#ifndef MV_EDITOR_ANIMATIONEDITORGIZMOPROVIDER_H
#define MV_EDITOR_ANIMATIONEDITORGIZMOPROVIDER_H

#include "interface/common/viewport_gizmo_providers/WorldObjectGizmoProvider.h"

namespace Editor {
    class AnimationEditorGizmoProvider : public ActiveWorldWorldObjectGizmoProvider
    {
    public:

        using OnDataChangedCallback = eastl::function<void(const MV::rc<MV::WorldObject>&)>;

    public:

        AnimationEditorGizmoProvider(const MV::rc<MV::WorldObject>& wo, MV::World& world);

    public:

        void OnDataChanged() override;

        void SetPosition(const glm::vec2& position) override;

        void SetRotation(float rotation) override;

        void SetScale(const glm::vec2& scale) override;

        void SetTransform(const glm::mat4& transform) override;

        const MV::unordered_map<MV::string, MV::Meta::Field>* GetWorldObjectFieldsOverride() const override;

    public:

        OnDataChangedCallback m_OnDataChangedCallback;

        MV::unordered_map<MV::string, MV::Meta::Field> m_OverrideFields;

    };
}


#endif //MV_EDITOR_ANIMATIONEDITORGIZMOPROVIDER_H
