//
// Created by Matty on 21.08.2023.
//

#include "AnimationEditorGizmoProvider.h"

namespace Editor {
    AnimationEditorGizmoProvider::AnimationEditorGizmoProvider(const MV::rc<MV::WorldObject>& wo, MV::World& world) : ActiveWorldWorldObjectGizmoProvider(wo, world, false)
    {

    }

    void AnimationEditorGizmoProvider::OnDataChanged()
    {
        ActiveWorldWorldObjectGizmoProvider::OnDataChanged();

        if(m_OnDataChangedCallback) m_OnDataChangedCallback(m_pWorldObject);
    }

    void AnimationEditorGizmoProvider::SetPosition(const glm::vec2& position)
    {
    }

    void AnimationEditorGizmoProvider::SetRotation(float rotation)
    {
    }

    void AnimationEditorGizmoProvider::SetScale(const glm::vec2& scale)
    {
    }

    void AnimationEditorGizmoProvider::SetTransform(const glm::mat4& transform)
    {
    }

    const MV::unordered_map<MV::string, MV::Meta::Field>*
    AnimationEditorGizmoProvider::GetWorldObjectFieldsOverride() const
    {
        return &m_OverrideFields;
    }
}
