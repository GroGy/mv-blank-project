//
// Created by Matty on 19.08.2023.
//

#ifndef MV_EDITOR_ANIMATIONEDITOR_H
#define MV_EDITOR_ANIMATIONEDITOR_H

#include <EngineMinimal.h>
#include "EditorMinimal.h"
#include "interface/worldobject_renderers/PropertyRendererBase.h"
#include "interface/common/Window.h"
#include "resources/AnimationData.h"
#include "common/World.h"
#include "world_picking/ViewportMousePickingImpl.h"
#include "interface/common/ViewportWidget.h"
#include "animation/AnimationInput.h"
#include "precompiler/AnimatablePropertyRegistrator.h"
#include "tools/animation_editor/state_machine/AESMEditor.h"
#include "resources/common/AnimationAssetType.h"

namespace MV {
    class AnimationResource;
    class AnimationProperty;
}

namespace Editor
{
    class AnimationEditor;
    class AnimationEditorGizmoProvider;

    namespace Config
    {
        static constexpr MV::string_view sc_AnimationEditorWindowName = "Animation Editor";
    }

    class AnimationEditorWorld : public MV::World
    {
    public:
        AnimationEditorWorld() = default;
        explicit AnimationEditorWorld(MV::WorldData && worldData) {
            m_pTickingEnabled = false;
            m_pIsRealWorld = true;
            m_WorldData = MV::move(worldData);
            loadFromData();
        };
    };

    struct AnimationInputData {
        MV::string m_Name;
        MV::string m_InternalName;
        uint32_t m_ID; // Different ID from saved one, this one is used for editor purposes

        MV::vector<MV::AnimationInputField> m_Inputs;

        MV::Meta::Class m_PropertyClass;

        int32_t m_Index = -1;
    };

    struct SequencerData {
        struct TimelineDataGroup {
            struct TimelineData {
                using Keyframe = MV::AnimationKeyframeData;

                uint32_t m_InputId;
                uint32_t m_InputFieldIndex;
                uint64_t m_InputFieldUUID;

                MV::string m_Name;

                MV::vector<Keyframe> m_Keyframes;
            };

            MV::vector<TimelineData> m_Timelines;

            MV::unordered_set<MV::Archive::ValueId> m_EnabledFields;

            MV::string m_Name;
            uint32_t m_InputIndex;
            bool m_Open = false;
        };

        int32_t m_SelectedTimelineGroup = -1;

        int32_t m_SelectedFrame = 0;

        int32_t m_PreviewFrame = 0;

        glm::ivec2 m_View = {0,32};
        double m_LastAddedTime = 0.0;
        double m_Time = 0.0;
        bool m_Playing = false;

        float m_FrameDuration = 1.0f / 12.0f;

        glm::ivec2 m_AnimationRange = {0,32};

        MV::vector<TimelineDataGroup> m_Groups;

        bool m_Dirty = false;
    };


    struct AnimationDragAndDropPayload {
        uint32_t m_DragAnimId = 0;
        AnimationEditor* m_Editor = nullptr;
    };


    // Inherits from PropertyRendererBase for easy rendering of settings
    class [[MV::Class(EditorWindow)]] AnimationEditor : public Window, public IPropertyRendererHelper
    {
        enum class DataState {
            NoData,
            Loading,
            HasData
        };

        enum class TrySelectSequenceResult {
            Selected,
            Delayed,
            Failed
        };
    public:

        AnimationEditor();

    public:

        void LoadData(MV::AssetReference<MV::AnimationAssetType> animationAsset);

    public:

        void ClearDragAndDrop();

        const  MV::SingleAnimationData* GetAnimationFromID(uint32_t id) const;

    protected:

        void WindowSetup() override;

        void Render() override;

        void PostRender() override;

        void OnWorldRender() override;

        void OnOpenStateChanged(bool newValue) override;

    public:

        void OnCleanup() override;

        void OnInit() override;

    public:

        bool m_AssetChanged = false;

    private:

        void setupDockspace();

        void drawSettings();

        void drawStateMachineEditor();

        void drawSequenceEditor();

        void drawEditors();

        void drawMenuBar();

        void drawSettingsSequences(bool& dirty);

        void drawAnimationInputs();

        void drawAllSequences();

        void drawInputPopup();

        void drawDragAndDropState();

    private:

        void addSequence(MV::string_view name, bool open = true);

        TrySelectSequenceResult tryToSelectSequence(const MV::SingleAnimationData& sequenceData);

        void createPreviewWorld();

        void addAnimationInput(const MV::RegisteredAnimatableProperties::PropertyData& data);

        void addFieldToSequence(uint32_t inputIndex, const AnimationInputData& input, uint32_t fieldIndex,
                                const MV::AnimationInputField& field);

        void updateActiveSequence();

        void addInputPropertyToWorld(AnimationInputData& data, MV::Meta::Class propertyClass);

        void saveData();

        void loadDataImpl(MV::AssetReference<MV::AnimationAssetType> asset);

        void updateResources();

        void loadEditorDataFromLoadedAsset();

        void saveEditorDataToAssetData();

        void saveSelectedSequence();

        void createKeyframe(MV::AnimationInputField* res);

        FORCEINLINE bool isDirty() const { return m_AssetChanged || (m_pSelectedSequenceIndex != -1 && m_pSequencerData.m_Dirty) || (m_pStateMachineEditor && m_pStateMachineEditor->IsDirty()); }

    private: // State machine editor

        MV::rc<AESMEditor> m_pStateMachineEditor;

    private: // Data

        DataState m_pDataState = DataState::NoData;

        MV::AssetReference<MV::AnimationAssetType> m_pAsset = MV::InvalidUUID;

        MV::rc<MV::AnimationResource> m_pResource;

        MV::AnimationData m_pData;

        int32_t m_pSelectedSequenceIndex = -1;

        MV::vector<AnimationInputData> m_Inputs;

    private: // Flags for UI

        bool m_pFocusSequencer = false;

        bool m_pOpenEditInputPopup = false;

    private: // Active sequencer data

        SequencerData m_pSequencerData;

        AnimationEditorWorld m_pWorld;

        MV::WorldData m_pWorldData;

        ViewportWidget<MousePickingBehavior::Disabled> m_pViewport;

        MV::rc<AnimationEditorGizmoProvider> m_pGizmoProvider;

        MV::AnimationProperty* m_pAnimationProperty = nullptr;

    private: // Drag and Drop

        uint32_t m_pDraggingAnimationId = 0;

        bool m_pJustStartedDragging = false;

    };
}

#endif //MV_EDITOR_ANIMATIONEDITOR_H
