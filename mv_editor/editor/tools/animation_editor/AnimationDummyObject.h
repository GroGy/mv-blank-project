//
// Created by Matty on 28.06.2024.
//

#ifndef MV_EDITOR_ANIMATIONDUMMYOBJECT_H
#define MV_EDITOR_ANIMATIONDUMMYOBJECT_H

#include "common/WorldObject.h"

namespace Editor
{
    class [[MV::Class()]] AnimationDummyObject : public MV::WorldObject
    {
    public:
        explicit AnimationDummyObject(const MV::ObjectInitializationContext& worldInitContext);
    };
}


#endif //MV_EDITOR_ANIMATIONDUMMYOBJECT_H
