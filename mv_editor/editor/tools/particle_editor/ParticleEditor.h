//
// Created by Matty on 09.04.2023.
//

#ifndef MV_EDITOR_PARTICLEEDITOR_H
#define MV_EDITOR_PARTICLEEDITOR_H

#include "common/WorldObject.h"
#include "common/camera/Camera.h"
#include "interface/common/ViewportWidget.h"
#include "interface/common/Window.h"
#include "common/World.h"
#include "particles/cpu/ParticleSystemProperty.h"
#include "resources/WorldData.h"
#include "tools/particle_editor/ParticleEditorDummy.h"

namespace Editor
{
    class ParticleEditorWorld : public MV::World
    {
    public:
        ParticleEditorWorld() = default;
        explicit ParticleEditorWorld(MV::WorldData && worldData) {
            m_pTickingEnabled = false;
            m_pIsRealWorld = false;
            m_WorldData = MV::move(worldData);
            loadFromData();
        };
    };

    namespace Config {
        static constexpr MV::string_view sc_ParticleEditorWindowName = "Particle editor";
    }

    using OnSavedCallback = eastl::function<void(void)>;

    class [[MV::Class(EditorWindow)]] ParticleEditor : public Window
    {
    public: // Types

        enum class DataState {
            NoData,
            HasData
        };

    public: // CTor and public overrides

        ParticleEditor();

        void OnCleanup() override;

        void OnInit() override;

        void Update(float deltaTime) override;

        void OnWorldRender() override;

    public: // Public API

        void SetParticleSystem(MV::World * world, MV::rc<MV::WorldObject> wo,const MV::Meta::Field& field);

        void SetOnSavedCallback(const OnSavedCallback & callback);

    protected: // Overrides

        void WindowSetup() override;

        void Render() override;

        void PostRender() override;

        bool ShouldRender() const override;

        void NoRender() override;

        void OnOpenStateChanged(bool newValue) override;

    private: // Helper functions

        void drawSettings();

        void setupDockspace();

        void drawMenubar();

        void saveToSource();

    private: // Internal fields

        ParticleEditorWorld m_pWorld;

        MV::WorldData m_pWorldData;

        ViewportWidget<MousePickingBehavior::Disabled> m_pViewport;

        MV::World * m_pSourceWorld = nullptr;

        MV::rc<MV::WorldObject> m_pSourceWorldObject;

        DataState m_pDataState = DataState::NoData;

        MV::Meta::Field m_pSourceField;
        MV::Meta::Field m_pParticleSystemField;

        uint32_t m_pBurstSize = 32;

        bool m_pUpdateSystem = true;

        bool m_pDirty = false;

        OnSavedCallback m_pOnSavedCallback;

        MV::rc<ParticleEditorDummy> m_pDummy;

        MV::Meta::Class m_pDummyMeta;
    };
}


#endif //MV_EDITOR_PARTICLEEDITOR_H
