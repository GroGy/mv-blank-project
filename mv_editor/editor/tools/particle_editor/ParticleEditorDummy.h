//
// Created by matty on 19.06.2024.
//

#ifndef MV_EDITOR_PARTICLEEDITORDUMMY_H
#define MV_EDITOR_PARTICLEEDITORDUMMY_H

#include "common/WorldObject.h"
#include "particles/cpu/ParticleSystemProperty.h"
#include <EngineMinimal.h>

namespace Editor {
    class [[MV::Class()]] ParticleEditorDummy : public MV::WorldObject {
    public:
        explicit ParticleEditorDummy(const MV::ObjectInitializationContext &world_init_context);

        [[MV::Field(Property)]]
        MV::ParticleSystemProperty* Particles = nullptr;

    };
}


#endif //MV_EDITOR_PARTICLEEDITORDUMMY_H
