//
// Created by Matty on 12.02.2023.
//

#ifndef MV_EDITOR_SCRIPTABLECONFIGEDITOR_H
#define MV_EDITOR_SCRIPTABLECONFIGEDITOR_H

#include "interface/common/ViewportWidget.h"
#include "interface/common/Window.h"
#include "resources/WorldData.h"
#include "common/WorldObject.h"
#include "common/ScriptableConfig.h"
#include "resources/ScriptableConfigData.h"
#include "resources/ScriptableConfigResource.h"
#include "common/World.h"
#include "imgui/imgui.h"
#include "world_picking/ViewportMousePickingImpl.h"
#include "meta/Class.h"
#include "resources/common/ScriptConfigAssetType.h"

namespace Editor
{
    class ScriptableConfigWorld : public MV::World
    {
    public:
        ScriptableConfigWorld() = default;
        explicit ScriptableConfigWorld(MV::WorldData && worldData) {
            m_pTickingEnabled = false;
            m_pIsRealWorld = false;
            m_WorldData = MV::move(worldData);
            loadFromData();
        };
    };

    namespace Config
    {
        static constexpr MV::string_view sc_ScriptableConfigEditorWindowName = "Scriptable config editor";
    }

    class [[MV::Class(EditorWindow)]] ScriptableConfigEditor : public Window
    {
    public: // overrides

        ScriptableConfigEditor();

        ~ScriptableConfigEditor();

        void WindowSetup() override;

        void Render() override;

        void PostRender() override;

        void OnCleanup() override;

        void OnInit() override;

        void OnOpenStateChanged(bool newValue) override;

    public: // External API

        void LoadScriptableConfig(MV::AssetReference<MV::ScriptConfigAssetType> config);

        void OnResize();

    private: // ImGui drawing functions

        void drawMenuBar();

        void drawContent();

    public:

        void OnWorldRender() override;

    private:
        void drawLoadingDataContent();

        void drawNoDataContent();

    private: // Drawing parts

        void drawPhysicsSettings(bool& dirty);

        void drawFields(bool& dirty);

        void drawPropertySettings(bool& dirty);

        void drawInfo();

    private: // Data processing

        void checkLoadTask();

        void createConfigWO();

        void clearData();

    private: // Internal fields

        enum class DataState
        {
            NoData,
            LoadingData,
            HasData
        };

        DataState m_pDataState = DataState::NoData;

        MV::AssetReference<MV::ScriptConfigAssetType> m_pConfigAsset = MV::InvalidUUID;

        MV::rc<MV::ScriptableConfigResource> m_pResource;

        MV::ScriptableConfigData m_pConfig;

        bool m_pDataDirty = false;

        // Fake world data, used in drawing callbacks
        MV::WorldData m_pFakeWorldData;

        ScriptableConfigWorld m_pFakeWorld;

        MV::rc<MV::WorldObject> m_pWO;

        MV::Meta::Class m_pClass;

        ViewportWidget<MousePickingBehavior::Disabled> m_pViewport;

        void saveData();
    };
}


#endif //MV_EDITOR_SCRIPTABLECONFIGEDITOR_H
