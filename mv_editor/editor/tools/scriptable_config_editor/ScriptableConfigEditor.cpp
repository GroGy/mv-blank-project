//
// Created by Matty on 12.02.2023.
//

#include <filesystem>
#include "ScriptableConfigEditor.h"
#include "imgui/imgui.h"
#include "interface/object_editor/common/FieldRenderer.h"
#include "interface/object_editor/GeneralFieldRenderers.h"
#include "common/World.h"
#include "interface/object_editor/NumericFieldRenderers.h"
#include "resources/ScriptableConfigProducer.h"
#include "util/UtilGui.h"
#include "common/EngineCommon.h"
#include "interface/object_editor/PropertyFieldRenderer.h"
#include "imgui/imgui_internal.h"
#include "imgui/imgui_impl_vulkan.h"
#include "render/vulkan/VulkanTextureImpl.h"
#include "util/UtilWorld.h"
#include "Engine.h"
#include "interface/common/viewport_gizmo_providers/WorldObjectGizmoProvider.h"

namespace Editor
{
    ScriptableConfigEditor::ScriptableConfigEditor() : Window(Config::sc_ScriptableConfigEditorWindowName.data(), false)
    {
        m_pOpen = false;
        m_Flags |= ImGuiWindowFlags_MenuBar;
    }

    void ScriptableConfigEditor::WindowSetup()
    {

    }


    static constexpr MV::string_view sc_ViewportWindowName = "##scriptable_editor_dockspace_viewport";
    static constexpr MV::string_view sc_SettingsWindowName = "##scriptable_editor_dockspace_settings";

    void ScriptableConfigEditor::Render()
    {
        checkLoadTask();
        drawMenuBar();

        auto centerNode = ImGui::GetID("##scriptable_editor_dockspace");
        ImGui::DockSpace(centerNode);

        if (ImGui::DockBuilderGetNode(centerNode)->IsLeafNode())
        {
            ImGuiID viewport_nodeId;
            ImGuiID settings_nodeId;
            ImGui::DockBuilderSplitNode(centerNode, ImGuiDir_Right, 0.3f, &settings_nodeId, &viewport_nodeId);

            const auto viewport_node = ImGui::DockBuilderGetNode(viewport_nodeId);
            viewport_node->LocalFlags |=
                    static_cast<ImGuiDockNodeFlags_>(ImGuiDockNodeFlags_NoDocking | ImGuiDockNodeFlags_NoTabBar) |
                    ImGuiDockNodeFlags_NoSplit;

            const auto settings_node = ImGui::DockBuilderGetNode(settings_nodeId);
            settings_node->LocalFlags |=
                    static_cast<ImGuiDockNodeFlags_>(ImGuiDockNodeFlags_NoDocking | ImGuiDockNodeFlags_NoTabBar) |
                    ImGuiDockNodeFlags_NoSplit;

            ImGui::DockBuilderDockWindow(sc_ViewportWindowName.data(), viewport_nodeId);
            ImGui::DockBuilderDockWindow(sc_SettingsWindowName.data(), settings_nodeId);

            ImGui::DockBuilderFinish(viewport_nodeId);
            ImGui::DockBuilderFinish(settings_nodeId);
        }

        {
            if(m_pFakeWorld.m_CameraOverride)
                m_pViewport.Draw(sc_ViewportWindowName.data(),m_pFakeWorld.m_CameraOverride);
        }

        if (ImGui::Begin(sc_SettingsWindowName.data()))
        {
            switch (m_pDataState)
            {
                case DataState::NoData:
                {
                    drawNoDataContent();
                    break;
                }
                case DataState::LoadingData:
                {
                    drawLoadingDataContent();
                    break;
                }
                case DataState::HasData:
                {
                    drawContent();
                    break;
                }
            }
        }
        ImGui::End();

    }

    void ScriptableConfigEditor::PostRender()
    {

    }

    void ScriptableConfigEditor::OnCleanup()
    {

    }

    void ScriptableConfigEditor::OnInit()
    {

    }

    void ScriptableConfigEditor::LoadScriptableConfig(MV::AssetReference<MV::ScriptConfigAssetType> config)
    {
        m_pFakeWorldData = MV::WorldData{};

        m_pConfigAsset = config;
        auto& configs = MV::GetResources()->m_ScriptableConfigs;

        if (MV::GetEngine()->m_ResourceManager.IsAssetLoaded(m_pConfigAsset))
        {
            ENSURE_TRUE(configs.find(config) != configs.end());

            m_pConfig = configs[m_pConfigAsset].ToData();

            m_pDataState = DataState::HasData;

            createConfigWO();
        } else
        {
            m_pResource.reset();

            m_pResource = MV::dynamic_pointer_cast<MV::ScriptableConfigResource>(
                    MV::GetEngine()->m_ResourceManager.LoadResource(m_pConfigAsset));

            m_pDataState = DataState::LoadingData;
        }

        m_pOpen = true;
    }

    void ScriptableConfigEditor::drawMenuBar()
    {
        if (ImGui::BeginMenuBar())
        {
            if (ImGui::BeginMenu("File"))
            {
                if (ImGui::MenuItem("Save"))
                {
                    saveData();
                }

                ImGui::EndMenu();
            }

            ImGui::EndMenuBar();
        }
    }

    void ScriptableConfigEditor::drawContent()
    {
        bool dirty = false;

        ImGui2::LabeledSeparator("Info");

        drawInfo();

        ImGui2::LabeledSeparator("Physics");

        drawPhysicsSettings(dirty);

        ImGui2::LabeledSeparator("Script fields");

        drawFields(dirty);

        ImGui2::LabeledSeparator("Properties");

        drawPropertySettings(dirty);

        m_pDataDirty |= dirty;
    }

    void ScriptableConfigEditor::drawLoadingDataContent()
    {
        ImGui::TextUnformatted("Loading data");
    }

    void ScriptableConfigEditor::drawNoDataContent()
    {
        ImGui::TextUnformatted("No data");
    }

    void ScriptableConfigEditor::drawPhysicsSettings(bool& dirty)
    {
        if (ImGui::BeginTable("##sc_physics_fields", 2, ImGuiTableFlags_NoSavedSettings))
        {
            ImGui::TableNextRow();

            {
                static EngineInternalFieldRendererHelper static_frh{
                        [&](MV::string_view fieldname_, const MV::rc<MV::EngineObject>& obj_, const MV::Meta::Class& klass)
                        {
                            auto wo_ = MV::static_pointer_cast<MV::WorldObject>(obj_);

                            const bool changed = FieldDrawingImpl::DrawBool(fmt::format("##{}", fieldname_).c_str(),
                                                                            wo_->m_IsStatic);

                            if (MV::GetEngine()->IsGameRunning() && wo_->GetPhysicsBody())
                                wo_->GetPhysicsBody()->SetType(wo_->m_IsStatic ? b2_staticBody : b2_dynamicBody);

                            return changed;
                        }};
                dirty |= static_frh.Draw("Static body", m_pWO, {});
            }

            {
                static EngineInternalFieldRendererHelper fixed_rotation_frh{
                        [&](MV::string_view fieldname_, const MV::rc<MV::EngineObject>& obj_, const MV::Meta::Class& klass)
                        {
                            auto wo_ = MV::static_pointer_cast<MV::WorldObject>(obj_);
                            const bool changed = FieldDrawingImpl::DrawBool(fmt::format("##{}", fieldname_).c_str(),
                                                                            wo_->m_FixedRotation);

                            if (MV::GetEngine()->IsGameRunning() && wo_->GetPhysicsBody())
                                wo_->GetPhysicsBody()->SetFixedRotation(wo_->m_FixedRotation);

                            return changed;
                        }};
                dirty |= fixed_rotation_frh.Draw("Fixed rotation", m_pWO, {});
            }

            {
                static EngineInternalFieldRendererHelper linearDampening_frh{
                        [&](MV::string_view fieldname_, const MV::rc<MV::EngineObject>& obj_, const MV::Meta::Class& klass)
                        {
                            auto wo_ = MV::static_pointer_cast<MV::WorldObject>(obj_);
                            const bool changed = FieldDrawingImpl::DrawFloatWithOverride(fmt::format("##{}", fieldname_).c_str(),
                                                                                         wo_->m_LinearDamping, wo_->m_OverrideLinearDamping);

                            if (MV::GetEngine()->IsGameRunning() && wo_->GetPhysicsBody())
                                wo_->GetPhysicsBody()->SetLinearDamping(wo_->m_LinearDamping);

                            return changed;
                        }};

                dirty |= linearDampening_frh.Draw("Linear Dampening", m_pWO, {});
            }

            {
                static EngineInternalFieldRendererHelper angularDampening_frh{
                        [&](MV::string_view fieldname_, const MV::rc<MV::EngineObject>& obj_, const MV::Meta::Class& klass)
                        {
                            auto wo_ = MV::static_pointer_cast<MV::WorldObject>(obj_);
                            const bool changed = FieldDrawingImpl::DrawFloatWithOverride(fmt::format("##{}", fieldname_).c_str(),
                                                                                         wo_->m_AngularDamping, wo_->m_OverrideAngularDamping);

                            if (MV::GetEngine()->IsGameRunning() && wo_->GetPhysicsBody())
                                wo_->GetPhysicsBody()->SetAngularDamping(wo_->m_AngularDamping);

                            return changed;
                        }};

                dirty |= angularDampening_frh.Draw("Angular Dampening", m_pWO, {});
            }

            ImGui::EndTable();
        }
    }

    void ScriptableConfigEditor::drawFields(bool& dirty)
    {
        if (ImGui::BeginTable("##sc_script_fields", 2, ImGuiTableFlags_NoSavedSettings))
        {
            ImGui::TableNextRow();

            uint32_t counter = 0;

            for (auto& v: m_pClass.GetFields())
            {
                if(v.m_Type == MV::Meta::FieldType::Property) {
                    continue;
                }

                dirty |= (FieldRendererProxy::Draw(v, counter, m_pWO, m_pClass));
            }

            ImGui::EndTable();
        }
    }

    void ScriptableConfigEditor::drawPropertySettings(bool& dirty)
    {
        uint32_t counter = 0;

        bool first = true;
        for (auto& v: m_pClass.GetFields())
        {
            if(v.m_Type != MV::Meta::FieldType::Property) {
                continue;
            }

            if(first) {
                first = false;
            } else {
                ImGui::Separator();
            }
            dirty |= (FieldRenderer<PropertyType>::DrawProperty(EditorPropertyDrawingData::FromField(v), counter, m_pWO, m_pWO.get(), m_pClass));
        }
    }

    static void DrawStringField(MV::string_view label, MV::string_view value)
    {

        if (!ImGui::TableSetColumnIndex(0))
            return;

        ImGui::TextUnformatted(label.data());

        if (!ImGui::TableSetColumnIndex(1))
            return;

        ImGui::TextUnformatted(value.data());

        ImGui::TableNextRow();
    };

    void ScriptableConfigEditor::drawInfo()
    {
        if (ImGui::BeginTable("##sc_info_fields", 2, ImGuiTableFlags_NoSavedSettings))
        {
            ImGui::TableNextRow();

            DrawStringField("Config for class:", "TODO");

            ImGui::EndTable();
        }
    }

    void ScriptableConfigEditor::checkLoadTask()
    {
        if (m_pDataState != DataState::LoadingData) return;

        if (!m_pResource) return;

        if (m_pResource->IsFinished())
        {
            if (m_pResource->Success())
            {
                auto& configs = MV::GetResources()->m_ScriptableConfigs;
                ENSURE_TRUE(configs.find(m_pConfigAsset) != configs.end());

                m_pConfig = configs[m_pConfigAsset].ToData();

                m_pDataState = DataState::HasData;

                createConfigWO();
            } else
            {
                m_pDataState = DataState::NoData;
            }
            m_pResource.reset();
        }


    }

    void ScriptableConfigEditor::createConfigWO()
    {
        if (m_pFakeWorld.m_CameraOverride)
        {
            m_pFakeWorld.m_CameraOverride->Cleanup();
            m_pFakeWorld.m_CameraOverride.reset();
        }

        if(m_pFakeWorld.IsValid())
            m_pFakeWorld.Cleanup();

        m_pFakeWorld = ScriptableConfigWorld{MV::WorldData{m_pFakeWorldData}};

        m_pFakeWorld.m_CameraOverride = MV::make_rc<MV::Camera>();
        m_pFakeWorld.m_CameraOverride->Init();

        MV::WorldObjectSpawnParams params{};

        params.m_Name = "World object config";
        params.m_SkipSerialization = false;

        auto meta = MV::Meta::MetaCache::GetInstance().GetClassMetaFromHash(m_pConfig.m_ClassHash);

        if (!meta.IsValid())
        {
            return;
        }

        m_pClass = MV::move(meta);

        params.m_Class = m_pClass;
        params.m_LoadType = MV::WorldObjectSpawnParams::FromData;
        params.m_BaseData = m_pConfig.m_Data.Copy();

        m_pWO = m_pFakeWorld.SpawnWorldObject(params);

        m_pViewport.SetGizmoProvider(
                MV::make_rc<ActiveWorldWorldObjectGizmoProvider>(m_pWO, m_pFakeWorld));
    }

    void ScriptableConfigEditor::OnResize()
    {
        m_pViewport.OnResize();
    }

    void ScriptableConfigEditor::OnWorldRender()
    {
        if (m_pDataState != DataState::HasData) return;

        m_pFakeWorld.Render();
    }

    void ScriptableConfigEditor::OnOpenStateChanged(bool newValue)
    {
        if(!newValue && m_pDataState != DataState::NoData) {
            clearData();
        }
    }

    ScriptableConfigEditor::~ScriptableConfigEditor()
    {
    }

    void ScriptableConfigEditor::clearData()
    {
        m_pDataState = DataState::NoData;

        if (m_pFakeWorld.m_CameraOverride)
        {
            m_pFakeWorld.m_CameraOverride->Cleanup();
            m_pFakeWorld.m_CameraOverride.reset();
        }

        if(m_pFakeWorld.IsValid())
            m_pFakeWorld.Cleanup();
    }

    MV::string PathProjectRelativeToAbsolute(MV::string_view pathRel) {
        return (std::filesystem::path(MV::GetEngine()->GetProject().m_Path.c_str()) /= pathRel.data()).string().c_str();
    }

    void ScriptableConfigEditor::saveData()
    {
        auto & wo = m_pFakeWorld.m_WorldObjects[1];

        MV::Archive ar;
        MV::SerializationWhitelist wl;

        const auto writeRes = MV::IClassSerializer::WriteTypeFromHash(wo.get(), m_pClass, ar, wl);

        if(!writeRes) {
            ENSURE_NO_ENTRY();
        }

        m_pConfig.m_Data = MV::move(ar);

        const auto& project = MV::GetEngine()->GetProject();

        auto assetInfo = project.m_Assets.find(m_pConfigAsset);

        auto res = ScriptableConfigProducer::CreateConfigResource(m_pConfig, PathProjectRelativeToAbsolute(assetInfo->second.m_Path), true);

        MV::GetEngine()->m_ResourceManager.UnloadAsset(m_pConfigAsset);

        m_pDataDirty = false;
    }
}