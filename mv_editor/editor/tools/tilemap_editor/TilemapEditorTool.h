//
// Created by Matty on 08.12.2022.
//

#ifndef MV_EDITOR_TILEMAPEDITORTOOL_H
#define MV_EDITOR_TILEMAPEDITORTOOL_H

#include <EngineMinimal.h>
#include "imgui/imgui.h"
#include "resources/TilemapData.h"

namespace Editor {
    struct TilemapEditorToolContext {
        bool m_LockScrollInput = false;
        ImVec2 m_WindowMin;
        ImVec2 m_WindowMax;
        float m_TileSizePx;
        glm::vec2 m_CameraPosition;

        MV::TilemapData & m_Data;
        MV::vector<glm::uvec2> & m_Highlight;

        uint32_t m_SelectedTile;

        bool & m_TilemapDirty;

        TilemapEditorToolContext(MV::TilemapData &data, MV::vector<glm::uvec2> & highlight, bool & tilemapDirty);
    };

    class [[register_tilemap_editor_tool()]] TilemapEditorTool {
    public:
        virtual MV::string_view GetName() = 0;

        virtual MV::string_view GetIconGlyph() = 0;

        virtual void Update(TilemapEditorToolContext & context) {};

        virtual void OnDeselect() {};

        virtual void OnSelect() {};
    };
}


#endif //MV_EDITOR_TILEMAPEDITORTOOL_H
