//
// Created by Matty on 10.12.2022.
//

#ifndef MV_EDITOR_BRUSHTILEMAPEDITORTOOL_H
#define MV_EDITOR_BRUSHTILEMAPEDITORTOOL_H

#include "tools/tilemap_editor/TilemapEditorTool.h"

namespace Editor {
    class IBrushTilemapEditorTool : public TilemapEditorTool {
    public: // Overrides

        MV::string_view GetName() override;

        MV::string_view GetIconGlyph() override;

        void Update(TilemapEditorToolContext &context) override;

        void OnDeselect() override;

        void OnSelect() override;

    private: // Helper functions

        bool shouldPlaceTiles() const;

        void updateRadius(TilemapEditorToolContext &context);

        void generateHighlight(TilemapEditorToolContext &context);

        void processInput(TilemapEditorToolContext &context);

    private:

        int32_t m_pRadius = 0;

    protected:

        bool m_pIsEraser = false;
    };

    template<bool Eraser>
    class BrushTilemapEditorToolImpl : public IBrushTilemapEditorTool {
    public:
        BrushTilemapEditorToolImpl() {
            m_pIsEraser = Eraser;
        }
    };

    using BrushTilemapEditorTool = BrushTilemapEditorToolImpl<false>;
    using EraserTilemapEditorTool = BrushTilemapEditorToolImpl<true>;
}


#endif //MV_EDITOR_BRUSHTILEMAPEDITORTOOL_H
