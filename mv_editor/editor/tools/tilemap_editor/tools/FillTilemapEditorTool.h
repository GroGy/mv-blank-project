//
// Created by Matty on 15.07.2024.
//

#ifndef MV_EDITOR_FILLTILEMAPEDITORTOOL_H
#define MV_EDITOR_FILLTILEMAPEDITORTOOL_H

#include "tools/tilemap_editor/TilemapEditorTool.h"

namespace Editor
{
    class FillTilemapEditorTool : public TilemapEditorTool
    {
    public: // Overrides

        MV::string_view GetName() override;

        MV::string_view GetIconGlyph() override;

        void Update(TilemapEditorToolContext& context) override;

        void OnDeselect() override;

        void OnSelect() override;

    private: // Helper functions

        bool shouldPlaceTiles() const;

        void processInput(TilemapEditorToolContext &context);

    };
}


#endif //MV_EDITOR_FILLTILEMAPEDITORTOOL_H
