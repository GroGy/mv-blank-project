//
// Created by Matty on 2022-01-24.
//

#ifndef MV_EDITOR_TILEMAPEDITOR_H
#define MV_EDITOR_TILEMAPEDITOR_H

#include "interface/common/Window.h"
#include "common/Atlas.h"
#include "resources/TilemapData.h"
#include "resources/TilemapResource.h"
#include "resources/AtlasResource.h"
#include "resources/TilesetResource.h"
#include "2d/TileSet.h"
#include "imgui/imgui.h"
#include "TilemapEditorTool.h"
#include "resources/TextureResource.h"
#include "async/GenerateTilemapCollisionTask.h"
#include "interface/common/EditorTexture.h"
#include "resources/common/TilemapAssetType.h"

namespace Editor {
    enum class TilemapEditorDataState {
        NoData,
        Loading,
        HasData
    };

    enum class TilemapEditorState {
        Idle,
        Saving,
        Baking
    };

    namespace Config {
        static constexpr MV::string_view sc_TilemapEditorWindowName = "Tilemap Editor";
    }

    class [[MV::Class(EditorWindow)]] TilemapEditor : public Window {
    public: // overrides

        TilemapEditor();

        void WindowSetup() override;

        void Render() override;

        void PostRender() override;

        void OnCleanup() override;

        void OnInit() override;

    public: // Upload data

        void LoadTilemap(MV::AssetReference<MV::TilemapAssetType> tilemapID);

    private: // Drawing helper functions

        void drawTopBar();

        void drawTileSelection();

        void drawTools();

        void drawViewport(const ImVec2 & windowMin, const ImVec2 & windowMax);

        void drawMissingData() const;

    private: // Viewport drawing functions

        void drawGrid(const ImVec2 & windowMin, const ImVec2 & windowMax);

        void drawHighlight(const ImVec2 & windowMin, const ImVec2 & windowMax);

        void drawBackground(const ImVec2 & windowMin, const ImVec2 & windowMax);

        void drawView(const ImVec2 &windowMin, const ImVec2 &windowMax);

        void drawCollision(const ImVec2 & windowMin, const ImVec2 & windowMax);

    private: // Data processing helper functions

        void checkAllResources();

        void checkCollisionGenerationTask();

        void resetSettings();

        void clearData();

    private: // Helper functions

        void processInput(const ImVec2 & windowMin, const ImVec2 & windowMax);

        ImVec2 WorldSpaceToViewSpace(const glm::vec2 & worldPosition,const ImVec2 &windowMin, const ImVec2 &windowMax);

        glm::vec2 ViewSpaceToWorldSpace(const ImVec2 & screenPos,const ImVec2 &windowMin, const ImVec2 &windowMax);

        float GetGridLineWidth() const;

        void updateActiveTool(const ImVec2 &windowMin, const ImVec2 &windowMax);

    private: // Loading new data helper functions

        void loadNewData();

    private: // Fields used for loading new data

        bool m_pLoadNewData = false;

        MV::AssetReference<MV::TilemapAssetType> m_pNewTilemapToLoad = MV::InvalidUUID;

    private: // Internal fields

        TilemapEditorState m_pState = TilemapEditorState::Idle;

        TilemapEditorDataState m_pDataState = TilemapEditorDataState::NoData;

        uint32_t m_pSelectedTile = 0;

        MV::rc<MV::TilemapResource> m_pResource;
        MV::TilemapData m_pData;
        bool m_pHasTilemap = false;
        MV::AssetReference<MV::TilemapAssetType> m_pTilemapID = MV::InvalidUUID;

        MV::rc<MV::TilesetResource> m_pTilesetResource;
        MV::rc<MV::Tileset> m_pTileset;

        MV::rc<MV::AtlasResource> m_pAtlasResource;
        MV::rc<MV::Atlas> m_pAtlas;

        EditorTexture m_pTexture;

        MV::rc<GenerateTilemapCollisionTask> m_pGenerateCollisionTask;

    private: // Options

        bool m_pShowGrid = true;

        bool m_pShowCollision = false;

    private: // Editing fields

        glm::vec2 m_pCameraPosition;

        float m_pTileSizePx = 24.0f;

        float m_pBaseTileSizePx = 24.0f;

        float m_pGridBaseWidth = 0.05f;

        bool m_pIsDraggingCamera = false;

        glm::vec2 m_pMouseDragStart;

        glm::vec2 m_pCameraDragStart;

        MV::vector<glm::uvec2> m_pHighlight;

        ImColor m_pGridColor{0.73f,0.73f,0.75f,0.8f};

        ImColor m_pTileHighlight{1.f,0.51f,0.18f,0.8f};

        bool m_pLockZoomInput = false;

    private: // Tools

        int32_t m_pActiveTool = -1;

        MV::vector<MV::rc<TilemapEditorTool>> m_pTools;
    };
}


#endif //MV_EDITOR_TILEMAPEDITOR_H
