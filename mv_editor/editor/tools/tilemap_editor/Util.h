//
// Created by Matty on 10.12.2022.
//

#ifndef MV_EDITOR_TILEMAP_EDITOR_UTIL_H
#define MV_EDITOR_TILEMAP_EDITOR_UTIL_H

#include "imgui/imgui.h"

#include <EngineMinimal.h>

namespace Editor {
    ImVec2 WorldSpaceToViewSpace(const glm::vec2 & worldPosition, const glm::vec2& cameraPos,const ImVec2 &windowMin, const ImVec2 &windowMax, const float tileSizePx);

    glm::vec2 ViewSpaceToWorldSpace(const ImVec2& screenPos, const glm::vec2& cameraPos, const ImVec2 &windowMin, const ImVec2 &windowMax, const float tileSizePx);
}


#endif //MV_EDITOR_TILEMAP_EDITOR_UTIL_H
