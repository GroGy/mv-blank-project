//
// Created by Matty on 10.07.2024.
//

#ifndef MV_EDITOR_COLLISIONLOOKUP_H
#define MV_EDITOR_COLLISIONLOOKUP_H

#include <common/Types.h>

namespace Editor
{
    namespace Lookup
    {
        static const MV::array<MV::vector<uint8_t>, 256> sc_EdgeLookup = {
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{0, 1, 2, 6, 7}, // 01010001 => 81, Left slope, Missing bottom and left middle
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{0, 1, 2, 3, 4}, // 01010100 => 84, Right slope, missing bottom and right middle
                MV::vector<uint8_t>{0, 1, 2, 3, 4, 5, 6, 7}, // 01010101 => 85, Full collision (only corners selected)
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{0, 1, 2, 3, 4, 5, 6, 7}, // 01010111 => 87, Full collision, corners + Top middle
                MV::vector<uint8_t>{0, 1, 2, 3}, // 01011000 => 88, Right half slope, missing bottom middle
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{0, 1, 2, 3, 4}, // 01011100 => 92, Right slope, missing bottom middle
                MV::vector<uint8_t>{0, 1, 2, 3, 4, 5, 6, 7}, // 01011101 => 93, Full collision, Corners + Right Middle
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{0, 1, 2, 3, 4, 5, 6,
                                    7}, // 01011111 => 95, Full collision, Bottom middle and Left middle missing
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{0, 1, 2, 6, 7}, // 01110001 => 113, Left slope, Missing left middle
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{0, 1, 2, 3, 4}, // 01110100 => 116, Right slope, missing right middle
                MV::vector<uint8_t>{0, 1, 2, 3, 4, 5, 6, 7}, // 01110101 => 117, Full collision, Corners + Bottom Middle
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{0, 1, 2, 3, 4, 5, 6,
                                    7}, // 01110111 => 119, Full collision, Missing left and right middle
                MV::vector<uint8_t>{0, 1, 2, 3}, // 01111000 => 120, Right half slope,
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{0, 1, 2, 3, 4}, // 01111100 => 124, right slope single tile
                MV::vector<uint8_t>{0, 1, 2, 3, 4, 5, 6,
                                    7}, // 01111101 => 125, Full collision, corners + bottom and right middle
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{0, 1, 2, 3, 4, 5, 6, 7}, // 01111111 => 127, Full collision, Left middle missing
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{0, 1, 2, 6, 7}, // 11010001 => 209, Left slope, missing bottom middle
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{0, 1, 2, 3, 4, 7}, // 11010100 => 212, Right half slope top half, no bottom middle, no right center
                MV::vector<uint8_t>{0, 1, 2, 3, 4, 5, 6, 7}, // 11010101 => 213, Full collision, Corners + Left middle
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{0, 1, 2, 3, 4, 5, 6,
                                    7}, // 11010111 => 215, Full collision, Bottom and right middle missing
                MV::vector<uint8_t>{0, 1, 2, 3, 7}, // 11011000 => 216, Bottom half, missing bottom middle
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{0, 1, 2, 3, 4, 7}, // 11011100 => 220, Right half slope top half, no bottom middle
                MV::vector<uint8_t>{0, 1, 2, 3, 4, 5, 6,
                                    7}, // 11011101 => 221, Full collision, Middle bottom and top missing
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{0, 1, 2, 3, 4, 5, 6, 7}, // 11011111 => 223, Full collision, Missing bottom middle
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{0, 1, 2, 3, 4, 5, 6, 7}, // 11011111 => 223, Full collision, Bottom middle missing
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{0, 1, 2, 6, 7}, // 11110001 => 241, Left slope
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{0, 1, 2, 3, 4, 7}, // 11110100 => 244, Right half slope top half, no right center
                MV::vector<uint8_t>{0, 1, 2, 3, 4, 5, 6,
                                    7}, // 11110101 => 245, Full collision, Corners + Bottom and Left middle
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{0, 1, 2, 3, 4, 5, 6, 7}, // 11110111 => 247, Full collision,Right middle missing
                MV::vector<uint8_t>{0, 1, 2, 3, 7}, // 11111000 => 248, Bottom half
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{0, 1, 2, 3, 4, 7}, // 11111100 => 252, Right half slope top half
                MV::vector<uint8_t>{0, 1, 2, 3, 4, 5, 6, 7}, // 11111101 => 253, Full collision, Top middle missing
                MV::vector<uint8_t>{}, // No collision
                MV::vector<uint8_t>{0, 1, 2, 3, 4, 5, 6, 7}, // Full collision
        };
    }
}

#endif //MV_EDITOR_COLLISIONLOOKUP_H
