//
// Created by Matty on 11.12.2022.
//

#ifndef MV_EDITOR_TILESETEDITOR_H
#define MV_EDITOR_TILESETEDITOR_H

#include <EngineMinimal.h>
#include "interface/common/Window.h"
#include "resources/TilesetData.h"
#include "resources/AtlasResource.h"
#include "common/Atlas.h"
#include "resources/TextureResource.h"
#include "imgui/imgui.h"
#include "resources/TilesetResource.h"
#include "interface/common/EditorTexture.h"

namespace Editor {
    namespace Config {
        static constexpr MV::string_view sc_TilesetEditorWindowName = "Tileset Editor";
        static constexpr glm::vec4 sc_MergeablePreviewColor = {0.016f,0.749f,0.271f, 1.0f};
        static constexpr glm::vec4 sc_NonMergeablePreviewColor = {0.337f,0.f,0.f, 1.0f};
        static constexpr float sc_MergeablePreviewSize = 0.3f;
    }

    class [[MV::Class(EditorWindow)]] TilesetEditor : public Window {
    // Tool state types
        enum class ToolState {
            None = 0,
            EditingCollision = 1,

            Count
        };
    public: // overrides

        TilesetEditor();

        void WindowSetup() override;

        void Render() override;

        void PostRender() override;

        void OnCleanup() override;

        void OnInit() override;

    public: // External API

        void LoadTileset(uint64_t tilesetID);

    private: // Internal data helper functions

        void updateData();

        void clearData();

        void setToolState(ToolState newState);

    private: // Editing helper

        void generateTileCollisionData(uint32_t tileIndex, bool setSolid = true);

        void regenerateCollisionForTile(uint32_t tileIndex);

    private: // ImGui drawing functions

        void drawTileSelectPanel();

        void drawViewport(const ImVec2 & windowMin, const ImVec2 & windowMax);

        void drawModePanel();

        void drawMenuBar();

        void drawBackground(const ImVec2 & windowMin, const ImVec2 & windowMax);

        void drawUnderViewportBar();

        void drawCollisionOverlay(const ImVec2 & tileMin, const ImVec2 & tileMax);

        void drawEdge(const ImVec2 & start, const ImVec2 & end, const MV::TileCollision::Edge & edge);

        void drawAddTilePopup();

    private: // Internal fields

        enum DataState {
            NoData,
            Loading,
            HasData
        };

        DataState m_pDataState = NoData;

        uint64_t m_pTilesetID = 0;

        MV::rc<MV::TilesetResource> m_pResource;
        MV::TilesetData m_pData;

        int32_t m_pSelectedTile = -1;

    private: // Editing state

        ToolState m_pToolState = ToolState::None;

        enum class CollisionPointPreview {
            None,
            Circle,

            Count
        };

        CollisionPointPreview m_pCollisionPointPreviewType = CollisionPointPreview::Circle;

    private: // Collision editing fields

        bool m_pShowCollisionPreview = false;

    private: // Data of tileset

        MV::rc<MV::AtlasResource> m_pAtlasResource;

        MV::rc<MV::Atlas> m_pAtlas;

        MV::rc<MV::TextureResource> m_pTextureResource;

    private: // Tile adding

        bool m_pTileAddOpen = false;

        glm::vec2 m_pAddTileWindowPosition;

        bool m_pAddTileSolid = true;

        int32_t m_pSelectedSpriteIndex = -1;

    private: // Imgui data

        EditorTexture m_pTexture;

    };
}


#endif //MV_EDITOR_TILESETEDITOR_H
