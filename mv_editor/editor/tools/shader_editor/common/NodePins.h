//
// Created by Matty on 29.04.2023.
//

#ifndef MV_EDITOR_NODEPINS_H
#define MV_EDITOR_NODEPINS_H

#include "imgui/imgui.h"

namespace Editor {
    void BasicPin(ImDrawList * drawList, bool connected, const ImVec2 & cursor, const ImColor & color);
}


#endif //MV_EDITOR_NODEPINS_H
