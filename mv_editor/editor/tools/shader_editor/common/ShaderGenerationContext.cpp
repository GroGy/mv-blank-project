//
// Created by Matty on 02.05.2023.
//

#include "ShaderGenerationContext.h"
#include "fmt/core.h"

namespace Editor
{
    MV::string ShaderGenerationContext::NextVariableName()
    {
        return fmt::format("Var_{}", ++m_VariableNameCounter).c_str();
    }

    void ShaderGenerationContext::AddShaderLine(MV::string && line)
    {
        m_pShaderLines.push_back(MV::move(line));
    }

    MV::string ShaderGenerationContext::NextFunctionParamName()
    {
        return fmt::format("Param_{}", ++m_VariableNameCounter).c_str();
    }

    void ShaderGenerationContext::AddFunction(MV::string_view name, MV::string&& body)
    {
        m_pPreparedFunctions[name.data()] = MV::move(body);
    }

    bool ShaderGenerationContext::IsFunctionCreated(MV::string_view name) const
    {
        return m_pPreparedFunctions.find(name.data()) != m_pPreparedFunctions.end();
    }

    void ShaderGenerationContext::AddInputVariable(uint32_t id)
    {
        m_pUsedInputVariables.emplace(id);
    }
}