//
// Created by Matty on 2022-06-19.
//

#ifndef MV_EDITOR_SHADERNODE_H
#define MV_EDITOR_SHADERNODE_H

#include <EngineMinimal.h>
#include "ShaderNodePinType.h"
#include "ShaderNodeIds.h"
#include "render/common/RenderPassFrameBuffers.h"
#include "render/common/ShaderPipeline.h"
#include "tools/shader_editor/common/ShaderGenerationContext.h"

// Keyword used in registration attribute after name to hide in popup
enum
{
    HideInPopup
};

namespace Editor
{
    struct ShaderNodePin
    {
        uint32_t m_Id = 0;
        uint32_t m_ConnectedPin = 0;
        ShaderNodePinType::Flags m_Type = ShaderNodePinType::FLOAT;
        MV::string m_Name;
        bool m_ShowName = true;
        bool m_Optional = false;
        MV::vector<uint32_t> m_ConnectedOutputPins;
    };

    class ShaderNode
    {
    public:
        friend class ShaderEditor;

        ShaderNode(uint32_t& idCounter) : m_pNodeId(++idCounter)
        {};

        void Draw();

        virtual void PrecompileFunctions(ShaderGenerationContext& context)
        {};

        // Returns if its code was inserted
        virtual bool InsertShaderCode(ShaderGenerationContext& context)
        { return true; };

        virtual void AssignShaderFlags(ShaderGenerationContext& context)
        { };

        static uint32_t GetId();

        uint32_t GetNodeID() const;

        void LoadData(const nlohmann::json& data);

        void SaveData(nlohmann::json& data);

        virtual bool ShouldCompilePreviewShader() const
        { return false; }

        bool AreOutputsUnAssigned() const;

    public: // Getters

        [[nodiscard]]
        FORCEINLINE const MV::vector<ShaderNodePin>& GetOutputPins() const
        { return m_pOutputPins; };

        [[nodiscard]]
        FORCEINLINE const MV::vector<ShaderNodePin>& GetInputPins() const
        { return m_pInputPins; };

        [[nodiscard]]
        FORCEINLINE MV::vector<ShaderNodePin>& GetOutputPins()
        { return m_pOutputPins; };

        [[nodiscard]]
        FORCEINLINE MV::vector<ShaderNodePin>& GetInputPins()
        { return m_pInputPins; };

    protected: // Overridable functions

        virtual void loadNodeData(const nlohmann::json& data)
        {};

        virtual void saveNodeData(nlohmann::json& data)
        {};

        virtual bool forceDrawPreview() const
        {
            return false;
        }

        virtual bool shouldDrawPreview() const
        {
            return false;
        }

        virtual bool shouldDrawHeader() const
        {
            return true;
        }

        virtual void drawPreviewImpl()
        {};

        virtual bool drawBodyImpl()
        { return false; };

    private: // Internal helper functions

        bool shouldDraw() const;

        void drawInputPins();

        void drawOutputPins();

        void drawHeader();

        void drawPreview();

    protected: // Internal fields

        MV::vector<ShaderNodePin> m_pInputPins{};

        MV::vector<ShaderNodePin> m_pOutputPins{};

        MV::string m_pNodeName;

        ImVec2 m_pPosition = ImVec2(0, 0);

        uint32_t m_pNodeId = 0;

        bool m_pPreviewVisible = false;

        MV::rc<MV::RenderPassFrameBuffers> m_pPreviewFrameBuffers;

        MV::rc<MV::ShaderPipeline> m_pPreviewPipeline;
    };
}


#endif //MV_EDITOR_SHADERNODE_H
