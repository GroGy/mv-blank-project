//
// Created by Matty on 2022-06-19.
//

#ifndef MV_EDITOR_SHADERNODEIDS_H
#define MV_EDITOR_SHADERNODEIDS_H

#include <cstdint>

namespace Editor {
    namespace ShaderNodeIds {
        static constexpr uint32_t InvalidNodeId = 0;

        static constexpr uint32_t ResultNodeId = 1;
        static constexpr uint32_t VariableNodeId = 2;
        static constexpr uint32_t SamplerNodeId = 3;
        static constexpr uint32_t MultiplyNodeId = 4;
    }
}

#endif //MV_EDITOR_SHADERNODEIDS_H
