//
// Created by Matty on 29.04.2023.
//

#include "NodePins.h"

#include "imgui/imgui_internal.h"
#include "imgui/imgui.h"

void Editor::BasicPin(ImDrawList * drawList, bool connected, const ImVec2 & cursor, const ImColor & color)
{
    auto height = ImGui::CalcTextSize("#").y;

    if(connected) {
        drawList->AddCircleFilled(cursor + ImVec2{height/2, height/2}, height / 3, color.operator ImU32(), 4);
    } else {
        drawList->AddCircle(cursor + ImVec2{height/2, height/2}, height / 3, color.operator ImU32(), 4);
    }
}
