//
// Created by Matty on 2022-06-19.
//

#ifndef MV_EDITOR_SHADERNODEPINTYPE_H
#define MV_EDITOR_SHADERNODEPINTYPE_H

#include "common/Types.h"
#include "imgui/imgui.h"
#include "vec4.hpp"

namespace Editor {
    namespace ShaderNodePinType {
        enum Flags {
          FLOAT,
          VEC2,
          VEC3,
          VEC4,
          BOOL
        };

        static const ImColor BasePinColor = {0.95f,0.96f,0.90f,1.0f};
        static const ImColor FloatPinColor = {0.5f,0.73f,1.0f,1.0f};

        MV::string ToString(Flags flag);
        ImColor GetColor(Flags flag);

        struct IFlagsProxy {
            static Flags Get() {return FLOAT;};
        };

        template <int32_t Len, typename Scalar>
        struct FlagsProxy : public IFlagsProxy {
            static Flags Get() {return FLOAT;};
        };

        template<>
        struct FlagsProxy<4, float> {
            static Flags Get() {return VEC4;};
        };

        template<>
        struct FlagsProxy<3, float> {
            static Flags Get() {return VEC3;};
        };

        template<>
        struct FlagsProxy<2, float> {
            static Flags Get() {return VEC2;};
        };
    }

}

#endif //MV_EDITOR_SHADERNODEPINTYPE_H
