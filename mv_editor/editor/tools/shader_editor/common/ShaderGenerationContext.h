//
// Created by Matty on 02.05.2023.
//

#ifndef MV_EDITOR_SHADERGENERATIONCONTEXT_H
#define MV_EDITOR_SHADERGENERATIONCONTEXT_H

#include "common/Types.h"
#include <EngineMinimal.h>

namespace Editor
{
    typedef int32_t FragmentValuePassFlags;
    typedef int32_t ShaderFlags;

    enum FragmentValuePassFlags_
    {
        FragmentValuePassFlags_None = 0,
        FragmentValuePassFlags_UV = 1 << 0,
        FragmentValuePassFlags_WorldPos = 1 << 1,
        FragmentValuePassFlags_ScreenPos = 1 << 2,
        FragmentValuePassFlags_ModelPos = 1 << 3,
    };

    enum ShaderFlags_ {
        ShaderFlags_None = 0,
        ShaderFlags_Time = 1 << 0,
    };

    class ShaderGenerationContext
    {
        friend class ShaderGraphCompiler;

    public: // ID Getters

        MV::string NextFunctionParamName();

        MV::string NextVariableName();

    public: // Code adding

        void AddShaderLine(MV::string&& line);

        void AddFunction(MV::string_view name, MV::string&& body);

        void AddInputVariable(uint32_t id);

    public: // Getters

        [[nodiscard]]
        bool IsFunctionCreated(MV::string_view name) const;

    public:

        MV::unordered_map<uint32_t, MV::string> m_OutputNames;

        FragmentValuePassFlags m_FragmentFlags = FragmentValuePassFlags_None;

        ShaderFlags m_Flags = FragmentValuePassFlags_None;

    private:

        uint32_t m_VariableNameCounter = 0;

        MV::vector<MV::string> m_pShaderLines;

        MV::unordered_map<MV::string, MV::string> m_pPreparedFunctions;

        MV::unordered_set<uint32_t> m_pUsedInputVariables;
    };
}


#endif //MV_EDITOR_SHADERGENERATIONCONTEXT_H
