//
// Created by Matty on 2022-06-19.
//

#ifndef MV_EDITOR_RESULTNODE_H
#define MV_EDITOR_RESULTNODE_H

#include "tools/shader_editor/common/ShaderNode.h"

namespace Editor {
    class ResultNode : public ShaderNode {
    public:
        explicit ResultNode(uint32_t & pinIdCounter);

        static uint32_t GetId();

        uint32_t m_VertexOffsetInputPinId;

        uint32_t m_VertexOffsetInputPinIndex;

    protected:

        void loadNodeData(const nlohmann::json &data) override;

        void saveNodeData(nlohmann::json &data) override;

    private:

    };
}


#endif //MV_EDITOR_RESULTNODE_H
