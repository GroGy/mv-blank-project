//
// Created by Matty on 02.05.2023.
//

#include "MathNodes.h"

namespace Editor
{
    Editor::MathNodes::MathNodes(uint32_t& idCounter) : ShaderNode(idCounter)
    {
    }

    void Editor::MathNodes::loadNodeData(const nlohmann::json& data)
    {
        ShaderNode::loadNodeData(data);
    }

    void Editor::MathNodes::saveNodeData(nlohmann::json& data)
    {
        ShaderNode::saveNodeData(data);
    }

    bool Editor::MathNodes::shouldDrawPreview() const
    {
        return ShaderNode::shouldDrawPreview();
    }

    bool Editor::MathNodes::shouldDrawHeader() const
    {
        return ShaderNode::shouldDrawHeader();
    }

    void Editor::MathNodes::drawPreviewImpl()
    {
        ShaderNode::drawPreviewImpl();
    }

    bool Editor::MathNodes::drawBodyImpl()
    {
        return ShaderNode::drawBodyImpl();
    }

    MultiplyNode::MultiplyNode(uint32_t& idCounter) : MathNodes(idCounter)
    {
        {
            ShaderNodePin mainOutput;

            mainOutput.m_Name = "Value";
            mainOutput.m_Id = ++idCounter;
            mainOutput.m_Type = ShaderNodePinType::FLOAT;
            mainOutput.m_ShowName = false;

            m_pOutputPins.push_back(MV::move(mainOutput));
        }
        {

            ShaderNodePin inputA;

            inputA.m_Name = "A";
            inputA.m_Id = ++idCounter;
            inputA.m_Type = ShaderNodePinType::FLOAT;
            inputA.m_ShowName = false;

            m_pInputPins.push_back(MV::move(inputA));

            ShaderNodePin inputB;

            inputB.m_Name = "B";
            inputB.m_Id = ++idCounter;
            inputB.m_Type = ShaderNodePinType::FLOAT;
            inputB.m_ShowName = false;

            m_pInputPins.push_back(MV::move(inputB));
        }

        m_pNodeName = "Multiply";
    }

    bool MultiplyNode::InsertShaderCode(ShaderGenerationContext& context)
    {
        for(auto && i : m_pInputPins) {
            if(context.m_OutputNames.find(i.m_ConnectedPin) == context.m_OutputNames.end())
                return false;
        }

        auto variableName = context.NextVariableName();
        context.m_OutputNames[m_pOutputPins.front().m_Id] = variableName;

        MV::string line = fmt::format("float {} = {} * {};", variableName,
                                      context.m_OutputNames[m_pInputPins[0].m_ConnectedPin],
                                      context.m_OutputNames[m_pInputPins[1].m_ConnectedPin]).c_str();


        context.AddShaderLine(MV::move(line));

        return true;
    }
}