//
// Created by Matty on 13.05.2023.
//

#ifndef MV_EDITOR_TEXTURESAMPLENODE_H
#define MV_EDITOR_TEXTURESAMPLENODE_H

#include "tools/shader_editor/common/ShaderNode.h"
namespace Editor
{
    class [[mv_register_node("Texture sample node")]] TextureSampleNode : public ShaderNode
    {

    };
}


#endif //MV_EDITOR_TEXTURESAMPLENODE_H
