//
// Created by Matty on 26.07.2023.
//

#include "ParameterNodes.h"

namespace Editor
{
    IParameterNode::IParameterNode(const ShaderNodePinType::Flags & outputType, uint32_t& idCounter) : ShaderNode(idCounter)
    {
        {
            ShaderNodePin mainOutput;

            mainOutput.m_Name = "Value";
            mainOutput.m_Id = ++idCounter;
            mainOutput.m_Type = outputType;
            mainOutput.m_ShowName = false;

            m_pOutputPins.push_back(MV::move(mainOutput));
        }
    }

    bool IParameterNode::InsertShaderCode(ShaderGenerationContext& context)
    {
        return ShaderNode::InsertShaderCode(context);
    }

    void IParameterNode::loadNodeData(const nlohmann::json& data)
    {
        ShaderNode::loadNodeData(data);
    }

    void IParameterNode::saveNodeData(nlohmann::json& data)
    {
        ShaderNode::saveNodeData(data);
    }

    bool IParameterNode::shouldDrawPreview() const
    {
        return ShaderNode::shouldDrawPreview();
    }

    bool IParameterNode::shouldDrawHeader() const
    {
        return ShaderNode::shouldDrawHeader();
    }

    void IParameterNode::drawPreviewImpl()
    {
        ShaderNode::drawPreviewImpl();
    }

    bool IParameterNode::drawBodyImpl()
    {
        return ShaderNode::drawBodyImpl();
    }

    void IParameterNode::SetNodeName(MV::string_view name)
    {
        m_pNodeName = name;
    }

    Vec4ParameterNode::Vec4ParameterNode(uint32_t& idCounter) : IParameterNode(ShaderNodePinType::VEC4,idCounter)
    {

    }

}