//
// Created by Matty on 29.04.2023.
//

#ifndef MV_EDITOR_CONSTANTNODE_H
#define MV_EDITOR_CONSTANTNODE_H

#include "detail/qualifier.hpp"
#include "ext/vector_float4.hpp"
#include "fmt/core.h"
#include "imgui/imgui.h"
#include "imgui/imgui_internal.h"
#include "tools/shader_editor/common/ShaderNode.h"
#include "gtc/type_ptr.hpp"
#include "tools/shader_editor/common/ShaderNodePinType.h"
#include <vcruntime_string.h>

namespace Editor
{
    namespace Config
    {
        static constexpr MV::string_view sc_DeclareShaderVariablePrefixFmt = "{} {} = ";
    }

    class IConstantNode : public ShaderNode
    {
    public:

        explicit IConstantNode(uint32_t& idCounter);

        bool InsertShaderCode(ShaderGenerationContext& context) override;

    protected:

        void loadNodeData(const nlohmann::json& data) override;

        void saveNodeData(nlohmann::json& data) override;

        bool shouldDrawPreview() const override;

        bool shouldDrawHeader() const override;

        void drawPreviewImpl() override;

        bool drawBodyImpl() override;

        virtual void updatePreviewColor()
        {};

    protected: // Helper functions

        MV::string getDeclareVariableShaderCode(MV::string_view variableType, MV::string_view variableName);

    protected:

        glm::vec4 m_pPreviewColor = {0, 0, 0, 0};
    };

    template<int32_t Len, typename Scalar>
    class ScalarConstantNode : public IConstantNode
    {
    public:
        ScalarConstantNode(uint32_t& idCounter) : IConstantNode(idCounter)
        {
            {
                ShaderNodePin mainOutput;

                mainOutput.m_Name = "Value";
                mainOutput.m_Id = ++idCounter;
                mainOutput.m_Type = ShaderNodePinType::FlagsProxy<Len, Scalar>::Get();
                mainOutput.m_ShowName = false;

                m_pOutputPins.push_back(MV::move(mainOutput));
            }

            m_pNodeName = "Scalar constant";

            memset(&m_Value, 0, sizeof(m_Value));

            if constexpr (Len == 4) {
                m_Value[Len - 1] = 1.0f;
            }
        }

        bool drawBodyImpl() override;

    protected:

        glm::vec<Len, Scalar, glm::defaultp> m_Value;

    };

    template<int32_t Len, typename Scalar>
    bool ScalarConstantNode<Len, Scalar>::drawBodyImpl()
    {
        //TODO: Data type from template
        ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, {3, 0});
        ImGui::PushMultiItemsWidths(Len, ImGui::GetTextLineHeight() * 2 * Len);
        bool dirty = false;
        for (uint32_t i = 0; i < Len; i++)
        {
            dirty |= ImGui::InputScalar(fmt::format("##input_contant_{}_{}", GetId(), i).c_str(), ImGuiDataType_Float,
                                        &m_Value[i], nullptr, nullptr, "%.1f");
        }
        ImGui::PopStyleVar();

        if (dirty)
        {
            updatePreviewColor();
        }

        return dirty;
    }

    template<int32_t Len, typename Scalar>
    class MakeScalarNode : public ScalarConstantNode<Len, Scalar>
    {
    public:
        MakeScalarNode(uint32_t & idCounter) : ScalarConstantNode<Len,Scalar>(idCounter) {
            static const char* pin_names[] = {
                    "X",
                    "Y",
                    "Z",
                    "W"
            };

            for(uint32_t i = 0; i < Len; i++) {
                ShaderNodePin pin;
                pin.m_Id = ++idCounter;
                pin.m_Optional = false;
                pin.m_ShowName = true;
                pin.m_Name = pin_names[i];
                pin.m_Type = ShaderNodePinType::FLOAT;
                this->m_pInputPins.push_back(MV::move(pin));
            }
        };

        bool drawBodyImpl() override
        {
            return IConstantNode::drawBodyImpl();
        }

    protected:

        bool shouldDrawPreview() const override {return false;}

    };

    template<int32_t Len, typename Scalar>
    class BreakScalarNode : public IConstantNode
    {
    public:
        explicit BreakScalarNode(uint32_t & idCounter) : IConstantNode(idCounter) {
            static const char* pin_names[] = {
                    "X",
                    "Y",
                    "Z",
                    "W"
            };

            for(uint32_t i = 0; i < Len; i++) {
                ShaderNodePin pin;
                pin.m_Id = ++idCounter;
                pin.m_Optional = false;
                pin.m_ShowName = true;
                pin.m_Name = pin_names[i];
                pin.m_Type = ShaderNodePinType::FLOAT;
                this->m_pOutputPins.push_back(MV::move(pin));
            }

            ShaderNodePin pin;
            pin.m_Id = ++idCounter;
            pin.m_Optional = false;
            pin.m_ShowName = false;
            pin.m_Name = "Input";
            pin.m_Type = ShaderNodePinType::FlagsProxy<Len, Scalar>::Get();
            this->m_pInputPins.push_back(MV::move(pin));


        };

    protected:

        bool shouldDrawPreview() const override {return false;}

        int32_t m_Len = Len;

    public:

        bool drawBodyImpl() override
        {
            return IConstantNode::drawBodyImpl();
        }

    };

    class [[mv_register_shader_node("Vec4 constant")]] Vec4ConstantNode : public ScalarConstantNode<4, float>
    {
    public:

        explicit Vec4ConstantNode(uint32_t& idCounter);

        bool InsertShaderCode(ShaderGenerationContext& context) override;

    protected:

        void updatePreviewColor() override;
    };

    class [[mv_register_shader_node("Vec3 constant")]] Vec3ConstantNode : public ScalarConstantNode<3, float>
    {
    public:

        explicit Vec3ConstantNode(uint32_t& idCounter);

        bool InsertShaderCode(ShaderGenerationContext& context) override;

    protected:

        void updatePreviewColor() override;
    };

    class [[mv_register_shader_node("Float constant")]] FloatConstantNode : public ScalarConstantNode<1, float>
    {
    public:
        explicit FloatConstantNode(uint32_t& idCounter);

        bool InsertShaderCode(ShaderGenerationContext& context) override;

    protected:

        bool shouldDrawPreview() const override {return false;};

    protected:

        void updatePreviewColor() override;
    };

    class [[mv_register_shader_node("Make Vec4")]] MakeVec4Node : public MakeScalarNode<4, float>
    {
    public:

        explicit MakeVec4Node(uint32_t& idCounter);

        bool InsertShaderCode(ShaderGenerationContext& context) override;

    protected:

        void updatePreviewColor() override;
    };

    class [[mv_register_shader_node("Make Vec3")]] MakeVec3Node : public MakeScalarNode<3, float>
    {
    public:

        explicit MakeVec3Node(uint32_t& idCounter);

        bool InsertShaderCode(ShaderGenerationContext& context) override;

    protected:

        void updatePreviewColor() override;
    };

    class [[mv_register_shader_node("Break Vec4")]] BreakVec4Node : public BreakScalarNode<4, float>
    {
    public:

        explicit BreakVec4Node(uint32_t& idCounter);

        bool InsertShaderCode(ShaderGenerationContext& context) override;

    };

    class [[mv_register_shader_node("Break Vec3")]] BreakVec3Node : public BreakScalarNode<3, float>
    {
    public:

        explicit BreakVec3Node(uint32_t& idCounter);

        bool InsertShaderCode(ShaderGenerationContext& context) override;
    };

    class [[mv_register_shader_node("Time")]] TimeNode : public ShaderNode
    {
    public:

        explicit TimeNode(uint32_t& idCounter);

        bool InsertShaderCode(ShaderGenerationContext& context) override;

        void AssignShaderFlags(ShaderGenerationContext& context) override;

    protected:

        bool drawBodyImpl() override;

        bool shouldDrawHeader() const override;
    };
}


#endif //MV_EDITOR_CONSTANTNODE_H
