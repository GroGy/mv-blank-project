//
// Created by Matty on 02.05.2023.
//

#ifndef MV_EDITOR_MATHNODES_H
#define MV_EDITOR_MATHNODES_H

#include "detail/qualifier.hpp"
#include "ext/vector_float4.hpp"
#include "fmt/core.h"
#include "imgui/imgui.h"
#include "imgui/imgui_internal.h"
#include "tools/shader_editor/common/ShaderNode.h"
#include "gtc/type_ptr.hpp"
#include <vcruntime_string.h>

namespace Editor
{
    class MathNodes : public ShaderNode
    {
    public:

        explicit MathNodes(uint32_t& idCounter);

    protected:

        void loadNodeData(const nlohmann::json& data) override;

        void saveNodeData(nlohmann::json& data) override;

        bool shouldDrawPreview() const override;

        bool shouldDrawHeader() const override;

        void drawPreviewImpl() override;

        bool drawBodyImpl() override;

        virtual void updatePreviewColor()
        {};

    protected:

        glm::vec4 m_pPreviewColor = {0, 0, 0, 0};
    };


    class [[mv_register_shader_node("Multiply")]] MultiplyNode : public MathNodes
    {
    public:

        explicit MultiplyNode(uint32_t& idCounter);

        bool InsertShaderCode(ShaderGenerationContext& context) override;

        bool ShouldCompilePreviewShader() const override
        { return true; };

    };
}


#endif //MV_EDITOR_MATHNODES_H
