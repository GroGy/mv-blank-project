//
// Created by Matty on 26.07.2023.
//

#ifndef MV_EDITOR_PARAMETERNODES_H
#define MV_EDITOR_PARAMETERNODES_H

#include "tools/shader_editor/common/ShaderNode.h"

namespace Editor {
    namespace Config
    {

    }

    class IParameterNode : public ShaderNode
    {
    public:

        explicit IParameterNode(const ShaderNodePinType::Flags & outputType, uint32_t& idCounter);

        bool InsertShaderCode(ShaderGenerationContext& context) override;

        void SetNodeName(MV::string_view name);

    protected:

        void loadNodeData(const nlohmann::json& data) override;

        void saveNodeData(nlohmann::json& data) override;

        bool shouldDrawPreview() const override;

        bool shouldDrawHeader() const override;

        void drawPreviewImpl() override;

        bool drawBodyImpl() override;

    public:

        int32_t m_ParameterId = -1;
    };

    class [[mv_register_shader_node("ParameterV4", HideInPopup)]] Vec4ParameterNode : public IParameterNode {
    public:
        explicit Vec4ParameterNode(uint32_t& idCounter);
    };
}


#endif //MV_EDITOR_PARAMETERNODES_H
