//
// Created by Matty on 2021-10-15.
//

#ifndef MV_EDITOR_SHADEREDITOR_H
#define MV_EDITOR_SHADEREDITOR_H

#include "asset/AssetUUID.h"
#include "common/Types.h"
#include "interface/common/Window.h"
#include "imgui/node_editor/imgui_node_editor.h"
#include "render/common/CameraData.h"
#include "render/common/RenderPass.h"
#include "render/common/RenderPassFrameBuffers.h"
#include "resources/ShaderData.h"
#include "tools/shader_editor/common/ShaderNode.h"
#include "resources/ShaderResource.h"
#include "interface/worldobject_renderers/PropertyRendererBase.h"
#include "async/ShaderTask.h"
#include "node_editor/NodeEditorLink.h"

namespace Editor
{
    namespace Config
    {
        static constexpr MV::string_view sc_ShaderEditorWindowName = "Shader Editor";
    }

    struct ShaderInput {
        enum Type {
            Float,
            Vec2,
            Vec3,
            Vec4,
            Mat4,
            Texture,
            Double
        };

        uint32_t m_Id;
        MV::string m_Name;
        Type m_Type = Float;
    };

    namespace ed = ax::NodeEditor;

    // Inherits from PropertyRendererBase for easy rendering of settings
    class [[MV::Class(EditorWindow)]] ShaderEditor : public Window, public IPropertyRendererHelper
    {
    public:
        ShaderEditor();

        bool Validate() const;

        void Compile();

        bool LoadShader(MV::UUID asset);

    public: // Getters

        [[nodiscard]]
        FORCEINLINE const MV::unordered_set<uint32_t> & GetErrorIds() const {return m_pErrorIds;}

    protected:

        void WindowSetup() override;

        void Render() override;

        void PostRender() override;

        void OnWorldRender() override;

    public:

        void OnCleanup() override;

        void OnInit() override;

    private: // Internal drawing functions

        void setupDockspace();

        void drawSettings();

        void drawNodeEditor();

        void drawShaderSettings(bool & dirty);

        void drawLoadingShader();

        void drawNoAssetSelected();

        void drawMenuBar();

        void setupStyle();

        void drawCreateNodePopup(const ImVec2 & clickPos);

        bool processNewLink(const ed::PinId& startPinId, const ed::PinId& endPinId);

        static void processAltLinkDelete() ;

        void processCreate();

        void processDelete();

        void deleteLink(uint32_t id);

        void deleteNode(uint32_t id);

        void drawPushConstants();

        void drawParams();

        void drawDragAndDrop();

    private:

        MV::rc<ShaderTask> m_pSaveTask;

        MV::rc<MV::ShaderResource> m_pShaderResource;

        MV::ShaderData m_pData;

        ed::EditorContext* g_Context = nullptr;

        bool m_AssetChanged = true;

        MV::UUID m_pAsset = MV::InvalidUUID;

        uint32_t m_pIdCounter = 0;

        MV::vector_map<uint32_t, MV::rc<ShaderNode>> m_pNodes{};

        MV::vector<NodeEditorLink> m_pLinks;

        MV::vector<ShaderInput> m_pInputs;

        MV::rc<MV::RenderPass> m_pPreviewRenderPass;

        MV::RenderPassBlueprint m_pRenderPassBp;

        MV::unordered_set<uint32_t> m_pErrorIds;

        glm::mat4 m_PreviewViewMatrix{};
        glm::mat4 m_PreviewProjMatrix{};

        bool m_pPushConstantsCollapsed = true;

        bool m_pParametersCollapsed = true;

        int32_t m_pDraggingParamIndex = -1;
    };
}

#endif //MV_EDITOR_SHADEREDITOR_H
