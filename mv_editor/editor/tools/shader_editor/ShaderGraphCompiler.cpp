//
// Created by Matty on 29.04.2023.
//

#include "ShaderGraphCompiler.h"
#include "ResultNode.h"
#include "allocator/Alloc.h"
#include "common/EngineCommon.h"
#include "common/ShaderGenerationContext.h"
#include "common/Types.h"
#include "render/common/AttributeFormat.h"
#include "render/common/ShaderStage.h"

namespace Editor
{
    ShaderGraphCompilerResult
    ShaderGraphCompiler::CompileShaderCode(const MV::vector_map<uint32_t, MV::rc<ShaderNode>>& nodes,
                                           const MV::vector<NodeEditorLink>& links, const MV::vector<ShaderInput>& inputs,
                                           const MV::ShaderData& data)
    {
        MV::AllocatorTagScope tagScope{MV::AllocationType::Editor};

        m_pNodes = nodes;
        m_pLinks = links;
        m_pInputs = inputs;
        m_Results.clear();
        m_ErrorIds.clear();
        m_pShaderData = data;
        m_ActiveContext = {};

        for (auto&& node: m_pNodes)
        {
            node.second->AssignShaderFlags(m_ActiveContext);
        }

        ShaderGraphCompilerResult res;
        for (auto&& node: m_pNodes)
        {
            if (!m_ErrorIds.empty())
            {
                res.m_NodeResults = m_Results;
                res.m_ErrorIds = MV::move(m_ErrorIds);
                return res;
            }

            if (node.second->ShouldCompilePreviewShader() || node.first == 1)
                doProcessPassForNode(node.first);

        }

        if (!m_ErrorIds.empty())
        {
            res.m_NodeResults = m_Results;
            res.m_ErrorIds = MV::move(m_ErrorIds);
            return res;
        }

        for (auto&& node: m_pNodes)
        {
            if (node.first == 1) //Result node
            {
                doProcessPassForNode(node.first, true);
                break;
            }
        }

        res.m_NodeResults = m_Results;
        res.m_ErrorIds = MV::move(m_ErrorIds);
        return res;
    }

    void ShaderGraphCompiler::removedUnusedNodes(bool vertexPass)
    {
        MV::GetLogger()->LogDebug("Removing unused nodes...");

        bool changed;

        do
        {
            changed = false;

            for (auto&& node: m_pNodes)
            {
                if (m_UnproccessedNodes.find(node.first) == m_UnproccessedNodes.end())
                    continue;

                bool remove = node.second->AreOutputsUnAssigned();

                if (!remove)
                {
                    const auto& outputs = node.second->GetOutputPins();

                    for (auto&& pin: outputs)
                    {
                        if (m_IgnoredPins.find(pin.m_ConnectedPin) != m_IgnoredPins.end())
                        {
                            remove = true;
                            break;
                        }
                    }
                }

                if (remove)
                {
                    changed = true;
                    m_UnproccessedNodes.erase(node.first);
                    for (auto&& input: node.second->GetInputPins())
                    {
                        m_IgnoredPins.emplace(input.m_Id);
                    }
                }
            }
        } while (changed);

        MV::GetLogger()->LogDebug("Removed {} / {} nodes.", (m_pNodes.size() - m_UnproccessedNodes.size()),
                                  m_pNodes.size());
    }

    void ShaderGraphCompiler::mergeShaderCode(bool vertexPass)
    {
        MV::GetLogger()->LogDebug("Building final shader code...");

        MV::string header = Config::sc_GLSLHeaderPrefix.data();

        MV::string functions;

        for (auto&& v: m_ActiveContext.m_pPreparedFunctions)
        {
            functions += v.second;
            functions += "\n";
        }

        MV::string body;

        for (auto&& v: m_ActiveContext.m_pShaderLines)
        {
            body += v;
            body += "\n";
        }

        if (vertexPass)
        {
            auto resultNode = MV::dynamic_pointer_cast<ResultNode>(m_pNodes[1]);
            ENSURE_VALID(resultNode);

            auto& vertexPin = resultNode->GetInputPins()[resultNode->m_VertexOffsetInputPinIndex];

            if (vertexPin.m_ConnectedPin != 0)
            {
                for (auto&& l: m_pLinks)
                {
                    if (l.m_EndPin == resultNode->m_VertexOffsetInputPinId)
                    {
                        body += fmt::format("vec3 FinalPosition = inPosition + {};",
                                            m_ActiveContext.m_OutputNames[l.m_StartPin]).c_str();
                        break;
                    }
                }
            } else
            {
                body += "vec3 FinalPosition = inPosition;";
            }

            body += "\n";

            auto vertex = fmt::format("{}\n\n{{}}\n{}\n{}\n{}\n{}\n}}}}\n", header, functions,
                                      Config::sc_GLSLMainFunctionPrefix, body, Config::sc_GLSLResultWriteFmt);

            auto shaderInput = fmt::format("{}\n\n{}\n", getVertexLayout(),
                                           getPushConstantLayout(MV::ShaderStage::VERTEX));

            //MV::string vertexCompleted = fmt::format(vertex, shaderInput,
            //                                         "gl_Position = PushConstants.scMVP * vec4(FinalPosition, 1.0);").c_str();

            //m_VertexShader = MV::move(vertexCompleted);

            MV::GetLogger()->LogDebug("VR:\n{}", m_VertexShader);
        } else
        {
            auto fragment = fmt::format("{}\n\n{{}}\n{}\n{}\n{}\n{}\n{}\n}}}}\n", header, getPushConstantLayout(MV::ShaderStage::FRAGMENT),functions,
                                        Config::sc_GLSLMainFunctionPrefix, body, Config::sc_GLSLResultWriteFmt);

            m_Results[m_CurrentResultNode] = fragment.c_str();

            MV::GetLogger()->LogDebug("FG:\n{}", fragment);
        }


        MV::GetLogger()->LogDebug("Done.");
    }

    void ShaderGraphCompiler::collectShaderCode(bool vertexPass)
    {
        MV::GetLogger()->LogDebug("Collecting shader code...");
        bool changed;

        if (vertexPass)
        {
            if (m_CurrentResultNode != 1)
            {
                MV::GetLogger()->LogError("Tried to collect shader code for node that isnt result node!");
                ENSURE_NO_ENTRY();
            }
        }

        do
        {
            changed = false;

            for (auto&& node: m_pNodes)
            {
                if (m_UnproccessedNodes.find(node.first) == m_UnproccessedNodes.end())
                    continue;

                if (node.second->InsertShaderCode(m_ActiveContext))
                {
                    if (node.first == 1)
                    {
                        //TODO: Special code for dependant on render pass
                    }
                    changed = true;
                    m_UnproccessedNodes.erase(node.first);
                }
            }
        } while (changed);

        MV::GetLogger()->LogDebug("Collected shader code. ({})", m_ActiveContext.m_pShaderLines.size());
    }

    void ShaderGraphCompiler::prepareFunctions()
    {
        MV::GetLogger()->LogDebug("Preparing functions...");
        for (auto&& node: m_pNodes)
        {
            if (m_UnproccessedNodes.find(node.first) != m_UnproccessedNodes.end())
            {
                node.second->PrecompileFunctions(m_ActiveContext);
            }
        }
        MV::GetLogger()->LogDebug("Prepared functions. ({})", m_ActiveContext.m_pPreparedFunctions.size());
    }

    void ShaderGraphCompiler::populateProcessingFields(bool vertexPass)
    {
        m_UnproccessedNodes.clear();

        if (vertexPass)
        {
            MV::queue<uint32_t> nodesToProcess;

            auto resultNode = MV::dynamic_pointer_cast<ResultNode>(m_pNodes[1]);

            ENSURE_VALID(resultNode);

            for (auto&& l: m_pLinks)
            {
                if (l.m_EndPin == resultNode->m_VertexOffsetInputPinId)
                {
                    nodesToProcess.push(l.m_InputNode);
                    break;
                }
            }

            while (!nodesToProcess.empty())
            {
                auto nodeId = nodesToProcess.front();
                nodesToProcess.pop();

                m_UnproccessedNodes.emplace(nodeId);

                auto node = m_pNodes[nodeId];

                for (auto&& p: node->GetInputPins())
                {
                    if (p.m_ConnectedPin != 0)
                    {
                        for (auto&& l: m_pLinks)
                        {
                            if (l.m_StartPin == p.m_ConnectedPin)
                            {
                                if (m_UnproccessedNodes.find(l.m_InputNode) != m_UnproccessedNodes.end())
                                {
                                    continue;
                                }
                                nodesToProcess.push(l.m_InputNode);
                                break;
                            }
                        }
                    }
                }
            }

            return;
        }

        for (auto&& n: m_pNodes)
            m_UnproccessedNodes.emplace(n.first);
    }

    void ShaderGraphCompiler::doProcessPassForNode(uint32_t nodeId, bool vertexPass)
    {
        if (nodeId == 1)
        {
            MV::GetLogger()->LogDebug("Creating shader for whole shader graph.");
        } else
        {
            MV::GetLogger()->LogDebug("Creating shader graph for node {}.", nodeId);
        }

        {
            FragmentValuePassFlags fragmentFlags = m_ActiveContext.m_FragmentFlags;
            ShaderFlags generalFlags = m_ActiveContext.m_Flags;

            m_ActiveContext = {};

            m_ActiveContext.m_Flags = generalFlags;
            m_ActiveContext.m_FragmentFlags = fragmentFlags;
        }

        m_CurrentResultNode = nodeId;

        populateProcessingFields(vertexPass);

        removedUnusedNodes(vertexPass);

        checkGraphValidity();

        if (!m_ErrorIds.empty())
        {
            MV::GetLogger()->LogError("Error while processing shader graph, some pins in graph are unconnected.");
            return;
        }

        prepareFunctions();

        collectShaderCode(vertexPass);

        try
        {
            mergeShaderCode(vertexPass);
        } catch (fmt::format_error& err)
        {
            MV::GetLogger()->LogError("Failed to format, {}", err.what());
        }
    }

    void ShaderGraphCompiler::checkGraphValidity()
    {
        for (auto& node: m_pNodes)
        {
            if (m_UnproccessedNodes.find(node.first) == m_UnproccessedNodes.end()) continue;

            for (auto& pin: node.second->GetInputPins())
            {
                if (pin.m_ConnectedPin == 0 && !pin.m_Optional)
                {
                    m_ErrorIds.emplace_back(pin.m_Id);
                }
            }
        }
    }

    MV::string ShaderGraphCompiler::getVertexLayout()
    {
        auto& vertexDataBuffers = m_pShaderData.m_DataBuffers;

        MV::vector<MV::string> vertLayouts;

        for (auto&& vdb: vertexDataBuffers)
        {
            MV::string res;

            for (auto& att: vdb.m_Attributes)
            {
                MV::string nameInShader = fmt::format("in{}", att.m_Name).c_str();

                auto type = static_cast<MV::AttributeType>(att.m_Type);

                MV::string typeStr = MV::AttributeTypeProxy::GetString(type).data();

                res += fmt::format("layout(location = {}) in {} {};\n", att.m_Location, typeStr, nameInShader).c_str();
            }

            vertLayouts.push_back(MV::move(res));
        }

        MV::string res;

        for (auto&& str: vertLayouts)
            res += str;

        return res;
    }

    MV::string ShaderGraphCompiler::getPushConstantLayout(MV::ShaderStage_ shaderStage)
    {
        auto pushConstants = m_pShaderData.m_PushConstants;

        if (m_ActiveContext.m_Flags & ShaderFlags_Time)
        {
            uint32_t offset = 0;
            for (auto&& c: pushConstants)
            {
                offset += c.m_Size;
            }

            auto& pc = pushConstants.emplace_back();
            pc.m_Type = static_cast<uint32_t>(MV::AttributeFormat<float>::GetAttributeType());
            pc.m_ShaderStage = shaderStage;
            pc.m_Name = Config::sc_ShaderPassTimeName.data();
            pc.m_Size = sizeof(float);
            pc.m_Offset = offset;
        }

        MV::string res;

        bool addedHeader = false;


        for (auto&& pc: pushConstants)
        {
            if (pc.m_ShaderStage != shaderStage) continue;

            if (!addedHeader)
            {
                res += "layout( push_constant ) uniform constant\n{\n";
                addedHeader = true;
            }

            MV::string nameInShader = fmt::format("sc{}", pc.m_Name).c_str();

            auto type = static_cast<MV::AttributeType>(pc.m_Type);

            MV::string typeStr = MV::AttributeTypeProxy::GetString(type).data();

            res += fmt::format("\t{} {};\n", typeStr, nameInShader).c_str();
        }

        if (!addedHeader)
        {
            return "";
        }

        res += "} PushConstants;\n";

        return res;
    }
}