//
// Created by Matty on 29.04.2023.
//

#ifndef MV_EDITOR_SHADERGRAPHCOMPILER_H
#define MV_EDITOR_SHADERGRAPHCOMPILER_H

#include <EngineMinimal.h>
#include "ShaderEditor.h"
#include "common/ShaderGenerationContext.h"
#include "common/ShaderNode.h"
#include "common/Types.h"

namespace Editor {
    namespace Config {
        static constexpr MV::string_view sc_GLSLHeaderPrefix = "#version 450\n"
                                                                 "#extension GL_ARB_separate_shader_objects : enable";

        static constexpr MV::string_view sc_GLSLMainFunctionPrefix = "void main() {{";

        static constexpr MV::string_view sc_GLSLResultWriteFmt = "\n{{\n"
                                                                 " // Writes result to out variables\n"
                                                                 " {}\n"
                                                                 "}}";


        static constexpr MV::string_view sc_ShaderPassUVName = "UV";
        static constexpr MV::string_view sc_ShaderPassTimeName = "Time";
        static constexpr MV::string_view sc_ShaderPassScreenPositionName = "ScreenPosition";
        static constexpr MV::string_view sc_ShaderPassWorldPositionName = "WorldPosition";
        static constexpr MV::string_view sc_ShaderPassModelPositionName = "ModelPosition";
    }

    struct ShaderGraphCompilerResult {
        struct Result {
            MV::string m_Code;
            //TODO: PushConstants
        };

        MV::unordered_map<uint32_t, MV::string> m_NodeResults;
        MV::string m_VertexShader;

        MV::vector<uint32_t> m_ErrorIds;
    };

    struct ShaderVariablePass {
        uint32_t m_Location;
        MV::string m_Name;
        MV::string m_Type;
    };

    class ShaderGraphCompiler
    {
    public:

        ShaderGraphCompilerResult CompileShaderCode(const MV::vector_map<uint32_t, MV::rc<ShaderNode>> & nodes, const MV::vector<NodeEditorLink> & links, const MV::vector<ShaderInput> & inputs, const MV::ShaderData & data);

    private:

        void doProcessPassForNode(uint32_t nodeId, bool vertexPass = false);

        void populateProcessingFields(bool vertexPass);

        void removedUnusedNodes(bool vertexPass);

        void mergeShaderCode(bool vertexPass);

        void collectShaderCode(bool vertexPass);

        void prepareFunctions();

        void checkGraphValidity();

        MV::string getVertexLayout();

        MV::string getPushConstantLayout(MV::ShaderStage_ shaderStage);

    private: // Results

        // Contains shader code, that can be used to render preview for each node in graph
        MV::unordered_map<uint32_t, MV::string> m_Results;

        MV::string m_VertexShader;

    private: // Fields used while processing

        ShaderGenerationContext m_ActiveContext;

        uint32_t m_CurrentResultNode;

        MV::unordered_set<uint32_t> m_UnproccessedNodes;

        // Pins that are connected to these pins are considered disconnected, because their owner nodes were removed for some reason
        MV::unordered_set<uint32_t> m_IgnoredPins;

        MV::vector<uint32_t> m_ErrorIds;

    private: // Input fields

        MV::vector<ShaderVariablePass> m_pShaderPasses;

        MV::vector_map<uint32_t, MV::rc<ShaderNode>> m_pNodes{};

        MV::vector<NodeEditorLink> m_pLinks;

        MV::vector<ShaderInput> m_pInputs;

        MV::ShaderData m_pShaderData;

    };
}


#endif //MV_EDITOR_SHADERGRAPHCOMPILER_H
