//
// Created by Matty on 23.06.2024.
//

#include "AtlasEditor.h"
#include "util/UtilGui.h"
#include "imgui/imgui_internal.h"
#include "imgui/imgui_impl_vulkan.h"
#include "common/Assert.h"
#include "EditorCommon.h"
#include <common/EngineCommon.h>
#include <common/TextureAsset.h>
#include "resources/AtlasProducer.h"
#include "EASTL/sort.h"
#include "input/Input.h"
#include "interface/worldobject_renderers/PropertyRendererBase.h"
#include "cvar/CVars.h"
#include "resources/ProducerUtils.h"

namespace Editor
{
    namespace CVar
    {
        static float s_DefaultPixelsPerUnit = 32.f;
        static bool s_DefaultShowMousePosition = false;
        static bool s_DefaultShowSpriteOverlays = false;
        static glm::vec4 s_SpriteOverlayColor = {1.0f, 1.0f, 1.0f, 1.0f};
        static glm::vec4 s_SelectedSpriteOverlayColor = {1.0f, 1.0f, 0.0f, 1.0f};
    }

    static MV::CVarRef CVar_AtlasEditor_DefaultPixelsPerUnit{
            "mv.editor.AtlasEditor.pixel_scale",
            "Default value for Pixel scale for atlases",
            CVar::s_DefaultPixelsPerUnit
    };

    static MV::CVarRef CVar_AtlasEditor_ShowMousePosition{
            "mv.editor.AtlasEditor.show_mouse_position",
            "Default value for Pixel scale for atlases",
            CVar::s_DefaultShowMousePosition
    };

    static MV::CVarRef CVar_AtlasEditor_ShowSpriteOverlays{
            "mv.editor.AtlasEditor.show_sprite_overlays",
            "Default value for Show sprite overlays",
            CVar::s_DefaultShowSpriteOverlays
    };

    static MV::CVarRef CVar_AtlasEditor_SpriteOverlayColor{
            "mv.editor.AtlasEditor.sprite_overlay_color",
            "Color for sprite overlay",
            CVar::s_SpriteOverlayColor
    };

    static MV::CVarRef CVar_AtlasEditor_SelectedSpriteOverlayColor{
            "mv.editor.AtlasEditor.selected_sprite_overlay_color",
            "Color for selected sprite overlay",
            CVar::s_SelectedSpriteOverlayColor
    };

    static constexpr MV::string_view sc_AtlasSelectionWindowName = "##atlas_editor_selection";
    static constexpr MV::string_view sc_AtlasViewportWindowName = "##atlas_editor_viewport";
    static constexpr MV::string_view sc_AtlasSettingsWindowName = "##atlas_editor_settings";

    AtlasEditor::AtlasEditor() : Window(Config::sc_AtlasEditorWindowName.data(), true)
    {
        m_pOpen = false;
        m_Flags |= ImGuiWindowFlags_MenuBar;
    }

    void AtlasEditor::WindowSetup()
    {

    }

    void AtlasEditor::Render()
    {
        m_pAnyChildFocused = false;

        updateData();
        updateAutoSlice();
        updateDeleteEmpty();
        updateAseprite();

        drawMenuBar();

        if (m_pDataState != HasData)
        {
            drawNoData();
            return;
        }

        setupDockspace();

        drawContent();

        drawAutoSlice();

        drawAsepriteImporter();
    }

    void AtlasEditor::PostRender()
    {

    }

    void AtlasEditor::OnCleanup()
    {
        m_pAsepriteFileDialog->Cleanup();
    }

    void AtlasEditor::OnInit()
    {
        m_pAsepriteFileDialog = FileBrowser::Get(FileBrowserMode::OPEN_SINGLE);
        m_pAsepriteFileDialog->SetTypeFilters({"*.json"});
        m_pAsepriteFileDialog->Init();
    }

    void AtlasEditor::LoadAtlas(uint64_t atlasID)
    {
        MV::AllocatorTagScope allocTag{MV::AllocationType::Editor};
        clearData();

        m_pAtlasID = atlasID;

        m_pResource = MV::dynamic_pointer_cast<MV::AtlasResource>(
                MV::GetEngine()->m_ResourceManager.LoadResource(m_pAtlasID, true));

        m_pDataState = Loading;

        m_pViewport.Init(&m_pTexture);
        m_pViewport.m_DrawCustomOverlayCallback = [&](TextureViewport* self, const glm::vec4& bounds)
        {
            drawAllSpriteOverlays(bounds);
        };
    }

    void AtlasEditor::updateData()
    {
        if (m_pDataState == NoData)
            return;

        if (m_pResource)
        {
            if (!m_pResource->IsFinished())
                return;
            if (!m_pResource->Success())
            {
                m_pResource.reset();
                m_pDataState = NoData;
                return;
            }

            m_pData = MV::move(m_pResource->m_Result);
            m_pPixelsPerUnit = m_pData.m_PixelsPerUnit;
            m_pResource.reset();
            m_pDataState = HasData;
        }

        if (m_pTexture.GetLoadState() == EditorTexture::Unloaded)
        {
            if (MV::GetEngine()->m_ResourceManager.IsAssetLoaded(m_pData.m_Texture))
            {
                auto foundTexture = MV::GetResources()->m_Textures.find(m_pData.m_Texture);

                CHECK(foundTexture != MV::GetResources()->m_Textures.end());

                if (!foundTexture->second->AssetLoaded())
                {
                    return;
                }

                m_pTextureHandle = foundTexture->second->GetTextureHandle();

                m_pTexture.Load(m_pTextureHandle);

                return;
            }

            MV::GetEngine()->m_ResourceManager.LoadResource(m_pData.m_Texture);
        }
    }

    void AtlasEditor::clearData()
    {
        m_pDataState = NoData;
        m_pData = {};
        m_pResource.reset();
        m_pTextureResource.reset();
        m_pTexture.Unload();
        m_pTextureHandle = {};
        m_pTextureDataDownloadTask.reset();
        m_pAutoSliceProcessing = false;
        m_pViewport.ResetState();
        m_pPixelsPerUnit = CVar::s_DefaultPixelsPerUnit;
        m_pShowSpriteOverlays = CVar::s_DefaultShowSpriteOverlays;
        m_pViewport.m_DrawMousePosition = CVar::s_DefaultShowMousePosition;
        m_pSelectedSpriteIndex = -1;
        m_pAutoSliceOpen = false;
        m_pAsepritePath.clear();
        m_pAsepriteImportOpen = false;
    }

    void AtlasEditor::setupDockspace()
    {
        MV::AllocatorTagScope allocatorTag{MV::AllocationType::Editor};
        auto centerNode = ImGui::GetID("##atlas_editor_dockspace");
        ImGui::DockSpace(centerNode);

        if (ImGui::DockBuilderGetNode(centerNode)->IsLeafNode())
        {
            auto flags = static_cast<ImGuiDockNodeFlags_>(ImGuiDockNodeFlags_NoDocking | ImGuiDockNodeFlags_NoTabBar) |
                         ImGuiDockNodeFlags_NoSplit |
                         ImGuiDockNodeFlags_NoDockingSplitMe |
                         ImGuiDockNodeFlags_NoDockingSplitOther |
                         ImGuiDockNodeFlags_NoDockingOverMe |
                         ImGuiDockNodeFlags_NoDockingOverOther |
                         ImGuiDockNodeFlags_NoDockingOverEmpty;

            ImGuiID viewportNodeId;
            ImGuiID selectionNodeId;
            ImGui::DockBuilderSplitNode(centerNode, ImGuiDir_Down, 0.2f, &selectionNodeId, &viewportNodeId);


            ImGuiID viewportNodeFinalId;
            ImGuiID settingsNodeId;
            ImGui::DockBuilderSplitNode(viewportNodeId, ImGuiDir_Right, 0.25f, &settingsNodeId, &viewportNodeFinalId);

            const auto viewport_node = ImGui::DockBuilderGetNode(viewportNodeFinalId);
            viewport_node->LocalFlags |= flags;

            const auto settings_node = ImGui::DockBuilderGetNode(settingsNodeId);
            settings_node->LocalFlags |= flags;

            const auto selection_node = ImGui::DockBuilderGetNode(selectionNodeId);
            selection_node->LocalFlags |= flags;

            ImGui::DockBuilderDockWindow(sc_AtlasViewportWindowName.data(), viewportNodeFinalId);
            ImGui::DockBuilderDockWindow(sc_AtlasSettingsWindowName.data(), settingsNodeId);
            ImGui::DockBuilderDockWindow(sc_AtlasSelectionWindowName.data(), selectionNodeId);

            ImGui::DockBuilderFinish(settingsNodeId);
            ImGui::DockBuilderFinish(viewportNodeFinalId);
            ImGui::DockBuilderFinish(selectionNodeId);
        }
    }

    void AtlasEditor::drawNoData()
    {
        ScopedVertical vert{"AtlasEditor_VertWrapper", ImGui::GetContentRegionAvail()};
        ImGui::Spring();
        {
            ScopedHorizontal hor{"AtlasEditor_HorWrapper"};
            if (m_pDataState == NoData)
            {
                ImGui::TextUnformatted("No atlas loaded.");
            } else
            {
                ImGui::ProgressBar(0.5f, ImVec2(-FLT_MIN, 0), "Loading...");
            }
        }
        ImGui::Spring();
    }

    void AtlasEditor::drawContent()
    {
        drawSelection();
        drawViewport();
        drawSettingsPanel();
    }

    void AtlasEditor::drawSelection()
    {
        MV::AllocatorTagScope allocatorTag{MV::AllocationType::Editor};
        if (!ImGui::Begin(sc_AtlasSelectionWindowName.data(), nullptr, ImGuiWindowFlags_AlwaysHorizontalScrollbar))
        {
            ImGui::End();
            return;
        }

        m_pAnyChildFocused |= ImGui::IsWindowFocused();

        float elHeight = ImGui::GetContentRegionAvail().y;

        int32_t deleteCandidate = -1;

        for (int32_t i = 0; i < m_pData.m_Sprites.size(); i++)
        {
            const auto& sprite = m_pData.m_Sprites[i];
            glm::vec4 UV = sprite.m_UV;
            MV::string frameName = sprite.m_Name;

            if (m_pTexture.GetLoadState() == EditorTexture::Loaded)
            {
                ImGui::PushID(i);
                if (ImGui::ImageButton(m_pTexture.GetHandle(), {elHeight, elHeight}, {UV.x, UV.y}, {UV.z, UV.w}))
                {
                    m_pSelectedSpriteIndex = i;
                }

                if (ImGui::IsItemClicked(ImGuiMouseButton_Left) && ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left))
                {
                    m_pViewport.MoveToUV(UV);
                }

                ImGui::PopID();
            } else
            {
                if (ImGui::Button(fmt::format("Loading...##{}", i).c_str(), {elHeight, elHeight}))
                {
                    m_pSelectedSpriteIndex = i;
                }
            }

            if (ImGui::IsItemHovered())
            {
                deleteCandidate = i;
            }

            if (i != m_pData.m_Sprites.size() - 1)
            {
                ImGui::SameLine();
            }
        }

        int32_t toDelete = -1;

        if (ImGui::BeginPopupContextWindow("##sprite_right_click"))
        {
            if (ImGui::MenuItem("Delete"))
            {

                toDelete = deleteCandidate;
            }

            ImGui::EndPopup();
        }

        if (toDelete != -1)
        {
            m_pData.m_Sprites.erase(m_pData.m_Sprites.begin() + toDelete);
            if (m_pSelectedSpriteIndex >= toDelete)
            {
                m_pSelectedSpriteIndex--;
            }
        }

        if (m_pData.m_Sprites.empty())
        {
            ScopedVertical vert{"AtlasEditor_VertWrapper", ImGui::GetContentRegionAvail()};
            ImGui::Spring();
            {
                ScopedHorizontal hor{"AtlasEditor_HorWrapper"};
                ImGui::TextUnformatted("No sprites are present");
            }
            ImGui::Spring();
        }

        ImGui::End();
    }

    void AtlasEditor::drawViewport()
    {
        MV::AllocatorTagScope allocatorTag{MV::AllocationType::Editor};
        if (!ImGui::Begin(sc_AtlasViewportWindowName.data()))
        {
            ImGui::End();
            return;
        }

        m_pAnyChildFocused |= ImGui::IsWindowFocused();

        const auto region = ImGui::GetContentRegionAvail();

        glm::vec4 viewBB;

        m_pViewport.Draw("viewport", {region.x, region.y});


        ImGui::End();
    }

    void AtlasEditor::drawMenuBar()
    {
        if (ImGui::BeginMenuBar())
        {
            ScopedDisabled dis{isLocked()};

            if (ImGui::BeginMenu("File"))
            {
                if (ImGui::MenuItem("Save"))
                {
                    auto path = MV::GetEngine()->GetProject().m_Assets[m_pAtlasID].m_Path;

                    calculateSpriteSizes();
                    createMissingSpriteUUIDs();
                    m_pData.m_PixelsPerUnit = m_pPixelsPerUnit;

                    auto resPath = AtlasProducer::CreateAtlasResource(m_pData, path, true);

                    if (MV::GetEngine()->m_ResourceManager.IsAssetLoaded(m_pAtlasID))
                        MV::GetEngine()->m_ResourceManager.UnloadAsset(m_pAtlasID);
                }


                if (ImGui::MenuItem("Reimport"))
                {
                    LoadAtlas(m_pAtlasID);
                }
                ImGui::EndMenu();
            }

            auto cursor = ImGui::GetCurrentWindow()->DC.CursorPos;
            m_pAutoSliceWindowPosition = {cursor.x,
                                          cursor.y + ImGui::GetTextLineHeight() + ImGui::GetStyle().FramePadding.y * 2};

            m_pAsepriteWindowPosition = m_pAutoSliceWindowPosition + glm::vec2{200, 0};

            if (ImGui::BeginMenu("Slice"))
            {
                ImGui::MenuItem("Auto", nullptr, &m_pAutoSliceOpen);


                ImGui::Separator();

                if (ImGui::MenuItem("Add sprite"))
                {
                    auto& sprite = m_pData.m_Sprites.emplace_back();

                    sprite.m_UV = {0, 0, 1, 1};
                    sprite.m_Name = "New";
                    sprite.m_Pivot = {0.5f, 0};
                }

                ImGui::MenuItem("Import aseprite...", nullptr, &m_pAsepriteImportOpen);

                ImGui::EndMenu();
            }

            if (ImGui::BeginMenu("Edit"))
            {
                if (ImGui::MenuItem("Delete all"))
                {
                    GetEditor()->GlobalPopupConfirmOrCancel("Confirm deletion of sprites", [&](bool confirmed)
                    {
                        if (confirmed)
                        {
                            m_pData.m_Sprites.clear();
                            m_pSelectedSpriteIndex = -1;
                        }
                    }, "Are you sure you want to delete all sprites?");
                }

                if (ImGui::MenuItem("Delete empty"))
                {
                    deleteEmpty();
                }

                if (ImGui::MenuItem("Delete single color sprites"))
                {
                    m_pDeleteEmptySingleColorSprites = true;
                    deleteEmpty();
                }

                ImGui::EndMenu();
            }

            if (ImGui::BeginMenu("Editor"))
            {
                ImGui::MenuItem("Pixel snap", nullptr, &m_pPixelSnap);
                ImGui::MenuItem("Show cursor position", nullptr, &m_pViewport.m_DrawMousePosition);
                ImGui::MenuItem("Show scale", nullptr, &m_pViewport.m_DrawScale);
                ImGui::MenuItem("Show sprite size", nullptr, &m_pShowSpriteSize);
                ImGui::MenuItem("Show sprite overlays", nullptr, &m_pShowSpriteOverlays);
                ImGui::EndMenu();
            }

            ImGui::EndMenuBar();
        }
    }

    void AtlasEditor::drawAllSpriteOverlays(const glm::vec4& textureBB)
    {
        if (m_pShowSpriteOverlays)
        {
            for (int32_t i = 0; i < m_pData.m_Sprites.size(); i++)
            {
                auto& sprite = m_pData.m_Sprites[i];
                auto& uv = sprite.m_UV;

                if (m_pSelectedSpriteIndex == i) continue;

                drawSprite(textureBB, uv, m_pSelectedSpriteIndex == i);
            }
        }

        if (m_pSelectedSpriteIndex != -1)
        {
            auto& sprite = m_pData.m_Sprites[m_pSelectedSpriteIndex];
            auto& uv = sprite.m_UV;
            drawSprite(textureBB, uv, true);
        }
    }

    void AtlasEditor::drawSprite(const glm::vec4& textureBB, const glm::vec4& uv, bool selected)
    {
        if (m_pViewport.GetPixelScale() < 0.5f)
        {
            return;
        }

        auto* drawList = ImGui::GetWindowDrawList();

        ImRect rect{textureBB.x, textureBB.y, textureBB.z, textureBB.w};

        const auto textSize = m_pTexture.GetSize();
        const auto uvPerPixelX = (1.0 / (double) textSize.x);
        const auto uvPerPixelY = (1.0 / (double) textSize.y);

        drawList->AddRect(rect.GetTL() +
                          rect.GetSize() * ImVec2{uv.x - (float) (uvPerPixelX / 2), uv.y - (float) (uvPerPixelY / 2)},
                          rect.GetTL() +
                          rect.GetSize() * ImVec2{uv.z + (float) (uvPerPixelX / 2), uv.w + (float) (uvPerPixelY / 2)},
                          selected ? ImGui::ColorConvertFloat4ToU32GLM(CVar::s_SelectedSpriteOverlayColor)
                                   : ImGui::ColorConvertFloat4ToU32GLM(CVar::s_SpriteOverlayColor),
                          0, 0, (selected ? 1.f : 0.5f) * m_pViewport.GetPixelScale()
        );

/*
        if (selected)
        {
            float cornerSize = 12.0f;
            ImVec2 offset = ImVec2{cornerSize / 2, cornerSize / 2};


            const auto tl = rect.GetTL() + rect.GetSize() * ImVec2{uv.x, uv.y};

            const auto tlBB = ImRect{tl - offset,
                                     tl + offset};


            const bool hoveredTL = ImGui::ItemAdd(tlBB, ImGui::GetID("tl##atlas_editor")) && ImGui::IsItemHovered();

            drawList->AddRectFilled(tlBB.GetTL(),
                                    tlBB.GetBR(),
                                    hoveredTL ? IM_COL32(255, 255, 255, 255) : IM_COL32(255, 255, 0, 255),
                                    0
            );

            if (m_pHoldingCorner == None)
            {
                if (hoveredTL && ImGui::IsMouseDown(ImGuiMouseButton_Left))
                {
                    m_pHoldingCorner = TL;
                    m_pDragStartScreen = {ImGui::GetMousePos().x, ImGui::GetMousePos().y};
                    m_pDragStartUV = {uv.x, uv.y};
                }
            }

            const auto br = rect.GetTL() + rect.GetSize() * ImVec2{uv.z, uv.w};

            const auto brBB = ImRect{br - offset,
                                     br + offset};

            const bool hoveredBR = ImGui::ItemAdd(brBB, ImGui::GetID("br##atlas_editor")) && ImGui::IsItemHovered();

            drawList->AddRectFilled(brBB.GetTL(),
                                    brBB.GetBR(),
                                    hoveredBR ? IM_COL32(255, 255, 255, 255) : IM_COL32(255, 255, 0, 255),
                                    0
            );

            if (m_pHoldingCorner == None)
            {
                if (hoveredBR && ImGui::IsMouseDown(ImGuiMouseButton_Left))
                {
                    m_pHoldingCorner = BR;
                    m_pDragStartScreen = {ImGui::GetMousePos().x, ImGui::GetMousePos().y};
                    m_pDragStartUV = {uv.z, uv.w};
                }
            }
        }
        */
    }

    void AtlasEditor::updateDragging(const glm::vec4& textureBB)
    {
        if (m_pHoldingCorner == None)
        {
            return;
        }

        const auto pressed = ImGui::IsMouseDown(ImGuiMouseButton_Left);

        if (pressed) // Update drag
        {
            const glm::vec2 mousePos = {ImGui::GetMousePos().x, ImGui::GetMousePos().y};

            const auto cursorDiff = m_pDragStartScreen - mousePos;

            ImRect region{textureBB.x, textureBB.y, textureBB.z, textureBB.w};

            const auto totalArea = region.GetSize();

            auto normalizedDiff = glm::vec2{cursorDiff.x / totalArea.x, cursorDiff.y / totalArea.y};

            normalizedDiff.x *= -1;
            normalizedDiff.y *= -1;

            auto& uv = m_pData.m_Sprites[m_pSelectedSpriteIndex].m_UV;

            int32_t indexOffset = m_pHoldingCorner == TL ? 0 : 2;

            if (m_pPixelSnap)
            {
                const auto res = m_pTexture.GetSize();

                const auto snap = glm::vec2{1, 1} / res;

                auto snapUV = [&](float uv, float snap)
                {
                    return snap * glm::round(uv / snap);
                };

                uv[indexOffset + 0] = glm::clamp(snapUV(m_pDragStartUV.x + normalizedDiff.x, snap.x), 0.f, 1.f);
                uv[indexOffset + 1] = glm::clamp(snapUV(m_pDragStartUV.y + normalizedDiff.y, snap.y), 0.f, 1.f);
            } else
            {
                uv[indexOffset + 0] = glm::clamp(m_pDragStartUV.x + normalizedDiff.x, 0.f, 1.f);
                uv[indexOffset + 1] = glm::clamp(m_pDragStartUV.y + normalizedDiff.y, 0.f, 1.f);
            }


            return;
        }

        m_pHoldingCorner = None;
        m_pDragStartUV = {0, 0};
        m_pDragStartScreen = {0, 0};
    }

    void AtlasEditor::drawAutoSlice()
    {
        if (!m_pAutoSliceOpen)
        {
            return;
        }

        const auto flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoMove;

        ImGui::SetNextWindowPos({m_pAutoSliceWindowPosition.x, m_pAutoSliceWindowPosition.y}, ImGuiCond_Always);
        ImGui::SetNextWindowSize({150, 190}, ImGuiCond_Always);


        if (ImGui::Begin("##auto_slicer", &m_pAutoSliceOpen, flags))
        {
            {
                ScopedDisabled processingDis{isLocked()};
                {
                    ScopedDisabled dis{
                            m_pAutoSliceSelectedMode == 1 &&
                            (m_pAutoSliceSpriteSize.x == 0 || m_pAutoSliceSpriteSize.y == 0)
                    };
                    if (ImGui::Button("Slice", {-1, 0}))
                    {
                        if (m_pData.m_Sprites.empty())
                        {
                            autoSlice();
                        } else
                        {
                            GetEditor()->GlobalPopupConfirmOrCancel("Confirm deletion of sprites", [&](bool confirmed)
                            {
                                if (confirmed)
                                    autoSlice();
                            }, "Your atlas has sprites in it, these will get deleted by auto-slicing.");
                        }
                    }
                }

                ImGui::Separator();

                static const char* SlicerModes[] = {
                        "Auto",
                        "Fixed"
                };

                ImGui::TextUnformatted("Mode:");
                ImGui::PushItemWidth(ImGui::GetContentRegionAvail().x);
                if (ImGui::BeginCombo("##autoslice_mode", SlicerModes[m_pAutoSliceSelectedMode]))
                {
                    for (uint32_t i = 0; i < 2; i++)
                    {
                        if (ImGui::Selectable(SlicerModes[i], i == m_pAutoSliceSelectedMode))
                        {
                            m_pAutoSliceSelectedMode = i;
                        }
                    }
                    ImGui::EndCombo();
                }

                {
                    ScopedDisabled dis{
                            m_pAutoSliceSelectedMode == 0
                    };

                    const auto totalW = ImGui::GetContentRegionAvail().x;
                    const auto perElementW = (totalW - ImGui::GetStyle().ItemSpacing.x) / 2;

                    const auto xMax = (uint32_t) m_pTexture.GetSize().x;
                    const auto yMax = (uint32_t) m_pTexture.GetSize().y;

                    ImGui::PushItemWidth(perElementW);
                    ImGui::DragScalar("##input_autoslice_x", ImGuiDataType_U32, &m_pAutoSliceSpriteSize.x, 1.0f,
                                      nullptr,
                                      &xMax);
                    ImGui::SameLine();
                    ImGui::DragScalar("##input_autoslice_y", ImGuiDataType_U32, &m_pAutoSliceSpriteSize.y, 1.0f,
                                      nullptr,
                                      &yMax);

                    ImGui::PopItemWidth();
                }

                static const char* PivotModes[] = {
                        "Bottom Center",
                        "Bottom Left"
                };

                ImGui::TextUnformatted("Pivot:");
                if (ImGui::BeginCombo("##autoslice_pivot", PivotModes[m_pAutoSliceSelectedPivotMode]))
                {
                    for (uint32_t i = 0; i < 2; i++)
                    {
                        if (ImGui::Selectable(PivotModes[i], i == m_pAutoSliceSelectedPivotMode))
                        {
                            m_pAutoSliceSelectedPivotMode = i;
                        }
                    }
                    ImGui::EndCombo();
                }

                ImGui::TextUnformatted("Sort:");
                ImGui::SameLine();
                ImGui::Checkbox("##sort", &m_pAutoSliceSort);
            }


            ImGui::End();
        }
    }

    void AtlasEditor::autoSlice()
    {
        static constexpr int32 sc_AutoSlicingMode = 0;
        static constexpr int32 sc_FixedSlicingMode = 1;

        if (m_pAutoSliceSelectedMode == sc_AutoSlicingMode)
        {
            if (!m_pTextureDataDownloadTask)
            {
                m_pTextureDataDownloadTask = MV::Texture::Download(m_pTextureHandle);

                if (!m_pTextureDataDownloadTask || m_pTextureDataDownloadTask->IsError())
                {
                    MV::GetLogger()->LogWarning("Failed to download texture for auto-slicing.");
                    return;
                }
            }

            m_pAutoSliceProcessing = true;

            return;
        }


        if (m_pAutoSliceSelectedMode == sc_FixedSlicingMode)
        {
            if (m_pAutoSliceSpriteSize.x == 0 || m_pAutoSliceSpriteSize.y == 0)
            {
                MV::GetLogger()->LogError("Passed invalid sprite size to auto-slicer!");
                return;
            }

            const auto yCount = (uint32_t) (m_pTexture.GetSize().y / m_pAutoSliceSpriteSize.y);
            const auto xCount = (uint32_t) (m_pTexture.GetSize().x / m_pAutoSliceSpriteSize.x);

            if (yCount == 0 || xCount == 0)
            {
                MV::GetLogger()->LogError("Passed invalid sprite size to auto-slicer!");
                return;
            }

            m_pData.m_Sprites.clear();

            uint32_t counter = 0;

            glm::vec2 uvStep = {(double) m_pAutoSliceSpriteSize.x / (double) m_pTexture.GetSize().x,
                                (double) m_pAutoSliceSpriteSize.y / (double) m_pTexture.GetSize().y};

            double singlePixelUVx = 1.0 / (double) m_pTexture.GetSize().x;
            double singlePixelUVy = 1.0 / (double) m_pTexture.GetSize().y;

            for (uint32_t x = 0; x < xCount; x++)
            {
                for (uint32_t y = 0; y < yCount; y++)
                {
                    auto& sprite = m_pData.m_Sprites.emplace_back();

                    sprite.m_Name = fmt::format("sprite_{}", counter++).c_str();
                    sprite.m_UV = {uvStep.x * x, uvStep.y * y, uvStep.x * (x + 1), uvStep.y * (y + 1)};
                    sprite.m_Pivot = m_pAutoSliceSelectedPivotMode == 0 ? glm::vec2{0, 0.5f} : glm::vec2{0.0f, 0.0f};


                    sprite.m_UV.x += singlePixelUVx / 2.0;
                    sprite.m_UV.y += singlePixelUVy / 2.0;
                    sprite.m_UV.z -= singlePixelUVx / 2.0;
                    sprite.m_UV.w -= singlePixelUVy / 2.0;
                }
            }

        }
    }

    void AtlasEditor::OnOpenStateChanged(bool newValue)
    {
        Window::OnOpenStateChanged(newValue);

        if (newValue)
        {
            return;
        }

        clearData();
    }

    void AtlasEditor::updateAutoSlice()
    {
        if (!m_pAutoSliceProcessing)
        {
            return;
        }

        if (!m_pTextureDataDownloadTask)
        {
            ENSURE_NO_ENTRY();
            m_pAutoSliceProcessing = false;
        }

        if (!m_pTextureDataDownloadTask->IsFinished())
        {
            return;
        }

        // Task is finished, data is downloaded
        auto& pixelData = m_pTextureDataDownloadTask->m_Data;

        if (pixelData.empty())
        {
            ENSURE_NO_ENTRY();
            m_pAutoSliceProcessing = false;
        }

        m_pData.m_Sprites.clear();

        struct Pixel
        {
            uint8_t r, g, b, a;
        };

        auto GetPixel = [&](uint32_t x, uint32_t y)
        {
            Pixel pixel;
            uint32_t index = (x + y * m_pTexture.GetSize().x) * 4;
            pixel.r = pixelData[index + 0];
            pixel.g = pixelData[index + 1];
            pixel.b = pixelData[index + 2];
            pixel.a = pixelData[index + 3];
            return pixel;
        };

        // Bitset to store processed frames
        MV::bitvector processedPixels{pixelData.size() / 4, false};

        // Queue of pixels to process for current sprite
        MV::queue<glm::uvec2> pixelQueue;

        uint32_t counter = 0;

        for (uint32_t i = 0; i < processedPixels.size(); i++)
        {
            if (processedPixels[i])
            {
                continue;
            }

            uint32_t x = i % (uint32_t) m_pTexture.GetSize().x;
            uint32_t y = i / (uint32_t) m_pTexture.GetSize().x;

            Pixel p = GetPixel(x, y);

            if (p.a == 0)
            {
                processedPixels[i] = true;
                continue;
            }

            pixelQueue.emplace(x, y);

            glm::uvec4 BB = {x, y, x, y};

            while (!pixelQueue.empty())
            {
                auto cord = pixelQueue.front();
                pixelQueue.pop();

                uint32_t pixelIndex = cord.x + cord.y * m_pTexture.GetSize().x;

                if (processedPixels[pixelIndex])
                {
                    continue; // Pixel is processed already
                }

                processedPixels[pixelIndex] = true;

                auto pixel = GetPixel(cord.x, cord.y);

                if (pixel.a == 0)
                {
                    continue;
                }

                if (cord.x < BB.x)
                {
                    BB.x = cord.x;
                }

                if (cord.y < BB.y)
                {
                    BB.y = cord.y;
                }

                if (cord.x > BB.z)
                {
                    BB.z = cord.x;
                }

                if (cord.y > BB.w)
                {
                    BB.w = cord.y;
                }

                if (cord.x > 0)
                {
                    pixelQueue.emplace(cord.x - 1, cord.y);
                }

                if (cord.x < m_pTexture.GetSize().x - 1)
                { // changed condition to m_pTexture.GetSize().x - 1
                    pixelQueue.emplace(cord.x + 1, cord.y);
                }

                if (cord.y > 0)
                {
                    pixelQueue.emplace(cord.x, cord.y - 1);
                }

                if (cord.y < m_pTexture.GetSize().y - 1)
                { // changed condition to m_pTexture.GetSize().y - 1
                    pixelQueue.emplace(cord.x, cord.y + 1);
                }
            }

            auto& sprite = m_pData.m_Sprites.emplace_back();

            sprite.m_Name = fmt::format("sprite_{}", counter++).c_str();
            sprite.m_Pivot = m_pAutoSliceSelectedPivotMode == 0 ? glm::vec2{0, 0.5f} : glm::vec2{0.0f, 0.0f};
            sprite.m_UV = {
                    static_cast<double>(BB.x) / (double) m_pTexture.GetSize().x,
                    static_cast<double>(BB.y) / (double) m_pTexture.GetSize().y,
                    static_cast<double>(BB.z + 1) / (double) m_pTexture.GetSize().x,
                    static_cast<double>(BB.w + 1) / (double) m_pTexture.GetSize().y
            };

            double singlePixelUVx = 1.0 / (double) m_pTexture.GetSize().x;
            double singlePixelUVy = 1.0 / (double) m_pTexture.GetSize().y;

            sprite.m_UV.x += singlePixelUVx / 2.0;
            sprite.m_UV.y += singlePixelUVy / 2.0;
            sprite.m_UV.z -= singlePixelUVx / 2.0;
            sprite.m_UV.w -= singlePixelUVy / 2.0;
        }

        if (m_pAutoSliceSort)
        {
            eastl::sort(m_pData.m_Sprites.begin(), m_pData.m_Sprites.end(),
                        [](const MV::AtlasSprite& a, const MV::AtlasSprite& b)
                        {
                            const auto aXCenter = a.m_UV.x + a.m_UV.z / 2;
                            const auto bXCenter = b.m_UV.x + b.m_UV.z / 2;

                            return aXCenter < bXCenter;
                        });

            for (uint32_t i = 0; i < m_pData.m_Sprites.size(); ++i)
            {
                auto& sprite = m_pData.m_Sprites[i];
                sprite.m_Name = fmt::format("sprite_{}", counter++).c_str();
            }
        }

        m_pAutoSliceProcessing = false;
    }

    void AtlasEditor::ProcessInput()
    {
        if (MV::IsKeyPressed(MV::Key::Delete))
        {
            if (m_pSelectedSpriteIndex > -1)
            {
                m_pData.m_Sprites.erase(m_pData.m_Sprites.begin() + m_pSelectedSpriteIndex);
                m_pSelectedSpriteIndex = -1;
            }
        }
    }

    bool AtlasEditor::GetIsFocused() const
    {
        return Window::GetIsFocused() || m_pAnyChildFocused;
    }

    void AtlasEditor::deleteEmpty()
    {
        if (m_pDeleteEmpty) return;

        if (!m_pTextureDataDownloadTask)
        {
            m_pTextureDataDownloadTask = MV::Texture::Download(m_pTextureHandle);

            if (!m_pTextureDataDownloadTask || m_pTextureDataDownloadTask->IsError())
            {
                MV::GetLogger()->LogWarning("Failed to download texture");
                return;
            }
        }

        m_pDeleteEmpty = true;
    }

    void AtlasEditor::updateDeleteEmpty()
    {
        if (!m_pDeleteEmpty) return;

        if (!m_pTextureDataDownloadTask)
        {
            ENSURE_NO_ENTRY();
            m_pDeleteEmptySingleColorSprites = false;
            m_pDeleteEmpty = false;
        }

        if (!m_pTextureDataDownloadTask->IsFinished())
        {
            return;
        }

        auto& pixelData = m_pTextureDataDownloadTask->m_Data;

        if (pixelData.empty())
        {
            ENSURE_NO_ENTRY();
            m_pDeleteEmptySingleColorSprites = false;
            m_pDeleteEmpty = false;
        }

        struct Pixel
        {
            uint8_t r, g, b, a;

            bool operator==(const Pixel& rhs) const
            {
                return r == rhs.r &&
                       g == rhs.g &&
                       b == rhs.b &&
                       a == rhs.a;
            }

            bool operator!=(const Pixel& rhs) const
            {
                return !(rhs == *this);
            }
        };

        auto GetPixel = [&](uint32_t x, uint32_t y)
        {
            Pixel pixel;
            uint32_t index = (x + y * m_pTexture.GetSize().x) * 4;
            pixel.r = pixelData[index + 0];
            pixel.g = pixelData[index + 1];
            pixel.b = pixelData[index + 2];
            pixel.a = pixelData[index + 3];
            return pixel;
        };

        const auto sizef = m_pTexture.GetSize();
        auto size = glm::uvec2{sizef.x, sizef.y};

        for (int32_t i = m_pData.m_Sprites.size() - 1; i >= 0; i--)
        {
            auto& s = m_pData.m_Sprites[i];
            auto uv = s.m_UV;
            auto uvSize = glm::vec2{glm::abs(uv.x - uv.z), glm::abs(uv.y - uv.w)};
            auto spriteSizeF = glm::vec2{size.x * uvSize.x, size.y * uvSize.y};

            auto spriteSize = glm::uvec2{
                    glm::ceil(spriteSizeF.x) +
                    1u, // Adding one because we lose sigle pixel in size due to shifting UV to center of pixels
                    glm::ceil(spriteSizeF.y) +
                    1u, // Adding one because we lose sigle pixel in size due to shifting UV to center of pixels
            };


            auto spriteOffset = glm::uvec2(size.x * uv.x, size.y * uv.y);

            bool isEmpty = true;
            Pixel singleColor = GetPixel(spriteOffset.x, spriteOffset.y);

            for (uint32_t x = 0; x < spriteSize.x; x++)
            {
                if (!isEmpty)
                {
                    break;
                }

                for (uint32_t y = 0; y < spriteSize.y; y++)
                {
                    auto p = GetPixel(spriteOffset.x + x, spriteOffset.y + y);

                    if (m_pDeleteEmptySingleColorSprites)
                    {
                        if (p != singleColor)
                        {
                            isEmpty = false;
                            break;
                        }
                    } else
                    {
                        if (p.a > 0)
                        {
                            isEmpty = false;
                            break;
                        }
                    }
                }
            }

            if (isEmpty)
            {
                m_pData.m_Sprites.erase(m_pData.m_Sprites.begin() + i);
            }
        }

        m_pDeleteEmptySingleColorSprites = false;
        m_pDeleteEmpty = false;
    }

    bool AtlasEditor::isLocked() const
    {
        return m_pAutoSliceProcessing || m_pTexture.GetLoadState() != EditorTexture::Loaded || m_pDeleteEmpty ||
               m_pAsepriteTask;
    }

    void AtlasEditor::calculateSpriteSizes()
    {
        ENSURE_TRUE(m_pTexture.GetLoadState() == EditorTexture::Loaded);

        auto textureSize = m_pTexture.GetSize();

        auto sizeUnits = glm::vec2{textureSize.x / m_pPixelsPerUnit, textureSize.y / m_pPixelsPerUnit};

        for (auto& s: m_pData.m_Sprites)
        {
            s.m_Size = {
                    (s.m_UV.z - s.m_UV.x) * sizeUnits.x,
                    (s.m_UV.w - s.m_UV.y) * sizeUnits.y,
            };
        }
    }

    void AtlasEditor::drawSettingsPanel()
    {
        if (!ImGui::Begin(sc_AtlasSettingsWindowName.data(), nullptr))
        {
            ImGui::End();
            return;
        }

        if (IPropertyRendererHelper::Begin("##settings"))
        {
            if (IPropertyRendererHelper::BeginField("Pixels per unit"))
            {
                ImGui::PushItemWidth(ImGui::GetContentRegionAvail().x);
                ImGui::InputFloat("##Pixels per unit", &m_pPixelsPerUnit);
                ImGui::PopItemWidth();
                IPropertyRendererHelper::EndField();
            }

            IPropertyRendererHelper::End();
        }


        ImGui::TextUnformatted("TEXT");

        ImGui::End();
    }

    void AtlasEditor::createMissingSpriteUUIDs()
    {
        for (auto& s: m_pData.m_Sprites)
        {
            if (s.m_UUID != 0) continue;

            s.m_UUID = Producer::ProducerUtils::GenerateUUID();
        }
    }

    void AtlasEditor::drawAsepriteImporter()
    {
        if (!m_pAsepriteImportOpen)
        {
            return;
        }

        const auto flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoMove;

        ImGui::SetNextWindowPos({m_pAsepriteWindowPosition.x, m_pAsepriteWindowPosition.y}, ImGuiCond_Always);
        ImGui::SetNextWindowSize({150, 190}, ImGuiCond_Always);


        if (ImGui::Begin("##aseprite_importer_atlas_editor", &m_pAsepriteImportOpen, flags))
        {
            {
                ScopedDisabled processingDis{isLocked()};
                {
                    ScopedDisabled dis{
                            m_pAsepritePath.empty() || isLocked()
                    };
                    if (ImGui::Button("Import", {-1, 0}))
                    {
                        if (m_pData.m_Sprites.empty())
                        {
                            importAseprite();
                        } else
                        {
                            GetEditor()->GlobalPopupConfirmOrCancel("Confirm deletion of sprites", [&](bool confirmed)
                            {
                                if (confirmed)
                                    importAseprite();
                            }, "Your atlas has sprites in it, these will get deleted by importing aseprite.");
                        }
                    }
                }

                ImGui::Separator();

                std::filesystem::path pth{m_pAsepritePath.c_str()};

                if (ImGui::Button(m_pAsepritePath.empty() ? "Browse" : pth.stem().string().c_str(), {-1, 0}))
                {
                    m_pAsepriteFileDialog->Open();
                    auto selected = m_pAsepriteFileDialog->GetSelectedFile();

                    if (!selected.empty())
                    {
                        m_pAsepritePath = selected;
                    }
                }
            }


            ImGui::End();
        }
    }

    void AtlasEditor::updateAseprite()
    {
        if(!m_pAsepriteTask) return;

        if(!m_pAsepriteTask->IsDone()) return;

        const bool success = m_pAsepriteTask->Success();
        auto result = m_pAsepriteTask->m_Result;

        m_pAsepriteTask.reset();

        if(success) {

            m_pData.m_Sprites.clear();

            m_pData.m_Sprites = result.m_Sprites;

            return;
        }

        GetEditor()->GlobalPopupOk("Error", [](bool){}, "Failed to import aseprite, check if file is valid.");
    }

    void AtlasEditor::importAseprite()
    {
        ENSURE_TRUE(!m_pAsepriteTask);

        m_pAsepriteTask = MV::make_rc<AsepriteTask>(m_pAsepritePath);
        m_pAsepriteTask->Start(m_pAsepriteTask);
    }
}