//
// Created by Matty on 23.06.2024.
//

#ifndef MV_EDITOR_ATLASEDITOR_H
#define MV_EDITOR_ATLASEDITOR_H

#include <EngineMinimal.h>
#include "interface/common/Window.h"
#include "resources/AtlasResource.h"
#include "common/Atlas.h"
#include "resources/TextureResource.h"
#include "imgui/imgui.h"
#include "interface/common/EditorTexture.h"
#include "interface/common/TextureViewport.h"
#include "async/AsepriteTask.h"

namespace Editor {
    class FileBrowser;

    namespace Config {
        static constexpr MV::string_view sc_AtlasEditorWindowName = "Atlas Editor";
    }

    class [[MV::Class(EditorWindow)]] AtlasEditor : public Window {
    public: // overrides

        AtlasEditor();

        void WindowSetup() override;

        void Render() override;

        void PostRender() override;

        void OnCleanup() override;

        void OnInit() override;

        void OnOpenStateChanged(bool newValue) override;

        void ProcessInput() override;

        bool GetIsFocused() const override;

    public: // External API

        void LoadAtlas(uint64_t atlasID);

    private: // Internal data helper functions

        void updateData();

        void clearData();

    private: // ImGui drawing functions

        void setupDockspace();

        void drawNoData();

        void drawContent();

        void drawSelection();

        void drawViewport();

        void drawAutoSlice();

        void drawAsepriteImporter();

        void drawAllSpriteOverlays(const glm::vec4& textureBB);

        void drawSprite(const glm::vec4& textureBB, const glm::vec4& uv, bool selected);

        void drawSettingsPanel();

        void drawMenuBar();

    private: // internal functions

        bool isLocked() const;

    private: // auto slicer functions

        void autoSlice();

        void updateAutoSlice();

    private: // Utility functions

        void deleteEmpty();

        void updateDeleteEmpty();

        void updateDragging(const glm::vec4& textureBB);

        void calculateSpriteSizes();

        void createMissingSpriteUUIDs();

    private: // Aseprite

        void updateAseprite();

        void importAseprite();

    private: // Internal fields

        enum DataState {
            NoData,
            Loading,
            HasData
        };

        DataState m_pDataState = NoData;

        uint64_t m_pAtlasID = 0;

        MV::rc<MV::AtlasResource> m_pResource;
        MV::AtlasData m_pData;

        bool m_pAnyChildFocused = false;

        float m_pPixelsPerUnit;

        TextureViewport m_pViewport;

    private: // Data of tileset

        MV::rc<MV::TextureResource> m_pTextureResource;

    private: // Texture

        EditorTexture m_pTexture;

        MV::RTHandle m_pTextureHandle;

    private: // Editor fields

        int32_t m_pSelectedSpriteIndex = -1;

        bool m_pPixelSnap = true;

        enum HoldingCorner {
            None,
            TL,
            BR
        };

        HoldingCorner m_pHoldingCorner = None;

        glm::vec2 m_pDragStartScreen;

        glm::vec2 m_pDragStartUV;

        bool m_pShowSpriteSize = true;

        bool m_pShowSpriteOverlays = true;

    private: // Aseprite import

        MV::rc<AsepriteTask> m_pAsepriteTask;

        bool m_pAsepriteImportOpen = false;

        MV::string m_pAsepritePath;

        glm::vec2 m_pAsepriteWindowPosition;

        MV::rc<FileBrowser> m_pAsepriteFileDialog;

    private: // Auto-slice variables

        bool m_pAutoSliceOpen = false;

        glm::vec2 m_pAutoSliceWindowPosition;

        glm::uvec2 m_pAutoSliceSpriteSize = {0,0};

        int32_t m_pAutoSliceSelectedPivotMode = 0;

        int32_t m_pAutoSliceSelectedMode = 0;

        bool m_pAutoSliceProcessing = false;

        bool m_pAutoSliceSort = false;

        glm::vec4 m_pAutoSliceBounds;

    private: // Utility fields

        MV::rc<MV::TextureDownloadTask> m_pTextureDataDownloadTask;

        bool m_pDeleteEmpty = false;

        /// When delete empty is called with this flag on true, it takes sprites which have single color as empty
        bool m_pDeleteEmptySingleColorSprites = false;

    };
}


#endif //MV_EDITOR_ATLASEDITOR_H
