//
// Created by Matty on 2022-02-17.
//

#ifndef MV_EDITOR_ASSETICONS_H
#define MV_EDITOR_ASSETICONS_H

#include <EngineMinimal.h>
#include "../imgui/icons.h"

namespace Editor::Config {
    static constexpr MV::string_view sc_ShaderIcon = ICON_FA_SENSOR;
    static constexpr MV::string_view sc_TextureIcon = ICON_FA_IMAGE;
    static constexpr MV::string_view sc_ModelIcon = ICON_FA_DNA;
    static constexpr MV::string_view sc_FolderIcon = ICON_FA_FOLDER_OPEN;
    static constexpr MV::string_view sc_AtlasIcon = ICON_FA_ATLAS;
    static constexpr MV::string_view sc_WorldIcon = ICON_FA_GLOBE_AMERICAS;
    static constexpr MV::string_view sc_ScriptIcon = ICON_FA_FILE_CODE;
    static constexpr MV::string_view sc_ScriptConfigIcon = ICON_FA_CUBE;
    static constexpr MV::string_view sc_ScriptConfigExtraDataIcon = ICON_FA_CUBES;
    static constexpr MV::string_view sc_TilesetIcon = ICON_FA_GAME_BOARD_ALT;
    static constexpr MV::string_view sc_TilemapIcon = ICON_FA_LAYER_GROUP;
    static constexpr MV::string_view sc_AnimationIcon = ICON_FA_FILM;
    static constexpr MV::string_view sc_BinaryDataIcon = ICON_FA_BOX;

    static constexpr MV::string_view sc_UnknownAsset = ICON_FA_QUESTION_CIRCLE;

    static constexpr MV::string_view sc_SystemFileIcon = ICON_FA_COGS;

    static const MV::unordered_set<MV::string> sc_AllowedExtensions = {
            ".mvasset",
            ".yaml",
            ".comp"
    };
}

#endif //MV_EDITOR_ASSETICONS_H
