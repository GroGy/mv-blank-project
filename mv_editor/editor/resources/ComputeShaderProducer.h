//
// Created by Matty on 20.07.2024.
//

#ifndef MV_EDITOR_COMPUTESHADERPRODUCER_H
#define MV_EDITOR_COMPUTESHADERPRODUCER_H

#include <EngineMinimal.h>
#include "resources/TilemapData.h"

namespace Editor {
    class ComputeShaderProducer {
    public:
        static MV::string CreateComputeShader(const MV::string & pathOut, bool overwrite = false);
    };
}


#endif //MV_EDITOR_COMPUTESHADERPRODUCER_H
