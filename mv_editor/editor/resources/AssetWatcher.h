//
// Created by Matty on 2022-02-18.
//

#ifndef MV_EDITOR_ASSETWATCHER_H
#define MV_EDITOR_ASSETWATCHER_H

#include <util/thread_safe_queue.h>
#include "FileWatch.hpp"
#include <common/Types.h>
#include <filesystem>

namespace Editor {
    class AssetWatcher {
    public:
        void Start(MV::string_view project_path);
        void Stop();
        void Init();
        void Cleanup();
        void Update(float delta);
    private:
        MV::rc<filewatch::FileWatch<std::filesystem::path>> m_pWatch{};
        MV::thread_safe_queue<MV::string> m_pTasks{};
    };
}


#endif //MV_EDITOR_ASSETWATCHER_H
