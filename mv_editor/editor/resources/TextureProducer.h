//
// Created by Matty on 2021-10-28.
//

#ifndef MV_EDITOR_TEXTUREPRODUCER_H
#define MV_EDITOR_TEXTUREPRODUCER_H

#include <EngineMinimal.h>

namespace Editor {
static const int TEXTURE_ASSET_VERSION = 1;

    class TextureProducer {
    public:
        static MV::string CreateTextureResource(const MV::string & pathIn, const MV::string & directory);

        static MV::string CreateTextureResourceToFile(const MV::string & pathIn, const MV::string & pathOut, bool overwrite = false);
    };
}


#endif //MV_EDITOR_TEXTUREPRODUCER_H
