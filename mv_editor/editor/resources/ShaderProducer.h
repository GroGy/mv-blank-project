//
// Created by Matty on 2022-03-13.
//

#ifndef MV_EDITOR_SHADERPRODUCER_H
#define MV_EDITOR_SHADERPRODUCER_H

#include "resources/ShaderData.h"

namespace Editor {
    class ShaderProducer {
    public:
        static MV::string CreateShaderResource(MV::ShaderData & data, const MV::string & path, bool override = false);
    };
}


#endif //MV_EDITOR_SHADERPRODUCER_H
