//
// Created by Matty on 20.07.2024.
//

#include "ComputeShaderProducer.h"
#include "ProducerUtils.h"
#include <fstream>

namespace Editor {
    static constexpr MV::string_view sc_Template = "#version 450\n"
                                                   "\n"
                                                   "//layout (binding = 0) uniform UBO {\n"
                                                   "//    float deltaTime;\n"
                                                   "//} ubo;\n"
                                                   "\n"
                                                   "//layout(std140, binding = 1) readonly buffer SSBOIn {\n"
                                                   "//    float val[ ];\n"
                                                   "//};\n"
                                                   "\n"
                                                   "//layout(std140, binding = 2) buffer SSBOOut {\n"
                                                   "//    float val[ ];\n"
                                                   "//};\n"
                                                   "\n"
                                                   "layout (local_size_x = 256, local_size_y = 1, local_size_z = 1) in;\n"
                                                   "\n"
                                                   "void main()\n"
                                                   "{\n"
                                                   "    \n"
                                                   "}";

    MV::string ComputeShaderProducer::CreateComputeShader(const MV::string& pathOut, bool overwrite)
    {
        using namespace Producer;

        auto finalPath = ProducerUtils::ResolvePath(pathOut, overwrite, ".comp");

        std::ofstream ofs{pathOut.c_str()};

        if (!ofs.is_open()) {
            return "";
        }

        ofs.write(sc_Template.data(), sc_Template.size() * sizeof(char));

        ofs.close();

        return finalPath;
    }
}