//
// Created by Matty on 2021-10-23.
//

#ifndef MV_EDITOR_PROJECTPRODUCER_H
#define MV_EDITOR_PROJECTPRODUCER_H

#include <common/Project.h>
#include <json.hpp>

namespace Editor {
    class ProjectProducer {
    private:
        static void addAssets(MV::Project & proj,nlohmann::json & json);
    public:
        static MV::Project CreateProject(const MV::string& name);
        static void SaveProject(MV::Project & proj, const MV::string & path);
        static MV::string CreateProjectInDirectory(MV::Project & proj, const MV::string & path);
    };
}


#endif //MV_EDITOR_PROJECTPRODUCER_H
