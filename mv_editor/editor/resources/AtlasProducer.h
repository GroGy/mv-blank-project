//
// Created by Matty on 2021-10-28.
//

#ifndef MV_EDITOR_ATLASPRODUCER_H
#define MV_EDITOR_ATLASPRODUCER_H


#include <vec4.hpp>
#include <resources/AtlasData.h>

namespace Editor {
    static const uint32_t ATLAS_VERSION = 1;

    class AtlasProducer {
    public:
        static MV::string CreateAtlasResource(MV::AtlasData & data, const MV::string & path, bool overwrite = false);
    };
}


#endif //MV_EDITOR_ATLASPRODUCER_H
