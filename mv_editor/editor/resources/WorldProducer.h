//
// Created by Matty on 2021-11-03.
//

#ifndef MV_EDITOR_WORLDPRODUCER_H
#define MV_EDITOR_WORLDPRODUCER_H

#include <resources/WorldData.h>

namespace Editor {
    static const uint32_t WORLD_ASSET_VERSION = 1;

    class WorldProducer {
    public:
        static MV::string CreateWorldResource(MV::WorldData & data, const MV::string & path, bool override = false);
    };
}


#endif //MV_EDITOR_WORLDPRODUCER_H
