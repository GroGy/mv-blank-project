//
// Created by Matty on 08.12.2022.
//

#ifndef MV_EDITOR_TILEMAPPRODUCER_H
#define MV_EDITOR_TILEMAPPRODUCER_H

#include <EngineMinimal.h>
#include "resources/TilemapData.h"

namespace Editor {
    class TilemapProducer {
    public:
        static MV::string CreateTilemapResource(const MV::TilemapData & data,const MV::string & pathOut, bool overwrite = false);
    };
}


#endif //MV_EDITOR_TILEMAPPRODUCER_H
