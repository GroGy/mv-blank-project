//
// Created by Matty on 11.02.2023.
//

#ifndef MV_EDITOR_SCRIPTABLECONFIGPRODUCER_H
#define MV_EDITOR_SCRIPTABLECONFIGPRODUCER_H

#include <EngineMinimal.h>
#include "resources/ScriptableConfigData.h"
#include "interface/common/AssetGenerator.h"

namespace Editor
{
    class ScriptableConfigProducer : public IAssetGenerator
    {
    public:
        static ScriptableConfigProducer & GetInstance();

        MV::string GetPath(MV::string_view filename);

        static MV::string CreateConfigResource(const MV::ScriptableConfigData & data,const MV::string & pathOut, bool overwrite = false);
    };
}


#endif //MV_EDITOR_SCRIPTABLECONFIGPRODUCER_H
