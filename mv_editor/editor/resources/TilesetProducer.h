//
// Created by Matty on 2021-11-03.
//

#ifndef MV_EDITOR_TILESETPRODUCER_H
#define MV_EDITOR_TILESETPRODUCER_H

#include <resources/TilesetData.h>

namespace Editor {
    class TilesetProducer {
    public:
        static MV::string CreateTilesetResource(const MV::TilesetData & data,const MV::string & pathOut, bool overwrite = false);
    };
}


#endif //MV_EDITOR_TILESETPRODUCER_H
