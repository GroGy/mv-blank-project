//
// Created by Matty on 2021-10-28.
//

#ifndef MV_EDITOR_PRODUCERUTILS_H
#define MV_EDITOR_PRODUCERUTILS_H

#include <resources/common/Asset.h>
#include <fstream>
#include <resources/TextureData.h>
#include <EngineMinimal.h>

namespace Editor {
    namespace Producer {
        template<typename T>
        void Write(std::ofstream &out, const T &data) {
            out.write((char *) (&data), sizeof(T));
        }

        class ProducerUtils {
        public:
            static void WriteHeader(std::ofstream &out, MV::AssetType type, uint32_t version);

            static MV::TextureData ReadImage(const MV::string &path);

            static MV::UUID GenerateUUID();

            static MV::string ResolvePath(const MV::string &inputPath, bool override = false, MV::string_view extension = ".mvasset");

            static MV::string PathAbsoluteToObjectRelative(MV::string_view absPath);

            static MV::string PathObjectRelativeToAbsolute(MV::string_view relativePath);
        };

        class ProducerException : public std::exception {
        private:
            MV::string m_pWhat;
        public:
            explicit ProducerException(MV::string what);

            ~ProducerException() MV_TXN_SAFE_DYN MV_NOTHROW override;

            [[nodiscard]]
            const char *what() const MV_TXN_SAFE_DYN MV_NOTHROW override;
        };
    }
}


#endif //MV_EDITOR_PRODUCERUTILS_H
