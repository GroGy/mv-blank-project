//
// Created by Matty on 19.08.2023.
//

#ifndef MV_EDITOR_ANIMATIONPRODUCER_H
#define MV_EDITOR_ANIMATIONPRODUCER_H

#include "resources/AnimationData.h"

namespace Editor
{
    class AnimationProducer
    {
    public:
        static MV::string CreateAnimationResource(MV::AnimationData& data, const MV::string& path, bool override = false);
    };
}


#endif //MV_EDITOR_ANIMATIONPRODUCER_H
