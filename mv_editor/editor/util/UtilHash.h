//
// Created by Matty on 2022-02-18.
//

#ifndef MV_EDITOR_UTILHASH_H
#define MV_EDITOR_UTILHASH_H

#include <filesystem>
#include <EngineMinimal.h>

namespace Editor {
    class UtilHash {
    public:
        static MV::string GetSha256(MV::string_view input);
        static MV::string GetFileSha256(const std::filesystem::path & path);
        static MV::string GetFileBinarySha256(const std::filesystem::path & path);
    private:
        static MV::string readFile(const std::filesystem::path & path, bool binary);
    };


    class SHA256
    {
    protected:
        const static uint32_t sha256_k[];
        static constexpr uint32_t SHA224_256_BLOCK_SIZE = (512/8);
    public:
        void init();
        void update(const uint8_t * message, unsigned int len);
        void final(uint8_t *digest);
        static constexpr uint32_t DIGEST_SIZE = ( 256 / 8);
    protected:
        void transform(const uint8_t * message, uint32_t block_nb);
        uint32_t m_tot_len;
        uint32_t m_len;
        uint8_t m_block[2*SHA224_256_BLOCK_SIZE];
        uint32_t m_h[8];
    };
}




#endif //MV_EDITOR_UTILHASH_H
