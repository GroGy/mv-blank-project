//
// Created by Matty on 2022-02-20.
//

#ifndef MV_EDITOR_UTILWORLD_H
#define MV_EDITOR_UTILWORLD_H

#include "common/World.h"

namespace Editor {
    class UtilWorld {
    public:
        static MV::rc<MV::WorldObject> AddScriptableToWorld(MV::World & world, MV::string_view scriptName);
    };
}


#endif //MV_EDITOR_UTILWORLD_H
