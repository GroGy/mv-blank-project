//
// Created by Matty on 2022-01-21.
//

#ifndef MV_EDITOR_UTILGUI_H
#define MV_EDITOR_UTILGUI_H

#include <functional>
#include "../imgui/imgui.h"
#include "common/PropertyReference.h"
#include <string_view>
#include <EngineMinimal.h>

namespace Editor {
    struct ScopedVertical {
        explicit ScopedVertical(MV::string_view name, ImVec2 size = {0,0});
        ~ScopedVertical();
    };

    struct ScopedHorizontal {
        explicit ScopedHorizontal(MV::string_view name, ImVec2 size = {0,0});
        ~ScopedHorizontal();
    };

    struct ScopedCenter {
        explicit ScopedCenter(MV::string_view name);
    private:
        ScopedVertical m_pVert;
        ScopedHorizontal m_pHor;
    };

    struct ScopedDisabled {
        explicit ScopedDisabled(bool enabled = true);
        ~ScopedDisabled();
    private:
        bool m_pEnabled;
    };

    class UtilGui {
    public:
        static ImVec4 FixImGuiColorSpace(const ImVec4 & color);
    };
}

namespace ImGui2 {
    float CalculateIconScaleToFitBox(float boxSideSize, MV::string_view icon, uint32_t customFontIndex = 0);

    void WideInputText(const char* label, MV::string* str, ImGuiInputTextFlags flags = 0, ImGuiInputTextCallback callback = NULL, void* user_data = NULL);
    bool AssetSelectButton(const uint64_t & asset, const ImVec2 & size = {0,0});
    bool AssetBrowserButton(MV::string_view label, const ImVec2 & size, MV::string_view icon, MV::string_view item_name,bool invalidAsset = false, bool selected = false, bool is_directory = false);

    void LabeledSeparator(MV::string_view label);

    bool PropertyRefButton(MV::string_view label,const ImVec2 &size, MV::IPropertyReference& propRef);
}

#endif //MV_EDITOR_UTILGUI_H
