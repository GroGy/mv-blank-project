//
// Created by Matty on 05.02.2023.
//

#ifndef MV_EDITOR_UTILVERIFY_H
#define MV_EDITOR_UTILVERIFY_H

#include <EngineMinimal.h>

namespace Editor {
    class UtilVerify {
    public:
        static bool VerifyScriptName(MV::string_view scriptName);

        static bool VerifyFileName(MV::wstring_view name);
    };
}

#endif //MV_EDITOR_UTILVERIFY_H
