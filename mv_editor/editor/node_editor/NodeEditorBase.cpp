//
// Created by Matty on 29.08.2023.
//

#include "NodeEditorBase.h"
#include "util/UtilGui.h"
#include "imgui/node_editor/imgui_node_editor_internal.h"

namespace Editor
{
    void NodeEditorBase::Render(MV::string_view label)
    {
        MV::AllocatorTagScope allocatorTag{MV::AllocationType::Editor};
        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0.0f, 0.0f});
        if (!ImGui::BeginChild(label.data(), ImGui::GetContentRegionAvail(), true))
        {
            ImGui::PopStyleVar();
            ImGui::EndChild();
            return;
        }
        ImGui::PopStyleVar();

        ed::SetCurrentEditor(m_pEditorContext);

        PreRender();

        ed::Begin("##editor");

        RenderImpl();

        for (auto& node: m_pNodes)
        {
            node->Render(m_pDrawContext);

            ed::NodeId nodeid{static_cast<uintptr_t>(node->GetID())};


            if (EnableNodeContextMenu() && ed::ShowNodeContextMenu(&nodeid) && nodeid.Get() == node->GetID())
            {
                ed::Suspend();
                ImGui::OpenPopup("##node_context_popup");
                m_pOpennedNodeContext = node;
                ed::Resume();
            }
        }

        if (m_pOpennedNodeContext)
        {
            ed::Suspend();
            drawNodeContextMenu(m_pOpennedNodeContext);
            ed::Resume();
        }

        ed::PushStyleVar(ed::StyleVar_DrawDeadLinks, true);

        for (auto& link: m_pLinks)
        {
            ed::Link(link.m_Id, link.m_StartPin, link.m_EndPin, ImColor(255, 255, 255, 255));

            ed::LinkId linkId;

            if (EnableLinkContextMenu() && ed::ShowLinkContextMenu(&linkId) && linkId.Get() == link.m_Id)
            {
                ed::Suspend();
                ImGui::OpenPopup("##link_context_popup");
                m_pOpennedLinkContext = &link;
                ed::Resume();
            }
        }

        if (m_pOpennedLinkContext)
        {
            ed::Suspend();
            drawLinkContextMenu(*m_pOpennedLinkContext);
            ed::Resume();
        }

        ed::PopStyleVar();

        {
            ed::Suspend();

            if (ed::ShowBackgroundContextMenu() && !m_pRegisteredNodes.empty())
            {
                ImGui::OpenPopup("##create_node_popup");
            }

            drawCreateNodePopup();

            ed::Resume();
        }

        processCreate();

        ed::End();

        PostRender();

        auto editor = reinterpret_cast<ed::Detail::EditorContext*>(m_pEditorContext);
        m_pDirty = editor->IsDirty();

        ImGui::EndChild();
    }

    void NodeEditorBase::Init()
    {
        InitNodeTypes();
        ed::Config config;
        config.SettingsFile = "";
        m_pEditorContext = ed::CreateEditor(&config);

        ed::SetCurrentEditor(m_pEditorContext);

        InitImpl();

        setupStyle();
    }

    void NodeEditorBase::Cleanup()
    {
        if (m_pEditorContext)
        {
            ed::DestroyEditor(m_pEditorContext);
            m_pEditorContext = nullptr;
        }
    }

    void NodeEditorBase::drawCreateNodePopup()
    {
        if (ImGui::BeginPopup("##create_node_popup"))
        {
            for (auto&& v: m_pRegisteredNodes)
            {
                if (ImGui::MenuItem(v.m_Name.c_str()))
                {
                    m_pNodes.emplace_back(v.m_CTor(m_pIdCounter));
                    OnNodeAdded(m_pNodes.back());
                    //TODO: set its position to click position
                    //ed::SetNodePosition(id, ed::ScreenToCanvas(clickPos));
                }
            }
            ImGui::EndPopup();
        }
    }

    void NodeEditorBase::processCreate()
    {
        if (ed::BeginCreate(ImColor(255, 255, 255), 2.0f))
        {
            ed::PinId startPinId;
            ed::PinId endPinId;

            m_pDrawContext.m_DraggingLink = true;

            if (ed::QueryNewLink(&startPinId, &endPinId))
            {
                if (startPinId != endPinId)
                {
                    bool newLink = true;

                    for (auto&& link: m_pLinks)
                    {
                        if (link.m_StartPin == startPinId.Get() && link.m_EndPin == endPinId.Get())
                        {
                            newLink = false;
                            break;
                        }
                    }

                    if (!newLink || !ValidateLink((NodeEditorId) startPinId.Get(), (NodeEditorId) endPinId.Get()))
                    {
                        ed::RejectNewItem({1, 0, 0, 1}, 4.0f);
                    } else if (ed::AcceptNewItem({0, 1, 0, 1}, 4.0f) &&
                               ConfirmLink((NodeEditorId) startPinId.Get(), (NodeEditorId) endPinId.Get()))
                    {
                        AddLink((NodeEditorId) startPinId.Get(), (NodeEditorId) endPinId.Get());
                    }
                }
            }
        } else
        {
            m_pDrawContext.m_DraggingLink = false;
        }
        ed::EndCreate();
    }

    NodeEditorLink& NodeEditorBase::AddLink(const NodeEditorId& startPin, const NodeEditorId& endPin)
    {
        auto& res = m_pLinks.emplace_back();
        res.m_Id = ++m_pIdCounter;
        res.m_StartPin = startPin;
        res.m_EndPin = endPin;
        return res;
    }

    void NodeEditorBase::Clear()
    {
        ed::DestroyEditor(m_pEditorContext);
        ed::Config config;
        config.SettingsFile = "";
        m_pEditorContext = ed::CreateEditor(&config);

        ed::SetCurrentEditor(m_pEditorContext);

        setupStyle();

        ClearImpl();

        m_pNodes.clear();
        m_pLinks.clear();
        m_pIdCounter = 0;
        m_pDirty = false;
        m_pOpennedNodeContext.reset();
        m_pOpennedLinkContext = nullptr;
    }

    void NodeEditorBase::setupStyle()
    {
        auto& neStyle = ed::GetStyle();

        neStyle.Colors[ax::NodeEditor::StyleColor_Bg] = {0.1f, 0.1f, 0.13f, 1.0f};

        SetupStyle();

        for (auto& c: neStyle.Colors)
        {
            c = UtilGui::FixImGuiColorSpace(c);
        }
    }

    void NodeEditorBase::ClearDirty()
    {
        auto editor = reinterpret_cast<ed::Detail::EditorContext*>(m_pEditorContext);
        editor->ClearDirty();
    }

    void NodeEditorBase::drawNodeContextMenu(const MV::rc<NodeEditorNodeBase>& node)
    {
        if (ImGui::BeginPopup("##node_context_popup"))
        {
            DrawNodeContext(node);
            ImGui::EndPopup();
        } else {
            m_pOpennedNodeContext.reset();
        }
    }

    void NodeEditorBase::drawLinkContextMenu(const NodeEditorLink& link)
    {
        if (ImGui::BeginPopup("##link_context_popup"))
        {
            DrawLinkContext(link);
            ImGui::EndPopup();
        } else {
            m_pOpennedLinkContext = nullptr;
        }
    }
}