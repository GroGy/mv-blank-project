//
// Created by Matty on 29.08.2023.
//

#ifndef MV_EDITOR_NODEEDITORREGISTEREDNODE_H
#define MV_EDITOR_NODEEDITORREGISTEREDNODE_H

#include "NodeEditorNode.h"
#include "NodeEditorTypes.h"
#include <EngineMinimal.h>

namespace Editor {
    struct NodeEditorRegisteredNode {
        eastl::function<MV::rc<NodeEditorNodeBase>(NodeEditorId&)> m_CTor;
        MV::string m_Name;
    };
}

#endif //MV_EDITOR_NODEEDITORREGISTEREDNODE_H
