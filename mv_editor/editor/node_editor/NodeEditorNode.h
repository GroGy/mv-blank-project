//
// Created by Matty on 29.08.2023.
//

#ifndef MV_EDITOR_NODEEDITORNODE_H
#define MV_EDITOR_NODEEDITORNODE_H

#include <EngineMinimal.h>
#include "NodeEditorContext.h"
#include "NodeEditorTypes.h"
#include "imgui/imgui_internal.h"
#include "imgui/node_editor/imgui_node_editor.h"
#include "NodeEditorPin.h"

namespace Editor
{
    namespace ed = ax::NodeEditor;

    class NodeEditorNodeBase
    {
    public:

        void Render(const NodeEditorContext& ctx);

        void Init(NodeEditorId& idCounter);

    public:

        FORCEINLINE void SetPosition(const glm::vec2& position) { m_pPosition = position; m_pPositionDirty = true;}

    public: // Getters

        FORCEINLINE NodeEditorId GetID() const { return m_pId; }

        FORCEINLINE const MV::vector<NodeEditorPin>& GetPins() { return m_pPins; };

        [[nodiscard]]
        FORCEINLINE const glm::vec2 GetPosition() const { return m_pPosition; }

    protected:

        virtual void PreRender() {};

        virtual void PostRender() {};

        virtual void RenderImpl(const NodeEditorContext& ctx) = 0;

        virtual void RenderPin(const NodeEditorPin& pin) = 0;

        virtual void AfterRenderImpl() {};

        virtual void InitImpl(NodeEditorId& idCounter) = 0;

        virtual void DrawPins(const NodeEditorContext& ctx,const ImVec2& startCursorPosition);

    protected: // Helper functions

        NodeEditorPin& AddInputPin(NodeEditorId& idCounter);

        NodeEditorPin& AddOutputPin(NodeEditorId& idCounter);

    protected:

        // Synced from imgui before every render
        glm::vec2 m_pPosition;

        MV::vector<NodeEditorPin> m_pPins;

        ImRect m_pRect;

    private:

        NodeEditorId m_pId = sc_InvalidNodeEditorId;

        bool m_pPositionDirty = false;

    };
}

#endif //MV_EDITOR_NODEEDITORNODE_H
