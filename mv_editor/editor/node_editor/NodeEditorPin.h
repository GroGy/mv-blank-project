//
// Created by Matty on 29.08.2023.
//

#ifndef MV_EDITOR_NODEEDITORPIN_H
#define MV_EDITOR_NODEEDITORPIN_H

#include "NodeEditorTypes.h"
#include <EngineMinimal.h>
#include "imgui/node_editor/imgui_node_editor.h"

namespace Editor {
    struct NodeEditorPinType {

        enum Type {
            Output,
            Input
        };

        Type m_Value;

        static FORCEINLINE ax::NodeEditor::PinKind ToPinKind(Type value) {
            switch (value)
            {
                case Output:
                    return ax::NodeEditor::PinKind::Output;
                case Input:
                    return ax::NodeEditor::PinKind::Input;
            }
        }
    };

    struct NodeEditorPin {
        NodeEditorId m_Id = sc_InvalidNodeEditorId;
        NodeEditorPinType::Type m_Type = NodeEditorPinType::Input;

        void* m_Payload = nullptr;
        size_t m_PayloadSize = 0;
    };
}

#endif //MV_EDITOR_NODEEDITORPIN_H
