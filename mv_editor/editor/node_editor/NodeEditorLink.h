//
// Created by Matty on 29.08.2023.
//

#ifndef MV_EDITOR_NODEEDITORLINK_H
#define MV_EDITOR_NODEEDITORLINK_H

#include <EngineMinimal.h>
#include "NodeEditorTypes.h"

namespace Editor
{

    struct NodeEditorLink
    {
        // Node from which link goes, Start pin is located on input node
        NodeEditorId m_InputNode = sc_InvalidNodeEditorId;
        NodeEditorId m_StartPin = sc_InvalidNodeEditorId;

        // Node to where this link goes, End pin is located on input node
        NodeEditorId m_OutputNode = sc_InvalidNodeEditorId;
        NodeEditorId m_EndPin = sc_InvalidNodeEditorId;

        NodeEditorId m_Id = sc_InvalidNodeEditorId;
    };
}

#endif //MV_EDITOR_NODEEDITORLINK_H
