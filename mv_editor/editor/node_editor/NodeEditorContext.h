//
// Created by Matty on 30.08.2023.
//

#ifndef MV_EDITOR_NODEEDITORCONTEXT_H
#define MV_EDITOR_NODEEDITORCONTEXT_H

#include <EngineMinimal.h>

namespace Editor
{
    struct NodeEditorContext
    {
        bool m_EnablePinSelect = true;
        bool m_EnableNodeSelect = true;

        bool m_DraggingLink = false;
    };
}

#endif //MV_EDITOR_NODEEDITORCONTEXT_H
