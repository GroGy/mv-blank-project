//
// Created by Matty on 29.08.2023.
//

#ifndef MV_EDITOR_NODEEDITORBASE_H
#define MV_EDITOR_NODEEDITORBASE_H

#include <EngineMinimal.h>
#include "NodeEditorLink.h"
#include "imgui/node_editor/imgui_node_editor.h"
#include "NodeEditorRegisteredNode.h"

namespace Editor
{
    namespace ed = ax::NodeEditor;

    class NodeEditorNodeBase;

    class NodeEditorBase
    {
    public:

        void Render(MV::string_view label);

        void Init();

        void Cleanup();

        void Clear();

        void ClearDirty();

    public: // Node related API

        template <typename T>
        FORCEINLINE MV::rc<T> AddNode();

        [[nodiscard]]
        FORCEINLINE const MV::vector<MV::rc<NodeEditorNodeBase>>& GetNodes() const { return m_pNodes; };

        [[nodiscard]]
        FORCEINLINE const MV::vector<NodeEditorLink>& GetLinks() const { return m_pLinks; };

        [[nodiscard]]
        FORCEINLINE bool IsDirty() const { return m_pDirty; }

    public: // Link related API

        NodeEditorLink& AddLink(const NodeEditorId& startPin, const NodeEditorId& endPin);

    protected:

        virtual void ClearImpl() = 0;

        virtual void RenderImpl() = 0;

        virtual void InitNodeTypes() = 0;

        virtual void InitImpl() {};

        virtual void PreRender() {};

        virtual void PostRender() {};

        virtual void SetupStyle() {};

        // Returns if link should be highlighted
        virtual bool ValidateLink(NodeEditorId beginPin, NodeEditorId endPin) { return true; };

        // Returns if link should be created on confirm
        virtual bool ConfirmLink(NodeEditorId beginPin, NodeEditorId endPin) { return false; };

    protected: // Callbacks

        virtual void OnNodeAdded(const MV::rc<NodeEditorNodeBase>& node) {}

        virtual void OnNodeRemoved(const MV::rc<NodeEditorNodeBase>& node) {};

    protected: // Optional overridable functions

        virtual void drawCreateNodePopup();

        virtual void DrawNodeContext(const MV::rc<NodeEditorNodeBase>& node) {};

        virtual void DrawLinkContext(const NodeEditorLink& link) {};

    protected: // Settings

        virtual bool EnableNodeContextMenu() { return false; };

        virtual bool EnableLinkContextMenu() { return false; };

    protected: // Helper functions

        template <typename T>
        FORCEINLINE void RegisterNodeType(MV::string_view name);

    private:

        void processCreate();

        void setupStyle();

        void drawNodeContextMenu(const MV::rc<NodeEditorNodeBase>& node);

        void drawLinkContextMenu(const NodeEditorLink& link);

    public:



    protected:

        MV::vector<MV::rc<NodeEditorNodeBase>> m_pNodes;

        MV::vector<NodeEditorLink> m_pLinks;

        MV::vector<NodeEditorRegisteredNode> m_pRegisteredNodes;

        ed::EditorContext* m_pEditorContext = nullptr;

        NodeEditorContext m_pDrawContext;

        NodeEditorId m_pIdCounter = 0;

        bool m_pDirty = false;

        MV::rc<NodeEditorNodeBase> m_pOpennedNodeContext;

        NodeEditorLink* m_pOpennedLinkContext = nullptr;
    };


    template<typename T>
    FORCEINLINE void NodeEditorBase::RegisterNodeType(MV::string_view name)
    {
        static_assert(eastl::is_base_of_v<NodeEditorNodeBase, T>, "T must be node type");

        NodeEditorRegisteredNode res;

        res.m_Name = name;
        res.m_CTor = [](NodeEditorId& idCounter){
            auto node = MV::make_rc<T>();
            node->Init(idCounter);
            return node;
        };

        m_pRegisteredNodes.push_back(MV::move(res));
    }


    template<typename T>
    FORCEINLINE MV::rc<T> NodeEditorBase::AddNode()
    {
        static_assert(eastl::is_base_of_v<NodeEditorNodeBase, T>, "T must be node type");

        auto node = MV::make_rc<T>();
        node->Init(m_pIdCounter);

        m_pNodes.emplace_back(node);

        OnNodeAdded(node);

        return node;
    }
}

#endif //MV_EDITOR_NODEEDITORBASE_H
