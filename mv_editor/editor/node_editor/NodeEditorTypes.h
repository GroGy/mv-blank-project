//
// Created by Matty on 29.08.2023.
//

#ifndef MV_EDITOR_NODEEDITORTYPES_H
#define MV_EDITOR_NODEEDITORTYPES_H

#include <cstdint>

namespace Editor
{
    using NodeEditorId = int32_t;

    static constexpr NodeEditorId sc_InvalidNodeEditorId = -1;
}

#endif //MV_EDITOR_NODEEDITORTYPES_H
