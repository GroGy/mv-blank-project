//
// Created by Matty on 29.08.2023.
//

#include "NodeEditorNode.h"
#include "imgui/node_editor/imgui_node_editor.h"
#include "imgui/imgui_internal.h"
#include "imgui/node_editor/imgui_node_editor_internal.h"

namespace Editor
{
    static inline ImRect ImGui_GetItemRect()
    {
        return ImRect(ImGui::GetItemRectMin(), ImGui::GetItemRectMax());
    }

    namespace ed = ax::NodeEditor;

    void NodeEditorNodeBase::Render(const NodeEditorContext& ctx)
    {
        ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2{0, 0});

        if(m_pPositionDirty) {
            ed::SetNodePosition(m_pId, {m_pPosition.x, m_pPosition.y});
            m_pPositionDirty = false;
        }

        PreRender();

        ed::BeginNode(m_pId);
        auto cursor = ImGui::GetCurrentWindow()->DC.CursorPos;

        ImGui::PopStyleVar();

        {
            RenderImpl(ctx);
            m_pRect = ImGui_GetItemRect();
        }

        auto cursorAfter = ImGui::GetCurrentWindow()->DC.CursorPos;

        DrawPins(ctx,cursor);

        ImGui::GetCurrentWindow()->DC.CursorPos = cursorAfter;

        ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2{0, 0});
        ed::EndNode();

        PostRender();

        auto tmpPos = ed::GetNodePosition(m_pId);
        m_pPosition = {tmpPos.x, tmpPos.y};

        ImGui::PopStyleVar();

        AfterRenderImpl();

    }

    void NodeEditorNodeBase::Init(NodeEditorId& idCounter)
    {
        m_pId = ++idCounter;

        InitImpl(idCounter);
    }

    NodeEditorPin& NodeEditorNodeBase::AddInputPin(NodeEditorId& idCounter)
    {
        auto& pin = m_pPins.emplace_back();
        pin.m_Id = ++idCounter;

        pin.m_Type = NodeEditorPinType::Input;

        return pin;
    }

    NodeEditorPin& NodeEditorNodeBase::AddOutputPin(NodeEditorId& idCounter)
    {
        auto& pin = m_pPins.emplace_back();
        pin.m_Id = ++idCounter;

        pin.m_Type = NodeEditorPinType::Output;

        return pin;
    }

    void NodeEditorNodeBase::DrawPins(const NodeEditorContext& ctx, const ImVec2& startCursorPosition)
    {
        for (auto& p: m_pPins)
        {
            ImGui::GetCurrentWindow()->DC.CursorPos = startCursorPosition;
            ed::BeginPin(p.m_Id, NodeEditorPinType::ToPinKind(p.m_Type));

            RenderPin(p);

            ed::EndPin();

            auto editor = reinterpret_cast<ed::Detail::EditorContext*>(ed::GetCurrentEditor());
            auto pinInternal = editor->GetPin(p.m_Id, NodeEditorPinType::ToPinKind(p.m_Type));

            if (!ctx.m_EnablePinSelect && !ctx.m_DraggingLink) // When node selection is disabled in select, we set the node to dead state, which disables its selection
            {
                pinInternal->m_IsLive = false;
            }
        }
    }
}