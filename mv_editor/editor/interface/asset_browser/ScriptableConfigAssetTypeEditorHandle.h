//
// Created by Matty on 20.07.2024.
//

#ifndef MV_EDITOR_SCRIPTABLECONFIGASSETTYPEEDITORHANDLE_H
#define MV_EDITOR_SCRIPTABLECONFIGASSETTYPEEDITORHANDLE_H

#include "AssetTypeEditorHandler.h"
#include "resources/common/ScriptConfigAssetType.h"

namespace Editor {
    class ScriptableConfigAssetTypeEditorHandle : public AssetTypeEditorHandler<MV::ScriptConfigAssetType>
    {
    public:
        static void OnAssetBrowserClicked(MV::UUID id);

        static void OnAssetBrowserRightClickPopup(MV::UUID id);
    };
}

#endif //MV_EDITOR_SCRIPTABLECONFIGASSETTYPEEDITORHANDLE_H
