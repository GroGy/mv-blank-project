//
// Created by Matty on 19.07.2024.
//

#ifndef MV_EDITOR_ASSETTYPEEDITORREGISTRY_H
#define MV_EDITOR_ASSETTYPEEDITORREGISTRY_H

#include "common/EngineDef.h"
#include <cstdint>
#include "common/Types.h"
#include "resources/common/AssetTypeBase.h"
#include "common/Assert.h"
#include "resources/common/AssetTypeRuntimeLookup.h"
#include "AssetTypeEditorHandler.h"
#include "resources/common/AssetTypeRegistry.h"
#include "resources/common/AssetType.h"
#include "asset/AssetUUID.h"

namespace Editor {
    using OnAssetFileRightClickPopupFn = eastl::function<void(MV::UUID)>;
    using OnAssetFileOpenFn = eastl::function<void(MV::UUID)>;
    using OnNewItemPopupFn = eastl::function<void(MV::string_view)>;

    class AssetTypeEditorRegistry
    {
        struct AssetTypeEditorInfo
        {
            uint16_t m_LookupID = (std::numeric_limits<uint16_t>::max)();

            OnAssetFileRightClickPopupFn m_OnRightClickPopupFn;
            OnAssetFileOpenFn m_OnFileOpenFn;
            OnNewItemPopupFn m_OnNewItemPopupFn;
        };
    public:

        static AssetTypeEditorRegistry& Get();

    public:

        template<typename T>
        FORCEINLINE void RegisterEditorAssetData()
        {
            static_assert(std::is_base_of<Editor::AssetTypeEditorHandlerBase, T>::value,
                          "T must be based from editor asset type handler base");
            static_assert(std::is_base_of<MV::AssetTypeBase, typename T::AssetType>::value,
                          "T::AssetType must be based from asset type base");

            using Type = T::AssetType;

            if(Registered<Type>()) return;

            registerImpl<Type>();

            const auto id = MV::AssetTypeRegistry::AssetTypeID<Type>::ID();

            auto& info = m_RuntimeLookup[id];

            info.m_OnFileOpenFn = [](MV::UUID id){ T::OnAssetBrowserClicked(id); };

            info.m_OnRightClickPopupFn = [](MV::UUID id){ T::OnAssetBrowserRightClickPopup(id); };

            info.m_OnNewItemPopupFn = [](MV::string_view directory){ T::OnAssetBrowserNewPopup(directory); };

        };

        template<typename AssetType>
        FORCEINLINE bool Registered() const
        {
            if (!MV::AssetTypeRegistry::AssetTypeID<AssetType>::Registered())
            {
                return false;
            }

            const auto id = MV::AssetTypeRegistry::AssetTypeID<AssetType>::ID();

            return m_RuntimeLookup.size() > id && m_RuntimeLookup[id].m_LookupID != (std::numeric_limits<uint16_t>::max)();
        }

        template<typename AssetType>
        AssetTypeEditorInfo GetEditorData() const {
            registerImpl<AssetType>();

            const auto id = MV::AssetTypeRegistry::AssetTypeID<AssetType>::ID();

            return m_RuntimeLookup[id];
        }

        AssetTypeEditorInfo GetEditorDataFromAssetType(const MV::AssetType& type) const {
            ENSURE_TRUE(type.m_LookupID != MV::AssetType::InvalidID, {});

            if(type.m_LookupID >= m_RuntimeLookup.size()) {
                return {};
            }

            return m_RuntimeLookup[type.m_LookupID];
        }

    private:

        template<typename AssetType>
        FORCEINLINE void registerImpl()
        {
            static_assert(std::is_base_of<MV::AssetTypeBase, AssetType>::value,
                          "T::AssetType must be based from asset type base");

            using Type = AssetType;

            if (!MV::AssetTypeRegistry::AssetTypeID<Type>::Registered())
            {
                MV::AssetTypeRegistry::Get().RegisterAssetType<Type>();
            }

            if (Registered<Type>()) {
                return;
            }

            const auto id = MV::AssetTypeRegistry::AssetTypeID<Type>::ID();

            auto info = AssetTypeEditorInfo{};

            if (m_RuntimeLookup.size() < id + 1)
            {
                m_RuntimeLookup.resize(id + 1);
            }

            info.m_LookupID = id;

            m_RuntimeLookup[id] = MV::move(info);
        };

    public:

        MV::vector<AssetTypeEditorInfo> m_RuntimeLookup;

    };

    template<typename T>
    class AssetTypeEditorRegistrator
    {
    public:
        AssetTypeEditorRegistrator() { Editor::AssetTypeEditorRegistry::Get().RegisterEditorAssetData<T>(); };
    };

#define REGISTER_EDITOR_ASSET_TYPE_HANDLER(AssetType) static ::Editor::AssetTypeEditorRegistrator<AssetType> CONCAT_TOKEN_ASSET_TYPE_NAME(s_EditorTypeRegistrator, __LINE__){};
}


#endif //MV_EDITOR_ASSETTYPEEDITORREGISTRY_H
