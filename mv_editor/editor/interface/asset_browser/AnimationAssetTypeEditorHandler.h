//
// Created by Matty on 20.07.2024.
//

#ifndef MV_EDITOR_ANIMATIONASSETTYPEEDITORHANDLER_H
#define MV_EDITOR_ANIMATIONASSETTYPEEDITORHANDLER_H

#include "AssetTypeEditorHandler.h"
#include "resources/common/ShaderAssetType.h"
#include "resources/common/AnimationAssetType.h"

namespace Editor {
    class AnimationAssetTypeEditorHandler : public AssetTypeEditorHandler<MV::AnimationAssetType>
    {
    public:
        static void OnAssetBrowserClicked(MV::UUID id);

        static void OnAssetBrowserRightClickPopup(MV::UUID id);
    };
}


#endif //MV_EDITOR_ANIMATIONASSETTYPEEDITORHANDLER_H
