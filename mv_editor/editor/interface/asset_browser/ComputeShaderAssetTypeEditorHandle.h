//
// Created by Matty on 20.07.2024.
//

#ifndef MV_EDITOR_COMPUTESHADERASSETTYPEEDITORHANDLE_H
#define MV_EDITOR_COMPUTESHADERASSETTYPEEDITORHANDLE_H

#include "AssetTypeEditorHandler.h"
#include "resources/common/ComputeShaderAssetType.h"

namespace Editor {
    class ComputeShaderAssetTypeEditorHandle : public AssetTypeEditorHandler<MV::ComputeShaderAssetType>
    {
    public:
        static void OnAssetBrowserClicked(MV::UUID id);

        static void OnAssetBrowserRightClickPopup(MV::UUID id);

        static void OnAssetBrowserNewPopup(MV::string_view currentDirectory);
    };
}



#endif //MV_EDITOR_COMPUTESHADERASSETTYPEEDITORHANDLE_H
