//
// Created by Matty on 20.07.2024.
//

#ifndef MV_EDITOR_TILESETASSETTYPEEDITORHANDLE_H
#define MV_EDITOR_TILESETASSETTYPEEDITORHANDLE_H

#include "AssetTypeEditorHandler.h"
#include "resources/common/TilesetAssetType.h"

namespace Editor {
    class TilesetAssetTypeEditorHandle : public AssetTypeEditorHandler<MV::TilesetAssetType>
    {
    public:
        static void OnAssetBrowserClicked(MV::UUID id);

        static void OnAssetBrowserRightClickPopup(MV::UUID id);
    };
}

#endif //MV_EDITOR_TILESETASSETTYPEEDITORHANDLE_H
