//
// Created by Matty on 20.07.2024.
//

#ifndef MV_EDITOR_TILEMAPASSETTYPEEDITORHANDLE_H
#define MV_EDITOR_TILEMAPASSETTYPEEDITORHANDLE_H

#include "AssetTypeEditorHandler.h"
#include "resources/common/TilemapAssetType.h"

namespace Editor {
    class TilemapAssetTypeEditorHandle : public AssetTypeEditorHandler<MV::TilemapAssetType>
    {
    public:
        static void OnAssetBrowserClicked(MV::UUID id);

        static void OnAssetBrowserRightClickPopup(MV::UUID id);
    };
}

#endif //MV_EDITOR_TILEMAPASSETTYPEEDITORHANDLE_H
