//
// Created by Matty on 18.07.2024.
//

#ifndef MV_EDITOR_ASSETTYPEEDITORHANDLER_H
#define MV_EDITOR_ASSETTYPEEDITORHANDLER_H

#include "asset/AssetUUID.h"
#include "common/Types.h"

namespace Editor {
    class AssetTypeEditorHandlerBase
    {
    public:

        static void OnAssetBrowserClicked(MV::UUID id) {};

        static void OnAssetBrowserRightClickPopup(MV::UUID id) {};

        static void OnAssetBrowserNewPopup(MV::string_view currentDirectory) {};
    };

    template<typename T>
    class AssetTypeEditorHandler : public AssetTypeEditorHandlerBase {
    public:
        using AssetType = T;
    };
}



#endif //MV_EDITOR_ASSETTYPEEDITORHANDLER_H
