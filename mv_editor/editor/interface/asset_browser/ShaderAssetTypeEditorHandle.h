//
// Created by Matty on 20.07.2024.
//

#ifndef MV_EDITOR_SHADERASSETTYPEEDITORHANDLE_H
#define MV_EDITOR_SHADERASSETTYPEEDITORHANDLE_H

#include "AssetTypeEditorHandler.h"
#include "resources/common/ShaderAssetType.h"

namespace Editor {
    class ShaderAssetTypeEditorHandle : public AssetTypeEditorHandler<MV::ShaderAssetType>
    {
    public:
        static void OnAssetBrowserClicked(MV::UUID id);

        static void OnAssetBrowserRightClickPopup(MV::UUID id);
    };
}

#endif //MV_EDITOR_SHADERASSETTYPEEDITORHANDLE_H
