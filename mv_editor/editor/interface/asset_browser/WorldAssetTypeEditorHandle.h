//
// Created by Matty on 20.07.2024.
//

#ifndef MV_EDITOR_WORLDASSETTYPEEDITORHANDLE_H
#define MV_EDITOR_WORLDASSETTYPEEDITORHANDLE_H

#include "AssetTypeEditorHandler.h"
#include "resources/common/WorldAssetType.h"

namespace Editor {
    class WorldAssetTypeEditorHandle : public AssetTypeEditorHandler<MV::WorldAssetType>
    {
    public:
        static void OnAssetBrowserClicked(MV::UUID id);

        static void OnAssetBrowserRightClickPopup(MV::UUID id);
    };
}


#endif //MV_EDITOR_WORLDASSETTYPEEDITORHANDLE_H
