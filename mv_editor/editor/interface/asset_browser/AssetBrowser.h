//
// Created by Matty on 2021-10-29.
//

#ifndef MV_EDITOR_ASSETBROWSER_H
#define MV_EDITOR_ASSETBROWSER_H

#include <filesystem>
#include "asset/AssetUUID.h"
#include "common/Types.h"
#include "interface/common/Window.h"
#include "interface/common/ImagePreview.h"
#include "resources/common/Asset.h"

namespace Editor {
    class FileBrowser;

    namespace Config {
        static constexpr MV::string_view sc_AssetBrowserWindowName = "Asset browser";
        static constexpr MV::string_view sc_CreateAssetPopup = "##create_asset_popup_asset_browser";
        static constexpr MV::string_view sc_AssetContextMenuPopup = "##asset_context_menu";
        static constexpr float sc_AssetMaxSize = 64.0f;
    }

    struct FileSystemEntry {
        enum class EntryType {
            Directory,
            File
        };

        MV::vector<FileSystemEntry> m_Entries;

        MV::string m_Name;

        EntryType m_Type;

        std::filesystem::path m_Path;

        MV::string m_AssetIcon;
        MV::AssetType m_AssetType;
        MV::UUID m_ID = MV::InvalidUUID;
        FileSystemEntry * m_Parent = nullptr;
    };

    class [[MV::Class(EditorWindow)]] AssetBrowser : public Window {
    public: // CTor

        AssetBrowser();

    public: // Public API

        void Refresh();

    protected: // Window overrides

        void WindowSetup() override;

        void Render() override;

        void PostRender() override;

        void OnCleanup() override;

        void OnInit() override;

        void OnOSDragAndDropEvent(OSDragAndDropEvent& event) override;

    public: // Image preview, used for previewing textures

        ImagePreview m_ImagePreview;

    private: // Rendering helper functions

        void renderCreateAssetPopup();

        void renderEditAssetPopup();

        void treeAddDirectory(const std::filesystem::path & path);

        /*
        void drawPopupMenu(bool isAsset, MV::AssetType type, uint64_t assetID, bool isAssetInProject, const std::filesystem::path & entryPath);
*/
        void drawPopupNoItemClicked();

    private: // Helper functions

        void rebuildCache();

        void populateFSEntry(FileSystemEntry & root);

    private: // File system fields

        std::filesystem::path m_pCurrentFolder;
        std::filesystem::path m_pRootFolder;
        std::filesystem::path m_pSelected;

        FileSystemEntry m_pCache;
        bool m_pCacheDirty = true;

        FileSystemEntry* m_pCurrentLocation = nullptr;

    private: // General internal fields

        bool m_pLoaded = false;

        MV::string m_pSearchVal;

        MV::string m_pCreateFolderName;

    };
}


#endif //MV_EDITOR_ASSETBROWSER_H
