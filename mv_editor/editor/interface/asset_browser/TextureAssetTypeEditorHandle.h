//
// Created by Matty on 19.07.2024.
//

#ifndef MV_EDITOR_TEXTUREASSETTYPEEDITORHANDLE_H
#define MV_EDITOR_TEXTUREASSETTYPEEDITORHANDLE_H

#include "AssetTypeEditorHandler.h"
#include "resources/common/TextureAssetType.h"

namespace Editor {
    class TextureAssetTypeEditorHandle : public AssetTypeEditorHandler<MV::TextureAssetType>
    {
    public:
        static void OnAssetBrowserClicked(MV::UUID id);

        static void OnAssetBrowserRightClickPopup(MV::UUID id);
    };
}



#endif //MV_EDITOR_TEXTUREASSETTYPEEDITORHANDLE_H
