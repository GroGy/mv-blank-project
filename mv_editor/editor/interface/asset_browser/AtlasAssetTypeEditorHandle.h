//
// Created by Matty on 20.07.2024.
//

#ifndef MV_EDITOR_ATLASASSETTYPEEDITORHANDLE_H
#define MV_EDITOR_ATLASASSETTYPEEDITORHANDLE_H

#include "AssetTypeEditorHandler.h"
#include "resources/common/AtlasAssetType.h"

namespace Editor {
    class AtlasAssetTypeEditorHandle : public AssetTypeEditorHandler<MV::AtlasAssetType>
    {
    public:
        static void OnAssetBrowserClicked(MV::UUID id);

        static void OnAssetBrowserRightClickPopup(MV::UUID id);
    };
}


#endif //MV_EDITOR_ATLASASSETTYPEEDITORHANDLE_H
