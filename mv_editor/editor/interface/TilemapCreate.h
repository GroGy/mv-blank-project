//
// Created by Matty on 08.12.2022.
//

#ifndef MV_EDITOR_TILEMAPCREATE_H
#define MV_EDITOR_TILEMAPCREATE_H

#include <resources/AtlasResource.h>
#include <common/EngineCommon.h>
#include "common/Window.h"
#include "../async/TilesetTask.h"
#include "common/AssetSelector.h"
#include "../imgui/imgui.h"
#include "interface/common/AssetGenerator.h"
#include "async/TilemapTask.h"
#include "resources/TilesetResource.h"

namespace Editor {
    namespace Config {
        static constexpr MV::string_view sc_TilemapCreateWindowName = "Tilemap Create";
    }

    class [[MV::Class(EditorWindow)]] TilemapCreate : public Window, public IAssetGenerator {
    public:
        TilemapCreate();
    protected:
        void WindowSetup() override;

        void Render() override;

        void PostRender() override;
    public:
        void OnCleanup() override;

        void OnInit() override;
    private:
        MV::rc<TilemapTask> m_pTask;
        bool m_pCreating = false;

        uint64_t m_pSelectedTileset = 0;

        MV::rc<MV::TilesetResource> m_pSelectedTilesetResource;
        AssetSelector m_AssetSelector;
        MV::string m_pName;
        MV::string m_pPath;
        MV::string m_pError;

        uint32_t m_pWidth = 128, m_pHeight = 128;
    };
}

#endif //MV_EDITOR_TILEMAPCREATE_H
