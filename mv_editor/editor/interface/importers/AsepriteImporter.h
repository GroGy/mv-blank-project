//
// Created by Matty on 2021-10-28.
//

#ifndef MV_EDITOR_ASEPRITEIMPORTER_H
#define MV_EDITOR_ASEPRITEIMPORTER_H

#include <interface/common/AssetGenerator.h>
#include <interface/common/FileBrowser.h>
#include "../../async/AsepriteTask.h"
#include "../common/Window.h"
#include "../../imgui/imgui.h"
#include "../../imgui/imfilebrowser.h"
#include "../common/AssetSelector.h"

namespace Editor {
    namespace Config {
        static constexpr MV::string_view sc_AsepriteImportWindowName = "Aseprite importer";
    }

    class [[MV::Class(EditorWindow)]] AsepriteImporter : public Window , public IAssetGenerator {
    private:
        MV::rc<AsepriteTask> m_pTask;
        bool m_pImporting = false;
    public:
        uint64_t m_Texture = 0;
        AssetSelector m_TextureSelector;
        MV::rc<FileBrowser> m_JsonFileDialog{};
        MV::string m_JsonPath;
        AsepriteImporter();
    protected:
        void WindowSetup() override;

        void Render() override;
    public:
        void OnCleanup() override;

        void OnInit() override;
    protected:
        void PostRender() override;
    };
}


#endif //MV_EDITOR_ASEPRITEIMPORTER_H
