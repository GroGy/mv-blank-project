//
// Created by Matty on 2021-10-28.
//

#ifndef MV_EDITOR_TEXTUREIMPORTER_H
#define MV_EDITOR_TEXTUREIMPORTER_H

#include <interface/common/AssetGenerator.h>
#include "interface/common/Window.h"
#include "async/TextureTask.h"
#include "imgui/imgui.h"
#include "imgui/imfilebrowser.h"
#include "interface/common/FileBrowser.h"

namespace Editor {
    namespace Config {
        static constexpr MV::string_view sc_TextureImportWindowName = "Texture importer";
    }

    class [[MV::Class(EditorWindow)]] TextureImporter : public Window, public IAssetGenerator {
    public: //CTor

        TextureImporter();

    protected: // Overrides

        void WindowSetup() override;

        void Render() override;

        void OnCleanup() override;

        void OnInit() override;

        void PostRender() override;

        void OnOpenStateChanged(bool newValue) override;

    public: // Public API

        void OpenForReimport(MV::UUID asset);

    public:

        MV::rc<FileBrowser> m_TextureFileDialog;

        MV::string m_FileDialogError = "Invalid file!";

        bool m_ShowFileError = false;

        MV::vector<MV::string> m_Paths;

        MV::string m_pError;

    private:

        MV::rc<TextureTask> m_pTask;

        bool m_pImporting = false;

        MV::UUID m_pReimportAsset = MV::InvalidUUID;

    };
}


#endif //MV_EDITOR_TEXTUREIMPORTER_H
