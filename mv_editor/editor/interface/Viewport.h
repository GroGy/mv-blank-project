//
// Created by Matty on 2022-02-20.
//

#ifndef MV_EDITOR_VIEWPORT_H
#define MV_EDITOR_VIEWPORT_H

#include "asset/AssetUUID.h"
#include "render/common/FrameContext.h"
#include "../imgui/imgui.h"
#include "common/WorldObject.h"
#include "../properties/PropertyGizmoManager.h"
#include "interface/common/ViewportWidget.h"
#include "world_picking/ViewportCamera.h"

namespace Editor {
    class Viewport {
    public:
        void Init();

        void Render();

        void OnResize();

    private: // Internal fields

        ViewportWidget<MousePickingBehavior::Disabled> m_ViewportWidget;

    private: // Drawing helper functions

    private: // Focus helper function

        void onFocused();

    private: // Focus tracking

        bool m_pWasFocused = false;

        uint64_t m_pSelectedWorldObject = 0;
    };

    /*
     * */
}


#endif //MV_EDITOR_VIEWPORT_H
