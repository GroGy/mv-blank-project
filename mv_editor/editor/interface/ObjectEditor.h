//
// Created by Matty on 2022-02-13.
//

#ifndef MV_EDITOR_OBJECTEDITOR_H
#define MV_EDITOR_OBJECTEDITOR_H

#include "common/Window.h"
#include "resources/WorldData.h"
#include "common/WorldObject.h"
#include "common/World.h"

namespace Editor {
    namespace Config {
        static constexpr MV::string_view sc_ObjectEditorWindowName = "Object editor";
    }

    class [[MV::Class(EditorWindow)]] ObjectEditor  : public Window {
    public:
        ObjectEditor();

        void OnCleanup() override;

        void OnInit() override;

    protected:

        bool ShouldRender() const override;

        void WindowSetup() override;

        void Render() override;

        void PostRender() override;

    private:

        void drawObjectSettings(uint64_t id);

        void drawObjectSettingsData(uint64_t id);

        void drawGeneralWorldObjectFields(MV::World& world, const MV::rc<MV::WorldObject>& wo) const;

        void drawObjectPhysicsSettings(MV::World& world, const MV::rc<MV::WorldObject>& wo);

    private:

        MV::Meta::Class m_pCachedMeta;

        uint64_t m_pCachedObjectId = 0;

        bool m_pFetchedData = false;
    };
}


#endif //MV_EDITOR_OBJECTEDITOR_H
