//
// Created by Matty on 2022-01-23.
//

#ifndef MV_EDITOR_EDITORSETTINGS_H
#define MV_EDITOR_EDITORSETTINGS_H

#include "common/Window.h"

namespace Editor {
    namespace Config {

    }

    //Deprecated, dont use, useless
    class EditorSettings : public Window {
    public:
        EditorSettings();

    protected:
        void WindowSetup() override;

        void Render() override;

        void PostRender() override;

    public:
        void OnCleanup() override;

        void OnInit() override;
    };
}


#endif //MV_EDITOR_EDITORSETTINGS_H
