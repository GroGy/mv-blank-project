//
// Created by Matty on 2021-10-15.
//

#ifndef MV_EDITOR_PERFORMANCE_H
#define MV_EDITOR_PERFORMANCE_H

#include "common/Window.h"

namespace Editor {
    namespace Config {
        static constexpr MV::string_view sc_PerformanceWindowName = "Performance";
    }

    class [[MV::Class(EditorWindow)]] Performance : public Window {
    public:
        Performance();
    protected:
        void WindowSetup() override;

        void Render() override;

        void PostRender() override;

    public:
        void OnCleanup() override;

        void OnInit() override;
    };
}


#endif //MV_EDITOR_PERFORMANCE_H
