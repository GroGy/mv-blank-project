//
// Created by Matty on 19.08.2023.
//

#ifndef MV_EDITOR_ANIMATIONCREATE_H
#define MV_EDITOR_ANIMATIONCREATE_H

#include "common/Window.h"
#include "async/AnimationTask.h"
#include "interface/common/AssetGenerator.h"

namespace Editor
{
    namespace Config {
        static constexpr MV::string_view sc_AnimationCreateWindowName = "Create animation";
    }

    class [[MV::Class(EditorWindow)]] AnimationCreate : public Window, public IAssetGenerator {
    public:
        AnimationCreate();

    public:

        void WindowSetup() override;

        void Render() override;

        void PostRender() override;

        void OnCleanup() override;

        void OnInit() override;

    private: // Task helper functions

        void startTask();

        void processTask();

    private: // Drawing functions

        void drawFields();

    public:

        MV::string m_pName;

        MV::string m_pError;

    private:
        MV::rc<AnimationTask> m_pTask;

        bool m_pCreating = false;
    };
}


#endif //MV_EDITOR_ANIMATIONCREATE_H
