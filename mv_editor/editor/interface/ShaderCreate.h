//
// Created by Matty on 2022-03-13.
//

#ifndef MV_EDITOR_SHADERCREATE_H
#define MV_EDITOR_SHADERCREATE_H

#include "common/Window.h"
#include "../async/ShaderTask.h"

namespace Editor {
    namespace Config {
        static constexpr MV::string_view sc_ShaderCreateWindowName = "Create shader";
    }

    class [[MV::Class(EditorWindow)]] ShaderCreate : public Window {
    public:
        ShaderCreate();

        MV::string m_pName;
        MV::string m_pError;

        void SetPath(MV::string_view path);
        void WindowSetup() override;

        void Render() override;

        void PostRender() override;

        void OnCleanup() override;

        void OnInit() override;

    private:
        MV::rc<ShaderTask> m_pTask;
        bool m_pCreating = false;

        MV::string m_pPathBase;
    };
}


#endif //MV_EDITOR_SHADERCREATE_H
