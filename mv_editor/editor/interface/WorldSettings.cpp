//
// Created by Matty on 28.07.2024.
//

#include "WorldSettings.h"
#include "util/UtilGui.h"
#include "interface/object_editor/common/FieldRenderer.h"
#include "common/World.h"
#include "common/subsystem/Subsystem.h"
#include "EditorCommon.h"

namespace Editor
{

    WorldSettings::WorldSettings() : Window(Config::sc_WorldSettingsWindowName.data(), false)
    {
        m_pOpen = false;
    }

    void WorldSettings::OnCleanup()
    {

    }

    void WorldSettings::OnInit()
    {

    }

    void WorldSettings::WindowSetup()
    {

    }

    void WorldSettings::Render()
    {
        if (m_pSubsystemClass.IsValid())
        {
            drawSubsystemSettings();
        } else
        {
            drawWorldSettings();
        }
    }

    void WorldSettings::PostRender()
    {

    }

    void WorldSettings::SetWorld(MV::World* World)
    {
        m_pWorld = World;
    }

    void WorldSettings::SetSubsystemToDraw(MV::Meta::Class subsystemClass)
    {
        m_pSubsystemClass = subsystemClass;
    }

    void WorldSettings::drawWorldSettings()
    {

    }

    void WorldSettings::drawSubsystemSettings()
    {
        ENSURE_TRUE(m_pSubsystemClass.IsValid());

        ImGui2::LabeledSeparator("Subsystem");

        if(!Begin("Subsystem")) return;

        const auto worldSubsystem = m_pWorld->GetWorldSubsystemByMetaClass(m_pSubsystemClass);

        if(!worldSubsystem)
        {
            ImGui::TextUnformatted("Instance of subsystem not created on world, woops");
            End();
        }

        const auto& fields = m_pSubsystemClass.GetFields();

        uint32_t counter = 0;

        for (auto& f: fields)
        {
            if(BeginField(f.m_Name)) {
                const bool dirty = (FieldRendererProxy::Draw(f, counter, worldSubsystem, m_pSubsystemClass));
                EndField();

                if (dirty)
                {
                    GetEditor()->m_DirtyAssets.emplace(m_pWorld->m_ID);
                }
            }
        }

        End();
    }
}