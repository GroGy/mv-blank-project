//
// Created by Matty on 07.12.2022.
//

#ifndef MV_EDITOR_PROJECTSETTINGS_H
#define MV_EDITOR_PROJECTSETTINGS_H

#include "common/Window.h"
#include "common/Project.h"
#include "interface/common/ClassSelector.h"

namespace Editor {
    namespace Config {
        static constexpr MV::string_view sc_ProjectSettingsWindowName = "Project Settings";
    }

    class [[MV::Class(EditorWindow)]] ProjectSettings : public Window {
    public:

        ProjectSettings();

    public: // Overrides

        void WindowSetup() override;

        void Render() override;

        void PostRender() override;

        void OnCleanup() override;

        void OnInit() override;

    public: // Tab drawing functions

        void drawTabSelect();

        void drawGeneralSettings();

        void drawInputSettings();

        void drawRenderingSettings();

        void drawCurrentWorldSettings();

    private: // Project saving / loading

        void applyChanges();

        void resetChanges();

    private: // Primitive fields

        bool m_pHasLoadedProject = false;

        MV::Project m_pEditedProject;

        struct Tab {
            MV::string m_Name;
            eastl::function<void()> m_Callback;
        };

        MV::vector<Tab> m_pTabs;

        uint32_t m_pSelectedTab = 0;

    private: // Helper classes

        ClassSelector m_pClassSelector;
    };
}


#endif //MV_EDITOR_PROJECTSETTINGS_H
