//
// Created by Matty on 2022-01-22.
//

#ifndef MV_EDITOR_ATLASCREATE_H
#define MV_EDITOR_ATLASCREATE_H

#include <resources/TextureResource.h>
#include "common/Window.h"
#include "../async/TilesetTask.h"
#include "common/AssetSelector.h"
#include "../imgui/imgui.h"
#include "../async/AtlasTask.h"
#include "interface/common/AssetGenerator.h"

namespace Editor {
    namespace Config {
        static constexpr MV::string_view sc_AtlasCreateWindowName = "Create atlas";
    }

    class [[MV::Class(EditorWindow)]] AtlasCreate : public Window , public IAssetGenerator {
    public:
        AtlasCreate();

    public: // Overrides from Window

        void WindowSetup() override;

        void Render() override;

        void PostRender() override;

        void OnCleanup() override;

        void OnInit() override;

    private: // Internal helper functions

        void processTexturePreview();

        // Assigns imgui texture from m_pTexture field
        void assignImGuiTexture();

    private: // Internal fields
        MV::rc<AtlasTask> m_pTask;

        bool m_pCreating = false;

        uint64_t m_pTexture = 0;

        MV::string m_pName;

        MV::string m_pError;

        bool m_pGenerateSprites = false;

        glm::uvec2 m_pSpriteSize{0,0};

        uint32_t m_pSpriteCount = 0;

    private: // Imgui fields

        AssetSelector m_pAssetSelector;

        MV::rc<MV::TextureResource> m_pTextureLoadResource;

        ImTextureID m_pImGuiTexture;

        enum TexturesState {
            Unloaded,
            Pending,
            Loaded
        };

        TexturesState m_pTextureState = Unloaded;

        ImVec2 m_pTextureSize = {0.0f,0.0f};
    };
}


#endif //MV_EDITOR_ATLASCREATE_H
