//
// Created by Matty on 21.01.2023.
//

#ifndef MV_EDITOR_FIELDRENDERER_H
#define MV_EDITOR_FIELDRENDERER_H

#include <EngineMinimal.h>
#include "resources/WorldData.h"
#include "common/WorldObject.h"
#include "serialization/json/JsonField.h"
#include "meta/Field.h"
#include "meta/Class.h"

#undef DrawText

namespace Editor {
    struct PropertyType {};
    struct ClassReferenceType {};
    struct ArrayType {};
    struct AssetReferenceType {};

    namespace FieldDrawingImpl {
        void DrawText(MV::string_view text);

        bool DrawCombo(MV::string_view internalName,const char** fields, uint32_t fieldCount ,uint32_t & selectedIndex);

        bool DrawCombo(MV::string_view internalName,const MV::vector<MV::string> & fields,uint32_t & selectedIndex);
    };

    class FieldRendererHelper {
    public: // Types

        using DrawingCallback = eastl::function<bool(const MV::Meta::Field&, uint32_t&,
                                                     const MV::rc<MV::EngineObject>&, void*,
                                                     const MV::Meta::Class&)>;

    public:
        friend class EngineInternalFieldRendererHelper;

        explicit FieldRendererHelper(const DrawingCallback & drawingCallback);

    public: // Public API, used by base code

        bool Draw(const MV::Meta::Field& field,
                  uint32_t& fieldCounter,
                  const MV::rc<MV::EngineObject>& obj,
                  void* objPtr,
                  const MV::Meta::Class& klass);

    private: // Helper functions

        static bool Begin(MV::string_view name);

        static void End();

    private: // Internal fields

        DrawingCallback m_pDrawingCallback;
    };

    // Used for drwaing internal fields, like position and rotation of world object,
    // These field dont have class field, just name
    class EngineInternalFieldRendererHelper : public FieldRendererHelper {
    public: // Types

        using NoFieldDrawingCallback = eastl::function<bool(MV::string_view,
                                                     const MV::rc<MV::EngineObject>&, const MV::Meta::Class&)>;

    public: // Ctor

        explicit EngineInternalFieldRendererHelper(NoFieldDrawingCallback callback);

    public:

        bool Draw(MV::string_view fieldName,
                  const MV::rc<MV::EngineObject>& obj,
                  const MV::Meta::Class& klass);

    private: // Internal fields

        NoFieldDrawingCallback m_pNoFieldDrawingCallback;

    };

    class IFieldRenderer {
    protected:
        static bool IsRunningGame();
    };

    template<typename Owner>
    class FieldRendererBase : public IFieldRenderer {
    public:
        static bool DrawImpl(const MV::Meta::Field& field,
                             uint32_t& fieldCounter,
                             const MV::rc<MV::EngineObject>& obj,
                             void* objPtr,
                             const MV::Meta::Class& klass) {
            static FieldRendererHelper fr{[](const MV::Meta::Field& f_, uint32_t& fc_,
                                             const MV::rc<MV::EngineObject>& obj_, void* objPtr_, const MV::Meta::Class& klass_){
                return Owner::DrawField(f_, fc_, obj_, objPtr_, klass_);
            }};
            return fr.Draw(field, fieldCounter, obj, objPtr, klass);
        }

        template <typename T>
        static bool Draw(const MV::Meta::Field& field,
                         uint32_t& fieldCounter,
                         const MV::rc<T>& obj,
                         void* objPtr,
                         const MV::Meta::Class& klass) {
            static_assert(std::is_base_of<MV::EngineObject, T>::value, "T must be engine object");

            return DrawImpl(field, fieldCounter, obj, objPtr, klass);
        }
    };

    template<typename T>
    class FieldRenderer : public FieldRendererBase<FieldRenderer<T>> {
    private:
        friend class FieldRendererBase<FieldRenderer<T>>;

        static bool DrawField(const MV::Meta::Field& field,
                              uint32_t& fieldCounter,
                              const MV::rc<MV::EngineObject>& obj,
                              void* objPtr,
                              const MV::Meta::Class& klass) {
            FieldDrawingImpl::DrawText(fmt::format("Not implemented for type: {}", typeid(T).name()).c_str());
            return false;
        }
    };


    class FieldRendererProxy {
    public:
        template <typename T>
        static bool Draw(const MV::Meta::Field& field,
                         uint32_t& fieldCounter,
                         const MV::rc<T>& obj,
                         const MV::Meta::Class& klass) {
            static_assert(std::is_base_of<MV::EngineObject, T>::value, "T must be engine object");
            return DrawImpl(field, fieldCounter, obj, obj.get(), klass);
        }

        static bool DrawImpl(const MV::Meta::Field& field,
                         uint32_t& fieldCounter,
                         const MV::rc<MV::EngineObject>& obj,
                         void* objPtr,
                         const MV::Meta::Class& klass);
    };
}


#endif //MV_EDITOR_FIELDRENDERER_H
