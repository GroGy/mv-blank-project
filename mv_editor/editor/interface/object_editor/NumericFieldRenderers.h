//
// Created by Matty on 21.01.2023.
//

#ifndef MV_EDITOR_NUMERICFIELDRENDERERS_H
#define MV_EDITOR_NUMERICFIELDRENDERERS_H

#include "common/FieldRenderer.h"
#include "common/World.h"
#include "meta/MetaCache.h"

namespace Editor {
    namespace FieldDrawingImpl {

        bool DrawFloat(MV::string_view internalName, float &value, MV::optional<float> min = {}, MV::optional<float> max = {});

        bool DrawFloatWithOverride(MV::string_view internalName, float &value, bool & override);

        bool DrawVec2(MV::string_view internalName, glm::vec2 &value, MV::optional<float> min = {}, MV::optional<float> max = {});

        bool DrawUVec2(MV::string_view internalName, glm::uvec2 &value, MV::optional<uint32_t> min = {}, MV::optional<uint32_t> max = {});

        bool DrawU32(MV::string_view internalName, uint32_t &value, MV::optional<uint32_t> min = {}, MV::optional<uint32_t> max = {});

        bool DrawI32(MV::string_view internalName, int32_t &value, MV::optional<int32_t> min = {}, MV::optional<int32_t> max = {});
    };

    template<>
    class FieldRenderer<float> : public FieldRendererBase<FieldRenderer<float>> {
    private:
        friend class FieldRendererBase<FieldRenderer<float>>;

        static bool DrawField(const MV::Meta::Field&field,
                              uint32_t &fieldCounter,
                              const MV::rc<MV::EngineObject> &obj,
                              void* objPtr,
                              const MV::Meta::Class& klass) {
            float value;

            const bool success = klass.GetField(objPtr, field, value);

            CHECK(success);

            const bool changed = FieldDrawingImpl::DrawFloat(
                    fmt::format("##f32_{}_{}", field.m_Name, fieldCounter++).c_str(), value);

            if (changed)
            {
                klass.SetField(objPtr, field, value);
            }

            return changed;
        }
    };

    template<>
    class FieldRenderer<glm::vec2> : public FieldRendererBase<FieldRenderer<glm::vec2>> {
    private:
        friend class FieldRendererBase<FieldRenderer<glm::vec2>>;

        static bool DrawField(const MV::Meta::Field&field,
                              uint32_t &fieldCounter,
                              const MV::rc<MV::EngineObject> &obj,
                              void* objPtr,
                              const MV::Meta::Class& klass) {
            return false;
        }
    };

    template<>
    class FieldRenderer<glm::uvec2> : public FieldRendererBase<FieldRenderer<glm::uvec2>> {
    private:
        friend class FieldRendererBase<FieldRenderer<glm::uvec2>>;

        static bool DrawField(const MV::Meta::Field&field,
                              uint32_t &fieldCounter,
                              const MV::rc<MV::EngineObject> &obj,
                              void* objPtr,
                              const MV::Meta::Class& klass) {
            return false;
        }
    };

    template<>
    class FieldRenderer<uint32_t> : public FieldRendererBase<FieldRenderer<uint32_t>> {
    private:
        friend class FieldRendererBase<FieldRenderer<uint32_t>>;

        static bool DrawField(const MV::Meta::Field&field,
                              uint32_t &fieldCounter,
                              const MV::rc<MV::EngineObject> &obj,
                              void* objPtr,
                              const MV::Meta::Class& klass) {
            uint32_t value;

            const bool success = klass.GetField(objPtr, field, value);

            CHECK(success);

            const bool changed = FieldDrawingImpl::DrawU32(
                    fmt::format("##u32_{}_{}", field.m_Name, fieldCounter++).c_str(), value);

            if (changed)
            {
                klass.SetField(objPtr, field, value);
            }

            return changed;
        }
    };

    template<>
    class FieldRenderer<int32_t> : public FieldRendererBase<FieldRenderer<int32_t>> {
    private:
        friend class FieldRendererBase<FieldRenderer<int32_t>>;

        static bool DrawField(const MV::Meta::Field&field,
                              uint32_t &fieldCounter,
                              const MV::rc<MV::EngineObject> &obj,
                              void* objPtr,
                              const MV::Meta::Class& klass) {
            int32_t value;

            const bool success = klass.GetField(objPtr, field, value);

            CHECK(success);

            const bool changed = FieldDrawingImpl::DrawI32(
                    fmt::format("##i32_{}_{}", field.m_Name, fieldCounter++).c_str(), value);

            if (changed)
            {
                klass.SetField(objPtr, field, value);
            }

            return changed;
        }
    };
}


#endif //MV_EDITOR_NUMERICFIELDRENDERERS_H
