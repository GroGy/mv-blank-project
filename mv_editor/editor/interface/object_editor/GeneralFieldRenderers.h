//
// Created by Matty on 22.01.2023.
//

#ifndef MV_EDITOR_GENERALFIELDRENDERERS_H
#define MV_EDITOR_GENERALFIELDRENDERERS_H

#include "common/FieldRenderer.h"
#include <EngineMinimal.h>
#include "common/World.h"

namespace Editor {
    namespace FieldDrawingImpl {

        bool DrawBool(MV::string_view internalName, bool & value);

        bool DrawString(MV::string_view internalName, MV::string & value);

        bool DrawAssetReference(MV::string_view internalName, const MV::AssetTypeRuntimeLookup& type, MV::IAssetReference& value);
    };

    template<>
    class FieldRenderer<bool> : public FieldRendererBase<FieldRenderer<bool>> {
    private:
        friend class FieldRendererBase<FieldRenderer<bool>>;

        static bool DrawField(const MV::Meta::Field& field,
                              uint32_t& fieldCounter,
                              const MV::rc<MV::EngineObject>& obj,
                              void* objPtr,
                              const MV::Meta::Class& klass) {
            bool value;

            const bool success = klass.GetField(objPtr, field, value);

            CHECK(success);

            const bool changed = FieldDrawingImpl::DrawBool(
                    fmt::format("##bool_{}_{}", field.m_Name, fieldCounter++).c_str(), value);

            klass.SetField(objPtr, field, value);

            return changed;
        }
    };

    template<>
    class FieldRenderer<AssetReferenceType> : public FieldRendererBase<FieldRenderer<AssetReferenceType>> {
    private:
        friend class FieldRendererBase<FieldRenderer<AssetReferenceType>>;

        static bool DrawField(const MV::Meta::Field& field,
                              uint32_t& fieldCounter,
                              const MV::rc<MV::EngineObject>& obj,
                              void* objPtr,
                              const MV::Meta::Class& klass) {
            MV::IAssetReference value;

            const bool success = klass.GetField(objPtr, field, value);

            CHECK(success);



            const bool changed = FieldDrawingImpl::DrawAssetReference(fmt::format("##asset_ref_{}_{}", field.m_Name, fieldCounter++).c_str(), field.m_ReferencedAssetType, value);


            if (changed)
            {
                klass.SetField(objPtr, field, value);
            }

            return changed;
        }
    };


}


#endif //MV_EDITOR_GENERALFIELDRENDERERS_H
