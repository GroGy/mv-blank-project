//
// Created by Matty on 22.01.2023.
//

#ifndef MV_EDITOR_PROPERTYFIELDRENDERER_H
#define MV_EDITOR_PROPERTYFIELDRENDERER_H

#include "common/FieldRenderer.h"
#include <EngineMinimal.h>
#include "render/common/PropertyManager.h"

namespace Editor {
    struct EditorPropertyDrawingData {
        enum class Type {
            FromField,
            Direct
        };

        Type m_Type;

        const MV::Meta::Field* m_Field = nullptr;

        MV::string m_Name;
        MV::Property* m_Property = nullptr;

        static EditorPropertyDrawingData FromField(const MV::Meta::Field& field);

        static EditorPropertyDrawingData FromProperty(const MV::string& name, MV::Property* property);
    };

    namespace FieldDrawingImpl {
        void SetNextPropertyHeight(float size = -1);

        bool DrawProperty(EditorPropertyDrawingData data,
                          uint32_t& fieldCounter,
                          const MV::rc<MV::WorldObject>& wo,const MV::Meta::Class& klass, bool forceOpen = false);

    };

    template<>
    class FieldRenderer<PropertyType> : public FieldRendererBase<FieldRenderer<PropertyType>> {
    public:

        static bool DrawProperty(EditorPropertyDrawingData data,
                                 uint32_t& fieldCounter,
                                 const MV::rc<MV::EngineObject>& obj,
                                 void* objPtr,
                                 const MV::Meta::Class& klass, bool forceOpen = false) {
            auto meta = MV::Meta::MetaCache::GetInstance().GetClassMetaFromHash(obj->GetTypeHash());
            auto woMeta = MV::GetClassMeta<MV::WorldObject>();

            CHECK(MV::Meta::MetaCache::GetInstance().IsChildOf(&meta, &woMeta));

            bool changed = FieldDrawingImpl::DrawProperty(data, fieldCounter, MV::static_pointer_cast<MV::WorldObject>(obj), klass, forceOpen);

            return changed;
        };

        static void SetNextPropertyWindowHeight(float height) {
            FieldDrawingImpl::SetNextPropertyHeight(height);
        }

    private:
        friend class FieldRendererBase<FieldRenderer<PropertyType>>;

        static bool DrawField(const MV::Meta::Field& field,
                              uint32_t& fieldCounter,
                              const MV::rc<MV::EngineObject>& wo,
                              void* objPtr,
                              const MV::Meta::Class& klass) {

            /*
            auto& jsonField = worldData.m_WorldObjects[wo->m_WorldID].m_Data[field.m_Name.c_str()];*/

            if (IsRunningGame()) {
                bool changed = false;

                return changed;
            } else {
                bool changed = false;

                return changed;
            }
        }
    };

}


#endif //MV_EDITOR_PROPERTYFIELDRENDERER_H
