//
// Created by Matty on 2022-09-20.
//

#ifndef MV_EDITOR_BOXCOLLIDERPROPERTYRENDERER_H
#define MV_EDITOR_BOXCOLLIDERPROPERTYRENDERER_H

#include "common/Property.h"
#include "PropertyRendererBase.h"
#include "2d/BoxColliderProperty.h"

namespace Editor {
    class BoxColliderPropertyRenderer : public PropertyRenderer<MV::BoxColliderProperty> {
    public:
        static bool Draw(const MV::Meta::Field* field, const MV::rc<MV::WorldObject> & wo,MV::Property* property);
    };
}

#endif //MV_EDITOR_BOXCOLLIDERPROPERTYRENDERER_H
