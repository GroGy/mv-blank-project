//
// Created by Matty on 2022-03-11.
//

#ifndef MV_EDITOR_SPRITEPROPERTYRENDERER_H
#define MV_EDITOR_SPRITEPROPERTYRENDERER_H

#include "common/Property.h"
#include "PropertyRendererBase.h"
#include "2d/SpriteProperty.h"

namespace Editor {
    class [[MV::Class(PropertyRenderer)]] SpritePropertyRenderer : public PropertyRenderer<MV::SpriteProperty> {
    public:
        static bool Draw(const MV::Meta::Field* field, const MV::rc<MV::WorldObject> & wo,MV::Property* property);
    };
}


#endif //MV_EDITOR_SPRITEPROPERTYRENDERER_H
