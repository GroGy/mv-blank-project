//
// Created by Matty on 02.03.2023.
//

#ifndef MV_EDITOR_CAMERAPROPERTYRENDERER_H
#define MV_EDITOR_CAMERAPROPERTYRENDERER_H

#include "common/Property.h"
#include "PropertyRendererBase.h"
#include "2d/CameraProperty.h"

namespace Editor {
    class [[mv_property_renderer()]] CameraPropertyRenderer : public PropertyRenderer<MV::CameraProperty> {
    public:
        static bool Draw(const MV::Meta::Field* field, const MV::rc<MV::WorldObject> & wo,MV::Property* property);
    };
}



#endif //MV_EDITOR_CAMERAPROPERTYRENDERER_H
