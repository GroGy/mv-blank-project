//
// Created by Matty on 2022-09-20.
//

#ifndef MV_EDITOR_PARTICLESYSTEMPROPERTYRENDERER_H
#define MV_EDITOR_PARTICLESYSTEMPROPERTYRENDERER_H

#include "common/Property.h"
#include "PropertyRendererBase.h"
#include "particles/cpu/ParticleSystemProperty.h"

namespace Editor {
    class ParticleSystemPropertyRenderer : public PropertyRenderer<MV::ParticleSystemProperty> {
    public:
        static bool Draw(const MV::Meta::Field* field, const MV::rc<MV::WorldObject> & wo,MV::Property* property);

    private: // Helper functions

        static void drawSpriteRenderingFields(bool &dirty, MV::Property *data);

        static void drawSpriteSheetRenderingFields(bool &dirty, MV::Property *data);

        static void drawParticleSettings(bool &dirty, MV::Property *data);
    };
}

#endif //MV_EDITOR_PARTICLESYSTEMPROPERTYRENDERER_H
