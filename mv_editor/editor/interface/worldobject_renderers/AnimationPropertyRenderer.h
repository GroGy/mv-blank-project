//
// Created by Matty on 31.08.2023.
//

#ifndef MV_EDITOR_ANIMATIONPROPERTYRENDERER_H
#define MV_EDITOR_ANIMATIONPROPERTYRENDERER_H

#include "common/Property.h"
#include "PropertyRendererBase.h"
#include "animation/AnimationProperty.h"

namespace Editor {
    class [[MV::Class(PropertyRenderer)]] AnimationPropertyRenderer : public PropertyRenderer<MV::AnimationProperty> {
    public:
        static bool Draw(const MV::Meta::Field* field, const MV::rc<MV::WorldObject> & wo,MV::Property* property);
    };
}



#endif //MV_EDITOR_ANIMATIONPROPERTYRENDERER_H
