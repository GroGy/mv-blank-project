//
// Created by Matty on 2022-09-27.
//

#ifndef MV_EDITOR_PROPERTYRENDERERBASE_H
#define MV_EDITOR_PROPERTYRENDERERBASE_H

#include "imgui/imgui.h"
#include "imgui/imgui_internal.h"
#include "animation/AnimationInput.h"
#include "render/common/IRenderedProperty.h"
#include <EngineMinimal.h>
#include "serialization/common/Archive.h"
#include <meta/EngineObject.h>

namespace Editor
{
    class IPropertyRendererHelper {
    public:

        using OnAnimableKeyframeCreateCallback = eastl::function<void(MV::AnimationInputField*)>;
        using IsFieldSerializationEnabledCallback = eastl::function<bool(MV::Archive::ValueId)>;
        using EnableFieldSerializationCallback = eastl::function<void(MV::Archive::ValueId, bool)>;

        static OnAnimableKeyframeCreateCallback s_OnCreateKeyframeCallback;
        static IsFieldSerializationEnabledCallback s_KeySerializationEnabledCallback;
        static EnableFieldSerializationCallback s_EnableFieldSerializationCallback;
        static bool s_ShowCreateKeyframeButton;

    public: // Base rendering wrapper

        static bool Begin(MV::string_view name);

        static void End();

    public: // Field drawing

        static bool BeginField(MV::string_view name, MV::Archive::ValueId valueId = 0, MV::AnimationInputField* animationData = nullptr);

        static void EndField();
    };

    class [[MV::Trivial()]] PropertyRendererBase : public MV::EngineObject, public IPropertyRendererHelper
    {
    private:
        static void drawSettingFieldImpl(bool& dirtyOut, MV::string_view wrapperName, MV::string_view title, void* data,
                                         ImGuiDataType dataType, MV::string_view format = "", float speed = 1.0f,
                                         float* min = nullptr, float* max = nullptr);

        static void
        drawSettingFieldVec2Impl(bool& dirtyOut, MV::string_view wrapperName, MV::string_view title, glm::vec2* data);

    protected:

        template<typename T>
        static inline void drawSettingField(bool& dirtyOut, MV::string_view wrapperName, MV::string_view title, T* data,
                                            MV::string_view format = "", float speed = 1.0f, float* min = nullptr,
                                            float* max = nullptr);

    };

    template<typename T>
    class [[MV::Class()]] PropertyRenderer : public PropertyRendererBase
    {
    public:

        using PropertyType = T;


    };

    using EditorRendererHelper = IPropertyRendererHelper;

    template<typename T>
    inline void
    PropertyRendererBase::drawSettingField(bool& dirtyOut, MV::string_view wrapperName, MV::string_view title,
                                           T* data, MV::string_view format, float speed, float* min, float* max)
    {
        ImGui::Text("Renderer for type %s is not implemented. (%s)", typeid(T).name(), title.data());
    }

    template<>
    inline void
    PropertyRendererBase::drawSettingField<uint32_t>(bool& dirtyOut, MV::string_view wrapperName, MV::string_view title,
                                                     uint32_t* data, MV::string_view format, float speed, float* min,
                                                     float* max)
    {
        drawSettingFieldImpl(dirtyOut, wrapperName, title, (void*) data, ImGuiDataType_U32, format);
    }

    template<>
    inline void PropertyRendererBase::drawSettingField<glm::vec2>(bool& dirtyOut, MV::string_view wrapperName,
                                                                  MV::string_view title,
                                                                  glm::vec2* data, MV::string_view format, float speed,
                                                                  float* min, float* max)
    {
        drawSettingFieldVec2Impl(dirtyOut, wrapperName, title, data);
    }
}


#endif //MV_EDITOR_PROPERTYRENDERERBASE_H
