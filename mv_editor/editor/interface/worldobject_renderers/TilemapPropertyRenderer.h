//
// Created by Matty on 19.12.2022.
//

#ifndef MV_EDITOR_TILEMAPPROPERTYRENDERER_H
#define MV_EDITOR_TILEMAPPROPERTYRENDERER_H

#include "common/Property.h"
#include "PropertyRendererBase.h"
#include "2d/TilemapProperty.h"

namespace Editor
{
    class TilemapPropertyRenderer : public PropertyRenderer<MV::TilemapProperty>
    {
    public:
        static bool Draw(const MV::Meta::Field* field, const MV::rc<MV::WorldObject>& wo, MV::Property* property);
    };
}


#endif //MV_EDITOR_TILEMAPPROPERTYRENDERER_H
