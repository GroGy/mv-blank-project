//
// Created by Matty on 05.06.2024.
//

#ifndef MV_EDITOR_WORLDOBJECTCONFIGCREATE_H
#define MV_EDITOR_WORLDOBJECTCONFIGCREATE_H

#include "common/Window.h"
#include "interface/common/AssetGenerator.h"
#include "async/WorldObjectConfigTask.h"
#include "meta/Class.h"

namespace Editor
{
    namespace Config {
        static constexpr MV::string_view sc_WorldObjectConfigCreateWindowName = "Create world object config";
    }

    class [[MV::Class(EditorWindow)]] WorldObjectConfigCreate : public Window, public IAssetGenerator {
    public:
        WorldObjectConfigCreate();

    public:

        void WindowSetup() override;

        void Render() override;

        void PostRender() override;

        void OnCleanup() override;

        void OnInit() override;

    private: // Task helper functions

        void startTask();

        void processTask();

        void processData();

    private: // Drawing functions

        void drawFields();

    public:

        MV::string m_pName;

        MV::string m_pError;


    private:
        MV::rc<WorldObjectConfigTask> m_pTask;

        MV::vector<MV::Meta::Class> m_pAllWorldObjectClasses;

        int32_t m_pSelectedIndex = -1;

        bool m_pHasData = false;

        bool m_pCreating = false;
    };
}


#endif //MV_EDITOR_WORLDOBJECTCONFIGCREATE_H
