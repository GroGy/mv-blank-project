//
// Created by Matty on 2021-11-02.
//

#ifndef MV_EDITOR_WORLDINSPECTOR_H
#define MV_EDITOR_WORLDINSPECTOR_H

#include "common/Window.h"
#include <EngineMinimal.h>

namespace MV {
    class World;
}

namespace Editor {
    namespace Config {
        static constexpr MV::string_view sc_WorldInspectorWindowName = "World inspector";
    }

    class [[MV::Class(EditorWindow)]] WorldInspector : public Window {
    public:
        WorldInspector();

        void OnCleanup() override;

        void OnInit() override;
    protected:
        void WindowSetup() override;

        void Render() override;

        void PostRender() override;
    private:

        void removeWorldObject(MV::World& world, uint64_t toErase) const;

        void drawSubsystems();

    private:

        uint64_t m_pRightClickedWorldObject = 0;

        MV::string m_pRenameWorldObjectNewName;
        
    };


}



#endif //MV_EDITOR_WORLDINSPECTOR_H
