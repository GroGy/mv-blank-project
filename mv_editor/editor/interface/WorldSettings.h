//
// Created by Matty on 28.07.2024.
//

#ifndef MV_EDITOR_WORLDSETTINGS_H
#define MV_EDITOR_WORLDSETTINGS_H

#include "common/Window.h"
#include "meta/Class.h"
#include "interface/worldobject_renderers/PropertyRendererBase.h"
#include <EngineMinimal.h>

namespace MV
{
    class World;
}

namespace Editor
{
    namespace Config
    {
        static constexpr MV::string_view sc_WorldSettingsWindowName = "World settings";
    }

    class [[MV::Class(EditorWindow)]] WorldSettings : public Window, public EditorRendererHelper
    {
    public:
        WorldSettings();

        void OnCleanup() override;

        void OnInit() override;

        void WindowSetup() override;

        void Render() override;

        void PostRender() override;

    public:

        void SetWorld(MV::World* World);

        void SetSubsystemToDraw(MV::Meta::Class subsystemClass);

    private:

        void drawWorldSettings();

        void drawSubsystemSettings();

    private:

        MV::World* m_pWorld = nullptr;

        MV::Meta::Class m_pSubsystemClass;

    };


}


#endif //MV_EDITOR_WORLDSETTINGS_H
