//
// Created by Matty on 07.12.2022.
//

#ifndef MV_EDITOR_ASSEMBLYINSPECTOR_H
#define MV_EDITOR_ASSEMBLYINSPECTOR_H

#include "common/Window.h"

namespace Editor {
    namespace Config {
        static constexpr MV::string_view sc_AssemblyInspectorWindowName = "Assembly inspector";
    }

    class [[MV::Class(EditorWindow)]] AssemblyInspector : public Window {
    public:

        AssemblyInspector();

    public: // Overrides

        void WindowSetup() override;

        void Render() override;

        void PostRender() override;

        void OnCleanup() override;

        void OnInit() override;

    private: // Internal drawing helpers

        void drawMissingData();

        void drawSelectPanel();

        void drawInfoPanel();

        void drawEnumInfo(uint64_t index);

        void drawClassInfo(uint64_t index);

        void drawMemberInfoPanel();

    private: // Data loading helper functions

        void updateData();

        void updateSearch();

    private: // Helper functions

        bool hasAnyData() const;

        void gotoClass(uint32_t token);

    private: // Primitive fields

        uint32_t m_pDataVersion = 0;

        int64_t m_pSelectedIndex = -1;

    private: // Search fields

        struct SearchPassedEntry {
            enum Type {
                Enum,
                Class
            };

            uint64_t m_Index;
            MV::string m_Name;
            Type m_Type;
            uint32_t m_ClassToken;
        };

        MV::string m_pSearchInput;

        MV::vector<SearchPassedEntry> m_pFilteredEntries;
    };
}


#endif //MV_EDITOR_ASSEMBLYINSPECTOR_H
