//
// Created by Matty on 2022-02-17.
//

#ifndef MV_EDITOR_TOOLBAR_H
#define MV_EDITOR_TOOLBAR_H

#include "common/Window.h"

namespace Editor {
    namespace Config {
        static constexpr MV::string_view sc_ToolbarWindowName = "TopToolbar";
        static constexpr float sc_ToolbarButtonSizePercentageOfHeight = 0.5f;
    }

    class [[MV::Class(EditorWindow)]] Toolbar : public Window {
    public:
        Toolbar();
    protected:
        void WindowSetup() override;

        void Render() override;

        void PostRender() override;

        void NoRender() override;
    public:
        void OnCleanup() override;

        void OnInit() override;
    };
}


#endif //MV_EDITOR_TOOLBAR_H
