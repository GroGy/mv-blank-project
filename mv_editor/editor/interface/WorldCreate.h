//
// Created by Matty on 2021-11-03.
//

#ifndef MV_EDITOR_WORLDCREATE_H
#define MV_EDITOR_WORLDCREATE_H

#include "common/Window.h"
#include "../async/WorldTask.h"
#include "common/AssetSelector.h"
#include "interface/worldobject_renderers/PropertyRendererBase.h"

namespace Editor {
    namespace Config {
        static constexpr MV::string_view sc_WorldCreateWindowName = "Create world";
    }

    class [[MV::Class(EditorWindow)]] WorldCreate : public Window, public EditorRendererHelper {
    public:

        WorldCreate();

    protected:

        void WindowSetup() override;

        void Render() override;

        void PostRender() override;

        void OnCleanup() override;

        void OnInit() override;

    public:

        void SetPath(MV::string_view path);

    private:

        MV::rc<WorldTask> m_pTask;

        bool m_pCreating = false;

        MV::string m_pPathBase;

        MV::string m_pName;

        MV::string m_pError;

        glm::uvec2 m_pSize{128,128};
    };
}

#endif //MV_EDITOR_WORLDCREATE_H
