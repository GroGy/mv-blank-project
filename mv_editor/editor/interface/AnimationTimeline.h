//
// Created by Matty on 2022-01-28.
//

#ifndef MV_EDITOR_ANIMATIONTIMELINE_H
#define MV_EDITOR_ANIMATIONTIMELINE_H

#include "common/Window.h"

namespace Editor {
    namespace Config {
        static constexpr MV::string_view sc_AnimationTimelineWindowName = "Animation timeline";
    }

    class [[MV::Class(EditorWindow)]] AnimationTimeline : public Window {
    private:
        uint32_t m_pFrameIndex = 0;
        uint32_t m_pFrameMin = 0;
        uint32_t m_pFrameMax = 64;

        uint32_t m_pOffsetFrame = 0;
        float m_pZoom = 1.0f;

        MV::string m_pSelectTimeline;

        bool m_pTransformOpen = false;
    protected:
        void WindowSetup() override;

        void Render() override;

        void PostRender() override;
    public:
        AnimationTimeline();

        void OnCleanup() override;

        void OnInit() override;
    };
}

#endif //MV_EDITOR_ANIMATIONTIMELINE_H
