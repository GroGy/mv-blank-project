//
// Created by Matty on 2021-10-24.
//

#ifndef TEST_EDITOR_CAMERA_H
#define TEST_EDITOR_CAMERA_H

#include "common/Window.h"
#include "../imgui/imgui.h"
#include <fmt/format.h>

namespace Editor {
    namespace Config {
        static constexpr MV::string_view sc_LogWindowName = "Debug##editor_logger";
        static constexpr MV::string_view sc_ScriptLogFmt = "{}:{} : {}\n";
    }

    class [[MV::Class(EditorWindow)]] LogWindow : public Window {
    public:
        static MV::rc<LogWindow> s_Instance;

        LogWindow();

        void AddLog(MV::string_view message);

        void OnCleanup() override;
        void OnInit() override;
        void Clear();
    protected:
        void WindowSetup() override;

        void Render() override;

        void PostRender() override;
    private:
        ImGuiTextBuffer m_pBuffer;
        ImGuiTextFilter m_pFilter;
        ImVector<int32_t> m_pLineOffsets;        // Index to lines offset
        bool m_pScrollToBottom;

    };
}


#endif //MV_EDITOR_CAMERA_H
