//
// Created by Matty on 2022-01-20.
//

#ifndef MV_EDITOR_TILESETCREATE_H
#define MV_EDITOR_TILESETCREATE_H

#include <resources/AtlasResource.h>
#include <common/EngineCommon.h>
#include "common/Window.h"
#include "../async/TilesetTask.h"
#include "common/AssetSelector.h"
#include "../imgui/imgui.h"
#include "interface/common/AssetGenerator.h"

namespace Editor {
    namespace Config {
        static constexpr MV::string_view sc_TilesetCreateWindowName = "Create tileset";
    }

    class [[MV::Class(EditorWindow)]] TilesetCreate : public Window , public IAssetGenerator {
        enum TextureState {
            Loaded,
            Pending,
            None
        };

    public:
        TilesetCreate();
    protected:
        void WindowSetup() override;

        void Render() override;

        void PostRender() override;
    public:
        void OnCleanup() override;

        void OnInit() override;
    private:
        MV::rc<TilesetTask> m_pTask;
        bool m_pCreating = false;

        void drawMappings();

        void generateMappings();


        MV::rc<MV::AtlasResource> m_pSelectedAtlasResource;
        AssetSelector m_AssetSelector;
        MV::string m_pName;
        MV::string m_pError;
        std::vector<uint32_t> m_pIDToAtlasID;

        glm::vec2 m_pTextureSize;
        uint64_t m_pSelectedAtlas = 0;
        ImTextureID m_pImGuiTextureID;
        TextureState m_pTextureState = None;
        MV::RTHandle m_pTextureHandle;
    };
}

#endif //MV_EDITOR_TILESETCREATE_H
