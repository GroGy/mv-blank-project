//
// Created by Matty on 24.06.2024.
//

#ifndef MV_EDITOR_EDITORTEXTURE_H
#define MV_EDITOR_EDITORTEXTURE_H

#include "common/EngineDef.h"
#include "imgui/imgui.h"
#include "render/common/handle/RTHandle.h"
#include "vec2.hpp"

namespace Editor {
    class EditorTexture
    {
    public:

        enum LoadState {
            Unloaded,
            Loading,
            Loaded
        };

    public:

        FORCEINLINE bool IsValid() const { return m_pLoadState == Loaded; }

        FORCEINLINE LoadState GetLoadState() const { return m_pLoadState; }

        FORCEINLINE ImTextureID GetHandle() const { return m_pHandle; }

        FORCEINLINE glm::vec2 GetSize() const { ENSURE_TRUE(m_pLoadState == Loaded, {}); return m_pSize; }

    public:

        void Unload();

        void Load(MV::RTHandle textureHandle);

    private:

        LoadState m_pLoadState = Unloaded;

        ImTextureID m_pHandle;

        glm::vec2 m_pSize;
    };
}


#endif //MV_EDITOR_EDITORTEXTURE_H
