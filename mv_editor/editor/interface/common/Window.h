//
// Created by Matty on 2021-10-15.
//

#ifndef MV_EDITOR_WINDOW_H
#define MV_EDITOR_WINDOW_H

#include <render/common/FrameContext.h>
#include "meta/EngineObject.h"
#include "common/OSDragAndDropEvent.h"

namespace Editor {
    class [[MV::Class()]] Window : public MV::EngineObject {
    public:
        explicit Window(MV::string && name, bool showInMenu = true);
        void OnRender();
        virtual void OnCleanup() = 0;
        virtual void OnInit() = 0;
        virtual void Update(float deltaTime) {};
        virtual void OnWorldRender() {};
        virtual void OnOSDragAndDropEvent(OSDragAndDropEvent& event) {};
        void SwitchBetweenOpenAndClose() { m_pOpen = !m_pOpen; OnOpenStateChanged(m_pOpen); };
        void SetOpen(bool val) { m_pOpen = val; OnOpenStateChanged(val); };
        [[nodiscard]] bool IsOpen() const { return m_pOpen; }
        [[nodiscard]] bool ShowInMenu() const { return m_pShowInMenu; }
        [[nodiscard]] MV::string GetName() const { return m_pName; }
        [[nodiscard]] MV::string_view GetNameV() const { return m_pName; }

        void FocusInDock();

    protected:
        int m_Flags = 0;
        virtual void WindowSetup() = 0;
        virtual void Render() = 0;
        virtual void PostRender() = 0;
        virtual bool ShouldRender() const {return true;}
        virtual void NoRender() {}; //Called when we dont render this window
        virtual void OnResize() {};
        /// Called when window should process input
        virtual void ProcessInput() {};

        virtual void OnOpenStateChanged(bool newValue) {};

        virtual bool GetIsFocused() const;


        MV::string m_pName;
        bool m_pOpen = true;
        bool m_pShowInMenu = true;
        bool m_pShowDirtyDot = false;

        glm::vec2 m_pSize;
        bool m_pNewlyCreated = true;
    };
}


#endif //MV_EDITOR_WINDOW_H
