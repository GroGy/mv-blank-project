//
// Created by Matty on 05.07.2024.
//

#ifndef MV_EDITOR_TEXTUREVIEWPORT_H
#define MV_EDITOR_TEXTUREVIEWPORT_H

#include "EditorTexture.h"
#include "vec4.hpp"

namespace Editor
{
    /// Class that create a viewport for texture, with zooming and pooling of drag and overlay.
    /// Also supports clipping
    /// Does not create new window, should be used as widget
    class TextureViewport
    {
    public: // Types

        // Callback invoked to allow custom drawing over texture
        // Params:
        // 1. TextureViewport* This
        // 2. ViewRect Current view of viewport (xy = UV0, zw = UV1)
        using DrawCustomOverlayCallback = eastl::function<void(TextureViewport*, const glm::vec4&)>;

        enum class DraggingState {
            None,

            Camera,
            Selection,
            Element
        };

    public: // API

        void Init(EditorTexture* texture);

        void Draw(MV::string_view label, const glm::vec2& size);

        void ResetState();

        void MoveToPosition(const glm::vec2& position);

        void MoveToUV(const glm::vec4& uv);

        FORCEINLINE float GetPixelScale() const { return m_pPixelScale; }

    public: // Drawing helpers

        /// Transforms position from texture space to scaled transformed viewport space
        glm::vec2 TransformToViewport(const glm::vec2& pixelPosition) const;

    public: // Callback

        DrawCustomOverlayCallback m_DrawCustomOverlayCallback;

    private: // Internal helper functions

        void processInput(const glm::vec4& bounds);

        /// Returns transformed bounds of texture
        glm::vec4 drawView(const glm::vec4& bounds);

        void updateAnimation();

    public: // Config

        bool m_ShowGrid = false;

        bool m_ShowOrigin = false;

        uint32_t m_GridPixelScale = 16;

        bool m_DrawScale = false;

        bool m_DrawMousePosition = false;

    private: // Internal fields

        float m_pPixelScale = 2.0f;

        float m_pPixelScaleBase = 2.0f;

        EditorTexture* m_pTexture = nullptr;

        glm::vec2 m_pMouseDragStart;

        glm::vec2 m_pCameraDragStart;

        glm::vec2 m_pCameraOffset = {0,0};

        bool m_pIsDraggingCamera = false;

        glm::vec2 m_pFocusPosition = {0,0};

        bool m_pInAnimation = false;
    };
}


#endif //MV_EDITOR_TEXTUREVIEWPORT_H
