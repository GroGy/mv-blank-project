//
// Created by Matty on 01.09.2023.
//

#ifndef MV_EDITOR_PROPERTYDRAGANDDROPPAYLOAD_H
#define MV_EDITOR_PROPERTYDRAGANDDROPPAYLOAD_H

#include <cstdint>
#include <limits>

namespace MV {
    class Property;
    class WorldObject;
}

namespace Editor
{
    struct PropertyDragAndDropPayload
    {
        MV::Property* m_Property = nullptr;
        MV::WorldObject* m_Owner = nullptr;
        uint32_t m_PropertyId = std::numeric_limits<uint32_t>::max();
    };
}
#endif //MV_EDITOR_PROPERTYDRAGANDDROPPAYLOAD_H
