//
// Created by matty on 01.02.2023.
//

#ifndef TEST_GAME_GRADIENTEDITOR_H
#define TEST_GAME_GRADIENTEDITOR_H

#include <EngineMinimal.h>
#include "common/Gradient.h"
#include "imgui/imgui.h"

namespace Editor {
    namespace Config {
        static constexpr float sc_GradientEditorWindowSize[2] = {720, 240};
    }

    class GradientEditor {
    public:

        bool Draw(MV::string_view windowName);

        void Open(MV::Gradient * gradient);

        void Close();

    private: // Helper functions

        void preDrawSetup();

        bool drawContent();

    private: // Color picker functions

        bool drawColorPicker(MV::string_view windowName);

        void openColorPicker(glm::vec4 * color);

        void closeColorPicker();

    private: // Internal fields

        bool m_pOpen = false;

        MV::Gradient * m_pGradient = nullptr;

        bool m_pColorPickerOpen = false;

        glm::vec4 * m_pColorPickerColor = nullptr;

        int32_t m_pDraggingIndex = -1;
    };
}
namespace ImGui {
    IMGUI_API bool GradientPreviewButton(const char * id,MV::Gradient * gradient, const ImVec2 & size = {0,0});
}


#endif //TEST_GAME_GRADIENTEDITOR_H
