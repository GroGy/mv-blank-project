//
// Created by Matty on 26.06.2024.
//

#ifndef MV_EDITOR_GENERALPOPUP_H
#define MV_EDITOR_GENERALPOPUP_H

#include <EngineMinimal.h>
#include <common/Types.h>

namespace Editor {
    enum class PopupType {
        Ok,
        ConfirmOrCancel,
        YesNo,

        CustomModal
    };

    using PopupCloseCallback = eastl::function<void(bool)>;

    // Draws content of modal popup, return true to close
    using CustomModalDrawCallback = eastl::function<bool()>;

    class GeneralPopup
    {
    public:

        bool OpenOk(MV::string_view title, PopupCloseCallback&& callback, MV::string_view body = "");

        bool OpenConfirmOrCancel(MV::string_view title, PopupCloseCallback&& callback, MV::string_view body = "");

        bool OpenYesNo(MV::string_view title, PopupCloseCallback&& callback, MV::string_view body = "");

        bool OpenCustomModal(MV::string_view title, CustomModalDrawCallback&& callback);

    public: // Getters

        FORCEINLINE bool IsOpen() const { return m_pOpen; }

    public: // Drawing

        void Draw();

    private:

        bool open(MV::string_view title, PopupCloseCallback&& callback, MV::string_view body, PopupType type);

    private:

        bool m_pOpen = false;

        bool m_pNewlyOpenned = false;

        PopupCloseCallback m_pCallback;

        CustomModalDrawCallback m_pCustomModalCallback;

        MV::string m_pTitle;

        MV::string m_pBody;

        PopupType m_pType;
    };
}


#endif //MV_EDITOR_GENERALPOPUP_H
