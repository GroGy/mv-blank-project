//
// Created by Matty on 06.04.2023.
//

#ifndef MV_EDITOR_VIEWPORTWIDGET_H
#define MV_EDITOR_VIEWPORTWIDGET_H

#include "common/EngineCommon.h"
#include "imgui/imgui.h"
#include "common/camera/Camera.h"
#include "imgui/imgui_internal.h"
#include "render/common/CameraData.h"
#include "world_picking/ViewportMousePickingImpl.h"
#include "meta/Field.h"
#include <initializer_list>
#include <type_traits>
#include <EngineMinimal.h>
#include <render/common/Renderer.h>

namespace Editor
{
    class IViewportGizmoProvider
    {
    public: // Setters

        virtual void SetPosition(const glm::vec2& position) = 0;

        virtual void SetRotation(float rotation) = 0;

        virtual void SetScale(const glm::vec2& scale) = 0;

        virtual void SetTransform(const glm::mat4& transform) = 0;

    public: // Getters

        [[nodiscard]]
        virtual glm::vec2 GetPosition() const = 0;

        [[nodiscard]]
        virtual float GetRotation() const = 0;

        [[nodiscard]]
        virtual glm::vec2 GetScale() const = 0;

        [[nodiscard]]
        virtual glm::mat4 GetTransform() const;

        [[nodiscard]]
        virtual const MV::unordered_map<MV::string, MV::Meta::Field>* GetWorldObjectFieldsOverride() const { return nullptr; };
    };

    /// Base class for viewport widget, contains all function, is there to allow passing outside of templates
    class IViewportWidget
    {
    public: // Types

        enum class ViewportOperation
        {
            Moving,
            Scaling,
            Rotating
        };


        enum class ViewportAction
        {
            None,
            Moving,
            Zooming //TODO
        };

    public: // General API

        virtual void OnResize() = 0;

        void SetGizmoProvider(MV::rc<IViewportGizmoProvider> provider);

    public: // Config fields

        bool m_DrawGrid = false;

        /// When enabled, allows moving with camera by dragging mouse
        bool m_AllowTranslation = true;

        /// When enabled, allows rotation of camera with mouse
        bool m_AllowRotation = true;

        /// When enabled, shows changing scale (zooming) with scroll
        bool m_AllowScale = true;

        /// When enabled, shows gizmo for currently selected object
        bool m_AllowGizmo = true;

        /// When enabled, shows property gizmos for currently selected object
        bool m_AllowPropertyGizmos = true;

    protected:

        virtual void DrawImpl(MV::string_view label, ImTextureID texture, const ImVec2& size_arg = {0.0f, 0.0f});

        MV::vector<ImTextureID> m_pCameraTextures;

        MV::CameraHandle m_pTexturesHandle = MV::sc_InvalidCameraHandle;

        bool recreateCameraTexturesIfNeeded(const MV::rc<MV::Camera>& camera);

        void drawGrid(const MV::rc<MV::Camera>& camera);

        void drawGizmo(const MV::rc<MV::Camera>& camera);

        MV::rc<IViewportGizmoProvider> m_pGizmoProvider;

        void processInput(const ImVec2& size, const MV::rc<MV::Camera>& camera);

        void drawPropertyGizmos(const ImVec2& size);

        enum TextureState {
            NotLoaded,
            Pending,
            Loaded
        };

        TextureState m_CameraTexturesState = NotLoaded;

    private:

        ViewportAction m_pViewportAction = ViewportAction::None;

        ViewportOperation m_pViewportOperation = ViewportOperation::Moving;

        ImVec2 m_pStartPos = {};

        glm::vec2 m_pCameraStartPos = {};


    };

    /// Renders actual world view to texture and handles all input based on passed params
    template<MousePickingBehavior MousePicking = MousePickingBehavior::Disabled>
    class ViewportWidget : public IViewportWidget
    {
    private: // Impl fields

        ViewportMousePickingImpl<MousePicking> m_MousePickingOps;

    public:

        ViewportWidget() = default;

        void Draw(MV::string_view label, const MV::rc<MV::Camera>& camera)
        {
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0.0f, 0.0f});

            const auto viewportFlags =
                    ImGuiWindowFlags_NoDecoration |
                    ImGuiWindowFlags_NoScrollWithMouse;

            if (!ImGui::Begin(label.data(), nullptr, viewportFlags | ImGuiWindowFlags_MenuBar))
            {
                ImGui::End();
                ImGui::PopStyleVar();
                return;
            }
            ENSURE_VALID(camera);

            auto size_arg = ImGui::GetContentRegionAvail();

            auto ratio = size_arg.y / size_arg.x;
            if (ratio != camera->GetRatio())
            {
                camera->SetRatio(ratio);
            }

            recreateCameraTexturesIfNeeded(camera);

            ImGui::PopStyleVar();

            drawMenuBar(camera);

            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0.0f, 0.0f});


            if (m_CameraTexturesState == Loaded)
                IViewportWidget::DrawImpl(label, m_pCameraTextures[MV::GetRenderer()->GetCurrentFrameIndex()],
                                          size_arg);
            else
            {
                auto min = ImGui::GetCurrentWindow()->DC.CursorPos;
                ImGui::GetWindowDrawList()->AddRectFilled(min, min + size_arg, IM_COL32(0, 0, 0, 255));
                ImGui::ItemHoverable(ImRect{min, min + size_arg}, ImGui::GetID("##viewport_missing_texture"));
            }

            processInput(size_arg, camera);

            if (m_DrawGrid)
                drawGrid(camera);

            if (m_AllowGizmo)
                drawGizmo(camera);

            if(m_AllowPropertyGizmos && m_AllowGizmo)
                drawPropertyGizmos(size_arg);

            ImGui::End();
            ImGui::PopStyleVar();
        }

        void OnResize() override
        {
            m_pCameraTextures.clear();
            m_pTexturesHandle = MV::sc_InvalidCameraHandle;
            m_CameraTexturesState = NotLoaded;
        }

    private:

        void drawMenuBar(const MV::rc<MV::Camera>& camera)
        {
            if (ImGui::BeginMenuBar())
            {
                if (ImGui::BeginMenu("Gizmo"))
                {
                    ImGui::MenuItem("Grid", nullptr, &m_DrawGrid);

                    ImGui::EndMenu();
                }

                if (ImGui::BeginMenu("Control"))
                {
                    ImGui::EndMenu();
                }

                if (ImGui::BeginMenu("Preview"))
                {
                    ImGui::EndMenu();
                }

                auto ResButton = [&](MV::string_view res, const glm::uvec2& value)
                {
                    if (ImGui::MenuItem(res.data()))
                    {
                        camera->SetResolution(value);
                        OnResize();
                    }
                };

                if (ImGui::BeginMenu("Camera"))
                {
                    if (ImGui::BeginMenu("Resolution"))
                    {
#define RES_STR(V) #V
#define RES(R, X, Y) ResButton(RES_STR(X##x##Y)#R,{X,Y})
                        RES((16:9), 640, 360);
                        RES((4:3), 800, 600);
                        RES((4:3), 1024, 768);
                        RES((16:9), 1280, 720);
                        RES((16:1), 1280, 800);
                        RES((5:4), 1280, 1024);
                        RES((16:9), 1360, 768);
                        RES((16:9), 1366, 768);
                        RES((16:1), 1440, 900);
                        RES((16:9), 1536, 864);
                        RES((16:9), 1600, 900);
                        RES((4:3), 1600, 1200);
                        RES((16:1), 1680, 1050);
                        RES((16:9), 1920, 1080);
                        RES((16:1), 1920, 1200);
                        RES((16:9), 2048, 1152);
                        RES((4:3), 2048, 1536);
                        RES((21:9), 2560, 1080);
                        RES((16:9), 2560, 1440);
                        RES((16:1), 2560, 1600);
                        RES((21:9), 3440, 1440);
                        RES((16:9), 3840, 2160);
#undef RES
#undef RES_STR
                        ImGui::EndMenu();
                    }

                    ImGui::EndMenu();
                }

                ImGui::EndMenuBar();
            }
        }
    };
}


#endif //MV_EDITOR_VIEWPORTWIDGET_H
