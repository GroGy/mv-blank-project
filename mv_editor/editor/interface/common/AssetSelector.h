//
// Created by Matty on 2021-10-31.
//

#ifndef MV_EDITOR_ASSETSELECTOR_H
#define MV_EDITOR_ASSETSELECTOR_H

#include <resources/common/Asset.h>

namespace Editor {
    namespace Config {

    }
    static const char *const ASSET_SELECTOR_POPUP_ID = "##asset_selector";

    class AssetSelector {
    private:
        MV::unordered_set<MV::AssetType> m_pTypeFilter;
        uint64_t m_pSelected;
        bool m_pWasOpened = true;
        bool m_pFinished = false;
        bool m_pMultiselect = false;
    public:
        static bool DrawAssetSelector(const char * name,bool open, const MV::unordered_set<MV::AssetType> & types, MV::UUID & res);
        static bool DrawAssetSelector(const char * name,bool open, const MV::unordered_set<MV::AssetType> & types, MV::IAssetReference & res);

        void SetFilters(const std::initializer_list<MV::AssetType> & types);
        void SetMultiselect(bool multiselect);
        void ClearFilters();
        void Render();
        void Open();
        void Clear();

        [[nodiscard]] bool HasSelected() const { return m_pFinished; }
        [[nodiscard]] uint64_t GetSelected() const { return m_pSelected; }

    };
}


#endif //MV_EDITOR_ASSETSELECTOR_H
