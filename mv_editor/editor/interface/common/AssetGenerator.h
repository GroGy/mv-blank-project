//
// Created by Matty on 2022-09-20.
//

#ifndef MV_EDITOR_ASSETGENERATOR_H
#define MV_EDITOR_ASSETGENERATOR_H

#include <EngineMinimal.h>

namespace Editor {
    class [[MV::Class(Abstract)]] IAssetGenerator {
    public:
        // Directory where result should be placed
        MV::string m_CreateDirectory;
    protected:

        virtual // Builds result path
        MV::string GetAssetPath(MV::string_view assetName, bool withMvExtension = true) const;
    };
}

#endif //MV_EDITOR_ASSETGENERATOR_H
