//
// Created by Matty on 07.12.2022.
//

#ifndef MV_EDITOR_CLASSSELECTOR_H
#define MV_EDITOR_CLASSSELECTOR_H

#include "interface/common/Window.h"
#include "meta/Class.h"

namespace Editor {
    namespace Config {
        static constexpr MV::string_view sc_ClassSelectorWindowName = "##ClassSelector";
    }

    class ClassSelector {
    private:
        MV::Meta::Class m_pBaseClass;
        MV::vector<MV::Meta::Class> m_pData;
        MV::Meta::Class m_pSelected;
        bool m_pWasOpened = false;
        bool m_pFinished = false;
        bool m_pHasFilter = false;
    public:
        //static bool DrawAssetSelector(const char * name,bool open, const MV::unordered_set<MV::AssetType> & types, uint64_t & res);

        void SetFilter(const MV::Meta::Class & base);
        void ClearFilters();
        void Render();
        void Open();
        void Clear();

        [[nodiscard]] bool HasSelected() const { return m_pFinished; }
        [[nodiscard]] MV::Meta::Class GetSelected() const { return m_pSelected; }

    };
}


#endif //MV_EDITOR_CLASSSELECTOR_H
