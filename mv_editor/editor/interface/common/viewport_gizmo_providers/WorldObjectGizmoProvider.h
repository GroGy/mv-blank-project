//
// Created by Matty on 09.04.2023.
//

#ifndef MV_EDITOR_WORLDOBJECTGIZMOPROVIDER_H
#define MV_EDITOR_WORLDOBJECTGIZMOPROVIDER_H

#include "common/World.h"
#include "common/WorldObject.h"
#include "interface/common/ViewportWidget.h"

namespace Editor {
    class WorldObjectGizmoProvider : public IViewportGizmoProvider
    {
    public: // Public API

        WorldObjectGizmoProvider(const MV::rc<MV::WorldObject> & wo, MV::World & world, bool modifyDataWhenRunning = false);

    public: // Overrides
        void SetPosition(const glm::vec2& position) override;

        void SetRotation(float rotation) override;

        void SetScale(const glm::vec2& scale) override;

        void SetTransform(const glm::mat4& transform) override;

        [[nodiscard]]
        glm::vec2 GetPosition() const override;

        [[nodiscard]]
        float GetRotation() const override;

        [[nodiscard]]
        glm::vec2 GetScale() const override;

    public: // Getters

        [[nodiscard]]
        const MV::rc<MV::WorldObject> & GetWorldObject() const {return m_pWorldObject;};

    protected: // Callback

        virtual void OnDataChanged() {};

    protected: // Internal fields

        MV::rc<MV::WorldObject> m_pWorldObject;

        MV::World & m_pWorld;

        bool m_pModifyDataWhenRunning;
    };

    class ActiveWorldWorldObjectGizmoProvider : public WorldObjectGizmoProvider {
    public:
        ActiveWorldWorldObjectGizmoProvider(const MV::rc<MV::WorldObject>& wo, MV::World& world, bool saveOnChange = true);

    protected:

        void OnDataChanged() override;

    private:

        bool m_pSaveOnChange;
    };

}


#endif //MV_EDITOR_WORLDOBJECTGIZMOPROVIDER_H
