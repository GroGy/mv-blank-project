//
// Created by Matty on 2022-09-03.
//

#ifndef MV_EDITOR_FILEBROWSER_H
#define MV_EDITOR_FILEBROWSER_H

#include <filesystem>
#include <EngineMinimal.h>

namespace Editor {
    /// Mode in which file browser will work
    enum class FileBrowserMode {
            OPEN_SINGLE,
            OPEN_MULTIPLE,

            OPEN_DIRECTORY,

            SAVE,
    };

    class FileBrowser {
    public:
        static MV::rc<Editor::FileBrowser> Get(FileBrowserMode mode);

        virtual void Open() = 0;
        virtual void Open(std::filesystem::path dir) = 0;
        virtual void Init() {};
        virtual void Cleanup() {};

        void SetTypeFilters(const MV::vector<MV::string> & fileFilters);
        void SetMode(FileBrowserMode mode);

        const MV::vector<MV::string> & GetSelectedFiles() const;
        const MV::string & GetSelectedFile() const;

        const MV::string & GetSavePath() const;
    protected:
        explicit FileBrowser(FileBrowserMode mode);

        FileBrowserMode m_pMode;

        MV::vector<MV::string> m_pFileFilters;

        /// Selected files, used when OPEN_MULTIPLE mode is used
        MV::vector<MV::string> m_pFiles;

        /// Selected file, used when OPEN_SINGLE mode is used
        MV::string m_pFile;

        /// Selected save path, used when SAVE mode is used
        MV::string m_pSave;
    };
}


#endif //MV_EDITOR_FILEBROWSER_H
