//
// Created by Matty on 2021-10-31.
//

#ifndef MV_EDITOR_IMAGEPREVIEW_H
#define MV_EDITOR_IMAGEPREVIEW_H

#include <EngineMinimal.h>
#include "../../imgui/imgui.h"

namespace Editor {
    namespace Config {

    }
    static const char *const IMAGE_PREVIEW_POPUP_ID = "##asset_selector";

    class ImagePreview {
    public:
        void Render();

        void OpenHandle(MV::RTHandle textureHandle);

        void OpenAsset(MV::UUID assetID);

        void Clear();

    private:

        bool m_pWasOpened = true;

        MV::RTHandle m_pTextureHandle;

        bool m_pImGuiImageCreated = false;

        ImTextureID m_pImGuiTextureID;

        glm::uvec2 m_pTextureRes;

        glm::uvec2 m_pPreviewSize;

    };
}


#endif //MV_EDITOR_IMAGEPREVIEW_H
