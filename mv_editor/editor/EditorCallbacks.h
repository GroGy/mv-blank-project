//
// Created by Matty on 2021-10-23.
//

#ifndef MV_EDITOR_EDITORCALLBACKS_H
#define MV_EDITOR_EDITORCALLBACKS_H

#include <EngineMinimal.h>
#include <render/common/FrameContext.h>

namespace Editor {
    struct EditorCallbacks{
        eastl::function<bool()> onCleanup{};
        eastl::function<bool()> onRendererCleanup{};
        eastl::function<void()> onWindowResize{};
        eastl::function<void()> onInit{};
        eastl::function<void()> onPostInit{};
        eastl::function<void(float)> onUpdate{};
        eastl::function<void()> onRender{};
        eastl::function<void()> onEditorRender{};
    };
}

#endif //MV_EDITOR_EDITORCALLBACKS_H
