//
// Created by Matty on 21.12.2022.
//

#include "AssetDependencyGraph.h"

namespace Editor {
    void AssetDependencyGraph::Serialize(nlohmann::json& field) {

    }

    void AssetDependencyGraph::Deserialize(const nlohmann::json& field) {

    }

    void AssetDependencyGraph::AddDependency(AssetDependencyElement element, MV::IAssetReference* asset) {

    }

    void AssetDependencyGraph::RemoveDependency(AssetDependencyElement element, MV::IAssetReference* asset) {

    }

    bool AssetDependencyGraph::DependsOnAsset(AssetDependencyElement element, MV::IAssetReference* asset) const {
        return false;
    }

    bool AssetDependencyGraph::DependsOnAsset(AssetDependencyElement element, MV::UUID asset) const {
        return false;
    }

    MV::optional<MV::vector<MV::UUID>> AssetDependencyGraph::GetDepedencies(AssetDependencyElement element) const {
        return {};
    }

    MV::optional<MV::vector<AssetDependencyElement>>
    AssetDependencyGraph::GetSelfDepedencies(MV::IAssetReference* asset) const {
        return {};
    }

    MV::optional<MV::vector<AssetDependencyElement>> AssetDependencyGraph::GetSelfDepedencies(MV::UUID asset) const {
        return {};
    }
}