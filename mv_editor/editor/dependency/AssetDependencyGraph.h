//
// Created by Matty on 21.12.2022.
//

#ifndef MV_EDITOR_ASSETDEPENDENCYGRAPH_H
#define MV_EDITOR_ASSETDEPENDENCYGRAPH_H

#include <EngineMinimal.h>

namespace Editor {
    // Asset reference source
    struct AssetDependencyElement {
        enum Type {
            Asset,
            WorldObject
        };

        Type m_Type;

        union {
            MV::UUID m_UUID;
            uint64_t m_WorldObject;
        };
    };

    class AssetDependencyGraph {
    public:

        void AddDependency(AssetDependencyElement element, MV::IAssetReference * asset);

        void RemoveDependency(AssetDependencyElement element, MV::IAssetReference * asset);

        bool DependsOnAsset(AssetDependencyElement element, MV::IAssetReference * asset) const;
        bool DependsOnAsset(AssetDependencyElement element, MV::UUID asset) const;

        MV::optional<MV::vector<MV::UUID>> GetDepedencies(AssetDependencyElement element) const;

        MV::optional<MV::vector<AssetDependencyElement>> GetSelfDepedencies(MV::IAssetReference * asset) const;
        MV::optional<MV::vector<AssetDependencyElement>> GetSelfDepedencies(MV::UUID asset) const;

    public: // Serialization function

        void Serialize(nlohmann::json & field);

        void Deserialize(const nlohmann::json & field);

    private: // Internal fields

        // Contains which assets certain AssetDependencyElements depends on
        MV::unordered_map<AssetDependencyElement, MV::vector<MV::UUID>> m_pDependsOnAssets;

        // Contains which AssetDependencyElements depend on asset
        MV::unordered_map<MV::UUID, MV::vector<AssetDependencyElement>> m_pDependsByAsset;
    };
}


#endif //MV_EDITOR_ASSETDEPENDENCYGRAPH_H
