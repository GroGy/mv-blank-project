//
// Created by Matty on 06.04.2023.
//

#ifndef MV_EDITOR_VIEWPORTMOUSEPICKINGIMPL_H
#define MV_EDITOR_VIEWPORTMOUSEPICKINGIMPL_H

namespace Editor
{
    enum class MousePickingBehavior {
        Enabled,
        Disabled
    };

    template <MousePickingBehavior Behavior>
    class ViewportMousePickingImpl
    {

    };

    template<>
    class ViewportMousePickingImpl<MousePickingBehavior::Disabled> {
    public:

    };

    template<>
    class ViewportMousePickingImpl<MousePickingBehavior::Enabled> {
    public:

    };
}


#endif //MV_EDITOR_VIEWPORTMOUSEPICKINGIMPL_H
