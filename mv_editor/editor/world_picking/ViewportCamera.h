//
// Created by Matty on 03.04.2023.
//

#ifndef MV_EDITOR_VIEWPORTCAMERA_H
#define MV_EDITOR_VIEWPORTCAMERA_H


#include "common/camera/Camera.h"

namespace Editor {
    class ViewportCamera : public MV::Camera {
    protected:
        MV::RenderPassBlueprint CreateRenderPassBlueprint() override;
    };;
}


#endif //MV_EDITOR_VIEWPORTCAMERA_H
