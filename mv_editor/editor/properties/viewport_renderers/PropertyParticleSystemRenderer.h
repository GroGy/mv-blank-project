//
// Created by Matty on 21.07.2023.
//

#ifndef MV_EDITOR_PROPERTYPARTICLESYSTEMRENDERER_H
#define MV_EDITOR_PROPERTYPARTICLESYSTEMRENDERER_H


#include <EditorMinimal.h>
#include "PropertyViewportRenderer.h"
#include <particles/cpu/ParticleSystemProperty.h>
#include "vec4.hpp"
#include <imgui/imgui.h>

namespace Editor {
    class [[MV::Class(PropertyGizmo)]] PropertyParticleSystemRenderer : public PropertyViewportRenderer<MV::ParticleSystemProperty> {
    private:
    public:
        static void RenderProperty(const glm::vec4& viewport, MV::Property * property);
    };
}


#endif //MV_EDITOR_PROPERTYPARTICLESYSTEMRENDERER_H
