//
// Created by Matty on 21.07.2023.
//

#ifndef MV_EDITOR_PROPERTYVIEWPORTRENDERER_H
#define MV_EDITOR_PROPERTYVIEWPORTRENDERER_H

#include <EngineMinimal.h>
#include <EditorMinimal.h>
#include <common/Property.h>
#include <meta/EngineObject.h>


namespace Editor
{
    template <typename T>
    class PropertyViewportRenderer : public MV::EngineObject
    {
    public:

        using PropertyType = T;

    public:
         static void RenderProperty(const glm::vec4& viewport, MV::Property* property) {}

    };
}


#endif //MV_EDITOR_PROPERTYVIEWPORTRENDERER_H
