//
// Created by Matty on 2022-07-12.
//

#include "2d/BoxColliderProperty.h"
#include "PropertyViewportRenderer.h"
#include <imgui/imgui.h>

namespace Editor {
    class [[mv_register_property_gizmo_renderer()]] BoxColliderPropertyViewportRenderer : public PropertyViewportRenderer<MV::BoxColliderProperty> {
    private:
        static ImU32 s_pColliderColor;
    public:
        static void RenderProperty(const glm::vec4& viewport, MV::Property * property);
    };
}