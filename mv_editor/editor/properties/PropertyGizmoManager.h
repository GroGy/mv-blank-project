//
// Created by Matty on 2022-07-12.
//

#ifndef MV_EDITOR_PROPERTYGIZMOMANAGER_H
#define MV_EDITOR_PROPERTYGIZMOMANAGER_H


#include "vec4.hpp"
#include "common/Property.h"

namespace Editor {
    class PropertyGizmoManager {
    public:
        // Initializes property viewport draw callbacks, which are used to draw property editor viewport elements, like icon, preview, range, etc
        void Init();

        //TODO: Rewrite this, this is just terrible, WTF is fields override
        void DrawProperties(const glm::vec4 & viewport,const MV::rc<MV::WorldObject> & worldObject, const MV::unordered_map<MV::string, MV::Meta::Field>* fieldsOverride = nullptr);

    };
}


#endif //MV_EDITOR_PROPERTYGIZMOMANAGER_H
