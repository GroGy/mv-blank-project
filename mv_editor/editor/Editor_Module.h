//
// Created by Matty on 2021-10-01.
//

#ifndef MV_EDITOR_EDITOR_MODULE_H
#define MV_EDITOR_EDITOR_MODULE_H

#include <module/Module.h>
#include "EditorCallbacks.h"

namespace Editor {
    class Editor_Module : public MV::Module {
    private:
        EditorCallbacks m_pEditorCallbacks;
    public:
        explicit Editor_Module(EditorCallbacks editor);

        bool OnInit() override;

        void OnUpdate(float deltaTime) override;

        void OnRender() override;

        bool OnCleanup() override;

        bool OnRenderCleanup() override;

        MV::string GetModuleName() const override;

        bool OnPostInit() override;

        void OnEditorRender() override;

        void OnWindowResize() override;
    };
}


#endif //MV_EDITOR_EDITOR_MODULE_H
