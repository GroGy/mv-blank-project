//
// Created by Matty on 2022-01-23.
//

#ifndef MV_EDITOR_EDITORSETTINGS_VALS_H
#define MV_EDITOR_EDITORSETTINGS_VALS_H

#include <EngineMinimal.h>

namespace Editor {
    namespace Config {
        static constexpr MV::string_view sc_DefaultEditorSettingsPath = "editor_settings.json";
    }

    struct EditorSettingsVals {
        bool m_DrawViewportGrid = false;
        float m_GridLineWidth = 3.0f;
        bool m_DrawDebugColliders = false;
        bool m_DrawGizmos = false;
        float m_UIScale = 1.0f;

        void Serialize(const MV::string & path = "");

        void Deserialize(const MV::string & path = "");
    };
}

#endif //MV_EDITOR_EDITORSETTINGS_VALS_H
