//
// Created by Matty on 11.12.2023.
//

#ifndef MV_EDITOR_TESTSERIALIZATIONOBJECT_H
#define MV_EDITOR_TESTSERIALIZATIONOBJECT_H

#include "EngineMinimal.h"
#include "common/World.h"

namespace Editor {
    class [[MV::Trivial()]] TestSerializationObject
    {
    public:
        [[MV::Field(Serialize)]] MV::vector<MV::WorldObjectData> WOs;
    };
}



#endif //MV_EDITOR_TESTSERIALIZATIONOBJECT_H
