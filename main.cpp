#include <iostream>
#include <Engine.h>
#include "allocator/Alloc.h"
#include "mv_editor/editor/Editor.h"
#include "internal/backward.hpp"
#include "common/LaunchParams.h"

int main(int argc, char* argv[])
{
    backward::SignalHandling sh{};

    MV::LaunchParams::Get().Load(argc, argv);

    MV::EngineConfig config{};

    if (!config.Init())
    {
        std::cerr << "Failed to initialize engine configuration." << std::endl;
        return -1;
    }

    config.m_RenderBackend = MV::RendererBackend::VULKAN;
    config.m_RendererDebugMessages = false;

    try
    {
        MV::Engine engine{config};

#if MV_EDITOR
        Editor::Editor editor{};

        if(config.m_EngineMode == MV::EngineMode::Editor) {
            MV::PushAllocatorFlag(MV::AllocationType::Editor);

            editor.Init(engine);

            MV::PopAllocatorFlag();
        }
#endif

        engine.Run();
    } catch(std::exception & e) {
        std::cout << e.what() << std::endl;
    }

    return 0;
}
