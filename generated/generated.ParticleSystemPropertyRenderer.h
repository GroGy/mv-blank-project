//
// Created by Matty on 2022-11-12.
//

#ifndef MV_GENERATED_PARTICLESYSTEMPROPERTY_RENDERER_H
#define MV_GENERATED_PARTICLESYSTEMPROPERTY_RENDERER_H

#include <precompiler/PropertyFieldRendererRegistrator.h>
#include <interface/worldobject_renderers/ParticleSystemPropertyRenderer.h>

namespace MV_generated {
    static Editor::PropertyFieldRendererRegistrator<Editor::ParticleSystemPropertyRenderer> s_ParticleSystemPropertyRenderer_Registrator;
}


#endif //MV_GENERATED_PARTICLESYSTEMPROPERTY_RENDERER_H
