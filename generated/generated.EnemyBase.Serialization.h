//
// File generated by mv_precompiler
// DO NOT EDIT !!!
//

#ifndef MV_ENEMYBASE_GENERATED_GUARD_H
#define MV_ENEMYBASE_GENERATED_GUARD_H

#include "precompiler/ClassSerializerRegistrator.h"
#include "serialization/common/FieldSerializer.h"

#include <../../mv_project/source/EnemyBase.h>

namespace MV_generated {
    static constexpr uint64_t sc_Sprite_EnemyBase_FieldId = 0x87be309bbfdd9b24;
    
}

namespace MV {
     template <> class ClassSerializer<Project::EnemyBase>;
    

    
    template <> class ClassSerializer<Project::EnemyBase> {
    public:
        static bool WriteClass(const Project::EnemyBase & object, Archive & archive,  const SerializationWhitelist& whitelist) {
            MV::Archive classArchive;

            ///////////////////// BASE SERIALIZATION /////////////////////
            if(!MV::TryToWriteParent<MV::WorldObject>(object, classArchive, whitelist)) return false;
            
            //////////////////////////////////////////////////////////////

            
            if(!SerField<&Project::EnemyBase::Sprite>(&object, MV_generated::sc_Sprite_EnemyBase_FieldId ,classArchive, whitelist)) return false;
            

            return MV::ClassSerializer<MV::Archive>::Write(classArchive, archive, whitelist);
        }

        static FieldReadResult ReadClass(Project::EnemyBase & object, Archive & archive, const SerializationWhitelist& whitelist) {
             bool endOfRead = false;

             bool reserialize = false;

             MV::Archive classArchive;
             auto readRes = MV::ClassSerializer<MV::Archive>::Read(classArchive, archive, whitelist);

             if(readRes != FieldReadResult::Ok) {
                 return readRes;
             }

             classArchive.SetToRead();

            ///////////////////// BASE SERIALIZATION /////////////////////
            
            {
                const auto parentRead = MV::TryToReadParent<MV::WorldObject>(object, classArchive, whitelist);
                if(parentRead != FieldReadResult::Ok) return parentRead;
            }
            
            //////////////////////////////////////////////////////////////

             do {
                 bool readData = false;
                 bool anyFieldMissing = false;

                 
                 if(!ProcessFieldReadResult(DeserField<&Project::EnemyBase::Sprite>(&object, MV_generated::sc_Sprite_EnemyBase_FieldId ,classArchive, whitelist), readData, anyFieldMissing)) return FieldReadResult::Error;
                 

                 if(anyFieldMissing) reserialize = true;

                 if(classArchive.OnEnd()) endOfRead = true;

                 if(!readData && !endOfRead) {
                     if(!SkipArchiveClassField(classArchive)) {
                         ENSURE_NO_ENTRY(FieldReadResult::Error);
                         return FieldReadResult::Error;
                     }
                 }
             } while(!endOfRead);

             //TODO: Implement reserialization

             return FieldReadResult::Ok;
        }

        static MV::vector<ClassFieldInfo> GetFields()
        {
            MV::vector<ClassFieldInfo> res;

            res.emplace_back(ClassFieldInfo{.m_Id = MV_generated::sc_Sprite_EnemyBase_FieldId, .m_DisplayName = "Sprite", .m_Offset = MV::GetFieldOffset<&Project::EnemyBase::Sprite>()});
            
            return res;
        }

        static uint32_t GetClassSize(const Project::EnemyBase& object, const SerializationWhitelist& wl)
        {
            uint32_t total = sizeof(uint32_t); // All serializable custom classes have size of their "archive" before them in memory

            
            {
                 uint32_t sz = GetFieldSize<&Project::EnemyBase::Sprite>(&object, wl);
                 if(sz == 0U) { ENSURE_NO_ENTRY(0U); return 0U; }
                 total += sz;
            }
            

            return total;
        }

        static bool Write(const Project::EnemyBase& in, Archive & ar,const SerializationWhitelist& wl, SerializationCallbackMode = SerializationCallbackMode::No) {
              return WriteClass(in, ar, wl);
        }

        static FieldReadResult Read(Project::EnemyBase& out, Archive & ar, const SerializationWhitelist& wl, SerializationCallbackMode = SerializationCallbackMode::No) {
              return ReadClass(out, ar, wl);
        }

        static uint32_t GetSize(const Project::EnemyBase& in, const SerializationWhitelist& wl) {
            return GetClassSize(in, wl);
        }
    };
    
}

namespace MV_generated {
    
    static MV::ClassSerializerRegistrator<Project::EnemyBase, 0x610bc87dd85f5473> s_EnemyBase_Serialization_Registrator
    {
         [](const void* out, MV::Archive& ar, const MV::SerializationWhitelist& wl, MV::SerializationCallbackMode cmode){
            const Project::EnemyBase& obj = *((Project::EnemyBase*)out);
            auto res = MV::ClassSerializer<Project::EnemyBase>::WriteClass(obj, ar, wl);
            

            return res;
         },
         [](void* in, MV::Archive& ar, const MV::SerializationWhitelist& wl, MV::SerializationCallbackMode cmode){
              Project::EnemyBase& obj = *((Project::EnemyBase*)in);

              auto res = MV::ClassSerializer<Project::EnemyBase>::ReadClass(obj, ar, wl);

              if(res != MV::FieldReadResult::Ok) return res;

              

              return res;

         },
         [](void* in, const MV::SerializationWhitelist& wl){
             Project::EnemyBase& obj = *((Project::EnemyBase*)in);
             return MV::ClassSerializer<Project::EnemyBase>::GetClassSize(obj, wl);
         },
         MV::ClassSerializer<Project::EnemyBase>::GetFields()
    };
    
}


#endif //MV_ENEMYBASE_GENERATED_GUARD_H
