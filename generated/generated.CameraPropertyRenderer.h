//
// Created by Matty on 2022-11-12.
//

#ifndef MV_GENERATED_CAMERAPROPERTY_RENDERER_H
#define MV_GENERATED_CAMERAPROPERTY_RENDERER_H

#include <precompiler/PropertyFieldRendererRegistrator.h>
#include <interface/worldobject_renderers/CameraPropertyRenderer.h>

namespace MV_generated {
    static Editor::PropertyFieldRendererRegistrator<Editor::CameraPropertyRenderer> s_CameraPropertyRenderer_Registrator;
}


#endif //MV_GENERATED_CAMERAPROPERTY_RENDERER_H
