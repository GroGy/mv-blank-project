//
// Created by Matty on 2022-11-12.
//

#ifndef MV_GENERATED_BOXCOLLIDERPROPERTY_RENDERER_H
#define MV_GENERATED_BOXCOLLIDERPROPERTY_RENDERER_H

#include <precompiler/PropertyFieldRendererRegistrator.h>
#include <interface/worldobject_renderers/BoxColliderPropertyRenderer.h>

namespace MV_generated {
    static Editor::PropertyFieldRendererRegistrator<Editor::BoxColliderPropertyRenderer> s_BoxColliderPropertyRenderer_Registrator;
}


#endif //MV_GENERATED_BOXCOLLIDERPROPERTY_RENDERER_H
