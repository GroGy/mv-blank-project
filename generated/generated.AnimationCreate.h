//
// File generated by mv_precompiler
// DO NOT EDIT !!!
//

#ifndef MV_ANIMATIONCREATE_GENERATED_GUARD_H
#define MV_ANIMATIONCREATE_GENERATED_GUARD_H

#include <precompiler/InterfaceWindowRegistrator.h>

#include <interface/AnimationCreate.h>

namespace MV_generated {
    static Editor::WindowRegistrator<Editor::AnimationCreate> s_AnimationCreate_Registrator;
}


#endif //MV_ANIMATIONCREATE_GENERATED_GUARD_H
