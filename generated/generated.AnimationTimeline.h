//
// File generated by mv_precompiler
// DO NOT EDIT !!!
//

#ifndef MV_ANIMATIONTIMELINE_GENERATED_GUARD_H
#define MV_ANIMATIONTIMELINE_GENERATED_GUARD_H

#include <precompiler/InterfaceWindowRegistrator.h>

#include <interface/AnimationTimeline.h>

namespace MV_generated {
    static Editor::WindowRegistrator<Editor::AnimationTimeline> s_AnimationTimeline_Registrator;
}


#endif //MV_ANIMATIONTIMELINE_GENERATED_GUARD_H
