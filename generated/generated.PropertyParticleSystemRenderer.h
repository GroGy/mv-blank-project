//
// File generated by mv_precompiler
// DO NOT EDIT !!!
//

#ifndef MV_PROPERTYPARTICLESYSTEMRENDERER_GENERATED_GUARD_H
#define MV_PROPERTYPARTICLESYSTEMRENDERER_GENERATED_GUARD_H

#include <precompiler/PropertyGizmoRegistrator.h>

#include <properties/viewport_renderers/PropertyParticleSystemRenderer.h>

namespace MV_generated {
    static Editor::PropertyGizmoRegistrator<Editor::PropertyParticleSystemRenderer> s_PropertyParticleSystemRenderer_Registrator;
}


#endif //MV_PROPERTYPARTICLESYSTEMRENDERER_GENERATED_GUARD_H
