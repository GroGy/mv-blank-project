//
// File generated by mv_precompiler
// DO NOT EDIT !!!
//

#ifndef MV_OBJECTEDITOR_GENERATED_GUARD_H
#define MV_OBJECTEDITOR_GENERATED_GUARD_H

#include <precompiler/InterfaceWindowRegistrator.h>

#include <interface/ObjectEditor.h>

namespace MV_generated {
    static Editor::WindowRegistrator<Editor::ObjectEditor> s_ObjectEditor_Registrator;
}


#endif //MV_OBJECTEDITOR_GENERATED_GUARD_H
