//
// Created by Matty on 2022-11-12.
//

#ifndef MV_GENERATED_TILEMAPPROPERTY_RENDERER_H
#define MV_GENERATED_TILEMAPPROPERTY_RENDERER_H

#include <precompiler/PropertyFieldRendererRegistrator.h>
#include <interface/worldobject_renderers/TilemapPropertyRenderer.h>

namespace MV_generated {
    static Editor::PropertyFieldRendererRegistrator<Editor::TilemapPropertyRenderer> s_TilemapPropertyRenderer_Registrator;
}


#endif //MV_GENERATED_TILEMAPPROPERTY_RENDERER_H
