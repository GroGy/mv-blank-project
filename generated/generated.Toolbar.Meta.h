//
// File generated by mv_precompiler
// DO NOT EDIT !!!
//

#ifndef MV_TOOLBAR_META_GENERATED_GUARD_H
#define MV_TOOLBAR_META_GENERATED_GUARD_H

#include <meta/ClassFactory.h>
#include <precompiler/ClassMetaRegistrator.h>
#include <serialization/common/FieldSerializer.h>
#include <common/World.h>

#include "../../mv_editor/editor/interface/Toolbar.h"

namespace MV_generated {

    
    static MV::ClassMetaRegistrator<Editor::Toolbar, 0xfa3961b9af5a3ed7> s_Toolbar_Meta_Registrator
    {
        []() -> MV::ClassMetaCreationResult {
             static_assert(std::is_base_of<MV::EngineObject,Editor::Toolbar>::value, "Non-trivial Non-Abstract meta classes must inherit from MV::EngineObject"); 
            MV::ClassMetaCreationResult result;

            ///////////////////// BASE CLASSES /////////////////////
            
            result.BaseClasses.emplace_back(0x9822cba161b92228); // Window
            
            ////////////////////////////////////////////////////////



            MV::Meta::ClassCreateInfo info;

            ////////////////////// CLASS META //////////////////////

            info.m_Name = "Toolbar";
            info.m_Abstract = false;
            info.m_Trivial = false;

            
            info.m_Fields = {
            
            };

            result.PostInitCallback = [](MV::Meta::Class* klass){
                  
            };

            

            
            info.m_CTor = [](MV::ObjectInitializationContext& ctx) -> MV::rc<MV::EngineObject> {
                

                
                auto newObject = MV::make_rc<Editor::Toolbar>();
                

                

                MV::Meta::ClassFactory::SetClassInstanceHash(newObject.get(), 0xfa3961b9af5a3ed7);

                return newObject;
            };
            

            
            info.m_DTor = [](const MV::rc<MV::EngineObject>& self) {
                auto castedSelf = MV::static_pointer_cast<Editor::Toolbar>(self);
                
            };
            

            ////////////////////////////////////////////////////////

            result.Klass = MV::Meta::ClassFactory::CreateClassMeta(MV::move(info));

            return result;
        }
    };
    
}


#endif //MV_TOOLBAR_META_GENERATED_GUARD_H
