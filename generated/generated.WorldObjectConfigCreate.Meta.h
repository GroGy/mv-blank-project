//
// File generated by mv_precompiler
// DO NOT EDIT !!!
//

#ifndef MV_WORLDOBJECTCONFIGCREATE_META_GENERATED_GUARD_H
#define MV_WORLDOBJECTCONFIGCREATE_META_GENERATED_GUARD_H

#include <meta/ClassFactory.h>
#include <precompiler/ClassMetaRegistrator.h>
#include <serialization/common/FieldSerializer.h>
#include <common/World.h>

#include "../../mv_editor/editor/interface/WorldObjectConfigCreate.h"

namespace MV_generated {

    
    static MV::ClassMetaRegistrator<Editor::WorldObjectConfigCreate, 0x2b9fe0fa815fd35d> s_WorldObjectConfigCreate_Meta_Registrator
    {
        []() -> MV::ClassMetaCreationResult {
             static_assert(std::is_base_of<MV::EngineObject,Editor::WorldObjectConfigCreate>::value, "Non-trivial Non-Abstract meta classes must inherit from MV::EngineObject"); 
            MV::ClassMetaCreationResult result;

            ///////////////////// BASE CLASSES /////////////////////
            
            result.BaseClasses.emplace_back(0x9822cba161b92228); // Window
            
            result.BaseClasses.emplace_back(0x8be370ec7f60f46); // IAssetGenerator
            
            ////////////////////////////////////////////////////////



            MV::Meta::ClassCreateInfo info;

            ////////////////////// CLASS META //////////////////////

            info.m_Name = "WorldObjectConfigCreate";
            info.m_Abstract = false;
            info.m_Trivial = false;

            
            info.m_Fields = {
            
            };

            result.PostInitCallback = [](MV::Meta::Class* klass){
                  
            };

            

            
            info.m_CTor = [](MV::ObjectInitializationContext& ctx) -> MV::rc<MV::EngineObject> {
                

                
                auto newObject = MV::make_rc<Editor::WorldObjectConfigCreate>();
                

                

                MV::Meta::ClassFactory::SetClassInstanceHash(newObject.get(), 0x2b9fe0fa815fd35d);

                return newObject;
            };
            

            
            info.m_DTor = [](const MV::rc<MV::EngineObject>& self) {
                auto castedSelf = MV::static_pointer_cast<Editor::WorldObjectConfigCreate>(self);
                
            };
            

            ////////////////////////////////////////////////////////

            result.Klass = MV::Meta::ClassFactory::CreateClassMeta(MV::move(info));

            return result;
        }
    };
    
}


#endif //MV_WORLDOBJECTCONFIGCREATE_META_GENERATED_GUARD_H
