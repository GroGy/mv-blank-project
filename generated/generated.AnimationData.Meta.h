//
// File generated by mv_precompiler
// DO NOT EDIT !!!
//

#ifndef MV_ANIMATIONDATA_META_GENERATED_GUARD_H
#define MV_ANIMATIONDATA_META_GENERATED_GUARD_H

#include <meta/ClassFactory.h>
#include <precompiler/ClassMetaRegistrator.h>
#include <serialization/common/FieldSerializer.h>
#include <common/World.h>

#include "resources/AnimationData.h"

namespace MV_generated {

    
    static MV::ClassMetaRegistrator<MV::AnimationInputData, 0x191a237ded1c4d28> s_AnimationInputData_Meta_Registrator
    {
        []() -> MV::ClassMetaCreationResult {
            
            MV::ClassMetaCreationResult result;

            ///////////////////// BASE CLASSES /////////////////////
            
            ////////////////////////////////////////////////////////



            MV::Meta::ClassCreateInfo info;

            ////////////////////// CLASS META //////////////////////

            info.m_Name = "AnimationInputData";
            info.m_Abstract = false;
            info.m_Trivial = true;

            

            

            

            ////////////////////////////////////////////////////////

            result.Klass = MV::Meta::ClassFactory::CreateClassMeta(MV::move(info));

            return result;
        }
    };
    
    static MV::ClassMetaRegistrator<MV::AnimationKeyframeData, 0x584e6b3be49e3e08> s_AnimationKeyframeData_Meta_Registrator
    {
        []() -> MV::ClassMetaCreationResult {
            
            MV::ClassMetaCreationResult result;

            ///////////////////// BASE CLASSES /////////////////////
            
            ////////////////////////////////////////////////////////



            MV::Meta::ClassCreateInfo info;

            ////////////////////// CLASS META //////////////////////

            info.m_Name = "AnimationKeyframeData";
            info.m_Abstract = false;
            info.m_Trivial = true;

            

            

            

            ////////////////////////////////////////////////////////

            result.Klass = MV::Meta::ClassFactory::CreateClassMeta(MV::move(info));

            return result;
        }
    };
    
    static MV::ClassMetaRegistrator<MV::AnimationSequenceData, 0x65a64de8d2ccc441> s_AnimationSequenceData_Meta_Registrator
    {
        []() -> MV::ClassMetaCreationResult {
            
            MV::ClassMetaCreationResult result;

            ///////////////////// BASE CLASSES /////////////////////
            
            ////////////////////////////////////////////////////////



            MV::Meta::ClassCreateInfo info;

            ////////////////////// CLASS META //////////////////////

            info.m_Name = "AnimationSequenceData";
            info.m_Abstract = false;
            info.m_Trivial = true;

            

            

            

            ////////////////////////////////////////////////////////

            result.Klass = MV::Meta::ClassFactory::CreateClassMeta(MV::move(info));

            return result;
        }
    };
    
    static MV::ClassMetaRegistrator<MV::AnimationPropertyData, 0xaaeb1676fd679e9d> s_AnimationPropertyData_Meta_Registrator
    {
        []() -> MV::ClassMetaCreationResult {
            
            MV::ClassMetaCreationResult result;

            ///////////////////// BASE CLASSES /////////////////////
            
            ////////////////////////////////////////////////////////



            MV::Meta::ClassCreateInfo info;

            ////////////////////// CLASS META //////////////////////

            info.m_Name = "AnimationPropertyData";
            info.m_Abstract = false;
            info.m_Trivial = true;

            

            

            

            ////////////////////////////////////////////////////////

            result.Klass = MV::Meta::ClassFactory::CreateClassMeta(MV::move(info));

            return result;
        }
    };
    
    static MV::ClassMetaRegistrator<MV::SingleAnimationData, 0x38c9f0fe6ca0dc3c> s_SingleAnimationData_Meta_Registrator
    {
        []() -> MV::ClassMetaCreationResult {
            
            MV::ClassMetaCreationResult result;

            ///////////////////// BASE CLASSES /////////////////////
            
            ////////////////////////////////////////////////////////



            MV::Meta::ClassCreateInfo info;

            ////////////////////// CLASS META //////////////////////

            info.m_Name = "SingleAnimationData";
            info.m_Abstract = false;
            info.m_Trivial = true;

            

            

            

            ////////////////////////////////////////////////////////

            result.Klass = MV::Meta::ClassFactory::CreateClassMeta(MV::move(info));

            return result;
        }
    };
    
    static MV::ClassMetaRegistrator<MV::AnimationStateMachineData::StateData, 0xd10e14f719d83cab> s_StateData_Meta_Registrator
    {
        []() -> MV::ClassMetaCreationResult {
            
            MV::ClassMetaCreationResult result;

            ///////////////////// BASE CLASSES /////////////////////
            
            ////////////////////////////////////////////////////////



            MV::Meta::ClassCreateInfo info;

            ////////////////////// CLASS META //////////////////////

            info.m_Name = "StateData";
            info.m_Abstract = false;
            info.m_Trivial = true;

            

            

            

            ////////////////////////////////////////////////////////

            result.Klass = MV::Meta::ClassFactory::CreateClassMeta(MV::move(info));

            return result;
        }
    };
    
    static MV::ClassMetaRegistrator<MV::AnimationStateMachineData::StateTransitionData, 0xe80e45b91b8aa5f4> s_StateTransitionData_Meta_Registrator
    {
        []() -> MV::ClassMetaCreationResult {
            
            MV::ClassMetaCreationResult result;

            ///////////////////// BASE CLASSES /////////////////////
            
            ////////////////////////////////////////////////////////



            MV::Meta::ClassCreateInfo info;

            ////////////////////// CLASS META //////////////////////

            info.m_Name = "StateTransitionData";
            info.m_Abstract = false;
            info.m_Trivial = true;

            

            

            

            ////////////////////////////////////////////////////////

            result.Klass = MV::Meta::ClassFactory::CreateClassMeta(MV::move(info));

            return result;
        }
    };
    
    static MV::ClassMetaRegistrator<MV::AnimationStateMachineData, 0xabdeec59f5aae80a> s_AnimationStateMachineData_Meta_Registrator
    {
        []() -> MV::ClassMetaCreationResult {
            
            MV::ClassMetaCreationResult result;

            ///////////////////// BASE CLASSES /////////////////////
            
            ////////////////////////////////////////////////////////



            MV::Meta::ClassCreateInfo info;

            ////////////////////// CLASS META //////////////////////

            info.m_Name = "AnimationStateMachineData";
            info.m_Abstract = false;
            info.m_Trivial = true;

            

            

            

            ////////////////////////////////////////////////////////

            result.Klass = MV::Meta::ClassFactory::CreateClassMeta(MV::move(info));

            return result;
        }
    };
    
    static MV::ClassMetaRegistrator<MV::AnimationData, 0x9344b1d27bb20e34> s_AnimationData_Meta_Registrator
    {
        []() -> MV::ClassMetaCreationResult {
            
            MV::ClassMetaCreationResult result;

            ///////////////////// BASE CLASSES /////////////////////
            
            ////////////////////////////////////////////////////////



            MV::Meta::ClassCreateInfo info;

            ////////////////////// CLASS META //////////////////////

            info.m_Name = "AnimationData";
            info.m_Abstract = false;
            info.m_Trivial = true;

            

            

            

            ////////////////////////////////////////////////////////

            result.Klass = MV::Meta::ClassFactory::CreateClassMeta(MV::move(info));

            return result;
        }
    };
    
}


#endif //MV_ANIMATIONDATA_META_GENERATED_GUARD_H
