//
// File generated by mv_precompiler
// DO NOT EDIT !!!
//

#ifndef MV_PARAMETERNODES_GENERATED_GUARD_H
#define MV_PARAMETERNODES_GENERATED_GUARD_H

#include <precompiler/ShaderEditorRegistrator.h>

#include <tools/shader_editor/nodes/ParameterNodes.h>


namespace MV_generated {
    
    static Editor::ShaderNodeRegistrator<Editor::Vec4ParameterNode,false> s_Vec4ParameterNode_Registrator{"ParameterV4"};
    
}


#endif //MV_PARAMETERNODES_GENERATED_GUARD_H
