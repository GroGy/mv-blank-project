//
// File generated by mv_precompiler
// DO NOT EDIT !!!
//

#ifndef MV_ANIMATIONEDITOR_GENERATED_GUARD_H
#define MV_ANIMATIONEDITOR_GENERATED_GUARD_H

#include <precompiler/InterfaceWindowRegistrator.h>

#include <tools/animation_editor/AnimationEditor.h>

namespace MV_generated {
    static Editor::WindowRegistrator<Editor::AnimationEditor> s_AnimationEditor_Registrator;
}


#endif //MV_ANIMATIONEDITOR_GENERATED_GUARD_H
